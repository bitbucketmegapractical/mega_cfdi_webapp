<script th:inline="javascript" type="text/javascript">
// Array de clave producto servicio
var claveProductoServicioSource = [];

// Request de clave producto servicio
var claveProductoServicioRequest = /*[[@{/claveProductoServicioSourceRequest}]]*/;

// Array de unidad de medida
var unidadMedidaSource = [];

// Request de unidad de medida
var unidadMedidaRequest = /*[[@{/unidadMedidaSourceRequest}]]*/;

// Array de uso cfdi
var usoCfdiSource = [];

// Request de uso cfdi
var usoCfdiSourceRequest = /*[[@{/usoCfdiSourceRequest}]]*/;

// Array de residencia fiscal (Pais)
var residenciaFiscalSource = [];

// Request de residencia fiscal (Pais)
var residenciaFiscalSourceRequest = /*[[@{/residenciaFiscalSourceRequest}]]*/;

// Array de moneda
var monedaSource = [];

// Request de moneda
var monedaSourceRequest = /*[[@{/monedaSourceRequest}]]*/;

// Request adicionar concepto
var conceptoAddRequest = /*[[@{/cfdiFreeStamp}]]*/;

// Request cargar datos del concepto
var conceptoLoadRequest = /*[[@{/cfdiFreeConceptAdd}]]*/;

var receptorSeparator1Data = ['rfc', 'nombreRazonSocial', 'usoCfdi'];
var receptorSeparator1Value = 'div-sep-rec-01';

$(document).ready(function(){
	// Cargando clave producto servicio para autocomplete 
	claveProductoServicioLoadSource();
	
	// Autocomplete de clave producto servicio
	$('#claveProductoServicio').typeahead({
        source: claveProductoServicioSource
    });
	
	// Cargando unidad de medida para autocomplete
	unidadMedidaLoadSource();
	
	// Autocomplete de unidad medida
	$('#unidadMedida').typeahead({
        source: unidadMedidaSource
    });
	
	// Cargando uso cfdi
	usoCfdiLoadSource();
	
	// Autocomplete de uso cfdi
	$('#usoCfdi').typeahead({
        source: usoCfdiSource
    });
	
	// Cargando residencia fiscal (Pais)
	residenciaFiscalLoadSource();
	
	// Autocomplete de residencia fiscal
	$('#residenciaFiscal').typeahead({
        source: residenciaFiscalSource
    });
	
	// Cargando moneda
	monedaLoadSource();
	
	// Autocomplete de moneda
	$('#moneda').typeahead({
        source: monedaSource
    });
	
	// Modal panel adicionar concepto
	$('#btn-add-concept').on('click', function (e) {
		// Limpiando campos del modal
		conceptFieldReady();
		
		// Abriendo modal panel
        $("#myModal").modal();
        /*$("#myModalBody").text("");
        $.ajax({
            url: "signup",
            cache: false
        }).done(function (html) {
            $("#myModalBody").append(html);
        });*/
    })
    
    $('#loadEdit').on('click', function (e) {
		console.log("Load Edit Concept!");
		console.log("Value: " + getElementValue('loadEdit'));
		//editConcept();
    })
    
	$('#residente').change(function(){
		var residente = getElementValue(this.id);
		console.log('Residente: ' + residente);
		if(residente != 'Nacional'){
			elementReadOnlyFalse('residenciaFiscal');
			elementValueClear('residenciaFiscal');
			elementReadOnlyFalse('numeroRegistroIdentidadFiscal');
		}else{
			elementReadOnlyTrue('residenciaFiscal');
			elementValueChange('residenciaFiscal', 'MEX (México)');
			elementReadOnly('numeroRegistroIdentidadFiscal');
		}
	});
	
	// Emisor
	$('#certificate').change(function(){			
		elementFieldClassReset(this.id);
		//separatorHide(receptorSeparator1Data, receptorSeparator1Value);
	});
	
	$('#privateKey').change(function(){			
		elementFieldClassReset(this.id);
		//separatorHide(receptorSeparator1Data, receptorSeparator1Value);
	});
	
	$('#passwd').keyup(function(){
		elementFieldClassReset(this.id);
	}).change(function(){			
		elementFieldClassReset(this.id);
	});
	
	// Receptor 
	$('#rfc').keyup(function(){
		elementFieldClassReset(this.id);
		//elementClassRemove('div-sep-rec-01', 'form-group-separator col-xs-12');
		separatorHide(receptorSeparator1Data, receptorSeparator1Value);
	}).change(function(){			
		elementFieldClassReset(this.id);
		separatorHide(receptorSeparator1Data, receptorSeparator1Value);
	});
	
	$('#nombreRazonSocial').keyup(function(){
		elementFieldClassReset(this.id);
		separatorHide(receptorSeparator1Data, receptorSeparator1Value);
	}).change(function(){			
		elementFieldClassReset(this.id);
		separatorHide(receptorSeparator1Data, receptorSeparator1Value);
	});
	
	$('#usoCfdi').keyup(function(){
		elementFieldClassReset(this.id);
		separatorHide(receptorSeparator1Data, receptorSeparator1Value);
	}).change(function(){			
		elementFieldClassReset(this.id);
		separatorHide(receptorSeparator1Data, receptorSeparator1Value);
	});
	
    /*$('#addConcept').on('click', function (e){
    	addConcept();
	})*/
})

function separatorHide(value, pId){
	var hasError = false;
	if($.isArray(value)){
		for (var i = 0; i < value.length; i++) {
			var item = value[i];
			var elementId = 'div-' + item;
			if(isElementHasClass(elementId, 'has-error')){
				hasError = true;
				break;
			}
		}
		if(!hasError){
			elementClassRemove(pId, 'form-group-separator col-xs-12');
		}
	}else{
		elementClassRemove(pId, 'form-group-separator col-xs-12');
	}
}

function claveProductoServicioLoadSource(){
	console.log("[INFO] claveProductoServicioLoadSource instanciate!");
	//Sending ajax request
    $.ajax({
        url: claveProductoServicioRequest,
        method: 'POST',
        data: {}
    }).done(function (data) {								        	
    	for (var i = 0; i < data.length; i++) {
    		item = data[i];
    		claveProductoServicioSource[claveProductoServicioSource.length] = item;	
		}
    }).fail(function (xhr, ajaxOptions, thrownError) {
        console.log("ERROR");
    });
};

function unidadMedidaLoadSource(){
	console.log("[INFO] unidadMedidaLoadSource instanciate!");
	//Sending ajax request
    $.ajax({
        url: unidadMedidaRequest,
        method: 'POST',
        data: {}
    }).done(function (data) {								        	
    	for (var i = 0; i < data.length; i++) {
    		item = data[i];
    		unidadMedidaSource[unidadMedidaSource.length] = item;	
		}
    }).fail(function (xhr, ajaxOptions, thrownError) {
        console.log("ERROR");
    });
};

function usoCfdiLoadSource(){
	console.log("[INFO] usoCfdiLoadSource instanciate!");
	//Sending ajax request
    $.ajax({
        url: usoCfdiSourceRequest,
        method: 'POST',
        data: {}
    }).done(function (data) {								        	
    	for (var i = 0; i < data.length; i++) {
    		item = data[i];
    		usoCfdiSource[usoCfdiSource.length] = item;	
		}
    }).fail(function (xhr, ajaxOptions, thrownError) {
        console.log("ERROR");
    });
};

function residenciaFiscalLoadSource(){
	console.log("[INFO] residenciaFiscalLoadSource instanciate!");
	//Sending ajax request
    $.ajax({
        url: residenciaFiscalSourceRequest,
        method: 'POST',
        data: {}
    }).done(function (data) {								        	
    	for (var i = 0; i < data.length; i++) {
    		item = data[i];
    		residenciaFiscalSource[residenciaFiscalSource.length] = item;	
		}
    }).fail(function (xhr, ajaxOptions, thrownError) {
        console.log("ERROR");
    });
};

function monedaLoadSource(){
	console.log("[INFO] monedaLoadSource instanciate!");
	//Sending ajax request
    $.ajax({
        url: monedaSourceRequest,
        method: 'POST',
        data: {}
    }).done(function (data) {								        	
    	for (var i = 0; i < data.length; i++) {
    		item = data[i];
    		monedaSource[monedaSource.length] = item;	
		}
    }).fail(function (xhr, ajaxOptions, thrownError) {
        console.log("ERROR");
    });
};

function conceptFieldReady(){
	// Descripcion
	elementFieldReady('descripcion');
	// Clave producto servicio
	elementFieldReady('claveProductoServicio');
	// Unidad medida
	elementFieldReady('unidadMedida');
	// No. identificacion
	elementFieldReady('noIdentificacion');
	// Unidad
	elementFieldReady('unidad');
	// Cantidad
	elementFieldReady('cantidad');
	// Valor unitario
	elementFieldReady('valorUnitario');
	// Descuento
	elementFieldReady('descuento');
	// Importe
	elementFieldReady('importe');
}

function loadEditConcept(pIndex){					
	if(pIndex != null){
		$.ajax({
			url: conceptoAddRequest + '?loadEditConcept',
			type: "POST",
			data: {index : pIndex} 
		}).done(function (data) {								        	
			
			// Descripcion
			elementValueChange('descripcion', data.descripcion);
			// Clave producto servicio
			elementValueChange('claveProductoServicio', data.claveProductoServicio);
			// Unidad medida
			elementValueChange('unidadMedida', data.unidadMedida);
			// No. identificacion
			elementValueChange('noIdentificacion', data.noIdentificacion);
			// Unidad
			elementValueChange('unidad', data.unidad);
			// Cantidad
			elementValueChange('cantidad', data.cantidad);
			// Valor unitario
			elementValueChange('valorUnitario', data.valorUnitario);
			// Descuento
			elementValueChange('descuento', data.descuento);
			// Importe
			elementValueChange('importe', data.importe);
			
			$("#myModal").modal();
			
        }).fail(function (xhr, ajaxOptions, thrownError) {
            console.log("ERROR");
        });
	}
}

/*function addConcept(){
	var bValid = true;
	var descripcion = null;
	bValid = bValid && fieldRequired('descripcion');
	if(bValid){
		descripcion = getElementValue('descripcion');
	}
	
	if(bValid){
		$.ajax({
			url: conceptoAddRequest + '?addConcept',
			type: "POST",
			data: {descripcion : descripcion} 
		}).done(function (data) {								        	
			console.log("DONE!");
        }).fail(function (xhr, ajaxOptions, thrownError) {
            console.log("ERROR");
        });
	}else{
		console.log("[LOG] Error by required file" + descripcion);
	} 
	
}*/



function ajxTest(){
	var objConcept = new Object();
	objConcept['a'] = 1;
	objConcept['b'] = 2;
	objConcept['c'] = 3;
	
	var conceptArray = [];
	conceptArray[conceptArray.length] = objConcept;
	conceptArray[conceptArray.length] = objConcept;
	conceptArray[conceptArray.length] = objConcept;
	
	var formData = new FormData(jQuery('form')[0]);
	formData.append('objConcept', JSON.stringify(objConcept));
	formData.append('conceptArray', JSON.stringify(conceptArray));
	
	var ajxTestRequest = /*[[@{/ajxTestRequest}]]*/;
	
	$.ajax({
		url: ajxTestRequest,
       	type: 'POST',
       	data: formData,
       	cache: false,
       	contentType: false,
       	processData: false,
       	//Ajax events
       	//beforeSend: beforeSendHandler,
       	success:  function(data) {
       		if(jQuery.parseJSON(data) != null){
				var objJson = jQuery.parseJSON(data);
				var response = objJson.response;
				if(response.success){
					processingEnd();
					
					var toastr = response.notification.toastr;
					var ToastrType = toastr['type'];
					var toastrTitle = toastr['title'];
					var toastrMessage = toastr['message'];
					elementValueChange('rfc', objJson.rfc);
					elementValueChange('nameOrBusineesName', objJson.nameOrBusineesName);
					elementValueChange('email', objJson.email);
					elementValueChange('emitterTypeConfigured', objJson.personType);
					elementValueChange('taxRegimeConfigured', objJson.taxRegime);
					elementValueChange('certificateCreateDate', objJson.certifaicateCreationDate);
					elementValueChange('certificateExpiredDate', objJson.certificateValidDate);
					elementValueChange('certificateNumber', objJson.certificateNumber);
					
					emitterTaxInformationAfterUpdate();
					systemNotificationShow(ToastrType, toastrMessage, toastrTitle);
				}else if(response.error){
					var errorSource = response.errors;
					for (var i = 0; i < errorSource.length; i++) {
						var error = errorSource[i];
						var field = error['field'];
						var message = error['message'];
						
						elementForceError(field, message);
						
						separatorShow(separator1Data, separator1Value);
						separatorShow(separator2Data, separator2Value);
						
						if(field == 'certificate'){
							elementClassAdd('div-span-certificate-error', 'div-file-input-span-has-error');
							elementClassAdd(separatorValue, 'form-group-separator col-xs-12');
						}else if(field == 'private-key'){
							elementClassAdd('div-span-private-key-error', 'div-file-input-span-has-error');
						}
					}
					processingEnd();
				}
       		}
      	},
      	error: function (xhr, ajaxOptions, thrownError) {
      		console.log("ERROR! " + thrownError);
        }
	});
}
</script>