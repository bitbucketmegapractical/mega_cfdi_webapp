//##### CSRF Spring Security ###################################################################
$(function () {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
});
//##############################################################################################

//##############################################################################################
//##############################################################################################
function afterDocumentLoad() {
	// Mostrando el contenido de la pagina
	hiddenClassRemove('application-page-content');
	
	// Mostrando el Footer
	hiddenClassRemove('application-page-footer');
	
	// Eliminando el contenido del mensaje de 'Cargando informacion' inicial
	if(isElementExist('div-loading-initial-content')){
		elementRemove('div-loading-initial-content');
	}
	
	scrollToHere('application-page-content');
}

function beforeApplicationPageContentShow() {
	// Mostrando el Footer
	hiddenClassRemove('application-page-footer');
	
	// Eliminando el contenido del mensaje de 'Cargando informacion' inicial
	if(isElementExist('div-loading-initial-content')){
		elementRemove('div-loading-initial-content');
	}
	
	scrollToHere('application-page-content');
}
//##############################################################################################

//##############################################################################################
//##### Propiedades ############################################################################
var WINDOW_WIDTH = null;
var WINDOW_HEIGHT = null;
var VIEWPORT_WIDTH = null;
var VIEWPORT_HEIGHT = null;
var BROWSER_AREA_WIDTH = null;
var BROWSER_AREA_HEIGHT = null;

$(document).ready(
	function(){
		//systemProperties();

		// Para detectar si existe scroll 
		//scrollBody();
	}
);

$(window).resize(
	function(){
		//systemProperties();
	}
); 

function systemProperties(){
	// Window
	WINDOW_WIDTH = getWindowWidth();
	WINDOW_HEIGHT = getWindowHeight();

	// Window
	VIEWPORT_WIDTH = window.innerWidth;
	VIEWPORT_HEIGHT = window.innerHeight;
	
	// Browser
	BROWSER_AREA_WIDTH = document.body.clientWidth;
	BROWSER_AREA_HEIGHT = document.body.clientHeight;
}

//Devuelve el ancho visible de pantalla
function getWindowWidth(){
	return $(window).width();
}

// Devuelve el alto visible de pantalla
function getWindowHeight(){
	return $(window).height();
}

//Devuelve el ancho de un elemento
function getElementWidth(pElementId){
	return $("#"+pElementId).width();
}

// Cambia el ancho de un elemento
function setElementWidth(pElementId, pValue){
	$("#"+pElementId).css('width', pValue);
}

// Devuelve al alto de un elemento
function getElementHeight(pElementId){
	return $("#"+pElementId).height(); //innerHeight();
}

function getElementInnerHeight(pElementId){
	return $("#"+pElementId).innerHeight();
}

function getElementOuterHeight(pElementId){
	return $("#"+pElementId).outerHeight();
}

function getElementOuterHeightTrue(pElementId){
	return $("#"+pElementId).outerHeight(true);
}

// Cambia el alto de un elemento
function setElementHeight(pElementId, pValue){
	$("#"+pElementId).css('height', pValue + 'px');
}

function setElementMinHeight(pElementId, pValue){
	$("#"+pElementId).css('min-height', pValue + 'px');
}
//##############################################################################################

//##############################################################################################
//##### Funcion para comprar dos string ########################################################
String.prototype.equalsIgnoreCase = function(str){
    return (str != null && typeof str === 'string' && this.toUpperCase() === str.toUpperCase());
}
//##############################################################################################

var waitForFinalEvent = (function(){
	var timers = {};
	return function (callback, ms, uniqueId) {
	    if (!uniqueId) {
	      uniqueId = "Don't call this twice without a uniqueId";
	    }
	    if (timers[uniqueId]) {
	      clearTimeout (timers[uniqueId]);
	    }
	    timers[uniqueId] = setTimeout(callback, ms);
	};
})();

function redirect(formName){
	document.forms[formName].submit();
}

function redirectPost(pUrlRedirect, pPostValues){
	$.redirect(pUrlRedirect, pPostValues);
}

function redirectGet(pUrlRedirect){
	window.location.replace(pUrlRedirect);
}

function convertSeg(pMinute){
	return pMinute * 60;
}

function convertMiliseg(pMinute){
	return convertSeg(pMinute) * 1000;
}

function interval(pFunction, pTime){
	return setInterval(pFunction, pTime);
}

function intervalClear(pId){
	clearInterval(pId);
}

// Muestra mensajes globales en las páginas
function globalMessageShow(pMessage, pClass = null, pDismiss = null, pFadeOut = null){
	//@param pMessage : Texto del mensaje a mostrar
	//@param pClass : Clase css del mensaje	
	//@param pFadeOut (optional) : Tiempo de visibilidad del mensaje
	
	/*<div id="global-message" class="hidden" style="margin-bottom: 5px;">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<span id="global-message-text"></span>
	</div>*/
	
	var cssClass = pClass != null ? pClass : 'alert alert-danger alert-dismissable';
	
	$("#global-message").html("");
	if(pDismiss){
		$("#global-message").append(
			'<div id="global-message-alert" class="'+cssClass+'" role="alert" style="margin-bottom: 5px !important;"> ' +
				'<button type="button" data-dismiss="alert" class="close" aria-label="Close">&times;</button> ' +
				'<i id="i-global-message" aria-hidden="true"></i> ' +
				'<span id="global-message-text">'+pMessage+'</span> ' +
			'</div>'
		);
	}else{
		$("#global-message").append(
			'<div class="'+cssClass+'" role="alert" style="margin-bottom: 5px !important;"> ' +
			'<i id="i-global-message" aria-hidden="true"></i> ' +
			'<span id="global-message-text">'+pMessage+'</span>'
		);
	}
	
	if(isElementHasClass('global-message-alert', 'alert-danger') || isElementHasClass('global-message-alert', 'alert-warning')){
		elementClassAdd('i-global-message', 'fa fa-warning');
	}else if(isElementHasClass('global-message-alert', 'alert-info')){
		elementClassAdd('i-global-message', 'fa fa-info-circle');
	}else if(isElementHasClass('global-message-alert', 'alert-success')){
		elementClassAdd('i-global-message', 'fa fa-check-circle');
	}
	
	hiddenClassRemove('global-message-content');
	elementShow('global-message-content');
	scrollToHere('application-page-content');
	
	if(pFadeOut != null){
		$("#global-message-content").fadeOut(pFadeOut);
	}
}

// Oculta los mensajes globales
function globalMessageHide(){
	$("#global-message").html("");
	hiddenClass('global-message-content');
}

// Muestra mensajes globales en los paneles
function panelMessageShow(pElementContent, pElement, pMessage, pClass = null, pDismiss = null, pFadeOut = null){
	//@param pElementContent : Div contenedor
	//@param pElement : Div con el mensaje
	//@param pMessage : Texto del mensaje a mostrar
	//@param pClass : Clase css del mensaje	
	//@param pFadeOut (optional) : Tiempo de visibilidad del mensaje
	
	var divContent = "#" + pElementContent;
	var div = "#" + pElement;
	var divAlert = pElement + "-alert";
	var iId = "i-" + pElement;
	var cssClass = pClass != null ? pClass : 'alert alert-danger alert-dismissable';
	
	$(div).html("");	
	if(pDismiss){
		$(div).append(
			'<div id="'+divAlert+'" class="'+cssClass+'" role="alert" style="margin-bottom: 5px !important;"> ' +
				'<button type="button" data-dismiss="alert" class="close" aria-label="Close">&times;</button> ' +
				'<i id="'+iId+'" aria-hidden="true"></i> ' +
				'<span id="global-message-text">'+pMessage+'</span> ' +
			'</div>'
		);
	}else{
		$(div).append(
			'<div id="'+divAlert+'" class="'+cssClass+'" role="alert" style="margin-bottom: 5px !important;"> ' +
			'<i id="'+iId+'" aria-hidden="true"></i> ' +
			'<span id="global-message-text">'+pMessage+'</span>'
		);
	}
	
	if(isElementHasClass(divAlert, 'alert-danger') || isElementHasClass(divAlert, 'alert-warning')){
		elementClassAdd(iId, 'fa fa-warning');
	}else if(isElementHasClass(divAlert, 'alert-info')){
		elementClassAdd(iId, 'fa fa-info-circle');
	}else if(isElementHasClass(divAlert, 'alert-success')){
		elementClassAdd(iId, 'fa fa-check-circle');
	}
	
	hiddenClassRemove(pElementContent);
	elementShow(pElementContent);
	scrollToHere(pElementContent);
	
	if(pFadeOut != null){
		$("#"+pElementContent).fadeOut(pFadeOut);
	}
}

function panelMessageHide(pElementContent, pElement){
	$("#"+pElement).html("");
	hiddenClass(pElementContent);
}

function getJsErrorMessage(pArray, pCode){
	var result = null;
	for (var i = 0; i < pArray.length; i++) {
		var obj = pArray[i];
		if(obj.code == pCode){
			result = obj.message;
			break;
		}
	}
	return result;
}

function getJsErrorField(pArray, pCode){
	var result = null;
	for (var i = 0; i < pArray.length; i++) {
		var obj = pArray[i];
		if(obj.code == pCode){
			result = obj.field != null ? obj.field : null;
			break;
		}
	}
	return result;
}

function getJsErrorMessageByField(pArray, pField){
	var result = null;
	for (var i = 0; i < pArray.length; i++) {
		var obj = pArray[i];
		if(obj.field == pField){
			result = obj.message;
			break;
		}
	}
	return result;
}

function isElementExist(pElementId){		
	return $("#"+pElementId).length > 0;
	/*if($("#"+pElementId).length > 0)
		return true;
	return false;*/
}

//Convierte numero a string
function numberToString(pVal){
	return String(pVal);	
}

// Convierte string a numero
function stringToNumber(pVal){
	return Number(pVal);	
}

// Imprime en consola
function log(pText){
	console.log(pText);
}

function elementAppend(pElementId, pVal){
	$("#"+pElementId).append(pVal);	
}

// Longitud de un elemento 
// Nota: Elementos con value (input....)
function getElementLength(pElementId){	
	return getElementValue(pElementId).length;
}

// Longitud de un elemento 
// Nota: Elementos que no tienen value (label, span....)
function getElementTextLength(pElementId){	
	return getElementText(pElementId).length;
}

// Devuelve valor de un elemento
function getElementValue(pElementId){
	if(!isElementExist(pElementId)){
		return "";
	}			
	return $('#'+pElementId).val();
}

// Devuelve texto de un elemento
function getElementText(pElementId){
	return $("#"+pElementId).text();	
}

// Saber si un elemento tiene valor
function isElementHasValue(pElementId){
	if(!isElementExist(pElementId)){
		return false;
	}			
	if(isSelect(pElementId)){
		return isSelectHasValue(pElementId);
	}
	return getElementValue(pElementId) != "" ? true : false;
}

// Cambia el valor de un elemento
function elementValueChange(pElementId, pVal){	
	$("#"+pElementId).val(pVal);
	elementFieldClassReset(pElementId);
}

// Cambia el texto de un elemento
function elementTextChange(pElementId, pVal){	
	$("#"+pElementId).text(pVal);
}

// Elimina el texto de un elemento
function elementTextClear(pElementId){	
	$("#"+pElementId).text("").change();
}

// Elimina el valor de un elemento
function elementValueClear(pElementId){	
	$("#"+pElementId).val("").change();
}

// Limpia un elemento
function elementClear(pElementId){	
	$("#"+pElementId).html("");	
}

//Elimina un elemento
function elementRemove(pElementId){
	$("#"+pElementId).remove();
}

// Elimina el contenido de un elemento
function elementEmpty(pElementId){
	$("#"+pElementId).empty();	
}

// Hace no visible a un elemento
function elementHide(pElementId, pElementClass = null){	
	if(pElementClass == null){
		$("#"+pElementId).css("display", "none");		
	}else{
		$("."+pElementClass).css("display", "none");
	}	
}

// Hace visible a un elemento
function elementShow(pElementId, pElementClass = null){	
	if(pElementClass == null){
		$("#"+pElementId).css("display", "");		
	}else{
		$("."+pElementClass).css("display", "");	
	}
	
}

// Agrega tooltip a un elemento
function elementTooltipOn(pElementId, pMessage){
	$("#"+pElementId).removeAttr('data-original-title');
	$("#"+pElementId).attr('data-original-title', pMessage);
	//$('#'+pElementId).tooltip('toggle');
	$('#'+pElementId).tooltip('show');	
}

// Elimina tooltip a un elemento
function elementTooltipOff(pElementId){		
	$("#"+pElementId).removeAttr('data-original-title');
	//$("#"+pElementId).tooltip('destroy');
	//$("#"+pElementId).tooltip('hide');
}

// Agrega 'tag' a un elemento
function elementTagShow(pElementId, pMessage, pPosition = null, pClassName = null, pStyle = null){
	var position = pPosition != null ? pPosition : 'right';
	$('#' + pElementId).w2tag(pMessage, {position: position, className: pClassName, style: pStyle});
}

// Elimina el 'tag' de un elemento
function elementTagHide(pElementId){
	$('#' + pElementId).w2tag();
}

// Agrega placeholder a un elemento 
function elementPlaceholder(pElementId, pMessage){
	elementAttrEvent(pElementId, 'placeholder', pMessage);
}

// Elimina placeholder de un elemento
function elementPlaceholderRemove(pElementId){
	elementAttrEventRemove(pElementId, 'placeholder');
}

// Agrega una propiedad y su valor a un elemento
function elementPropertyAdd(pElementId, pProperty, pValue){
	$('#'+pElementId).css(pProperty, pValue);
}

// Elimina una propiedad a un elemento
function elementPropertyRemove(pElementId, pProperty){
	$('#'+pElementId).prop('style').removeProperty(pProperty);
}

// Agrega conteido a un elemento
function elementAttr(pElementId, pHtml){	
	$('#'+pElementId).html(''+pHtml+'');	
}

//Elimina un evento de un elemento
function elementAttrRemove(pElementId, pProp){
	$('#'+pElementId).removeAttr(''+pProp+'');
}

// Agrega un evento a un elemento
function elementAttrEvent(pElementId, pEvent, pAction){	
	//@param pElementId : Id Elemento
	//@param pEvent : Evento a realizar. Ej: onclick, onchange...
	//@param pAction : Accion (function) a realizar.
	$('#'+pElementId).attr(''+pEvent+'',''+pAction+'');
}

// Elimina un evento de un elemento
function elementAttrEventRemove(pElementId, pEvent){
	$('#'+pElementId).removeAttr(''+pEvent+'');
}

function elementAttrEventExist(pElementId, pEvent){
	try{
		var propertyLength = $('#'+pElementId).attr(''+pEvent+'');
		return propertyLength.length > 0 ? true : false;
	}catch(err){
		return false;
	}	
}

// Mantiene el focus en un elemento
function elementFocus(pElementId){
	$("#"+pElementId).focus();	
}

// Quita el focus de un elemento
function elementFocusOut(pElementId){
	$("#"+pElementId).blur();	
}

function formFieldsClear(fields){
	if($.isArray(fields)){
		for (var i = 0; i < fields.length; i++) {
			var field = fields[i];
			!isSelect(field) ? elementFieldReady(field) : selectFieldReady(field);  
		}
	}else{
		if(fields != null){
			!isSelect(field) ? elementFieldReady(field) : selectFieldReady(field);
		}		
	}
}

function formFieldsChangeReady(fields){
	if($.isArray(fields)){
		for (var i = 0; i < fields.length; i++) {
			var field = fields[i];
			elementFieldClassReset(field);  
		}
	}else{
		if(fields != null){
			elementFieldClassReset(field);
		}		
	}
}

function formFieldsEnabled(fields){
	if($.isArray(fields)){
		for (var i = 0; i < fields.length; i++) {
			var field = fields[i];
			elementReadOnlyFalse(field);  
		}
	}else{
		if(fields != null){
			elementReadOnlyFalse(field);
		}		
	}
}

function formFieldsDisabled(fields){
	if($.isArray(fields)){
		for (var i = 0; i < fields.length; i++) {
			var field = fields[i];
			elementReadOnlyTrue(field);  
		}
	}else{
		if(fields != null){
			elementReadOnlyTrue(field);
		}		
	}
}

// Resetea las clases de un elemento en un formulario y elimina su valor
function elementFieldReady(pElementId){
	// @param
	// pElementId : id del elemento
	// field : nombre del elemento
	// label : nombre del label
	// div : nombre del div
	var field = pElementId;
	var label = "label-" + field;
	var div = "div-" + field;
	var span = "span-" + field;

	elementValueClear(field);	
	elementClear(span);
	requiredClassRemove(label);
	errorClassRemove(div);
	elementAttrEventRemove(field, 'placeholder');
	//elementTooltipOff(field);
	elementFocusOut(field);
}

// Resetea las clases de un elemento en un formulario
function elementFieldClassReset(pElementId){
	// @param		
	// pElementId : id del elemento
	// field : nombre del elemento
	// label : nombre del label
	// div : nombre del div
	var field = pElementId;
	var label = "label-" + field;
	var div = "div-" + field;
	var span = "span-" + field;

	requiredClassRemove(label);
	errorClassRemove(div);
	elementClear(span);
	elementPlaceholderRemove(pElementId);
	elementTagHide(field);
}

// Resetea la clase de un select en un formulario y setea su valor
function selectFieldReady(pElementId){
	// @param		
	// pElementId : id del select
	// field : nombre del elemento
	// label : nombre del label
	// div : nombre del div
	var field = pElementId;
	var label = "label-" + field;
	var div = "div-" + field;
	var span = "span-" + field;

	selectValueDefault(pElementId);
	requiredClassRemove(label);
	errorClassRemove(div);
	elementClear(span);
}

//Saber si un elemento es un select
function isSelect(pElementId){
	return selectLength(pElementId) >= 1 ? true : false;
}

// Saber si un select tiene valor
function isSelectHasValue(pElementId){
	if(!isElementExist(pElementId)){
		return false;
	}
	return (getElementValue(pElementId) != "-" && getElementValue(pElementId) != "") ? true : false;
}

// Muestra/Oculta select2 (plugin)
function select2ShowHide(content, action){
	if(action.equalsIgnoreCase('hide')){
		$('#'+content).children("span").attr("style", "display: none");
	}else{
		$('#'+content).children("span").removeAttr("style");
		$('#'+content).children("span").children("span").children("span").removeAttr("style");
	}
}

function select2AriaLabelledby(value, action, content){
	$("span").each(function(i){
		var labelledby = $(this).attr("aria-labelledby");
		if(labelledby != null){
			if(labelledby.equalsIgnoreCase(value)){
				if(action.equalsIgnoreCase('hide')){
					$(this).attr("style", "display: none");
				}else if(action.equalsIgnoreCase('show')){
					$(this).removeAttr("style");
				}			
			}
		}		
	});
}

// Devuelve el valor de la opcion seleccionada
function getSelectedOptionText(pElementId){
	return $("#"+pElementId+" option:selected" ).text();
}

//Setea un select en su valor por defecto
function selectValueDefault(pElementId){
	elementValueChange(pElementId, "-");	
}

// Setea un select en un valor determinado
function selectValueSet(pElementId, pVal){
	elementValueChange(pElementId, pVal);
}

// Devuelve el length (cantidad de option) de un select
function selectLength(pElementId){
	return $("select#"+pElementId+" option").length
}

// Carga dinamicamente la informacion de un combobox
function selectRendered(pIdSelect, pOptions){
	$("#"+pIdSelect).html(pOptions);
}

// Deshabilita un elemento
function elementDisabledOnly(pElementId){	
	$("#"+pElementId).prop("disabled", true);	
}

// Deshabilita un elemento y elimina su valor
function elementDisabledTrue(pElementId){	
	$("#"+pElementId).val("");
	$("#"+pElementId).prop("disabled", true);	
}

// Habilita un elemento
function elementDisabledFalse(pElementId){	
	$("#"+pElementId).prop("disabled", false);
}

// Hace un elemento de solo lectura y elimina el valor
function elementReadOnly(pElementId){
	$("#"+pElementId).val("");
	$("#"+pElementId).prop("readonly", true);	
}

// Hace un elemento de solo lectura y mantiene el valor
function elementReadOnlyTrue(pElementId){	
	$("#"+pElementId).prop("readonly", true);	
}

// Elimina la propiedad de solo lectura de un elemento
function elementReadOnlyFalse(pElementId){	
	$("#"+pElementId).prop("readonly", false);	
}

// Posicionamiento
function scrollToHere(pElementId){
	$('html,body').animate({
		scrollTop: $("#"+pElementId).offset().top
	}, 1000);	
}

function pageScrollTop(){
	scrollToHere('application-page-content');
}

// Codifica un string
function encodeValue(pString){
	if(pString == null){
		return "";
	}
	return Base64.encode(String(pString));
}

// Decodifica un string
function decodeValue(pString){
	if(pString == null){
		return "";
	}
	return Base64.decode(pString);
}

//Establece el numero minimo (Integer) en un input
function elementSetMinDefault(pElementId){
	$("#"+pElementId).attr('min', 0);
}

//Establece el numero minimo (Integer) en un input
function elementSetMin(pElementId, pValue){
	var value = parseInt(pValue);
	$("#"+pElementId).prop('min', value);
}

// Establece el numero maximo (Integer) en un input
function elementSetMax(pElementId, pValue){
	var value = parseInt(pValue);
	$("#"+pElementId).attr('max', value);
}

// Cambia el valor de un checkbox a true (checked = true)
function elementCheckedTrue(pElementId){
	$("#"+pElementId).prop('checked', true);	
}

// Cambia el valor de un checkbox a false (checked = false)
function elementCheckedFalse(pElementId){
	$("#"+pElementId).prop('checked', false);	
}

// Devuelve el estado de un checkbox (checked)
function isElementChecked(pElementId){
	return $("#"+pElementId).prop("checked");	
}

// Cambia la clase de un checkbox
function elementChecked(pElementId){
	elementCheckedTrue(pElementId);
}

// Resetea el valor (checked = false) de un checkbox y resetea las clases
function elementUnchecked(pElementId){
	elementCheckedFalse(pElementId);
}

// Devuelve si un valor se encuentra dentro de arreglo
function isElementInArray(pArray, pValue){
	if($.isArray(pArray)){
		var indexElement = $.inArray(pValue, pArray);
		return indexElement >= 0 ? true : false;
	}
}

//Devuelve la posicion de un elemento si se encuentra dentro de arreglo
function elementPosInArray(pArray, pValue){
	if($.isArray(pArray)){
		var indexElement = $.inArray(pValue, pArray);
		return indexElement >= 0 ? indexElement : null;
	}
}

//Elimina un elemento dentro de un arreglo y devuelve el nuevo arreglo
function elementInArrayRemove(pArray, pElement){
	//$.isArray(array) returns a Boolean indicating whether the object 
	//is a JavaScript array (not an array-like object, such as a jQuery object).

	//$.inArray(value, array) is a jQuery function which finds the first index of a value in an array

	//The above returns -1 if the value is not found, so check that i is a valid index before 
	//we do the removal. Removing index -1 means removing the last, which isn't helpful here.

	//.splice(index, count) removes count number of values starting at index, so we just want a count of 1		
	if($.isArray(pArray)){
		var indexElement = $.inArray(pElement, pArray);
		if(indexElement >= 0)
			pArray.splice(indexElement, 1);
		return pArray;	
	}
	return pArray;
}

// Devuelve si un arreglo está vacio
function isArrayEmpty(pArray){
	if($.isArray(pArray)){
		return pArray.length == 0;
	}
	return true;
}

// Elimina un elemento por su posicion dentro de un arregloy devuelve el nuevo arreglo
function elementIndexInArrayRemove(pArray, pIndex){
	if($.isArray(pArray)){
		if(pIndex >= 0 && pIndex < pArray.length){
			pArray.splice(pIndex, 1);
		}
		return pArray;	
	}
	return pArray;
}

function encriptionArrayData(pArray){
	if($.isArray(pArray)){
		for (var i = 0; i < pArray.length; i++) {
			pArray[i] = encodeValue(pArray[i]);
		}
		return pArray;
	}
}

function activateTab(pTabs, pTab){
	if($.isArray(pTabs)){
		var li = null;
		
		for (var i = 0; i < pTabs.length; i++) {
			var tab = pTabs[i];
			li = 'li-' + tab;
			elementClassRemove(tab, 'active');
			elementClassRemove(li, 'active');
		}
		
		li = 'li-' + pTab;
		elementClassAdd(pTab, 'active');
		elementClassAdd(li, 'active');
	}
}

function separatorShow(fields, pId){
	var hasError = false;
	if($.isArray(fields)){
		for (var i = 0; i < fields.length; i++) {
			var item = fields[i];
			var elementId = 'div-' + item;
			if(isElementHasClass(elementId, 'has-error')){
				hasError = true;
				break;
			}
		}
		if(hasError){
			elementClassAdd(pId, 'form-group-separator col-xs-12');
		}
	}else{
		var elementId = 'div-' + fields;
		if(isElementHasClass(elementId, 'has-error')){
			elementClassAdd(pId, 'form-group-separator col-xs-12');
		}
	}
}

function separatorHide(fields, pId){
	var hasError = false;
	if($.isArray(fields)){
		for (var i = 0; i < fields.length; i++) {
			var item = fields[i];
			var elementId = 'div-' + item;
			if(isElementHasClass(elementId, 'has-error')){
				hasError = true;
				break;
			}
		}
		if(!hasError){
			elementClassRemove(pId, 'form-group-separator col-xs-12');
		}
	}else{
		elementClassRemove(pId, 'form-group-separator col-xs-12');
	}
}

function inputSpinInstance(pClass, pMin, pMax, pStep){
	$(pClass).TouchSpin({
		min: pMin,
        max: pMax,
        step: pStep,
		verticalbuttons: true,
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });
}

function inputSpinUpdate(pClass, pMin, pMax, pStep){
	$(pClass).trigger("touchspin.updatesettings", {
		min: pMin,
        max: pMax,
        step: pStep
    });
}



/* Clases CSS */

// Devuelve la clase que tiene un elemento
function getElementClass(pElementId){
	return $('#'+pElementId).attr('class');
}

// Devuelve si un elemento tiene una clase
function isElementHasClass(pElementId, pClass){
	return $('#'+pElementId).hasClass(pClass);	
}

// Agrega una clase a un elemento
function elementClassAdd(pElementId, pClass){
	if(!isElementHasClass(pElementId, pClass))
		$('#'+pElementId).addClass(pClass);
}

// Elimina una clase de un elemento
function elementClassRemove(pElementId, pClass){
	if(isElementHasClass(pElementId, pClass))	
		$("#"+pElementId).removeClass(pClass);
}

// Cambia una clase por otra en un elemento
function elementClassChange(pElementId, pClass, pNewClass){	
	if(isElementHasClass(pElementId, pClass))
		elementClassRemove(pElementId, pClass);
	elementClassAdd(pElementId, pNewClass);
}

// Elimina todas las clases de un elemento
function elementClassRemoveAll(pElementId){
	$("#"+pElementId).removeClass();	
}

// Agrega clase de error a un elemento
function errorClass(pElementId){	
	elementClassAdd(pElementId, 'has-error');
}

// Elimina la clase de error a un elemento
function errorClassRemove(pElementId){	
	elementClassRemove(pElementId, 'has-error');		
}

// Agrega clase required a un elemento
function requiredClass(pElementId){	
	elementClassAdd(pElementId, 'required');	
}

//Elimina la clase required a un elemento
function requiredClassRemove(pElementId){	
	elementClassRemove(pElementId, 'required');		
}

// Agrega clase hidden a un elemento
function hiddenClass(pElementId){	
	elementClassAdd(pElementId, 'hidden');
}

// Elimina la clase hidden a un elemento
function hiddenClassRemove(pElementId){	
	elementClassRemove(pElementId, 'hidden');		
}

const availableTypeahead = (pField, pName, pDataSets) => {
	$(`#${pField}`).typeahead({
	    name: pName,
	    source: pDataSets
	});
}

function resetTypeahead(pField) {
	$(`#${pField}`).typeahead('destroy');
}
