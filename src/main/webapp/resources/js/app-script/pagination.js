function pagination(pId, pPage, pPaginator){
	var idDiv = "div-" + pId;
	var idUl = "ul-" + pId;
	
	//##### Inicio
	var idLiBg = "li-bg-" + pId;
	
	//##### Anterior
	var idLiBf = "li-bf-" + pId;
	
	//##### Paginas
	var idLiPg = "li-pg-" + pId; 
	
	//##### Siguiente
	var idLiNx = "li-nx-" + pId; 
	
	//##### Fin
	var idLiLs = "li-ls-" + pId; 
	
	var page = pPage;
	var paginator = pPaginator;
	var items = pPaginator.items;
	
	var liBgClass = (page.first) ? 'disabled' : '';
	var liBgPage = encodeValue("1");
	var liBgContent = (page.first) ? '<span>← Inicio</span>' : '<a onclick=paginate("'+liBgPage+'") title="Inicio">← Inicio</a>'; 
	
	var liBfClass = (!page.hasPrevious) ? 'disabled' : '';
	var previous = (page.hasPrevious) ? parseInt(page.number) : '';
	var liBfPage = encodeValue(previous);
	var liBfContent = (!page.hasPrevious) ? '<span>«</span>' : '<a onclick=paginate("'+liBfPage+'") title="Anterior">«</a>';
	
	var liNxClass = !page.hasNext ? 'disabled' : '';
	var next = page.hasNext ? parseInt(page.number) + parseInt(2) : '';
	var liNxPage = encodeValue(next);
	var liNxContent = (!page.hasNext) ? '<span>»</span>' : '<a onclick=paginate("'+liNxPage+'") title="Siguiente">»</a>';
	
	var liLsClass = page.last ? 'disabled' : '';
	var liLsPage = encodeValue(page.totalPages);
	var liLsContent = (page.last) ? '<span>Fin →</span>' : '<a onclick=paginate("'+liLsPage+'") title="Fin">Fin →</a>';
	
	$('#' + idDiv).html("");
	if(page.hasContent){
		$('#' + idDiv).append(
			'<ul id="'+idUl+'" class="pagination"> ' +
				//##### Inicio			
				'<li id="'+idLiBg+'" class="'+liBgClass+'">'+liBgContent+'</li> ' +
				//##### Anterior
				'<li id="'+idLiBf+'" class="'+liBfClass+'">'+liBfContent+'</li> ' +
			'</ul> '
		);
		
		//##### Paginas
		for (var i = 0; i < items.length; i++) {
			
			var item = items[i];
			var pageId = idLiPg + "-" + i;
			var pageClass = item.current ? 'active' : '';
			var pageNumber = encodeValue(item.number);
			var pageContent = (item.current) ? '<span>'+item.number+'</span>' : '<a onclick=paginate("'+pageNumber+'")><span>'+item.number+'</span></a>'; 
				
			$('#' + idUl).append(
				'<li id="'+pageId+'" class="'+pageClass+'">'+pageContent+'</li>'
			);
		}
		
		$('#' + idUl).append(
			//##### Siguiente
			'<li id="'+idLiNx+'" class="'+liNxClass+'">'+liNxContent+'</li> ' +
			//##### Fin
			'<li id="'+idLiLs+'" class="'+liLsClass+'">'+liLsContent+'</li>'
		);
	}
}

function pagination1(pId, pPage, pPaginator){
	var idDiv = "div-" + pId;
	var idUl = "ul-" + pId;
	
	//##### Inicio
	var idLiBg = "li-bg-" + pId;
	
	//##### Anterior
	var idLiBf = "li-bf-" + pId;
	
	//##### Paginas
	var idLiPg = "li-pg-" + pId; 
	
	//##### Siguiente
	var idLiNx = "li-nx-" + pId; 
	
	//##### Fin
	var idLiLs = "li-ls-" + pId; 
	
	var page = pPage;
	var paginator = pPaginator;
	var items = pPaginator.items;
	
	var liBgClass = (page.first) ? 'disabled' : '';
	var liBgPage = encodeValue("1");
	var liBgContent = (page.first) ? '<span>← Inicio</span>' : '<a onclick=paginate1("'+liBgPage+'") title="Inicio">← Inicio</a>'; 
	
	var liBfClass = (!page.hasPrevious) ? 'disabled' : '';
	var previous = (page.hasPrevious) ? parseInt(page.number) : '';
	var liBfPage = encodeValue(previous);
	var liBfContent = (!page.hasPrevious) ? '<span>«</span>' : '<a onclick=paginate1("'+liBfPage+'") title="Anterior">«</a>';
	
	var liNxClass = !page.hasNext ? 'disabled' : '';
	var next = page.hasNext ? parseInt(page.number) + parseInt(2) : '';
	var liNxPage = encodeValue(next);
	var liNxContent = (!page.hasNext) ? '<span>»</span>' : '<a onclick=paginate1("'+liNxPage+'") title="Siguiente">»</a>';
	
	var liLsClass = page.last ? 'disabled' : '';
	var liLsPage = encodeValue(page.totalPages);
	var liLsContent = (page.last) ? '<span>Fin →</span>' : '<a onclick=paginate1("'+liLsPage+'") title="Fin">Fin →</a>';
	
	$('#' + idDiv).html("");
	if(page.hasContent){
		$('#' + idDiv).append(
			'<ul id="'+idUl+'" class="pagination"> ' +
				//##### Inicio			
				'<li id="'+idLiBg+'" class="'+liBgClass+'">'+liBgContent+'</li> ' +
				//##### Anterior
				'<li id="'+idLiBf+'" class="'+liBfClass+'">'+liBfContent+'</li> ' +
			'</ul> '
		);
		
		//##### Paginas
		for (var i = 0; i < items.length; i++) {
			
			var item = items[i];
			var pageId = idLiPg + "-" + i;
			var pageClass = item.current ? 'active' : '';
			var pageNumber = encodeValue(item.number);
			var pageContent = (item.current) ? '<span>'+item.number+'</span>' : '<a onclick=paginate1("'+pageNumber+'")><span>'+item.number+'</span></a>'; 
				
			$('#' + idUl).append(
				'<li id="'+pageId+'" class="'+pageClass+'">'+pageContent+'</li>'
			);
		}
		
		$('#' + idUl).append(
			//##### Siguiente
			'<li id="'+idLiNx+'" class="'+liNxClass+'">'+liNxContent+'</li> ' +
			//##### Fin
			'<li id="'+idLiLs+'" class="'+liLsClass+'">'+liLsContent+'</li>'
		);
	}
}