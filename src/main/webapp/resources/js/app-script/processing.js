// Modalspanel que indica que la sesion ha expirado
function sessionExpiredByInactivity(){
	$.blockUI({
		message:'<div id="modalspanel-session-expired" class="modalspanel-default modalspanel-session-expired"> ' +
					'<div id="modalspanel-session-expired-body" class="panel-default-body-content"> ' +
						'<div id="modalspanel-session-expired-content" class="modalspanel-session-expired-content"> ' +
							'<p id="modalspanel-session-expired-text" style="font-weight: bold"> ' +
								'Su sesión ha expirado por inactividad. Por favor, vuelva a acceder al sistema para continuar. ' +
							'</p> ' +
						'</div> ' +
					'</div> ' +
					'<div id="modalspanel-session-expired-button-content" class="modalspanel-session-expired-buttons-content"> ' +
						'<div class="modalspanel-session-expired-buttons"> ' +
							'<button id="modalspanel-session-expired-button-action" class="btn-action" type="button" style="">Aceptar</button> ' +
						'</div> ' +
					'</div> ' +
				'</div>',
		css:{
			border: '0px',
			left: '40%',
			'background-color': '#333'
		},
		overlayCSS:  {
			'background-color': '#aaa'
		}
	});
	elementAttrEvent('modalspanel-session-expired-button-action', 'onclick', 'logout()');
}

/*'<img id="img-aplication-block" src="../../../resources/src/loading/loading_x_3.gif" /> ' +*/

/*'<div class="sk-spinner sk-spinner-wave" style="float: left"> ' +
                        	'<div class="sk-rect1"></div> ' +
                        	'<div class="sk-rect2"></div> ' +
                        	'<div class="sk-rect3"></div> ' +
                        	'<div class="sk-rect4"></div> ' +
                        	'<div class="sk-rect5"></div> ' +
                        '</div> ' +*/

// Procesando
function processing(pText){
	
	// Ocultando los mensajes globales
	globalMessageHide();
	
	$.blockUI({
		message:'<div id="div-aplication-block" class="block-div"> ' +
					'<div id="div-block" class="block-content"> ' +
                        '<div class="sk-spinner sk-spinner-fading-circle" style="float: left"> ' +
	                        '<div class="sk-circle1 sk-circle"></div> ' +
	                        '<div class="sk-circle2 sk-circle"></div> ' +
	                        '<div class="sk-circle3 sk-circle"></div> ' +
	                        '<div class="sk-circle4 sk-circle"></div> ' +
	                        '<div class="sk-circle5 sk-circle"></div> ' +
	                        '<div class="sk-circle6 sk-circle"></div> ' +
	                        '<div class="sk-circle7 sk-circle"></div> ' +
	                        '<div class="sk-circle8 sk-circle"></div> ' +
	                        '<div class="sk-circle9 sk-circle"></div> ' +
	                        '<div class="sk-circle10 sk-circle"></div> ' +
	                        '<div class="sk-circle11 sk-circle"></div> ' +
	                        '<div class="sk-circle12 sk-circle"></div> ' +
	                        '<div class="sk-circle13 sk-circle"></div> ' +
	                        '<div class="sk-circle14 sk-circle"></div> ' +
	                        '<div class="sk-circle15 sk-circle"></div> ' +
	                    '</div> ' +
                        '<div style="float: left"> ' +
							'<span id="span-text-aplication-block" class="block-text">'+pText+'</span> '+
						'</div> ' +
					'</div> ' +
				'</div>',
		css: {
			border: '0px',
			'border-radius': '0.3em',
			'box-shadow': '0 0 5px #000',
			left: '40%',
			width: 'auto !important'
		},
		overlayCSS:  {
			'background-color': '#aaa'
		}
	});
}

function block(){
	$.blockUI({
		message: null
	});	
}

function blockElement(pElementId){
	$("#"+pElementId).block({
		message: null
	});	
}

function blank(){
	elementHide('div-aplication-block');
}

function processingEnd(){
	$.unblockUI();
}

function processingTextChange(pText){
	elementTextChange('span-text-aplication-block', pText);
}