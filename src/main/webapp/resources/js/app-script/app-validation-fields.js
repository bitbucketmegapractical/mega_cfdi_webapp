var messageRequired = 'El valor de este campo es requerido.';
function appValidationSetRequiredMessage(pMessage){
	messageRequired = pMessage;
}

function fieldsCheck(fields, placeholder = false, content = null){
	if($.isArray(fields)){
		var hasFieldNullOrEmpty = false;
		var pos = null;
		for (var i = 0; i < fields.length; i++) {
			var field = fields[i];
			var div = "div-" + field;
			var label = "label-" + field;
			var span = "span-" + field;
			
			var bValid = isElementHasValue(field);  
			if(!bValid){
				errorClass(div);
				requiredClass(label);
				// Mostrando error en span o agregando placeholder al elemento requerido
				!placeholder ? elementTextChange(span, messageRequired) : elementPlaceholder(field, messageRequired);
				hasFieldNullOrEmpty = true;
				if(pos == null){
					pos = i;
				}
			}else{
				elementFieldClassReset(field);		
			}
		}
		if(hasFieldNullOrEmpty){
			var fieldFocus = fields[pos];
			var divScroll = "div-" + fields[pos];
			elementFocus(fieldFocus);
			if(isElementExist(divScroll)){
				scrollToHere(divScroll);
			}
			return false;
		}
		return true;
	}else{
		if(fields != null){
			return !isSelect(fields) ? fieldRequired(fields, placeholder, content) : selectRequired(fields, placeholder, content);
		}		
	}
}

// Valida valor de un campo requerido (No <select>)
function fieldRequired(pElementId, placeholder = false, content = null){
	// @param		
		// pElementId : id del elemento
	// Nombre del elemento
	var field = pElementId;
	// Nombre del label
	var label = "label-" + field;
	// Nombre del div
	var div = "div-" + field;
	if(content != null){
		div = content;
	}
	// Nombre del span
	var span = "span-" + field;
	// Verificando valor
	if(!isElementHasValue(pElementId)){		
		// Agregando la clase 'required' al label
		requiredClass(label);
		// Agregando clase 'has-error' al div
		errorClass(div);
		// Mostrando error en span o agregando placeholder al elemento requerido
		!placeholder ? elementTextChange(span, messageRequired) : elementPlaceholder(field, messageRequired);
		// Mantener el foco en el elemento requerido
		elementFocus(field);
		// Moverse hacia el contenedor del elemento
		scrollToHere(div);
		return false;
	}else{
		requiredClassRemove(label);
		errorClassRemove(div);
		elementPlaceholderRemove(field);
		elementFocusOut(field);
		return true;
	}
}

//Valida valor de un combobox requerido (<select>)
function selectRequired(pElementId, placeholder = false, content = null){
	// @param		
		// pElementId : id del elemento
	// Nombre del elemento
	var field = pElementId;
	// Nombre del label
	var label = "label-" + field;
	// Nombre del div
	var div = "div-" + field;
	if(content != null){
		div = content;
	}
	// Nombre del span
	var span = "span-" + field;
	// Verificando valor
	if(!isElementHasValue(pElementId)){		
		// Agregando la clase 'required' al label
		requiredClass(label);
		// Agregando clase 'has-error' al div
		errorClass(div);
		// Mostrando error en span o agregando placeholder al elemento requerido
		!placeholder ? elementTextChange(span, messageRequired) : elementPlaceholder(field, messageRequired);
		// Mantenemos el foco en el elemento requerido
		elementFocus(field);
		// Nos movemos hacia el contenedor del elemento
		scrollToHere(div);
		return false;
	}else{
		requiredClassRemove(label);
		errorClassRemove(div);
		elementPlaceholderRemove(field);
		elementFocusOut(field);
		return true;
	}
}

function elementForceError(pElementId, pMessage = null){
	// @param		
		// pElementId : id del elemento
	// Nombre del elemento
	var field = pElementId;
	// Nombre del label
	var label = "label-" + field;
	// Nombre del div
	var div = "div-" + field;
	// Nombre del span
	var span = "span-" + field;
	// Agregando la clase 'required' al label
	requiredClass(label);
	// Agregando clase 'has-error' al div
	errorClass(div);
	// Agregando la descripcion del error
	if(pMessage != null){
		elementTextChange(span, pMessage);
	}	
}

function select2ClearError(pElementId){
	var field = pElementId;
	var div = "div-" + field; 
	$('#'+div).children("span").children("span").children("span").removeAttr("style");
	elementFieldClassReset(field);
}

function select2ForceError(pDiv){
	$('#'+pDiv).children("span").children("span").children("span").attr("style", "border-color: #ed5565 !important;");
}

function select2ForceError(pDiv, pLabel, pSpan, pMessage){
	
	$('#'+pDiv).children("span").children("span").children("span").attr("style", "border-color: #ed5565 !important;");

	// Nombre del label
	var label = pLabel;
	// Nombre del div
	var div = pDiv;
	// Nombre del span
	var span = pSpan;
	// Agregando la clase 'required' al label
	requiredClass(label);
	// Agregando clase 'has-error' al div
	errorClass(div);
	// Agregando la descripcion del error
	elementTextChange(span, pMessage);
	// Nos movemos hacia el contenedor del elemento
	scrollToHere(div);
}

/**
 * Sets select2 value
 * @param pElementId - Selector id
 * @param value - Value to set
 */
function select2SetVal(pElementId, value) {
    if (value != null && value != '') {
        $(`#${pElementId}`).val(value).trigger('change');
    } else {
        select2Clean(pElementId);
    }
}

/* Select2 Limpia valor y pone valor por defecto */
function select2DefaultVal(pElementId) {
	 $(`#${pElementId}`).val('-').trigger('change');
}

/**
 * Trigger change event on select2 and delete value
 * @param pElementId - Selector id
 */
function select2Clean(pElementId) {
    $(`#${pElementId}`).val(null).trigger('change');
}

function elementTagError(pElementId, pTagMessage, pPosition = null, pClassName = null, pStyle = null){
	// @param		
		// pElementId : id del elemento
	// Nombre del elemento
	var field = pElementId;
	// Nombre del label
	var label = "label-" + field;
	// Nombre del div
	var div = "div-" + field;
	// Nombre del span
	var span = "span-" + field;
	// Agregando la clase 'required' al label
	requiredClass(label);
	// Agregando clase 'has-error' al div
	errorClass(div);
	// Agregamos tag
	elementTagShow(field, pTagMessage, pPosition);
}

function elementTooltipError(pElementId, pTooltipMessage, pClassName, pStyle){
	// @param		
		// pElementId : id del elemento
	// Nombre del elemento
	var field = pElementId;
	// Nombre del label
	var label = "label-" + field;
	// Nombre del div
	var div = "div-" + field;
	// Nombre del span
	var span = "span-" + field;
	// Agregando la clase 'required' al label
	requiredClass(label);
	// Agregando clase 'has-error' al div
	errorClass(div);
	// Agregamos tooltip
	elementTooltipOn(field, pTooltipMessage);
}

function elementPlaceholderError(pElementId, placeholderMessage){
	// @param		
		// pElementId : id del elemento
	// Nombre del elemento
	var field = pElementId;
	// Nombre del label
	var label = "label-" + field;
	// Nombre del div
	var div = "div-" + field;
	// Nombre del span
	var span = "span-" + field;
	// Agregando la clase 'required' al label
	requiredClass(label);
	// Agregando clase 'has-error' al div
	errorClass(div);
	// Agregando placeholder al elemento requerido
	elementAttrEvent(field, 'placeholder', placeholderMessage);
}

var nameOrBusinessNameRegex = /^[^|]{1,254}$/;
var rfcRegex = /^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$/;
var certificateNumberRegex = /^[0-9]{20}$/;
var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var serieRegex = /^[^|]{1,25}$/;
var folioRegex = /^[^|]{1,40}$/;
var paymentConditionsRegex = /^[^|]{1,1000}$/;
var confirmationCodeRegex = /^[0-9a-zA-Z]{5}$/;
var uuidRegex = /^[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}$/;

var conceptDescription = /^[^|]{1,1000}$/;
var conceptIdentificationNumberRegex = /^[^|]{1,100}$/;
var conceptUnitRegex = /^[^|]{1,20}$/;
var conceptCustomsInformationNumberRegex = /^[0-9]{2}  [0-9]{2}  [0-9]{4}  [0-9]{7}$/;
var conceptPropertyAccountNumberRegex = /^[0-9]{1,150}$/;

var passwdRegex = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S[^|}]+$).{8,}$/;

function validateExpression(pField){
	var value = getElementValue(pField);
	switch(pField) {
		case 'rfc' :
			return value.search(rfcRegex);
		case 'emitter-name', 'name-business-name', 'name':
			return value.search(nameOrBusinessNameRegex);
		case 'email' :
			return value.search(emailRegex);
		case 'serie':
			return value.search(serieRegex);
		case 'folio':
			return value.search(folioRegex);
		case 'payment-conditions':
			return value.search(paymentConditionsRegex);
		case 'cfdi-related-uuid', 'cmp-payment-related-document-id':
			return value.search(uuidRegex);
		case 'description':
			return value.search(conceptDescription);
		case 'identification-number':
			return value.search(conceptIdentificationNumberRegex);
		case 'unit':
			return value.search(conceptUnitRegex);
		case 'customs-information-number':
    		return value.search(conceptCustomsInformationNumberRegex);
		case 'property-account-number':
    		return value.search(conceptPropertyAccountNumberRegex);
		case 'passwd':
    		return value.search(passwdRegex);
    	default:
    		return false;
	}
}

function validateLength(pElementId, pValue){
	return getElementLength(pElementId) <= pValue ? true : false;
}

function validateMinLength(pElementId, pValue){
	return getElementLength(pElementId) >= pValue ? true : false;
}

function validateMinMaxLength(pElementId, pValueMin, pValueMax){
	return (validateMinLength(pElementId, pValueMin) && validateLength(pElementId, pValueMax)) ? true : false;
}

function validateEmailFormat(pEmail){	
	if (emailRegex.test(getElementValue(pEmail))) {
	    return true;
	} else {
		return false;
	}
}