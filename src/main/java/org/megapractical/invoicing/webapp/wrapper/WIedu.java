package org.megapractical.invoicing.webapp.wrapper;

public class WIedu {

	private String version;
    private String studentName;
    private String curp;
    private String educationLevel;
    private String autRVOE;
    private String rfc;

    /*Getters and Setters*/
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String getAutRVOE() {
		return autRVOE;
	}

	public void setAutRVOE(String autRVOE) {
		this.autRVOE = autRVOE;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	
}