package org.megapractical.invoicing.webapp.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.CuentaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;

public class WAccount {

	private ContribuyenteEntity taxpayer;
	private String action;
	private Boolean accountDeletedFinded;
	
	// Contribuyente (Asociado)
	ContribuyenteEntity taxpayerAsociated;
	
	// Contribuyente - Regimen Fiscal
	ContribuyenteRegimenFiscalEntity taxpayerTaxRegime;
	
	// Contribuyente - Tipo persona
	ContribuyenteTipoPersonaEntity taxpayerPersonType;
	
	// Contribuyente - Certificado
	ContribuyenteCertificadoEntity taxpayerCertificate;
	
	// Contribuyente - Cuenta
	CuentaEntity taxpayerAccount;
	
	// Contribuyente - Preferencias
	PreferenciasEntity preferences;
	
	/*Getters and Setters*/
	public ContribuyenteEntity getTaxpayer() {
		return taxpayer;
	}

	public void setTaxpayer(ContribuyenteEntity taxpayer) {
		this.taxpayer = taxpayer;
	}

	public ContribuyenteEntity getTaxpayerAsociated() {
		return taxpayerAsociated;
	}

	public void setTaxpayerAsociated(ContribuyenteEntity taxpayerAsociated) {
		this.taxpayerAsociated = taxpayerAsociated;
	}

	public ContribuyenteRegimenFiscalEntity getTaxpayerTaxRegime() {
		return taxpayerTaxRegime;
	}

	public void setTaxpayerTaxRegime(ContribuyenteRegimenFiscalEntity taxpayerTaxRegime) {
		this.taxpayerTaxRegime = taxpayerTaxRegime;
	}

	public ContribuyenteTipoPersonaEntity getTaxpayerPersonType() {
		return taxpayerPersonType;
	}

	public void setTaxpayerPersonType(ContribuyenteTipoPersonaEntity taxpayerPersonType) {
		this.taxpayerPersonType = taxpayerPersonType;
	}

	public CuentaEntity getTaxpayerAccount() {
		return taxpayerAccount;
	}

	public void setTaxpayerAccount(CuentaEntity taxpayerAccount) {
		this.taxpayerAccount = taxpayerAccount;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Boolean getAccountDeletedFinded() {
		return accountDeletedFinded;
	}

	public void setAccountDeletedFinded(Boolean accountDeletedFinded) {
		this.accountDeletedFinded = accountDeletedFinded;
	}

	public ContribuyenteCertificadoEntity getTaxpayerCertificate() {
		return taxpayerCertificate;
	}

	public void setTaxpayerCertificate(ContribuyenteCertificadoEntity taxpayerCertificate) {
		this.taxpayerCertificate = taxpayerCertificate;
	}

	public PreferenciasEntity getPreferences() {
		return preferences;
	}

	public void setPreferences(PreferenciasEntity preferences) {
		this.preferences = preferences;
	}	
}