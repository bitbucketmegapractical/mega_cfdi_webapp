package org.megapractical.invoicing.webapp.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.PrefacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;

public class WPreInvoiceNotification {

	private String emitterName;
	private String emitterEmail;
	private String emitterRfc;
	
	private String receiverName;
	private String receiverEmail;
	private String receiverRfc;
	
	private PrefacturaEntity preInvoice;
	private String pdfPath;
	private String pdfName;
	
	private PreferenciasEntity preferences;

	/*Getters and Setters*/
	public String getEmitterName() {
		return emitterName;
	}

	public void setEmitterName(String emitterName) {
		this.emitterName = emitterName;
	}

	public String getEmitterEmail() {
		return emitterEmail;
	}

	public void setEmitterEmail(String emitterEmail) {
		this.emitterEmail = emitterEmail;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public PreferenciasEntity getPreferences() {
		return preferences;
	}

	public void setPreferences(PreferenciasEntity preferences) {
		this.preferences = preferences;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public String getEmitterRfc() {
		return emitterRfc;
	}

	public void setEmitterRfc(String emitterRfc) {
		this.emitterRfc = emitterRfc;
	}

	public String getReceiverRfc() {
		return receiverRfc;
	}

	public void setReceiverRfc(String receiverRfc) {
		this.receiverRfc = receiverRfc;
	}

	public PrefacturaEntity getPreInvoice() {
		return preInvoice;
	}

	public void setPreInvoice(PrefacturaEntity preInvoice) {
		this.preInvoice = preInvoice;
	}

	public String getPdfName() {
		return pdfName;
	}

	public void setPdfName(String pdfName) {
		this.pdfName = pdfName;
	}	
}