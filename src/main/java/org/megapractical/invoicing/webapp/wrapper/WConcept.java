package org.megapractical.invoicing.webapp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteConceptoEntity;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WConcept {
	// @Nullable
	private String identificationNumber;
	// @Required
	private String description;
	// @Required
	private String quantity;
	// @Required
	private String measurementUnit;
	// @Nullable
	private String unit;
	// @Required
	private String unitValue;
	// @Required
	private String amount;
	// @Nullable
	private String discount;
	// @Required
	private String keyProductService;
	// @Required
	private String objImp;
	// @Nullable
	@Getter(AccessLevel.NONE)
    private List<WTaxes> taxesTransferred;
    // @Nullable
	@Getter(AccessLevel.NONE)
    private List<WTaxes> taxesWithheld;
    // @Nullable
	private List<WCustomsInformation> customsInformation;
	// @Nullable
	private WPropertyAccount propertyAccount;
	// @Nullable
	private WIedu iedu;
	// @Nullable
	private List<Comprobante.Conceptos.Concepto.Parte> parts;
	
	private ContribuyenteConceptoEntity taxpayerConcept;
	private String action;
	private Boolean conceptDeletedFinded;
	private String conceptSelected;
	private String taxType;
		
	/*Getters and Setters*/
	public List<WTaxes> getTaxesTransferred() {
		if (taxesTransferred == null) {
			taxesTransferred = new ArrayList<>();
		}
		return taxesTransferred;
	}

	public List<WTaxes> getTaxesWithheld() {
		if (taxesWithheld == null) {
			taxesWithheld = new ArrayList<>();
		}
		return taxesWithheld;
	}
	
}