package org.megapractical.invoicing.webapp.wrapper;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContactoEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WCfdiNotification {
	private String emitterName;
	private String emitterEmail;
	private String emitterRfc;
	
	private String receiverName;
	private String receiverEmail;
	private String receiverRfc;
	
	private CfdiEntity cfdi;
	private NominaEntity payroll;
	private RetencionEntity retention;
	private String xmlPath;
	private String pdfPath;
	private String uuid;
	private String xmlName;
	private String pdfName;
	
	@Getter(AccessLevel.NONE)
	private List<ContactoEntity> contacts;
	private PreferenciasEntity preferences;
	
	InputStream xmlInputStream;
	InputStream pdfInputStream;
	@Getter(AccessLevel.NONE)
	private List<String> recipients;

	/* Getters */
	public List<ContactoEntity> getContacts() {
		if(contacts == null)
			contacts = new ArrayList<>();
		return contacts;
	}

	public List<String> getRecipients() {
		if(recipients == null)
			recipients = new ArrayList<>();
		return recipients;
	}
	
}