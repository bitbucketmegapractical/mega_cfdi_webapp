package org.megapractical.invoicing.webapp.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;

public class WSerieSheet {

	private ContribuyenteSerieFolioEntity serieSheet;
	private String action;
	private Boolean serieSheetDeletedFinded;

	public ContribuyenteSerieFolioEntity getSerieSheet() {
		return serieSheet;
	}

	public void setSerieSheet(ContribuyenteSerieFolioEntity serieSheet) {
		this.serieSheet = serieSheet;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Boolean getSerieSheetDeletedFinded() {
		return serieSheetDeletedFinded;
	}

	public void setSerieSheetDeletedFinded(Boolean serieSheetDeletedFinded) {
		this.serieSheetDeletedFinded = serieSheetDeletedFinded;
	}
}