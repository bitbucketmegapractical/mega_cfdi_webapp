package org.megapractical.invoicing.webapp.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;

public class WStampControl {

	private ContribuyenteEntity taxpayer;
	private boolean isBuy;
	private boolean isTransfer;
	private boolean isStamp;
	
	/*Getters and Setters*/
	public ContribuyenteEntity getTaxpayer() {
		return taxpayer;
	}
	
	public void setTaxpayer(ContribuyenteEntity taxpayer) {
		this.taxpayer = taxpayer;
	}
	
	public boolean isBuy() {
		return isBuy;
	}
	
	public void setBuy(boolean isBuy) {
		this.isBuy = isBuy;
	}
	
	public boolean isTransfer() {
		return isTransfer;
	}
	
	public void setTransfer(boolean isTransfer) {
		this.isTransfer = isTransfer;
	}
	
	public boolean isStamp() {
		return isStamp;
	}
	
	public void setStamp(boolean isStamp) {
		this.isStamp = isStamp;
	}	
}
