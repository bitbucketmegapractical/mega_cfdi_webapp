package org.megapractical.invoicing.webapp.wrapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.voucher.catalogs.CFormaPago;
import org.megapractical.invoicing.voucher.catalogs.CMoneda;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WComplementPayment {
	private WPaymentTotal paymentTotal;
	@Getter(AccessLevel.NONE)
	private List<WPayment> payments;
	
	public List<WPayment> getPayments() {
		if (payments == null) {
			payments = new ArrayList<>();
		}
		return payments;
	}
	
	public Stream<WPayment> payments() {
		return payments == null ? Stream.empty() : payments.stream();
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class WPaymentTotal {
		private BigDecimal totalWithheldIva;
		private BigDecimal totalWithheldIsr;
		private BigDecimal totalWithheldIeps;
		private BigDecimal totalTransferredBaseIva16;
		private BigDecimal totalTransferredTaxIva16;
		private BigDecimal totalTransferredBaseIva8;
		private BigDecimal totalTransferredTaxIva8;
		private BigDecimal totalTransferredBaseIva0;
		private BigDecimal totalTransferredTaxIva0;
		private BigDecimal totalTransferredBaseIvaExempt;
		private BigDecimal totalAmountPayments;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class WPayment {
		private String paymentDate;
		private CFormaPago paymentWay;
		private String paymentWayCode;
		private String paymentWayValue;
		private CMoneda currency;
		private MonedaEntity currencyEntity;
		private BigDecimal changeType;
		private BigDecimal amount;
		private String operationNumber;
		private String sourceAccountRfc;
		private String bankName;
		private String payerAccount;
		private String targetAccountRfc;
		private String receiverAccount;
		private String stringTypeCode; // Este valor va en el XML
		private String stringType; // Este valor va en el pdf
		private byte[] paymentCertificate;
		private String originalString;
		private byte[] paymentSeal;
		@Getter(AccessLevel.NONE)
		private List<WRelatedDocument> relatedDocuments;
		@Getter(AccessLevel.NONE)
	    private List<WTaxes> transferreds;
		@Getter(AccessLevel.NONE)
	    private List<WTaxes> withhelds;
		
		public List<WRelatedDocument> getRelatedDocuments() {
			if (relatedDocuments == null) {
				relatedDocuments = new ArrayList<>();
			}
			return relatedDocuments;
		}
		
		public List<WTaxes> getTransferreds() {
			if (transferreds == null) {
				transferreds = new ArrayList<>();
			}
			return transferreds;
		}

		public List<WTaxes> getWithhelds() {
			if (withhelds == null) {
				withhelds = new ArrayList<>();
			}
			return withhelds;
		}
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class WRelatedDocument {
			private String documentId;
			private String serie;
			private String sheet;
			private CMoneda currency;
			private MonedaEntity currencyEntity;
			private BigDecimal changeType;
			private Integer partiality;
			private BigDecimal amountPartialityBefore;
			private BigDecimal amountPaid;
			private BigDecimal difference;
			private String taxObject;
			private String taxObjectCode;
			@Getter(AccessLevel.NONE)
		    private List<WTaxes> transferreds;
			@Getter(AccessLevel.NONE)
		    private List<WTaxes> withhelds;
			
			public List<WTaxes> getTransferreds() {
				if (transferreds == null) {
					transferreds = new ArrayList<>();
				}
				return transferreds;
			}

			public List<WTaxes> getWithhelds() {
				if (withhelds == null) {
					withhelds = new ArrayList<>();
				}
				return withhelds;
			}
		}
	}

}