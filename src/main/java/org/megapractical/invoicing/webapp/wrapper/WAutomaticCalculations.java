package org.megapractical.invoicing.webapp.wrapper;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WAutomaticCalculations {
	private BigDecimal voucherSubtotal;
	private BigDecimal voucherDiscount;
	private BigDecimal voucherTaxTransferred;
	private BigDecimal voucherTaxWithheld;
	private BigDecimal voucherTotal;
}