package org.megapractical.invoicing.webapp.wrapper;

public class WError {
	
	//##### Linea del error
	private Integer line;
	
	//##### Posicion 
	private Integer posicion;
	
	//##### Descripcion del error
	private String description;
	
	//##### Define si es un error de timbrado
	private boolean isStampError;
	
	//##### Codigo del error de timbrado
	private String stampErrorCode;
	
	//##### RFC del receptor
	private String rfcError;
	
	//##### Define si es un error de valdiacion del contenido del xml
	private boolean isSAXParseException;
	
	//##### Mensaje del error de validacion 
	private String saxParseErrorMessage;
	
	//##### Descripcion del error de validacion
	private String saxParseErrorLocalizedMessage;
	
	/* Getters and Setters */
	public Integer getLine() {
		return line;
	}

	public void setLine(Integer line) {
		this.line = line;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPosicion() {
		return posicion;
	}

	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

	public String getStampErrorCode() {
		return stampErrorCode;
	}

	public void setStampErrorCode(String stampErrorCode) {
		this.stampErrorCode = stampErrorCode;
	}

	public String getRfcError() {
		return rfcError;
	}

	public void setRfcError(String rfcError) {
		this.rfcError = rfcError;
	}

	public boolean isStampError() {
		return isStampError;
	}

	public void setStampError(boolean isStampError) {
		this.isStampError = isStampError;
	}

	public boolean isSAXParseException() {
		return isSAXParseException;
	}

	public void setSAXParseException(boolean isSAXParseException) {
		this.isSAXParseException = isSAXParseException;
	}

	public String getSaxParseErrorMessage() {
		return saxParseErrorMessage;
	}

	public void setSaxParseErrorMessage(String saxParseErrorMessage) {
		this.saxParseErrorMessage = saxParseErrorMessage;
	}

	public String getSaxParseErrorLocalizedMessage() {
		return saxParseErrorLocalizedMessage;
	}

	public void setSaxParseErrorLocalizedMessage(String saxParseErrorLocalizedMessage) {
		this.saxParseErrorLocalizedMessage = saxParseErrorLocalizedMessage;
	}
}