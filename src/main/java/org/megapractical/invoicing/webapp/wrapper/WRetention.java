package org.megapractical.invoicing.webapp.wrapper;

import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;

public class WRetention {

	private ContribuyenteEntity taxpayer;
	private ApiRetention apiRetention;

	/*Getters and Setters*/
	public ContribuyenteEntity getTaxpayer() {
		return taxpayer;
	}

	public void setTaxpayer(ContribuyenteEntity taxpayer) {
		this.taxpayer = taxpayer;
	}

	public ApiRetention getApiRetention() {
		return apiRetention;
	}

	public void setApiRetention(ApiRetention apiRetention) {
		this.apiRetention = apiRetention;
	}
	
}