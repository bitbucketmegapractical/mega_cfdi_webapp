package org.megapractical.invoicing.webapp.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.ContactoEntity;

public class WContact {

	private ContactoEntity contact;
	private String action;
	private Boolean contactDeletedFinded;

	public ContactoEntity getContact() {
		return contact;
	}

	public void setContact(ContactoEntity contact) {
		this.contact = contact;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Boolean getContactDeletedFinded() {
		return contactDeletedFinded;
	}

	public void setContactDeletedFinded(Boolean contactDeletedFinded) {
		this.contactDeletedFinded = contactDeletedFinded;
	}	
}