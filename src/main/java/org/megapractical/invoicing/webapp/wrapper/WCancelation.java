package org.megapractical.invoicing.webapp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.client.CancelationProperties;

public class WCancelation {

	private String uuid;
	private List<String> uuids;
	private CancelationProperties properties;

	/*Getters and Setters*/
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public CancelationProperties getProperties() {
		return properties;
	}

	public void setProperties(CancelationProperties properties) {
		this.properties = properties;
	}

	public List<String> getUuids() {
		if(uuids == null) {
			uuids = new ArrayList<>();
		}
		return uuids;
	}

	public void setUuids(List<String> uuids) {
		this.uuids = uuids;
	}
}