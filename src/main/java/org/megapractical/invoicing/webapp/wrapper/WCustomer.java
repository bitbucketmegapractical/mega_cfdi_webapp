package org.megapractical.invoicing.webapp.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.ClienteEntity;

public class WCustomer {

	private ClienteEntity customer;
	private String action;
	private Boolean customerDeletedFinded;
	
	public ClienteEntity getCustomer() {
		return customer;
	}

	public void setCustomer(ClienteEntity customer) {
		this.customer = customer;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Boolean getCustomerDeletedFinded() {
		return customerDeletedFinded;
	}

	public void setCustomerDeletedFinded(Boolean customerDeletedFinded) {
		this.customerDeletedFinded = customerDeletedFinded;
	}
}