package org.megapractical.invoicing.webapp.wrapper;

public class WPropertyAccount {

	private String number;
	
	public WPropertyAccount(){
		super();
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}	
}