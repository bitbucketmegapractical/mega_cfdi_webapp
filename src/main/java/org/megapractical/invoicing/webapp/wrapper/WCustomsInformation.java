package org.megapractical.invoicing.webapp.wrapper;

public class WCustomsInformation {

	private String requirementNumber;
	
	public WCustomsInformation(){
		super();
	}

	public String getRequirementNumber() {
		return requirementNumber;
	}

	public void setRequirementNumber(String requirementNumber) {
		this.requirementNumber = requirementNumber;
	}	
}