package org.megapractical.invoicing.webapp.wrapper;

import java.math.BigDecimal;

import org.megapractical.invoicing.voucher.catalogs.CTipoFactor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WTaxes {
	private BigDecimal base;
	private String tax;
	private CTipoFactor factorType;
	private BigDecimal rateOrFee;
	private BigDecimal amount;
}