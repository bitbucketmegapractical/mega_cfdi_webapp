package org.megapractical.invoicing.webapp.notification;

public class W2Tag {

	private String message;
	private String position;
	private String cssClassName;
	private String cssStyle;

	public W2Tag(){
		super();
	}
	
	/*Getters and Setters*/
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getCssClassName() {
		return cssClassName;
	}

	public void setCssClassName(String cssClassName) {
		this.cssClassName = cssClassName;
	}

	public String getCssStyle() {
		return cssStyle;
	}

	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}
}