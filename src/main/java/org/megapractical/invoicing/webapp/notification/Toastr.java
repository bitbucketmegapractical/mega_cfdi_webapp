package org.megapractical.invoicing.webapp.notification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Toastr {
	private String type;
	private String title;
	private String message;
	
	public static final Toastr success(String title, String message) {
		return toastr(title, message, ToastrType.SUCCESS);
	}
	
	public static final Toastr error(String title, String message) {
		return toastr(title, message, ToastrType.ERROR);
	}
	
	public static final Toastr info(String title, String message) {
		return toastr(title, message, ToastrType.INFO);
	}
	
	public static final Toastr warning(String title, String message) {
		return toastr(title, message, ToastrType.WARNING);
	}
	
	public static final Toastr toastr(String title, String message, ToastrType toastrType) {
		return Toastr.builder().title(title).message(message).type(toastrType.value()).build();
	}
	
	public enum ToastrType {
		SUCCESS("success"),
		ERROR("error"),
		INFO("info"),
		WARNING("warning");
		
		private final String value;

		ToastrType(String v) {
	        value = v;
	    }

	    public String value() {
	        return value;
	    }

	    public ToastrType fromValue(String v) {
	        return valueOf(v);
	    }
	}
	
}