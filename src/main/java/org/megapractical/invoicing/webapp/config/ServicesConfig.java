package org.megapractical.invoicing.webapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;

@Configuration
public class ServicesConfig {

	@Bean
	public SimpleJaxWsServiceExporter jaxWsExporter() {
		SimpleJaxWsServiceExporter exporter = new SimpleJaxWsServiceExporter();
		exporter.setBaseAddress("http://localhost:8888/services/");
		return exporter;
	}
}
