package org.megapractical.invoicing.webapp.config;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
public class AppSettings {

	@Getter
	@Value("${spring.profiles.active}")
	private String activeProfile;

	@Getter
	private int paginationSize = 10;
	
	public String getPropertyValue(String key) {
		if (key != null) {
			var loadPropertiesFrom = "application-dev.properties";
			if (activeProfile.equals("qa")) {
				loadPropertiesFrom = "application-qa.properties";
			} else if (activeProfile.equals("prod")) {
				loadPropertiesFrom = "application-prod.properties";
			}
			
			var propertyValue = getPropertyByProfile(key, loadPropertiesFrom);
			if (propertyValue == null) {
				loadPropertiesFrom = "application.properties";
				propertyValue = getPropertyByProfile(key, loadPropertiesFrom);
			}
			return propertyValue;
		}
		return key;
	}
	
	public String getEnvironment() {
		return getPropertyValue("cfdi.environment");
	}
	
	public boolean isDev() {
		return activeProfile.equals("dev");
	}
	
	public boolean isQa() {
		return activeProfile.equals("qa");
	}
	
	public boolean isProd() {
		return activeProfile.equals("prod");
	}
	
	public boolean getBooleanValue(String key) {
		val value = getPropertyValue(key);
		return Boolean.valueOf(value);
	}
	
	public int getIntValue(String key) {
		val value = getPropertyValue(key);
		return Integer.valueOf(value);
	}
	
	public String getDateFormat() {
		return getPropertyValue("cfdi.date.single.format");
	}
	
	private String getPropertyByProfile(String key, String profile) {
		try {
			
			if (key != null && profile != null) {
				val properties = new Properties();
				properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(profile));
				return properties.getProperty(key);
			}
			
		} catch (IOException e) {
			System.out.println(String.format("#getPropertyValue error - %s", e.getMessage()));
		}
		return null;
	}
	
}