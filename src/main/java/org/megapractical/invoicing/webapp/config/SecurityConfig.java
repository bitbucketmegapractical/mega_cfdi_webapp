package org.megapractical.invoicing.webapp.config;

import javax.sql.DataSource;

import org.megapractical.invoicing.webapp.security.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

@EnableAsync
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;
	
	@Autowired
    private UserDetailsService userDetailsService;
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
    private TrackerAuthenticationFailureHandler trackerAuthenticationFailureHandler;

	@Autowired
	public void configureManager(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService)
    		.passwordEncoder(passwordEncoder());
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	@Bean
	@Override
	protected UserDetailsService userDetailsService() {
		//return userDetailsService;
		return customUserDetailsService;
	}

	@Bean
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	@Bean
	public TrackerAuthenticationFailureHandler trackerAuthenticationFailureHandler(){
		return new TrackerAuthenticationFailureHandler();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/login").permitAll()
				.antMatchers("/loginFailure").permitAll()
				.antMatchers("/forgotPassword").permitAll()
				.antMatchers("/registerAccount").permitAll()
				.antMatchers("/registerSuccessfully").permitAll()
				.antMatchers("/confirmAccount").permitAll()
				.antMatchers("/confirmAssociatedAccountUser").permitAll()
				.antMatchers("/resources/**").permitAll()
				.anyRequest().authenticated()
			.and()
			.formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/loginSuccess", true)
				.failureUrl("/loginFailure")
				.failureHandler(trackerAuthenticationFailureHandler)				
			.and()
			.logout()
				.logoutSuccessUrl("/")
			.and()
			.exceptionHandling()
				.accessDeniedPage("/accessDenied")
			.and()
			.csrf();
	}

}