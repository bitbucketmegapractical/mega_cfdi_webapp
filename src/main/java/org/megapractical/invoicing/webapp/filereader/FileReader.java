package org.megapractical.invoicing.webapp.filereader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import de.siegmar.fastcsv.reader.CsvReader;
import de.siegmar.fastcsv.reader.CsvRow;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class FileReader {
	
	private FileReader() {		
	}
	
	public static List<String[]> readFile(File file) {
		if (file != null) {
			val absolutePath = file.getAbsolutePath();
			return readFile(absolutePath);
		}
		return new ArrayList<>();
	}
	
	public static List<String[]> readFile(String path) {
		val entries = new ArrayList<String[]>();
		try (val csvReader = CsvReader.builder()
									  .fieldSeparator('|')
	            					  .quoteCharacter('\'')
	            					  .build(Paths.get(path))) {
			csvReader.stream().map(CsvRow::getFields).map(i -> i.toArray(new String[0])).forEach(entries::add);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return entries;
	}
	
}