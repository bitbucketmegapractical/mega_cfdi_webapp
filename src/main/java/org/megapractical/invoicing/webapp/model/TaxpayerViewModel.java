package org.megapractical.invoicing.webapp.model;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.megapractical.invoicing.dal.bean.Contribuyente;

public class TaxpayerViewModel extends Contribuyente{
	
	private static final long serialVersionUID = 1L;
	
	private String emitterType;
	private String countryDefault;
	private String passwd;
	private String taxRegime;
	private String certificateNumber;
	private String certificateCreateDate;
	private String certificateExpiredDate;
	private String action;
	private String rfc;
	private String nameOrBusinessName;
	private String email;
	private String phone;

	public TaxpayerViewModel(Contribuyente contribuyente){
		try {
			PropertyUtils.copyProperties(this, contribuyente);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	public TaxpayerViewModel(){}

	/* Getters and Setters */
	public String getCountryDefault() {
		return countryDefault;
	}

	public void setCountryDefault(String countryDefault) {
		this.countryDefault = countryDefault;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getEmitterType() {
		return emitterType;
	}

	public void setEmitterType(String emitterType) {
		this.emitterType = emitterType;
	}

	public String getTaxRegime() {
		return taxRegime;
	}

	public void setTaxRegime(String taxRegime) {
		this.taxRegime = taxRegime;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public String getCertificateCreateDate() {
		return certificateCreateDate;
	}

	public void setCertificateCreateDate(String certificateCreateDate) {
		this.certificateCreateDate = certificateCreateDate;
	}

	public String getCertificateExpiredDate() {
		return certificateExpiredDate;
	}

	public void setCertificateExpiredDate(String certificateExpiredDate) {
		this.certificateExpiredDate = certificateExpiredDate;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getNameOrBusinessName() {
		return nameOrBusinessName;
	}

	public void setNameOrBusinessName(String nameOrBusinessName) {
		this.nameOrBusinessName = nameOrBusinessName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}	
	
	
}