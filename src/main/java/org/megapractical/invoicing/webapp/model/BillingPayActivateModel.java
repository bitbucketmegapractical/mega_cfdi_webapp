package org.megapractical.invoicing.webapp.model;

import java.util.ArrayList;
import java.util.List;

public class BillingPayActivateModel {

	private String taxpayerRfc;
	private String stampPackage;
	private String rfc;
	
	private List<String> stampPackSource = new ArrayList<>();
	private List<String> rfcSource = new ArrayList<>();
	
	/*Geteters and Setters*/
	public List<String> getStampPackSource() {
		return stampPackSource;
	}

	public void setStampPackSource(List<String> stampPackSource) {
		this.stampPackSource = stampPackSource;
	}

	public List<String> getRfcSource() {
		return rfcSource;
	}

	public void setRfcSource(List<String> rfcSource) {
		this.rfcSource = rfcSource;
	}

	public String getStampPackage() {
		return stampPackage;
	}

	public void setStampPackage(String stampPackage) {
		this.stampPackage = stampPackage;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getTaxpayerRfc() {
		return taxpayerRfc;
	}

	public void setTaxpayerRfc(String taxpayerRfc) {
		this.taxpayerRfc = taxpayerRfc;
	}	
}