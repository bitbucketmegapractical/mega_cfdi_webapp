package org.megapractical.invoicing.webapp.model;

import java.lang.reflect.InvocationTargetException;

import javax.validation.constraints.Pattern;

import org.apache.commons.beanutils.PropertyUtils;
import org.megapractical.invoicing.dal.bean.Usuario;

public class UserViewModel extends Usuario {

	private static final long serialVersionUID = 1L;

	private String passwd;
	private String passwdRepeat;
	
	public UserViewModel(Usuario user){
		try {
			PropertyUtils.copyProperties(this, user);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	public UserViewModel() {
		
	}

	/*Getters and Setters*/
	@Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}", message = "La contraseña debe ser al menos de 8 caracteres de longitud, incluir mayúsculas y números.")
	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getPasswdRepeat() {
		return passwdRepeat;
	}

	public void setPasswdRepeat(String passwdRepeat) {
		this.passwdRepeat = passwdRepeat;
	}
	
	
}
