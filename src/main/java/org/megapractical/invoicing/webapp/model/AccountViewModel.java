package org.megapractical.invoicing.webapp.model;

import javax.validation.constraints.AssertTrue;

public class AccountViewModel {
	
	private UserViewModel usuarioViewModel = new UserViewModel();
	private TaxpayerViewModel contribuyenteViewModel = new TaxpayerViewModel();
	private CredentialViewModel credencialViewModel = new CredentialViewModel();
	private ProfileViewModel perfilViewModel = new ProfileViewModel(); 

	@AssertTrue
	private Boolean accept;
	private Long id;
	
	/*Getters and Setters*/
	public Boolean getAccept() {
		return accept;
	}
	
	public void setAccept(Boolean accept) {
		this.accept = accept;
	}
	
	public UserViewModel getUsuarioViewModel() {
		return usuarioViewModel;
	}

	public void setUsuarioViewModel(UserViewModel usuarioViewModel) {
		this.usuarioViewModel = usuarioViewModel;
	}

	public TaxpayerViewModel getContribuyenteViewModel() {
		return contribuyenteViewModel;
	}

	public void setContribuyenteViewModel(TaxpayerViewModel contribuyenteViewModel) {
		this.contribuyenteViewModel = contribuyenteViewModel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CredentialViewModel getCredencialViewModel() {
		return credencialViewModel;
	}

	public void setCredencialViewModel(CredentialViewModel credencialViewModel) {
		this.credencialViewModel = credencialViewModel;
	}

	public ProfileViewModel getPerfilViewModel() {
		return perfilViewModel;
	}

	public void setPerfilViewModel(ProfileViewModel perfilViewModel) {
		this.perfilViewModel = perfilViewModel;
	}	
}