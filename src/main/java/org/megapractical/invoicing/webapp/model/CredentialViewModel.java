package org.megapractical.invoicing.webapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CredentialViewModel {
    private Long id;
    private Long idContribuyente;
    private Long idUsuario;
    private Long idLenguaje;
    private Boolean eliminado;
}