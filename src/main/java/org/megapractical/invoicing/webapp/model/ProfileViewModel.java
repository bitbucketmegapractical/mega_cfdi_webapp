package org.megapractical.invoicing.webapp.model;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.megapractical.invoicing.dal.bean.Perfil;

public class ProfileViewModel extends Perfil {

	private static final long serialVersionUID = 1L;

	public ProfileViewModel(Perfil perfil){
		try {
			PropertyUtils.copyProperties(this, perfil);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	public ProfileViewModel(){
		
	}
}
