package org.megapractical.invoicing.webapp.ztmp;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

import de.siegmar.fastcsv.reader.CsvReader;
import de.siegmar.fastcsv.reader.CsvRow;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public class FileReaderTmp {

	public static void main(String[] args) {
		val filePath = "path/to/file";
		loadFile(filePath);
	}

	private static void loadFile(String filePath) {
		val file = new File(filePath);
		val absolutePath = file.getAbsolutePath();
		readFile(absolutePath);
	}
	
	private static void readFile(String filePath) {
		val absolutePath = Paths.get(filePath);
		
		val entries = new ArrayList<String[]>();
		try (val csvReader = CsvReader.builder()
									  .fieldSeparator('|')
	            					  .quoteCharacter('\'')
	            					  .build(absolutePath)) {
            csvReader.stream().map(CsvRow::getFields).map(i -> i.toArray(new String[0])).forEach(entries::add);
            entries.forEach(System.out::println);
        } catch (IOException e) {
			e.printStackTrace();
		}
	}

}