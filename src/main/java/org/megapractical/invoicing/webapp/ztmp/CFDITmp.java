package org.megapractical.invoicing.webapp.ztmp;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.opencsv.CSVWriter;

@Controller
public class CFDITmp {
	
	@Autowired
	AppSettings appSettings;
	
	@Resource
	ICfdiJpaRepository iCfdiJpaRepository;
	
	@RequestMapping(value="/cfditmp", method=RequestMethod.GET)
	public String cfditmp(Model model) {
		try {
			
			String sDate = "2018-09-21";
			Date startDate = UDate.date(sDate);
			
			String eDate = "2018-09-26";
			Date endDate = UDate.date(eDate);
			
			String repositoryPath = appSettings.getPropertyValue("cfdi.repository");
			
			List<String> cfdiTmps = new ArrayList<>();
			
			List<CfdiEntity> cfdis = iCfdiJpaRepository.findByCanceladoFalseAndRutaXmlNotNullAndFechaExpedicionBetween(startDate, endDate);
			for (CfdiEntity cfdi : cfdis) {
				String xmlPath = repositoryPath + File.separator + cfdi.getRutaXml();
				
				File xmlFile = new File(xmlPath);
				if(xmlFile.exists()) {
					String xml = FileUtils.readFileToString(xmlFile, "UTF-8");
					
					String hash = UUID.randomUUID().toString().replaceAll("-", "");
					
					String rfc = cfdi.getContribuyente().getRfc();
					String stampDate = UDate.formattedDate(cfdi.getFechaExpedicion(), "yyyy-MM-dd");
					
					
					String sql = "INSERT INTO stamp_service.cfdi(id_status, id_cfdi_type, version, uuid, stamp_date, emisor_rfc, xml, hash, is_enable, is_deleted) " + 
								 "VALUES (3, 2, '3.3', '"+cfdi.getUuid()+"', '"+stampDate+"', '"+rfc+"', '"+xml+"', '"+hash+"', true, false);";
					
					cfdiTmps.add(sql);
				}
			}
			
			String cfdiTmpPath = repositoryPath + File.separator + "CFDI_TMP";
			File folderCfidTmp = new File(cfdiTmpPath);
    		if(!folderCfidTmp.exists()){
    			folderCfidTmp.mkdirs();
    		}
    		
			String fileCfidTmpPath = cfdiTmpPath + File.separator + "cfdi_tmp.sql";
			
			File file = new File(fileCfidTmpPath);
    		if(!file.exists()){
    			file.createNewFile();
    		}else{
    			try {
				
    				file.delete();
    				
				} catch (Exception e) {}
    		}
			
    		//CSVWriter writer = new CSVWriter(new FileWriter(file), '|', CSVWriter.NO_QUOTE_CHARACTER);
    		CSVWriter writer = new CSVWriter(new FileWriter(file), '|', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
    		
			for (String item : cfdiTmps) {
				String[] sql = new String[]{item + "\n"};
				writer.writeNext(sql);
			}
			writer.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "/denied/ResourceDenied";
	}
	
}
