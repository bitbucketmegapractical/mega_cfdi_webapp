package org.megapractical.invoicing.webapp.ztmp;

import java.io.File;
import java.security.PrivateKey;
import java.security.Signature;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.ssl.PKCS8Key;
import org.megapractical.invoicing.api.util.UFile;

public class TmpSeal {
	public static void main(String[] args) throws Exception {
		String uuid = "13750CAF-0EDF-4E4A-B313-23E2F6662C26";
		String date = "2018-08-07T19:53:14";
		String selloCFD = "DKOr74Z36mX+L4q65EWzY4TcXOxuIlvQMMjDeUEuWbqeMZvpmkU8M2eXfKsJkvH288e/N87T0rPTtfl1+Jw5qG9nrXfGRzwgQRfsFtEUq+54YrwhgnjOtVD5lks/HiQtdb\r\n" + 
				"v6gudOEf8LJBwpB/azrn73cuN2NZllcq6nHHYx4YRihvD3pmsjjJQ9il0fJsbt6xq2dR/aI610ZMeXr3/neyBTkYRxQRbv7agpTC211EVL8cEK2hx1/PQ5HmMZB9wR70NkM4\r\n" + 
				"Hwx+A5bwYBG08h0UbGkJYCudS59+OZeYhTpvsv32dElmes2Hjy0ykr9b9JPgnhxZAvTCr5ApFw5FEpsg==";
		String cretNumber = "30001000000300036827";
		
		String originalString = getCadenaOriginal(uuid, date, selloCFD, cretNumber);
		System.out.println(originalString);
		
		String privateKeyPath = "E:\\DEVELOPMENT\\COMPANY_WORK\\MEGAPRACTICAL\\PROJECTS\\MEGA_CFDI_SAT\\TOOLS\\UAT\\CERTIFICATE\\PAC\\CSD_CFDI_PRUEBAS_2_DAL050601L35.key";
		File privateKeyFile = new File(privateKeyPath);
		byte[] privateKey = UFile.fileToByte(privateKeyFile);
		String passwd = "12345678a";
		
		String seal = seal(originalString, privateKey, passwd);
		
		System.out.println(seal);
	}

	public static String getCadenaOriginal(String uuid, String date, String selloCFD, String cretNumber) throws Exception {
        /*
         	MapaDatos.put("selloSAT", XMLR.getAttribute("tfd:TimbreFiscalDigital", "SelloSAT"));
			MapaDatos.put("selloCFD", XMLR.getAttribute("tfd:TimbreFiscalDigital", "SelloCFD"));
			MapaDatos.put("noCertificadoSAT", XMLR.getAttribute("tfd:TimbreFiscalDigital", "NoCertificadoSAT"));
			MapaDatos.put("UUID", XMLR.getAttribute("tfd:TimbreFiscalDigital", "UUID"));
			MapaDatos.put("RfcProvCertif", XMLR.getAttribute("tfd:TimbreFiscalDigital", "RfcProvCertif"));
			MapaDatos.put("VersionTFD", XMLR.getAttribute("tfd:TimbreFiscalDigital", "Version"));
			MapaDatos.put("FechaTimbrado", XMLR.getAttribute("tfd:TimbreFiscalDigital", "FechaTimbrado"));
			MapaDatos.put("CadenaoriginalCertSAT", "||" + u.getValue(MapaDatos, "VersionTFD") + "|" + u.getValue(MapaDatos, "UUID") + "|" + u.getValue(MapaDatos, "FechaTimbrado") + "|" + u.getValue(MapaDatos, "RfcProvCertif") + "|" + u.getValue(MapaDatos, "selloCFD") + "|" + u.getValue(MapaDatos, "noCertificadoSAT") + "|"); 
         * */
        
        /*String tfdString = "||1.1|" + uuid + "|" + date + "|DAL050601L35|Megapractical SA de\r\n" + 
        		"CV|" + selloCFD + "|" + cretNumber + "||";*/
		String tfdString = "||1.1|F3B109C7-1286-4A3F-A640-412F1A52C4E3|2018-08-08T08:14:37|DAL050601L35|P4pagwyksLNNys0TZBeIQLjKmNW+18Fkk3mrl82DQJByCok6IjEibblZzw0E1GnkWuEFJiHHUhTxkXmYYuW7lbihnHMthsKJvCK18pglnYhB8Q39CVTS+QfQBAIcyGMei69rO7jV6SAwGBfW4xOdaXq+yiRDsztNcWKYndFOxBafnMR3+qp/WK7Fs/AkBJskICu3T+T0ti7nDp+pWdWqRbrQVdoO170KF7qPj68qTkX5DphLF6Mo3Al0c3qo5Q266+AP5SywMMVyD84pf/JgMWBlqMjJ8HO5dzlJQIPjRzM1GG8+b1mjZcqn8S883+dDM3cJmWRdgKS1LGQUFzETXA==|30001000000300036827||";
        return tfdString;
    }
	
	public static String seal(String originalString, byte[] privateKey, String passwd){
		String seal = "";
        
		try {
        	
            //##### Obtiene la llave privada
        	PKCS8Key pkcs8Key = new PKCS8Key(privateKey, passwd.toCharArray());
            PrivateKey key = pkcs8Key.getPrivateKey();

            //##### Codificando en SHA1
            Signature sign = Signature.getInstance("SHA256withRSA");
            sign.initSign(key);

            //##### Agregar Cadena Original en UTF-8
            sign.update(originalString.getBytes("UTF-8"));

            //##### Codificando en Base64
            seal = new String(Base64.encodeBase64(sign.sign()), "UTF-8");

        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        return seal.toString();
	}
	
}
