package org.megapractical.invoicing.webapp.ztmp;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UCancelation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class TmpClass {

	public class ObjWrapper{
		private String a;
		private String b;
		private String c;
		public String getA() {
			return a;
		}
		public void setA(String a) {
			this.a = a;
		}
		public String getB() {
			return b;
		}
		public void setB(String b) {
			this.b = b;
		}
		public String getC() {
			return c;
		}
		public void setC(String c) {
			this.c = c;
		}
	}
	
	@RequestMapping(value = "/ajxTestRequest", method = RequestMethod.POST)
	public @ResponseBody String emitterRfcAccount(HttpServletRequest request, @RequestParam Object objConcept) {
		
		Gson gson = new Gson();
		
		if(request.getParameter("objConcept") != null){
			/*Object objConcept = request.getAttribute("objConcept");
			
			ScriptableObject scriptableObject = (ScriptableObject) objConcept;
			String asd = scriptableObject.get("a").toString();
			
			ObjWrapper obj = (ObjWrapper) Context.jsToJava(value, desiredType);*/
			
			
			ObjWrapper obj = gson.fromJson(objConcept.toString(), ObjWrapper.class);
			
			String valA = objConcept.toString();
			System.out.println(valA);
		}
		
		if(request.getParameter("conceptArray") != null){
			String conceptList = request.getParameter("conceptArray");
			List<ObjWrapper> list = gson.fromJson(conceptList, new TypeToken<List<ObjWrapper>>(){}.getType());
			list.forEach(x -> System.out.println(x));		
		}
		
		
		return null;
	}
	
	public void pdfWaterMark(){
		String fileNameOrigen = "E:\\DEVELOPMENT\\MEGAPRACTICAL\\PROJECTS\\MEGA_CFDI\\TOOLS\\STORE\\CFDIs\\GUFM851126SK1\\emited\\MEMC790519ML3\\749C48A2-0072-45F9-839E-79A01A405F9E.pdf";
		String path = "E:\\DEVELOPMENT\\MEGAPRACTICAL\\PROJECTS\\MEGA_CFDI\\TOOLS\\STORE\\CFDIs\\GUFM851126SK1\\emited\\MEMC790519ML3\\";
		OutputStream outputStream;
		try {
			outputStream = new FileOutputStream(path + "749C48A2-0072-45F9-839E-79A01A405F9E_CANCELADO.pdf");
			UCancelation.estampaPDF(fileNameOrigen, outputStream);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
