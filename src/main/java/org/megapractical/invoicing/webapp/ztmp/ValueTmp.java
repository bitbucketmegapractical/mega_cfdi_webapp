package org.megapractical.invoicing.webapp.ztmp;

import org.megapractical.invoicing.api.util.MoneyUtils;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValue;

import lombok.val;

public class ValueTmp {

	public static void main(String[] args) {
		doubleValue();
		encode();
		formatWithDecimalSafe();
		formatWithDecimal();
	}
	
	static void doubleValue() {
		Double doubleValue = Double.valueOf("0.15");
		String value = UValue.doubleString(doubleValue);
		System.out.println("Value: " + value);
	}
	
	static void encode() {
		String valueToEncode = "Firrus_17&";
		String encodedValue = UBase64.base64Encode(valueToEncode);
		System.out.println("Value: " + encodedValue);
	}
	
	static void formatWithDecimalSafe() {
		val value = "19.2160";
		val moneyVal = MoneyUtils.formatSafe(value);
		val moneyVal1 = MoneyUtils.formatWithDecimalSafe(value);
		System.out.println("formatSafe: " + moneyVal);
		System.out.println("formatWithDecimalSafe: " + moneyVal1);
	}
	
	static void formatWithDecimal() {
		String nullVal = null;
		val value = MoneyUtils.formatWithDecimal(nullVal);
		val value1 = MoneyUtils.formatWithDecimal("91.2054");
		
		System.out.println("formatWithDecimal val: " + value);
		System.out.println("formatWithDecimal val1: " + value1);
	}

}