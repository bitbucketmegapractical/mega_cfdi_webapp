package org.megapractical.invoicing.webapp.ztmp;

import java.util.regex.Pattern;

public class CatalogTmp {

	public static void main(String[] args) {

		String value = "78101802 (Servicios (transporte) de carga por carretera (en camión) a nivel (regional y nacional))";
		String[] catalog = value.split(Pattern.quote(" ("), 2);

		try {
			
			if(catalog[1].endsWith(")")) {
				catalog[1] = catalog[1].substring(0, catalog[1].length() - 1).trim();
			}
			
		} catch (Exception e) {
			catalog = new String[]{catalog[0], null};
		}
		
		System.out.println("Cat[0]: " + catalog[0]);
		System.out.println("Cat[1]: " + catalog[1]);
	}
}