package org.megapractical.invoicing.webapp.log;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.dal.bean.jpa.BitacoraEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoOperacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IOperationTypeJpaRepository;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class AppLog {

	@PersistenceContext
	private EntityManager entityManager;

	@Resource
	IOperationTypeJpaRepository tipoOperacionJpaRepositoryCustom;

	@Autowired
	UserTools userTools;
	
	@Autowired
	SessionController sessionController; 

	private static final String[] oprationExcluedes = {
		"REGISTRO_CUENTA_USUARIO", 
		"CERRAR_SESION", 
		"SOLICITUD_CAMBIO_CLAVE",
		"GENERACION_CODIGO_ACTIVACION",
		"DESHABILITAR_CODIGO_ACTIVACION",
		"ACTIVACION_CUENTA_USUARIO"
	};
	
	@Transactional
	public void logOperation(OperationCode operationCode) {
		try {
			
			BitacoraEntity bitacora = new BitacoraEntity();
			TipoOperacionEntity operationType = tipoOperacionJpaRepositoryCustom.findByCodigo(operationCode.getFieldDescription());
			bitacora.setTipoOperacion(operationType);
			bitacora.setFecha(new Date());
			bitacora.setHora(new Date());
			bitacora.setUrlAcceso(sessionController.currentUrl());
			bitacora.setEjercicio(UDateTime.yearOfDate(new Date()));
			bitacora.setMes(UDateTime.monthOfDate(new Date()));
			bitacora.setDia(UDateTime.dayOfDate(new Date()));

			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			bitacora.setIp(request.getRemoteAddr());

			UsuarioEntity usuario = userTools.getCurrentUser();
			
			bitacora.setUsuario(usuario);

			entityManager.persist(bitacora);
			entityManager.flush();
			
			try {
				
				Boolean finded = false;
				for (String item : oprationExcluedes) {
					if(item.equals(operationType.getCodigo())){
						finded = true;
						break;
					}
				}
				if(!finded){
					sessionController.sessionUpdate();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void logOperation(List<OperationCode> operationCodeSource) {
		try {
			
			Boolean updateSession = true;
			
			for (OperationCode operationCode : operationCodeSource) {
				BitacoraEntity bitacora = new BitacoraEntity();
				TipoOperacionEntity operationType = tipoOperacionJpaRepositoryCustom.findByCodigo(operationCode.getFieldDescription());
				bitacora.setTipoOperacion(operationType);
				bitacora.setFecha(new Date());
				bitacora.setHora(new Date());
				bitacora.setUrlAcceso(sessionController.currentUrl());
				bitacora.setEjercicio(UDateTime.yearOfDate(new Date()));
				bitacora.setMes(UDateTime.monthOfDate(new Date()));
				bitacora.setDia(UDateTime.dayOfDate(new Date()));
				
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
				bitacora.setIp(request.getRemoteAddr());

				UsuarioEntity usuario = userTools.getCurrentUser();
				
				bitacora.setUsuario(usuario);

				entityManager.persist(bitacora);
				entityManager.flush();
				
				if(updateSession){
					for (String item : oprationExcluedes) {
						if(item.equals(operationCode.getFieldDescription())){
							updateSession = !updateSession;
						}
					}
				}
			}
			
			try {
				
				if(updateSession){
					sessionController.sessionUpdate();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void logOperation(OperationCode operationCode, UsuarioEntity user, String urlAcceso, String ip) {
		try {
			
			TipoOperacionEntity operationType = tipoOperacionJpaRepositoryCustom.findByCodigo(operationCode.getFieldDescription());
			
			BitacoraEntity bitacora = new BitacoraEntity();
			bitacora.setTipoOperacion(operationType);
			bitacora.setFecha(new Date());
			bitacora.setHora(new Date());
			bitacora.setUrlAcceso(urlAcceso);
			bitacora.setIp(ip);
			bitacora.setUsuario(user);
			bitacora.setEjercicio(UDateTime.yearOfDate(new Date()));
			bitacora.setMes(UDateTime.monthOfDate(new Date()));
			bitacora.setDia(UDateTime.dayOfDate(new Date()));
			
			entityManager.persist(bitacora);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void logOperation(OperationCode operationCode, UsuarioEntity user) {
		try {
			
			TipoOperacionEntity operationType = tipoOperacionJpaRepositoryCustom.findByCodigo(operationCode.getFieldDescription());
			
			BitacoraEntity bitacora = new BitacoraEntity();
			bitacora.setTipoOperacion(operationType);
			bitacora.setFecha(new Date());
			bitacora.setHora(new Date());
			bitacora.setUrlAcceso(sessionController.currentUrl());
			bitacora.setUsuario(user);
			
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			bitacora.setIp(request.getRemoteAddr());
			
			bitacora.setEjercicio(UDateTime.yearOfDate(new Date()));
			bitacora.setMes(UDateTime.monthOfDate(new Date()));
			bitacora.setDia(UDateTime.dayOfDate(new Date()));
			
			entityManager.persist(bitacora);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void logOperation(OperationCode operationCode, UsuarioEntity user, String description) {
		try {
			
			TipoOperacionEntity operationType = tipoOperacionJpaRepositoryCustom.findByCodigo(operationCode.getFieldDescription());
			
			BitacoraEntity bitacora = new BitacoraEntity();
			bitacora.setTipoOperacion(operationType);
			bitacora.setFecha(new Date());
			bitacora.setHora(new Date());
			bitacora.setUrlAcceso(sessionController.currentUrl());
			bitacora.setUsuario(user);
			bitacora.setDescripcion(description);
			
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			bitacora.setIp(request.getRemoteAddr());
			
			bitacora.setEjercicio(UDateTime.yearOfDate(new Date()));
			bitacora.setMes(UDateTime.monthOfDate(new Date()));
			bitacora.setDia(UDateTime.dayOfDate(new Date()));
			
			entityManager.persist(bitacora);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void logOperation(OperationCode operationCode, String description) {
		try {
			
			TipoOperacionEntity operationType = tipoOperacionJpaRepositoryCustom.findByCodigo(operationCode.getFieldDescription());
			
			BitacoraEntity bitacora = new BitacoraEntity();
			bitacora.setTipoOperacion(operationType);
			bitacora.setFecha(new Date());
			bitacora.setHora(new Date());
			bitacora.setUrlAcceso(sessionController.currentUrl());
			bitacora.setDescripcion(description);
			
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			bitacora.setIp(request.getRemoteAddr());
			
			bitacora.setEjercicio(UDateTime.yearOfDate(new Date()));
			bitacora.setMes(UDateTime.monthOfDate(new Date()));
			bitacora.setDia(UDateTime.dayOfDate(new Date()));
			
			entityManager.persist(bitacora);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}