package org.megapractical.invoicing.webapp.log;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.dal.bean.jpa.BitacoraSucesoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoSucesoEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.ISuccessTypeJpaRepository;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class TimelineLog {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	UserTools userTools;
	
	@Resource
	ISuccessTypeJpaRepository iSuccessTypeJpaRepository;
	
	@Transactional
	public void timelineLog(EventTypeCode successTypeCode) {
		try {
			
			BitacoraSucesoEntity log = new BitacoraSucesoEntity();
			TipoSucesoEntity successType = iSuccessTypeJpaRepository.findByCodigo(successTypeCode.getFieldDescription());
			log.setTipoSuceso(successType);
			log.setFecha(new Date());
			log.setHora(new Date());

			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			log.setIp(request.getRemoteAddr());

			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive();
			log.setContribuyente(taxpayer);
			
			UsuarioEntity user = userTools.getCurrentUser();
			log.setUsuario(user);

			entityManager.persist(log);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void timelineLog(EventTypeCode successTypeCode, ContribuyenteEntity taxpayer, UsuarioEntity user, String ip) {
		try {
			
			BitacoraSucesoEntity log = new BitacoraSucesoEntity();
			TipoSucesoEntity successType = iSuccessTypeJpaRepository.findByCodigo(successTypeCode.getFieldDescription());
			log.setTipoSuceso(successType);
			log.setFecha(new Date());
			log.setHora(new Date());
			log.setIp(ip);
			log.setContribuyente(taxpayer);
			log.setUsuario(user);

			entityManager.persist(log);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void timelineLog(EventTypeCode successTypeCode, ContribuyenteEntity taxpayer) {
		try {
			
			BitacoraSucesoEntity log = new BitacoraSucesoEntity();
			TipoSucesoEntity successType = iSuccessTypeJpaRepository.findByCodigo(successTypeCode.getFieldDescription());
			log.setTipoSuceso(successType);
			log.setFecha(new Date());
			log.setHora(new Date());
			log.setContribuyente(taxpayer);
			
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			log.setIp(request.getRemoteAddr());
			
			UsuarioEntity user = userTools.getCurrentUser();
			log.setUsuario(user);

			entityManager.persist(log);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void timelineLog(EventTypeCode successTypeCode, ContribuyenteEntity taxpayer, String description) {
		try {
			
			BitacoraSucesoEntity log = new BitacoraSucesoEntity();
			TipoSucesoEntity successType = iSuccessTypeJpaRepository.findByCodigo(successTypeCode.getFieldDescription());
			log.setTipoSuceso(successType);
			log.setFecha(new Date());
			log.setHora(new Date());
			log.setContribuyente(taxpayer);
			log.setDescripcion(description);
			
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			log.setIp(request.getRemoteAddr());
			
			UsuarioEntity user = userTools.getCurrentUser();
			log.setUsuario(user);

			entityManager.persist(log);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void timelineLog(EventTypeCode successTypeCode, String description) {
		try {
			
			BitacoraSucesoEntity log = new BitacoraSucesoEntity();
			TipoSucesoEntity successType = iSuccessTypeJpaRepository.findByCodigo(successTypeCode.getFieldDescription());
			log.setTipoSuceso(successType);
			log.setFecha(new Date());
			log.setHora(new Date());
			log.setDescripcion(description);
			
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			log.setIp(request.getRemoteAddr());
			
			UsuarioEntity user = userTools.getCurrentUser();
			log.setUsuario(user);

			entityManager.persist(log);
			entityManager.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void timelineLog(List<Timeline> timelineSource) {
		try {
			
			for (Timeline timeline : timelineSource) {
				BitacoraSucesoEntity log = new BitacoraSucesoEntity();
				TipoSucesoEntity successType = iSuccessTypeJpaRepository.findByCodigo(timeline.getSuccessTypeCode().getFieldDescription());
				log.setTipoSuceso(successType);
				log.setFecha(new Date());
				log.setHora(new Date());
				
				if(timeline.getTaxpayer() != null){
					log.setContribuyente(timeline.getTaxpayer());
				}
				
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
				log.setIp(request.getRemoteAddr());
				
				UsuarioEntity user = userTools.getCurrentUser();
				log.setUsuario(user);

				entityManager.persist(log);
				entityManager.flush();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public class Timeline{
		private EventTypeCode successTypeCode;
		private ContribuyenteEntity taxpayer;
		
		public Timeline(){}

		public EventTypeCode getSuccessTypeCode() {
			return successTypeCode;
		}

		public void setSuccessTypeCode(EventTypeCode successTypeCode) {
			this.successTypeCode = successTypeCode;
		}

		public ContribuyenteEntity getTaxpayer() {
			return taxpayer;
		}

		public void setTaxpayer(ContribuyenteEntity taxpayer) {
			this.taxpayer = taxpayer;
		}		
	}
}
