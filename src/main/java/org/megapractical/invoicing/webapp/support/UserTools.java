package org.megapractical.invoicing.webapp.support;

import java.time.Period;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioContratoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.CredencialEntity;
import org.megapractical.invoicing.dal.bean.jpa.LenguajeEntity;
import org.megapractical.invoicing.dal.bean.jpa.PerfilEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IActivationCodeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IConsigneeContractJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IConsigneeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICredentialJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ILanguageJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollFileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPreferencesJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IProfileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IUserJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class UserTools {

	@Autowired
	AppSettings appSettings;
	
	@Resource
	IUserJpaRepository iUserJpaRepository;
	
	@Resource
	ITaxpayerJpaRepository iTaxpayerJpaRepository;
	
	@Resource
	ICredentialJpaRepository iCredentialJpaRepository;
	
	@Resource
	IProfileJpaRepository iProfileJpaRepository;
	
	@Resource
	private IActivationCodeJpaRepository iActivationCodeJpaRepository;
	
	@Resource
	private ILanguageJpaRepository iLanguageJPARepository;
	
	@Resource
	private IPreferencesJpaRepository iPreferencesJpaRepository;
	
	@Resource
	IAccountJpaRepository iAccountJpaRepository; 
	
	@Resource
	IConsigneeJpaRepository iConsigneeJpaRepository;
	
	@Resource
	IConsigneeContractJpaRepository iConsigneeContractJpaRepository;
	
	@Resource
	IPayrollFileJpaRepository iPayrollFileJpaRepository;
	
	private static final String ROLE_USR = "ROLE_USR";
	private static final String ROLE_MST = "ROLE_MST";
	private static final String ROLE_ADM = "ROLE_ADM";
	private static final String ROLE_ROOT = "ROLE_ROOT";
	
	private static final String LOCALE_DEFAULT = "es_MX";	
	
	private static final int FREE_PAYROLL_FILES_UPLOAD = 1000000;
	private static final int FREE_PAYMENTS_FILES_UPLOAD = 1000000;
	
	public String getUsernameOfLoggedUser() {
		String username = null;
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			val principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			} else {
				username = principal.toString();
			}
		}
		return username;
	}
	
	public PerfilEntity getProfile(ContribuyenteEntity contribuyente) {
		return iProfileJpaRepository.findByContribuyente(contribuyente);
	}

	public UsuarioEntity getCurrentUser() {
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			String username;
			
			val principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			} else {
				username = principal.toString();
			}
			return getUserByEmail(username);
		}
		return null;
	}
	
	public UsuarioEntity getAdmonLogged() {
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			val principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (principal instanceof UserDetails) {
				val username = ((UserDetails) principal).getUsername();
				val authorities = ((UserDetails) principal).getAuthorities().iterator().next().getAuthority();
				if (authorities.equals(ROLE_ADM)) {
					return getUserByEmail(username);
				}
			} 
		}
		return null;
	}
	
	public UsuarioEntity getRootLogged() {
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			val principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (principal instanceof UserDetails) {
				val username = ((UserDetails) principal).getUsername();
				val authorities = ((UserDetails) principal).getAuthorities().iterator().next().getAuthority();
				if (authorities.equals(ROLE_ROOT)) {
					return getUserByEmail(username);
				}
			} 
		}
		return null;
	}
	
	public UsuarioEntity getUserByEmail(String email) {
		return iUserJpaRepository.findByCorreoElectronico(email);
	}
	
	public UsuarioEntity getUserByTaxpayer(ContribuyenteEntity taxpayer) {
		val credential = iCredentialJpaRepository.findByContribuyenteAndEliminadoFalse(taxpayer);
		if (!UValidator.isNullOrEmpty(credential)) {
			return credential.getUsuario();
		}
		return null;
	}
	
	public CredencialEntity getCredentialByUser() {
		val currentUser = getCurrentUser();
		if (currentUser != null) {
			CredencialEntity credential = null;
			final PageRequest pageRequest = PageRequest.of(0, 1);
			Page<CredencialEntity> credentialsPage = iCredentialJpaRepository.findByUsuario(currentUser, pageRequest);
			if (credentialsPage.getNumberOfElements() > 0) {
				credential = credentialsPage.getContent().get(0); 
			}
			return credential;
		}
		return null;
	}
	
	public CredencialEntity getCredentialByUser(UsuarioEntity usuarioEntity) {
		if (usuarioEntity != null) {
			CredencialEntity credential = null;
			final PageRequest pageRequest = PageRequest.of(0, 1);
			Page<CredencialEntity> credentialsPage = iCredentialJpaRepository.findByUsuario(usuarioEntity, pageRequest);
			if (credentialsPage.getNumberOfElements() > 0) {
				credential = credentialsPage.getContent().get(0); 
			}
			return credential;
		}
		return null;
	}
	
	public CredencialEntity getCredentialByUserByTaxpayer(UsuarioEntity user, ContribuyenteEntity taxpayer) {
		if (user != null && taxpayer != null) {
			return iCredentialJpaRepository.findByUsuarioAndContribuyenteAndEliminadoFalse(user, taxpayer);
		}
		return null;
	}
	
	public LenguajeEntity getLocaleDefault() {
		return iLanguageJPARepository.findByCodigoAndEliminadoFalse(LOCALE_DEFAULT);
	}
	
	public LenguajeEntity getLocaleByUser() {
		return this.getCredentialByUser().getLenguaje();
	}
	
	public String getLocale() {
		if (isAdmon() || isRoot()) {
			return this.getLocaleDefault().getCodigo();
		}

		if (this.getCurrentUser() != null) {
			return this.getLocaleByUser().getCodigo();
		} else {
			return this.getLocaleDefault().getCodigo();
		}
	}
	
	public String getLocale(UsuarioEntity user) {
		if (user == null) {
			return this.getLocaleDefault().getCodigo();
		}
		
		val username = user.getCorreoElectronico();
		if (isAdmon(username) || isRoot(username)) {
			return this.getLocaleDefault().getCodigo();
		}
		return this.getLocaleByUser().getCodigo();
	}
	
	public ContribuyenteEntity getContribuyenteActive() {
		val credential = getCredentialByUser();
		if (credential != null) {
			return iTaxpayerJpaRepository.findById(credential.getContribuyente().getId()).orElse(null);
		}
		return null;
	}
	
	public ContribuyenteEntity getContribuyenteActive(UsuarioEntity usuarioEntity) {
		val credential = getCredentialByUser(usuarioEntity);
		if (credential != null) {
			return iTaxpayerJpaRepository.findById(credential.getContribuyente().getId()).orElse(null);
		}
		return null;
	}
	
	public ContribuyenteEntity getContribuyenteActive(String rfc) {
		return iTaxpayerJpaRepository.findByRfc(rfc);
	}
	
	public boolean isMaster() {
		val activeTaxpayer = getContribuyenteActive();
		val profile = getProfile(activeTaxpayer);
		if (!UValidator.isNullOrEmpty(profile)) {
			return profile.getRol().getCodigo().equals(ROLE_MST);
		}
		return false;
	}
	
	public boolean isMaster(String username) {
		val user = getUserByEmail(username);
		if (user != null) {
			return user.getAuthority().equals(ROLE_MST);
		}
		return false;
	}
	
	public boolean isMaster(ContribuyenteEntity taxpayer) {
		if (UValidator.isNullOrEmpty(taxpayer)) {
			return false;
		}
		
		//##### Determinando si el contribuyente tiene perfil
		val profile = getProfile(taxpayer);
		if (!UValidator.isNullOrEmpty(profile)) {
			return profile.getRol().getCodigo().equals(ROLE_MST);
		} else {
			//##### Determinando si el contribuyente es una cuenta asociada				
			val account = iAccountJpaRepository.findByContribuyenteAsociadoAndEliminadoFalse(taxpayer);
			if (!UValidator.isNullOrEmpty(account)) {
				val masterTaxpayer = account.getContribuyente();
				return isMaster(masterTaxpayer);
			}
		}
		
		return false;
	}
	
	public boolean isAssociatedAccount(ContribuyenteEntity taxpayer) {
		val account = iAccountJpaRepository.findByContribuyenteAsociadoAndEliminadoFalse(taxpayer);
		return !UValidator.isNullOrEmpty(account);
	}
	
	public boolean isAssociatedAccount() {
		val taxpayer = getContribuyenteActive();
		val account = iAccountJpaRepository.findByContribuyenteAsociadoAndEliminadoFalse(taxpayer);
		return !UValidator.isNullOrEmpty(account);
	}
	
	public boolean isAdmon() {
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			val principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (principal instanceof UserDetails) {
				val authorities = ((UserDetails) principal).getAuthorities().iterator().next().getAuthority();
				if (authorities.equals(ROLE_ADM)) {
					val username = ((UserDetails) principal).getUsername();
					return getUserByEmail(username) != null;
				}
			} 
		}
		return false;
	}
	
	public boolean isAdmon(String username) {
		val user = getUserByEmail(username);
		if (user != null) {
			return user.getAuthority().equals(ROLE_ADM);
		}
		return false;
	}
	
	public boolean isRoot() {
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			val principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (principal instanceof UserDetails) {
				val authorities = ((UserDetails) principal).getAuthorities().iterator().next().getAuthority();
				if (authorities.equals(ROLE_ROOT)) {
					val username = ((UserDetails) principal).getUsername();
					return getUserByEmail(username) != null;
				}
			} 
		}
		return false;
	}
	
	public boolean isRoot(String username) {
		val user = getUserByEmail(username);
		if (user != null) {
			return user.getAuthority().equals(ROLE_ROOT);
		}
		return false;
	}
	
	public boolean isUser() {
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			val principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			if (principal instanceof UserDetails) {
				val authorities = ((UserDetails) principal).getAuthorities().iterator().next().getAuthority();
				if (authorities.equals(ROLE_USR)) {
					val username = ((UserDetails) principal).getUsername();
					return getUserByEmail(username) != null;
				}
			} 
		}
		return false;
	}
	
	public ConsignatarioEntity consigneeByContribuyente() {
		val taxpayer = getContribuyenteActive();
		return consigneeByContribuyente(taxpayer);
	}
	
	public ConsignatarioEntity consigneeByContribuyente(ContribuyenteEntity taxpayer) {
		return iConsigneeJpaRepository.findByContribuyente(taxpayer);
	}
	
	public boolean isConsignee() {
		val taxpayer = getContribuyenteActive();
		return isConsignee(taxpayer);
	}
	
	public Boolean isConsignee(ContribuyenteEntity taxpayer) {
		val consignee = iConsigneeJpaRepository.findByContribuyente(taxpayer);
		if (!UValidator.isNullOrEmpty(consignee)) {
			return !consignee.getContratoFinalizado();
		}
		return false;
	}
	
	public boolean isConsigneeActive() {
		val consignee = this.consigneeByContribuyente();
		if (!UValidator.isNullOrEmpty(consignee)) {				
			val now = new Date();
			int durationInDays = UDateTime.durationBetweenDatesInDates(now, consignee.getFechaFinContrato());
			return durationInDays > 0;
		}
		return false;
	}
	
	public boolean isConsigneeActive(ContribuyenteEntity taxpayer) {
		val consignee = iConsigneeJpaRepository.findByContribuyente(taxpayer);
		if (!UValidator.isNullOrEmpty(consignee)) {				
			val now = new Date();
			int durationInDays = UDateTime.durationBetweenDatesInDates(now, consignee.getFechaFinContrato());
			return durationInDays > 0;
		}
		return false;
	}
	
	public boolean isConsigneeAvailable(ContribuyenteEntity taxpayer) {
		val consignee = iConsigneeJpaRepository.findByContribuyente(taxpayer);
		if (!UValidator.isNullOrEmpty(consignee)) {
			return consignee.getActivo();
		}
		return false;
	}
	
	public ConsignatarioContratoEntity consigneeContractActive(ConsignatarioEntity consignee) {
		return iConsigneeContractJpaRepository.findByConsignatarioAndActivoTrue(consignee);
	}
	
	public Integer testPeriod() {
		try {
			String testPeriod = appSettings.getPropertyValue("cfdi.test.period");
			return Integer.valueOf(testPeriod);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Integer determinePeriod() {
		val loggedUser = getCurrentUser();
		val activationCode = iActivationCodeJpaRepository.findByUsuario(loggedUser);
		val activationDate = activationCode.getFechaActivacion();
		
		val testPeriodInt = testPeriod();
		val finalTestPeriod = UDateTime.localDateFromDate(activationDate).plusDays(testPeriodInt);
		val period = Period.between(UDateTime.getLocalDateToday(), finalTestPeriod);
		return period.getDays();
	}
	
	public PreferenciasEntity getPreferences() {
		return iPreferencesJpaRepository.findByContribuyente(getContribuyenteActive(getContribuyenteActive().getRfcActivo()));
	}
	
	public PreferenciasEntity getPreferences(ContribuyenteEntity taxpayer) {
		return iPreferencesJpaRepository.findByContribuyente(getContribuyenteActive(taxpayer.getRfcActivo()));
	}
	
	public void log(String log) {
		val environment = UProperties.env();
		
		val activeTaxpayer = getContribuyenteActive();
		val activeRfc = activeTaxpayer != null ? activeTaxpayer.getRfcActivo() : null;
		
		val username = getUsernameOfLoggedUser();
		val dateTime = UPrint.consoleDatetime();
		
		String console = null;
		if (!UValidator.isNullOrEmpty(activeRfc) && !UValidator.isNullOrEmpty(username)) {
			console = String.format("%s <MEGA-CFDI WEBAPP | %s | %s | %s> %s", dateTime, environment, username, activeRfc, log);
		} else {
			console = String.format("%s <%s> %s", dateTime, environment, log);
		}
		UPrint.logWithLine(console);
	}
	
	public String consolePrintLog() {
		String dateTime = UPrint.consoleDatetime();
		return dateTime + "[SYSTEM LOG] ";
	}
	
	//Devuleve el dia actual del periodo contratado
	public int getCurrentDay() {
		//conversion a dias		
		val initialDate = this.getContribuyenteActive().getFechaInicioPlan();
		val today = Calendar.getInstance().getTime();
		//dia que va transcurrido desde que comenzo el servicio en milisegundos
		long diff = today.getTime() - initialDate.getTime();
		return (int) (diff / (1000 * 60 * 60 * 24));
	}
	
	//Devuelve la cantidad de dias que quedan del periodo contratado
	public int getDaysLeft() {
		if (isRoot() || isAdmon()) {
			return 0;
		}
		
		val finalDate = this.getContribuyenteActive().getFechaFinPlan();
		val today = Calendar.getInstance().getTime();
		//días que quedan del servicio en milisegundos 
		long diff = finalDate.getTime() - today.getTime();
		//conversion a dias
		return (int) (diff / (1000 * 60 * 60 * 24)) + 1;			
	}
	/*
	//Devuelve la cantidad de dias que quedan del periodo contratado
		public int getDaysLeft(String rfc) {		
			try {			
				Date finalDate = this.getContribuyenteActive(rfc).getFechaFinPlan();
				Date today = Calendar.getInstance().getTime();
				//días que quedan del servicio en milisegundos 
				long diff = finalDate.getTime() - today.getTime();
				//conversion a dias
				int daysLeft = (int) ((diff / (1000 * 60 * 60 * 24))+1);
				return daysLeft;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 0;
		}
		*/
	
	//Regresa la cantidad de dias totales del servicio
	public int getTotalDays() {		
		val finalDate = this.getContribuyenteActive().getFechaFinPlan();
		val initialDate = this.getContribuyenteActive().getFechaInicioPlan();
		
		long diff = finalDate.getTime() - initialDate.getTime();
		//conversion a dias
		return (int) (diff / (1000 * 60 * 60 * 24));					
	}
	
	//Regresa true si tiene días en su cuenta
	public boolean haveDaysLeft() {
		return this.getDaysLeft() > 0;
	}
	
	/*
	//Regresa true si tiene días en su cuenta
		public boolean haveDaysLeft(String rfc) {
			
			try {
				if (this.getDaysLeft(rfc) > 0) {
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
			return false;
		}
	/*
	//Regresa la cantidas de archivos de nomina que ha subido
	public int getNominasSubidas() {
		try {
			ContribuyenteEntity test = this.getContribuyenteActive();
			int nominasSubidas = this.getContribuyenteActive().getNominasSubidas();
			return nominasSubidas;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	//Regresa la cantidas de archivos de nomina que ha subido
		public int getNominasSubidas(String rfc) {
			try {
				int nominasSubidas = this.getContribuyenteActive(rfc).getNominasSubidas();
				return nominasSubidas;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 0;
		}
	*/
	//regresa true si puede subir archivos de nomina
	public boolean canUploadPayroll() {
		val archivosSubidos = this.getContribuyenteActive().getNominasSubidas();
		return (archivosSubidos < FREE_PAYROLL_FILES_UPLOAD || !this.isUser())  && this.haveDaysLeft();
	}
	
	public boolean canUploadPayments() {
		val archivosSubidos = this.getContribuyenteActive().getPagosSubidos();
		return (archivosSubidos < FREE_PAYMENTS_FILES_UPLOAD || !this.isUser())  && this.haveDaysLeft();
	}
	
}