package org.megapractical.invoicing.webapp.support;

import java.io.File;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("session")
public class EmitterTools {
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Resource
	ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	
	@Resource
	ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;
	
	@Resource
	ITaxpayerPersonTypeJpaRepository iTaxpayerPersonTypeJpaRepository;
	
	@Resource
	IStampJpaRepository iStampJpaRepository;
	
	public ContribuyenteEntity getTaxpayer(){
		try {
			
			return userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ContribuyenteEntity getTaxpayerByActiveRfc(){
		try {
			
			ContribuyenteEntity taxpayer = this.getTaxpayer(); 
			return userTools.getContribuyenteActive(taxpayer.getRfcActivo());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ContribuyenteCertificadoEntity getCertificate(){
		try {
			
			return iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(getTaxpayer());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ContribuyenteRegimenFiscalEntity getTaxRegime(){
		try {
			
			return iTaxpayerTaxRegimeJpaRepository.findByContribuyente(getTaxpayer());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ContribuyenteTipoPersonaEntity getPersonType(){
		try {
			
			return iTaxpayerPersonTypeJpaRepository.findByContribuyente(getTaxpayer());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean isTaxInformationAvailable(){
		try {
			
			return getTaxRegime() != null ? true : false;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean isStampAvailable(){
		try {
			
			TimbreEntity stamp = iStampJpaRepository.findByContribuyente(getTaxpayer());
			return (stamp != null && stamp.getTimbresDisponibles() >= 1 && stamp.getHabilitado()) ? true : false; 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public String getLogo(){
		try {
			
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			String logo = taxpayer.getLogo();
			
			String path = null;
			path = !UValidator.isNullOrEmpty(logo) ? appSettings.getPropertyValue("cfdi.logo.path") + File.separator + logo : null;
			
			return path;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
