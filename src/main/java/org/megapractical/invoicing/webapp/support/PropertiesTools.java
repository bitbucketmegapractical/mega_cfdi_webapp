package org.megapractical.invoicing.webapp.support;

import org.apache.commons.lang3.RandomStringUtils;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IProductServiceJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PropertiesTools {

	@Autowired
	private AppSettings appSettings;

	@Autowired
	private UserTools userTools;

	@Autowired
	private IProductServiceJpaRepository iProductoServicioJpaRepository;

	public String getEnvironment() {
		return appSettings.getPropertyValue("cfdi.environment");
	}

	public String getEmitterId() {
		return userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo()).getId().toString();
	}
	
	public String getEmitterId(ContribuyenteEntity taxpayer) {
		return userTools.getContribuyenteActive(taxpayer.getRfcActivo()).getId().toString();
	}

	public String getSessionId() {
		return userTools.getCurrentUser().getId().toString();
	}
	
	public String getSessionId(UsuarioEntity currentUser) {
		return currentUser.getId().toString();
	}

	public String getSourceSystem() {
		val productoServicioEntity = iProductoServicioJpaRepository.findByCodigoAndHabilitadoTrue(userTools
				.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo()).getProductoServicioActivo());
		return productoServicioEntity.getCodigo();
	}
	
	public String getSourceSystem(ContribuyenteEntity taxpayer) {
		val productoServicioEntity = iProductoServicioJpaRepository.findByCodigoAndHabilitadoTrue(userTools
				.getContribuyenteActive(taxpayer.getRfcActivo()).getProductoServicioActivo());
		return productoServicioEntity.getCodigo();
	}

	public String getSourceSystem(String rfc) {
		val productoServicioEntity = iProductoServicioJpaRepository
				.findByCodigoAndHabilitadoTrue(userTools.getContribuyenteActive(rfc).getProductoServicioActivo());
		return productoServicioEntity.getCodigo();
	}
	
	public String getCorrelationId() {
		return UValue.stringValueUppercase(RandomStringUtils.randomAlphabetic(12));
	}

	public String getServices() {
		val activeServices = appSettings.getPropertyValue("cfdi.services");
		System.out.println("===== Active Services: " + activeServices + " =====");
		return activeServices;
	}
	
}