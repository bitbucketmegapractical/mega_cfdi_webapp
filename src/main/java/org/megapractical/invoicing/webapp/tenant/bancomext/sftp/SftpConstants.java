package org.megapractical.invoicing.webapp.tenant.bancomext.sftp;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class SftpConstants {
	
	private SftpConstants() {
	}
	
	public static final String SFTP_IN_PATH = "IN";
	
	public static final String SFTP_OUT_PATH = "OUT";
	
	public static final String SFTP_ERROR_PATH = "ERROR";
	
	public static final String CFDI_SFTP = "EDC";
	
	public static final String PAYROLL_SFTP = "PAYROLL";
	
	public static final String SFTP_PATH_FORMAT = "%s/%s";
	
}