package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Future;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextMailService;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary.StampSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class BancomextStampSummaryServiceImpl implements BancomextStampSummaryService {

	private final AppSettings appSettings;
	private final BancomextMailService bancomextMailService;

	@Override
	@Async
	public Future<Boolean> sendStampSummaryMail(BancomextStampSummary stampSummary) {
		try {
			addRecipient(stampSummary);
			logInfo(stampSummary);
			sendSummaryReport(stampSummary);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new AsyncResult<>(Boolean.TRUE);
	}

	@Override
	@Async
	public Future<Boolean> sendMailForDuplicateFileError(String fileName, String recipients) {
		try {
			val stampSummary = StampSummary.summary(BancomextFileErrorWrapper.DUPLICATE_FILE);
			val summary = Arrays.asList(stampSummary);
			val bancomextStampSummary = new BancomextStampSummary(recipients, summary, fileName);
			addRecipient(bancomextStampSummary);
			logInfo(bancomextStampSummary);
			sendSummaryReport(bancomextStampSummary);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new AsyncResult<>(Boolean.TRUE);
	}
	
	@Override
	@Async
	public Future<Boolean> sendMailForFileContentError(BancomextFileErrorWrapper fileErrorWrapper) {
		try {
			if (fileErrorWrapper != null && fileErrorWrapper.getErrorDetail() != null) {
				val errorDetail = fileErrorWrapper.getErrorDetail();
				val summary = new ArrayList<StampSummary>();
				if (AssertUtils.hasValue(errorDetail.getErrorDescription())) {
					summary.add(StampSummary.summary(errorDetail.getErrorDescription()));
				}
				errorDetail.getParserErrors().forEach(error -> summary.add(StampSummary.summary(error)));
				errorDetail.getVoucherDetails().forEach(detail -> summary.add(StampSummary.summary(detail)));
				
				val recipients = fileErrorWrapper.getSummaryEmailInfo();
				val fileName = fileErrorWrapper.getFileWrapper().getBancomextFile().getNombre();
				val bancomextStampSummary = new BancomextStampSummary(recipients, summary, fileName);
				addRecipient(bancomextStampSummary);
				logInfo(bancomextStampSummary);
				sendSummaryReport(bancomextStampSummary);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new AsyncResult<>(Boolean.TRUE);
	}

	/**
	 * Adding bancomext@megapractical.com to recipients
	 * 
	 * @param stampSummary
	 */
	private void addRecipient(final BancomextStampSummary stampSummary) {
		val recipients = new ArrayList<String>();
		recipients.addAll(stampSummary.getRecipients());
		
		val includeBancomextMega = appSettings.getBooleanValue("send.email.to.bancomext.mega");
		if (includeBancomextMega) {
			recipients.add("bancomext@megapractical.com");
		}
		
		stampSummary.setRecipients(recipients);
	}

	private void sendSummaryReport(BancomextStampSummary stampSummary) {
		bancomextMailService.sendEmail(stampSummary);
	}
	
	private void logInfo(BancomextStampSummary stampSummary) {
		val sendTo = stampSummary.getRecipients().toString();
		UPrint.logWithLine(UProperties.env(),
				"[INFO] BANCOMEXT STAMP SUMMARY SERVICE > PREPARING TO SEND STAMP SUMMARY REPORT OF "
						+ stampSummary.getFileName() + " FILE TO " + sendTo);
	}

}