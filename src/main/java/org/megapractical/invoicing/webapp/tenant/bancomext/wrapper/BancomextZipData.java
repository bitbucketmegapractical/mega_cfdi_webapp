package org.megapractical.invoicing.webapp.tenant.bancomext.wrapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextZipData {
	private BancomextFileWrapper fileWrapper;
	private String cfdisPath;
	private String cfdisZippedPath;
	private String errorPath;
	private String errorZippedPath;
	
	public BancomextZipData(BancomextFileWrapper fileWrapper, String cfdisPath, String errorPath) {
		this.fileWrapper = fileWrapper;
		this.cfdisPath = cfdisPath;
		this.errorPath = errorPath;
	}
}