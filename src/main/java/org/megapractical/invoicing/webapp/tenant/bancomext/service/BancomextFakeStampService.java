package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.util.HashMap;
import java.util.Random;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.sat.complement.payroll.Nomina;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.tfd.TfdParser;
import org.megapractical.invoicing.voucher.catalogs.CTipoDeComprobante;
import org.megapractical.invoicing.webapp.util.FakeStampUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.val;
import lombok.var;

/**
 * This is a test service that should only be used in the development
 * environment to simulate the stamping of a CFDI
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public final class BancomextFakeStampService {

	private static final ObjectMapper objectMapper = new ObjectMapper();
	private static final Random RANDOM = new Random();

	private BancomextFakeStampService() {
	}

	public static StampResponse stamp(Comprobante voucher) {
		return stamp(voucher, false);
	}
	
	public static StampResponse stampWithError(Comprobante voucher) {
		return stamp(voucher, true);
	}
	
	 public static StampResponse stampRandom(Comprobante voucher) {
        return stamp(voucher, RANDOM.nextBoolean());
    }
	
	private static StampResponse stamp(Comprobante voucher, boolean withStampError) {
		try {
			if (!withStampError) {
				val stampResponse = readFromJsonFile(voucher);
				if (stampResponse != null) {
					stampResponse.setUuid(UValue.uuid()); // New UUID to avoid persistence issues
					stampResponse.setVoucher(voucher);
					stampResponse.setTfd(TfdParser.getTfd(stampResponse.getStampedXml()));
					return stampResponse;
				}
			} else {
				return FakeStampUtils.cfdiFakeStampError();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static StampResponse readFromJsonFile(Comprobante voucher) {
		try {
			String filePath = null;
			var serie = voucher.getSerie();
			
			val voucherType = voucher.getTipoDeComprobante();
			if (CTipoDeComprobante.N.equals(voucherType)) {
				serie = determineSerie(voucher);
			}
			filePath = getJsonFile(serie);
			
			val is = BancomextFakeStampService.class.getResourceAsStream(filePath);
			return objectMapper.readValue(is, StampResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String determineSerie(Comprobante voucher) {
		var serie = voucher.getSerie();
		if (serie.endsWith("vdz")) {
			serie = "RH-PEN";
		} else {
			for (Object object : voucher.getComplementos().iterator().next().getAnies()) {
				if (object instanceof Nomina) {
					val payroll = (Nomina) object;
					val employerRegistration = payroll.getEmisor().getRegistroPatronal();
					if (AssertUtils.hasValue(employerRegistration)) {
						serie = "RH-ACT";
					} else {
						serie = "RH-JUB";
					}
				}
			}
		}
		return serie;
	}

	private static String getJsonFile(String serie) {
		val data = new HashMap<String, String>();
		data.put("BK", "adca36a8-ce8e-4ccd-8f2f-a6c44821367d.json");
		data.put("COM", "2514e3d8-68fc-4439-889d-2a3b55154faa.json");
		data.put("IMX", "86420316-c232-463d-83e5-ded5b8990759.json");
		data.put("FAC", "3811ff94-3472-47e7-b48e-7b061995f4a8.json");
		data.put("PM", "bc59c912-1d71-4457-ae80-10f828686e78.json");
		data.put("NC", "e9239b7d-eb80-4cb4-92c3-da6f50404be6.json");
		data.put("DF", "2e557925-0e2a-4557-937a-85d6e9cfba80.json");
		data.put("RP", "50a7d3ff-59ae-4633-b6a6-fcf51851877b.json");
		data.put("REPF", "e7a7308f-1bc5-4e43-b65e-6564b83a45b7.json");		
		data.put("RH-PEN", "bc63aa3e-4097-4215-bf03-6fe4fb0c933c.json");
		data.put("RH-JUB", "b4df0c96-6e5f-43ae-9a6a-65943e4479ad.json");
		data.put("RH-ACT", "b9f4f27f-1a4e-4f3e-9614-25c6a416810e.json");
		data.put("EDC", "c3f9beda-f4e4-497a-8186-ed1cdcdf68ed.json");
		data.put("BNCEINT", "035953ab-c0ff-4b94-b35e-302d7300cfaf.json");
		//data.put("BNCENAL", "809b1c19-0029-4082-a422-252dde7b96eb.json"); //TesDep 
		data.put("BNCENAL", "ad59a800-295d-46ad-92a7-c6aa875ef547.json"); //TesNac
		data.put("BNCEDIV", "459bf390-93d1-461a-b644-759850804972.json");

		return FakeStampUtils.JSON_TMP_PATH + data.get(serie);
	}

}