package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.dal.bean.datatype.Origen;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.api.S3UploadService;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.CfdiSftpService;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.PayrollSftpService;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.SftpConstants;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.SftpUtils;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextZipData;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * Use this service class only in development environment
 * 
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class BancomextFileReaderService {

	private final AppSettings appSettings;
	private final BancomextService bancomextService;
	private final S3UploadService s3UploadService;
	private final BancomextStampSummaryService bancomextStampSummaryService;
	private final CfdiSftpService cfdiSftpService;
	private final PayrollSftpService payrollSftpService;
	private final SftpUtils sftpUtils;

	private static final Origen ORIGIN = Origen.WEB;

	//@Scheduled(fixedRate = 600000)
	public void readCfdiFiles() {
		if (appSettings.isDev()) {
			// The file must be exist in path :
			// STORE/BANCOMEXT/EKU9003173C9/<CURRENT_DATE>/<FILE_NAME_WITHOUT_EXTENSION>/<FILE_NAME_WITHOUT_EXTENSION>/<FILE_NAME.txt>
			val fileNameWithExtension = "<FILE_NAME.txt>";
			val files = Arrays.asList(fileNameWithExtension);
			readFiles(files, SftpConstants.CFDI_SFTP);
		}
	}
	
	//@Scheduled(fixedRate = 600000)
	public void readPayrollFiles() {
		if (appSettings.isDev()) {
			// The file must be exist in path :
			// STORE/BANCOMEXT/EKU9003173C9/<CURRENT_DATE>/<FILE_NAME_WITHOUT_EXTENSION>/<FILE_NAME_WITHOUT_EXTENSION>/<FILE_NAME.txt>
			val fileNameWithExtension = "<FILE_NAME.txt>";
			val files = Arrays.asList(fileNameWithExtension);
			readFiles(files, SftpConstants.PAYROLL_SFTP);
		}
	}
	
	//@Scheduled(fixedRate = 600000)
	public void testSftpUtils() {
		if (appSettings.isDev()) {
			val file = new File("<PATH/TO/FILE/FILE.EXTENSION>");
			val is = UFile.fileToInputStream(file);
			val fileName = "<FILE_NAME.EXTENSION>";
			
			sftpUtils.uploadFile(SftpConstants.CFDI_SFTP, fileName, is, SftpConstants.SFTP_OUT_PATH);
			sftpUtils.deleteFile(SftpConstants.CFDI_SFTP, "<FILE_NAME.EXTENSION>", SftpConstants.SFTP_OUT_PATH);
			sftpUtils.downloadFile(SftpConstants.CFDI_SFTP, "<SFTP_FOLDER/FILE_NAME.EXTENSION>", "<PATH/TO/FILE/FILE_NAME.EXTENSION>");
		}
	}
	
	//@Scheduled(fixedRate = 600000)
	public void testUploadFile() {
		if (appSettings.isDev()) {
			val localFilePath = "<PATH/TO/FILE/FILE.EXTENSION>";
			uploadFileToSftp(SftpConstants.SFTP_OUT_PATH, localFilePath, SftpConstants.CFDI_SFTP);
		}
	}

	private void readFiles(final List<String> files, final String sftp) {
		val filesData = bancomextService.sftpFileData(files);
		filesData.getValidFiles().forEach(fileName -> processFile(fileName, sftp));
		filesData.getDuplicateFiles().forEach(fileName -> processDuplicateFile(fileName, sftp));
	}
	
	private void processFile(String fileName, String sftp) {
		try {
			UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT FILE READER SERVICE > " + fileName);
			val fileWrapper = bancomextService.createFile(fileName, ORIGIN);
			if (fileWrapper != null) {
				val bancomextStampResponse = bancomextService.parseAndStamp(fileWrapper);
				if (bancomextStampResponse != null) {
					if (AssertUtils.nonNull(bancomextStampResponse.getZipData())) {
						val zipData = bancomextStampResponse.getZipData();
						// Upload to Sftp/OUT folder
						uploadFileToSftp(zipData, sftp);
						// Upload to S3
						val resourceDataSource = bancomextStampResponse.getResourceDataSource();
						val s3ErrorPath = s3UploadService.uploadToS3(zipData.getFileWrapper(), resourceDataSource);
						uploadS3ErrorFileToSftp(zipData.getFileWrapper(), s3ErrorPath, sftp);
						// Send Stamp Summary Mail
						bancomextStampSummaryService.sendStampSummaryMail(bancomextStampResponse.getStampSummary());
					}
					
					if (bancomextStampResponse.getFileErrorWrapper() != null) {
						uploadFileErrorToSftp(bancomextStampResponse.getFileErrorWrapper(), sftp);
						// Send Stamp Summary Mail
						bancomextStampSummaryService.sendMailForFileContentError(bancomextStampResponse.getFileErrorWrapper());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void processDuplicateFile(String fileName, String sftp) {
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT FILE READER SERVICE > DUPLICATE FILE " + fileName);
		val duplicateFileWrapper = bancomextService.logDuplicateFile(fileName);
		if (duplicateFileWrapper != null) {
			uploadFileErrorToSftp(duplicateFileWrapper, sftp);
			// Send Stamp Summary Mail
			val recipients = SftpConstants.CFDI_SFTP.equals(sftp) 
					? BancomextFileErrorWrapper.CFDI_ERROR_EMAIL
					: BancomextFileErrorWrapper.PAYROLL_ERROR_EMAIL;
			bancomextStampSummaryService.sendMailForDuplicateFileError(fileName, recipients);
		}
	}

	private void uploadFileErrorToSftp(BancomextFileErrorWrapper fileErrorWrapper, String sftp) {
		try {
			if (fileErrorWrapper != null) {
				// Upload to Sftp/ERROR folder
				val zippedFilePath = fileErrorWrapper.getZippedFilePath();
				uploadFileToSftp(SftpConstants.SFTP_ERROR_PATH, zippedFilePath, sftp);
				if (fileErrorWrapper.getFileWrapper() != null
						&& fileErrorWrapper.getFileWrapper().getBancomextFile() != null) {
					// Updating content error path in file
					val sftpErrPath = getSftpFilePath(zippedFilePath, SftpConstants.SFTP_ERROR_PATH);
					fileErrorWrapper.getFileWrapper().getBancomextFile().setRutaErrorContenido(sftpErrPath);
					bancomextService.updateBancomextFile(fileErrorWrapper.getFileWrapper().getBancomextFile());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void uploadFileToSftp(BancomextZipData zipData, String sftp) {
		if (zipData != null) {
			val bancomextFile = zipData.getFileWrapper().getBancomextFile();
			
			val zippedCfdisPath = zipData.getCfdisZippedPath();
			if (zippedCfdisPath != null) {
				uploadFileToSftp(SftpConstants.SFTP_OUT_PATH, zippedCfdisPath, sftp);
				// Updating sftp path in file
				val sfptPath = getSftpFilePath(zippedCfdisPath, SftpConstants.SFTP_OUT_PATH);
				bancomextFile.setRutaDescarga(sfptPath);
				bancomextService.updateBancomextFile(bancomextFile);
			}
			
			val zippedErrorPath = zipData.getErrorZippedPath();
			if (zippedErrorPath != null) {
				uploadFileToSftp(SftpConstants.SFTP_ERROR_PATH, zippedErrorPath, sftp);
				// Updating validation error path in file
				val sftpErrPath = getSftpFilePath(zippedErrorPath, SftpConstants.SFTP_ERROR_PATH);
				bancomextFile.setRutaErrorValidacion(sftpErrPath);
				bancomextService.updateBancomextFile(bancomextFile);
			}
		}
	}
	
	private void uploadFileToSftp(String sftpPath, String zippedFilePath, String sftp) {
		val zippedFile = new File(zippedFilePath);
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT FILE READER SERVICE > FILE " + zippedFile + " WILL BE UPLOAD TO " + sftpPath);
		val zippedFileName = zippedFile.getName();
		val path = String.format(SftpConstants.SFTP_PATH_FORMAT, sftpPath, zippedFileName);
		if (SftpConstants.CFDI_SFTP.equals(sftp)) {
			cfdiSftpService.uploadFile(zippedFilePath, path);
		} else if (SftpConstants.PAYROLL_SFTP.equals(sftp)) {
			payrollSftpService.uploadFile(zippedFilePath, path);
		}
		// Delete file from server
		UFile.deleteFile(zippedFilePath);
	}
	
	public void uploadS3ErrorFileToSftp(BancomextFileWrapper fileWrapper, String s3ErrorPath, String sftp) {
		if (AssertUtils.hasValue(s3ErrorPath)) {
			uploadFileToSftp(SftpConstants.SFTP_ERROR_PATH, s3ErrorPath, sftp);
			// Updating s3 error path in file
			val sftpErrPath = getSftpFilePath(s3ErrorPath, SftpConstants.SFTP_ERROR_PATH);
			fileWrapper.getBancomextFile().setRutaErrorS3(sftpErrPath);
			bancomextService.updateBancomextFile(fileWrapper.getBancomextFile());
		}
	}
	
	private String getSftpFilePath(String filePath, String sftpPath) {
		val fileName = UFile.getFileFullName(filePath);
		return String.format(SftpConstants.SFTP_PATH_FORMAT, sftpPath, fileName);
	}

}