package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.util.concurrent.Future;

import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface BancomextStampSummaryService {
	
	Future<Boolean> sendStampSummaryMail(BancomextStampSummary stampSummary);
	
	Future<Boolean> sendMailForDuplicateFileError(String fileName, String recipients);
	
	Future<Boolean> sendMailForFileContentError(BancomextFileErrorWrapper fileErrorWrapper);
	
}