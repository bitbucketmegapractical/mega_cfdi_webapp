package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextCfdi {
	private ApiCfdi cfdi;
	@Getter(AccessLevel.NONE)
	private List<BancomextInvoice> invoices;
	
	public List<BancomextInvoice> getInvoices() {
		if (invoices == null) {
			invoices = new ArrayList<>();
		}
		return invoices;
	}
}