package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextTreasuryCurrentValueTotal {
	private String totalesTitulos;
	private String totalesPrecio;
	private String totalesImporte;
	private String totalesTasa;
	private String totalesPlazo;
	private String totalesDxv;
	private String totalesPremio;
	private String totalesIsr;
	private String totalesImpNeto;
}