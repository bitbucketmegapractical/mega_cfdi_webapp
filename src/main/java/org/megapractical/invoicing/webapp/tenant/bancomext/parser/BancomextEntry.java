package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class BancomextEntry {
	
	private BancomextEntry() {		
	}
	
	protected static final List<FileEntry> REQUIRED_PAYROLL_ENTRIES = Arrays.asList(
		FileEntry.ENTRY_LOTE, FileEntry.ENTRY_DOC, FileEntry.ENTRY_VOUCHER, FileEntry.ENTRY_PRINT
	);
	
	protected static final List<FileEntry> REQUIRED_CFDI_EDC_ENTRIES = Arrays.asList(
		FileEntry.ENTRY_LOTE, FileEntry.ENTRY_DOC, FileEntry.ENTRY_VOUCHER, FileEntry.ENTRY_ADDRESSES
	);
	
	public static boolean isEqual(String entryValue, FileEntry entry) {
		val cleanEntryValue = removeNonPrintable(entryValue);
		return entry.value.equalsIgnoreCase(cleanEntryValue);
	}
	
	public static boolean isPayrollEntry(String entryValue) {
		val cleanEntryValue = removeNonPrintable(entryValue);
		return Arrays.asList("O", "E").stream().anyMatch(cleanEntryValue::equalsIgnoreCase);
	}
	
	public static String removeNonPrintable(String s) {
		var cleanString = s.replaceAll("\\p{C}", "")
				   		   .replaceAll("\uFEFF", "");

		val pattern = Pattern.compile("[\\000]*");
		val matcher = pattern.matcher(cleanString);
		if (matcher.find()) {
			cleanString = matcher.replaceAll("");
		}
	
		return cleanString;
	}
	
	public enum FileEntry {
		ENTRY_LOTE("Lote"),
	    ENTRY_DOC("DOCUMENTO"),
	    ENTRY_VOUCHER("COMPROBANTE"),
	    ENTRY_RELATED_CFDI("CFDI_RELACIONADOS"),
	    ENTRY_EMITTER("EMISOR"),
	    ENTRY_RECEIVER("RECEPTOR"),
	    ENTRY_CONCEPT("CONCEPTO"),
	    ENTRY_TAX("IMPUESTOS"),
	    ENTRY_TAX_T_NODE("C_IMP_TRASLADADOS"),
	    ENTRY_TAX_T_TYPE("IMP_TRASLADADO"),
	    ENTRY_TAX_TRA_NODE("TRASLADOS"),
	    ENTRY_TAX_R_NODE("C_IMP_RETENCIONES"),
	    ENTRY_TAX_R_TYPE("IMP_RETENIDO"),
	    ENTRY_TAX_RET_NODE("RETENCIONES"),
	    ENTRY_TAX_RET_TYPE("RETENCION"),
	    ENTRY_COMPLEMENT("COMPLEMENTO"),
	    ENTRY_PAYMENT_TAG("Pagos20"),
	    ENTRY_PAYMENT_TOTALS("TOTALES"),
	    ENTRY_PAYMENT("PAGO"),
	    ENTRY_PAYMENT_TRA("TRASLADOP"),
	    ENTRY_PAYMENT_REL_DOC("DOCUMENTORELACIONADO"),
	    ENTRY_PAYMENT_REL_DOC_TRA("TRASLADODR"),
	    ENTRY_PAYROLL("Nomina12"),
	    ENTRY_PAYROLL_PERCEPTIONS("Percepciones"),
	    ENTRY_PAYROLL_PERCEPTION("Percepcion"),
	    ENTRY_PAYROLL_DEDUCTIONS("Deducciones"),
	    ENTRY_PAYROLL_DEDUCTION("Deduccion"),
	    ENTRY_PAYROLL_JPR("JUBILACIONPENSIONRETIRO"),
	    ENTRY_PAYROLL_OTHER_PAYMENT("OTROPAGO"),
	    ENTRY_PAYROLL_EMPLOYMENT_SUBSIDY("SubsidioAlEmpleo"),
	    ENTRY_FOREIGN_EXCHANGE("Divisas"),
	    ENTRY_ADDRESSES("DOMICILIOS"),
		ENTRY_ROW("FILA"),
		ENTRY_ROW_H1("H1"),
		ENTRY_ROW_H2("H2"),
		ENTRY_ROW_ADD01("adicionales01"),
		ENTRY_ROW_GENERAL("General"),
		ENTRY_ROW_ITEM("Partida"),
		ENTRY_ROW_DOCUMENT_INVOICE("DocumentoFactura"),
		ENTRY_ROW_DATE_VALUE("FechaValor"),
		ENTRY_ROW_LINE("renglon"),
		ENTRY_ROW_SUMMARY_DATA("resumendatos"),
		ENTRY_ROW_HEADER("encabezado"),
		ENTRY_ROW_FLOWS("Movimientos"),
		ENTRY_ROW_FLOWS_MONTH("MovimientoMes"),
		ENTRY_ROW_CURRENT_VALUES("ValoresVigentes"),
		ENTRY_ROW_GUARANTEE_VALUES("ValoresVigentesGarant"),
		ENTRY_ROW_SALES_OPERATIONS("OperacionesVenta"),
		ENTRY_ROW_SALES_TRANSACTIONS_SUMMARY("ResumenMovimientosVenta"),
		ENTRY_ROW_CUSTOMER("Cliente"),
		ENTRY_ROW_RETRIEVED("Recuperado"),
		ENTRY_ROW_DISPOSITIONS("disposiciones"),
		ENTRY_ROW_FEES("Comisiones"),
		ENTRY_PRINT("Impresion"),
		ENTRY_SENT_RECEIVER("ENVIO_RECEPTOR");

	    private final String value;

	    FileEntry(String value) {
	        this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }
	}
	
}