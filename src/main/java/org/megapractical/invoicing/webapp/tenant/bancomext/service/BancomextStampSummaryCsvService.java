package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.util.List;

import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary.StampSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface BancomextStampSummaryCsvService {
	
	void createStampSummaryCsvFile(BancomextFileWrapper fileWrapper, List<StampSummary> stampSummarySource);
	
}