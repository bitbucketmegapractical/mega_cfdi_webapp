package org.megapractical.invoicing.webapp.tenant.bancomext.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
public class BancomextSftpException extends RuntimeException {
	
	public BancomextSftpException(String message) {
		super(message);
	}

}