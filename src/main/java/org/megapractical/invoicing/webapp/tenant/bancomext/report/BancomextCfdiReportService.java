package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import static java.util.stream.Collectors.toList;
import static org.megapractical.invoicing.api.util.MoneyUtils.format;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.megapractical.invoicing.api.report.ReportManagerTools;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayment;
import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.report.payload.ReportParam;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAddress;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextInvoice;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextSerie;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class BancomextCfdiReportService {
	
	private final ReportManager reportManager;
	private final UserTools userTools;
	
	public boolean generatePdf(Area area, BancomextStampData stampData) {
		try {
			val cfdi = stampData.getCfdi();
			val reportData = BancomextReport.getReportDataBySerie(area, cfdi.getVoucher().getSerie());
			if (reportData != null) {
				val template = BancomextCfdiReportService.class.getResourceAsStream(reportData.getTemplatePath());
				
				val parameters = new HashMap<String, Object>();
				parameters.put("template", template);
				
				val pdfType = reportData.getPdfType();
				if (BancomextPdfType.CFDI.equals(pdfType)) {
					populateCfdiData(stampData, parameters);
				} else if (BancomextPdfType.PAYMENT.equals(pdfType)) {
					populatePaymentData(stampData, parameters);
				}
				
				val exported = reportManager.exportReportToPdf(template, parameters, cfdi.getPdfPath());
				if (exported) {
					userTools.log("[INFO] BANCOMEXT CFDI REPORT - PDF HAS BEEN GENERATED SUCCESSFULLY");
				}
				return exported;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private ReportParam populateCfdiData(BancomextStampData stampData, final Map<String, Object> parameters) {
		val cfdi = stampData.getCfdi();
		val invoices = stampData.getInvoices();
		populateCommonData(cfdi,  parameters);
		setInvoiceData(invoices, parameters);
		setTaxesData(cfdi, parameters);
		setAdditionalsData(cfdi, parameters);
		setConditionalData(cfdi, parameters);
		return new ReportParam(parameters);
	}
	
	private ReportParam populatePaymentData(BancomextStampData stampData, final Map<String, Object> parameters) {
		val cfdi = stampData.getCfdi();
		val payment = cfdi.getComplement().getApiPayment();
		populateCommonData(cfdi, parameters);
		parameters.put("totalLetras", "CERO 0/100");
		setPaymentsData(payment, parameters);
		setPaymentTransferredData(payment, parameters);
		setPaymentRelatedDocsData(payment, parameters);
		setPaymentTotalsData(payment, parameters);
		setPaymentAdditionals(parameters);
		return new ReportParam(parameters);
	}
	
	private void populateCommonData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		setHeaderData(cfdi, parameters);
		setConceptData(cfdi, parameters);
		setVoucherData(cfdi, parameters);
		setStampData(cfdi, parameters);
	}

	private final void setHeaderData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val stampResponse = cfdi.getStampResponse();
		val voucher = stampResponse.getVoucher();
		val receiver = cfdi.getReceiver();
		val additionals = cfdi.getAdditionals();
		
		parameters.put("logo", BancomextReport.getVerticalLogo());
		parameters.put("rfcReceptor", receiver.getRfc());
		parameters.put("razonSocialReceptor", receiver.getNombreRazonSocial());
		parameters.put("regimenFiscalReceptor", additionals.get(BancomextAdditional.RECEIVER_TAX_REGIME));
		parameters.put("usoCfdi", additionals.get(BancomextAdditional.RECEIVER_CFDI_USE));
		parameters.put("serieFolio", voucher.getSerie() + " " + voucher.getFolio());
		parameters.put("lugarFechaExpedicion", voucher.getLugarExpedicion() + ", " + voucher.getFecha());
		parameters.put("fechaHoraEmision", stampResponse.getDateExpedition());
		parameters.put("fechaHoraCertificacion", stampResponse.getDateStamp());
		parameters.put("noCertificadoEmisor", stampResponse.getCertificateEmitterNumber());
		parameters.put("noCertificadoSat", stampResponse.getCertificateSatNumber());
		parameters.put("folioFiscal", stampResponse.getUuid().toUpperCase());
		
		val address = (BancomextAddress) additionals.get(BancomextAdditional.RECEIVER_ADDRESS);
		if (address != null) {
			parameters.put("addressLine1", address.getLine1());
			parameters.put("addressLine2", address.getLine2());
			parameters.put("addressLine3", address.getLine3());
			parameters.put("addressLine4", address.getLine4());
			parameters.put("addressLine5", address.getLine5());
		}
	}
	
	private final void setConceptData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val conceptReportList = cfdi.getConcepts().stream()
												  .map(BancomextConceptReport::convert)
												  .collect(toList());
		parameters.put("conceptos", conceptReportList);
	}
	
	private void setInvoiceData(List<BancomextInvoice> invoices, Map<String, Object> parameters) {
		if (invoices != null && !invoices.isEmpty()) {
			parameters.put("facturas", invoices);
		}
	}
	
	private void setTaxesData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val transferreds = cfdi.getTransferred().stream()
												.map(BancomextTaxReport::convert)
												.collect(toList());
		parameters.put("impuestos", transferreds);
	}
	
	private void setVoucherData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val voucher = cfdi.getVoucher();
		
		String iva16 = null;
		if (voucher.getTaxes() != null) {
			iva16 = format(voucher.getTaxes().getTotalTaxTransferred());
		}
		
		parameters.put("totalLetras", voucher.getTotalInLetter());
		parameters.put("subtotal", voucher.getSubTotalString());
		parameters.put("iva16", iva16);
		parameters.put("total", voucher.getTotalString());
		parameters.put("metodoPago", voucher.getPaymentMethodString());
		parameters.put("formaPago", voucher.getPaymentWayString());
		parameters.put("moneda", voucher.getCurrencyString());
		parameters.put("tipoComprobante", voucher.getVoucherTypeString());
		parameters.put("exportacion", voucher.getExportationString());
		
		if (voucher.getRelatedCfdi() != null) {
			val uuids = voucher.getRelatedCfdi()
							   .getUuids()
							   .stream()
							   .map(BancomextRelatedCfdiReport::new)
							   .collect(toList());
					
			parameters.put("tipoRelacion", voucher.getRelatedCfdi().getRelationshipTypeValue());
			parameters.put("docRelacionados", uuids);
		}
	}
	
	private void setAdditionalsData(ApiCfdi cfdi, Map<String, Object> parameters) {
		val additionals = cfdi.getAdditionals();
		parameters.put("negociacion", additionals.get(BancomextAdditional.NEGOCIATION));
		parameters.put("fechaValor", additionals.get(BancomextAdditional.DATE_VALUE));
	}
	
	private void setStampData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val stampResponse = cfdi.getStampResponse();
		parameters.put("qrImagen", ReportManagerTools.getQR(stampResponse));
		parameters.put("selloDigitalSat", stampResponse.getSatSeal());
		parameters.put("selloDigitalEmisor", stampResponse.getEmitterSeal());
		parameters.put("cadenaOriginal", stampResponse.getOriginalString());
	}
	
	private void setConditionalData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val serie = cfdi.getVoucher().getSerie();
		if (BancomextSerie.CRED_FAC.getValue().equalsIgnoreCase(serie)) {
			val keyProductService = cfdi.getConcepts().get(0).getKeyProductService();
			val measurementUnit = cfdi.getConcepts().get(0).getMeasurementUnitPrint();
			
			parameters.put("claveProductoServicio", keyProductService);
			parameters.put("cantidad", "1");
			parameters.put("unidadMedida", measurementUnit);
		}
	}
	
	private void setPaymentsData(ApiPayment apiPayment, Map<String, Object> parameters) {
		val payments = apiPayment.getPayments().stream()
				  							   .map(BancomextPaymentReport::convert)
				  							   .collect(toList());
		val payment = apiPayment.getPayments().get(0);
		
		parameters.put("pagos", payments);
		parameters.put("rfcEmisorCtaOrdenante", payment.getSourceAccountRfc());
		parameters.put("bancoOrdenanteExtranjero", payment.getBankName());
		parameters.put("cuentaOrdenante", payment.getPayerAccount());
		parameters.put("rfcEntidadOperadora", payment.getTargetAccountRfc());
		parameters.put("cuentaBeneficiario", payment.getReceiverAccount());
	}
	
	private void setPaymentTransferredData(ApiPayment apiPayment, Map<String, Object> parameters) {
		val transferreds = apiPayment.getPayments().stream()
												   .flatMap(i -> i.getTransferreds().stream())
												   .map(BancomextTaxReport::convert)
												   .collect(toList());
		parameters.put("impuestos", transferreds);
	}
	
	private void setPaymentRelatedDocsData(ApiPayment apiPayment, Map<String, Object> parameters) {
		val relDocs = apiPayment.getPayments()
								.get(0)
								.getRelatedDocuments()
								.stream()
								.map(BancomextRelatedDocReport::convert)
								.collect(toList());
		parameters.put("docRelacionados", relDocs);
	}
	
	private void setPaymentTotalsData(ApiPayment payment, Map<String, Object> parameters) {
		val paymentTotal = payment.getPaymentTotal();
		parameters.put("totalPagos", paymentTotal.getTotalAmountPaymentsString());
		parameters.put("totalTrasladosBaseIva", paymentTotal.getTotalTransferredBaseIva16String());
		parameters.put("totalTrasladosImpIva", paymentTotal.getTotalTransferredTaxIva16String());
	}
	
	private void setPaymentAdditionals(Map<String, Object> parameters) {
		parameters.put("tipoRelacion", null);
		parameters.put("uuidREP", null);
	}
	
}