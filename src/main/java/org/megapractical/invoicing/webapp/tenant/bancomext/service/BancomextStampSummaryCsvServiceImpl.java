package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.io.FileWriter;
import java.util.List;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary.StampSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.util.BancomextUtils;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
public class BancomextStampSummaryCsvServiceImpl implements BancomextStampSummaryCsvService {

	@Override
	public void createStampSummaryCsvFile(BancomextFileWrapper fileWrapper, List<StampSummary> stampSummarySource) {
		if (AssertUtils.nonNull(fileWrapper) && !AssertUtils.isEmpty(stampSummarySource)) {
			try {
				val absolutePath = fileWrapper.getUploadFilePath().getAbsolutePath();
				val csvFile = fileWrapper.getFileDetail().getFileName() + "_RESUMEN_PROCESAMIENTO.csv";
				val csvFilePath = absolutePath + BancomextUtils.SEPARATOR + csvFile;
				UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT STAMP SUMMARY CSV SERVICE > CREATING FILE " + csvFilePath);
				
				try (val writer = new CSVWriter(new FileWriter(csvFilePath), ICSVWriter.DEFAULT_SEPARATOR,
						ICSVWriter.NO_QUOTE_CHARACTER, ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END)) {
					val headers = new String[] { 
						"RFC Emisor", "RFC Receptor", "Serie", "Folio", "UUID", "Estado",
						"Secuencial", "Mensaje de Error", "ID Control", "Archivo" 
					};
					writer.writeNext(headers);
					
					stampSummarySource.forEach(data -> {
						val csvData = new String[] {
							data.getEmitterRfc(),
							data.getReceiverRfc(),
							data.getSerie(),
							data.getFolio(),
							data.getUuid(),
							data.getStatus().name(),
							String.valueOf(data.getSequence()),
							data.getErrorMessage(),
							data.getControlId(),
							data.getXmlName()
						};
						writer.writeNext(csvData);
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}