package org.megapractical.invoicing.webapp.tenant.bancomext.mail;

import java.util.Arrays;
import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.ResourceData;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;

import lombok.val;

public final class BancomextMailUtils {
	
	private BancomextMailUtils() {
	}
	
	public static BancomextMailData populateMailData(ResourceData resourceData, ApiCfdi apiCfdi) {
		if (apiCfdi != null) {
			return BancomextMailData
					.builder()
					.resourceData(resourceData)
					.emitterRfc(apiCfdi.getEmitter().getRfc())
					.emitterName(apiCfdi.getEmitter().getNombreRazonSocial())
					.receiverRfc(apiCfdi.getReceiver().getRfc())
					.serie(apiCfdi.getVoucher().getSerie())
					.folio(apiCfdi.getVoucher().getFolio())
					.uuid(apiCfdi.getStampResponse().getUuid())
					.recipients(getRecipients(apiCfdi))
					.xmlFileName(resourceData.getXmlFileName())
					.xmlPath(resourceData.getXmlPath())
					.pdfFileName(resourceData.getPdfFileName())
					.pdfPath(resourceData.getPdfPath())
					.subject(BancomextMailConstants.SUBJECT_DOC)
					.build();
		}
		return null;
	}
	
	private static List<String> getRecipients(ApiCfdi apiCfdi) {
		val source = apiCfdi.getAdditionals().get(BancomextAdditional.EMAIL).toString().split(";");
		return Arrays.asList(source);
	}
	
}