package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextForeignExchangeSaleReport;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextForeignExchange {
	@Getter(AccessLevel.NONE)
	private BancomextForeignExchangeData data;
	@Getter(AccessLevel.NONE)
	private List<BancomextForeignExchangeSaleReport> sales;
	private BancomextForeignExchangeTransactionSummary transactionSummary;
	
	public BancomextForeignExchangeData getData() {
		if (data == null) {
			data = new BancomextForeignExchangeData();
		}
		return data;
	}
	
	public List<BancomextForeignExchangeSaleReport> getSales() {
		if (sales == null) {
			sales = new ArrayList<>();
		}
		return sales;
	}
	
}