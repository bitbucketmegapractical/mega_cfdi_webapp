package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.xml.XmlTransformer;
import org.megapractical.invoicing.api.xml.XmlTransformerCfdiResponse;
import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextParser;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextParseData;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCfdiReportService;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * Use this service class only in development environment
 * 
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class BancomextGeneratePdfService {
	
	private final AppSettings appSettings;
	private final UserTools userTools;
	private final BancomextParser bancomextParser;
	private final BancomextCfdiReportService bancomextCfdiReportService;
	private final BancomextAreaService bancomextAreaService;
	private final ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	
	private ContribuyenteEntity taxpayer;
	private ContribuyenteCertificadoEntity certificate;
	
	@PostConstruct
	public void init() {
		val bancomextRfc = appSettings.getPropertyValue("bancomext.rfc");
		this.taxpayer = userTools.getContribuyenteActive(bancomextRfc);
		this.certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
	}
	
	//@Scheduled(fixedRate = 600000)
	public void generatePdfFromXml() {
		if (appSettings.isDev()) {
			val files = Arrays.asList(
				"PATH/TO/FILE/FILE_NAME.txt"
			);
			files.forEach(filePath -> {
				val file = new File(filePath);
				val xmlsPath = Paths.get(file.getParentFile().toString());
				
				// Procesando los xmls
				val transformedXmls = processXmlFiles(xmlsPath);
				UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT GENERATE PDF SERVICE > TOTAL XMLS TRANSFORMED: " + transformedXmls.size());
				
				// Parseando el archivo
				val bancomextParseData = bancomextParser.parse(file, certificate);
				
				if (AssertUtils.nonNull(transformedXmls) && AssertUtils.nonNull(bancomextParseData)) {
					val stampDataSource = processData(transformedXmls, bancomextParseData);
					generatePdfs(stampDataSource);
				}
			});
		}
	}

	private List<BancomextStampData> processData(List<CfdiFromXml> transformedXmls,
												 BancomextParseData bancomextParseData) {
		val stampDataSource = new ArrayList<BancomextStampData>();
		for (val item : bancomextParseData.getBancomextCfdis()) {
			val apiCfdi = item.getCfdi();
			for (val cfdiFromXml : transformedXmls) {
				val data = cfdiFromXml.getData();
				val voucher = data.getVoucher();
				if (voucher.getSerie().equals(apiCfdi.getVoucher().getSerie())
						&& voucher.getFolio().equals(apiCfdi.getVoucher().getFolio())) {
					apiCfdi.setStampResponse(data.getStampResponse());
					apiCfdi.setXmlPdfName(cfdiFromXml.getXmlPath());
					
					val dataSource = new BancomextStampData(apiCfdi, item.getInvoices(), voucher);
					stampDataSource.add(dataSource);
				}
			}
		}
		return stampDataSource;
	}
	
	private void generatePdfs(List<BancomextStampData> stampDataSource) {
		stampDataSource.forEach(data -> {
			Area area = null;
			if (stampDataSource != null && !stampDataSource.isEmpty()) {
				val serie = stampDataSource.get(0).getVoucher().getSerie().replace("vdz", "");
				area = bancomextAreaService.findBySerie(serie);
			}
			
			// Generate and storing PDF
			val pdfPath = data.getCfdi().getXmlPdfName().replace(".xml", ".pdf");
			UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT GENERATE PDF SERVICE > GENERATING PDF " + pdfPath);
			data.getCfdi().setPdfPath(pdfPath);
			bancomextCfdiReportService.generatePdf(area, data);
		});
	}
	
    private List<CfdiFromXml> processXmlFiles(Path xmlPath) {
    	val targetList = new ArrayList<CfdiFromXml>();
    	try (val paths = Files.walk(xmlPath)) {
	        paths.filter(Files::isRegularFile)
	             .filter(path -> path.toString().toLowerCase().endsWith(".xml"))
	             .map(this::mapToCfdiFromXml)
	             .forEach(targetList::add);
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
    	return targetList;
    }
    
    private CfdiFromXml mapToCfdiFromXml(Path xmlPath) {
    	val data = XmlTransformer.transformToCfdi(xmlPath.toString());
    	return new CfdiFromXml(xmlPath.toString(), data);
    }
    
    @Data
    @AllArgsConstructor
    static class CfdiFromXml {
    	private String xmlPath;
    	private XmlTransformerCfdiResponse data;
    }
	
}