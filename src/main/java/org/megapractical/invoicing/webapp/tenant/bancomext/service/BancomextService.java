package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.util.List;

import org.megapractical.invoicing.dal.bean.datatype.Origen;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextSftpFileData;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampResponse;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface BancomextService {
	
	BancomextSftpFileData sftpFileData(List<String> sftpFiles);
	
	BancomextFileWrapper createFile(String fileName, Origen origen);
	
	ArchivoStfpEntity updateBancomextFile(ArchivoStfpEntity bancomextFile);
	
	BancomextFileErrorWrapper logDuplicateFile(String fileName);
	
	BancomextStampResponse parseAndStamp(BancomextFileWrapper fileWrapper);
	
}