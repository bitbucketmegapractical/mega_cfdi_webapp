package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCreditDispositionReport;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCreditFeeReport;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCreditRetrievedReport;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextCredit {
	@Getter(AccessLevel.NONE)
	private BancomextCreditData data;
	@Getter(AccessLevel.NONE)
	private List<BancomextCreditRetrievedReport> retrieveds;
	@Getter(AccessLevel.NONE)
	private List<BancomextCreditDispositionReport> dispositions;
	@Getter(AccessLevel.NONE)
	private List<BancomextCreditFeeReport> fees;

	public BancomextCreditData getData() {
		if (data == null) {
			data = new BancomextCreditData();
		}
		return data;
	}

	public List<BancomextCreditRetrievedReport> getRetrieveds() {
		if (retrieveds == null) {
			retrieveds = new ArrayList<>();
		}
		return retrieveds;
	}

	public List<BancomextCreditDispositionReport> getDispositions() {
		if (dispositions == null) {
			dispositions = new ArrayList<>();
		}
		return dispositions;
	}

	public List<BancomextCreditFeeReport> getFees() {
		if (fees == null) {
			fees = new ArrayList<>();
		}
		return fees;
	}
	
}