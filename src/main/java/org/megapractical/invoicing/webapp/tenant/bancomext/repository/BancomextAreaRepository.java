package org.megapractical.invoicing.webapp.tenant.bancomext.repository;

import java.util.Optional;

import org.megapractical.invoicing.dal.bean.jpa.BancomextAreaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BancomextAreaRepository extends JpaRepository<BancomextAreaEntity, Long> {
	
	Optional<BancomextAreaEntity> findBySerieIgnoreCaseAndEliminadoFalse(String serie);
	
}