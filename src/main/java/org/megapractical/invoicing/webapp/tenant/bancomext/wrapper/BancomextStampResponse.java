package org.megapractical.invoicing.webapp.tenant.bancomext.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.webapp.exposition.cfdi.payload.ResourceData;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextStampResponse {
	@Getter(AccessLevel.NONE)
	private List<ResourceData> resourceDataSource;
	private BancomextZipData zipData;
	private BancomextStampSummary stampSummary;
	private BancomextFileErrorWrapper fileErrorWrapper;

	public BancomextStampResponse(List<ResourceData> resourceDataSource, BancomextZipData zipData,
			BancomextStampSummary stampSummary) {
		this.resourceDataSource = resourceDataSource;
		this.zipData = zipData;
		this.stampSummary = stampSummary;
	}

	/* Getters */
	public List<ResourceData> getResourceDataSource() {
		if (resourceDataSource == null) {
			resourceDataSource = new ArrayList<>();
		}
		return resourceDataSource;
	}

}