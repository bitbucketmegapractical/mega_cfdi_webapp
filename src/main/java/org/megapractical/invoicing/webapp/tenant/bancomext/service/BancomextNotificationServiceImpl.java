package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import static org.megapractical.common.validator.AssertUtils.nonNull;

import java.time.LocalDateTime;

import org.megapractical.invoicing.dal.bean.jpa.BancomextNotification;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextMailData;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.repository.BancomextNotificationRepository;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class BancomextNotificationServiceImpl implements BancomextNotificationService {

	private final BancomextNotificationRepository bancomextNotificationRepository;

	@Override
	public BancomextNotification save(BancomextMailData mailData, String reason) {
		val bn = mapToEntity(mailData, reason);
		return save(bn);
	}

	@Override
	public BancomextNotification save(BancomextStampSummary stampSummary, String reason) {
		val bn = mapToEntity(stampSummary, reason);
		return save(bn);
	}

	private BancomextNotification save(BancomextNotification bn) {
		if (bn != null) {
			return bancomextNotificationRepository.save(bn);
		}
		return null;
	}

	private BancomextNotification mapToEntity(BancomextMailData mailData, String reason) {
		if (nonNull(mailData)) {
			val bn = new BancomextNotification();
			bn.setRecipient(mailData.getRecipient());
			bn.setReceiverRfc(mailData.getReceiverRfc());
			bn.setSerie(mailData.getSerie());
			bn.setFolio(mailData.getFolio());
			bn.setUuid(mailData.getUuid());
			bn.setXmlFileName(mailData.getXmlFileName());
			bn.setXmlPath(mailData.getXmlPath());
			bn.setPdfFileName(mailData.getPdfFileName());
			bn.setPdfPath(mailData.getPdfPath());
			bn.setSendDate(LocalDateTime.now());
			bn.setReason(reason);
			bn.setSubject(mailData.getSubject());
			return bn;
		}
		return null;
	}

	private BancomextNotification mapToEntity(BancomextStampSummary stampSummary, String reason) {
		if (nonNull(stampSummary)) {
			val bn = new BancomextNotification();
			bn.setRecipient(stampSummary.getRecipient());
			bn.setFileName(stampSummary.getFileName());
			bn.setSendDate(LocalDateTime.now());
			bn.setReason(reason);
			bn.setSubject(stampSummary.getSubject());
			return bn;
		}
		return null;
	}

}