package org.megapractical.invoicing.webapp.tenant.bancomext.service;

public interface BancomextSftpService {
	
	public void loadCfdiFiles();
	
	public void loadPayrollFiles();
	
}