package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextCreditData {
	private String period;
	private String periodDays;
	private String customerNumber;
	private String creditLine;
	private String currency;
	private String authorizedAmount;
	private String availableAmount;
	private String currencyCapitalBalance;
	private String openingBalance;
	private String disposedAmount;
	private String recoveredPrincipalAmount;
	private String outstandingPrincipalBalance;
	private String totalFeesCollected;
}