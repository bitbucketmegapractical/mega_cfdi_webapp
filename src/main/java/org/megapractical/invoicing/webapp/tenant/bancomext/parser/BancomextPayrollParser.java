package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

import static org.megapractical.invoicing.api.util.MoneyUtils.format;
import static org.megapractical.invoicing.api.util.MoneyUtils.formatSafe;
import static org.megapractical.invoicing.api.util.MoneyUtils.formatStrict;
import static org.megapractical.invoicing.api.util.MoneyUtils.fullFormatStrict;
import static org.megapractical.invoicing.api.util.UValue.stringValue;
import static org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.isEqual;
import static org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.isPayrollEntry;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.common.datetime.DateUtils;
import org.megapractical.common.datetime.LocalDateUtils;
import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction.PayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollEmitter;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment.EmploymentSubsidy;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPensionRetirement;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception;
import org.megapractical.invoicing.api.wrapper.ApiPayrollReceiver;
import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.dal.bean.datatype.StorageType;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.webapp.exposition.sat.catalog.SatCatalog;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.FileEntry;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextParseData;
import org.megapractical.invoicing.webapp.tenant.bancomext.service.BancomextAreaService;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;
import org.megapractical.invoicing.webapp.util.EntryUtils;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class BancomextPayrollParser {
	
	private final SatCatalog satCatalog;
	private final BancomextCommonParser bancomextCommonParser;
	private final BancomextAreaService bancomextAreaService;
	
	public BancomextParseData parse(List<String[]> entries, ContribuyenteCertificadoEntity certificate) {
		ApiPayroll apiPayroll = null;
		ApiCfdi cfdi = null;
		val payrolls = new ArrayList<ApiPayroll>();
		bancomextCommonParser.setCertificate(certificate);
		String summaryEmailInfo = null;
		String entry = null;
		val parserErrors = new ArrayList<String>();
		int line = 1;
		Area area = null;
		
		try {
			BancomextEntryValidator.validateEntries(BancomextEntry.REQUIRED_PAYROLL_ENTRIES, entries, parserErrors);
			if (parserErrors.isEmpty()) {
				for (int i = 0; i < entries.size(); i++) {
					line ++;
					
					val item = entries.get(i);
					if (item.length > 0) {
						entry = item[0];
						if (isEqual(item[0], FileEntry.ENTRY_LOTE)) {
							summaryEmailInfo = stringValue(item[item.length - 1]);
						}
						
						if (isEqual(item[0], FileEntry.ENTRY_DOC)) {
							cfdi = new ApiCfdi();
							
							if (EntryUtils.contains(item, FileEntry.ENTRY_SENT_RECEIVER.getValue())) {
								val email = stringValue(item[9]);
								bancomextCommonParser.addAdditional(cfdi, BancomextAdditional.EMAIL, email);
							}
							
							val xmlPdfName = stringValue(item[item.length - 1]);
							apiPayroll = new ApiPayroll();
							apiPayroll.setXmlPdfName(xmlPdfName);
							cfdi.setXmlPdfName(xmlPdfName);
						}
						
						if (isEqual(item[0], FileEntry.ENTRY_VOUCHER) && area == null) {
							area = bancomextAreaService.findBySerie(stringValue(item[2]));
						}
						
						populatePayroll(cfdi, apiPayroll, item);
						if (apiPayroll != null && cfdi != null && isEqual(item[0], FileEntry.ENTRY_PRINT)) {
							apiPayroll.setApiCfdi(cfdi);
							payrolls.add(apiPayroll);
						}
					}
				}
			} else {
				summaryEmailInfo = BancomextFileErrorWrapper.PAYROLL_ERROR_EMAIL;
			}
		} catch (Exception e) {
			UPrint.logWithLine(UProperties.env(), "[ERROR] BANCOMEXT PAYROLL PARSER > AN ERROR OCCURRED WHILE PARSING THE FILE");
			parserErrors.add(BancomextParseData.determineErrorCause(line, entry, e.getMessage()));
			summaryEmailInfo = BancomextFileErrorWrapper.PAYROLL_ERROR_EMAIL;
			e.printStackTrace();
		}
		
		return BancomextParseData
				.builder()
				.area(area)
				.summaryEmailInfo(summaryEmailInfo)
				.payrolls(payrolls)
				.parserErrors(parserErrors)
				.build();
	}
	
	private void populatePayroll(final ApiCfdi cfdi, final ApiPayroll apiPayroll, String[] entry) {
		bancomextCommonParser.fillVoucher(cfdi, entry);
		bancomextCommonParser.fillEmitter(cfdi, entry);
		bancomextCommonParser.fillReceiver(cfdi, entry);
		fillConcept(cfdi, entry);
		if (isEqual(entry[0], FileEntry.ENTRY_COMPLEMENT) && isEqual(entry[1], FileEntry.ENTRY_PAYROLL)) {
			fillPayroll(apiPayroll, entry);
			fillPayrollEmitter(apiPayroll, entry);
			fillPayrollReceiver(apiPayroll, entry);
			fillPayrollPerceptions(apiPayroll, entry);
			fillPayrollPerception(apiPayroll, entry);
			fillPayrollRetirementPension(apiPayroll, entry);
			fillPayrollDeductions(apiPayroll, entry);
			fillPayrollDeduction(apiPayroll, entry);
			fillPayrollOtherPayments(apiPayroll, entry);
			fillEmploymentSubsidy(apiPayroll, entry);
		}
	}

	private void fillConcept(final ApiCfdi cfdi, String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_CONCEPT)) {
			val concept = new ApiConcept();
			concept.setKeyProductServiceCode(stringValue(entry[1]));
			concept.setIdentificationNumber(stringValue(entry[2]));
			concept.setQuantity(formatStrict(stringValue(entry[3])));
			concept.setQuantityString(stringValue(entry[3]));
			concept.setMeasurementUnitCode(stringValue(entry[4]));
			concept.setUnit(stringValue(entry[5]));
			concept.setDescription(stringValue(entry[6]));
			concept.setUnitValue(formatStrict(stringValue(entry[7])));
			concept.setUnitValueString(format(stringValue(entry[7])));
			concept.setAmount(formatStrict(stringValue(entry[8])));
			concept.setAmountString(format(stringValue(entry[8])));
			
			if (!UValidator.isNullOrEmpty(stringValue(entry[9]))) {
				val discount = UValue.bigDecimalStrict(stringValue(entry[9]));
				if (discount.compareTo(BigDecimal.ZERO) != 0) {
    				concept.setDiscount(discount);
    				concept.setDiscountString(UValue.bigDecimalString(stringValue(entry[9])));
				}
			}
			
			concept.setObjImp(stringValue(entry[10]));
			cfdi.getConcepts().add(concept);
		}
	}
	
	private void fillPayroll(final ApiPayroll apiPayroll, String[] entry) {
		if (isPayrollEntry(entry[2])) {
			val payrollType = stringValue(entry[2]);
			apiPayroll.setPayrollType(satCatalog.getPayrollTypeEntity(payrollType));
			apiPayroll.setPayrollTypeCode(payrollType);
			apiPayroll.setPayrollTypePrint(satCatalog.getPayrollTypePrint(payrollType));
			
			val paymentDate = stringValue(entry[3]);
			apiPayroll.setPaymentDate(LocalDateUtils.parse(paymentDate));
			apiPayroll.setPaymentDateString(DateUtils.customDashFormat(paymentDate));
			
			val paymentIntialDate = stringValue(entry[4]);
			apiPayroll.setPaymentIntialDate(LocalDateUtils.parse(paymentIntialDate));
			apiPayroll.setPaymentIntialDateString(DateUtils.customDashFormat(paymentIntialDate));
			
			val paymentEndDate = stringValue(entry[5]);
			apiPayroll.setPaymentEndDate(LocalDateUtils.parse(paymentEndDate));
			apiPayroll.setPaymentEndDateString(DateUtils.customDashFormat(paymentEndDate));
			
			apiPayroll.setNumberDaysPaid(stringValue(entry[6]));
			
			if (AssertUtils.hasValue(entry[7])) {
				apiPayroll.setTotalPerceptions(format((entry[7])));
			}
			
			if (AssertUtils.hasValue(entry[8])) {
				apiPayroll.setTotalDeductions(format((entry[8])));
			}
			
			if (AssertUtils.hasValue(entry[9])) {
				apiPayroll.setTotalOtherPayments(format((entry[9])));
			}
			apiPayroll.setStorageType(StorageType.S3);
		}
	}
	
	private void fillPayrollEmitter(final ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_EMITTER)) {
			val payrollEmitter = new ApiPayrollEmitter();
			payrollEmitter.setEmployerRegistration(stringValue(entry[4]));
			payrollEmitter.setRfcPatronOrigin(stringValue(entry[5]));
			apiPayroll.setPayrollEmitter(payrollEmitter);
		}
	}
	
	private void fillPayrollReceiver(final ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_RECEIVER)) {
			val payrollReceiver = new ApiPayrollReceiver();
			payrollReceiver.setCurp(stringValue(entry[3]));
			payrollReceiver.setSocialSecurityNumber(stringValue(entry[4]));
			
			val startDateEmploymentRelationship = stringValue(entry[5]);
			if (!UValidator.isNullOrEmpty(startDateEmploymentRelationship)) {
				payrollReceiver.setStartDateEmploymentRelationship(LocalDateUtils.parse(startDateEmploymentRelationship));
				payrollReceiver.setStartDateEmploymentRelationshipString(DateUtils.customDashFormat(startDateEmploymentRelationship));
			}
			
			payrollReceiver.setAge(stringValue(entry[6]));
			
			val contractType = stringValue(entry[7]);
			payrollReceiver.setContractTypeCode(contractType);
			payrollReceiver.setContractTypePrint(satCatalog.getContractTypePrint(contractType));
			
			payrollReceiver.setUnionized(stringValue(entry[8]));
			
			val dayType = stringValue(entry[9]);
			payrollReceiver.setDayTypeCode(dayType);
			payrollReceiver.setDayTypePrint(satCatalog.getDayTypePrint(dayType));
			
			val regimeType = stringValue(entry[10]);
			payrollReceiver.setRegimeTypeCode(regimeType);
			payrollReceiver.setRegimeTypePrint(satCatalog.getRegimeTypePrint(regimeType));
			
			payrollReceiver.setEmployeeNumber(stringValue(entry[11]));
			payrollReceiver.setDepartment(stringValue(entry[12]));
			payrollReceiver.setJobTitle(stringValue(entry[13]));
			
			val jobRisk = stringValue(entry[14]);
			payrollReceiver.setJobRiskCode(jobRisk);
			payrollReceiver.setJobRiskPrint(satCatalog.getJobRiskPrint(jobRisk));
			
			val paymentFrequency = stringValue(entry[15]);
			payrollReceiver.setPaymentFrequencyCode(paymentFrequency);
			payrollReceiver.setPaymentFrequencyPrint(satCatalog.getPaymentFrequencyPrint(paymentFrequency));
			
			val bank = stringValue(entry[16]);
			payrollReceiver.setBankCode(bank);
			payrollReceiver.setBankPrint(satCatalog.getBankPrint(bank));
			
			payrollReceiver.setBankAccount(stringValue(entry[17]));
			payrollReceiver.setShareContributionBaseSalary(fullFormatStrict(stringValue(entry[18])));
			payrollReceiver.setIntegratedDailySalary(format((entry[19])));
			
			val federalEntityCode = stringValue(entry[20]);
			payrollReceiver.setFederalEntityCode(federalEntityCode);
			payrollReceiver.setFederalEntityPrint(satCatalog.getFederalEntityPrint(federalEntityCode));
			
			apiPayroll.setPayrollReceiver(payrollReceiver);
		}
	}
	
	private void fillPayrollPerceptions(final ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYROLL_PERCEPTIONS)) {
			val apiPayrollPerception = new ApiPayrollPerception();
			apiPayrollPerception.setTotalSalaries(format((entry[3])));
			apiPayrollPerception.setTotalSeparationCompensation(format((entry[4])));
			apiPayrollPerception.setTotalPensionRetirement(format((entry[5])));
			apiPayrollPerception.setTotalTaxed(format((entry[6])));
			apiPayrollPerception.setTotalExempt(format((entry[7])));
			
			apiPayroll.setPayrollPerception(apiPayrollPerception);
		}
	}

	private void fillPayrollPerception(final ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYROLL_PERCEPTION)) {
			val payrollPerception = new PayrollPerception();
			
			val perceptionType = stringValue(entry[3]);
			payrollPerception.setPerceptionTypeCode(perceptionType);
			payrollPerception.setPerceptionTypePrint(satCatalog.getPerceptionTypePrint(perceptionType));
			
			payrollPerception.setKey(stringValue(entry[4]));
			payrollPerception.setConcept(stringValue(entry[5]));
			payrollPerception.setTaxableAmount(format((entry[6])));
			payrollPerception.setExemptAmount(format((entry[7])));
			
			apiPayroll.addPerception(payrollPerception);
		}
	}
	
	private void fillPayrollRetirementPension(final ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYROLL_JPR)) {
			val payrollPensionRetirement = new PayrollPensionRetirement();
			if (!UValidator.isNullOrEmpty(entry[3])) {
				payrollPensionRetirement.setTotalExhibition(format((entry[3])));
			}
			payrollPensionRetirement.setTotalPartiality(format((entry[4])));
			payrollPensionRetirement.setDailyAmount(format((entry[5])));
			payrollPensionRetirement.setIncomeAccumulate(format((entry[6])));
			payrollPensionRetirement.setIncomeNoAccumulate(format((entry[7])));
			
			apiPayroll.addPensionRetirement(payrollPensionRetirement);
		}
	}
	
	private void fillPayrollDeductions(final ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYROLL_DEDUCTIONS)) {
			val payrollDeduction = new ApiPayrollDeduction();
			payrollDeduction.setTotalOtherDeductions(formatSafe((entry[3])));
			payrollDeduction.setTotalTaxesWithheld(formatSafe((entry[4])));
			
			apiPayroll.setPayrollDeduction(payrollDeduction);
		}
	}

	private void fillPayrollDeduction(final ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYROLL_DEDUCTION)) {
			val payrollDeduction = new PayrollDeduction();
			
			val deductionType = stringValue(entry[3]);
			payrollDeduction.setDeductionTypeCode(deductionType);
			payrollDeduction.setDeductionTypePrint(satCatalog.getDeductionTypePrint(deductionType));
			
			payrollDeduction.setKey(stringValue(entry[4]));
			payrollDeduction.setConcept(stringValue(entry[5]));
			payrollDeduction.setAmount(format((entry[6])));
			
			apiPayroll.addDeduction(payrollDeduction);
		}
	}
	
	private void fillPayrollOtherPayments(ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYROLL_OTHER_PAYMENT)) {
			val payrollOtherPayment = new ApiPayrollOtherPayment();
			
			val otherPaymentTypeCode = stringValue(entry[3]);
			payrollOtherPayment.setOtherPaymentTypeCode(otherPaymentTypeCode);
			payrollOtherPayment.setOtherPaymentTypePrint(satCatalog.getOtherPaymentTypePrint(otherPaymentTypeCode));
			
			payrollOtherPayment.setKey(stringValue(entry[4]));
			payrollOtherPayment.setConcept(stringValue(entry[5]));
			payrollOtherPayment.setAmount(format(stringValue(entry[6])));
			
			apiPayroll.getPayrollOtherPayments().add(payrollOtherPayment);
		}
	}
	
	private void fillEmploymentSubsidy(ApiPayroll apiPayroll, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYROLL_EMPLOYMENT_SUBSIDY)) {
			val employmentSubsidy = new EmploymentSubsidy();
			employmentSubsidy.setSubsidyCaused(format(stringValue(entry[3])));
			
			if (apiPayroll.getPayrollOtherPayments().size() == 1) {
				apiPayroll.getPayrollOtherPayments().get(0).setEmploymentSubsidy(employmentSubsidy);
			} else {
				apiPayroll.getPayrollOtherPayments().forEach(op -> {
					if (op.getOtherPaymentTypeCode().equals("002")) {
						op.setEmploymentSubsidy(employmentSubsidy);
					}
				});
			}
		}
	}
	
}