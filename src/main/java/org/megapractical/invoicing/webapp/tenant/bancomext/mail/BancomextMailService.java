package org.megapractical.invoicing.webapp.tenant.bancomext.mail;

import static org.megapractical.common.validator.AssertUtils.hasValue;
import static org.megapractical.common.validator.AssertUtils.nonNull;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Properties;

import javax.mail.MessagingException;

import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.cancellation.CancellationNotificationResponse;
import org.megapractical.invoicing.webapp.exposition.cancellation.CancellationNotification.CancellationNotificationData;
import org.megapractical.invoicing.webapp.tenant.bancomext.service.BancomextNotificationService;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class BancomextMailService {

	private final AppSettings appSettings;
	private final JavaMailSenderImpl mailSender;
	private final SpringTemplateEngine thymeleaf;
	private final BancomextNotificationService bancomextNotificationService;

	public void sendEmail(BancomextMailData mailData) {
		val zipPath = BancomextResourceZipper.zipXmlAndPdf(mailData.getXmlPath(), mailData.getPdfPath());
		if (zipPath != null) {
			mailData.setZipPath(zipPath);
			val attachmentFile = new FileSystemResource(zipPath);
			val attachmentName = mailData.getXmlFileName().replace(".xml", ".zip");
			sendEmail(mailData, attachmentFile, attachmentName);
		}
	}

	public void sendEmail(BancomextMailData mailData, FileSystemResource attachmentFile, String attachmentName) {
		val recipients = mailData.getRecipients();
		if (recipients != null && !recipients.isEmpty()) {
			for (val email : recipients) {
				val subject = determineSubject(mailData.getSubject());
				mailData.setSubject(subject);
				mailData.setRecipient(email);
				
				String reason = null;
				try {
					val message = mailSender.createMimeMessage();
					val messageHelper = new MimeMessageHelper(message, true, StandardCharsets.UTF_8.name());
					messageHelper.setFrom(BancomextMailConstants.FROM);
					messageHelper.setSubject(subject);
					messageHelper.setText(populateEmailBody(mailData), true);
					if (nonNull(attachmentFile) && hasValue(attachmentName)) {
						messageHelper.addAttachment(attachmentName, attachmentFile);
					}
					
					populateProperties();
					UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT MAIL SERVICE > SENDING EMAIL WITH SUBJECT " + message.getSubject() + " TO " + email);
					messageHelper.setTo(email);
					mailSender.send(message);
					UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT MAIL SERVICE > EMAIL WITH SUBJECT " + message.getSubject() + " SUCCESSFULLY SENT TO " + email);
				} catch (MailException | MessagingException e) {
					reason = e.getMessage();
					UPrint.logWithLine(UProperties.env(), "[ERROR] BANCOMEXT MAIL SERVICE > EMAIL HAS NOT BEEN SENT. REASON : " + reason);
					e.printStackTrace();
				}
				registerNotification(mailData, reason);
			}
		}
	}

	public void sendEmail(BancomextStampSummary stampSummary) {
		val recipients = stampSummary.getRecipients();
		if (recipients != null && !recipients.isEmpty()) {
			for (val email : recipients) {
				val subject = determineSubject(BancomextMailConstants.SUBJECT_SUMMARY);
				stampSummary.setSubject(subject);
				stampSummary.setRecipient(email);
				
				String reason = null;
				try {
					val message = mailSender.createMimeMessage();
					val messageHelper = new MimeMessageHelper(message, true, StandardCharsets.UTF_8.name());
					messageHelper.setFrom(BancomextMailConstants.FROM);
					messageHelper.setSubject(subject);
					messageHelper.setText(populateEmailBody(stampSummary), true);
					
					populateProperties();
					UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT MAIL SERVICE > SENDING EMAIL WITH SUBJECT " + message.getSubject() + " TO " + email);
					messageHelper.setTo(email);
					mailSender.send(message);
					UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT MAIL SERVICE > EMAIL WITH SUBJECT " + message.getSubject() + " SUCCESSFULLY SENT TO " + email);
				} catch (MailException | MessagingException e) {
					reason = e.getMessage();
					UPrint.logWithLine(UProperties.env(), "[ERROR] BANCOMEXT MAIL SERVICE > EMAIL HAS NOT BEEN SENT. REASON : " + reason);
					e.printStackTrace();
				}
				registerNotification(stampSummary, reason);
			}
		}
	}
	
	public CancellationNotificationResponse sendCancellationNotification(CancellationNotificationData data) {
		try {
			val message = mailSender.createMimeMessage();
			val messageHelper = new MimeMessageHelper(message, true, StandardCharsets.UTF_8.name());
			messageHelper.setFrom(BancomextMailConstants.FROM);
			messageHelper.setSubject(data.getSubject());
			messageHelper.setText(populateCancellationNotificationEmailBody(data), true);
			
			populateProperties();
			UPrint.logWithLine(UProperties.env(),
					"[INFO] BANCOMEXT MAIL SERVICE > SENDING CANCELLATION NOTIFICATION EMAIL WITH SUBJECT "
							+ message.getSubject() + " TO " + data.getRecipient());
			messageHelper.setTo(data.getRecipient());
			mailSender.send(message);
			UPrint.logWithLine(UProperties.env(),
					"[INFO] BANCOMEXT MAIL SERVICE > CANCELLATION NOTIFICATION EMAIL WITH SUBJECT "
							+ message.getSubject() + " SUCCESSFULLY SENT TO " + data.getRecipient());
			return CancellationNotificationResponse.sent();
		} catch (MailException | MessagingException e) {
			UPrint.logWithLine(UProperties.env(),
					"[ERROR] BANCOMEXT MAIL SERVICE > CANCELLATION NOTIFICATION EMAIL HAS NOT BEEN SENT. REASON : "
							+ e.getMessage());
			e.printStackTrace();
			return CancellationNotificationResponse.error(e.getMessage());
		}
	}

	private String determineSubject(String subject) {
		if (appSettings.getActiveProfile().equals("prod")) {
			return subject.replace("{PREFIX}", "PROD");
		}
		return subject.replace("{PREFIX}", "QA");
	}

	private String populateEmailBody(BancomextMailData mailData) {
		val ctx = new Context();
		ctx.setVariable("emitterRfc", mailData.getEmitterRfc());
		ctx.setVariable("emitterName", mailData.getEmitterName());
		ctx.setVariable("receiverRfc", mailData.getReceiverRfc());
		ctx.setVariable("serie", mailData.getSerie());
		ctx.setVariable("folio", mailData.getFolio());
		ctx.setVariable("uuid", mailData.getUuid());
		return thymeleaf.process("/mail/BancomextDocTemplate", ctx);
	}

	private String populateEmailBody(BancomextStampSummary stampSummary) {
		val ctx = new Context();
		ctx.setVariable("fileName", stampSummary.getFileName());
		ctx.setVariable("summary", stampSummary.getSummary());
		return thymeleaf.process("/mail/BancomextSummaryTemplate", ctx);
	}
	
	private String populateCancellationNotificationEmailBody(CancellationNotificationData data) {
		val ctx = new Context();
		ctx.setVariable("emitterRfc", data.getEmitterRfc());
		ctx.setVariable("emitterName", data.getEmitterName());
		ctx.setVariable("fileName", data.getFileName());
		ctx.setVariable("date", UDateTime.format(LocalDateTime.now()));
		return thymeleaf.process("/mail/CancellationNotificationTemplate", ctx);
	}

	private void populateProperties() {
		val properties = smtpProperties();
		mailSender.setHost(properties.getProperty("mail.smtp.host"));
		mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
		mailSender.setJavaMailProperties(properties);
		mailSender.setUsername(properties.getProperty("mail.smtp.user"));
		mailSender.setPassword(properties.getProperty("mail.smtp.password"));
	}

	private Properties smtpProperties() {
		val prop = new Properties();
		prop.put("mail.smtp.host", BancomextMailConstants.SMTP);
		prop.put("mail.smtp.port", BancomextMailConstants.PORT);
		prop.put("mail.smtp.user", BancomextMailConstants.USERNAME);
		prop.put("mail.smtp.password", BancomextMailConstants.PWD);
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true");
		return prop;
	}

	private void registerNotification(BancomextMailData mailData, String reason) {
		bancomextNotificationService.save(mailData, reason);
	}
	
	private void registerNotification(BancomextStampSummary stampSummary, String reason) {
		bancomextNotificationService.save(stampSummary, reason);
	}

}