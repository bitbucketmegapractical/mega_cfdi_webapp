package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

import java.util.Arrays;
import java.util.List;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class BancomextParserType {
	
	private BancomextParserType() {		
	}
	
	private static final String PAYROLL = "Nomina";
	private static final List<String> EDC = Arrays.asList("EDC", "BNCEDIV", "BNCEINT", "BNCENAL");
	
	public static ParserType getParser(String entryValue) {
		if (isPayroll(entryValue)) {
			return ParserType.PAYROLL;
		} else if (isEdc(entryValue)) {
			return ParserType.EDC;					
		}
		return ParserType.CFDI;
	}
	
	public static boolean isPayroll(String entryValue) {
		return entryValue != null && entryValue.startsWith(PAYROLL);
	}
	
	public static boolean isEdc(String entryValue) {
		return entryValue != null && EDC.contains(entryValue);
	}
	
	public enum ParserType {
		CFDI, PAYROLL, RETENTION, EDC
	}
	
}