package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.dal.bean.datatype.Area;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextParseData {
	private String summaryEmailInfo;
	@Getter(AccessLevel.NONE)
	private List<BancomextCfdi> bancomextCfdis;
	@Getter(AccessLevel.NONE)
	private List<ApiPayroll> payrolls;
	@Getter(AccessLevel.NONE)
	private List<BancomextEdc> bancomextEdcs;
	@Getter(AccessLevel.NONE)
	private List<String> parserErrors;
	private Area area;

	public static String determineErrorCause(int line, String parserEntry, String errorCause) {
		val entry = "Línea " + line + ". Trama " + (parserEntry != null ? parserEntry : "No identificada");
		try {
			val errorInt = Integer.valueOf(errorCause);
			return entry + " -> java.lang.ArrayIndexOutOfBoundsException: " + errorInt;
		} catch (NumberFormatException e) {
			return entry + " -> " + (errorCause != null ? errorCause : "Sin información del error");
		}
	}
	
	/* Getters */
	public List<BancomextCfdi> getBancomextCfdis() {
		if (bancomextCfdis == null) {
			bancomextCfdis = new ArrayList<>();
		}
		return bancomextCfdis;
	}

	public List<ApiPayroll> getPayrolls() {
		if (payrolls == null) {
			payrolls = new ArrayList<>();
		}
		return payrolls;
	}

	public List<BancomextEdc> getBancomextEdcs() {
		if (bancomextEdcs == null) {
			bancomextEdcs = new ArrayList<>();
		}
		return bancomextEdcs;
	}
	
	public List<String> getParserErrors() {
		if (parserErrors == null) {
			parserErrors = new ArrayList<>();
		}
		return parserErrors;
	}

}