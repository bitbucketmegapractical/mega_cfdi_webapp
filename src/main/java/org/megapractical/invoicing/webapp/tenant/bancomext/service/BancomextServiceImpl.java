package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.service.CfdiXmlService;
import org.megapractical.invoicing.api.smarterweb.SWCfdiStamp;
import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.util.UZipper;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.api.xml.XmlHelper;
import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.dal.bean.datatype.FileStatus;
import org.megapractical.invoicing.dal.bean.datatype.Origen;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.voucher.catalogs.CTipoDeComprobante;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.ResourceData;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.ResourceData.ResourceCfdiData;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.ResourceData.ResourcePayrollData;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.payload.UploadFilePath;
import org.megapractical.invoicing.webapp.exposition.service.CfdiBuilderService;
import org.megapractical.invoicing.webapp.exposition.service.CfdiService;
import org.megapractical.invoicing.webapp.exposition.service.PayrollBuilderService;
import org.megapractical.invoicing.webapp.exposition.service.PayrollService;
import org.megapractical.invoicing.webapp.exposition.service.api.StampClientService;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.tenant.bancomext.error.BancomextError;
import org.megapractical.invoicing.webapp.tenant.bancomext.error.BancomextError.BancomextErrorDetail;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary.StampSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextParser;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextCfdi;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextEdc;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCfdiReportService;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextEdcReportService;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextPayrollReportService;
import org.megapractical.invoicing.webapp.tenant.bancomext.util.BancomextUtils;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextSftpFileData;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData.BancomextVoucherDetail;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampDataWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampDataWrapper.BancomextCfdiType;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampResponse;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextZipData;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class BancomextServiceImpl implements BancomextService {

	private final AppSettings appSettings;
	private final UserTools userTools;
	private final PropertiesTools propertiesTools;
	private final BancomextArchivoService bancomextArchivoService;
	private final BancomextParser bancomextParser;
	private final CfdiBuilderService cfdiBuilderService;
	private final PayrollBuilderService payrollBuilderService;
	private final ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	private final BancomextCfdiReportService bancomextCfdiReportService;
	private final BancomextPayrollReportService bancomextPayrollReportService;
	private final BancomextEdcReportService bancomextEdcReportService;
	private final CfdiService cfdiService;
	private final PayrollService payrollService;
	private final StampClientService stampClientService;
	private final BancomextStampSummaryCsvService bancomextStampSummaryCsvService;
	
	private ContribuyenteEntity taxpayer;
	private ContribuyenteCertificadoEntity certificate;
	private String storePath;
	private boolean useFakeStampService;
	private boolean useStampClient;
	
	@PostConstruct
	public void init() {
		val bancomextRfc = appSettings.getPropertyValue("bancomext.rfc");
		this.taxpayer = userTools.getContribuyenteActive(bancomextRfc);
		this.certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
		this.storePath = appSettings.getPropertyValue("bancomext.store");
		this.useFakeStampService = appSettings.getBooleanValue("cfdi.use.fake.stamp.service");
		this.useStampClient = appSettings.getBooleanValue("cfdi.use.stamp.client");
	}
	
	@Override
	public BancomextSftpFileData sftpFileData(List<String> sftpFiles) {
		val validFiles = new ArrayList<String>();
		val duplicateFiles = new ArrayList<String>();
		sftpFiles.forEach(fileName -> {
			if (!bancomextArchivoService.existByName(fileName)) {
				validFiles.add(fileName);
			} else {
				duplicateFiles.add(fileName);
			}
		});
		return new BancomextSftpFileData(validFiles, duplicateFiles);
	}
	
	@Override
	public BancomextFileWrapper createFile(String fileName, Origen origen) {
		val bancomextFile = new ArchivoStfpEntity(taxpayer, origen, FileStatus.P, fileName);
		bancomextArchivoService.save(bancomextFile);
		return bancomextFile(fileName, bancomextFile);
	}
	
	@Override
	public ArchivoStfpEntity updateBancomextFile(ArchivoStfpEntity bancomextFile) {
		return bancomextArchivoService.save(bancomextFile);
	}

	@Override
	public BancomextFileErrorWrapper logDuplicateFile(String fileName) {
		val fileWrapper = bancomextFile(fileName, null);
		return writeFileError(fileWrapper, BancomextFileErrorWrapper.DUPLICATE_FILE);
	}
	
	@Override
	public BancomextStampResponse parseAndStamp(BancomextFileWrapper fileWrapper) {
		if (fileWrapper != null) {
			BancomextStampResponse bancomextStampResponse = null;
			val bancomextParseData = bancomextParser.parse(fileWrapper.getLocalFile(), certificate);
			val summaryEmailInfo = bancomextParseData.getSummaryEmailInfo();
			val voucherDetails = new ArrayList<BancomextVoucherDetail>();
			
			String errorDescription = null;
			if (AssertUtils.nonNull(bancomextParseData.getParserErrors())) {
				errorDescription = BancomextFileErrorWrapper.CONTENT_ERROR;
			}
			
			val bancomextCfdis = bancomextParseData.getBancomextCfdis();
			val payrolls = bancomextParseData.getPayrolls();
			val bancomextEdcs = bancomextParseData.getBancomextEdcs();
			
			val stampDataSource = new ArrayList<BancomextStampData>();
			if (!bancomextCfdis.isEmpty()) {
				cfdiStampDataSource(bancomextCfdis, stampDataSource, voucherDetails);
			} else if (!payrolls.isEmpty()) {
				payrollStampDataSource(payrolls, stampDataSource, voucherDetails);
			} else if (!bancomextEdcs.isEmpty()) {
				edcStampDataSource(bancomextEdcs, stampDataSource, voucherDetails);
			}
			
			updateFile(bancomextParseData.getArea(), fileWrapper);
			if (!stampDataSource.isEmpty()) {
				UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > FILE NAME: " + fileWrapper.getFileDetail().getOriginalFilename());
				UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > TOTAL VOUCHERS TO STAMP: " + stampDataSource.size());
				
				val stampDataWrapper = new BancomextStampDataWrapper(fileWrapper, stampDataSource, summaryEmailInfo, voucherDetails);
				bancomextStampResponse = stampAndStore(stampDataWrapper);
			}
			
			val fileErrorWrapper = writeFileError(fileWrapper, errorDescription, bancomextParseData.getParserErrors(), summaryEmailInfo, voucherDetails);
			if (bancomextStampResponse == null) {
				bancomextStampResponse = BancomextStampResponse.builder().fileErrorWrapper(fileErrorWrapper).build();
			} else {
				bancomextStampResponse.setFileErrorWrapper(fileErrorWrapper);
			}
			return bancomextStampResponse;
		}
		return null;
	}

	private BancomextFileWrapper bancomextFile(String fileName, ArchivoStfpEntity bancomextFile) {
		val filePath = BancomextUtils.determineAbsolutePath(taxpayer.getRfcActivo(), storePath, fileName);
		UFile.createParentDirectory(filePath);
		val localFile = UFile.createFileIfNotExist(filePath);
		val fileDetails = UFile.fileDetails(localFile);
		val uploadFilePath = UploadFilePath
								.builder()
								.absolutePath(BancomextUtils.getAbsolutePath(taxpayer.getRfcActivo(), storePath, fileName))
								.uploadPath(filePath)
								.dbPath(BancomextUtils.getBasePath(taxpayer.getRfcActivo(), fileName))
								.build();
		
		return BancomextFileWrapper
				.builder()
				.localFile(localFile)
				.bancomextFile(bancomextFile)
				.fileDetail(fileDetails)
				.uploadFilePath(uploadFilePath)
				.build();
	}
	
	private BancomextFileErrorWrapper writeFileError(BancomextFileWrapper fileWrapper, String errorDescription) {
		return writeFileError(fileWrapper, errorDescription, null, null, null);
	}
	
	private BancomextFileErrorWrapper writeFileError(BancomextFileWrapper fileWrapper, 
													 String errorDescription,
													 List<String> parserErrors, 
													 String summaryEmailInfo, 
													 List<BancomextVoucherDetail> voucherDetails) {
		try {
			if (AssertUtils.hasValue(errorDescription) || AssertUtils.nonNull(parserErrors)
					|| AssertUtils.nonNull(voucherDetails)) {
				// Fichero de error a generar
				val errorPath = BancomextUtils.errorPath(fileWrapper);
				val errorFile = fileWrapper.getFileDetail().getFileName() + "_ERROR.txt";
				val fileErrorPath = errorPath + BancomextUtils.SEPARATOR + errorFile;
				val errorDetail = new BancomextErrorDetail(errorPath, fileErrorPath, errorDescription, parserErrors, voucherDetails);
				
				UFile.createParentDirectory(fileErrorPath);
				BancomextError.writeFileError(errorDetail);
				
				if (UFile.totalFilesInDirectory(errorPath) > 0) {
					val zippedDuplicateFilePath = zipper(errorPath);
					return new BancomextFileErrorWrapper(fileWrapper, zippedDuplicateFilePath, errorDetail, summaryEmailInfo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void cfdiStampDataSource(List<BancomextCfdi> bancomextCfdis, 
									 final List<BancomextStampData> stampDataSource,
									 final List<BancomextVoucherDetail> voucherDetails) {
		val targetList = bancomextCfdis.stream().map(this::mapToBancomextStamp).collect(toList());
		targetList.stream().filter(i -> i.getVoucher() != null).forEach(stampDataSource::add);
		targetList.stream().map(BancomextStampData::getVoucherDetail).filter(Objects::nonNull).forEach(voucherDetails::add);
	}
	
	private void payrollStampDataSource(List<ApiPayroll> payrolls, 
										final List<BancomextStampData> stampDataSource, 
										final List<BancomextVoucherDetail> voucherDetails) {
		val targetList = payrolls.stream().map(this::mapToBancomextStamp).collect(toList());
		targetList.stream().filter(i -> i.getVoucher() != null).forEach(stampDataSource::add);
		targetList.stream().map(BancomextStampData::getVoucherDetail).filter(Objects::nonNull).forEach(voucherDetails::add);
	}
	
	private void edcStampDataSource(List<BancomextEdc> bancomextEdcs, 
									final List<BancomextStampData> stampDataSource, 
									final List<BancomextVoucherDetail> voucherDetails) {
		val targetList = bancomextEdcs.stream().map(this::mapToBancomextStamp).collect(toList());
		targetList.stream().filter(i -> i.getVoucher() != null).forEach(stampDataSource::add);
		targetList.stream().map(BancomextStampData::getVoucherDetail).filter(Objects::nonNull).forEach(voucherDetails::add);
	}

	private BancomextStampResponse stampAndStore(BancomextStampDataWrapper stampDataWrapper) {
		val fileWrapper = stampDataWrapper.getFileWrapper();
		val stampDataSource = new ArrayList<BancomextStampData>();
		val errors = new ArrayList<BancomextError>();
		val stampSummary = new ArrayList<StampSummary>();
		
		stampDataWrapper.getStampDataSource().forEach(data -> {
			val voucher = data.getVoucher();
			val allowStamp = data.isAllowStamp();
			val cfdi = getCfdiFromStampData(data);
			
			val stampResponse = stamp(voucher, allowStamp);
			if (stampResponse.isStamped()) {
				BancomextStampData stampData = null;
				if (CTipoDeComprobante.N.equals(voucher.getTipoDeComprobante())) {
					stampData = new BancomextStampData(data.getPayroll(), voucher);
				} else if (data.getEdc() != null) {
					stampData = new BancomextStampData(data.getEdc(), voucher, allowStamp);
				} else {
					stampData = new BancomextStampData(data.getCfdi(), data.getInvoices());
				}
				stampDataSource.add(stampData);
			} else {
				addError(voucher, stampResponse, errors, cfdi);
			}
			
			if (data.getCfdi() != null) {
				data.getCfdi().setStampResponse(stampResponse);
			} else if (data.getPayroll() != null) {
				data.getPayroll().getApiCfdi().setStampResponse(stampResponse);
			} else if (data.getEdc() != null) {
				data.getEdc().getCfdi().setStampResponse(stampResponse);
			}
			
			val sequence = stampSummary.size() + 1;
			val summary = StampSummary.summary(cfdi, sequence);
			stampSummary.add(summary);
		});
		
		stampDataWrapper.getVoucherDetails().stream().map(StampSummary::summary).forEach(stampSummary::add); 
		
		val resourceDataSource = store(fileWrapper, stampDataSource);
		bancomextStampSummaryCsvService.createStampSummaryCsvFile(fileWrapper, stampSummary);
		val zipData = zipData(fileWrapper, errors);
		val bancomextStampSummary = new BancomextStampSummary(stampDataWrapper.getSummaryEmailInfo(), stampSummary,
				fileWrapper.getFileDetail().getOriginalFilename());
		return new BancomextStampResponse(resourceDataSource, zipData, bancomextStampSummary);
	}

	private ApiCfdi getCfdiFromStampData(BancomextStampData data) {
		ApiCfdi cfdi = null;
		if (data.getCfdi() != null) {
			cfdi = data.getCfdi();
		} else if (data.getPayroll() != null) {
			cfdi = data.getPayroll().getApiCfdi();
		} else if (data.getEdc() != null) {
			cfdi = data.getEdc().getCfdi();
		}
		return cfdi;
	}
	
	private void addError(Comprobante voucher, 
						  StampResponse stampResponse,
						  final List<BancomextError> errors, 
						  ApiCfdi cfdi) {
		val error = BancomextError
					.builder()
					.rfc(voucher.getReceptor().getRfc())
					.serie(voucher.getSerie())
					.folio(voucher.getFolio())
					.error(stampResponse.getError())
					.detail(stampResponse.getErrorMessage())
					.xmlErrorName(cfdi.getXmlPdfName())
					.sealedXml(stampResponse.getSealedXml())
					.build();
		errors.add(error);
	}

	private BancomextStampData mapToBancomextStamp(BancomextCfdi bancomextCfdi) {
		val cfdi = bancomextCfdi.getCfdi();
		val invoices = bancomextCfdi.getInvoices();
		val allowStamp = cfdiBuilderService.voucherBuild(cfdi, false);
		val voucher = allowStamp.getVoucher();
		val voucherDetail = voucher == null ? new BancomextVoucherDetail(cfdi, allowStamp.getUuid()) : null;
		return BancomextStampData
				.builder()
				.cfdi(cfdi)
				.invoices(invoices)
				.voucher(voucher)
				.voucherDetail(voucherDetail)
				.build();
	}
	
	private BancomextStampData mapToBancomextStamp(ApiPayroll apiPayroll) {
		val allowStamp = payrollBuilderService.build(apiPayroll, false);
		val voucher = allowStamp.getVoucher();
		val voucherDetail = voucher == null ? new BancomextVoucherDetail(apiPayroll, allowStamp.getUuid()) : null;
		return BancomextStampData
				.builder()
				.payroll(apiPayroll)
				.voucher(voucher)
				.voucherDetail(voucherDetail)
				.build();
	}
	
	private BancomextStampData mapToBancomextStamp(BancomextEdc bancomextEdc) {
		val cfdi = bancomextEdc.getCfdi();
		val allowStamp = cfdiBuilderService.voucherBuild(cfdi, false);
		val voucher = allowStamp.getVoucher();
		val voucherDetail = voucher == null ? new BancomextVoucherDetail(cfdi, allowStamp.getUuid()) : null;
		return BancomextStampData
				.builder()
				.edc(bancomextEdc)
				.voucher(voucher)
				.cfdiType(BancomextCfdiType.EDC)
				.allowStamp(bancomextEdc.isAllowStamp())
				.voucherDetail(voucherDetail)
				.build();
	}
	
	private void updateFile(Area area, BancomextFileWrapper fileWrapper) {
		fileWrapper.getBancomextFile().setArea(area);
		fileWrapper.getBancomextFile().setStatus(FileStatus.G);
		updateBancomextFile(fileWrapper.getBancomextFile());
	}
	
	private StampResponse stamp(Comprobante voucher, boolean allowStamp) {
		val cert = this.certificate.getCertificado();
		val privateKey = this.certificate.getLlavePrivada();
		val pwd = UBase64.base64Decode(this.certificate.getClavePrivada());
		
		val stampProperties = StampProperties
								.builder()
								.voucher(voucher)
								.certificate(cert)
								.privateKey(privateKey)
								.passwd(pwd)
								.environment(propertiesTools.getEnvironment())
								.build();
		
		StampResponse stampResponse;
		if (!useStampClient && !useFakeStampService) {
			if (allowStamp) {
				stampResponse = SWCfdiStamp.cfdiStamp(stampProperties);
			} else {
				UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > NON FISCAL");
				val xml = CfdiXmlService.generateXml(voucher);
				val sanitizedXml = UValue.unescape(UValue.byteToString(xml));
				val sealedXml = XmlHelper.sealedXml(sanitizedXml, privateKey, pwd);
				stampResponse = StampResponse.nonFiscal(voucher, sealedXml);
			}
		} else {
			if (useStampClient) {
				stampResponse = stampClientService.stamp(stampProperties);
			} else {
				stampResponse = BancomextFakeStampService.stamp(voucher);
			}
		}
		return stampResponse;
	}
	
	private List<ResourceData> store(BancomextFileWrapper fileWrapper, List<BancomextStampData> stampDataSource) {
		val resourceDataSource = new ArrayList<ResourceData>();
		stampDataSource.forEach(stampData -> {
			val isCfdi = stampData.getCfdiType().equals(BancomextCfdiType.CFDI);
			val isPayroll = stampData.getCfdiType().equals(BancomextCfdiType.PAYROLL);
			val isEdc = stampData.getCfdiType().equals(BancomextCfdiType.EDC);
			
			val apiPayroll = stampData.getPayroll();
			val edc = stampData.getEdc();
			
			val apiCfdi = getApiCfdi(stampData, isCfdi, isPayroll, isEdc, apiPayroll, edc);
			if (apiCfdi != null) {
				val stampResponse = apiCfdi.getStampResponse();
				val absolutePath = fileWrapper.getUploadFilePath().getAbsolutePath();
				val xmlPdfName = determineFilename(isPayroll, apiPayroll, apiCfdi);
				
				// Storing XML
				val xmlFileName = xmlPdfName + ".xml";
				val xmlPath = absolutePath + BancomextUtils.SEPARATOR + xmlFileName;
				UFile.writeFile(stampResponse.getXml(), xmlPath);

				// Generate and storing PDF
				val pdfFileName = xmlPdfName + ".pdf";
				val pdfPath = absolutePath + BancomextUtils.SEPARATOR + pdfFileName;
				apiCfdi.setPdfPath(pdfPath);
				
				val area = fileWrapper.getBancomextFile().getArea();
				if (isCfdi) {
					bancomextCfdiReportService.generatePdf(area, stampData);
				} else if (isPayroll) {
					bancomextPayrollReportService.generatePdf(stampData);
				} else if (isEdc) {
					bancomextEdcReportService.generatePdf(stampData);
				}
				
				val resourceData = ResourceData
									.builder()
									.xmlFileName(xmlFileName)
									.xmlPath(xmlPath)
									.pdfFileName(pdfFileName)
									.pdfPath(pdfPath)
									.build();
				if (isCfdi || isEdc) {
					resourceCfdiData(resourceData, apiCfdi);
				} else if (isPayroll) {
					resourcePayrollData(resourceData, apiPayroll, apiCfdi);
				}
				resourceDataSource.add(resourceData);
			}
		});
		return resourceDataSource;
	}

	private ApiCfdi getApiCfdi(BancomextStampData stampData, 
							   final boolean isCfdi, 
							   final boolean isPayroll,
							   final boolean isEdc, 
							   final ApiPayroll apiPayroll, 
							   final BancomextEdc edc) {
		ApiCfdi apiCfdi = null;
		if (isCfdi) {
			apiCfdi = stampData.getCfdi();
		} else if (isPayroll) {
			apiCfdi = apiPayroll.getApiCfdi();
		} else if (isEdc) {
			apiCfdi = edc.getCfdi();
		}
		return apiCfdi;
	}
	
	private String determineFilename(boolean isPayroll, ApiPayroll apiPayroll, ApiCfdi apiCfdi) {
		return isPayroll ? apiPayroll.getXmlPdfName() : apiCfdi.getXmlPdfName();
	}

	private void resourceCfdiData(final ResourceData resourceData, ApiCfdi apiCfdi) {
		// Saving Cfdi
		if (AssertUtils.hasValue(apiCfdi.getStampResponse().getUuid())) {
			UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > SAVING CFDI " + apiCfdi.getStampResponse().getUuid());
		}
		
		val cfdi = cfdiService.save(apiCfdi);
		if (cfdi != null) {
			if (AssertUtils.hasValue(apiCfdi.getStampResponse().getUuid())) {
				UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > CFDI "
						+ apiCfdi.getStampResponse().getUuid() + " HAS BEEN SAVED SUCCESSFULLY");
			}
			
			val resourceCfdiData = ResourceCfdiData
									.builder()
									.cfdi(cfdi)
									.apiCfdi(apiCfdi)
									.build();
			resourceData.setResourceCfdiData(resourceCfdiData);
		}
	}
	
	private void resourcePayrollData(final ResourceData resourceData, ApiPayroll apiPayroll, ApiCfdi apiCfdi) {
		// Saving payroll
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > SAVING PAYROLL " + apiCfdi.getStampResponse().getUuid());
		val payroll = payrollService.save(apiPayroll);
		if (payroll != null) {
			UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > PAYROLL "
					+ apiCfdi.getStampResponse().getUuid() + " HAS BEEN SAVED SUCCESSFULLY");
			val resourcePayrollData = ResourcePayrollData
										.builder()
										.payroll(payroll)
										.apiPayroll(apiPayroll)
										.build();
			resourceData.setResourcePayrollData(resourcePayrollData);
		}
	}

	private BancomextZipData zipData(BancomextFileWrapper fileWrapper, List<BancomextError> errors) {
		val zipDirectory = fileWrapper.getUploadFilePath().getAbsolutePath();
		val fileErrorPath = writeError(fileWrapper, errors);
		val zipData = new BancomextZipData(fileWrapper, zipDirectory, fileErrorPath);
		zipper(zipData);
		return zipData;
	}
	
	private String writeError(BancomextFileWrapper fileWrapper, List<BancomextError> errors) {
		if (errors != null && !errors.isEmpty()) {
			// Fichero de error a generar
			val errorPath = BancomextUtils.errorPath(fileWrapper);
			val errorFile = fileWrapper.getFileDetail().getFileName() + "_ERROR.txt";
			val fileErrorPath = errorPath + BancomextUtils.SEPARATOR + errorFile;
			val errorDetail = new BancomextErrorDetail(errorPath, fileErrorPath);
			
			UFile.createParentDirectory(fileErrorPath);
			BancomextError.writeError(errorDetail, errors);
			return errorPath;
		}
		return null;
	}
	
	private void zipper(final BancomextZipData zipData) {
		try {
			if (UFile.totalFilesInDirectory(zipData.getCfdisPath()) > 1) {
				val zippedPath = zipper(zipData.getCfdisPath());
				zipData.setCfdisZippedPath(zippedPath);
			}
			
			if (UFile.totalFilesInDirectory(zipData.getErrorPath()) > 0) {
				val zippedPath = zipper(zipData.getErrorPath());
				zipData.setErrorZippedPath(zippedPath);
			}
			UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > ZIPPING DIRECTORY FINISHED");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String zipper(String zipDirectory) {
		val zipFile = zipDirectory + ".zip";
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SERVICE > ZIPPING DIRECTORY: " + zipDirectory);
		UZipper.zipping(new File(zipDirectory), zipFile);
		return zipFile;
	}

}