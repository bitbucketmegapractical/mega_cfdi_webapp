package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;

public interface BancomextArchivoService {
	
	boolean existByName(String fileName);
	
	ArchivoStfpEntity save(ArchivoStfpEntity entity);
	
}