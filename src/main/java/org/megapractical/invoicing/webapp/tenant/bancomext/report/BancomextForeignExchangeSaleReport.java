package org.megapractical.invoicing.webapp.tenant.bancomext.report;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextForeignExchangeSaleReport {
	private String fecha;
	private String tipoOperacion;
	private String montoDivisas;
	private String tipoCambio;
	private String contraValor;
	private String fechaOperacion;
	private String idTransaccion;

	/* Getters and Setters */
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getMontoDivisas() {
		return montoDivisas;
	}

	public void setMontoDivisas(String montoDivisas) {
		this.montoDivisas = montoDivisas;
	}

	public String getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public String getContraValor() {
		return contraValor;
	}

	public void setContraValor(String contraValor) {
		this.contraValor = contraValor;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

}