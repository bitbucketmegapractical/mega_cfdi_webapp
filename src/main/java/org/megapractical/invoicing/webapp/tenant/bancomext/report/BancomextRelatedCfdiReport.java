package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextRelatedCfdiReport {
	private String folioFiscal;

	public BancomextRelatedCfdiReport() {		
	}
	
	public BancomextRelatedCfdiReport(RelatedCfdiUuid relatedCfdi) {
		this.folioFiscal = relatedCfdi.getUuid();
	}
	
	public String getFolioFiscal() {
		return folioFiscal;
	}

	public void setFolioFiscal(String folioFiscal) {
		this.folioFiscal = folioFiscal;
	}
	
}