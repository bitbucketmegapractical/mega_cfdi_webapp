package org.megapractical.invoicing.webapp.tenant.bancomext.wrapper;

import org.megapractical.invoicing.webapp.tenant.bancomext.error.BancomextError.BancomextErrorDetail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextFileErrorWrapper {
	
	public static final String DUPLICATE_FILE = "Archivo no procesado por duplicidad de nombre";
	public static final String CONTENT_ERROR = "Archivo no procesado debido a que no cumple con la estructura adecuada para la generación de los comprobantes";
	public static final String REQUIRED_ENTRY = "La trama {entry} es requerida";
	public static final String MATCH_COUNT_ENTRY = "Deben existir la misma cantidad de las tramas {entries} en el archivo";
	public static final String CFDI_ERROR_EMAIL = "mgonzale@bancomext.gob.mx";
	public static final String PAYROLL_ERROR_EMAIL = "hgomezh@bancomext.gob.mx";
	
	private BancomextFileWrapper fileWrapper;
	private String zippedFilePath;
	private BancomextErrorDetail errorDetail;
	private String summaryEmailInfo;
}