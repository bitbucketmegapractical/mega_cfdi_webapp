package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextAddress {
	private String line1;
	private String line2;
	private String line3;
	private String line4;
	private String line5;
	private String street;
	private String number;
	private String insideNumber;
	private String town;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	
	public String getCustomStreet() {
		val streetVal = this.street + " " + this.number;
		if (this.insideNumber != null) {
			return streetVal + " " + this.insideNumber;
		}
		return streetVal;
	}
	
	public String getFullAddress() {
		val address = new StringBuilder();
		address.append(this.street);
		address.append(this.number != null ? ", " + this.number : "");
		address.append(this.insideNumber != null ? ", " + this.insideNumber : "");
		address.append(this.town != null ? ", " + this.town : "");
		address.append(this.city != null ? ", " + this.city : "");
		address.append(this.state != null ? ", " + this.state : "");
		address.append(this.country != null ? ", " + this.country : "");
		address.append(this.postalCode != null ? ", " + this.postalCode : "");
		return address.toString();	
	}
	
}