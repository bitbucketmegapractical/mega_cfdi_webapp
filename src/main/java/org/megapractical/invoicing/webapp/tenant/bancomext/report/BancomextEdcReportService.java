package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import java.util.HashMap;
import java.util.Map;

import org.megapractical.invoicing.api.report.ReportManagerTools;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.EdcType;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextEdc;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class BancomextEdcReportService {

	private final ReportManager reportManager;
	private final UserTools userTools;

	public boolean generatePdf(BancomextStampData stampData) {
		try {
			val edc = stampData.getEdc();
			val reportData = BancomextEdc.isCredit(edc.getEdcType())
					? BancomextReport.getReportData(edc.getEdcType(), stampData.isAllowStamp())
					: BancomextReport.getReportData(edc.getEdcType());
			if (reportData != null) {
				val template = BancomextEdcReportService.class.getResourceAsStream(reportData.getTemplatePath());

				val parameters = new HashMap<String, Object>();
				parameters.put("template", template);
				populatePdfData(stampData, parameters);

				val cfdi = edc.getCfdi();
				val exported = reportManager.exportReportToPdf(template, parameters, cfdi.getPdfPath());
				if (exported) {
					userTools.log("[INFO] BANCOMEXT EDC REPORT SERVICE - PDF HAS BEEN GENERATED SUCCESSFULLY");
				}
				return exported;
			}
		} catch (Exception e) {
			val rfc = stampData.getVoucher().getReceptor().getRfc();
			userTools.log(
					"[ERROR] BANCOMEXT EDC REPORT SERVICE - AN ERROR OCCURRED GENERATING THE PDF ASSOCIATED WITH RFC "
							+ rfc);
			e.printStackTrace();
		}
		return false;
	}

	private void populatePdfData(BancomextStampData stampData, final Map<String, Object> parameters) {
		val edc = stampData.getEdc();
		val edcType = edc.getEdcType();

		populateCommonData(edc, parameters);
		if (BancomextEdc.isTreasury(edcType)) {
			populateTreasuryData(edc, parameters);
			populateTreasuryFlows(edc, parameters);
			populateTreasurySummary(edc, parameters);
			if (EdcType.TESORERIANACIONAL.equals(edcType)) {
				populateTreasuryNatCurrentValues(edc, parameters);
				populateTreasuryNatGuaranteeValues(edc, parameters);
			}
		} else if (BancomextEdc.isForeignExchange(edcType)) {
			populateForeignExchangeData(edc, parameters);
			populateForeignExchangeSales(edc, parameters);
			populateForeignExchangeTransactionSummary(edc, parameters);
		} else if (BancomextEdc.isCredit(edcType)) {
			populateCreditData(edc, parameters);
			populateCreditRetrieveds(edc, parameters);
			populateCreditDispositions(edc, parameters);
			populateCreditFees(edc, parameters);
			
			if (!stampData.isAllowStamp()) {
				parameters.put("qrImagen", ReportManagerTools.getQR());
			}
		}
	}

	private void populateCommonData(BancomextEdc edc, final Map<String, Object> parameters) {
		val cfdi = edc.getCfdi();
		val stampResponse = cfdi.getStampResponse();
		val voucher = stampResponse.getVoucher();
		val additionals = cfdi.getAdditionals();
		val emitterTaxRegime = cfdi.getEmitter().getRegimenFiscal();

		parameters.put("logo", BancomextReport.getVerticalLogo());
		parameters.put("logo2", BancomextReport.getShcpLogo());
		parameters.put("serieFolio", voucher.getSerie() + " " + voucher.getFolio());
		parameters.put("tipoComprobante", cfdi.getVoucher().getVoucherTypeString());
		parameters.put("regimenFiscalEmisor", emitterTaxRegime.getCodigo() + " " + emitterTaxRegime.getValor());
		parameters.put("regimenFiscal", emitterTaxRegime.getCodigo() + " " + emitterTaxRegime.getValor());
		parameters.put("regimenFiscalReceptor", additionals.get(BancomextAdditional.RECEIVER_TAX_REGIME));
		parameters.put("rfcReceptor", cfdi.getReceiver().getRfc());
		parameters.put("razonSocialReceptor", cfdi.getReceiver().getNombreRazonSocial());
		parameters.put("usoCfdi", additionals.get(BancomextAdditional.RECEIVER_CFDI_USE));
		parameters.put("cadenaOriginal", stampResponse.getOriginalString());
		parameters.put("selloDigitalSat", stampResponse.getSatSeal());
		parameters.put("selloDigitalEmisor", stampResponse.getEmitterSeal());
		parameters.put("uuid", stampResponse.getUuidUpperCase());
		parameters.put("moneda", cfdi.getVoucher().getCurrencyString());
		parameters.put("metodoPago", cfdi.getVoucher().getPaymentMethodString());
		parameters.put("formaPago", cfdi.getVoucher().getPaymentWayString());
		parameters.put("noCertificadoEmisor", stampResponse.getCertificateEmitterNumber());
		parameters.put("noCertificadoSat", stampResponse.getCertificateSatNumber());
		parameters.put("fechaEmision", stampResponse.getDateExpedition());
		parameters.put("fechaCertificacion", stampResponse.getDateStamp());
		parameters.put("lugarFechaExpedicion", voucher.getLugarExpedicion() + ", " + voucher.getFecha());
		parameters.put("unidadMedida", cfdi.getConcepts().get(0).getMeasurementUnitPrint());
		parameters.put("qrImagen", ReportManagerTools.getQR(stampResponse));
	}

	private void populateTreasuryData(BancomextEdc edc, final Map<String, Object> parameters) {
		val data = edc.getTreasury().getData();
		val cfdi = edc.getCfdi();
		val stampResponse = cfdi.getStampResponse();
		val voucher = stampResponse.getVoucher();
		val address = edc.getAddress();

		parameters.put("periodo", data.getPeriod());
		parameters.put("fechaCorte", data.getPeriodEndDate());
		parameters.put("diasPeriodo", data.getPeriodDays());
		parameters.put("contrato", edc.getContract());
		parameters.put("atencion", data.getAtencion());
		parameters.put("direccion", address.getCustomStreet());
		parameters.put("colonia", address.getTown());
		parameters.put("ciudad", address.getState());
		parameters.put("codigoPostal", address.getPostalCode());
		parameters.put("sucursal", data.getBranch());
		parameters.put("promotor", data.getPromoter());
		parameters.put("folioFiscal", voucher.getFolio());
		parameters.put("fechaEmision", stampResponse.getDateExpedition());
		parameters.put("cuentaOrdenante", null);
	}

	private void populateTreasuryFlows(BancomextEdc edc, final Map<String, Object> parameters) {
		val flows = edc.getTreasury().getFlows();
		parameters.put("movimientosMesList", flows);
	}

	private void populateTreasurySummary(BancomextEdc edc, final Map<String, Object> parameters) {
		val summary = edc.getTreasury().getSummary();
		parameters.put("saldoInicial", summary.getSaldoInicial());
		parameters.put("promedioSaldoDiario", summary.getPromedioSaldoDiario());
		parameters.put("retiros", summary.getRetiros());
		parameters.put("depositos", summary.getDepositos());
		parameters.put("interesesGenerados", summary.getInteresesGenerados());
		parameters.put("impuestosRetenidosUsd", summary.getImpuestosRetenidosUsd());
		parameters.put("impuestosRetenidosMn", summary.getImpuestosRetenidosMn());
		parameters.put("tipoCambioPromedio", summary.getTipoCambioPromedio());
		parameters.put("tasaOcuota", summary.getTasaOCuota());
		parameters.put("impuesto", summary.getImpuesto());
		parameters.put("base", summary.getBase());
		parameters.put("saldoFinal", summary.getSaldoFinal());
		parameters.put("saldoMesAnterior", summary.getSaldoMesAnterior());
		parameters.put("vencimientos", summary.getVencimientos());
		parameters.put("inversiones", summary.getInversiones());
		parameters.put("premio", summary.getPremio());
		parameters.put("impuestoRetenido", summary.getImpuestoRetenido());
		parameters.put("saldoActual", summary.getSaldoActual());
		parameters.put("ivnValVig", summary.getIvnValVig());
		parameters.put("totalDepositos", summary.getTotalDepositos());
		parameters.put("totalRetiros", summary.getTotalRetiros());
		parameters.put("saldoMesAnteriorGarantia", summary.getSaldoMesAnteriorGarantia());
		parameters.put("totalTitulosGarantiaInicio", summary.getTotalTitulosGarantiaInicio());
		parameters.put("totalTitulosGarantiaFin", summary.getTotalTitulosGarantiaFin());
		parameters.put("saldoActualGarantia", summary.getSaldoActualGarantia());
	}

	private void populateTreasuryNatCurrentValues(BancomextEdc edc, final Map<String, Object> parameters) {
		val currentValues = edc.getTreasury().getCurrentValues();
		parameters.put("valoresVigentesList", currentValues);

		val currentValueTotal = edc.getTreasury().getCurrentValueTotal();
		if (currentValueTotal != null) {
			parameters.put("vvTotalesPrecio", currentValueTotal.getTotalesPrecio());
			parameters.put("vvTotalesImporte", currentValueTotal.getTotalesImporte());
			parameters.put("vvTotalesTasa", currentValueTotal.getTotalesTasa());
			parameters.put("vvTotalesPlazo", currentValueTotal.getTotalesPlazo());
			parameters.put("vvTotalesDxv", currentValueTotal.getTotalesDxv());
			parameters.put("vvTotalesPremio", currentValueTotal.getTotalesPremio());
			parameters.put("vvTotalesIsr", currentValueTotal.getTotalesIsr());
			parameters.put("vvTotalesImpNeto", currentValueTotal.getTotalesImpNeto());
		}
	}

	private void populateTreasuryNatGuaranteeValues(BancomextEdc edc, final Map<String, Object> parameters) {
		val guaranteeValues = edc.getTreasury().getGuaranteeValues();
		parameters.put("valoresVigentesGarantiasList", guaranteeValues);

		val guaranteeValueTotal = edc.getTreasury().getGuaranteeValueTotal();
		if (guaranteeValueTotal != null) {
			parameters.put("vvgTotalesTitulos", guaranteeValueTotal.getTotalesTitulos());
			parameters.put("vvgTotalesPrecio", guaranteeValueTotal.getTotalesPrecio());
			parameters.put("vvgTotalesImporte", guaranteeValueTotal.getTotalesImporte());
			parameters.put("vvgTotalesTasa", guaranteeValueTotal.getTotalesTasa());
			parameters.put("vvgTotalesPlazo", guaranteeValueTotal.getTotalesPlazo());
			parameters.put("vvgTotalesDxv", guaranteeValueTotal.getTotalesDxv());
			parameters.put("vvgTotalesPremio", guaranteeValueTotal.getTotalesPremio());
			parameters.put("vvgTotalesIsr", guaranteeValueTotal.getTotalesIsr());
			parameters.put("vvgTotalesImpNeto", guaranteeValueTotal.getTotalesImpNeto());
		}
	}

	private void populateForeignExchangeData(BancomextEdc edc, Map<String, Object> parameters) {
		val data = edc.getForeignExchange().getData();
		val cfdi = edc.getCfdi();
		val voucher = cfdi.getVoucher();
		val stampResponse = cfdi.getStampResponse();
		val additionals = cfdi.getAdditionals();
		val complement = cfdi.getComplement().getForeignExchange();

		parameters.put("periodoInit", data.getPeriodFrom());
		parameters.put("periodoEnd", data.getPeriodTo());
		parameters.put("fechaCorte", data.getStatementDay());
		parameters.put("diasPeriodo", data.getPeriodDays());
		parameters.put("cliente", cfdi.getReceiver().getNombreRazonSocial());
		parameters.put("direccion", data.getFullAddress());
		parameters.put("noCuenta", data.getAccountNum());
		parameters.put("rfcPac", stampResponse.getTfd().getRfcProvCertif());
		parameters.put("version", complement.getVersion());
		parameters.put("tipoOperacion", complement.getOperationType());
		parameters.put("serieFolio", additionals.get(BancomextAdditional.SERIE_FOLIO));
		parameters.put("lugarFechaExpedicion", voucher.getPostalCode() + " - " + voucher.getDateTimeExpedition());
	}

	private void populateForeignExchangeSales(BancomextEdc edc, final Map<String, Object> parameters) {
		val sales = edc.getForeignExchange().getSales();
		parameters.put("movimientoMesList", sales);
	}

	private void populateForeignExchangeTransactionSummary(BancomextEdc edc, final Map<String, Object> parameters) {
		val transactionSummary = edc.getForeignExchange().getTransactionSummary();
		if (transactionSummary != null) {
			parameters.put("montoTotalVentasUsd", transactionSummary.getMontoTotalVentasUsd());
			parameters.put("montoTotalVentasMxn", transactionSummary.getMontoTotalVentasMxn());
		}
	}

	private void populateCreditData(BancomextEdc edc, final Map<String, Object> parameters) {
		val data = edc.getCredit().getData();
		parameters.put("periodo", data.getPeriod());
		parameters.put("diasPeriodo", data.getPeriodDays());
		parameters.put("cliente", data.getCustomerNumber());
		parameters.put("lineaCredito", data.getCreditLine());
		parameters.put("moneda", data.getCurrency());
		parameters.put("montoAutorizado", data.getAuthorizedAmount());
		parameters.put("montoDisponible", data.getAvailableAmount());
		parameters.put("direccion", edc.getAddress().getFullAddress());
		parameters.put("monedaSaldoCapital", data.getCurrencyCapitalBalance());
		parameters.put("saldoInicial", data.getOpeningBalance());
		parameters.put("montoDispuesto", data.getDisposedAmount());
		parameters.put("montoRecuperadoCapital", data.getRecoveredPrincipalAmount());
		parameters.put("saldoInsolutoCapital", data.getOutstandingPrincipalBalance());
		parameters.put("totalComisionesCobradas", data.getTotalFeesCollected());
	}

	private void populateCreditRetrieveds(BancomextEdc edc, final Map<String, Object> parameters) {
		val retrieveds = edc.getCredit().getRetrieveds();
		parameters.put("movimientoMesList", retrieveds);
	}
	
	private void populateCreditDispositions(BancomextEdc edc, final Map<String, Object> parameters) {
		val dispositions = edc.getCredit().getDispositions();
		parameters.put("disposicionesList", dispositions);
	}
	
	private void populateCreditFees(BancomextEdc edc, final Map<String, Object> parameters) {
		val fees = edc.getCredit().getFees();
		parameters.put("comisionesList", fees);
	}

}