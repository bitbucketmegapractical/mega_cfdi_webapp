package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextTreasuryFlowReport;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextTreasury {
	private BancomextTreasuryData data;
	@Getter(AccessLevel.NONE)
	private List<BancomextTreasuryFlowReport> flows;
	private BancomextTreasurySummary summary;
	@Getter(AccessLevel.NONE)
	private List<BancomextTreasuryFlowReport> currentValues;
	@Getter(AccessLevel.NONE)
	private List<BancomextTreasuryFlowReport> guaranteeValues;
	private BancomextTreasuryCurrentValueTotal currentValueTotal;
	private BancomextTreasuryCurrentValueTotal guaranteeValueTotal;
	
	public List<BancomextTreasuryFlowReport> getFlows() {
		if (flows == null) {
			flows = new ArrayList<>();
		}
		return flows;
	}

	public List<BancomextTreasuryFlowReport> getCurrentValues() {
		if (currentValues == null) {
			currentValues = new ArrayList<>();
		}
		return currentValues;
	}

	public List<BancomextTreasuryFlowReport> getGuaranteeValues() {
		if (guaranteeValues == null) {
			guaranteeValues = new ArrayList<>();
		}
		return guaranteeValues;
	}
}