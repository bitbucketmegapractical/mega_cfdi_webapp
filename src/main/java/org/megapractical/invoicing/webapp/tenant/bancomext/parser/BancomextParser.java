package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

import static org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.isEqual;

import java.io.File;
import java.util.List;

import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.webapp.filereader.FileReader;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.FileEntry;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextParserType.ParserType;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextParseData;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class BancomextParser {

	private final BancomextCfdiParser bancomextCfdiParser;
	private final BancomextPayrollParser bancomextPayrollParser;
	private final BancomextEdcParser bancomextEdcParser;

	public BancomextParseData parse(File localFile, ContribuyenteCertificadoEntity certificate) {
		val entries = FileReader.readFile(localFile);

		String identifyParser = null;
		for (val item : entries) {
			if (isEqual(item[0], FileEntry.ENTRY_VOUCHER)) {
				identifyParser = item[2];
				break;
			}
		}

		val parserType = BancomextParserType.getParser(identifyParser);
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT PARSER > PARSER TYPE: " + parserType);
		return parse(entries, certificate, parserType);
	}

	private BancomextParseData parse(List<String[]> entries, 
									 ContribuyenteCertificadoEntity certificate,
									 ParserType parserType) {
		var bpd = new BancomextParseData();
		if (ParserType.CFDI.equals(parserType)) {
			bpd = bancomextCfdiParser.parse(entries, certificate);
		} else if (ParserType.PAYROLL.equals(parserType)) {
			bpd = bancomextPayrollParser.parse(entries, certificate);
		} else if (ParserType.EDC.equals(parserType)) {
			bpd = bancomextEdcParser.parse(entries, certificate);
		}
		return bpd;
	}

}