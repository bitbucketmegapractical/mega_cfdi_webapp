package org.megapractical.invoicing.webapp.tenant.bancomext.report;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextCreditFeeReport {
	private String descripcionComision;
	private String fechaPagoComision;
	private String baseCalculoComision;
	private String porcentajeComision;
	private String montoComision;
	private String ivaComision;

	/* Getters and Setters */
	public String getDescripcionComision() {
		return descripcionComision;
	}

	public void setDescripcionComision(String descripcionComision) {
		this.descripcionComision = descripcionComision;
	}

	public String getFechaPagoComision() {
		return fechaPagoComision;
	}

	public void setFechaPagoComision(String fechaPagoComision) {
		this.fechaPagoComision = fechaPagoComision;
	}

	public String getBaseCalculoComision() {
		return baseCalculoComision;
	}

	public void setBaseCalculoComision(String baseCalculoComision) {
		this.baseCalculoComision = baseCalculoComision;
	}

	public String getPorcentajeComision() {
		return porcentajeComision;
	}

	public void setPorcentajeComision(String porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	public String getMontoComision() {
		return montoComision;
	}

	public void setMontoComision(String montoComision) {
		this.montoComision = montoComision;
	}

	public String getIvaComision() {
		return ivaComision;
	}

	public void setIvaComision(String ivaComision) {
		this.ivaComision = ivaComision;
	}
	
}