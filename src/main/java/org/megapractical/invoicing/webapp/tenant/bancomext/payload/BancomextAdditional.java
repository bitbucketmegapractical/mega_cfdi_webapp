package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

public final class BancomextAdditional {
	
	private BancomextAdditional() {		
	}
	
	public static final String RECEIVER_TAX_REGIME = "receiverTaxRegime";
	
	public static final String RECEIVER_CFDI_USE = "receiverCfdiUse";
	
	public static final String RECEIVER_ADDRESS = "receiverAddress";
	
	public static final String TRANSFERRED_TAX = "transferredTax";
	
	public static final String VOUCHER_IVA16 = "iva16";
	
	public static final String NEGOCIATION = "negociation";
	
	public static final String DATE_VALUE = "dateValue";
	
	public static final String EMAIL = "email";
	
	public static final String EDC_TYPE = "edcType";
	
	public static final String SERIE_FOLIO = "serieFolio";
	
}