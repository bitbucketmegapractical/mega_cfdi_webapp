package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextInvoice {
	private String documentoFactura;
	private String prestamo;
	private String plazo;
	private String periodoInicioFin;
	private String tasa;
	private String importe;
	
	public String getDocumentoFactura() {
		return documentoFactura;
	}

	public void setDocumentoFactura(String documentoFactura) {
		this.documentoFactura = documentoFactura;
	}

	public String getPrestamo() {
		return prestamo;
	}

	public void setPrestamo(String prestamo) {
		this.prestamo = prestamo;
	}

	public String getPlazo() {
		return plazo;
	}

	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}

	public String getPeriodoInicioFin() {
		return periodoInicioFin;
	}

	public void setPeriodoInicioFin(String periodoInicioFin) {
		this.periodoInicioFin = periodoInicioFin;
	}

	public String getTasa() {
		return tasa;
	}

	public void setTasa(String tasa) {
		this.tasa = tasa;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}
	
}