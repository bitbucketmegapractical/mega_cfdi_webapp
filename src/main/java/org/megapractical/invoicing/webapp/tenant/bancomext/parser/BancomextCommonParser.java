package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

import static org.megapractical.invoicing.api.util.UValue.stringValue;
import static org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.isEqual;

import java.math.BigDecimal;
import java.util.Arrays;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi;
import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiEmitter;
import org.megapractical.invoicing.api.wrapper.ApiReceiver;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiUseJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxRegimeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.sat.catalog.SatCatalog;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.FileEntry;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class BancomextCommonParser {
	
	private final SatCatalog satCatalog;
	private final ITaxRegimeJpaRepository iTaxRegimeJpaRepository;
	private final ICfdiUseJpaRepository iCfdiUseJpaRepository;
	
	@Setter
	private ContribuyenteCertificadoEntity certificate;
	
	public void fillVoucher(final ApiCfdi cfdi, final String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_VOUCHER)) {
			val voucher = new ApiVoucher();
			voucher.setVersion(stringValue(entry[1]));
			voucher.setSerie(stringValue(entry[2]));
			voucher.setFolio(stringValue(entry[3]));
			voucher.setDateTimeExpedition(stringValue(entry[4]));
			
			val paymentWay = stringValue(entry[5]);
			voucher.setPaymentWay(UCatalog.formaPago(paymentWay));
			voucher.setPaymentWayString(satCatalog.getPaymentWayPrint(paymentWay));
			
			val paymentConditions = stringValue(entry[7]);
			if (!UValidator.isNullOrEmpty(paymentConditions)) {
				voucher.setPaymentConditions(paymentConditions);
			}
			
			voucher.setSubTotal(UValue.bigDecimalStrict(stringValue(entry[8])));
			voucher.setSubTotalString(UValue.bigDecimalString(stringValue(entry[8])));
			
			if (!UValidator.isNullOrEmpty(stringValue(entry[9]))) {
				val discount = UValue.bigDecimalStrict(stringValue(entry[9]));
				if (discount.compareTo(BigDecimal.ZERO) != 0) {
    				voucher.setDiscount(discount);
    				voucher.setDiscountString(UValue.bigDecimalString(stringValue(entry[9])));
				}
			}
			
			val currency = stringValue(entry[10]);
			voucher.setCurrency(UCatalog.moneda(currency));
			voucher.setCurrencyString(satCatalog.getCurrencyPrint(currency));
			
			voucher.setChangeType(UValue.bigDecimalStrict(stringValue(entry[11])));
			voucher.setChangeTypeString(UValue.bigDecimalString(stringValue(entry[11])));
			
			voucher.setTotal(UValue.bigDecimalStrict(stringValue(entry[12])));
			voucher.setTotalString(UValue.bigDecimalString(stringValue(entry[12])));
			
			val voucherType = stringValue(entry[13]);
			voucher.setVoucherType(UCatalog.tipoDeComprobante(voucherType));
			voucher.setVoucherTypeString(satCatalog.getVoucherTypePrint(voucherType));
			
			val exportation = stringValue(entry[14]);
			voucher.setExportation(exportation);
			voucher.setExportationString(satCatalog.getExportationPrint(exportation));
			
			val paymentMethod = stringValue(entry[15]);
			voucher.setPaymentMethod(UCatalog.metodoPago(paymentMethod));
			voucher.setPaymentMethodString(satCatalog.getPaymentMethodPrint(paymentMethod));
			
			voucher.setPostalCode(stringValue(entry[16]));
			
			cfdi.setVoucher(voucher);
		}
	}
	
	public void fillEmitter(final ApiCfdi cfdi, final String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_EMITTER)) {
			val emitter = new ApiEmitter();
			emitter.setRfc(stringValue(entry[1]));
			emitter.setNombreRazonSocial(stringValue(entry[2]));
			
			val taxRegime = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalse(stringValue(entry[3]));
			emitter.setRegimenFiscal(taxRegime);
			emitter.setCertificado(this.certificate.getCertificado());
			emitter.setLlavePrivada((this.certificate.getLlavePrivada())); 
			emitter.setPasswd(UBase64.base64Decode(this.certificate.getClavePrivada()));
			
			cfdi.setEmitter(emitter);
		}
	}
	
	public void fillReceiver(final ApiCfdi cfdi, final String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_RECEIVER)) {
			val receiver = new ApiReceiver();
			receiver.setRfc(stringValue(entry[1]));
			receiver.setNombreRazonSocial(stringValue(entry[2]));
			receiver.setDomicilioFiscal(stringValue(entry[3]));
			receiver.setResidenciaFiscal(null);

			val numRegIdTrib = stringValue(entry[5]);
			if (!UValidator.isNullOrEmpty(numRegIdTrib)) {
				receiver.setNumRegIdTrib(numRegIdTrib);
			}

			receiver.setTaxRegime(stringValue(entry[6]));
			val receiverTaxRegime = satCatalog.getTaxRegimePrint(receiver.getTaxRegime());
			addAdditional(cfdi, BancomextAdditional.RECEIVER_TAX_REGIME, receiverTaxRegime);

			val cfdiUse = iCfdiUseJpaRepository.findByCodigoAndEliminadoFalse(stringValue(entry[7]));
			receiver.setUsoCFDI(cfdiUse);
			addAdditional(cfdi, BancomextAdditional.RECEIVER_CFDI_USE, cfdiUse.getCodigo() + " " + cfdiUse.getValor());

			cfdi.setReceiver(receiver);
		}
	}
	
	public void fillRelatedCfdis(final ApiCfdi cfdi, String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_RELATED_CFDI)) {
			val relationshipType = stringValue(entry[1]);
			val uuid = stringValue(entry[3]);
				
			val relatedCfdi = ApiRelatedCfdi
								.builder()
								.relationshipTypeCode(relationshipType)
								.relationshipTypeValue(satCatalog.getRelationshipTypePrint(relationshipType))
								.uuids(Arrays.asList(RelatedCfdiUuid.builder().uuid(uuid).build()))
								.build();
			
			cfdi.getVoucher().setRelatedCfdi(relatedCfdi);
		}
	}
	
	public void fillConcept(final ApiCfdi cfdi, String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_CONCEPT)) {
			val concept = new ApiConcept();
			concept.setKeyProductServiceCode(stringValue(entry[1]));
			concept.setKeyProductService(satCatalog.getKeyProductServicePrint(stringValue(entry[1])));
			concept.setIdentificationNumber(stringValue(entry[2]));
			concept.setQuantity(UValue.bigDecimalStrict(stringValue(entry[3])));
			concept.setQuantityString(stringValue(entry[3]));
			concept.setMeasurementUnitCode(stringValue(entry[4]));
			concept.setMeasurementUnit(satCatalog.getMeasurementUnitValue(stringValue(entry[4])));
			concept.setMeasurementUnitPrint(satCatalog.getMeasurementUnitPrint(stringValue(entry[4])));
			concept.setUnit(stringValue(entry[5]));
			concept.setDescription(stringValue(entry[6]));
			concept.setUnitValue(UValue.bigDecimalStrict(stringValue(entry[7])));
			concept.setUnitValueString(UValue.bigDecimalString(stringValue(entry[7])));
			concept.setAmount(UValue.bigDecimalStrict(stringValue(entry[8])));
			concept.setAmountString(UValue.bigDecimalString(stringValue(entry[8])));
			
			if (!UValidator.isNullOrEmpty(stringValue(entry[9]))) {
				val discount = UValue.bigDecimalStrict(stringValue(entry[9]));
				if (discount.compareTo(BigDecimal.ZERO) != 0) {
    				concept.setDiscount(discount);
    				concept.setDiscountString(UValue.bigDecimalString(stringValue(entry[9])));
				}
			}
			
			concept.setObjImp(stringValue(entry[10]));
			
			if (entry.length > 11) {
				if (isEqual(entry[11], FileEntry.ENTRY_TAX_T_NODE) && isEqual(entry[12], FileEntry.ENTRY_TAX_T_TYPE)) {
					fillConceptTaxTransferred(concept, entry);
				} else if (isEqual(entry[11], FileEntry.ENTRY_TAX_R_NODE) && isEqual(entry[12], FileEntry.ENTRY_TAX_R_TYPE)) {
					fillConceptTaxWithheld(concept, entry);
				}
			}

			cfdi.getConcepts().add(concept);
		}
	}
	
	private void fillConceptTaxTransferred(final ApiConcept concept, String[] entry) {
		val transferredTax = new ApiConceptTaxes();
		transferredTax.setBaseBigDecimal(UValue.bigDecimalStrict(stringValue(entry[13])));
		transferredTax.setBase(UValue.bigDecimalString(stringValue(entry[13])));
		transferredTax.setTax(stringValue(entry[14]));
		transferredTax.setTaxPrint(satCatalog.getTaxTypePrint(transferredTax.getTax()));
		transferredTax.setFactorType(UCatalog.factorType(stringValue(entry[15])));
		transferredTax.setFactor(stringValue(entry[15]));
		transferredTax.setRateOrFeeBigDecimal(UValue.bigDecimalStrict(stringValue(entry[16])));
		transferredTax.setRateOrFee(UValue.bigDecimalString(stringValue(entry[16])));
		val amount = UValue.bigDecimalStrict(stringValue(entry[17]));
		transferredTax.setAmountBigDecimal(amount);
		transferredTax.setAmount(UValue.bigDecimalString(stringValue(entry[17])));
		
		concept.getTaxesTransferred().add(transferredTax);
	}
	
	private void fillConceptTaxWithheld(final ApiConcept concept, String[] entry) {
		val withheldTax = new ApiConceptTaxes();
		withheldTax.setBaseBigDecimal(UValue.bigDecimalStrict(stringValue(entry[13])));
		withheldTax.setBase(UValue.bigDecimalString(UValue.stringValue(entry[13])));
		withheldTax.setTax(stringValue(entry[14]));
		withheldTax.setTaxPrint(satCatalog.getTaxTypePrint(withheldTax.getTax()));
		withheldTax.setFactorType(UCatalog.factorType(stringValue(entry[15])));
		withheldTax.setFactor(stringValue(entry[15]));
		withheldTax.setRateOrFeeBigDecimal(UValue.bigDecimalStrict(stringValue(entry[16])));
		withheldTax.setRateOrFee(UValue.bigDecimalString(stringValue(entry[16])));
		val amount = UValue.bigDecimalStrict(stringValue(entry[17]));
		withheldTax.setAmountBigDecimal(amount);
		withheldTax.setAmount(UValue.bigDecimalString(stringValue(entry[17])));
		
		concept.getTaxesWithheld().add(withheldTax);
	}
	
	public void addAdditional(final ApiCfdi cfdi, String key, String value) {
		cfdi.getAdditionals().put(key, value);
	}
}