package org.megapractical.invoicing.webapp.tenant.bancomext.report;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextTreasuryFlowReport {
	private String fechaInicio;
	private String fechaVenCim;
	private String concepto;
	private String debe;
	private String haber;
	private String saldo;
	private String tasa;
	private String intereses;
	private String tcmn;
	private String isr;
	private String isrmn;
	private String isrRet;
	private String pzo;
	private String plazoDias;
	private String descripcion;
	private String emisora;
	private String titulos;
	private String precio;
	private String impNeto;
	private String importe;
	private String rend;
	private String premio;
	private String dxv;

	/* Getters and Setters */
	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaVenCim() {
		return fechaVenCim;
	}

	public void setFechaVenCim(String fechaVenCim) {
		this.fechaVenCim = fechaVenCim;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getDebe() {
		return debe;
	}

	public void setDebe(String debe) {
		this.debe = debe;
	}

	public String getHaber() {
		return haber;
	}

	public void setHaber(String haber) {
		this.haber = haber;
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	public String getTasa() {
		return tasa;
	}

	public void setTasa(String tasa) {
		this.tasa = tasa;
	}

	public String getIntereses() {
		return intereses;
	}

	public void setIntereses(String intereses) {
		this.intereses = intereses;
	}

	public String getTcmn() {
		return tcmn;
	}

	public void setTcmn(String tcmn) {
		this.tcmn = tcmn;
	}

	public String getIsr() {
		return isr;
	}

	public void setIsr(String isr) {
		this.isr = isr;
	}

	public String getIsrmn() {
		return isrmn;
	}

	public void setIsrmn(String isrmn) {
		this.isrmn = isrmn;
	}

	public String getIsrRet() {
		return isrRet;
	}

	public void setIsrRet(String isrRet) {
		this.isrRet = isrRet;
	}

	public String getPzo() {
		return pzo;
	}

	public void setPzo(String pzo) {
		this.pzo = pzo;
	}

	public String getPlazoDias() {
		return plazoDias;
	}

	public void setPlazoDias(String plazoDias) {
		this.plazoDias = plazoDias;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEmisora() {
		return emisora;
	}

	public void setEmisora(String emisora) {
		this.emisora = emisora;
	}

	public String getTitulos() {
		return titulos;
	}

	public void setTitulos(String titulos) {
		this.titulos = titulos;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getImpNeto() {
		return impNeto;
	}

	public void setImpNeto(String impNeto) {
		this.impNeto = impNeto;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getRend() {
		return rend;
	}

	public void setRend(String rend) {
		this.rend = rend;
	}

	public String getPremio() {
		return premio;
	}

	public void setPremio(String premio) {
		this.premio = premio;
	}

	public String getDxv() {
		return dxv;
	}

	public void setDxv(String dxv) {
		this.dxv = dxv;
	}

}