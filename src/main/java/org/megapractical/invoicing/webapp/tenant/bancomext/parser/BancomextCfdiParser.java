package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

import static org.megapractical.invoicing.api.util.MoneyUtils.format;
import static org.megapractical.invoicing.api.util.MoneyUtils.formatStrict;
import static org.megapractical.invoicing.api.util.UValue.stringSafeValue;
import static org.megapractical.invoicing.api.util.UValue.stringToByte;
import static org.megapractical.invoicing.api.util.UValue.stringValue;
import static org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.isEqual;
import static org.megapractical.invoicing.webapp.util.EntryUtils.nextElement;
import static org.megapractical.invoicing.webapp.util.EntryUtils.posInArray;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiVoucherTaxes;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiCmp;
import org.megapractical.invoicing.api.wrapper.ApiPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.BaseTax;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.RelatedDocument;
import org.megapractical.invoicing.api.wrapper.ApiPayment.PaymentTotal;
import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.webapp.exposition.sat.catalog.SatCatalog;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.FileEntry;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAddress;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextCfdi;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextInvoice;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextParseData;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextSerie;
import org.megapractical.invoicing.webapp.tenant.bancomext.service.BancomextAreaService;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;
import org.megapractical.invoicing.webapp.util.PaymentUtils;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class BancomextCfdiParser {
	
	private final SatCatalog satCatalog;
	private final BancomextCommonParser bancomextCommonParser;
	private final BancomextAreaService bancomextAreaService;
	
	public BancomextParseData parse(List<String[]> entries, ContribuyenteCertificadoEntity certificate) {
		ApiCfdi cfdi = null;
		ApiPayment apiPayment = null;
		var invoices = new ArrayList<BancomextInvoice>();
		val bancomextCfdis = new ArrayList<BancomextCfdi>();
		bancomextCommonParser.setCertificate(certificate);
		String summaryEmailInfo = null;
		String entry = null;
		val parserErrors = new ArrayList<String>();
		StringBuilder xmlPdfNameBuilder = null;
		int line = 1;
		Area area = null;
		
		try {
			BancomextEntryValidator.validateEntries(BancomextEntry.REQUIRED_CFDI_EDC_ENTRIES, entries, parserErrors);
			if (parserErrors.isEmpty()) {
				for (int i = 0; i < entries.size(); i++) {
					line ++;
					
					val item = entries.get(i);
					if (item.length > 0) {
						entry = item[0];
						if (isEqual(item[0], FileEntry.ENTRY_LOTE)) {
							summaryEmailInfo = stringValue(item[item.length - 1]);
						}
						
						if (isEqual(item[0], FileEntry.ENTRY_DOC)) {
							cfdi = new ApiCfdi();
							xmlPdfNameBuilder = new StringBuilder();
							apiPayment = new ApiPayment();
							invoices = new ArrayList<>();
							
							xmlPdfNameBuilder.append(stringValue(item[4]));
							
							val email = stringValue(item[item.length - 1]);
							bancomextCommonParser.addAdditional(cfdi, BancomextAdditional.EMAIL, email);
						}
						
						if (isEqual(item[0], FileEntry.ENTRY_VOUCHER)) {
							xmlPdfNameBuilder.append("_" + stringValue(item[2]));
							xmlPdfNameBuilder.append("_" + stringValue(item[3]));
							xmlPdfNameBuilder.append("_<RECEIVER_RFC>");
							xmlPdfNameBuilder.append("_" + stringValue(item[4]).replace("T", "_").replace(":", "_"));
							
							if (area == null) {
								area = bancomextAreaService.findBySerie(stringValue(item[2]));
							}
						}
						
						populateCfdi(cfdi, apiPayment, invoices, item);
						if (cfdi != null && isEqual(item[0], FileEntry.ENTRY_ADDRESSES)) {
							val xmlPdfName = xmlPdfNameBuilder.toString().replace("<RECEIVER_RFC>", cfdi.getReceiver().getRfc());
							cfdi.setXmlPdfName(xmlPdfName);
							
							fillComplementAndAddress(cfdi, apiPayment, item);
							val bancomextCfdi = new BancomextCfdi(cfdi, invoices);
							bancomextCfdis.add(bancomextCfdi);
						}
					}
				}
			} else {
				summaryEmailInfo = BancomextFileErrorWrapper.CFDI_ERROR_EMAIL;
			}
		} catch (Exception e) {
			UPrint.logWithLine(UProperties.env(), "[ERROR] BANCOMEXT CFDI PARSER > AN ERROR OCCURRED WHILE PARSING THE FILE");
			parserErrors.add(BancomextParseData.determineErrorCause(line, entry, e.getMessage()));
			summaryEmailInfo = BancomextFileErrorWrapper.CFDI_ERROR_EMAIL;
			e.printStackTrace();
		}
		
		return BancomextParseData
				.builder()
				.area(area)
				.summaryEmailInfo(summaryEmailInfo)
				.bancomextCfdis(bancomextCfdis)
				.parserErrors(parserErrors)
				.build();
	}

	private void populateCfdi(final ApiCfdi cfdi, 
							  final ApiPayment apiPayment, 
							  final List<BancomextInvoice> invoices,
							  String[] entry) {
		bancomextCommonParser.fillVoucher(cfdi, entry);
		bancomextCommonParser.fillEmitter(cfdi, entry);
		bancomextCommonParser.fillReceiver(cfdi, entry);
		bancomextCommonParser.fillRelatedCfdis(cfdi, entry);
		bancomextCommonParser.fillConcept(cfdi, entry);
		fillVoucherTax(cfdi, entry);
		fillComplementPayment(apiPayment, entry);
		fillRowData(cfdi, invoices, entry);
	}
	
	private void fillVoucherTax(final ApiCfdi cfdi, String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_TAX)) {
			val taxes = new ApiVoucherTaxes();
			taxes.setTotalTaxTransferred(UValue.bigDecimalStrict(stringValue(entry[2])));
			cfdi.getVoucher().setTaxes(taxes);
		}
	}
	
	private void fillComplementPayment(final ApiPayment apiPayment, String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_COMPLEMENT) && isEqual(entry[1], FileEntry.ENTRY_PAYMENT_TAG)) {
			fillPaymentTotals(apiPayment, entry);
			fillPayment(apiPayment, entry);
			fillRelDoc(apiPayment, entry);
			fillRelDocTransferred(apiPayment, entry);
			fillPaymentTransferred(apiPayment, entry);
		}
	}

	private void fillPaymentTotals(final ApiPayment apiPayment, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYMENT_TOTALS)) {
			val paymentTotal = PaymentTotal
								.builder()
								.totalWithheldIva(formatStrict(entry[3]))
								.totalWithheldIvaString(format(entry[3]))
								.totalWithheldIsr(formatStrict(entry[4]))
								.totalWithheldIsrString(format(entry[4]))
								.totalWithheldIeps(formatStrict(entry[5]))
								.totalWithheldIepsString(format(entry[5]))
								.totalTransferredBaseIva16(formatStrict(entry[6]))
								.totalTransferredBaseIva16String(format(entry[6]))
								.totalTransferredTaxIva16(formatStrict(entry[7]))
								.totalTransferredTaxIva16String(format(entry[7]))
								.totalTransferredBaseIva8(formatStrict(entry[8]))
								.totalTransferredBaseIva8String(format(entry[8]))
								.totalTransferredTaxIva8(formatStrict(entry[9]))
								.totalTransferredTaxIva8String(format(entry[9]))
								.totalTransferredBaseIva0(formatStrict(entry[10]))
								.totalTransferredBaseIva0String(format(entry[10]))
								.totalTransferredTaxIva0(formatStrict(entry[11]))
								.totalTransferredTaxIva0String(format(entry[11]))
								.totalTransferredBaseIvaExempt(formatStrict(entry[12]))
								.totalTransferredBaseIvaExemptString(format(entry[12]))
								.totalAmountPayments(formatStrict(entry[13]))
								.totalAmountPaymentsString(format(entry[13]))
								.build();
			apiPayment.setPaymentTotal(paymentTotal);
		}
	}
	
	private void fillPayment(final ApiPayment apiPayment, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYMENT)) {
			val payment = Payment
							.builder()
							.paymentDate(stringValue(entry[3]))
							.paymentWay(UCatalog.formaPago(stringValue(entry[4])))
							.paymentWayPrint(satCatalog.getPaymentWayPrint(stringValue(entry[4])))
							.currency(UCatalog.moneda(stringValue(entry[5])))
							.currencyPrint(satCatalog.getCurrencyPrint(stringValue(entry[5])))
							.changeType(formatStrict(entry[6]))
							.changeTypeString(format(entry[6]))
							.amount(formatStrict(entry[7]))
							.amountString(format(entry[7]))
							.operationNumber(stringValue(entry[8]))
							.sourceAccountRfc(stringValue(entry[9]))
							.bankName(stringValue(entry[10]))
							.payerAccount(stringValue(entry[11]))
							.targetAccountRfc(stringValue(entry[12]))
							.receiverAccount(stringValue(entry[13]))
							.stringType(stringValue(entry[14]))
							.paymentCertificate(stringToByte(stringValue(entry[15])))
							.originalString(stringValue(entry[16]))
							.paymentSeal(stringToByte(stringValue(entry[17])))
							.build();
			apiPayment.getPayments().add(payment);
		}
	}
	
	private void fillRelDoc(final ApiPayment apiPayment, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYMENT_REL_DOC)) {
			val relDoc = RelatedDocument
							.builder()
							.documentId(stringValue(entry[3]))
							.serie(stringValue(entry[4]))
							.sheet(stringValue(entry[5]))
							.currency(UCatalog.moneda(stringValue(entry[6])))
							.currencyPrint(satCatalog.getCurrencyPrint(stringValue(entry[6])))
							.changeType(formatStrict(entry[7]))
							.changeTypePrint(format(entry[7]))
							.partiality(Integer.valueOf(stringValue(entry[8])))
							.amountPartialityBefore(formatStrict(entry[9]))
							.amountPartialityBeforePrint(format(entry[9]))
							.amountPaid(formatStrict(entry[10]))
							.amountPaidPrint(format(entry[10]))
							.difference(formatStrict(entry[11]))
							.differencePrint(format(entry[11]))
							.taxObjectCode(stringValue(entry[12]))
							.build();
			
			apiPayment.getPayments().get(0).getRelatedDocuments().add(relDoc);
		}
	}
	
	private void fillRelDocTransferred(final ApiPayment apiPayment, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYMENT_REL_DOC_TRA)) {
			val baseTax = BaseTax
							.builder()
							.baseBigDecimal(formatStrict(entry[3]))
							.base(format(entry[3]))
							.tax(stringValue(entry[4]))
							.factorType(UCatalog.factorType(stringValue(entry[5])))
							.factor(stringValue(entry[5]))
							.rateOrFeeBigDecimal(formatStrict(entry[6]))
							.rateOrFee(format(entry[6]))
							.amountBigDecimal(formatStrict(entry[7]))
							.amount(format(entry[7]))
							.build();
			val lastRelDocPos = apiPayment.getPayments().get(0).getRelatedDocuments().size() - 1;
			apiPayment.getPayments().get(0).getRelatedDocuments().get(lastRelDocPos).getTransferreds().add(baseTax);
		}
	}
	
	private void fillPaymentTransferred(final ApiPayment apiPayment, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_PAYMENT_TRA)) {
			val baseTax = BaseTax
					.builder()
					.baseBigDecimal(formatStrict(entry[3]))
					.base(format(entry[3]))
					.tax(stringValue(entry[4]))
					.factorType(UCatalog.factorType(stringValue(entry[5])))
					.factor(stringValue(entry[5]))
					.rateOrFeeBigDecimal(formatStrict(entry[6]))
					.rateOrFee(format(entry[6]))
					.amountBigDecimal(formatStrict(entry[7]))
					.amount(format(entry[7]))
					.build();
			apiPayment.getPayments().get(0).getTransferreds().add(baseTax);
		}
	}
	
	private void fillComplementAndAddress(final ApiCfdi cfdi, final ApiPayment apiPayment, String[] entry) {
		if (isEqual(entry[2], FileEntry.ENTRY_RECEIVER)) {
			fillReceiverAddress(cfdi, entry);
		}
		
		if (apiPayment != null && !apiPayment.getPayments().isEmpty()) {
			val apiCmp = new ApiCmp(PaymentUtils.PAYMENT_PACKAGE, apiPayment);
			cfdi.setComplement(apiCmp);
		}
	}
	
	private void fillReceiverAddress(final ApiCfdi cfdi, String[] entry) {
		val bancomextAddress = BancomextAddress
								.builder()
								.line1(stringSafeValue(entry[3]))
								.line2(stringSafeValue(entry[4]) + " " + stringSafeValue(entry[5]))
								.line3(stringSafeValue(entry[6]))
								.line4(stringSafeValue(entry[7]) + " " + stringSafeValue(entry[8]))
								.line5(stringSafeValue(entry[9]) + " " + stringSafeValue(entry[10]))
								.build();
		cfdi.getAdditionals().put("receiverAddress", bancomextAddress);
	}
	
	private void fillRowData(final ApiCfdi cfdi, final List<BancomextInvoice> invoices, String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_ROW)) {
			val serie = cfdi.getVoucher().getSerie();
			if (isEqual(entry[1], FileEntry.ENTRY_ROW_ADD01)) {
				cfdi.getVoucher().setTotalInLetter(stringValue(entry[10]));
			} else if (isEqual(entry[1], FileEntry.ENTRY_ROW_GENERAL)) {
				bancomextCommonParser.addAdditional(cfdi, BancomextAdditional.NEGOCIATION, stringValue(entry[13]));
				cfdi.getVoucher().setTotalInLetter(stringValue(entry[19]));
				
				val posInArray = posInArray(entry, FileEntry.ENTRY_ROW_DATE_VALUE.getValue());
				if (posInArray > -1) {
					val dateValue = nextElement(entry, posInArray);
					bancomextCommonParser.addAdditional(cfdi, BancomextAdditional.DATE_VALUE, dateValue);
				}
			} else if (isEqual(entry[1], FileEntry.ENTRY_ROW_ITEM)
					&& BancomextSerie.CRED_FAC.getValue().equalsIgnoreCase(serie)) {
				fillInvoiceData(invoices, entry);
			}
		}
	}
	
	private void fillInvoiceData(final List<BancomextInvoice> invoices, String[] entry) {
		if (isEqual(entry[3], FileEntry.ENTRY_ROW_DOCUMENT_INVOICE)) {
			val invoice = new BancomextInvoice();
			invoice.setDocumentoFactura(stringValue(entry[4]));
			invoice.setPrestamo(stringValue(entry[7]));
			invoice.setPlazo(stringValue(entry[10]));
			invoice.setPeriodoInicioFin(stringValue(entry[13]));
			invoice.setTasa(UValue.bigDecimalString(stringValue(entry[16])));
			invoice.setImporte(UValue.bigDecimalString(stringValue(entry[19])));
			invoices.add(invoice);
		}
	}
}