package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.FileEntry;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class BancomextEntryValidator {
	
	private BancomextEntryValidator() {
	}
	
	public static void validateEntries(List<FileEntry> fileEntries, List<String[]> entries, final List<String> errors) {
		val source = entries.stream()
							.map(entry -> BancomextEntry.removeNonPrintable(entry[0]).toLowerCase())
							.collect(Collectors.toList());
		
		fileEntries.stream()
				   .map(FileEntry::getValue)
				   .filter(i -> source.stream().noneMatch(s -> s.equalsIgnoreCase(i)))				   
				   .map(i -> BancomextFileErrorWrapper.REQUIRED_ENTRY.replace("{entry}", i))
				   .forEach(errors::add);
		
		val entryCountMap = fileEntries.stream()
								  	   .map(FileEntry::getValue)
								  	   .filter(i -> !i.equalsIgnoreCase(FileEntry.ENTRY_LOTE.getValue()))
								  	   .collect(Collectors.toMap(s -> s, s -> (int) source.stream().filter(s::equalsIgnoreCase).count()));
		
		int maxValue = entryCountMap.values().stream().max(Integer::compare).orElse(-1);
		val notMatchCount = entryCountMap.entrySet().stream()
        											.filter(entry -> entry.getValue() < maxValue)
        											.map(Map.Entry::getKey)
        											.collect(Collectors.toList());
		if (!notMatchCount.isEmpty()) {
			val keys = entryCountMap.entrySet().stream()
            								   .map(Map.Entry::getKey)
            								   .collect(Collectors.joining(", "));
			errors.add(BancomextFileErrorWrapper.MATCH_COUNT_ENTRY.replace("{entries}", keys));
		}
	}
	
}