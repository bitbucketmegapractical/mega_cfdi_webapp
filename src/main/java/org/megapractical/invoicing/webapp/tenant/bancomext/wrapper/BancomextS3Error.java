package org.megapractical.invoicing.webapp.tenant.bancomext.wrapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextS3Error {
	private String fileName;
	private String error;
}