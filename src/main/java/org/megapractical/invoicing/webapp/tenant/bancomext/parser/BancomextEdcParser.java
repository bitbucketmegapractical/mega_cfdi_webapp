package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

import static org.megapractical.invoicing.api.util.MoneyUtils.format;
import static org.megapractical.invoicing.api.util.MoneyUtils.formatSafe;
import static org.megapractical.invoicing.api.util.MoneyUtils.formatStrict;
import static org.megapractical.invoicing.api.util.MoneyUtils.formatWith4Decimal;
import static org.megapractical.invoicing.api.util.MoneyUtils.formatWithDecimalSafe;
import static org.megapractical.invoicing.api.util.UValue.stringSafeValue;
import static org.megapractical.invoicing.api.util.UValue.stringValue;
import static org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.isEqual;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiVoucherTaxes;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiCmp;
import org.megapractical.invoicing.api.wrapper.ApiForeignExchange;
import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.webapp.constant.ForeignExchangeConstants;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.BancomextEntry.FileEntry;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAddress;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextEdc;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextForeignExchangeTransactionSummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextParseData;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextTreasuryCurrentValueTotal;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextTreasuryData;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextTreasurySummary;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCreditDispositionReport;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCreditFeeReport;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCreditRetrievedReport;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextCreditRetrievedReport.BancomextCreditFlow;
import org.megapractical.invoicing.webapp.tenant.bancomext.service.BancomextAreaService;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextForeignExchangeSaleReport;
import org.megapractical.invoicing.webapp.tenant.bancomext.report.BancomextTreasuryFlowReport;
import org.megapractical.invoicing.webapp.util.EntryUtils;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class BancomextEdcParser {
	
	private final BancomextCommonParser bancomextCommonParser;
	private final BancomextAreaService bancomextAreaService;
	
	public BancomextParseData parse(List<String[]> entries, ContribuyenteCertificadoEntity certificate) {
		ApiCfdi cfdi = null;
		val bancomextEdcs = new ArrayList<BancomextEdc>();
		bancomextCommonParser.setCertificate(certificate);
		String summaryEmailInfo = null;
		EdcType edcType = null;
		boolean allowStamp = true;
		String contract = null;
		String code = null;
		String entry = null;
		val parserErrors = new ArrayList<String>();
		StringBuilder xmlPdfNameBuilder = null;
		int line = 1;
		Area area = null;
		
		try {
			BancomextEntryValidator.validateEntries(BancomextEntry.REQUIRED_CFDI_EDC_ENTRIES, entries, parserErrors);
			if (parserErrors.isEmpty()) {
				for (int i = 0; i < entries.size(); i++) {
					line ++;
					
					val item = entries.get(i);
					if (item.length > 0) {
						entry = item[0];
						if (isEqual(item[0], FileEntry.ENTRY_LOTE)) {
							summaryEmailInfo = stringValue(item[item.length - 1]);
						}
						
						if (isEqual(item[0], FileEntry.ENTRY_DOC)) {
							cfdi = new ApiCfdi();
							xmlPdfNameBuilder = new StringBuilder();
							allowStamp = BancomextEdc.allowStampEdc(stringValue(item[1]));
							contract = stringValue(item[8]);
							code = UValue.uuid();
							edcType = EdcType.valueOf(stringValue(item[4]));
							val email = stringValue(item[item.length - 1]);
							xmlPdfNameBuilder.append(edcType);
							
							populateDocumentValues(cfdi, edcType, item, email);
						}
						
						if (isEqual(item[0], FileEntry.ENTRY_VOUCHER)) {
							xmlPdfNameBuilder.append("_" + stringValue(item[2]));
							xmlPdfNameBuilder.append("_" + stringValue(item[3]));
							xmlPdfNameBuilder.append("_<RECEIVER_RFC>");
							xmlPdfNameBuilder.append("_" + stringValue(item[4]).replace("T", "_").replace(":", "_"));
							
							if (area == null) {
								area = bancomextAreaService.findBySerie(stringValue(item[2]));
							}
						}
						
						populateCfdi(cfdi, item, edcType);
						if (cfdi != null && isEqual(item[0], FileEntry.ENTRY_ADDRESSES)) {
							val xmlPdfName = xmlPdfNameBuilder.toString().replace("<RECEIVER_RFC>", cfdi.getReceiver().getRfc());
							cfdi.setXmlPdfName(xmlPdfName);
							
							val bancomextEdc = new BancomextEdc(cfdi, edcType, allowStamp);
							bancomextEdc.setContract(contract);
							bancomextEdc.setCode(code);
							
							fillAddress(bancomextEdc, item, edcType);
							bancomextEdcs.add(bancomextEdc);
						}
						populatePdfExtras(bancomextEdcs, code, item, edcType);					
					}
				}
			} else {
				summaryEmailInfo = BancomextFileErrorWrapper.CFDI_ERROR_EMAIL;
			}
		} catch (Exception e) {
			UPrint.logWithLine(UProperties.env(), "[ERROR] BANCOMEXT EDC PARSER > AN ERROR OCCURRED WHILE PARSING THE FILE");
			parserErrors.add(BancomextParseData.determineErrorCause(line, entry, e.getMessage()));
			summaryEmailInfo = BancomextFileErrorWrapper.CFDI_ERROR_EMAIL;
			e.printStackTrace();
		}
		
		return BancomextParseData
				.builder()
				.area(area)
				.bancomextEdcs(bancomextEdcs)
				.summaryEmailInfo(summaryEmailInfo)
				.parserErrors(parserErrors)
				.build();
	}

	private void populateDocumentValues(final ApiCfdi cfdi, EdcType edcType, String[] item, String email) {
		bancomextCommonParser.addAdditional(cfdi, BancomextAdditional.EDC_TYPE, edcType.name());
		bancomextCommonParser.addAdditional(cfdi, BancomextAdditional.EMAIL, email);
		if (EdcType.EDODIVISAS.equals(edcType)) {
			val serieFolio = stringValue(item[6]);
			bancomextCommonParser.addAdditional(cfdi, BancomextAdditional.SERIE_FOLIO, serieFolio);
		}
	}
	
	private void populateCfdi(final ApiCfdi cfdi, String[] entry, EdcType edcType) {
		bancomextCommonParser.fillVoucher(cfdi, entry);
		bancomextCommonParser.fillEmitter(cfdi, entry);
		bancomextCommonParser.fillReceiver(cfdi, entry);
		bancomextCommonParser.fillConcept(cfdi, entry);
		fillVoucherTax(cfdi, entry);
		
		if (EdcType.EDODIVISAS.equals(edcType)) {
			fillForeignExchangeComplement(cfdi, entry);
		}
	}
	
	private void fillVoucherTax(final ApiCfdi cfdi, String[] entry) {
		if (isEqual(entry[0], FileEntry.ENTRY_TAX)) {
			val taxes = new ApiVoucherTaxes();
			if (isEqual(entry[3], FileEntry.ENTRY_TAX_RET_NODE)) {
				taxes.setTotalTaxWithheld(formatStrict(entry[1])); 
			} else if (isEqual(entry[3], FileEntry.ENTRY_TAX_TRA_NODE)) {
				taxes.setTotalTaxTransferred(formatStrict(entry[2]));
			}
			cfdi.getVoucher().setTaxes(taxes);
		}
	}
	
	private void fillForeignExchangeComplement(final ApiCfdi cfdi, String[] entry) {
		if (isEqual(entry[1], FileEntry.ENTRY_FOREIGN_EXCHANGE)) {
			val foreignExchange = new ApiForeignExchange(stringValue(entry[2]), stringValue(entry[3]));
			val apiComp = new ApiCmp(ForeignExchangeConstants.PACKAGE, foreignExchange);
			cfdi.setComplement(apiComp);
		}
	}
	
	private void fillAddress(final BancomextEdc edc, String[] entry, EdcType edcType) {
		val address = new BancomextAddress();
		if (BancomextEdc.isTreasury(edcType) || BancomextEdc.isCredit(edcType)) {
			address.setStreet(stringValue(entry[3]));
			address.setNumber(stringValue(entry[4]));
			address.setInsideNumber(stringValue(entry[5]));
			address.setTown(stringValue(entry[6]));
			address.setCity(stringValue(entry[7]));
			address.setState(stringValue(entry[8]));
			address.setCountry(stringValue(entry[9]));
			address.setPostalCode(stringValue(entry[10]));
		}
		edc.setAddress(address);
	}
	
	private void populatePdfExtras(ArrayList<BancomextEdc> bancomextEdcs, String code, String[] entry, EdcType edcType) {
		val edc = bancomextEdcs.stream().filter(i -> i.getCode().equals(code)).findFirst().orElse(null);
		if (edc != null) {
			if (EdcType.TESORERIAINTERNACIONALTERCEROS.equals(edcType)) {
				fillIntTreasuryGeneral(entry, edc);
				fillIntTreasuryFlows(entry, edc);
			} else if (EdcType.EDOCTADPMN.equals(edcType)) {
				fillDepTreasuryGeneral(entry, edc);
				fillDepTreasuryFlows(entry, edc);
			} else if (EdcType.TESORERIANACIONAL.equals(edcType)) {
				fillNatTreasuryGeneral(entry, edc);
				fillNatTreasuryFlows(entry, edc);
				fillNatTreasuryCurrentValues(entry, edc);
				fillNatTreasuryGuaranteeValues(entry, edc);
			} else if (EdcType.EDODIVISAS.equals(edcType)) {
				fillForeignExchangeData(entry, edc);
				fillForeignExchangeSalesOperations(entry, edc);
				fillForeignExchangeTransactionSummary(entry, edc);
			} else if (EdcType.CREDITO.equals(edcType)) {
				fillCreditData(entry, edc);
				fillCreditRetrieveds(entry, edc);
				fillCreditDispositions(entry, edc);
				fillCreditFees(entry, edc);
			}
		}
	}
	
	private void fillIntTreasuryGeneral(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_GENERAL)) {
			val data = new BancomextTreasuryData();
			data.setPeriodStartDate(stringValue(entry[13]));
			data.setPeriodEndDate(stringValue(entry[16]));
			data.setPeriodDays(stringValue(entry[19]));
			data.setBranchCode(stringValue(entry[22]));
			data.setBranch(stringValue(entry[25]));
			data.setPromoter(stringValue(entry[31]));
			edc.getTreasury().setData(data);
			
			val summary = new BancomextTreasurySummary();
			summary.setSaldoInicial(format(entry[40]));
			summary.setPromedioSaldoDiario(format(entry[43]));
			summary.setRetiros(format(entry[46]));
			summary.setDepositos(format(entry[49]));
			summary.setInteresesGenerados(format(entry[52]));
			summary.setImpuestosRetenidosUsd(format(entry[55]));
			summary.setImpuestosRetenidosMn(format(entry[58]));
			summary.setTipoCambioPromedio(format(entry[61]));
			summary.setSaldoFinal(format(entry[64]));
			edc.getTreasury().setSummary(summary);
		}
	}
	
	private void fillIntTreasuryFlows(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_FLOWS)) {
			val flow = new BancomextTreasuryFlowReport();
			flow.setFechaInicio(stringSafeValue(entry[4]));
			flow.setConcepto(stringSafeValue(entry[7]));
			flow.setDebe(formatSafe(entry[10]));
			flow.setHaber(formatSafe(entry[13]));
			flow.setSaldo(formatSafe(entry[16]));
			flow.setPzo(stringSafeValue(entry[19]));
			flow.setTasa(formatWithDecimalSafe(entry[22]));
			flow.setIntereses(formatSafe(entry[25]));
			flow.setIsrRet(formatSafe(entry[28]));
			flow.setTcmn(formatWithDecimalSafe(entry[31]));
			flow.setIsrmn(formatWithDecimalSafe(entry[34]));
			edc.getTreasury().getFlows().add(flow);
		}
	}
	
	private void fillDepTreasuryGeneral(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_HEADER)) {
			val data = new BancomextTreasuryData();
			data.setPromoter(stringValue(entry[4]));
			data.setBranch(stringValue(entry[7]));
			data.setPeriod(stringValue(entry[10]));
			data.setPeriodEndDate(stringValue(entry[13]));
			data.setPeriodDays(stringValue(entry[16]));
			edc.getTreasury().setData(data);
		} else if (isEqual(entry[1], FileEntry.ENTRY_ROW_SUMMARY_DATA)) {
			val summary = new BancomextTreasurySummary();
			summary.setSaldoInicial(format(entry[4]));
			summary.setPromedioSaldoDiario(format(entry[7]));
			summary.setRetiros(format(entry[10]));
			summary.setDepositos(format(entry[13]));
			summary.setInteresesGenerados(format(entry[16]));
			summary.setImpuestosRetenidosMn(format(entry[19]));
			summary.setTasaOCuota(format(entry[22]));
			summary.setImpuesto(format(entry[25]));
			summary.setBase(format(entry[28]));
			summary.setSaldoFinal(format(entry[34]));
			edc.getTreasury().setSummary(summary);
		}
	}
	
	private void fillDepTreasuryFlows(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_LINE)) {
			val flow = new BancomextTreasuryFlowReport();
			flow.setFechaInicio(stringSafeValue(entry[4]));
			flow.setFechaVenCim(stringSafeValue(entry[7]));
			flow.setConcepto(stringSafeValue(entry[10]));
			flow.setDebe(formatSafe(entry[13]));
			flow.setHaber(formatSafe(entry[16]));
			flow.setSaldo(formatSafe(entry[19]));
			flow.setPlazoDias(formatSafe(entry[22]));
			flow.setTasa(formatSafe(entry[25]));
			flow.setIntereses(formatSafe(entry[28]));
			flow.setIsrRet(formatSafe(entry[31]));
			edc.getTreasury().getFlows().add(flow);
		}
	}
	
	private void fillNatTreasuryGeneral(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_GENERAL)) {
			edc.setContract(stringValue(entry[10]));
			
			val data = new BancomextTreasuryData();
			data.setAtencion(UValue.fixQuotes(stringValue(entry[13])));
			data.setPeriodStartDate(stringValue(entry[16]));
			data.setPeriodEndDate(stringValue(entry[19]));
			data.setPeriodDays(stringValue(entry[22]));
			data.setBranchCode(stringValue(entry[25]));
			data.setBranch(stringValue(entry[28]));
			data.setPromoter(stringValue(entry[34]));
			edc.getTreasury().setData(data);
			
			val summary = new BancomextTreasurySummary();
			summary.setSaldoMesAnterior(format(entry[43]));
			summary.setPromedioSaldoDiario(format(entry[46]));
			summary.setVencimientos(format(entry[49]));
			summary.setInversiones(format(entry[52]));
			summary.setPremio(format(entry[55]));
			summary.setImpuestoRetenido(format(entry[58]));
			summary.setSaldoActual(format(entry[61]));
			summary.setIvnValVig(format(entry[64]));
			summary.setTotalDepositos(format(entry[67]));
			summary.setTotalRetiros(format(entry[70]));
			summary.setSaldoMesAnteriorGarantia(format(entry[73]));
			summary.setTotalTitulosGarantiaInicio(format(entry[76]));
			summary.setTotalTitulosGarantiaFin(format(entry[79]));
			summary.setSaldoActualGarantia(format(entry[82]));
			edc.getTreasury().setSummary(summary);
		}
	}

	private void fillNatTreasuryFlows(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_FLOWS_MONTH)) {
			val flow = new BancomextTreasuryFlowReport();
			flow.setFechaInicio(stringSafeValue(entry[4]));
			flow.setFechaVenCim(stringSafeValue(entry[7]));
			flow.setDescripcion(stringSafeValue(entry[10]));
			flow.setEmisora(stringSafeValue(entry[13]));
			flow.setTitulos(formatSafe(entry[16]));
			flow.setPrecio(formatSafe(entry[19]));
			flow.setImporte(formatSafe(entry[22]));
			flow.setTasa(formatSafe(entry[25]));
			flow.setPzo(stringSafeValue(entry[28]));
			flow.setRend(formatSafe(entry[31]));
			flow.setIsrRet(formatSafe(entry[34]));
			flow.setImpNeto(formatSafe(entry[37]));
			flow.setSaldo(formatSafe(entry[40]));
			edc.getTreasury().getFlows().add(flow);
		}
	}
	
	private void fillNatTreasuryCurrentValues(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_CURRENT_VALUES)) {
			if (!EntryUtils.contains(entry, "TOTALES")) {
				val currentValue = fillCurrentValue(entry);
				edc.getTreasury().getCurrentValues().add(currentValue);
			} else {
				val currentValueTotal = fillCurrentValueTotal(entry);
				edc.getTreasury().setCurrentValueTotal(currentValueTotal);
			}
		}
	}

	private void fillNatTreasuryGuaranteeValues(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_GUARANTEE_VALUES)) {
			if (!EntryUtils.contains(entry, "TOTALES")) {
				val guaranteeValue = fillCurrentValue(entry);
				edc.getTreasury().getGuaranteeValues().add(guaranteeValue);
			} else {
				val guaranteeValueTotal = fillCurrentValueTotal(entry);
				edc.getTreasury().setGuaranteeValueTotal(guaranteeValueTotal);
			}
		}
	}
	
	private BancomextTreasuryFlowReport fillCurrentValue(String[] entry) {
		val currentValue = new BancomextTreasuryFlowReport();
		currentValue.setFechaInicio(stringSafeValue(entry[4]));
		currentValue.setFechaVenCim(stringSafeValue(entry[7]));
		currentValue.setDescripcion(stringSafeValue(entry[10]));
		currentValue.setEmisora(stringSafeValue(entry[13]));
		currentValue.setTitulos(formatSafe(entry[16]));
		currentValue.setPrecio(formatSafe(entry[19]));
		currentValue.setImporte(formatSafe(entry[22]));
		currentValue.setTasa(formatSafe(entry[25]));
		currentValue.setPzo(stringSafeValue(entry[28]));
		currentValue.setDxv(formatSafe(entry[31]));
		currentValue.setPremio(formatSafe(entry[34]));
		currentValue.setIsr(formatSafe(entry[37]));
		currentValue.setImpNeto(formatSafe(entry[40]));
		return currentValue;
	}
	
	private BancomextTreasuryCurrentValueTotal fillCurrentValueTotal(String[] entry) {
		val currentValueTotal = new BancomextTreasuryCurrentValueTotal();
		currentValueTotal.setTotalesTitulos(formatSafe(entry[16]));
		currentValueTotal.setTotalesPrecio(formatSafe(entry[19]));
		currentValueTotal.setTotalesImporte(formatSafe(entry[22]));
		currentValueTotal.setTotalesTasa(formatSafe(entry[25]));
		currentValueTotal.setTotalesPlazo(formatSafe(entry[28]));
		currentValueTotal.setTotalesDxv(formatSafe(entry[31]));
		currentValueTotal.setTotalesPremio(formatSafe(entry[34]));
		currentValueTotal.setTotalesIsr(formatSafe(entry[37]));
		currentValueTotal.setTotalesImpNeto(formatSafe(entry[40]));
		return currentValueTotal;
	}
	
	private void fillForeignExchangeData(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_H1)) {
			edc.getForeignExchange().getData().setStatementDay(stringValue(entry[31]));
			edc.getForeignExchange().getData().setPeriodDays(stringValue(entry[34]));
		} else if (isEqual(entry[1], FileEntry.ENTRY_ROW_GENERAL)) {
			edc.getForeignExchange().getData().setPeriodFrom(stringValue(entry[4]));
			edc.getForeignExchange().getData().setPeriodTo(stringValue(entry[7]));
		} else if (isEqual(entry[1], FileEntry.ENTRY_ROW_CUSTOMER)) {
			edc.getForeignExchange().getData().setFullAddress(stringValue(entry[4]));
			edc.getForeignExchange().getData().setAccountNum(stringValue(entry[7]));
		}
	}
	
	private void fillForeignExchangeSalesOperations(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_SALES_OPERATIONS)) {
			val sale = new BancomextForeignExchangeSaleReport();
			sale.setFecha(stringValue(entry[4]));
			sale.setTipoOperacion(stringValue(entry[7]));
			sale.setMontoDivisas(stringValue(entry[10]));
			sale.setTipoCambio(stringValue(entry[13]));
			sale.setContraValor(stringValue(entry[16]));
			sale.setFechaOperacion(stringValue(entry[19]));
			sale.setIdTransaccion(stringValue(entry[22]));
			edc.getForeignExchange().getSales().add(sale);
		}
	}
	
	private void fillForeignExchangeTransactionSummary(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_SALES_TRANSACTIONS_SUMMARY)) {
			val transactionSummary = new BancomextForeignExchangeTransactionSummary();
			transactionSummary.setMontoTotalVentasUsd(format(entry[7]));
			transactionSummary.setMontoTotalVentasMxn(format(entry[13]));
			edc.getForeignExchange().setTransactionSummary(transactionSummary);
		}
	}
	
	private void fillCreditData(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_GENERAL)) {
			edc.getCredit().getData().setCustomerNumber(stringValue(entry[10]));
			edc.getCredit().getData().setCreditLine(stringValue(entry[13]));
			edc.getCredit().getData().setCurrency(stringValue(entry[16]));
			edc.getCredit().getData().setAuthorizedAmount(format(entry[19]));
			edc.getCredit().getData().setAvailableAmount(format(entry[22]));
			edc.getCredit().getData().setPeriod(stringValue(entry[31]));
			edc.getCredit().getData().setPeriodDays(stringValue(entry[34]));
			edc.getCredit().getData().setCurrencyCapitalBalance(stringValue(entry[37]));
			edc.getCredit().getData().setOpeningBalance(formatSafe(entry[40]));
			edc.getCredit().getData().setDisposedAmount(formatSafe(entry[43]));
			edc.getCredit().getData().setRecoveredPrincipalAmount(formatSafe(entry[46]));
			edc.getCredit().getData().setOutstandingPrincipalBalance(formatSafe(entry[49]));
			edc.getCredit().getData().setTotalFeesCollected(formatSafe(entry[52]));
		}
	}
	
	private void fillCreditRetrieveds(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_RETRIEVED)) {
			val retrieved = new BancomextCreditRetrievedReport();
			retrieved.setNoPrestamo(stringSafeValue(entry[10]));
			retrieved.setSaldoInicial(formatSafe(entry[13]));
			retrieved.setImporteCalculoInteresOrd(formatSafe(entry[16]));
			retrieved.setImporteCalculoInteresMoratorio(formatSafe(entry[19]));
			retrieved.setTasaInteresOrd(formatWith4Decimal(entry[22]));
			retrieved.setEsquemaTasaOrdinaria(stringSafeValue(entry[25]));
			retrieved.setTasaInteresMoratoria(formatWith4Decimal(entry[28]));
			retrieved.setEsquemaTasaMoratoria(stringSafeValue(entry[31]));
			retrieved.setVencidas(stringSafeValue(entry[34]));
			retrieved.setPagadas(stringSafeValue(entry[37]));
			retrieved.setPorPagar(stringSafeValue(entry[40]));
			edc.getCredit().getRetrieveds().add(retrieved);
		} else if (isEqual(entry[1], FileEntry.ENTRY_ROW_FLOWS)) {
			val retrieveds = edc.getCredit().getRetrieveds();
			if (!retrieveds.isEmpty()) {
				val flow = new BancomextCreditFlow();
				flow.setFechaPago(stringValue(entry[7]));
				flow.setCapital(formatSafe(entry[10]));
				flow.setPrepagosCapital(formatSafe(entry[13]));
				flow.setIvaCapital(formatSafe(entry[16]));
				flow.setInteresOrdinario(formatSafe(entry[19]));
				flow.setIvaInteresOrdinario(formatSafe(entry[22]));
				flow.setInteresesMoratorios(formatSafe(entry[25]));
				flow.setIvaInteresMoratorios(formatSafe(entry[28]));
				flow.setSubsidio(formatSafe(entry[31]));
				flow.setTotalPagado(formatSafe(entry[34]));
				
				val retrieved = retrieveds.get(retrieveds.size() - 1);
				retrieved.getMovimientosList().add(flow);
			}
		}
	}
	
	private void fillCreditDispositions(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_DISPOSITIONS)) {
			val disposition = new BancomextCreditDispositionReport();
			disposition.setNumPrestamo(stringSafeValue(entry[4]));
			disposition.setFechaDisposicion(stringSafeValue(entry[7]));
			disposition.setMontoDispuesto(formatSafe(entry[10]));
			edc.getCredit().getDispositions().add(disposition);
		}
	}
	
	private void fillCreditFees(String[] entry, final BancomextEdc edc) {
		if (isEqual(entry[1], FileEntry.ENTRY_ROW_FEES)) {
			val fee = new BancomextCreditFeeReport();
			fee.setDescripcionComision(stringSafeValue(entry[4]));
			fee.setFechaPagoComision(stringSafeValue(entry[7]));
			fee.setBaseCalculoComision(formatSafe(entry[10]));
			fee.setPorcentajeComision(formatSafe(entry[13]));
			fee.setMontoComision(formatSafe(entry[16]));
			fee.setIvaComision(formatSafe(entry[19]));
			edc.getCredit().getFees().add(fee);
		}
	}
}