package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import org.megapractical.invoicing.dal.bean.datatype.Area;

public interface BancomextAreaService {
	
	Area findBySerie(String serie);
	
}