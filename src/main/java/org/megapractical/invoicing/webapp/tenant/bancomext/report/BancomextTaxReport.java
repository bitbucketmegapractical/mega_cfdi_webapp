package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.BaseTax;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextTaxReport {
	private String base;
	private String impuesto;
	private String tipo;
	private String tasa;
	private String importe;

	public static final BancomextTaxReport convert(ApiConceptTaxes item) {
		BancomextTaxReport taxReport = new BancomextTaxReport();
		taxReport.setBase(item.getBase());
		taxReport.setImpuesto(item.getTaxPrint());
		taxReport.setTipo(item.getFactor());
		taxReport.setTasa(item.getRateOrFee());
		taxReport.setImporte(item.getAmount());
		return taxReport;
	}
	
	public static final BancomextTaxReport convert(BaseTax baseTax) {
		BancomextTaxReport taxReport = new BancomextTaxReport();
		taxReport.setBase(baseTax.getBase());
		taxReport.setImpuesto(baseTax.getTax());
		taxReport.setTipo(baseTax.getFactor());
		taxReport.setTasa(baseTax.getRateOrFee());
		taxReport.setImporte(baseTax.getAmount());
		return taxReport;
	}
	
	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTasa() {
		return tasa;
	}

	public void setTasa(String tasa) {
		this.tasa = tasa;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}
}