package org.megapractical.invoicing.webapp.tenant.bancomext.report;

public enum BancomextPdfType {
	CFDI, PAYMENT, PAYROLL, RET, EDC
}