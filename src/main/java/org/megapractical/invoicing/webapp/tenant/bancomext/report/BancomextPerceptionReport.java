package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextPerceptionReport {
	private String clave;
	private String tipoPercepcion;
	private String concepto;
	private String importeExento;
	private String importeGravado;

	public static final BancomextPerceptionReport convert(PayrollPerception item) {
		BancomextPerceptionReport perceptionReport = new BancomextPerceptionReport();
		perceptionReport.setClave(item.getKey());
		perceptionReport.setTipoPercepcion(item.getPerceptionTypePrint());
		perceptionReport.setConcepto(item.getConcept());
		perceptionReport.setImporteExento(item.getExemptAmount());
		perceptionReport.setImporteGravado(item.getTaxableAmount());
		return perceptionReport;
	}
	
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getTipoPercepcion() {
		return tipoPercepcion;
	}

	public void setTipoPercepcion(String tipoPercepcion) {
		this.tipoPercepcion = tipoPercepcion;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getImporteExento() {
		return importeExento;
	}

	public void setImporteExento(String importeExento) {
		this.importeExento = importeExento;
	}

	public String getImporteGravado() {
		return importeGravado;
	}

	public void setImporteGravado(String importeGravado) {
		this.importeGravado = importeGravado;
	}
	
}