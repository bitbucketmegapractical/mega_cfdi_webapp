package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;
import org.megapractical.invoicing.dal.data.repository.ISftpFileJpaRepository;
import org.springframework.stereotype.Service;

@Service
public class BancomextArchivoServiceImpl implements BancomextArchivoService {

	private final ISftpFileJpaRepository iArchivoSftpRepository;
	
	public BancomextArchivoServiceImpl(ISftpFileJpaRepository iArchivoSftpRepository) {
		this.iArchivoSftpRepository = iArchivoSftpRepository;
	}

	@Override
	public boolean existByName(String fileName) {
		return iArchivoSftpRepository.findByNombreIgnoreCaseAndEliminadoFalse(fileName).isPresent();
	}

	@Override
	public ArchivoStfpEntity save(ArchivoStfpEntity entity) {
		if (entity != null) {
			return iArchivoSftpRepository.save(entity);
		}
		return null;
	}

}