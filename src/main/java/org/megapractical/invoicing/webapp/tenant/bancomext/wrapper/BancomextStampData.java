package org.megapractical.invoicing.webapp.tenant.bancomext.wrapper;

import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextEdc;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextInvoice;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampDataWrapper.BancomextCfdiType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextStampData {
	private ApiCfdi cfdi;
	private ApiPayroll payroll;
	private BancomextEdc edc;
	private List<BancomextInvoice> invoices;
	private Comprobante voucher;
	private BancomextCfdiType cfdiType;
	@Default
	private boolean allowStamp = true;
	private BancomextVoucherDetail voucherDetail;
	
	public BancomextStampData(ApiCfdi cfdi, List<BancomextInvoice> invoices) {
		this.cfdi = cfdi;
		this.invoices = invoices;
		this.cfdiType = BancomextCfdiType.CFDI;
	}
	
	public BancomextStampData(ApiCfdi cfdi, List<BancomextInvoice> invoices, Comprobante voucher) {
		this.cfdi = cfdi;
		this.invoices = invoices;
		this.voucher = voucher;
		this.cfdiType = BancomextCfdiType.CFDI;
	}
	
	public BancomextStampData(ApiPayroll payroll) {
		this.payroll = payroll;
		this.cfdiType = BancomextCfdiType.PAYROLL;
	}
	
	public BancomextStampData(ApiPayroll payroll, Comprobante voucher) {
		this.payroll = payroll;
		this.voucher = voucher;
		this.cfdiType = BancomextCfdiType.PAYROLL;
	}
	
	public BancomextStampData(BancomextEdc edc, Comprobante voucher, boolean allowStamp) {
		this.edc = edc;
		this.voucher = voucher;
		this.cfdiType = BancomextCfdiType.EDC;
		this.allowStamp = allowStamp;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class BancomextVoucherDetail {
		private String emitterRfc;
		private String rfcOrCurp;
		private String serie;
		private String folio;
		private String uuid;
		
		public BancomextVoucherDetail(ApiCfdi apiCfdi, String uuid) {
			this.emitterRfc = apiCfdi.getEmitter().getRfc();
			this.rfcOrCurp = apiCfdi.getReceiver().getRfc();
			this.serie = apiCfdi.getVoucher().getSerie();
			this.folio = apiCfdi.getVoucher().getFolio();
			this.uuid = uuid;
		}
		
		public BancomextVoucherDetail(ApiPayroll apiPayroll, String uuid) {
			this.rfcOrCurp = apiPayroll.getPayrollReceiver().getCurp();
			this.serie = apiPayroll.getApiCfdi().getVoucher().getSerie();
			this.folio = apiPayroll.getApiCfdi().getVoucher().getFolio();
			this.uuid = uuid;
		}
	}
	
}