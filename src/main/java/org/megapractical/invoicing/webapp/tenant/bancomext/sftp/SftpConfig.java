package org.megapractical.invoicing.webapp.tenant.bancomext.sftp;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exception.FileNotFoundException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;

import com.jcraft.jsch.ChannelSftp.LsEntry;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Configuration
@RequiredArgsConstructor
public class SftpConfig {
	
	private final AppSettings appSettings;

	@Bean("cfdiSessionFactory")
	public SessionFactory<LsEntry> cfdiSftpSessionFactory() {
		val sftpHost = appSettings.getPropertyValue("sftp.host");
		val sftpPort = appSettings.getIntValue("sftp.port");
		val sftpUser = appSettings.getPropertyValue("sftp.user");
		val sftpPrivateKeyPath = appSettings.getPropertyValue("sftp.key.path");
		
		val factory = new DefaultSftpSessionFactory();
		factory.setHost(sftpHost);
		factory.setPort(sftpPort);
		factory.setUser(sftpUser);
		factory.setPrivateKey(privateKey(sftpPrivateKeyPath));
		factory.setAllowUnknownKeys(true);
		return new CachingSessionFactory<>(factory);
	}

	@Bean("cfdiSftpRemoteFileTemplate")
	public SftpRemoteFileTemplate cfdiSftpRemoteFileTemplate() {
		return new SftpRemoteFileTemplate(cfdiSftpSessionFactory());
	}
	
	@Bean("payrollSessionFactory")
	public SessionFactory<LsEntry> payrollStpSessionFactory() {
		val sftpHost = appSettings.getPropertyValue("sftp.host");
		val sftpPort = appSettings.getIntValue("sftp.port");
		val sftpUser = appSettings.getPropertyValue("sftp.payroll.user");
		val sftpPrivateKeyPath = appSettings.getPropertyValue("sftp.payroll.key.path");
		
		val factory = new DefaultSftpSessionFactory();
		factory.setHost(sftpHost);
		factory.setPort(sftpPort);
		factory.setUser(sftpUser);
		factory.setPrivateKey(privateKey(sftpPrivateKeyPath));
		factory.setAllowUnknownKeys(true);
		return new CachingSessionFactory<>(factory);
	}
	
	@Bean("payrollSftpRemoteFileTemplate")
	public SftpRemoteFileTemplate payrollSftpRemoteFileTemplate() {
		return new SftpRemoteFileTemplate(payrollStpSessionFactory());
	}

	private Resource privateKey(String sftpPrivateKeyPath) {
		try (val is = Thread.currentThread().getContextClassLoader().getResourceAsStream(sftpPrivateKeyPath)) {
			val bytes = IOUtils.toByteArray(is);
			return new ByteArrayResource(bytes) {
				@Override
				public String getFilename() {
					return sftpPrivateKeyPath;
				}
			};
		} catch (IOException e) {
			throw new FileNotFoundException("SftpConfig error. Unable to load private key file");
		}
	}
	
}