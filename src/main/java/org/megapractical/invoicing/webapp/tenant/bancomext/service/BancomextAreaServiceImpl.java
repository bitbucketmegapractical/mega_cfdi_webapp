package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.dal.bean.jpa.BancomextAreaEntity;
import org.megapractical.invoicing.webapp.tenant.bancomext.repository.BancomextAreaRepository;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BancomextAreaServiceImpl implements BancomextAreaService {

	private final BancomextAreaRepository bancomextAreaRepository;
	
	@Override
	public Area findBySerie(String serie) {
		serie = sanitizeSerie(serie);
		return bancomextAreaRepository.findBySerieIgnoreCaseAndEliminadoFalse(serie)
									  .map(BancomextAreaEntity::getArea)
									  .orElse(null);
	}
	
	private String sanitizeSerie(String serie) {
		if (!UValidator.isNullOrEmpty(serie)) {
			return serie.replace("vdz", "");
		}
		return null;
	}

}