package org.megapractical.invoicing.webapp.tenant.bancomext.wrapper;

import java.io.File;

import org.megapractical.invoicing.api.wrapper.ApiFileDetail;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.payload.UploadFilePath;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextFileWrapper {
	private ArchivoStfpEntity bancomextFile;
	private File localFile;
	private ApiFileDetail fileDetail;
	private UploadFilePath uploadFilePath;
}