package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextForeignExchangeData {
	private String periodFrom;
	private String periodTo;
	private String statementDay;
	private String periodDays;
	private String accountNum;
	private String fullAddress;
}