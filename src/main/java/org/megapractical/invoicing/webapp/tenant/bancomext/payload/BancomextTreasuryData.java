package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextTreasuryData {
	@Getter(AccessLevel.NONE)
	private String period;
	private String periodStartDate;
	private String periodEndDate;
	private String periodDays;
	private String branchCode;
	@Getter(AccessLevel.NONE)
	private String branch;
	private String promoter;
	private String atencion;
	
	public String getPeriod() {
		if (this.periodStartDate != null) {
			return this.periodStartDate + " al " + this.periodEndDate;
		}
		return period;
	}

	public String getBranch() {
		if (this.branchCode != null) {
			return this.branchCode + " " + this.branch;
		}
		return branch;
	}
	
}