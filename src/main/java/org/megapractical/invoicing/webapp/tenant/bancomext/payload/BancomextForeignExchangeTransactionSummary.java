package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextForeignExchangeTransactionSummary {
	private String montoTotalVentasUsd;
	private String montoTotalVentasMxn;
}