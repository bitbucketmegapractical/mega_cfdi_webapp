package org.megapractical.invoicing.webapp.tenant.bancomext.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData.BancomextVoucherDetail;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextStampDataWrapper {
	private BancomextFileWrapper fileWrapper;
	@Getter(AccessLevel.NONE)
	private List<BancomextStampData> stampDataSource;
	private String summaryEmailInfo;
	@Getter(AccessLevel.NONE)
	private List<BancomextVoucherDetail> voucherDetails;
	
	/* Getters */
	public List<BancomextStampData> getStampDataSource() {
		if (stampDataSource == null) {
			stampDataSource = new ArrayList<>();
		}
		return stampDataSource;
	}

	public List<BancomextVoucherDetail> getVoucherDetails() {
		if (voucherDetails == null) {
			voucherDetails = new ArrayList<>();
		}
		return voucherDetails;
	}
	
	public enum BancomextCfdiType {
		CFDI, PAYROLL, EDC
	}
}