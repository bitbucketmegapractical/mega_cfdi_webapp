package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.megapractical.invoicing.dal.bean.datatype.Origen;
import org.megapractical.invoicing.webapp.tenant.bancomext.ftp.FtpClient;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextZipData;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

@Service
@RequiredArgsConstructor
public class BancomextFtpServiceImpl implements BancomextFtpService {

	private final FtpClient ftpClient;
	private final BancomextArchivoService bancomextArchivoService;
	private final BancomextService bancomextService;

	private static final String FTP_IN_PATH = "BANCOMEX/IN";
	private static final String FTP_OUT_PATH = "BANCOMEX/OUT";
	private static final String FTP_ERROR_PATH = "BANCOMEX/ERROR";
	private static final Origen ORIGIN = Origen.FTP;
	
	@Override
	//@Scheduled(fixedRate = 10, timeUnit = TimeUnit.MINUTES)
	public void loadFiles() throws IOException {
		try {
			ftpClient.open();
			val files = ftpClient.listFiles(FTP_IN_PATH).stream()
														.filter(bancomextArchivoService::existByName)
														.collect(toList());
			files.forEach(this::processFile);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			ftpClient.close();
		}
	}

	private void processFile(String fileName) {
		try {
			val fileWrapper = bancomextService.createFile(fileName, ORIGIN);
			if (fileWrapper != null) {
				downloadFileFromFtp(fileWrapper);
				
				val bancomextStampResponse = bancomextService.parseAndStamp(fileWrapper);
				if (bancomextStampResponse != null) {
					val zipData = bancomextStampResponse.getZipData();
					// Upload to Sftp
					uploadFileToFtp(zipData);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void downloadFileFromFtp(BancomextFileWrapper fileWrapper) throws IOException {
		val ftpPath = String.format("%s/%s", FTP_IN_PATH, fileWrapper.getFileDetail().getOriginalFilename());
		ftpClient.downloadFile(ftpPath, new FileOutputStream(fileWrapper.getLocalFile()));
	}
	
	private void uploadFileToFtp(BancomextZipData zipData) {
		if (zipData != null) {
			val zippedCfdisPath = zipData.getCfdisZippedPath();
			if (zippedCfdisPath != null) {
				uploadFileToFtp(FTP_OUT_PATH, zippedCfdisPath);
			}
			
			val zippedErrorPath = zipData.getErrorZippedPath();
			if (zippedErrorPath != null) {
				uploadFileToFtp(FTP_ERROR_PATH, zippedErrorPath);
			}
		}
	}
	
	private void uploadFileToFtp(String sftpPath, String zippedFilePath) {
		try {
			val zippedFile = new File(zippedFilePath);
			val zippedFileName = zippedFile.getName();
			val path = String.format("%s/%s", sftpPath, zippedFileName);
			ftpClient.putFileToPath(zippedFile, path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}