package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.wrapper.ApiConcept;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextConceptReport {
	private String clave;
	private String claveProductoServicio;
	private String cantidad;
	private String unidadMedida;
	private String descripcion;
	private String precioUnitario;
	private String importeNeto;
	private String importe;
	private String objetoImpuesto;
	private String moneda;

	public static final BancomextConceptReport convert(ApiConcept item) {
		String uval = AssertUtils.hasValue(item.getUnit()) ? item.getUnit() : item.getMeasurementUnitCode();
		
		BancomextConceptReport conceptReport = new BancomextConceptReport();
		conceptReport.setClaveProductoServicio(item.getKeyProductServiceCode());
		conceptReport.setClave(item.getKeyProductServiceCode());
		conceptReport.setCantidad(item.getQuantityString());
		conceptReport.setUnidadMedida(uval);
		conceptReport.setDescripcion(item.getDescription());
		conceptReport.setPrecioUnitario(item.getUnitValueString());
		conceptReport.setImporteNeto(item.getAmountString());
		conceptReport.setImporte(item.getAmountString());
		conceptReport.setObjetoImpuesto(item.getObjImp());
		return conceptReport;
	}

	public String getClaveProductoServicio() {
		return claveProductoServicio;
	}

	public void setClaveProductoServicio(String claveProductoServicio) {
		this.claveProductoServicio = claveProductoServicio;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public String getImporteNeto() {
		return importeNeto;
	}

	public void setImporteNeto(String importeNeto) {
		this.importeNeto = importeNeto;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getObjetoImpuesto() {
		return objetoImpuesto;
	}

	public void setObjetoImpuesto(String objetoImpuesto) {
		this.objetoImpuesto = objetoImpuesto;
	}

	public String getMoneda() {
		if (moneda == null) {
			moneda = "XXX";
		}
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

}