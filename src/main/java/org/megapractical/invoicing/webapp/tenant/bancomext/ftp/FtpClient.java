package org.megapractical.invoicing.webapp.tenant.bancomext.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.val;

@Component
public class FtpClient {

	@Value("${ftp.server:173.214.162.58}")
	private String server;
	
	@Value("${ftp.port:21}")
	private int port;
	
	@Value("${ftp.user:st21081}")
	private String user;
	
	@Value("${ftp.pwd:@uJf6g$M}")
	private String password;
	
	private FTPClient ftp;

	public void open() throws IOException {
		System.out.println("=== FTP Client: Opening connection to FTP server");
		
		ftp = new FTPClient();
		ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

		ftp.connect(server, port);
		int reply = ftp.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftp.disconnect();
			throw new IOException("Exception in connecting to FTP Server");
		}

		ftp.login(user, password);
		System.out.println("=== FTP Client: Successfully connected to FTP server");
	}

	public void close() throws IOException {
		System.out.println("=== FTP Client: Closing connection to FTP server");
		ftp.disconnect();
	}

	public List<String> listFiles(String path) throws IOException {
		ftp.enterLocalPassiveMode();
		val files = ftp.listFiles(path);
		return Arrays.stream(files).map(FTPFile::getName).filter(i -> i.endsWith(".txt")).collect(Collectors.toList());
	}
	
	public void putFileToPath(File file, String path) throws IOException {
		ftp.storeFile(path, new FileInputStream(file));
	}

	public void downloadFile(String source, FileOutputStream out) throws IOException {
		ftp.retrieveFile(source, out);
		out.close();
	}
	
	public String[] retrieveFileContent(String path) {
	    try (val is = ftp.retrieveFileStream(path)) {
	        val lines = new ArrayList<String>();
	        try (Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name())) {
	            while (scanner.hasNextLine()) {
	                lines.add(scanner.nextLine());
	            }
	        }
	        return lines.toArray(new String[0]);
	    } catch (IOException e) {
			e.printStackTrace();
			return new String[] {};
		}
	}
	
}