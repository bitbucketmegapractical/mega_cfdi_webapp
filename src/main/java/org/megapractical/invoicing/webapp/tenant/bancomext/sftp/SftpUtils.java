package org.megapractical.invoicing.webapp.tenant.bancomext.sftp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exception.FileNotFoundException;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class SftpUtils {
	
	private final AppSettings appSettings;
	
	private static final String CHANNEL = "sftp";
	
	public boolean uploadFile(String sftp, String sftpPath, String localFilePath) {
		try {
			val file = new File(localFilePath);
			val is = UFile.fileToInputStream(file);
			val fileName = UFile.getFileName(file);
			return uploadFile(sftp, fileName, is, sftpPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean uploadFile(String sftp, String filename, InputStream fis, String sftpPath) {
		val session = loadSession(sftp);
		try {
			if (session != null) {
				if (!session.isConnected()) {
					session.connect();
				}

				UPrint.logWithLine(UProperties.env(),
						"[INFO] BANCOMEXT SFTP UTILS UPLOAD > FILE " + filename + " WILL BE UPLOADED TO " + sftpPath);
				val startTime = System.nanoTime();
				
				val channel = session.openChannel(CHANNEL);
				channel.connect();

				val channelSftp = (ChannelSftp) channel;
				channelSftp.cd(sftpPath);
				channelSftp.put(fis, filename);
				
				val endTime = System.nanoTime();
			    val duration = (endTime - startTime) / 1_000_000;
				UPrint.logWithLine(UProperties.env(),
						"[INFO] BANCOMEXT SFTP UTILS UPLOAD > UPLOAD COMPLETED IN " + duration + " MILLISECONDS");

				channelSftp.exit();
				channel.disconnect();
				session.disconnect();
				return true;
			}
		} catch (SftpException | JSchException e) {
			e.printStackTrace();
			if (session.isConnected()) {
				session.disconnect();
			}
		}
		return false;
	}
	
	public void downloadFile(String sftp, String remoteFilePath, String localFilePath) {
		val session = loadSession(sftp);
		try {
			if (session != null) {
				if (!session.isConnected()) {
					session.connect();
				}
				
				UPrint.logWithLine(UProperties.env(),
						"[INFO] BANCOMEXT SFTP UTILS DOWNLOAD > FILE " + remoteFilePath + " WILL BE DOWNLOADED TO " + localFilePath);
				val startTime = System.nanoTime();
				
				val channel = session.openChannel(CHANNEL);
				channel.connect();
				
				val channelSftp = (ChannelSftp) channel;
				channelSftp.get(remoteFilePath, localFilePath);
				
				val endTime = System.nanoTime();
			    val duration = (endTime - startTime) / 1_000_000;
				UPrint.logWithLine(UProperties.env(),
						"[INFO] BANCOMEXT SFTP UTILS DOWNLOAD > DOWNLOAD COMPLETED IN " + duration + " MILLISECONDS");
				
				channelSftp.exit();
				channel.disconnect();
				session.disconnect();
			}
		} catch (SftpException | JSchException e) {
			e.printStackTrace();
			if (session.isConnected()) {
				session.disconnect();
			}
		}
	}
	
	public byte[] downloadFile(String sftp, String remoteFilePath) {
	    val session = loadSession(sftp);
	    try {
	        if (session != null) {
	            if (!session.isConnected()) {
	                session.connect();
	            }

	            val channel = session.openChannel(CHANNEL);
	            channel.connect();

	            val channelSftp = (ChannelSftp) channel;
	            val inputStream = channelSftp.get(remoteFilePath);
	            
	            val outputStream = new ByteArrayOutputStream();
	            byte[] buffer = new byte[4096];
	            int bytesRead;
	            while ((bytesRead = inputStream.read(buffer)) != -1) {
	                outputStream.write(buffer, 0, bytesRead);
	            }

	            channelSftp.exit();
	            channel.disconnect();
	            session.disconnect();

	            return outputStream.toByteArray();
	        }
	    } catch (SftpException | JSchException | IOException e) {
	        e.printStackTrace();
	        if (session.isConnected()) {
	            session.disconnect();
	        }
	    }
	    return new byte[0];
	}
	
	public void deleteFile(String sftp, String fileName, String sftpPath) {
		val session = loadSession(sftp);
		try {
			if (session != null) {
				if (!session.isConnected()) {
					session.connect();
				}	
				
				UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP UTILS DELETE > FILE " + fileName + " WILL BE DELETED");
				val startTime = System.nanoTime();
				
				val channel = session.openChannel(CHANNEL);
				channel.connect();
				
				val channelSftp = (ChannelSftp) channel;
				channelSftp.cd(sftpPath);
				channelSftp.rm(fileName);
				
				val endTime = System.nanoTime();
				val duration = (endTime - startTime) / 1_000_000;
				UPrint.logWithLine(UProperties.env(),
						"[INFO] BANCOMEXT SFTP UTILS UPLOAD > DELETE COMPLETED IN " + duration + " MILLISECONDS");
				
				channelSftp.exit();
				channel.disconnect();
				session.disconnect();			
			}
		} catch (SftpException | JSchException e) {
			e.printStackTrace();
			if (session.isConnected()) {
				session.disconnect();
			}
		}
	}

	private Session loadSession(String sftp) {
		return SftpConstants.CFDI_SFTP.equals(sftp) ? cfdiSession() : payrollSession();
	}
	
	private Session cfdiSession() {
		try {
			val sftpHost = appSettings.getPropertyValue("sftp.host");
			val sftpPort = appSettings.getIntValue("sftp.port");
			val sftpUser = appSettings.getPropertyValue("sftp.user");
			val sftpPrivateKeyPath = appSettings.getPropertyValue("sftp.key.path");
			
			return initSession(sftpUser, sftpHost, sftpPort, sftpPrivateKeyPath);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Session payrollSession() {
		try {
			val sftpHost = appSettings.getPropertyValue("sftp.host");
			val sftpPort = appSettings.getIntValue("sftp.port");
			val sftpUser = appSettings.getPropertyValue("sftp.payroll.user");
			val sftpPrivateKeyPath = appSettings.getPropertyValue("sftp.payroll.key.path");
			
			return initSession(sftpUser, sftpHost, sftpPort, sftpPrivateKeyPath);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Session initSession(String user, String host, int port, String sftpPrivateKeyPath) {
		try {
			val pkBytes = privateKey(sftpPrivateKeyPath);
			
			val jsch = new JSch();
			jsch.addIdentity("key", pkBytes, null, null);
			val session = jsch.getSession(user, host, port);
			
			val config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			
			session.setConfig(config);
			session.setTimeout(30000);
			return session;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	private byte[] privateKey(String sftpPrivateKeyPath) {
		try (val is = Thread.currentThread().getContextClassLoader().getResourceAsStream(sftpPrivateKeyPath)) {
			return UFile.inputStreamToByte(is);
		} catch (IOException e) {
			throw new FileNotFoundException("SftpConfig error. Unable to load private key file");
		}
	}
	
}