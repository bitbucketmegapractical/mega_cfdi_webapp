package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextOtherPaymentReport {
	private String clave;
	private String tipoOtroPago;
	private String concepto;
	private String importe;

	public static final BancomextOtherPaymentReport convert(ApiPayrollOtherPayment item) {
		BancomextOtherPaymentReport otherPaymentReport = new BancomextOtherPaymentReport();
		otherPaymentReport.setClave(item.getKey());
		otherPaymentReport.setTipoOtroPago(item.getOtherPaymentTypePrint());
		otherPaymentReport.setConcepto(item.getConcept());
		otherPaymentReport.setImporte(item.getAmount());
		return otherPaymentReport;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getTipoOtroPago() {
		return tipoOtroPago;
	}

	public void setTipoOtroPago(String tipoOtroPago) {
		this.tipoOtroPago = tipoOtroPago;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

}
