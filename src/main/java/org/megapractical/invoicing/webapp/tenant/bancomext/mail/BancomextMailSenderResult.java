package org.megapractical.invoicing.webapp.tenant.bancomext.mail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextMailSenderResult {
	private boolean sent;
	private String reason;
}