package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextPaymentReport {
	private String fechaPago;
	private String formaPago;
	private String moneda;
	private String tipoCambio;
	private String importePago;
	private String noOperacion;

	public static final BancomextPaymentReport convert(Payment payment) {
		BancomextPaymentReport paymentReport = new BancomextPaymentReport();
		paymentReport.setFechaPago(payment.getPaymentDate());
		paymentReport.setFormaPago(payment.getPaymentWayPrint());
		paymentReport.setMoneda(payment.getCurrencyPrint());
		paymentReport.setTipoCambio(payment.getChangeTypeString());
		paymentReport.setImportePago(payment.getAmountString());
		paymentReport.setNoOperacion(null);
		return paymentReport;
	}
	
	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public String getImportePago() {
		return importePago;
	}

	public void setImportePago(String importePago) {
		this.importePago = importePago;
	}

	public String getNoOperacion() {
		return noOperacion;
	}

	public void setNoOperacion(String noOperacion) {
		this.noOperacion = noOperacion;
	}
	
}