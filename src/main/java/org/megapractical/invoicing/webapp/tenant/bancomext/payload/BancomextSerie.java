package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

public enum BancomextSerie {
	CRED_BK("BK"),
	CRED_COM("COM"),
	CRED_FAC("FAC"),
	CRED_IMX("IMX"),
	CRED_REPF("REPF"),
	CRED_EDC("EDC"),
	FM_NC("NC"),
	FM_PM("PM"),
	FM_RP("RP"),
	FI_DF("DF");
	
	private final String value;

	BancomextSerie(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}