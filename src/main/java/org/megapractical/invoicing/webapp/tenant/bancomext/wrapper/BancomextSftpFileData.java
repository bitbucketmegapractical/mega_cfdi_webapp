package org.megapractical.invoicing.webapp.tenant.bancomext.wrapper;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextSftpFileData {
	@Getter(AccessLevel.NONE)
	private List<String> validFiles;
	
	@Getter(AccessLevel.NONE)
	private List<String> duplicateFiles;

	public List<String> getValidFiles() {
		if (validFiles == null) {
			validFiles = new ArrayList<>();
		}
		return validFiles;
	}

	public List<String> getDuplicateFiles() {
		if (duplicateFiles == null) {
			duplicateFiles = new ArrayList<>();
		}
		return duplicateFiles;
	}
	
}