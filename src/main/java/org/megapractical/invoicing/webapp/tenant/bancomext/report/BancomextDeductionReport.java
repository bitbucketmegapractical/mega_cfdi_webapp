package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction.PayrollDeduction;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextDeductionReport {
	private String clave;
	private String tipoDeduccion;
	private String concepto;
	private String importe;

	public static final BancomextDeductionReport convert(PayrollDeduction item) {
		BancomextDeductionReport deductionReport = new BancomextDeductionReport();
		deductionReport.setClave(item.getKey());
		deductionReport.setTipoDeduccion(item.getDeductionTypePrint());
		deductionReport.setConcepto(item.getConcept());
		deductionReport.setImporte(item.getAmount());
		return deductionReport;
	}
	
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getTipoDeduccion() {
		return tipoDeduccion;
	}

	public void setTipoDeduccion(String tipoDeduccion) {
		this.tipoDeduccion = tipoDeduccion;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}
	
}