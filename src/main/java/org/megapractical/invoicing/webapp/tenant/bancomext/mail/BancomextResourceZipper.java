package org.megapractical.invoicing.webapp.tenant.bancomext.mail;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import lombok.val;

public final class BancomextResourceZipper {

	private BancomextResourceZipper() {
	}

	public static String zipXmlAndPdf(String xmlPath, String pdfPath) {
		try {
			if (xmlPath != null && pdfPath != null) {
				val xmlFilePath = Paths.get(xmlPath);
				val pdfFilePath = Paths.get(pdfPath);
				val directory = xmlFilePath.getParent();
				val xmlFileName = xmlFilePath.getFileName().toString();
				val pdfFileName = pdfFilePath.getFileName().toString();
				
				// Crear el archivo ZIP
				val zipFileName = xmlFileName.replace(".xml", ".zip");
				val zipFilePath = directory.resolve(zipFileName);
				try (val zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFilePath.toFile()))) {
					zipFile(xmlFilePath, xmlFileName, zipOutputStream);
					zipFile(pdfFilePath, pdfFileName, zipOutputStream);
					return zipFilePath.toString();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void zipFile(Path filePath, String fileName, ZipOutputStream zipOutputStream) throws IOException {
		if (Files.exists(filePath)) {
			try (val is = new FileInputStream(filePath.toFile())) {
				val zipEntry = new ZipEntry(fileName);
				zipOutputStream.putNextEntry(zipEntry);
				byte[] buffer = new byte[1024];
				int length;
				while ((length = is.read(buffer)) >= 0) {
					zipOutputStream.write(buffer, 0, length);
				}
			}
		}
	}

}