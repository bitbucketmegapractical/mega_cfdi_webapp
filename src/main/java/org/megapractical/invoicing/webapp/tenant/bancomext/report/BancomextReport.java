package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import java.io.InputStream;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.EdcType;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextSerie;
import org.megapractical.invoicing.webapp.util.UProperties;

import lombok.val;
import lombok.var;

public final class BancomextReport {
	
	private static final String BANCOMEX_LOGO_PATH = "/report/img/bancomext/";
	private static final String BANCOMEX_TEMPLATE_PATH = "/report/template/bancomext/";
	
	private BancomextReport() {		
	}
	
	public static InputStream getVerticalLogo() {
		val logoPath = BANCOMEX_LOGO_PATH + "bancomext-logo-vertical.png";
		return BancomextReport.class.getResourceAsStream(logoPath);
	}
	
	public static InputStream getHorizontalLogo() {
		val logoPath = BANCOMEX_LOGO_PATH + "bancomext-logo-horizontal.png";
		return BancomextReport.class.getResourceAsStream(logoPath);
	}
	
	public static InputStream getShcpLogo() {
		val logoPath = BANCOMEX_LOGO_PATH + "bancomext-logo-shcp.png";
		return BancomextReport.class.getResourceAsStream(logoPath);
	}
	
	public static BancomextReportData getReportDataBySerie(Area area, String serie) {
		if (AssertUtils.nonNull(area)) {
			String template = null;
			var pdfType = BancomextPdfType.CFDI;

			if (BancomextSerie.CRED_FAC.getValue().equalsIgnoreCase(serie)) {
				template = BANCOMEX_TEMPLATE_PATH + "bancomext_credito_serie_fac.jasper";
			} else if (BancomextSerie.CRED_REPF.getValue().equalsIgnoreCase(serie)
					|| BancomextSerie.FM_RP.getValue().equalsIgnoreCase(serie)) {
				template = BANCOMEX_TEMPLATE_PATH + "bancomext_payment.jasper";
				pdfType = BancomextPdfType.PAYMENT;
			} else {
				template = BANCOMEX_TEMPLATE_PATH + "bancomext_cfdi.jasper";
			}
			
			printTemplateInfo(template, pdfType);
			return new BancomextReportData(template, pdfType);
		}
		return null;
	}
	
	public static BancomextReportData getReportData(Comprobante voucher, ApiPayroll payroll) {
		if (AssertUtils.nonNull(voucher)) {
			String template = null;
			if (voucher.getSerie().endsWith("vdz")) {
				template = BANCOMEX_TEMPLATE_PATH + "bancomext_rh_serie_pensionados.jasper";
			} else {
				val employerRegistration = payroll.getPayrollEmitter().getEmployerRegistration();
				if (AssertUtils.hasValue(employerRegistration)) {
					template = BANCOMEX_TEMPLATE_PATH + "bancomext_rh_serie_activos.jasper";
				} else {
					template = BANCOMEX_TEMPLATE_PATH + "bancomext_rh_serie_jubilados.jasper";
				}
			}
			
			printTemplateInfo(template, BancomextPdfType.PAYROLL);
			return new BancomextReportData(template, BancomextPdfType.PAYROLL);
		}
		return null;
	}
	
	public static BancomextReportData getReportData(EdcType edcType) {
		if (AssertUtils.nonNull(edcType)) {
			String template = null;
			if (EdcType.TESORERIAINTERNACIONALTERCEROS.equals(edcType)) {
				template = BANCOMEX_TEMPLATE_PATH + "bancomext_tesoreria_serie_internacional.jasper";
			} else if (EdcType.TESORERIANACIONAL.equals(edcType)) {
				template = BANCOMEX_TEMPLATE_PATH + "bancomext_tesoreria_serie_nacional.jasper";
			} else if (EdcType.EDOCTADPMN.equals(edcType)) {
				template = BANCOMEX_TEMPLATE_PATH + "bancomext_tesoreria_serie_edoctadpmn.jasper";
			} else if (EdcType.EDODIVISAS.equals(edcType)) {
				template = BANCOMEX_TEMPLATE_PATH + "bancomext_mercados_serie_edc.jasper";
			}
			
			printTemplateInfo(template, BancomextPdfType.EDC);
			return new BancomextReportData(template, BancomextPdfType.EDC);
		}
		return null;
	}
	
	public static BancomextReportData getReportData(EdcType edcType, boolean allowStamp) {
		if (AssertUtils.nonNull(edcType)) {
			String template = null;
			if (EdcType.CREDITO.equals(edcType)) {
				if (allowStamp) {
					template = BANCOMEX_TEMPLATE_PATH + "bancomext_credito_serie_edc_fiscal.jasper";
				} else {
					template = BANCOMEX_TEMPLATE_PATH + "bancomext_credito_serie_edc_no_fiscal.jasper";
				}
			}
			
			printTemplateInfo(template, BancomextPdfType.EDC);
			return new BancomextReportData(template, BancomextPdfType.EDC);
		}
		return null;
	}

	private static void printTemplateInfo(String template, BancomextPdfType pdfType) {
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT REPORT > TEMPLATE: " + template);
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT REPORT > PDF TYPE: " + pdfType);
	}
	
}