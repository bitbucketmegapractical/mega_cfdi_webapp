package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.io.File;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.dal.bean.datatype.Origen;
import org.megapractical.invoicing.webapp.exposition.service.api.S3UploadService;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.CfdiSftpService;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.PayrollSftpService;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.SftpConstants;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileErrorWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextZipData;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class BancomextSftpServiceImpl implements BancomextSftpService {
	
	private final CfdiSftpService cfdiSftpService;
	private final PayrollSftpService payrollSftpService;
	private final BancomextService bancomextService;
	private final S3UploadService s3UploadService;
	private final BancomextStampSummaryService bancomextStampSummaryService;

	private static final Origen ORIGIN = Origen.SFTP;
	
	@Override
	@Scheduled(fixedRate = 600000)
	public void loadCfdiFiles() {
		val sftpFiles = cfdiSftpService.listFiles(SftpConstants.SFTP_IN_PATH);
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE > EDC");
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE > TOTAL LOADED FILES: " + sftpFiles.size());
		
		val filesData = bancomextService.sftpFileData(sftpFiles);
		filesData.getValidFiles().forEach(fileName -> processFile(fileName, SftpConstants.CFDI_SFTP));
		filesData.getDuplicateFiles().forEach(fileName -> processDuplicateFile(fileName, SftpConstants.CFDI_SFTP));
		
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE > EDC PROCESS COMPLETED");
	}
	
	@Override
	@Scheduled(fixedRate = 600000)
	public void loadPayrollFiles() {
		val sftpFiles = payrollSftpService.listFiles(SftpConstants.SFTP_IN_PATH);
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE > PAYROLL");
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE > TOTAL LOADED FILES: " + sftpFiles.size());
		
		val filesData = bancomextService.sftpFileData(sftpFiles);
		filesData.getValidFiles().forEach(fileName -> processFile(fileName, SftpConstants.PAYROLL_SFTP));
		filesData.getDuplicateFiles().forEach(fileName -> processDuplicateFile(fileName, SftpConstants.PAYROLL_SFTP));
		
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE > PAYROLL PROCESS COMPLETED");
	}
	
	private void processFile(String fileName, String sftp) {
		try {
			val fileWrapper = bancomextService.createFile(fileName, ORIGIN);
			if (fileWrapper != null) {
				downloadFileFromFtp(fileWrapper, sftp);
				val bancomextStampResponse = bancomextService.parseAndStamp(fileWrapper);
				if (bancomextStampResponse != null) {
					if (AssertUtils.nonNull(bancomextStampResponse.getZipData())) {
						val zipData = bancomextStampResponse.getZipData();
						// Upload to Sftp/OUT folder
						uploadFileToSftp(zipData, sftp);
						// Delete file from Sfpt/IN folder
						deleteFileFromSftp(fileWrapper, sftp);
						// Upload to S3
						val resourceDataSource = bancomextStampResponse.getResourceDataSource();
						val s3ErrorPath = s3UploadService.uploadToS3(zipData.getFileWrapper(), resourceDataSource);
						uploadS3ErrorFileToSftp(zipData.getFileWrapper(), s3ErrorPath, sftp);
						// Send Stamp Summary Mail
						bancomextStampSummaryService.sendStampSummaryMail(bancomextStampResponse.getStampSummary());
					}
					
					if (bancomextStampResponse.getFileErrorWrapper() != null) {
						uploadFileErrorToSftp(bancomextStampResponse.getFileErrorWrapper(), sftp);
						// Send Stamp Summary Mail
						bancomextStampSummaryService.sendMailForFileContentError(bancomextStampResponse.getFileErrorWrapper());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void processDuplicateFile(String fileName, String sftp) {
		try {
			UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE > DUPLICATE FILE " + fileName);
			val duplicateFileWrapper = bancomextService.logDuplicateFile(fileName);
			if (duplicateFileWrapper != null) {
				// Upload to Sftp/ERROR folder
				uploadFileErrorToSftp(duplicateFileWrapper, sftp);
				// Send Stamp Summary Mail
				val recipients = SftpConstants.CFDI_SFTP.equals(sftp) 
						? BancomextFileErrorWrapper.CFDI_ERROR_EMAIL
						: BancomextFileErrorWrapper.PAYROLL_ERROR_EMAIL;
				bancomextStampSummaryService.sendMailForDuplicateFileError(fileName, recipients);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void uploadFileErrorToSftp(BancomextFileErrorWrapper fileErrorWrapper, String sftp) {
		try {
			if (fileErrorWrapper != null) {
				// Upload to Sftp/ERROR folder
				val zippedFilePath = fileErrorWrapper.getZippedFilePath();
				uploadFileToSftp(SftpConstants.SFTP_ERROR_PATH, zippedFilePath, sftp);
				// Delete file from Sfpt/IN folder
				deleteFileFromSftp(fileErrorWrapper.getFileWrapper(), sftp);
				if (fileErrorWrapper.getFileWrapper() != null
						&& fileErrorWrapper.getFileWrapper().getBancomextFile() != null) {
					// Updating content error path in file
					val sftpErrPath = getSftpFilePath(zippedFilePath, SftpConstants.SFTP_ERROR_PATH);
					fileErrorWrapper.getFileWrapper().getBancomextFile().setRutaErrorContenido(sftpErrPath);
					bancomextService.updateBancomextFile(fileErrorWrapper.getFileWrapper().getBancomextFile());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void downloadFileFromFtp(BancomextFileWrapper fileWrapper, String sftp) {
		val sftpPath = getSftpFilePath(fileWrapper, SftpConstants.SFTP_IN_PATH);
		if (SftpConstants.CFDI_SFTP.equals(sftp)) {
			cfdiSftpService.downloadFile(sftpPath, fileWrapper.getLocalFile().getAbsolutePath());
		} else if (SftpConstants.PAYROLL_SFTP.equals(sftp)) {
			payrollSftpService.downloadFile(sftpPath, fileWrapper.getLocalFile().getAbsolutePath());
		}
	}
	
	private void uploadFileToSftp(BancomextZipData zipData, String sftp) {
		if (zipData != null) {
			val bancomextFile = zipData.getFileWrapper().getBancomextFile();
			
			val zippedCfdisPath = zipData.getCfdisZippedPath();
			if (zippedCfdisPath != null) {
				uploadFileToSftp(SftpConstants.SFTP_OUT_PATH, zippedCfdisPath, sftp);
				// Updating sftp path in file
				val sfptPath = getSftpFilePath(zippedCfdisPath, SftpConstants.SFTP_OUT_PATH);
				bancomextFile.setRutaDescarga(sfptPath);
				bancomextService.updateBancomextFile(bancomextFile);
			}
			
			val zippedErrorPath = zipData.getErrorZippedPath();
			if (zippedErrorPath != null) {
				uploadFileToSftp(SftpConstants.SFTP_ERROR_PATH, zippedErrorPath, sftp);
				// Updating validation error path in file
				val sftpErrPath = getSftpFilePath(zippedErrorPath, SftpConstants.SFTP_ERROR_PATH);
				bancomextFile.setRutaErrorValidacion(sftpErrPath);
				bancomextService.updateBancomextFile(bancomextFile);
			}
		}
	}
	
	private void uploadFileToSftp(String sftpPath, String zippedFilePath, String sftp) {
		val zippedFile = new File(zippedFilePath);
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE UPLOAD > FILE " + zippedFile + " WILL BE UPLOAD TO " + sftpPath);
		val zippedFileName = zippedFile.getName();
		val path = String.format(SftpConstants.SFTP_PATH_FORMAT, sftpPath, zippedFileName);
		if (SftpConstants.CFDI_SFTP.equals(sftp)) {
			cfdiSftpService.uploadFile(zippedFilePath, path);
		} else if (SftpConstants.PAYROLL_SFTP.equals(sftp)) {
			payrollSftpService.uploadFile(zippedFilePath, path);
		}
		// Delete file from server
		UFile.deleteFile(zippedFilePath);
	}

	private void deleteFileFromSftp(BancomextFileWrapper fileWrapper, String sftp) {
		val sftpPath = getSftpFilePath(fileWrapper, SftpConstants.SFTP_IN_PATH);
		UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT SFTP SERVICE DELETE > FILE " + sftpPath + " WILL BE DELETED");
		if (SftpConstants.CFDI_SFTP.equals(sftp)) {
			cfdiSftpService.deleteFile(sftpPath);
		} else if (SftpConstants.PAYROLL_SFTP.equals(sftp)) {
			payrollSftpService.deleteFile(sftpPath);
		}
	}
	
	private void uploadS3ErrorFileToSftp(BancomextFileWrapper fileWrapper, String s3ErrorPath, String sftp) {
		if (AssertUtils.hasValue(s3ErrorPath)) {
			uploadFileToSftp(SftpConstants.SFTP_ERROR_PATH, s3ErrorPath, sftp);
			// Updating s3 error path in file
			val sftpErrPath = getSftpFilePath(s3ErrorPath, SftpConstants.SFTP_ERROR_PATH);
			fileWrapper.getBancomextFile().setRutaErrorS3(sftpErrPath);
			bancomextService.updateBancomextFile(fileWrapper.getBancomextFile());
		}
	}
	
	private String getSftpFilePath(BancomextFileWrapper fileWrapper, String sftpPath) {
		return String.format(SftpConstants.SFTP_PATH_FORMAT, sftpPath, fileWrapper.getFileDetail().getOriginalFilename());
	}
	
	private String getSftpFilePath(String filePath, String sftpPath) {
		val fileName = UFile.getFileFullName(filePath);
		return String.format(SftpConstants.SFTP_PATH_FORMAT, sftpPath, fileName);
	}

}