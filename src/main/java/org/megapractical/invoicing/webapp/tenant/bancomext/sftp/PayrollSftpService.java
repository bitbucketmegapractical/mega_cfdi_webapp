package org.megapractical.invoicing.webapp.tenant.bancomext.sftp;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.webapp.tenant.bancomext.exception.BancomextSftpException;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
public class PayrollSftpService {

	private final SftpRemoteFileTemplate sftpTemplate;
	private final SftpUtils sftpUtils;

	public PayrollSftpService(@Qualifier("payrollSftpRemoteFileTemplate") SftpRemoteFileTemplate sftpTemplate,
							  SftpUtils sftpUtils) {
		this.sftpTemplate = sftpTemplate;
		this.sftpUtils = sftpUtils;
	}

	public List<String> listFiles(String remoteDirectory) {
		val fileList = new ArrayList<String>();
		try {
			val list = sftpTemplate.execute(session -> session.list(remoteDirectory));
			for (val entry : list) {
				if (!entry.getAttrs().isDir()) {
					fileList.add(entry.getFilename());
				}
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileList;
	}

	public void downloadFile(String remoteFilePath, String localFilePath) {
		sftpTemplate.get(remoteFilePath, inputStream -> {
			try (val outputStream = new FileOutputStream(localFilePath)) {
				IOUtils.copy(inputStream, outputStream);
			} catch (IOException e) {
				e.printStackTrace();
				throw new BancomextSftpException(e.getMessage());
			}
		});
	}

	public void uploadFile(String localFilePath, String remoteFilePath) {
		try {
			sftpTemplate.execute(session -> {
				try (val inputStream = new FileInputStream(localFilePath)) {
					session.write(inputStream, remoteFilePath);
				} catch (Exception e) {
					throw new BancomextSftpException(e.getMessage());
				}
				return true;
			});
		} catch (Exception e) {
			UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT PAYROLL SFTP SERVICE UPLOAD > " + localFilePath + " FILE COULD NOT BE UPLOADED");
			sftpUtils.uploadFile(SftpConstants.PAYROLL_SFTP, remoteFilePath, localFilePath);
		}
	}
	
	public void deleteFile(String remoteFilePath) {
		try {
			sftpTemplate.execute(session -> {
				try {
					session.remove(remoteFilePath);
				} catch (IOException e) {
					e.printStackTrace();
					throw new BancomextSftpException(e.getMessage());
				}
				return true;
			});
		} catch (MessagingException e) {
			e.printStackTrace();
			UPrint.logWithLine(UProperties.env(), "[INFO] BANCOMEXT PAYROLL SFTP SERVICE DELETE > FILE " + remoteFilePath + " DOESN'T EXIST IN SFTP");
		}
	}
}