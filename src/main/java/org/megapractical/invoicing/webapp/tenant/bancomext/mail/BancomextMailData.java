package org.megapractical.invoicing.webapp.tenant.bancomext.mail;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.webapp.exposition.cfdi.payload.ResourceData;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextMailData {
	private ResourceData resourceData;
	@Getter(value = AccessLevel.NONE)
	private List<String> recipients;
	private String recipient;
	private String emitterRfc;
	private String emitterName;
	private String receiverRfc;
	private String serie;
	private String folio;
	private String uuid;
	private String xmlFileName;
	private String xmlPath;
	private String pdfFileName;
	private String pdfPath;
	private String zipPath;
	private String subject;
	@Default
	private boolean sendAttachment = true;
	
	public List<String> getRecipients() {
		if (recipients == null) {
			recipients = new ArrayList<>();
		}
		return recipients;
	}
	
}