package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.webapp.tenant.bancomext.parser.EdcType;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextEdc {
	private ApiCfdi cfdi;
	private EdcType edcType;
	@Getter(AccessLevel.NONE)
	private BancomextTreasury treasury;
	@Getter(AccessLevel.NONE)
	private BancomextForeignExchange foreignExchange;
	@Getter(AccessLevel.NONE)
	private BancomextCredit credit;
	private BancomextAddress address;
	private String contract;
	private String code;
	@Default
	private boolean allowStamp = true;
	
	public BancomextEdc(ApiCfdi cfdi, EdcType edcType, boolean allowStamp) {
		this.cfdi = cfdi;
		this.edcType = edcType;
		this.allowStamp = allowStamp;
	}
	
	public static boolean allowStampEdc(String value) {
		return !value.equals("NO_FISCAL");
	}
	
	public static boolean isTreasury(EdcType edcType) {
		return EdcType.TESORERIAINTERNACIONALTERCEROS.equals(edcType) || EdcType.TESORERIANACIONAL.equals(edcType)
				|| EdcType.EDOCTADPMN.equals(edcType);
	}
	
	public static boolean isForeignExchange(EdcType edcType) {
		return EdcType.EDODIVISAS.equals(edcType);
	}
	
	public static boolean isCredit(EdcType edcType) {
		return EdcType.CREDITO.equals(edcType);
	}
	
	public BancomextTreasury getTreasury() {
		if (treasury == null) {
			treasury = new BancomextTreasury();
		}
		return treasury;
	}

	public BancomextForeignExchange getForeignExchange() {
		if (foreignExchange == null) {
			foreignExchange = new BancomextForeignExchange();
		}
		return foreignExchange;
	}

	public BancomextCredit getCredit() {
		if (credit == null) {
			credit = new BancomextCredit();
		}
		return credit;
	}
}