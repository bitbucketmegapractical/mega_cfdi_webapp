package org.megapractical.invoicing.webapp.tenant.bancomext.util;

import java.io.File;
import java.time.LocalDate;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;

import lombok.val;

public final class BancomextUtils {
	
	public static final String SEPARATOR = File.separator;
	private static final String PATH_FORMAT = "%s%s%s";
	
	private BancomextUtils() {		
	}
	
	public static String getBasePath(String rfc, String fileName) {
		val today = LocalDate.now().toString().replace("-", "");
		val folderName = UFile.getName(fileName);
		return String.format("%s%s%s%s%s%s%s", rfc, SEPARATOR, today, SEPARATOR, folderName, SEPARATOR, folderName);
	}
	
	public static String getAbsolutePath(String rfc, String storePath, String fileName) {
		val basePath = getBasePath(rfc, fileName);
		return String.format(PATH_FORMAT, storePath, SEPARATOR, basePath);
	}
	
	public static String determineDbPath(String rfc, String fileName) {
		val basePath = getBasePath(rfc, fileName);
		return String.format(PATH_FORMAT, basePath, SEPARATOR, fileName);
	}
	
	public static String determineAbsolutePath(String rfc, String storePath, String fileName) {
		val absolutePath = getAbsolutePath(rfc, storePath, fileName);
		return absolutePath + BancomextUtils.SEPARATOR + fileName;
	}
	
	public static String errorPath(BancomextFileWrapper fileWrapper) {
		return fileWrapper.getUploadFilePath().getAbsolutePath() + "_ERROR";
	}
	
	public static String s3ErrorPath(BancomextFileWrapper fileWrapper) {
		return fileWrapper.getUploadFilePath().getAbsolutePath() + "_S3_ERROR";
	}
}