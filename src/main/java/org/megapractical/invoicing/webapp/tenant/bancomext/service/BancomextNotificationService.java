package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import org.megapractical.invoicing.dal.bean.jpa.BancomextNotification;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextMailData;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextStampSummary;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface BancomextNotificationService {
	
	BancomextNotification save(BancomextMailData mailData, String reason);
	
	BancomextNotification save(BancomextStampSummary stampSummary, String reason);
	
}