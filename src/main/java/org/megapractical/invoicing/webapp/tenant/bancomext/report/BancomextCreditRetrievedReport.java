package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextCreditRetrievedReport {
	private String noPrestamo;
	private String saldoInicial;
	private String importeCalculoInteresOrd;
	private String importeCalculoInteresMoratorio;
	private String tasaInteresOrd;
	private String esquemaTasaOrdinaria;
	private String tasaInteresMoratoria;
	private String esquemaTasaMoratoria;
	private String vencidas;
	private String pagadas;
	private String porPagar;
	private List<BancomextCreditFlow> movimientosList;
	
	public static class BancomextCreditFlow {
		private String fechaPago;
		private String capital;
		private String prepagosCapital;
		private String ivaCapital;
		private String interesOrdinario;
		private String ivaInteresOrdinario;
		private String interesesMoratorios;
		private String ivaInteresMoratorios;
		private String subsidio;
		private String totalPagado;
		
		public String getFechaPago() {
			return fechaPago;
		}

		public void setFechaPago(String fechaPago) {
			this.fechaPago = fechaPago;
		}

		public String getCapital() {
			return capital;
		}

		public void setCapital(String capital) {
			this.capital = capital;
		}

		public String getPrepagosCapital() {
			return prepagosCapital;
		}

		public void setPrepagosCapital(String prepagosCapital) {
			this.prepagosCapital = prepagosCapital;
		}

		public String getIvaCapital() {
			return ivaCapital;
		}

		public void setIvaCapital(String ivaCapital) {
			this.ivaCapital = ivaCapital;
		}

		public String getInteresOrdinario() {
			return interesOrdinario;
		}

		public void setInteresOrdinario(String interesOrdinario) {
			this.interesOrdinario = interesOrdinario;
		}

		public String getIvaInteresOrdinario() {
			return ivaInteresOrdinario;
		}

		public void setIvaInteresOrdinario(String ivaInteresOrdinario) {
			this.ivaInteresOrdinario = ivaInteresOrdinario;
		}

		public String getInteresesMoratorios() {
			return interesesMoratorios;
		}

		public void setInteresesMoratorios(String interesesMoratorios) {
			this.interesesMoratorios = interesesMoratorios;
		}

		public String getIvaInteresMoratorios() {
			return ivaInteresMoratorios;
		}

		public void setIvaInteresMoratorios(String ivaInteresMoratorios) {
			this.ivaInteresMoratorios = ivaInteresMoratorios;
		}

		public String getSubsidio() {
			return subsidio;
		}

		public void setSubsidio(String subsidio) {
			this.subsidio = subsidio;
		}

		public String getTotalPagado() {
			return totalPagado;
		}

		public void setTotalPagado(String totalPagado) {
			this.totalPagado = totalPagado;
		}
	}

	/* Getters and Setters */
	public String getNoPrestamo() {
		return noPrestamo;
	}

	public void setNoPrestamo(String noPrestamo) {
		this.noPrestamo = noPrestamo;
	}

	public String getSaldoInicial() {
		return saldoInicial;
	}

	public void setSaldoInicial(String saldoInicial) {
		this.saldoInicial = saldoInicial;
	}

	public String getImporteCalculoInteresOrd() {
		return importeCalculoInteresOrd;
	}

	public void setImporteCalculoInteresOrd(String importeCalculoInteresOrd) {
		this.importeCalculoInteresOrd = importeCalculoInteresOrd;
	}

	public String getImporteCalculoInteresMoratorio() {
		return importeCalculoInteresMoratorio;
	}

	public void setImporteCalculoInteresMoratorio(String importeCalculoInteresMoratorio) {
		this.importeCalculoInteresMoratorio = importeCalculoInteresMoratorio;
	}

	public String getTasaInteresOrd() {
		return tasaInteresOrd;
	}

	public void setTasaInteresOrd(String tasaInteresOrd) {
		this.tasaInteresOrd = tasaInteresOrd;
	}

	public String getEsquemaTasaOrdinaria() {
		return esquemaTasaOrdinaria;
	}

	public void setEsquemaTasaOrdinaria(String esquemaTasaOrdinaria) {
		this.esquemaTasaOrdinaria = esquemaTasaOrdinaria;
	}

	public String getTasaInteresMoratoria() {
		return tasaInteresMoratoria;
	}

	public void setTasaInteresMoratoria(String tasaInteresMoratoria) {
		this.tasaInteresMoratoria = tasaInteresMoratoria;
	}

	public String getEsquemaTasaMoratoria() {
		return esquemaTasaMoratoria;
	}

	public void setEsquemaTasaMoratoria(String esquemaTasaMoratoria) {
		this.esquemaTasaMoratoria = esquemaTasaMoratoria;
	}

	public String getVencidas() {
		return vencidas;
	}

	public void setVencidas(String vencidas) {
		this.vencidas = vencidas;
	}

	public String getPagadas() {
		return pagadas;
	}

	public void setPagadas(String pagadas) {
		this.pagadas = pagadas;
	}

	public String getPorPagar() {
		return porPagar;
	}

	public void setPorPagar(String porPagar) {
		this.porPagar = porPagar;
	}

	public List<BancomextCreditFlow> getMovimientosList() {
		if (movimientosList == null) {
			movimientosList = new ArrayList<>();
		}
		return movimientosList;
	}

	public void setMovimientosList(List<BancomextCreditFlow> movimientosList) {
		this.movimientosList = movimientosList;
	}

}