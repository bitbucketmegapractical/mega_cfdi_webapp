package org.megapractical.invoicing.webapp.tenant.bancomext.report;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextCreditDispositionReport {
	private String numPrestamo;
	private String fechaDisposicion;
	private String montoDispuesto;

	/* Getters and Setters */
	public String getNumPrestamo() {
		return numPrestamo;
	}

	public void setNumPrestamo(String numPrestamo) {
		this.numPrestamo = numPrestamo;
	}

	public String getFechaDisposicion() {
		return fechaDisposicion;
	}

	public void setFechaDisposicion(String fechaDisposicion) {
		this.fechaDisposicion = fechaDisposicion;
	}

	public String getMontoDispuesto() {
		return montoDispuesto;
	}

	public void setMontoDispuesto(String montoDispuesto) {
		this.montoDispuesto = montoDispuesto;
	}

}