package org.megapractical.invoicing.webapp.tenant.bancomext.repository;

import org.megapractical.invoicing.dal.bean.jpa.BancomextNotification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BancomextNotificationRepository extends JpaRepository<BancomextNotification, Long> {
	
}