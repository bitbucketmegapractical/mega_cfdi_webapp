package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import static java.util.stream.Collectors.toList;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.report.ReportManagerTools;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment.EmploymentSubsidy;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.report.payload.ReportParam;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class BancomextPayrollReportService {
	
	private final ReportManager reportManager;
	private final UserTools userTools;
	
	public boolean generatePdf(BancomextStampData stampData) {
		try {
			val reportData = BancomextReport.getReportData(stampData.getVoucher(), stampData.getPayroll());
			if (reportData != null) {
				val template = BancomextPayrollReportService.class.getResourceAsStream(reportData.getTemplatePath());
				
				val parameters = new HashMap<String, Object>();
				parameters.put("template", template);
				populatePdfData(stampData, parameters);
				
				val cfdi = stampData.getPayroll().getApiCfdi();
				val exported = reportManager.exportReportToPdf(template, parameters, cfdi.getPdfPath());
				if (exported) {
					userTools.log("[INFO] BANCOMEXT PAYROLL REPORT - PDF HAS BEEN GENERATED SUCCESSFULLY");
				}
				return exported;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private ReportParam populatePdfData(BancomextStampData stampData, final Map<String, Object> parameters) {
		val payroll = stampData.getPayroll();
		val cfdi = payroll.getApiCfdi();
		
		setHeaderData(cfdi, parameters);
		setReceiverData(cfdi, parameters);
		setBusinessInformation(cfdi, parameters);
		setConceptData(cfdi, parameters);
		setPayrollData(payroll, parameters);
		setPayrollEmitterData(payroll, parameters);
		setPayrollReceiverData(payroll, parameters);
		setPayrollPerceptions(payroll, parameters);
		setPayrollDeductions(payroll, parameters);
		setOtherPayments(payroll, parameters);
		setStampData(cfdi, parameters);
		
		return new ReportParam(parameters);
	}
	
	private final void setHeaderData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val emitter = cfdi.getEmitter();
		val stampResponse = cfdi.getStampResponse();
		val voucher = stampResponse.getVoucher();
		
		parameters.put("logo", BancomextReport.getVerticalLogo());
		parameters.put("regimenFiscalEmisor", emitter.getRegimenFiscal().getCodigo() + " " + emitter.getRegimenFiscal().getValor());
		parameters.put("noCertSat", stampResponse.getCertificateSatNumber());
		parameters.put("noCertEmisor", stampResponse.getCertificateEmitterNumber());
		parameters.put("folioFiscal", stampResponse.getUuid().toUpperCase());
		parameters.put("lugarExpedicion", voucher.getLugarExpedicion());
		parameters.put("fechaCertificacion", stampResponse.getDateStamp());
		parameters.put("fechaEmision", stampResponse.getDateExpedition());
	}
	
	private void setPayrollEmitterData(ApiPayroll payroll, Map<String, Object> parameters) {
		val emitter = payroll.getPayrollEmitter();
		parameters.put("registroPatronal", emitter.getEmployerRegistration());
		parameters.put("rfcPatron", emitter.getRfcPatronOrigin());
	}
	
	private void setReceiverData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val receiver = cfdi.getReceiver();
		val additionals = cfdi.getAdditionals();
		
		parameters.put("rfcRazonSocialReceptor", receiver.getRfc() + " - " + receiver.getNombreRazonSocial());
		parameters.put("usoCfdi", additionals.get(BancomextAdditional.RECEIVER_CFDI_USE));
		parameters.put("domicilioFiscalReceptor", receiver.getDomicilioFiscal());
		parameters.put("regimenFiscalReceptor", additionals.get(BancomextAdditional.RECEIVER_TAX_REGIME));
	}
	
	private void setBusinessInformation(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val voucher = cfdi.getVoucher();
		
		parameters.put("serieFolio", voucher.getSerie() + " " + voucher.getFolio());
		parameters.put("tipoComprobante", voucher.getVoucherTypeString());
		parameters.put("moneda", voucher.getCurrencyString());
		parameters.put("exportacion", voucher.getExportationString());
		parameters.put("formaPago", voucher.getPaymentWayString());
		parameters.put("metodoPago", voucher.getPaymentMethodString());
		parameters.put("subtotal", voucher.getSubTotalString());
		parameters.put("descuento", voucher.getDiscountString());
		parameters.put("total", voucher.getTotalString());
		parameters.put("totalLetras", ReportManagerTools.totalInLetter(voucher.getTotal(), voucher.getCurrency().value()));
	}
	
	private void setConceptData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val conceptReportList = cfdi.getConcepts().stream()
												  .map(BancomextConceptReport::convert)
												  .collect(toList());
		parameters.put("conceptoList", conceptReportList);
	}
	
	private void setPayrollData(ApiPayroll payroll, final Map<String, Object> parameters) {
		parameters.put("tipoNomina", payroll.getPayrollTypePrint());
		parameters.put("fechaInicialPago", payroll.getPaymentIntialDateString());
		parameters.put("numDiasPagados", payroll.getNumberDaysPaid());
		parameters.put("totalPercepciones", payroll.getTotalPerceptions());
		parameters.put("fechaPago", payroll.getPaymentDateString());
		parameters.put("fechaFinalPago", payroll.getPaymentEndDateString());
		parameters.put("totalDeducciones", payroll.getTotalDeductions());
	}
	
	private void setPayrollReceiverData(ApiPayroll payroll, final Map<String, Object> parameters) {
		val receiver = payroll.getPayrollReceiver();
		
		parameters.put("curp", receiver.getCurp());
		parameters.put("inicioRelacionLaboral", receiver.getStartDateEmploymentRelationshipString());
		parameters.put("tipoContrato", receiver.getContractTypePrint());
		parameters.put("tipoRegimen", receiver.getRegimeTypePrint());
		parameters.put("noEmpleado", receiver.getEmployeeNumber());
		parameters.put("puesto", receiver.getJobTitle());
		parameters.put("riesgoPuesto", receiver.getJobRiskPrint());
		parameters.put("formaEstablecePago", receiver.getPaymentFrequencyPrint());
		parameters.put("salarioBaseCotizacion", receiver.getShareContributionBaseSalary());
		parameters.put("entidadFederativa", receiver.getFederalEntityPrint());
		parameters.put("noSeguroSocial", receiver.getSocialSecurityNumber());
		parameters.put("antiguedad", receiver.getAge());
		parameters.put("esSindicalizado", receiver.getUnionized());
		parameters.put("tipoJornada", receiver.getDayTypePrint());
		parameters.put("departamento", receiver.getDepartment());
		parameters.put("banco", receiver.getBankPrint());
		parameters.put("salarioDiarioIntegrado", receiver.getIntegratedDailySalary());
	}
	
	private void setPayrollPerceptions(ApiPayroll payroll, final Map<String, Object> parameters) {
		val perception = payroll.getPayrollPerception();
		if (AssertUtils.nonNull(perception)) {
			parameters.put("totalPercepciones", payroll.getTotalPerceptions());
			parameters.put("totalJubilacionesPenRet", perception.getTotalPensionRetirement());
			parameters.put("totalGravado", perception.getTotalTaxed());
			parameters.put("totalExento", perception.getTotalExempt());
			
			val perceptionReportList = payroll.getPayrollPerception().getPayrollPerceptions()
																	 .stream()
																	 .map(BancomextPerceptionReport::convert)
																	 .collect(toList());
			parameters.put("percepcionList", perceptionReportList);
			
			if (AssertUtils.nonNull(perception.getPayrollPensionRetirement())) {
				val pensionRetirement = perception.getPayrollPensionRetirement();
				parameters.put("montoDiario", pensionRetirement.getDailyAmount());
				parameters.put("totalParcialidad", pensionRetirement.getTotalPartiality());
				parameters.put("ingresoAcumulable", pensionRetirement.getIncomeAccumulate());
				parameters.put("ingresoNoAcumulable", pensionRetirement.getIncomeNoAccumulate());
			}
		}
	}
	
	private void setPayrollDeductions(ApiPayroll payroll, final Map<String, Object> parameters) {
		val deduction = payroll.getPayrollDeduction();
		if (AssertUtils.nonNull(deduction)) {
			parameters.put("totalDeducciones", deduction.getTotalOtherDeductions());
			parameters.put("totalImpuestosFedRetenidos", deduction.getTotalTaxesWithheld());
			
			val deductionReportList = payroll.getPayrollDeduction().getPayrollDeductions()
																   .stream()
																   .map(BancomextDeductionReport::convert)
																   .collect(toList());
			parameters.put("deducionList", deductionReportList);
		}
	}
	
	private void setOtherPayments(ApiPayroll payroll, final Map<String, Object> parameters) {
		val otherPayments = payroll.getPayrollOtherPayments();
		if (AssertUtils.nonNull(otherPayments)) {
			val otherPaymentReportList = otherPayments.stream()
													  .map(BancomextOtherPaymentReport::convert)
													  .collect(toList());
			
			val employmentSubsidy = otherPayments.stream()
												 .map(ApiPayrollOtherPayment::getEmploymentSubsidy)
												 .filter(Objects::nonNull)
												 .map(EmploymentSubsidy::getSubsidyCaused)
												 .findFirst().orElse("0.00");
			
			parameters.put("otroPagoList", otherPaymentReportList);
			parameters.put("subsidioEmpleo", employmentSubsidy);
		}
	}
	
	private void setStampData(ApiCfdi cfdi, final Map<String, Object> parameters) {
		val stampResponse = cfdi.getStampResponse();
		parameters.put("qrImagen", ReportManagerTools.getQR(stampResponse));
		parameters.put("rfcProveedorCertifica", stampResponse.getTfd().getRfcProvCertif());
		parameters.put("cadenaOriginal", stampResponse.getOriginalString());
		parameters.put("selloSat", stampResponse.getSatSeal());
		parameters.put("selloDigitalEmisor", stampResponse.getEmitterSeal());
	}
	
}