package org.megapractical.invoicing.webapp.tenant.bancomext.parser;

public enum EdcType {
	CREDITO, EDODIVISAS, TESORERIAINTERNACIONALTERCEROS, TESORERIANACIONAL, EDOCTADPMN
}