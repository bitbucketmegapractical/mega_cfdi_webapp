package org.megapractical.invoicing.webapp.tenant.bancomext.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextTreasurySummary {
	private String saldoInicial;
	private String promedioSaldoDiario;
	private String retiros;
	private String depositos;
	private String interesesGenerados;
	private String impuestosRetenidosUsd;
	private String impuestosRetenidosMn;
	private String tipoCambioPromedio;
	private String tasaOCuota;
	private String impuesto;
	private String base;
	private String saldoFinal;
	private String saldoMesAnterior;
	private String vencimientos;
	private String inversiones;
	private String premio;
	private String impuestoRetenido;
	private String saldoActual;
	private String ivnValVig;
	private String totalDepositos;
	private String totalRetiros;
	private String saldoMesAnteriorGarantia;
	private String totalTitulosGarantiaInicio;
	private String totalTitulosGarantiaFin;
	private String saldoActualGarantia;
}