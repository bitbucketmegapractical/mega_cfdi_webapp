package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.BaseTax;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.RelatedDocument;

/**
 * <p>
 * <b>NOTA IMPORTANTE!</b>
 * </p>
 * <p>
 * Las clases usadas en los reportes, no deben utilizar Lombok
 * </p>
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public class BancomextRelatedDocReport {
	private String folioFiscal;
	private String serieFolio;
	private String moneda;
	private String equivalencia;	
	private String objetoImpuesto;
	private String noParcialidad;
	private String saldoAnterior;
	private String importePagado;
	private String saldoInsoluto;
	private String base;
	private String impuesto;
	private String tipoFactor;
	private String tasa;
	private String importe;

	public BancomextRelatedDocReport() {		
	}
	
	public BancomextRelatedDocReport(RelatedCfdiUuid relatedCfdi) {
		this.folioFiscal = relatedCfdi.getUuid();
	}
	
	public static final BancomextRelatedDocReport convert(RelatedDocument relDoc) {
		BancomextRelatedDocReport relDocReport = new BancomextRelatedDocReport();
		relDocReport.setFolioFiscal(relDoc.getDocumentId());
		relDocReport.setSerieFolio(relDoc.getSerie() + " " + relDoc.getSheet());
		relDocReport.setMoneda(relDoc.getCurrencyPrint());
		relDocReport.setEquivalencia(relDoc.getChangeTypePrint());
		relDocReport.setObjetoImpuesto(relDoc.getTaxObjectCode());
		relDocReport.setNoParcialidad(String.valueOf(relDoc.getPartiality()));
		relDocReport.setSaldoAnterior(relDoc.getAmountPartialityBeforePrint());
		relDocReport.setImportePagado(relDoc.getAmountPaidPrint());
		relDocReport.setSaldoInsoluto(relDoc.getDifferencePrint());
		
		if (relDoc.getTransferreds() != null && !relDoc.getTransferreds().isEmpty()) {
			BaseTax transferred = relDoc.getTransferreds().get(0);
			relDocReport.setBase(transferred.getBase());
			relDocReport.setImpuesto(transferred.getTax());
			relDocReport.setTipoFactor(transferred.getFactor());
			relDocReport.setTasa(transferred.getRateOrFee());
			relDocReport.setImporte(transferred.getAmount());
		}
		return relDocReport;
	}
	
	public String getFolioFiscal() {
		return folioFiscal;
	}

	public void setFolioFiscal(String folioFiscal) {
		this.folioFiscal = folioFiscal;
	}

	public String getSerieFolio() {
		return serieFolio;
	}

	public void setSerieFolio(String serieFolio) {
		this.serieFolio = serieFolio;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getEquivalencia() {
		return equivalencia;
	}

	public void setEquivalencia(String equivalencia) {
		this.equivalencia = equivalencia;
	}

	public String getObjetoImpuesto() {
		return objetoImpuesto;
	}

	public void setObjetoImpuesto(String objetoImpuesto) {
		this.objetoImpuesto = objetoImpuesto;
	}

	public String getNoParcialidad() {
		return noParcialidad;
	}

	public void setNoParcialidad(String noParcialidad) {
		this.noParcialidad = noParcialidad;
	}

	public String getSaldoAnterior() {
		return saldoAnterior;
	}

	public void setSaldoAnterior(String saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public String getImportePagado() {
		return importePagado;
	}

	public void setImportePagado(String importePagado) {
		this.importePagado = importePagado;
	}

	public String getSaldoInsoluto() {
		return saldoInsoluto;
	}

	public void setSaldoInsoluto(String saldoInsoluto) {
		this.saldoInsoluto = saldoInsoluto;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}

	public String getTipoFactor() {
		return tipoFactor;
	}

	public void setTipoFactor(String tipoFactor) {
		this.tipoFactor = tipoFactor;
	}

	public String getTasa() {
		return tasa;
	}

	public void setTasa(String tasa) {
		this.tasa = tasa;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}
	
}