package org.megapractical.invoicing.webapp.tenant.bancomext.mail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData.BancomextVoucherDetail;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextStampSummary {
	@Getter(value = AccessLevel.NONE)
	private List<String> recipients;
	private String recipient;
	private List<StampSummary> summary;
	private String fileName;
	private String subject;
	
	public BancomextStampSummary(String summaryEmailInfo, List<StampSummary> summary, String fileName) {
		populateRecipients(summaryEmailInfo);
		this.summary = summary;
		this.fileName = fileName;
	}
	
	private void populateRecipients(String summaryEmailInfo) {
		if (summaryEmailInfo != null) {
			val source = summaryEmailInfo.split(";");
			this.recipients = Arrays.asList(source);
		} else {
			this.recipients = new ArrayList<>();
		}
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class StampSummary {
		private static final String DEFAULT_UUID = "00000000-0000-0000-0000-000000000000";
		
		private String emitterRfc;
		private String receiverRfc;
		private String serie;
		private String folio;
		private String uuid;
		private SummaryStatus status;
		private int sequence;
		private String controlId;
		private String errorMessage;
		private String xmlName;
		
		public static StampSummary summary(ApiCfdi apiCfdi, int sequence) {
			val stampResponse = apiCfdi.getStampResponse();
			val uuid = stampResponse.isStamped() ? stampResponse.getUuid() : DEFAULT_UUID;
			val status = stampResponse.isStamped() ? SummaryStatus.EMITIDO : SummaryStatus.ERROR;
			val controlId = apiCfdi.getVoucher().getSerie().concat(apiCfdi.getVoucher().getFolio());
			val xmlName = apiCfdi.getXmlPdfName();
			
			return StampSummary
					.builder()
					.emitterRfc(apiCfdi.getEmitter().getRfc())
					.receiverRfc(apiCfdi.getReceiver().getRfc())
					.serie(apiCfdi.getVoucher().getSerie())
					.folio(apiCfdi.getVoucher().getFolio())
					.uuid(uuid)
					.status(status)
					.sequence(sequence)
					.controlId(controlId)
					.errorMessage(stampResponse.getError())
					.xmlName(xmlName)
					.build();
		}
		
		public static StampSummary summary(String errorMessage) {
			return StampSummary
					.builder()
					.emitterRfc("")
					.receiverRfc("")
					.serie("")
					.folio("")
					.uuid("")
					.status(SummaryStatus.ERROR)
					.sequence(0)
					.controlId("")
					.errorMessage(errorMessage)
					.xmlName("")
					.build();
		}
		
		public static StampSummary summary(BancomextVoucherDetail voucherDetail) {
			return StampSummary
					.builder()
					.emitterRfc(voucherDetail.getEmitterRfc())
					.receiverRfc(voucherDetail.getRfcOrCurp())
					.serie(voucherDetail.getSerie())
					.folio(voucherDetail.getFolio())
					.uuid(voucherDetail.getUuid())
					.status(SummaryStatus.ERROR)
					.sequence(0)
					.controlId("")
					.errorMessage("Comprobante procesado previamente")
					.xmlName("")
					.build();
		}
	}
	
	public List<String> getRecipients() {
		if (recipients == null) {
			recipients = new ArrayList<>();
		}
		return recipients;
	}
	
}