package org.megapractical.invoicing.webapp.tenant.bancomext.service;

import java.io.IOException;

public interface BancomextFtpService {
	
	void loadFiles() throws IOException;
	
}