package org.megapractical.invoicing.webapp.tenant.bancomext.error;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.ApiFileErrorWritter;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.webapp.tenant.bancomext.util.BancomextUtils;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextS3Error;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextStampData.BancomextVoucherDetail;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextError {
	private String rfc;
	private String serie;
	private String folio;
	private String error;
	private String detail;
	private String xmlErrorName;
	private String sealedXml;

	public static void writeError(BancomextErrorDetail errorDetail, List<BancomextError> errors) {
		try {
			val errorFile = ApiFileErrorWritter.errorFile(errorDetail.getFileErrorPath());
			if (errorFile != null) {
				try (val writer = new CSVWriter(new FileWriter(errorFile), '|', ICSVWriter.NO_QUOTE_CHARACTER,
						ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END)) {
					for (val error : errors) {
						val sentence = new StringBuilder();
						sentence.append(String.format("RFC receptor: %s", error.getRfc()));
						sentence.append(String.format("\nError: %s", error.getError()));
						sentence.append(String.format("\nDetalle: %s", error.getDetail()));
						sentence.append("\n-------------------------------------------------------------------------");

						val errorSet = new String[] { sentence.toString() + "\n" };
						writer.writeNext(errorSet);

						val xmlErrorName = String.format("%s_.xml", error.getXmlErrorName());
						writeXml(errorDetail.getErrorPath(), xmlErrorName, error.getSealedXml());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeS3Error(BancomextErrorDetail errorDetail, List<BancomextS3Error> errors) {
		try {
			val errorFile = ApiFileErrorWritter.errorFile(errorDetail.getFileErrorPath());
			if (errorFile != null) {
				try (val writer = new CSVWriter(new FileWriter(errorFile), '|', ICSVWriter.NO_QUOTE_CHARACTER,
						ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END)) {
					for (val error : errors) {
						val sentence = new StringBuilder();
						sentence.append(String.format("Archivo: %s", error.getFileName()));
						sentence.append(String.format("\nError: %s", error.getError()));
						sentence.append("\n-------------------------------------------------------------------------");

						val errorSet = new String[] { sentence.toString() + "\n" };
						writer.writeNext(errorSet);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeFileError(BancomextErrorDetail errorDetail) {
		try {
			val errorFile = ApiFileErrorWritter.errorFile(errorDetail.getFileErrorPath());
			if (errorFile != null) {
				try (val writer = new CSVWriter(new FileWriter(errorFile), '|', ICSVWriter.NO_QUOTE_CHARACTER,
						ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END)) {
					val sentence = new StringBuilder();
					
					if (AssertUtils.nonNull(errorDetail.getErrorDescription())) {
						sentence.append(errorDetail.getErrorDescription());
					}

					if (AssertUtils.nonNull(errorDetail.getParserErrors())) {
						errorDetail.getParserErrors().forEach(error -> sentence.append("\n" + error));
					}

					if (AssertUtils.hasValue(errorDetail.getErrorCause())) {
						sentence.append(String.format("\nCausa: %s", errorDetail.getErrorCause()));
					}
					
					if (AssertUtils.nonNull(errorDetail.getVoucherDetails())) {
						errorDetail.getVoucherDetails().forEach(error -> sentence.append(buildError(error)));
					}

					val errorSet = new String[] { sentence.toString() + "\n" };
					writer.writeNext(errorSet);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String buildError(BancomextVoucherDetail voucherDetail) {
		val detail = new StringBuilder();
		detail.append("\nComprobante procesado previamente - ");
		detail.append("Rfc/Curp:" + voucherDetail.getRfcOrCurp());
		detail.append(", Serie:" + voucherDetail.getSerie());
		detail.append(", Folio:" + voucherDetail.getFolio());
		detail.append(", Uuid:" + voucherDetail.getUuid());
		return detail.toString();
	}

	private static void writeXml(String errorPath, String xmlErrorName, String sealedXml) {
		try {
			val xmlErrorPath = errorPath + BancomextUtils.SEPARATOR + xmlErrorName;
			UFile.writeFile(UValue.stringToByte(sealedXml), xmlErrorPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class BancomextErrorDetail {
		private String errorPath;
		private String fileErrorPath;
		private String errorDescription;
		private String errorCause;
		@Getter(AccessLevel.NONE)
		private List<String> parserErrors;
		@Getter(AccessLevel.NONE)
		List<BancomextVoucherDetail> voucherDetails;

		public BancomextErrorDetail(String errorPath, String fileErrorPath) {
			this.errorPath = errorPath;
			this.fileErrorPath = fileErrorPath;
		}

		public BancomextErrorDetail(String errorPath, String fileErrorPath, String errorDescription) {
			this.errorPath = errorPath;
			this.fileErrorPath = fileErrorPath;
			this.errorDescription = errorDescription;
		}

		public BancomextErrorDetail(String errorPath, String fileErrorPath, String errorDescription,
				List<String> parserErrors, List<BancomextVoucherDetail> voucherDetails) {
			this.errorPath = errorPath;
			this.fileErrorPath = fileErrorPath;
			this.errorDescription = errorDescription;
			this.parserErrors = parserErrors;
			this.voucherDetails = voucherDetails;
		}

		public List<String> getParserErrors() {
			if (parserErrors == null) {
				parserErrors = new ArrayList<>();
			}
			return parserErrors;
		}

		public List<BancomextVoucherDetail> getVoucherDetails() {
			if (voucherDetails == null) {
				voucherDetails = new ArrayList<>();
			}
			return voucherDetails;
		}
	}
}