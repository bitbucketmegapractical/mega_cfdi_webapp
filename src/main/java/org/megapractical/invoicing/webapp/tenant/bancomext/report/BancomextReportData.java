package org.megapractical.invoicing.webapp.tenant.bancomext.report;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BancomextReportData {
	private String templatePath;
	private BancomextPdfType pdfType;
}