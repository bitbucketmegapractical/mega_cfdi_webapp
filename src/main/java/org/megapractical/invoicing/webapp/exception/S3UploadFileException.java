package org.megapractical.invoicing.webapp.exception;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@SuppressWarnings("serial")
public class S3UploadFileException extends RuntimeException {
	
	public S3UploadFileException(String message) {
		super(message);
	}

}