package org.megapractical.invoicing.webapp.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
public class FileAlreadyExistException extends RuntimeException {
	
	public FileAlreadyExistException(String message) {
		super(message);
	}

}