package org.megapractical.invoicing.webapp.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
public class FileNotFoundException extends RuntimeException {
	
	public FileNotFoundException(String message) {
		super(message);
	}

}