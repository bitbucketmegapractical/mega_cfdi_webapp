package org.megapractical.invoicing.webapp.gson;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class GsonClient {

	private GsonClient() {
	}

	public static final <T> T deserialize(String json, Class<T> clazz) {
		try {
			return new Gson().fromJson(json, clazz);
		} catch (JsonSyntaxException e) {
			return null;
		}
	}
	
	public static final String response(Object object) {
		if (object != null) {
			return new Gson().toJson(object);
		}
		return null;
	}

}