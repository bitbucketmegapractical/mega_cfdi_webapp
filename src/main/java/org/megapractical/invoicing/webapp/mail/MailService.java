package org.megapractical.invoicing.webapp.mail;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ContactoEntity;
import org.megapractical.invoicing.dal.bean.jpa.NotificacionCfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.NotificacionPrefacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.webapp.exposition.cancellation.CancellationNotification.CancellationNotificationData;
import org.megapractical.invoicing.webapp.exposition.cancellation.CancellationNotificationResponse;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WCfdiNotification;
import org.megapractical.invoicing.webapp.wrapper.WPreInvoiceNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import lombok.val;

@Service
public class MailService {

	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	SpringTemplateEngine thymeleaf;

	@Autowired
	JavaMailSenderImpl mailSender;
	
	@Autowired
	UserTools userTools;

	private final String amazonSmtp = "email-smtp.us-east-1.amazonaws.com";
	private final String amazonUser = "AKIAW5HR2CHBBQFG6CPQ";
	private final String amazonPasswd = "BKp6TMyLXnig7YgQzTLo8CzRWBhhfS71LeQLgjXnh0Y7";
	private final String noreply = "notificaciones@megacfdi.com";
	
	private Properties mailProperties() {
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.host", this.amazonSmtp);
		properties.setProperty("mail.smtp.port", "587");
		properties.setProperty("mail.smtp.user", this.amazonUser);
		properties.setProperty("mail.smtp.password", this.amazonPasswd);
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		
		return properties;
	}
	
	public boolean sendAccountCreationConfirmationMail(String receiverEmail, String receiverName, String confirmationCode) {
		try {
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			simpleMailMessage.setTo(receiverEmail);
			
			MimeMessage message = mailSender.createMimeMessage();
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(simpleMailMessage.getFrom());
			messageHelper.setTo(simpleMailMessage.getTo());
			messageHelper.setSubject("Confirmar registro en Mega-CFDI");

			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			String rootUrl = request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());

			Context ctx = new Context();			
			ctx.setVariable("saludo", "Señor(a): " + receiverName);
			ctx.setVariable("vinculoParaConfirmar", rootUrl + "/confirmAccount?code=" + confirmationCode);

			String emailText = thymeleaf.process("/mail/AccountConfirmationTemplate", ctx);
			
			messageHelper.setText(emailText, true);
			
			Properties properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			mailSender.send(message);			
			
			userTools.log(String.format("[INFO] EMAILING SERVICE > EMAIL SENDED TO %s", receiverEmail));			
			
			return true;
			
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean sendAssociatedAccountUserMail(String receiverEmail, String receiverName, String username, String password, String activationCode) {
		try {
			
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			simpleMailMessage.setTo(receiverEmail);
			
			MimeMessage message = mailSender.createMimeMessage();
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(simpleMailMessage.getFrom());
			messageHelper.setTo(simpleMailMessage.getTo());
			messageHelper.setSubject("Cuenta de usuario en Mega-CFDI");

			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			String rootUrl = request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());
			
			Context ctx = new Context();			
			ctx.setVariable("saludo", "Señor(a): " + receiverName);
			ctx.setVariable("username", username);
			ctx.setVariable("passwd", password);
			ctx.setVariable("vinculoParaConfirmar", rootUrl + "/confirmAssociatedAccountUser?code=" + activationCode);

			String emailText = thymeleaf.process("/mail/AssociatedAccountUserTemplate", ctx);
			
			messageHelper.setText(emailText, true);
					
			Properties properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			mailSender.send(message);			
			
			userTools.log(String.format("[INFO] EMAILING SERVICE > EMAIL SENDED TO %s", receiverEmail));			
			
			return true;
			
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void sendCfdiToEmitter(WCfdiNotification notification){
		try {
			
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			
			MimeMessage message = mailSender.createMimeMessage();
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(simpleMailMessage.getFrom());
			messageHelper.setSubject("Recibo electrónico (CFDI)");
			
			Context ctx = new Context();
			ctx.setVariable("emitterRfc", notification.getEmitterRfc().toUpperCase());
			ctx.setVariable("emitterName", notification.getEmitterName().toUpperCase());
			ctx.setVariable("receiverRfc", notification.getReceiverRfc().toUpperCase());
			ctx.setVariable("receiverName", notification.getReceiverName().toUpperCase());
			ctx.setVariable("uuid", notification.getUuid().toUpperCase());
			
			PreferenciasEntity preferences = notification.getPreferences();
			
			Properties properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			
			// ##### Cfdi notification to emitter
			if(preferences.getEmisorXmlPdf() || preferences.getEmisorXml() || preferences.getEmisorPdf()){
				
				if(!UValidator.isNullOrEmpty(notification.getEmitterEmail())){
					
					simpleMailMessage.setTo(notification.getEmitterEmail());
					messageHelper.setTo(simpleMailMessage.getTo());
								
					ctx.setVariable("saludo", "Señor(a): " + notification.getEmitterName());
					
					if(preferences.getEmisorXmlPdf()){
						FileSystemResource xml = new FileSystemResource(notification.getXmlPath());
						String xmlName = notification.getUuid() + ".xml";
						messageHelper.addAttachment(xmlName, xml);
						
						FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
						String pdfName = notification.getUuid() + ".pdf";
						messageHelper.addAttachment(pdfName, pdf);
						
						ctx.setVariable("attachment", "xml y pdf");
					}else if(preferences.getEmisorXml()){
						FileSystemResource xml = new FileSystemResource(notification.getXmlPath());
						String xmlName = notification.getUuid() + ".xml";
						messageHelper.addAttachment(xmlName, xml);
						
						ctx.setVariable("attachment", "xml");
					}else if(preferences.getEmisorPdf()){
						FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
						String pdfName = notification.getUuid() + ".pdf";
						messageHelper.addAttachment(pdfName, pdf);
						
						ctx.setVariable("attachment", "pdf");
					}
					
					String emailText = thymeleaf.process("/mail/CfdiNotificationTemplate", ctx);
					
					messageHelper.setText(emailText, true);
					
					try {
						//##### Send email
						mailSender.send(message);			
						
						userTools.log(String.format("[INFO] EMAILING SERVICE > CFDI NOTIFICATION SENDED TO %s", notification.getEmitterEmail()));
						
						//##### Registering notification
						NotificacionCfdiEntity cfdiNotification = new NotificacionCfdiEntity();
						cfdiNotification.setCfdi(notification.getCfdi());
						cfdiNotification.setEmisor(Boolean.TRUE);
						cfdiNotification.setNombre(notification.getEmitterName().toUpperCase());
						cfdiNotification.setCorreoElectronico(notification.getEmitterEmail());
						cfdiNotification.setXmlPdf(preferences.getEmisorXmlPdf());
						cfdiNotification.setXml(preferences.getEmisorXml());
						cfdiNotification.setPdf(preferences.getEmisorPdf());
						cfdiNotification.setFecha(new Date());
						cfdiNotification.setHora(new Date());
						persistenceDAO.persist(cfdiNotification);
						
						userTools.log("[INFO] EMAILING SERVICE > CFDI NOTIFICATION TO EMITTER WAS STORED");
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendCfdiToReceiver(WCfdiNotification notification){
		try {
			
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			
			MimeMessage message = mailSender.createMimeMessage();
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(simpleMailMessage.getFrom());
			messageHelper.setSubject("Recibo electrónico (CFDI)");
			
			Context ctx = new Context();
			ctx.setVariable("emitterRfc", notification.getEmitterRfc().toUpperCase());
			ctx.setVariable("emitterName", notification.getEmitterName().toUpperCase());
			ctx.setVariable("receiverRfc", notification.getReceiverRfc().toUpperCase());
			ctx.setVariable("receiverName", notification.getReceiverName().toUpperCase());
			ctx.setVariable("uuid", notification.getUuid().toUpperCase());
			
			PreferenciasEntity preferences = notification.getPreferences();
			
			Properties properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			
			// ##### Cfdi notification to receiver
			if(preferences.getReceptorXmlPdf() || preferences.getReceptorXml() || preferences.getReceptorPdf()){
				
				if(!UValidator.isNullOrEmpty(notification.getReceiverEmail())){
					
					simpleMailMessage.setTo(notification.getReceiverEmail());
					messageHelper.setTo(simpleMailMessage.getTo());
								
					ctx.setVariable("saludo", "Señor(a): " + notification.getReceiverName());
					
					if(preferences.getReceptorXmlPdf()){
						FileSystemResource xml = new FileSystemResource(notification.getXmlPath());
						String xmlName = notification.getUuid() + ".xml";
						messageHelper.addAttachment(xmlName, xml);
						
						FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
						String pdfName = notification.getUuid() + ".pdf";
						messageHelper.addAttachment(pdfName, pdf);
						
						ctx.setVariable("attachment", "xml y pdf");
					}else if(preferences.getReceptorXml()){
						FileSystemResource xml = new FileSystemResource(notification.getXmlPath());
						String xmlName = notification.getUuid() + ".xml";
						messageHelper.addAttachment(xmlName, xml);
						
						ctx.setVariable("attachment", "xml");
					}else if(preferences.getReceptorPdf()){
						FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
						String pdfName = notification.getUuid() + ".pdf";
						messageHelper.addAttachment(pdfName, pdf);
						
						ctx.setVariable("attachment", "pdf");
					}
					
					String emailText = thymeleaf.process("/mail/CfdiNotificationTemplate", ctx);
					messageHelper.setText(emailText, true);
					
					try {
						//##### Send email
						mailSender.send(message);			
						
						userTools.log(String.format("[INFO] EMAILING SERVICE > CFDI NOTIFICATION SENDED TO %s", notification.getReceiverEmail()));
						
						//##### Registering notification
						NotificacionCfdiEntity cfdiNotification = new NotificacionCfdiEntity();
						cfdiNotification.setCfdi(notification.getCfdi());
						cfdiNotification.setReceptor(Boolean.TRUE);
						cfdiNotification.setNombre(notification.getReceiverName().toUpperCase());
						cfdiNotification.setCorreoElectronico(notification.getReceiverEmail());
						cfdiNotification.setXmlPdf(preferences.getReceptorXmlPdf());
						cfdiNotification.setXml(preferences.getReceptorXml());
						cfdiNotification.setPdf(preferences.getReceptorPdf());
						cfdiNotification.setFecha(new Date());
						cfdiNotification.setHora(new Date());
						persistenceDAO.persist(cfdiNotification);
						
						userTools.log("[INFO] EMAILING SERVICE > CFDI NOTIFICATION TO RECEIVER WAS STORED");
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendCfdiToContacts(WCfdiNotification notification){
		try {
			PreferenciasEntity preferences = notification.getPreferences();
			
			// ##### Cfdi notification to contacts
			if(preferences.getContactoXmlPdf() || preferences.getContactoXml() || preferences.getContactoPdf()){
				List<ContactoEntity> contacts = notification.getContacts();
				
				for (ContactoEntity item : contacts) {
				
					if(!UValidator.isNullOrEmpty(item.getCorreoElectronico())){
						
						SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
						simpleMailMessage.setFrom(this.noreply);
						
						MimeMessage message = mailSender.createMimeMessage();
						
						MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
						messageHelper.setFrom(simpleMailMessage.getFrom());
						messageHelper.setSubject("Recibo electrónico (CFDI)");
						
						Context ctx = new Context();
						ctx.setVariable("emitterRfc", notification.getEmitterRfc().toUpperCase());
						ctx.setVariable("emitterName", notification.getEmitterName().toUpperCase());
						ctx.setVariable("receiverRfc", notification.getReceiverRfc().toUpperCase());
						ctx.setVariable("receiverName", notification.getReceiverName().toUpperCase());
						ctx.setVariable("uuid", notification.getUuid().toUpperCase());
						
						simpleMailMessage.setTo(item.getCorreoElectronico());
						messageHelper.setTo(simpleMailMessage.getTo());
									
						ctx.setVariable("saludo", "Señor(a): " + item.getNombre());
						
						Properties properties = this.mailProperties();
						mailSender.setHost(properties.getProperty("mail.smtp.host"));
						mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
						mailSender.setJavaMailProperties(properties);
						mailSender.setUsername(properties.getProperty("mail.smtp.user"));
						mailSender.setPassword(properties.getProperty("mail.smtp.password"));
						
						if(preferences.getContactoXmlPdf()){
							FileSystemResource xml = new FileSystemResource(notification.getXmlPath());
							String xmlName = notification.getUuid() + ".xml";
							messageHelper.addAttachment(xmlName, xml);
							
							FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
							String pdfName = notification.getUuid() + ".pdf";
							messageHelper.addAttachment(pdfName, pdf);
							
							ctx.setVariable("attachment", "xml y pdf");
						}else if(preferences.getContactoXml()){
							FileSystemResource xml = new FileSystemResource(notification.getXmlPath());
							String xmlName = notification.getUuid() + ".xml";
							messageHelper.addAttachment(xmlName, xml);
							
							ctx.setVariable("attachment", "xml");
						}else if(preferences.getContactoPdf()){
							FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
							String pdfName = notification.getUuid() + ".pdf";
							messageHelper.addAttachment(pdfName, pdf);
							
							ctx.setVariable("attachment", "pdf");
						}
						
						String emailText = thymeleaf.process("/mail/CfdiNotificationTemplate", ctx);
						
						messageHelper.setText(emailText, true);
						
						try {
							
							//##### Send email
							mailSender.send(message);
							
							userTools.log(String.format("[INFO] EMAILING SERVICE > CFDI NOTIFICATION SENDED TO %s", item.getCorreoElectronico()));
							
							//##### Registering notification
							NotificacionCfdiEntity cfdiNotification = new NotificacionCfdiEntity();
							cfdiNotification.setCfdi(notification.getCfdi());
							cfdiNotification.setContactos(Boolean.TRUE);
							cfdiNotification.setNombre(item.getNombre().toUpperCase());
							cfdiNotification.setCorreoElectronico(item.getCorreoElectronico());
							cfdiNotification.setXmlPdf(preferences.getContactoXmlPdf());
							cfdiNotification.setXml(preferences.getContactoXml());
							cfdiNotification.setPdf(preferences.getContactoPdf());
							cfdiNotification.setFecha(new Date());
							cfdiNotification.setHora(new Date());
							persistenceDAO.persist(cfdiNotification);
							
							userTools.log("[INFO] EMAILING SERVICE > CFDI NOTIFICATION TO CONTACT WAS STORED");
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendCfdi(WCfdiNotification notification){
		try {
			sendCfdiToReceiver(notification);
			sendCfdiToEmitter(notification);
			sendCfdiToContacts(notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean sendCfdiToRecipients(WCfdiNotification notification) {
		try {
			val simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			
			val message = mailSender.createMimeMessage();
			val messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(this.noreply);
			messageHelper.setSubject("Recibo electrónico (CFDI)");
			
			val ctx = new Context();
			ctx.setVariable("emitterRfc", notification.getEmitterRfc().toUpperCase());
			ctx.setVariable("emitterName", notification.getEmitterName().toUpperCase());
			ctx.setVariable("receiverRfc", notification.getReceiverRfc().toUpperCase());
			ctx.setVariable("receiverName", notification.getReceiverName().toUpperCase());
			ctx.setVariable("uuid", notification.getUuid().toUpperCase());
			ctx.setVariable("attachment", "xml y pdf");
			
			val properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			
			// Receivers
			val recipients = notification.getRecipients();
			simpleMailMessage.setTo(recipients.toArray(new String[recipients.size()]));
			messageHelper.setTo(recipients.toArray(new String[recipients.size()]));
			
			// XML
			val xmlName = notification.getXmlName();
			val xmlInputStream = notification.getXmlInputStream();
			messageHelper.addAttachment(xmlName, new ByteArrayResource(IOUtils.toByteArray(xmlInputStream)));
			
			// PDF
			val pdfName = notification.getPdfName();
			val pdfInputStream = notification.getPdfInputStream();
			messageHelper.addAttachment(pdfName, new ByteArrayResource(IOUtils.toByteArray(pdfInputStream)));
			
			val emailText = thymeleaf.process("/mail/CfdiNotificationTemplate", ctx);
			messageHelper.setText(emailText, true);
			
			// Send email
			mailSender.send(message);
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean sendRetentionToRecipients(WCfdiNotification notification) {
		try {
			
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			
			MimeMessage message = mailSender.createMimeMessage();
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(simpleMailMessage.getFrom());
			messageHelper.setSubject("Recibo electrónico (Retenciones)");
			
			Context ctx = new Context();
			ctx.setVariable("emitterRfc", notification.getEmitterRfc().toUpperCase());
			ctx.setVariable("emitterName", notification.getEmitterName().toUpperCase());
			ctx.setVariable("receiverRfc", notification.getReceiverRfc().toUpperCase());
			ctx.setVariable("receiverName", notification.getReceiverName().toUpperCase());
			ctx.setVariable("uuid", notification.getUuid().toUpperCase());
			
			Properties properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			
			// ##### Cfdi notification to receivers
			List<String> recipients = new ArrayList<>();
			recipients.addAll(notification.getRecipients());
			
			simpleMailMessage.setTo(recipients.toArray(new String[recipients.size()]));
			messageHelper.setTo(simpleMailMessage.getTo());
			
			//##### XML
			String xmlName = notification.getUuid() + ".xml";
			InputStream xmlInputStream = notification.getXmlInputStream();
			messageHelper.addAttachment(xmlName, new ByteArrayResource(IOUtils.toByteArray(xmlInputStream)));
			
			//##### PDF
			String pdfName = notification.getUuid() + ".pdf";
			InputStream pdfInputStream = notification.getPdfInputStream();
			messageHelper.addAttachment(pdfName, new ByteArrayResource(IOUtils.toByteArray(pdfInputStream)));
			
			ctx.setVariable("attachment", "xml y pdf");
			
			String emailText = thymeleaf.process("/mail/RetentionNotificationTemplate", ctx);
			
			messageHelper.setText(emailText, true);
			
			try {
				//##### Send email
				mailSender.send(message);
				
				return true;
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void sendPayrollToReceiver(WCfdiNotification notification){
		try {
			
			MimeMessage message = mailSender.createMimeMessage();
			
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(simpleMailMessage.getFrom());
			messageHelper.setSubject("Recibo electrónico (CFDI)");
			
			Context ctx = new Context();
			ctx.setVariable("emitterRfc", notification.getEmitterRfc().toUpperCase());
			ctx.setVariable("emitterName", notification.getEmitterName().toUpperCase());
			ctx.setVariable("receiverRfc", notification.getReceiverRfc().toUpperCase());
			ctx.setVariable("receiverName", notification.getReceiverName().toUpperCase());
			ctx.setVariable("uuid", notification.getUuid().toUpperCase());
			
			PreferenciasEntity preferences = notification.getPreferences();
			
			Properties properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			
			// ##### Cfdi notification to receiver
			if(preferences.getReceptorXmlPdf() || preferences.getReceptorXml() || preferences.getReceptorPdf()){
				
				if(!UValidator.isNullOrEmpty(notification.getReceiverEmail())){
					simpleMailMessage.setTo(notification.getReceiverEmail());
					messageHelper.setTo(simpleMailMessage.getTo());
								
					if(preferences.getReceptorXmlPdf()){
						FileSystemResource xml = new FileSystemResource(notification.getXmlPath());
						String xmlName = notification.getUuid() + ".xml";
						messageHelper.addAttachment(xmlName, xml);
						
						FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
						String pdfName = notification.getUuid() + ".pdf";
						messageHelper.addAttachment(pdfName, pdf);
						
						ctx.setVariable("attachment", "xml y pdf");
					}else if(preferences.getReceptorXml()){
						FileSystemResource xml = new FileSystemResource(notification.getXmlPath());
						String xmlName = notification.getUuid() + ".xml";
						messageHelper.addAttachment(xmlName, xml);
						
						ctx.setVariable("attachment", "xml");
					}else if(preferences.getReceptorPdf()){
						FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
						String pdfName = notification.getUuid() + ".pdf";
						messageHelper.addAttachment(pdfName, pdf);
						
						ctx.setVariable("attachment", "pdf");
					}
					
					String emailText = thymeleaf.process("/mail/PayrollNotificationTemplate", ctx);
					
					messageHelper.setText(emailText, true);
					
					try {
						//##### Send email
						mailSender.send(message);			
						
						userTools.log(String.format("[INFO] EMAILING SERVICE > CFDI NOTIFICATION SENDED TO %s", notification.getReceiverEmail()));
						
						//##### Registering notification
						NotificacionCfdiEntity cfdiNotification = new NotificacionCfdiEntity();
						cfdiNotification.setNomina(notification.getPayroll());
						cfdiNotification.setReceptor(Boolean.TRUE);
						cfdiNotification.setNombre(notification.getReceiverName().toUpperCase());
						cfdiNotification.setCorreoElectronico(notification.getReceiverEmail());
						cfdiNotification.setXmlPdf(preferences.getReceptorXmlPdf());
						cfdiNotification.setXml(preferences.getReceptorXml());
						cfdiNotification.setPdf(preferences.getReceptorPdf());
						cfdiNotification.setFecha(new Date());
						cfdiNotification.setHora(new Date());
						persistenceDAO.persist(cfdiNotification);
						
						userTools.log("[INFO] EMAILING SERVICE > CFDI NOTIFICATION TO RECEIVER WAS STORED");
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendPayroll(WCfdiNotification notification){
		try {
			sendPayrollToReceiver(notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendRecoveryPassword(String receiverEmail, String receiverName, String password) {
		try {
			
			MimeMessage message = mailSender.createMimeMessage();
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(this.noreply);
			messageHelper.setTo(receiverEmail);
			messageHelper.setSubject("Reestablecimiento de contraseña en Mega-CFDI");

			Context ctx = new Context();			
			ctx.setVariable("saludo", "Señor(a): " + receiverName);
			ctx.setVariable("recoveryPasswd", password);
			
			Properties properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			
			String emailText = thymeleaf.process("/mail/PasswordRecoveryTemplate", ctx);
			
			messageHelper.setText(emailText, true);
			
			mailSender.send(message);		
			
			userTools.log(String.format("[INFO] EMAILING SERVICE > EMAIL SENDED TO %s", receiverEmail));
			
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void sendPreInvoiceToReceiver(WPreInvoiceNotification notification){
		try {
			
			MimeMessage message = mailSender.createMimeMessage();
			
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(simpleMailMessage.getFrom());
			messageHelper.setSubject("Recibo electrónico de prefactura");
			
			Context ctx = new Context();
			ctx.setVariable("emitterRfc", notification.getEmitterRfc().toUpperCase());
			ctx.setVariable("emitterName", notification.getEmitterName().toUpperCase());
			ctx.setVariable("receiverRfc", notification.getReceiverRfc().toUpperCase());
			ctx.setVariable("receiverName", notification.getReceiverName().toUpperCase());
			
			PreferenciasEntity preferences = notification.getPreferences();
			
			Properties properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			
			// ##### Cfdi notification to receiver
			if(preferences.getPrefacturaPdf()){
				
				if(!UValidator.isNullOrEmpty(notification.getReceiverEmail())){
					
					simpleMailMessage.setTo(notification.getReceiverEmail());
					messageHelper.setTo(simpleMailMessage.getTo());
								
					ctx.setVariable("saludo", "Señor(a): " + notification.getReceiverName());
					
					if(preferences.getPrefacturaPdf()){
						FileSystemResource pdf = new FileSystemResource(notification.getPdfPath());
						String pdfName = notification.getPdfName();
						messageHelper.addAttachment(pdfName, pdf);
						
						ctx.setVariable("attachment", "pdf");
					}
					
					String emailText = thymeleaf.process("/mail/PreInvoiceNotificationTemplate", ctx);
					
					messageHelper.setText(emailText, true);
					
					try {
						
						//##### Send email
						mailSender.send(message);			
						
						userTools.log(String.format("[INFO] EMAILING SERVICE > PREINVOICE NOTIFICATION SENDED TO %s", notification.getReceiverEmail()));
						
						//##### Registering notification
						NotificacionPrefacturaEntity preInvoiceNotification = new NotificacionPrefacturaEntity();
						preInvoiceNotification.setPrefactura(notification.getPreInvoice());
						preInvoiceNotification.setReceptor(Boolean.TRUE);
						preInvoiceNotification.setNombre(notification.getReceiverName().toUpperCase());
						preInvoiceNotification.setCorreoElectronico(notification.getReceiverEmail());
						preInvoiceNotification.setFecha(new Date());
						preInvoiceNotification.setHora(new Date());
						persistenceDAO.persist(preInvoiceNotification);
						
						userTools.log("[INFO] EMAILING SERVICE > PREINVOICE NOTIFICATION TO RECEIVER WAS STORED");
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendPreInvoice(WPreInvoiceNotification notification){
		try {
			sendPreInvoiceToReceiver(notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public CancellationNotificationResponse sendCancellationNotification(CancellationNotificationData data) {
		try {
			val simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(this.noreply);
			
			val message = mailSender.createMimeMessage();
			val messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(simpleMailMessage.getFrom());
			messageHelper.setSubject(data.getSubject());
			
			val ctx = new Context();
			ctx.setVariable("emitterRfc", data.getEmitterRfc());
			ctx.setVariable("emitterName", data.getEmitterName());
			ctx.setVariable("fileName", data.getFileName());
			ctx.setVariable("date", UDateTime.format(LocalDateTime.now()));
			
			val properties = this.mailProperties();
			mailSender.setHost(properties.getProperty("mail.smtp.host"));
			mailSender.setPort(Integer.valueOf(properties.getProperty("mail.smtp.port")));
			mailSender.setJavaMailProperties(properties);
			mailSender.setUsername(properties.getProperty("mail.smtp.user"));
			mailSender.setPassword(properties.getProperty("mail.smtp.password"));
			
			simpleMailMessage.setTo(data.getRecipient());
			messageHelper.setTo(simpleMailMessage.getTo());
			
			val emailText = thymeleaf.process("/mail/CancellationNotificationTemplate", ctx);
			messageHelper.setText(emailText, true);
			
			UPrint.logWithLine(UProperties.env(),
					"[INFO] MAIL SERVICE > SENDING CANCELLATION NOTIFICATION EMAIL WITH SUBJECT " + message.getSubject()
							+ " TO " + data.getRecipient());
			mailSender.send(message);
			UPrint.logWithLine(UProperties.env(), "[INFO] MAIL SERVICE > CANCELLATION NOTIFICATION EMAIL WITH SUBJECT "
					+ message.getSubject() + " SUCCESSFULLY SENT TO " + data.getRecipient());
			return CancellationNotificationResponse.sent();
		} catch (Exception e) {
			UPrint.logWithLine(UProperties.env(),
					"[ERROR] MAIL SERVICE > CANCELLATION NOTIFICATION EMAIL HAS NOT BEEN SENT. REASON : "
							+ e.getMessage());
			e.printStackTrace();
			return CancellationNotificationResponse.error(e.getMessage());
		}
	}

	public String getNoreply() {
		return noreply;
	}	
}