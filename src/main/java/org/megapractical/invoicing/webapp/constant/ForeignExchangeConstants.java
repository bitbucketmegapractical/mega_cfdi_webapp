package org.megapractical.invoicing.webapp.constant;

public final class ForeignExchangeConstants {
	
	private ForeignExchangeConstants() {		
	}
	
	public static final String PACKAGE = "org.megapractical.invoicing.sat.complement.foreignexchange";
	
	public static final String CURRENT_VERSION = "1.0";
	
}