package org.megapractical.invoicing.webapp.report;

import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.webapp.report.payload.ReportParam;

public interface RetentionV2ReportService {
	
	ReportParam populateData(ApiRetention apiRetention);
	
}