package org.megapractical.invoicing.webapp.report;

import java.util.Map;

public interface CfdiV4ReportTemplateService {
	
	void cfdiV4Template(Map<String, Object> parameters);
	
	void cfdiV4PaymentTemplate(Map<String, Object> parameters);
	
}