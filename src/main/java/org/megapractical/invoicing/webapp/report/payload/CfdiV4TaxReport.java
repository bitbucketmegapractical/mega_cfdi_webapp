package org.megapractical.invoicing.webapp.report.payload;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.BaseTax;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.PaymentWithheld;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class CfdiV4TaxReport {
	private String conceptNumber;
	private String base;
	private String tax;
	private String factor;
	private String rateOrFee;
	private String amount;
	
	public CfdiV4TaxReport() {			
	}
	
	public CfdiV4TaxReport(ApiConceptTaxes item, Integer decimal) {
		this.conceptNumber = item.getConceptNumber();
		this.base = UValue.bigDecimalString(item.getBase(), decimal);
		this.tax = item.getTax();
		this.factor = item.getFactor();
		this.rateOrFee = item.getRateOrFee();
		this.amount = UValue.bigDecimalString(item.getAmount(), decimal);
	}
	
	public CfdiV4TaxReport(BaseTax item, Integer decimal) {
		this.conceptNumber = item.getNumber();
		this.base = UValue.bigDecimalString(item.getBase(), decimal);
		this.tax = item.getTax();
		this.factor = item.getFactor();
		this.rateOrFee = item.getRateOrFee();
		this.amount = UValue.bigDecimalString(item.getAmount(), decimal);
	}
	
	public CfdiV4TaxReport(PaymentWithheld item, Integer decimal) {
		this.conceptNumber = item.getNumber();
		this.tax = item.getTax();
		this.amount = UValue.bigDecimalString(item.getAmount(), decimal);
	}

	/*Getters and Setters*/
	public String getConceptNumber() {
		return conceptNumber;
	}

	public void setConceptNumber(String conceptNumber) {
		this.conceptNumber = conceptNumber;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getFactor() {
		return factor;
	}

	public void setFactor(String factor) {
		this.factor = factor;
	}

	public String getRateOrFee() {
		return rateOrFee;
	}

	public void setRateOrFee(String rateOrFee) {
		this.rateOrFee = rateOrFee;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
}