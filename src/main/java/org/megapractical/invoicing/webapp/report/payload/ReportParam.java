package org.megapractical.invoicing.webapp.report.payload;

import java.util.HashMap;
import java.util.Map;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class ReportParam {
	private Map<String, Object> parameters;
	
	public ReportParam() {
	}
	
	public ReportParam(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	public Map<String, Object> getParameters() {
		if (parameters == null) {
			parameters = new HashMap<>();
		}
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
	
}