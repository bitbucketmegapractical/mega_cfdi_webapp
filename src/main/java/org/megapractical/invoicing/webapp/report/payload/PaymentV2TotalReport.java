package org.megapractical.invoicing.webapp.report.payload;

import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiPayment.PaymentTotal;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class PaymentV2TotalReport {
	private String totalWithheldIva;
	private String totalWithheldIsr;
	private String totalWithheldIeps;
	private String totalTransferredBaseIva16;
	private String totalTransferredTaxIva16;
	private String totalTransferredBaseIva8;
	private String totalTransferredTaxIva8;
	private String totalTransferredBaseIva0;
	private String totalTransferredTaxIva0;
	private String totalTransferredBaseIvaExempt;
	private String totalAmountPayments;
	
	public static final PaymentV2TotalReport convert(PaymentTotal pt, Integer decimal) {
		PaymentV2TotalReport paymentTotal = new PaymentV2TotalReport();
		if (pt != null) {
			paymentTotal.setTotalWithheldIva(UValue.bigDecimalStringForce(pt.getTotalWithheldIva(), decimal));
			paymentTotal.setTotalWithheldIsr(UValue.bigDecimalStringForce(pt.getTotalWithheldIsr(), decimal));
			paymentTotal.setTotalWithheldIeps(UValue.bigDecimalStringForce(pt.getTotalWithheldIeps(), decimal));
			paymentTotal.setTotalTransferredBaseIva16(UValue.bigDecimalStringForce(pt.getTotalTransferredBaseIva16(), decimal));
			paymentTotal.setTotalTransferredTaxIva16(UValue.bigDecimalStringForce(pt.getTotalTransferredTaxIva16(), decimal));
			paymentTotal.setTotalTransferredBaseIva8(UValue.bigDecimalStringForce(pt.getTotalTransferredBaseIva8(), decimal));
			paymentTotal.setTotalTransferredTaxIva8(UValue.bigDecimalStringForce(pt.getTotalTransferredTaxIva8(), decimal));
			paymentTotal.setTotalTransferredBaseIva0(UValue.bigDecimalStringForce(pt.getTotalTransferredBaseIva0(), decimal));
			paymentTotal.setTotalTransferredTaxIva0(UValue.bigDecimalStringForce(pt.getTotalTransferredTaxIva0(), decimal));
			paymentTotal.setTotalTransferredBaseIvaExempt(UValue.bigDecimalStringForce(pt.getTotalTransferredBaseIvaExempt(), decimal));
			paymentTotal.setTotalAmountPayments(UValue.bigDecimalStringForce(pt.getTotalAmountPayments(), decimal));
		}
		return paymentTotal;
	}

	/*Getters and Setters*/
	public String getTotalWithheldIva() {
		return totalWithheldIva;
	}

	public void setTotalWithheldIva(String totalWithheldIva) {
		this.totalWithheldIva = totalWithheldIva;
	}

	public String getTotalWithheldIsr() {
		return totalWithheldIsr;
	}

	public void setTotalWithheldIsr(String totalWithheldIsr) {
		this.totalWithheldIsr = totalWithheldIsr;
	}

	public String getTotalWithheldIeps() {
		return totalWithheldIeps;
	}

	public void setTotalWithheldIeps(String totalWithheldIeps) {
		this.totalWithheldIeps = totalWithheldIeps;
	}

	public String getTotalTransferredBaseIva16() {
		return totalTransferredBaseIva16;
	}

	public void setTotalTransferredBaseIva16(String totalTransferredBaseIva16) {
		this.totalTransferredBaseIva16 = totalTransferredBaseIva16;
	}

	public String getTotalTransferredTaxIva16() {
		return totalTransferredTaxIva16;
	}

	public void setTotalTransferredTaxIva16(String totalTransferredTaxIva16) {
		this.totalTransferredTaxIva16 = totalTransferredTaxIva16;
	}

	public String getTotalTransferredBaseIva8() {
		return totalTransferredBaseIva8;
	}

	public void setTotalTransferredBaseIva8(String totalTransferredBaseIva8) {
		this.totalTransferredBaseIva8 = totalTransferredBaseIva8;
	}

	public String getTotalTransferredTaxIva8() {
		return totalTransferredTaxIva8;
	}

	public void setTotalTransferredTaxIva8(String totalTransferredTaxIva8) {
		this.totalTransferredTaxIva8 = totalTransferredTaxIva8;
	}

	public String getTotalTransferredBaseIva0() {
		return totalTransferredBaseIva0;
	}

	public void setTotalTransferredBaseIva0(String totalTransferredBaseIva0) {
		this.totalTransferredBaseIva0 = totalTransferredBaseIva0;
	}

	public String getTotalTransferredTaxIva0() {
		return totalTransferredTaxIva0;
	}

	public void setTotalTransferredTaxIva0(String totalTransferredTaxIva0) {
		this.totalTransferredTaxIva0 = totalTransferredTaxIva0;
	}

	public String getTotalTransferredBaseIvaExempt() {
		return totalTransferredBaseIvaExempt;
	}

	public void setTotalTransferredBaseIvaExempt(String totalTransferredBaseIvaExempt) {
		this.totalTransferredBaseIvaExempt = totalTransferredBaseIvaExempt;
	}

	public String getTotalAmountPayments() {
		return totalAmountPayments;
	}

	public void setTotalAmountPayments(String totalAmountPayments) {
		this.totalAmountPayments = totalAmountPayments;
	}
	
}