package org.megapractical.invoicing.webapp.report;

import static java.util.stream.Collectors.toList;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;
import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.report.ReportManagerTools;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.webapp.report.payload.CfdiV4ConceptReport;
import org.megapractical.invoicing.webapp.report.payload.CfdiV4CustomsInformationReport;
import org.megapractical.invoicing.webapp.report.payload.CfdiV4PropertyAccountReport;
import org.megapractical.invoicing.webapp.report.payload.CfdiV4RelatedCfdiReport;
import org.megapractical.invoicing.webapp.report.payload.CfdiV4TaxReport;
import org.megapractical.invoicing.webapp.report.payload.PaymentV2RelatedDocumentReport;
import org.megapractical.invoicing.webapp.report.payload.PaymentV2Report;
import org.megapractical.invoicing.webapp.report.payload.PaymentV2TotalReport;
import org.megapractical.invoicing.webapp.report.payload.ReportParam;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class CfdiV4ReportServiceImpl implements CfdiV4ReportService {
	
	@Autowired
	private CfdiV4ReportTemplateService cfdiV4ReportTemplateService;
	
	private static final String CATALOG_FORMAT = "%s %s";
	
	@Override
	public ReportParam populateData(ApiCfdi cfdi) {
		val parameters = new HashMap<String, Object>();

		setGeneralData(cfdi, parameters);
		setEmitterData(cfdi, parameters);
		setReceiverData(cfdi, parameters);
		setVoucherData(cfdi, parameters);
		setConceptData(cfdi, parameters);
		cfdiV4ReportTemplateService.cfdiV4Template(parameters);
		
		return new ReportParam(parameters);
	}
	
	@Override
	public ReportParam paymentPopulateData(ApiCfdi cfdi) {
		val parameters = new HashMap<String, Object>();
		
		setEmitterData(cfdi, parameters);
		setReceiverData(cfdi, parameters);
		setVoucherDataForPayment(cfdi, parameters);
		setPaymentTotals(cfdi, parameters);
		setPayments(cfdi, parameters);
		cfdiV4ReportTemplateService.cfdiV4PaymentTemplate(parameters);
		
		return new ReportParam(parameters);
	}

	private final void setGeneralData(ApiCfdi cfdi, Map<String, Object> parameters) {
		val voucher = cfdi.getStampResponse().getVoucher();
		
		String invoiceType = null;
		if (voucher.getTipoDeComprobante().value().equalsIgnoreCase(VoucherUtils.VOUCHER_TYPE_CODE_I)) {
			invoiceType = "FACTURA";
		} else if (voucher.getTipoDeComprobante().value().equalsIgnoreCase(VoucherUtils.VOUCHER_TYPE_CODE_E)) {
			invoiceType = "NOTA DE CRÉDITO";
		}
        
		parameters.put("logo", getLogo(cfdi));
        parameters.put("invoice.type", invoiceType);
        parameters.put("legend", cfdi.getLegend());
	}
	
	private final InputStream getLogo(ApiCfdi cfdi) {
		try {
        	
			val emitterLogo = cfdi.getEmitter().getLogo();
			if (!UValidator.isNullOrEmpty(emitterLogo)) {
				val logoFile = new File(emitterLogo);
				return UFile.fileToInputStream(logoFile);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private final void setEmitterData(ApiCfdi cfdi, Map<String, Object> parameters) {
		val emitter = cfdi.getEmitter();
		val name = UValue.revertSingleReplace(emitter.getNombreRazonSocial());
		val emitterTaxRegime = emitter.getRegimenFiscal();
		val taxRegime = String.format(CATALOG_FORMAT, emitterTaxRegime.getCodigo(), emitterTaxRegime.getValor());
		
		parameters.put("emitter.rfc", emitter.getRfc());
		parameters.put("emitter.name", name);
		parameters.put("emitter.tax.regime", taxRegime);
	}

	private final void setReceiverData(ApiCfdi cfdi, Map<String, Object> parameters) {
		val receiver = cfdi.getReceiver();
		val name = UValue.revertSingleReplace(cfdi.getReceiver().getNombreRazonSocial());
		
		var resident = "-";
		if (receiver.getResidente() != null) {
			resident = receiver.getResidente();
		}

		val receiverCfdiUse = receiver.getUsoCFDI();
		val cfdiUse = String.format(CATALOG_FORMAT, receiverCfdiUse.getCodigo(), receiverCfdiUse.getValor());

		val receiverTaxRegime = receiver.getTaxRegimeEntity();
		val taxRegime = String.format(CATALOG_FORMAT, receiverTaxRegime.getCodigo(), receiverTaxRegime.getValor());

		parameters.put("receiver.rfc", receiver.getRfc());
		parameters.put("receiver.name", name);
		parameters.put("receiver.resident", resident);
		parameters.put("receiver.cfdi.use", cfdiUse);
		parameters.put("receiver.postal.code", receiver.getDomicilioFiscal());
		parameters.put("receiver.tax.regime", taxRegime);
	}
	
	private final void setVoucherData(ApiCfdi cfdi, Map<String, Object> parameters) {
		val voucher = cfdi.getVoucher();
		val stampResponse = cfdi.getStampResponse();
		val decimal = cfdi.getCurrency().getDecimales();
		val cfdv4 = stampResponse.getCfdv4();
		
		setVoucherType(cfdi, parameters);
		setCurrency(cfdi, parameters);
		setChangeType(parameters, voucher, decimal);
		setSerie(parameters, voucher);
		setSheet(parameters, voucher);
		setPaymentMethod(cfdi, parameters);
		setPaymentWay(cfdi, parameters);
		setSubtotal(parameters, voucher, decimal);
		setDiscount(parameters, voucher, decimal);
		setTotal(parameters, voucher, decimal);
		setTaxes(cfdi, parameters, decimal);
		setRelatedCfdi(cfdi, parameters);
		setPropertyAccounts(cfdi, parameters);
		setCustomsInformations(cfdi, parameters);
		
		parameters.put("voucher.postal.code", voucher.getPostalCode());
		parameters.put("voucher.stamp.date", stampResponse.getDateStamp());
		parameters.put("voucher.tax.sheet", voucher.getTaxSheet());
		parameters.put("voucher.cert.emitter.num", stampResponse.getCertificateEmitterNumber());
		parameters.put("voucher.cert.sat.num", stampResponse.getCertificateSatNumber());
		parameters.put("voucher.confirmation.code", voucher.getConfirmation());
		parameters.put("voucher.payment.conditions", UValue.revertSingleReplace(voucher.getPaymentConditions()));
		parameters.put("voucher.emitter.seal", stampResponse.getEmitterSeal());
		parameters.put("voucher.sat.seal", stampResponse.getSatSeal());
		parameters.put("voucher.original.string", stampResponse.getOriginalString());
		
		ByteArrayInputStream qrCode = null;
		var totalInLetters = "";
		if (cfdv4 != null) {
			qrCode = ReportManagerTools.getQR(cfdv4, cfdi.getStampResponse());
			totalInLetters = ReportManagerTools.totalInLetter(cfdv4);
		} else {
			qrCode = ReportManagerTools.getQR(cfdi.getStampResponse());
			totalInLetters = ReportManagerTools.totalInLetter(voucher.getTotal(), voucher.getCurrencyString());
		}
		parameters.put("voucher.qr.code.image", qrCode);
		parameters.put("voucher.total.letters", totalInLetters);
	}

	private void setVoucherType(ApiCfdi cfdi, Map<String, Object> parameters) {
		val voucherTypeEntity = cfdi.getVoucherType();
		val voucherType = String.format(CATALOG_FORMAT, voucherTypeEntity.getCodigo(), voucherTypeEntity.getValor());
		parameters.put("voucher.type", voucherType);
	}

	private void setCurrency(ApiCfdi cfdi, Map<String, Object> parameters) {
		val currencyEntity = cfdi.getCurrency();
		val currency = String.format(CATALOG_FORMAT, currencyEntity.getCodigo(), currencyEntity.getValor());
		parameters.put("voucher.currency", currency);
	}

	private void setChangeType(Map<String, Object> parameters, final ApiVoucher voucher, final Integer decimal) {
		var changeType = "-";
		if (voucher.getChangeType() != null) {
			changeType = UValue.bigDecimalString(voucher.getChangeType(), decimal);
		}
		parameters.put("voucher.change.type", changeType);
	}

	private void setSerie(Map<String, Object> parameters, final ApiVoucher voucher) {
		var serie = "-";
		if (voucher.getSerie() != null) {
			serie = voucher.getSerie();
		}
		parameters.put("voucher.serie", serie);
	}

	private void setSheet(Map<String, Object> parameters, final ApiVoucher voucher) {
		var sheet = "-";
		if (voucher.getFolio() != null) {
			sheet = voucher.getFolio();
		}
		parameters.put("voucher.sheet", sheet);
	}

	private void setPaymentMethod(ApiCfdi cfdi, Map<String, Object> parameters) {
		var paymentMethod = "-";
		if (cfdi.getPaymentMethod() != null) {
			val pm = cfdi.getPaymentMethod();
			paymentMethod = String.format(CATALOG_FORMAT, pm.getCodigo(), pm.getValor());
		}
		parameters.put("voucher.payment.method", paymentMethod);
	}

	private void setPaymentWay(ApiCfdi cfdi, Map<String, Object> parameters) {
		var paymentWay = "-";
		if (cfdi.getPaymentWay() != null) {
			val pw = cfdi.getPaymentWay();
			paymentWay = String.format(CATALOG_FORMAT, pw.getCodigo(), pw.getValor());
		}
		parameters.put("voucher.payment.way", paymentWay);
	}

	private void setSubtotal(Map<String, Object> parameters, final ApiVoucher voucher, final Integer decimal) {
		val subtotal = UValue.bigDecimalString(voucher.getSubTotal(), decimal);
		parameters.put("voucher.subtotal", subtotal);
	}

	private void setTotal(Map<String, Object> parameters, final ApiVoucher voucher, final Integer decimal) {
		val total = UValue.bigDecimalString(voucher.getTotal(), decimal);
		parameters.put("voucher.total", total);
	}

	private void setDiscount(Map<String, Object> parameters, final ApiVoucher voucher, final Integer decimal) {
		var discount = "0.00";
		if (voucher.getDiscountString() != null) {
			discount = UValue.bigDecimalString(voucher.getDiscountString(), decimal);
		}
		parameters.put("voucher.discount", discount);
	}

	private final void setConceptData(ApiCfdi cfdi, Map<String, Object> parameters) {
		val decimal = cfdi.getCurrency().getDecimales();
		
		val conceptReportList = cfdi.getConcepts().stream()
												  .map(item -> CfdiV4ConceptReport.convert(item, decimal))
												  .collect(toList());
		parameters.put("voucher.concepts", conceptReportList);
		
		val iedus = conceptReportList.stream()
									 .map(CfdiV4ConceptReport::getIedu)
									 .filter(Objects::nonNull)
									 .collect(toList());
		if (!iedus.isEmpty()) {
			val iedu = iedus.get(0);
			parameters.put("cc.iedu", "complemento.concepto.instituciones.educativas.privadas");
    		parameters.put("cc.iedu.version", iedu.getVersion());
    		parameters.put("cc.iedu.name", iedu.getStudentName());
    		parameters.put("cc.iedu.curp", iedu.getCurp());
    		parameters.put("cc.iedu.education.level", iedu.getEducationalLevel());
    		parameters.put("cc.iedu.aut.rvoe", iedu.getAutRvoe());
    		parameters.put("cc.iedu.rfc", iedu.getPaymentRfc());
		}
	}
	
	private void setTaxes(ApiCfdi cfdi, Map<String, Object> parameters, final Integer decimal) {
		val voucher = cfdi.getVoucher();
		
		val transferreds = cfdi.getTransferred().stream()
												.map(item -> new CfdiV4TaxReport(item, decimal))
												.collect(toList());
		if (!transferreds.isEmpty()) {
			parameters.put("voucher.transferreds", transferreds);
		}
		
		val withhelds = cfdi.getWithheld().stream()
										  .map(item -> new CfdiV4TaxReport(item, decimal))
										  .collect(toList());
		if (!withhelds.isEmpty()) {
			parameters.put("voucher.withhelds", withhelds);
		}
		
		if (voucher.getTaxes() != null) {
			if (voucher.getTaxes().getTotalTaxTransferred() != null) {
				val tt = UValue.bigDecimalString(cfdi.getVoucher().getTaxes().getTotalTaxTransferred(), decimal);
				parameters.put("voucher.total.tax.transferred", tt);
			}
			
			if (voucher.getTaxes().getTotalTaxWithheld() != null) {
				val tw = UValue.bigDecimalString(cfdi.getVoucher().getTaxes().getTotalTaxWithheld(), decimal);
				parameters.put("voucher.total.tax.withheld", tw);
			}
		}
	}
	
	private void setRelatedCfdi(ApiCfdi cfdi, Map<String, Object> parameters) {
		val voucher = cfdi.getVoucher();
		if (voucher.getRelatedCfdi() != null) {
			val relatedCfdi = cfdi.getVoucher().getRelatedCfdi();

			val rcCode = relatedCfdi.getRelationshipTypeCode();
			val rcValue = cfdi.getVoucher().getRelatedCfdi().getRelationshipTypeValue();
			parameters.put("related.cfdi.relationship", String.format(CATALOG_FORMAT, rcCode, rcValue));

			val uuids = relatedCfdi.getUuids().stream()
											  .map(RelatedCfdiUuid::getUuid)
											  .map(CfdiV4RelatedCfdiReport::new)
											  .collect(toList());
			parameters.put("related.cfdi.uuids", uuids);
		}
	}
	
	private void setPropertyAccounts(ApiCfdi cfdi, Map<String, Object> parameters) {
		val propertyAccounts = cfdi.getPropertyAccounts().stream()
														 .map(CfdiV4PropertyAccountReport::new)
														 .collect(toList());
		if (!propertyAccounts.isEmpty()) {
			parameters.put("voucher.property.account.list", propertyAccounts);
		}
	}
	
	private void setCustomsInformations(ApiCfdi cfdi, Map<String, Object> parameters) {
		val customsInformations = cfdi.getCustomsInformations().stream()
															   .map(CfdiV4CustomsInformationReport::new)
															   .collect(toList());
		if (!customsInformations.isEmpty()) {
			parameters.put("voucher.customs.information.list", customsInformations);
		}
	}
	
	private final void setVoucherDataForPayment(ApiCfdi cfdi, Map<String, Object> parameters) {
		val voucher = cfdi.getVoucher();
		val stampResponse = cfdi.getStampResponse();
		val decimal = cfdi.getCurrency().getDecimales();
		val cfdv4 = stampResponse.getCfdv4();
		
		setVoucherType(cfdi, parameters);
		setChangeType(parameters, voucher, decimal);
		setSerie(parameters, voucher);
		setSheet(parameters, voucher);
		setTaxes(cfdi, parameters, decimal);
		setRelatedCfdi(cfdi, parameters);
		
		parameters.put("voucher.complement", "payment");
		parameters.put("voucher.postal.code", voucher.getPostalCode());
		parameters.put("voucher.stamp.date", stampResponse.getDateStamp());
		parameters.put("voucher.tax.sheet", voucher.getTaxSheet());
		parameters.put("voucher.cert.emitter.num", stampResponse.getCertificateEmitterNumber());
		parameters.put("voucher.cert.sat.num", stampResponse.getCertificateSatNumber());		
		parameters.put("voucher.emitter.seal", stampResponse.getEmitterSeal());
		parameters.put("voucher.sat.seal", stampResponse.getSatSeal());
		parameters.put("voucher.original.string", stampResponse.getOriginalString());
		parameters.put("voucher.qr.code.image", ReportManagerTools.getQR(cfdv4, cfdi.getStampResponse()));
		parameters.put("voucher.total.letters", ReportManagerTools.totalInLetter(cfdv4));
	}
	
	private void setPaymentTotals(ApiCfdi cfdi, HashMap<String, Object> parameters) {
		val paymentTotal = cfdi.getComplement().getApiPayment().getPaymentTotal();
		val totalReport = PaymentV2TotalReport.convert(paymentTotal, 2);
		
		parameters.put("total.withheld.iva", totalReport.getTotalWithheldIva());
		parameters.put("total.withheld.isr", totalReport.getTotalWithheldIsr());
		parameters.put("total.withheld.ieps", totalReport.getTotalWithheldIeps());
		parameters.put("total.transferred.base.iva16", totalReport.getTotalTransferredBaseIva16());
		parameters.put("total.transferred.tax.iva16", totalReport.getTotalTransferredTaxIva16());
		parameters.put("total.transferred.base.iva8", totalReport.getTotalTransferredBaseIva8());
		parameters.put("total.transferred.tax.iva8", totalReport.getTotalTransferredTaxIva8());
		parameters.put("total.transferred.base.iva0", totalReport.getTotalTransferredBaseIva0());
		parameters.put("total.transferred.tax.iva0", totalReport.getTotalTransferredTaxIva0());
		parameters.put("total.transferred.base.iva.exempt", totalReport.getTotalTransferredBaseIvaExempt());
		parameters.put("total.amount.payments", totalReport.getTotalAmountPayments());
	}
	
	private void setPayments(ApiCfdi cfdi, HashMap<String, Object> parameters) {
		val payments = cfdi.getComplement().getApiPayment().getPayments();
		
		val reportPayments = payments.stream().map(PaymentV2Report::convert).collect(toList());
		parameters.put("payments", reportPayments);
		
		val paymentTransferreds = reportPayments.stream().flatMap(PaymentV2Report::getTaxesTransferred).collect(toList());
		parameters.put("payment.transferreds", paymentTransferreds);
		
		val paymentWithhelds = reportPayments.stream().flatMap(PaymentV2Report::getTaxesWithhelds).collect(toList());
		parameters.put("payment.withhelds", paymentWithhelds);
		
		val reportRelatedDocs = reportPayments.stream().flatMap(PaymentV2Report::getDocuments).collect(toList());
		parameters.put("payment.related.documents", reportRelatedDocs);
		
		val relatedDocTransferreds = reportRelatedDocs.stream().flatMap(PaymentV2RelatedDocumentReport::getTaxesTransferred).collect(toList());
		parameters.put("payment.related.documents.transferreds", relatedDocTransferreds);
		
		val relatedDocWithhelds = reportRelatedDocs.stream().flatMap(PaymentV2RelatedDocumentReport::getTaxesWithhelds).collect(toList());
		parameters.put("payment.related.documents.withhelds", relatedDocWithhelds);
	}

}