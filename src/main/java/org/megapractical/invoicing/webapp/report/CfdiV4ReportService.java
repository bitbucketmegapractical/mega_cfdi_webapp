package org.megapractical.invoicing.webapp.report;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.webapp.report.payload.ReportParam;

public interface CfdiV4ReportService {
	
	ReportParam populateData(ApiCfdi cfdi);
	
	ReportParam paymentPopulateData(ApiCfdi cfdi);
	
}