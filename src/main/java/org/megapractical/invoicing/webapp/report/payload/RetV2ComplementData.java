package org.megapractical.invoicing.webapp.report.payload;

import org.megapractical.invoicing.sat.complement.dividendos.Dividendos;
import org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenaciondeAcciones;
import org.megapractical.invoicing.sat.complement.intereses.Intereses;
import org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros;
import org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RetV2ComplementData {
	private RetComplement retComplement;
	private Dividendos div;
	private EnajenaciondeAcciones ena;
	private Intereses ints;
	private OperacionesconDerivados ocd;
	private PagosaExtranjeros pex;
	private SectorFinanciero sfi;
	
	public enum RetComplement {
		INTS, OCD, DIV, ENA, PEX, SFI
	}
}