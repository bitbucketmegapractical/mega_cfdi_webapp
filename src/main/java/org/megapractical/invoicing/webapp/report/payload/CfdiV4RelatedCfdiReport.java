package org.megapractical.invoicing.webapp.report.payload;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class CfdiV4RelatedCfdiReport {
	private String uuid;
	
	public CfdiV4RelatedCfdiReport() {		
	}
	
	public CfdiV4RelatedCfdiReport(String uuid) {
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}