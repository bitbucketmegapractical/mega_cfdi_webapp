package org.megapractical.invoicing.webapp.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.megapractical.invoicing.api.report.ReportManagerTools;
import org.megapractical.invoicing.api.util.MoneyUtils;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.sat.complement.dividendos.Dividendos;
import org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenaciondeAcciones;
import org.megapractical.invoicing.sat.complement.intereses.Intereses;
import org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros;
import org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero;
import org.megapractical.invoicing.sat.integration.retention.v2.RETv20;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones.Complemento;
import org.megapractical.invoicing.webapp.exposition.retention.source.RetentionDataSource;
import org.megapractical.invoicing.webapp.json.JOption;
import org.megapractical.invoicing.webapp.report.payload.ReportParam;
import org.megapractical.invoicing.webapp.report.payload.RetV2ComplementData;
import org.megapractical.invoicing.webapp.report.payload.RetV2ComplementData.RetComplement;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class RetentionV2ReportServiceImpl implements RetentionV2ReportService {

	private final RetentionV2ReportTemplateService retentionV2ReportTemplateService;
	private final RetentionDataSource retentionDataSource;

	public RetentionV2ReportServiceImpl(RetentionV2ReportTemplateService retentionV2ReportTemplateService, 
										RetentionDataSource retentionDataSource) {
		this.retentionV2ReportTemplateService = retentionV2ReportTemplateService;
		this.retentionDataSource = retentionDataSource;
	}

	private static final Map<Class<?>, RetComplement> COMPLEMENT_TYPE_MAP = new HashMap<>();
	static {
	    COMPLEMENT_TYPE_MAP.put(Dividendos.class, RetComplement.DIV);
	    COMPLEMENT_TYPE_MAP.put(EnajenaciondeAcciones.class, RetComplement.ENA);
	    COMPLEMENT_TYPE_MAP.put(Intereses.class, RetComplement.INTS);
	    COMPLEMENT_TYPE_MAP.put(OperacionesconDerivados.class, RetComplement.OCD);
	    COMPLEMENT_TYPE_MAP.put(PagosaExtranjeros.class, RetComplement.PEX);
	    COMPLEMENT_TYPE_MAP.put(SectorFinanciero.class, RetComplement.SFI);
	}
	
	@Override
	public ReportParam populateData(ApiRetention apiRetention) {
		val satRet = apiRetention.getStampResponse().getRetention();
		val retComplement = getRetComplement(satRet);
		val parameters = new HashMap<String, Object>();

		setEmitterData(apiRetention, parameters);
		setReceiverData(apiRetention, parameters);
		setRetentionData(satRet, parameters);
		setPeriodData(satRet, parameters);
		setTotalsData(satRet, parameters);
		setRelatedDocumentData(apiRetention, parameters);
		setTaxWithheldData(apiRetention, parameters);
		setVoucherData(apiRetention, satRet, parameters);
		setComplementData(apiRetention, retComplement, parameters);
		retentionV2ReportTemplateService.retentionV2Template(parameters);

		return new ReportParam(parameters);
	}

	private Complemento getRetComplement(Retenciones satRet) {
		try {
			val retObj = new RETv20(satRet);
			if (retObj.getRetenciones() != null) {
				return retObj.getRetenciones().getComplemento();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setEmitterData(ApiRetention apiRetention, final Map<String, Object> parameters) {
		parameters.put("emitter.rfc", apiRetention.getEmitterRfc());
		parameters.put("emitter.business.name", apiRetention.getEmitterBusinessName());
		parameters.put("emitter.tax.regime", apiRetention.getEmitterTaxRegime());
	}

	private void setReceiverData(ApiRetention apiRetention, final Map<String, Object> parameters) {
		val receiverName = !UValidator.isNullOrEmpty(apiRetention.getReceiverNatBusinessName())
				? apiRetention.getReceiverNatBusinessName()
				: apiRetention.getReceiverExtBusinessName();

		parameters.put("receiver.rfc", apiRetention.getReceiverNatRfc());
		parameters.put("receiver.business.name", receiverName);
		parameters.put("receiver.postal.code", apiRetention.getReceiverNatPostalCode());
		parameters.put("receiver.curp", apiRetention.getReceiverNatCurp());
		parameters.put("receiver.resident", apiRetention.getReceiverNationality());
		parameters.put("receiver.nrif", apiRetention.getReceiverExtNumber());
	}

	private void setRetentionData(Retenciones satRet, final Map<String, Object> parameters) {
		parameters.put("info.ret.key", satRet.getCveRetenc());
		parameters.put("info.expedition.place", satRet.getLugarExpRetenc());
		parameters.put("info.folio", satRet.getFolioInt());
		parameters.put("info.description", satRet.getDescRetenc());
	}

	private void setPeriodData(Retenciones satRet, final Map<String, Object> parameters) {
		parameters.put("period.initial.month", satRet.getPeriodo().getMesIni());
		parameters.put("period.end.month", satRet.getPeriodo().getMesFin());
		parameters.put("period.year", satRet.getPeriodo().getEjercicio());
	}

	private void setTotalsData(Retenciones satRet, final Map<String, Object> parameters) {
		parameters.put("total.operation", MoneyUtils.format(satRet.getTotales().getMontoTotOperacion()));
		parameters.put("total.taxed", MoneyUtils.format(satRet.getTotales().getMontoTotGrav()));
		parameters.put("total.exempt", MoneyUtils.format(satRet.getTotales().getMontoTotExent()));
		parameters.put("total.withheld", MoneyUtils.format(satRet.getTotales().getMontoTotRet()));
		parameters.put("total.quarterly.profit", MoneyUtils.format(satRet.getTotales().getUtilidadBimestral()));
		parameters.put("total.corresponding.isr", MoneyUtils.format(satRet.getTotales().getISRCorrespondiente()));
	}

	private void setTaxWithheldData(ApiRetention apiRetention, final Map<String, Object> parameters) {
		if (!apiRetention.getApiRetImpRetenidos().isEmpty()) {
			apiRetention.getApiRetImpRetenidos()
						.forEach(i -> {
							val customValue = retentionDataSource.getPaymetTypeSource().stream()
																					   .filter(opt -> opt.getCode().equalsIgnoreCase(i.getPaymentType()))
																					   .map(JOption::getCustomValue)
																					   .findFirst().orElse(null);
							i.setPaymentType(customValue);
						}); 
			parameters.put("tax.withheld.list", apiRetention.getApiRetImpRetenidos());
		}
	}
	
	private void setVoucherData(ApiRetention apiRetention, Retenciones satRet, final Map<String, Object> parameters) {
		val tfd = apiRetention.getStampResponse().getTfd();
		
		parameters.put("voucher.certificate.emitter.number", apiRetention.getCertificateEmitterNumber());
    	parameters.put("voucher.certificate.sat.number", tfd.getNoCertificadoSAT());
    	parameters.put("voucher.exp.date", satRet.getFechaExp());
    	parameters.put("voucher.stamp.date", tfd.getFechaTimbrado());
    	parameters.put("voucher.uuid", tfd.getUUID());
    	parameters.put("voucher.qr", ReportManagerTools.getQR(satRet, apiRetention.getStampResponse()));
        parameters.put("voucher.emitter.seal", tfd.getSelloCFD());
        parameters.put("voucher.original.string", apiRetention.getStampResponse().getOriginalString());
        parameters.put("voucher.sat.seal", tfd.getSelloSAT());
        parameters.put("voucher.version", apiRetention.getVersion());
	}
	
	private void setRelatedDocumentData(ApiRetention apiRetention, final Map<String, Object> parameters) {
		if (apiRetention.getRelatedDocumentPlainValue() != null && apiRetention.getRelatedDocumentUuid() != null) {
			parameters.put("related.cfdi.type", apiRetention.getRelatedDocumentPlainValue());
			parameters.put("related.cfdi.uuid", apiRetention.getRelatedDocumentUuid());
		}
	}
	
	private void setComplementData(ApiRetention apiRetention, Complemento complement,
			final Map<String, Object> parameters) {
		String cpmVersion = null;
		val retComplement = determineRetComplement(complement);
		if (retComplement != null) {
			if (RetComplement.INTS.equals(retComplement.getRetComplement()) && retComplement.getInts() != null) {
				cpmVersion = populateComplementInts(apiRetention, retComplement.getInts(), parameters);
			} else if (RetComplement.OCD.equals(retComplement.getRetComplement()) && retComplement.getOcd() != null) {
				cpmVersion = populateComplementOcd(apiRetention, retComplement.getOcd(), parameters);
			} else if (RetComplement.DIV.equals(retComplement.getRetComplement()) && retComplement.getDiv() != null) {
				cpmVersion = populateComplementDiv(apiRetention, retComplement.getDiv(), parameters);
			} else if (RetComplement.ENA.equals(retComplement.getRetComplement()) && retComplement.getEna() != null) {
				cpmVersion = populateComplementEna(apiRetention, retComplement.getEna(), parameters);
			} else if (RetComplement.PEX.equals(retComplement.getRetComplement()) && retComplement.getPex() != null) {
				cpmVersion = populateComplementPex(apiRetention, retComplement.getPex(), parameters);
			} else if (RetComplement.SFI.equals(retComplement.getRetComplement()) && retComplement.getSfi() != null) {
				cpmVersion = populateComplementSfi(apiRetention, retComplement.getSfi(), parameters);
			}
		}
		parameters.put("voucher.cpm.version", cpmVersion);
	}
	
	private RetV2ComplementData determineRetComplement(Complemento complement) {
	    if (complement == null) {
	        return null;
	    }

	    val complementMap = new HashMap<Class<?>, Object>();
	    for (val comp : complement.getAny()) {
	        if (comp != null) {
	            complementMap.put(comp.getClass(), comp);
	        }
	    }

	    RetComplement retComplement = null;
	    val complementObjects = new ArrayList<Object>();
	    for (val entry : COMPLEMENT_TYPE_MAP.entrySet()) {
	        val complementClass = entry.getKey();

	        val complementObject = complementMap.get(complementClass);
	        if (complementObject != null) {
	            complementObjects.add(complementObject);
	            retComplement = entry.getValue();
	        }
	    }

	    return RetV2ComplementData
	    		.builder()
	            .retComplement(retComplement)
	            .div(getFromList(complementObjects, Dividendos.class))
	            .ena(getFromList(complementObjects, EnajenaciondeAcciones.class))
	            .ints(getFromList(complementObjects, Intereses.class))
	            .ocd(getFromList(complementObjects, OperacionesconDerivados.class))
	            .pex(getFromList(complementObjects, PagosaExtranjeros.class))
	            .sfi(getFromList(complementObjects, SectorFinanciero.class))
	            .build();
	}

	private static <T> T getFromList(List<?> list, Class<T> clazz) {
	    for (Object obj : list) {
	        if (clazz.isInstance(obj)) {
	            return clazz.cast(obj);
	        }
	    }
	    return null;
	}
	
	private String populateComplementInts(ApiRetention apiRetention, Intereses intereses,
			final Map<String, Object> parameters) {
		parameters.put("int", "complemento_intereses");
		parameters.put("int.finance.system", apiRetention.getFinanceSystem());
		parameters.put("int.retirement", apiRetention.getRetirement());
		parameters.put("int.financial.operations", apiRetention.getFinancialOperations());
		parameters.put("int.nominal.amount", MoneyUtils.format(intereses.getMontIntNominal()));
		parameters.put("int.actual.amount", MoneyUtils.format(intereses.getMontIntReal()));
		parameters.put("int.loss", MoneyUtils.format(intereses.getPerdida()));
		return intereses.getVersion();
	}
	
	private String populateComplementOcd(ApiRetention apiRetention, OperacionesconDerivados ocd,
			final Map<String, Object> parameters) {
		parameters.put("ocd", "complemento_operaciones_con_derivados");
		parameters.put("ocd.profits", MoneyUtils.format(apiRetention.getProfitAmount()));
		parameters.put("ocd.losses", MoneyUtils.format(apiRetention.getDeductibleWaste()));
		return ocd.getVersion();
	}
	
	private String populateComplementDiv(ApiRetention apiRetention, Dividendos div,
			final Map<String, Object> parameters) {
		val type = UCatalog.plainValue(retentionDataSource.dividendTypeSource().get(apiRetention.getCveTipDivOUtil()));
		
		parameters.put("div", "complemento_dividendos");
		parameters.put("div.cve.tip.div.outil", type);
		parameters.put("div.mont.isr.acred.ret.mexico", MoneyUtils.format(apiRetention.getMontISRAcredRetMexico()));
		parameters.put("div.mont.isr.acred.ret.extranjero", MoneyUtils.format(apiRetention.getMontISRAcredRetExtranjero()));
		parameters.put("div.mont.ret.ext.div.ext", MoneyUtils.format(apiRetention.getMontRetExtDivExt()));
		parameters.put("div.tipo.soc.distr.div", apiRetention.getTipoSocDistrDiv());
		parameters.put("div.mont.isr.acred.nal", MoneyUtils.format(apiRetention.getMontISRAcredNal()));
		parameters.put("div.mont.div.acum.nal", MoneyUtils.format(apiRetention.getMontDivAcumNal()));
		parameters.put("div.mont.div.acum.ext", MoneyUtils.format(apiRetention.getMontDivAcumExt()));
		parameters.put("div.proporcion.rem", MoneyUtils.format(apiRetention.getProporcionRem()));
		return div.getVersion();
	}

	private String populateComplementEna(ApiRetention apiRetention, EnajenaciondeAcciones ena,
			final Map<String, Object> parameters) {
		parameters.put("ena", "complemento_enajenacion_acciones");
		parameters.put("ena.contract", apiRetention.getContract());
		parameters.put("ena.profit", apiRetention.getProfit());
		parameters.put("ena.waste", apiRetention.getWaste());
		return ena.getVersion();
	}
	
	private String populateComplementPex(ApiRetention apiRetention, PagosaExtranjeros pex,
			final Map<String, Object> parameters) {
		parameters.put("pex.payment.concept", apiRetention.getPaymentConcept());
		parameters.put("pex.concept.description", apiRetention.getConceptDescription());
		if (apiRetention.isBeneficiary()) {
			parameters.put("pex.isb", "complemento_pagos_extranjeros");
			parameters.put("pex.isb.rfc", apiRetention.getBeneficiaryRfc());
    		parameters.put("pex.isb.business.name", apiRetention.getBeneficiaryBusinessName());
    		parameters.put("pex.isb.curp", apiRetention.getBeneficiaryCurp());
		} else {
			parameters.put("pex.nob", "complemento_pagos_extranjeros");
			parameters.put("pex.nob.country", apiRetention.getNoBeneficiaryCountry());
		}
		return pex.getVersion();
	}
	
	private String populateComplementSfi(ApiRetention apiRetention, SectorFinanciero sfi,
			final Map<String, Object> parameters) {
		parameters.put("sfi", "complemento_sector_financiero");
		parameters.put("sfi.fid.number", apiRetention.getFideicomId());
		parameters.put("sfi.fid.name", apiRetention.getFideicomName());
		parameters.put("sfi.fid.description", apiRetention.getFideicomDescription());
		return sfi.getVersion();
	}

}