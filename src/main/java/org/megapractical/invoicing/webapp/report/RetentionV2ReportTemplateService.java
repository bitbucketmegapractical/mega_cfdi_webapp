package org.megapractical.invoicing.webapp.report;

import java.util.Map;

public interface RetentionV2ReportTemplateService {
	
	void retentionV2Template(Map<String, Object> parameters);
	
}