package org.megapractical.invoicing.webapp.report.payload;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class PaymentV2Report {
	private String paymentDate;
	private String paymentWay;
	private String currency;
	private String changeType;
	private String amount;
	private String operationNumber;
	private String sourceAccountRfc;
	private String bankName;
	private String payerAccount;
	private String targetAccountRfc;
	private String receiverAccount;
	private String stringType;
	private String paymentCertificate;
	private String originalString;
	private String paymentSeal;
	private List<PaymentV2RelatedDocumentReport> relatedDocuments;
	private List<CfdiV4TaxReport> transferreds;
	private List<CfdiV4TaxReport> withhelds;
	private String paymentNumber;
	
	public static final PaymentV2Report convert(Payment item) {
		String emptyValue = "-";
		String pw = String.format("%s %s", item.getPaymentWayCode(), item.getPaymentWayValue());
		
		String currencyCode = item.getCurrencyEntity().getCodigo();
		String currencyValue = item.getCurrencyEntity().getValor();
		Integer decimal = item.getCurrencyEntity().getDecimales();
		String currency = String.format("%s %s", currencyCode, currencyValue);
		
		PaymentV2Report paymentReport = new PaymentV2Report();
		paymentReport.setPaymentNumber(item.getPaymentNumber());
		paymentReport.setPaymentDate(item.getPaymentDate());
		paymentReport.setPaymentWay(pw);
		paymentReport.setCurrency(currency);
		paymentReport.setAmount(UValue.bigDecimalString(item.getAmount(), decimal));
		
		String changeType = !UValidator.isNullOrEmpty(item.getChangeType())
				? UValue.bigDecimalString(item.getChangeType())
				: emptyValue;
		paymentReport.setChangeType(changeType);
		
		String paymentCertificate = !UValidator.isNullOrEmpty(item.getPaymentCertificate())
				? UValue.byteToString(item.getPaymentCertificate())
				: emptyValue;
		paymentReport.setPaymentCertificate(paymentCertificate);
		
		String paymentSeal = !UValidator.isNullOrEmpty(item.getPaymentSeal())
				? UValue.byteToString(item.getPaymentSeal())
				: emptyValue;
		paymentReport.setPaymentSeal(paymentSeal);
		
		String originalString = !UValidator.isNullOrEmpty(item.getOriginalString())
				? item.getOriginalString()
				: emptyValue;
		paymentReport.setOriginalString(originalString);
		
		String operationNumber = !UValidator.isNullOrEmpty(item.getOperationNumber())
				? item.getOperationNumber()
				: emptyValue;
		paymentReport.setOperationNumber(operationNumber);
		
		String sourceAccountRfc = !UValidator.isNullOrEmpty(item.getSourceAccountRfc())
				? item.getSourceAccountRfc()
				: emptyValue;
		paymentReport.setSourceAccountRfc(sourceAccountRfc);
		
		String targetAccountRfc = !UValidator.isNullOrEmpty(item.getTargetAccountRfc())
				? item.getTargetAccountRfc()
				: emptyValue;
		paymentReport.setTargetAccountRfc(targetAccountRfc);
		
		String bankName = !UValidator.isNullOrEmpty(item.getBankName()) ? item.getBankName() : emptyValue;
		paymentReport.setBankName(bankName);
		
		String payerAccount = !UValidator.isNullOrEmpty(item.getPayerAccount()) 
				? item.getPayerAccount()
				: emptyValue;
		paymentReport.setPayerAccount(payerAccount);
		
		String receiverAccount = !UValidator.isNullOrEmpty(item.getReceiverAccount())
				? item.getReceiverAccount()
				: emptyValue;
		paymentReport.setReceiverAccount(receiverAccount);
		
		String stringType = !UValidator.isNullOrEmpty(item.getStringType()) 
				? item.getStringType()
				: emptyValue;
		paymentReport.setStringType(stringType);
		
		List<CfdiV4TaxReport> transferreds = new ArrayList<>();
		item.getTransferreds().forEach(transferred -> {
			CfdiV4TaxReport taxReport = new CfdiV4TaxReport(transferred, decimal);
			transferreds.add(taxReport);
		});
		paymentReport.setTransferreds(transferreds);
		
		List<CfdiV4TaxReport> withhelds = new ArrayList<>();
		item.getWithhelds().forEach(withheld -> {
			CfdiV4TaxReport taxReport = new CfdiV4TaxReport(withheld, decimal);
			withhelds.add(taxReport);
		});
		paymentReport.setWithhelds(withhelds);
		
		List<PaymentV2RelatedDocumentReport> relatedDocuments = item.getRelatedDocuments().stream()
																						  .map(PaymentV2RelatedDocumentReport::convert)
																						  .collect(toList());
		paymentReport.setRelatedDocuments(relatedDocuments);
		
		return paymentReport;
	}

	/*Getters and Setters*/
	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentWay() {
		return paymentWay;
	}

	public void setPaymentWay(String paymentWay) {
		this.paymentWay = paymentWay;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}

	public String getSourceAccountRfc() {
		return sourceAccountRfc;
	}

	public void setSourceAccountRfc(String sourceAccountRfc) {
		this.sourceAccountRfc = sourceAccountRfc;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getPayerAccount() {
		return payerAccount;
	}

	public void setPayerAccount(String payerAccount) {
		this.payerAccount = payerAccount;
	}

	public String getTargetAccountRfc() {
		return targetAccountRfc;
	}

	public void setTargetAccountRfc(String targetAccountRfc) {
		this.targetAccountRfc = targetAccountRfc;
	}

	public String getReceiverAccount() {
		return receiverAccount;
	}

	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	public String getStringType() {
		return stringType;
	}

	public void setStringType(String stringType) {
		this.stringType = stringType;
	}

	public String getPaymentCertificate() {
		return paymentCertificate;
	}

	public void setPaymentCertificate(String paymentCertificate) {
		this.paymentCertificate = paymentCertificate;
	}

	public String getOriginalString() {
		return originalString;
	}

	public void setOriginalString(String originalString) {
		this.originalString = originalString;
	}

	public String getPaymentSeal() {
		return paymentSeal;
	}

	public void setPaymentSeal(String paymentSeal) {
		this.paymentSeal = paymentSeal;
	}

	public List<PaymentV2RelatedDocumentReport> getRelatedDocuments() {
		if (relatedDocuments == null) {
			relatedDocuments = new ArrayList<>();
		}
		return relatedDocuments;
	}

	public void setRelatedDocuments(List<PaymentV2RelatedDocumentReport> relatedDocuments) {
		this.relatedDocuments = relatedDocuments;
	}
	
	public Stream<PaymentV2RelatedDocumentReport> getDocuments() {
		return (relatedDocuments == null) ? Stream.empty() : relatedDocuments.stream();
	}

	public List<CfdiV4TaxReport> getTransferreds() {
		if (transferreds == null) {
			transferreds = new ArrayList<>();
		}
		return transferreds;
	}

	public void setTransferreds(List<CfdiV4TaxReport> transferreds) {
		this.transferreds = transferreds;
	}
	
	public Stream<CfdiV4TaxReport> getTaxesTransferred() {
		return (transferreds == null) ? Stream.empty() : transferreds.stream();
	}

	public List<CfdiV4TaxReport> getWithhelds() {
		if (withhelds == null) {
			withhelds = new ArrayList<>();
		}
		return withhelds;
	}

	public void setWithhelds(List<CfdiV4TaxReport> withhelds) {
		this.withhelds = withhelds;
	}
	
	public Stream<CfdiV4TaxReport> getTaxesWithhelds() {
		return (withhelds == null) ? Stream.empty() : withhelds.stream();
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	
}