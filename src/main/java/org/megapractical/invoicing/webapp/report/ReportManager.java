package org.megapractical.invoicing.webapp.report;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.megapractical.invoicing.api.report.ReportManagerTools;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction.PayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment.CompensationCreditBalances;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment.EmploymentSubsidy;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPensionRetirement;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception.PayrollActionTitle;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception.PayrollExtraHour;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollSeparationCompensation;
import org.megapractical.invoicing.api.wrapper.ApiPayrollVoucher;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@Service
public class ReportManager {

	@Autowired
	private AppSettings appSettings;
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private CfdiV4ReportService cfdiV4ReportService;
	
	@Autowired
	private RetentionV2ReportService retentionV2ReportService;
	
	private static final String PAYROLL_TEMPLATE_PATH = "/report/template/payroll/1.2/";
	
	private static final String TEMPLATE_PARAM = "template";
	private static final String NULL_VALUE = null;
	
	public boolean cfdiV4GeneratePdf(ApiCfdi cfdi, String pdfPath) {
		userTools.log("[INFO] CFDI V4 - GENERATING PDF...");
		
		val cfdiV4Report = cfdiV4ReportService.populateData(cfdi);
		val parameters = cfdiV4Report.getParameters();
		val template = (InputStream) parameters.get(TEMPLATE_PARAM);
		
		val exported = exportReportToPdf(template, parameters, pdfPath);
		if (exported) {
			userTools.log("[INFO] CFDI V4 - PDF HAS BEEN GENERATED SUCCESSFULLY");
		}
		return exported;
	}
	
	public boolean cfdiV4PaymentV2GeneratePdf(ApiCfdi cfdi, String pdfPath) {
		userTools.log("[INFO] CFDI V4 PAYMENT V2 - GENERATING PDF...");
		val cfdiV4Report = cfdiV4ReportService.paymentPopulateData(cfdi);
		val parameters = cfdiV4Report.getParameters();
		val template = (InputStream) parameters.get(TEMPLATE_PARAM);
		
		val exported = exportReportToPdf(template, parameters, pdfPath);
		if (exported) {
			userTools.log("[INFO] CFDI V4 PAYMENT V2 - PDF HAS BEEN GENERATED SUCCESSFULLY");
		}
		return exported;
	}
	
	public boolean retV2GeneratePdf(ApiRetention apiRetention, String pdfPath) {
		userTools.log("[INFO] RETENTION V2 - GENERATING PDF...");
		
		val retV2Report = retentionV2ReportService.populateData(apiRetention);
		val parameters = retV2Report.getParameters();
		val template = (InputStream) parameters.get(TEMPLATE_PARAM);
		
		val exported = exportReportToPdf(template, parameters, pdfPath);
		if (exported) {
			userTools.log("[INFO] RETENTION V2 - PDF HAS BEEN GENERATED SUCCESSFULLY");
		}
		return exported;
	}
	
	public boolean exportReportToPdf(InputStream template, Map<String, Object> parameters, String pdfPath) {
		try {

			val report = (JasperReport) JRLoader.loadObject(template);
			// JRDataSource dataSource = new JRBeanCollectionDataSource(cfdi.getConcepts());

			// Cargando listado Reporte principal / Subreport.
			val jp = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());

			// Generando reporte.
			JasperExportManager.exportReportToPdfFile(jp, pdfPath);
			return true;

		} catch (JRException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean generatePDFPayroll(ApiPayrollVoucher payrollVoucher, String pdfPath){
		try {
			
			userTools.log("[INFO] GENERATING PDF...");
			
			ApiCfdi cfdi = payrollVoucher.getCfdi();
			ApiPayroll payroll = payrollVoucher.getPayroll();						
			
			// Obteniendo comprobante
			Comprobante voucher = payrollVoucher.getCfdi().getStampResponse().getVoucher();
			
			//##### Plantilla principal del reporte
            InputStream is = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll.jasper");
			
			// Inicializando parametros			
            Map<String, Object> parameters = new HashMap<>();
            
            cfdi.getEmitter().setNombreRazonSocial(UValue.revertSingleReplace(cfdi.getEmitter().getNombreRazonSocial()));
            cfdi.getReceiver().setNombreRazonSocial(UValue.revertSingleReplace(cfdi.getReceiver().getNombreRazonSocial()));
            cfdi.getVoucher().setPaymentConditions(UValue.revertSingleReplace(cfdi.getVoucher().getPaymentConditions()));
                        
            //##### Cargando informacion del emisor
            parameters.put("emitter.name", cfdi.getEmitter().getNombreRazonSocial());
    		parameters.put("emitter.rfc", cfdi.getEmitter().getRfc());
    		parameters.put("emitter.regime", cfdi.getEmitter().getRegimenFiscal().getCodigo() + " " + cfdi.getEmitter().getRegimenFiscal().getValor());
    		
    		
    		//##### Logo
            //InputStream logo = ReportManager.class.getResourceAsStream(DEFAULT_PATH_LOGO);
            InputStream logo = null;
            String emitterLogo = userTools.getContribuyenteActive(cfdi.getEmitter().getRfc()).getLogo();
            File logoFile = null; 
            try {
            	
            	if(!UValidator.isNullOrEmpty(emitterLogo)){
            		String logoPath = appSettings.getPropertyValue("cfdi.logo.path");
            		emitterLogo = logoPath + File.separator + emitterLogo;
            		
	            	logoFile = new File(emitterLogo);
	            	logo = UFile.fileToInputStream(logoFile);		            	
	            }
            	parameters.put("logo", logo);	            	

			} catch (Exception e) {}
    		
    		
    		if(!UValidator.isNullOrEmpty(payroll.getPayrollEmitter())){
    			if(!UValidator.isNullOrEmpty(payroll.getPayrollEmitter().getCurp())){
    				parameters.put("emitter.curp", payroll.getPayrollEmitter().getCurp());
    			}else{
    				parameters.put("emitter.curp", NULL_VALUE);
    			}
    			if(!UValidator.isNullOrEmpty(payroll.getPayrollEmitter().getEmployerRegistration())){
    				parameters.put("emitter.employer.registration", payroll.getPayrollEmitter().getEmployerRegistration());
    			}else{
    				parameters.put("emitter.employer.registration", NULL_VALUE);
    			}
    			if(!UValidator.isNullOrEmpty(payroll.getPayrollEmitter().getRfcPatronOrigin())){
    				parameters.put("emitter.rfc.patron.origin", payroll.getPayrollEmitter().getRfcPatronOrigin());
    			}else{
    				parameters.put("emitter.rfc.patron.origin", NULL_VALUE);
    			}
    			if(!UValidator.isNullOrEmpty(payroll.getPayrollEmitter().getResourceOriginCode()) && !UValidator.isNullOrEmpty(payroll.getPayrollEmitter().getResourceOriginValue())){
    				parameters.put("emitter.resource.origin", payroll.getPayrollEmitter().getResourceOriginCode() + " " + payroll.getPayrollEmitter().getResourceOriginValue());
    			}else{
    				parameters.put("emitter.resource.origin", NULL_VALUE);
    			}
    			if(!UValidator.isNullOrEmpty(payroll.getPayrollEmitter().getOwnResourceAmount())){
    				parameters.put("emitter.own.resource.amount", payroll.getPayrollEmitter().getOwnResourceAmount());
    			}else{
    				parameters.put("emitter.own.resource.amount", NULL_VALUE);
    			}	
    		}
    		//TODO Revisar luego en base la nomina de salarios si sera requerido
    		//checar luego ya q deberia funcionar con alguno de los de arriba no orgullosos de este cambio pero debe funcionar  por la urgencia

            //##### Cargando informacion del receptor
            parameters.put("receiver.name", cfdi.getReceiver().getNombreRazonSocial());
    		parameters.put("receiver.rfc", cfdi.getReceiver().getRfc());
    		parameters.put("receiver.curp", payroll.getPayrollReceiver().getCurp());
    		parameters.put("receiver.employee.number", payroll.getPayrollReceiver().getEmployeeNumber());
    		parameters.put("receiver.social.security.number", payroll.getPayrollReceiver().getSocialSecurityNumber());
    		parameters.put("receiver.start.date.employment.relationship", payroll.getPayrollReceiver().getStartDateEmploymentRelationshipString());
    		parameters.put("receiver.age", payroll.getPayrollReceiver().getAge());
    		
    		if(!UValidator.isNullOrEmpty(payroll.getPayrollReceiver().getDayType())){
    			parameters.put("receiver.day.type", payroll.getPayrollReceiver().getDayTypeCode() + " " + payroll.getPayrollReceiver().getDayTypeValue());
    		}else{
    			parameters.put("receiver.day.type", NULL_VALUE);
    		}
    		
    		parameters.put("receiver.unionized", payroll.getPayrollReceiver().getUnionized());
    		parameters.put("receiver.regime.type", payroll.getPayrollReceiver().getRegimeTypeCode() + " " + payroll.getPayrollReceiver().getRegimeTypeValue());
    		parameters.put("receiver.contract.type", payroll.getPayrollReceiver().getContractTypeCode() + " " + payroll.getPayrollReceiver().getContractTypeValue());
    		parameters.put("receiver.bank.account", payroll.getPayrollReceiver().getBankAccount());
    		
    		if(!UValidator.isNullOrEmpty(payroll.getPayrollReceiver().getBank())){
    			parameters.put("receiver.bank", payroll.getPayrollReceiver().getBankCode() + " " + payroll.getPayrollReceiver().getBankValue());
    		}else{
    			parameters.put("receiver.bank", NULL_VALUE);
    		}
    		
    		parameters.put("receiver.integrated.daily.salary", payroll.getPayrollReceiver().getIntegratedDailySalary());
    		parameters.put("receiver.base.salary", payroll.getPayrollReceiver().getShareContributionBaseSalary());
    		parameters.put("receiver.department", payroll.getPayrollReceiver().getDepartment());
    		parameters.put("receiver.job.title", payroll.getPayrollReceiver().getJobTitle());
    		
    		if(!UValidator.isNullOrEmpty(payroll.getPayrollReceiver().getJobRisk())){
    			parameters.put("receiver.job.risk", payroll.getPayrollReceiver().getJobRiskCode() + " " + payroll.getPayrollReceiver().getJobRiskValue());
    		}else{
    			parameters.put("receiver.job.risk", NULL_VALUE);
    		}
    		
    		parameters.put("receiver.federal.entity", payroll.getPayrollReceiver().getFederalEntityCode() + " " + payroll.getPayrollReceiver().getFederalEntityValue());
    		parameters.put("receiver.payment.frequency", payroll.getPayrollReceiver().getPaymentFrequencyCode() + " " + payroll.getPayrollReceiver().getPaymentFrequencyValue());
    		parameters.put("receiver.cfdi.use", cfdi.getReceiver().getUsoCFDI().getCodigo() + " " + cfdi.getReceiver().getUsoCFDI().getValor());
    		parameters.put("receiver.postal.code", cfdi.getReceiver().getDomicilioFiscal());
    		
    		//##### Cargando informacion de las subcontrataciones
    		if(!payroll.getPayrollReceiver().getPayrollOutsourcings().isEmpty()){
    			parameters.put("receiver.outsourcings", payroll.getPayrollReceiver().getPayrollOutsourcings());
    			
    			//##### Plantilla subreport subcontrataciones
        		InputStream subreportOutsourcings = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-outsourcings.jasper");
            	parameters.put("payroll.subreport.outsourcings", subreportOutsourcings);
    		}
            
    		//##### Cargando informacion de la nomina
    		parameters.put("payroll.payroll.type", payroll.getPayrollTypeCode() + " " + payroll.getPayrollTypeValue());
    		parameters.put("payroll.payment.date", payroll.getPaymentDateString());
    		parameters.put("payroll.payment.intial.date", payroll.getPaymentIntialDateString());
    		parameters.put("payroll.payment.end.date", payroll.getPaymentEndDateString());
    		parameters.put("payroll.number.days.paid", payroll.getNumberDaysPaid());
    		parameters.put("payroll.total.perceptions", payroll.getTotalPerceptions());
    		parameters.put("payroll.total.deductions", payroll.getTotalDeductions());
    		parameters.put("payroll.total.other.payments", payroll.getTotalOtherPayments());
    		
    		//##### Cargando informacion del comprobante
    		parameters.put("voucher.voucher.type", cfdi.getVoucherType().getCodigo() + " " + cfdi.getVoucherType().getValor());
    		parameters.put("voucher.tax.sheet", cfdi.getVoucher().getTaxSheet());
    		parameters.put("voucher.currency", cfdi.getCurrency().getCodigo() + " " + cfdi.getCurrency().getValor());
    		parameters.put("voucher.date.stamp", cfdi.getStampResponse().getDateStamp());
    		parameters.put("voucher.serie", cfdi.getVoucher().getSerie());
    		parameters.put("voucher.sheet", cfdi.getVoucher().getFolio());
    		parameters.put("voucher.certificate.emitter.number", cfdi.getStampResponse().getCertificateEmitterNumber());
    		parameters.put("voucher.certificate.sat.number", cfdi.getStampResponse().getCertificateSatNumber());
    		parameters.put("voucher.payment.method", cfdi.getPaymentMethod().getCodigo() + " " + cfdi.getPaymentMethod().getValor());
    		parameters.put("voucher.expedition.place", cfdi.getPostalCode().getValor());
    		parameters.put("voucher.subtotal", cfdi.getVoucher().getSubTotalString());
    		parameters.put("voucher.discount", cfdi.getVoucher().getDiscountString());
    		parameters.put("voucher.total", cfdi.getVoucher().getTotalString());
    		parameters.put("voucher.total.letters", ReportManagerTools.totalInLetter(voucher.getTotal(), voucher.getMoneda().value()));
    		parameters.put("voucher.qr", ReportManagerTools.getQR(cfdi.getStampResponse()));
    		parameters.put("voucher.emitter.seal", cfdi.getStampResponse().getEmitterSeal());
    		parameters.put("voucher.sat.seal", cfdi.getStampResponse().getSatSeal());
    		parameters.put("voucher.original.string", cfdi.getStampResponse().getOriginalString());
    		
            //##### Cargando el concepto
            String conceptUnitValue = null;
            String conceptAmount = null;
            String conceptDiscount = null;
            for (ApiConcept item : cfdi.getConcepts()) {
				item.setDescription(UValue.revertSingleReplace(item.getDescription()));
				conceptUnitValue = item.getUnitValueString();
				conceptAmount = item.getAmountString();
				conceptDiscount = item.getDiscountString();
			}
            parameters.put("concept.unit.value", conceptUnitValue);
            parameters.put("concept.amount", conceptAmount);
            parameters.put("concept.discount", conceptDiscount);
            
            //##### Cargando las percepciones
            ApiPayrollPerception apiPayrollPerception = payroll.getPayrollPerception();
            if(!UValidator.isNullOrEmpty(apiPayrollPerception)){
            	parameters.put("perception.perceptions", apiPayrollPerception.getPayrollPerceptions());
        		parameters.put("perception.total.taxed", apiPayrollPerception.getTotalTaxed());
        		parameters.put("perception.total.exempt", apiPayrollPerception.getTotalExempt());
        		parameters.put("perception.total.salaries", apiPayrollPerception.getTotalSalaries());
        		parameters.put("perception.total.separation.compensation", apiPayrollPerception.getTotalSeparationCompensation());
        		parameters.put("perception.total.pension.retirement", apiPayrollPerception.getTotalPensionRetirement());
        		
        		//##### Plantilla subreport percepciones
        		InputStream subreportPerceptions = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-percepctions.jasper");
            	parameters.put("payroll.subreport.perceptions", subreportPerceptions);
            	
            	List<PayrollActionTitle> payrollActionTitles = new ArrayList<>();
            	List<PayrollExtraHour> payrollExtraHours = new ArrayList<>();
            	
            	for (PayrollPerception item : apiPayrollPerception.getPayrollPerceptions()) {
                	if(!UValidator.isNullOrEmpty(item.getPayrollActionTitle())){
                		payrollActionTitles.add(item.getPayrollActionTitle());
                	}
                	
                	if(!item.getPayrollExtraHours().isEmpty()){
                		for (PayrollExtraHour extraHour : payrollExtraHours) {
                			payrollExtraHours.add(extraHour);
						}
                	}
				}
            	
            	//##### Cargando acciones o titulos
            	if(!payrollActionTitles.isEmpty()){
            		parameters.put("perception.action.titles", payrollActionTitles);
            		
            		//##### Plantilla subreport acciones o titulos
            		InputStream subreportActionTitles = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-action-titles.jasper");
                	parameters.put("payroll.subreport.action.titles", subreportActionTitles);
            	}
            	
            	//##### Cargando horas extra
            	if(!payrollExtraHours.isEmpty()){
            		parameters.put("perception.extra.hours", payrollExtraHours);
            		
            		//##### Plantilla subreport horas extra
            		InputStream subreportExtraHours = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-extra-hours.jasper");
                	parameters.put("payroll.subreport.extra.hours", subreportExtraHours);
            	}
            	
            	//##### Cargando jubilacion pension retiro
            	if(!UValidator.isNullOrEmpty(apiPayrollPerception.getPayrollPensionRetirement())){
            		PayrollPensionRetirement payrollPensionRetirement = apiPayrollPerception.getPayrollPensionRetirement(); 
            		parameters.put("payroll.pension.retirement.total.exhibition", payrollPensionRetirement.getTotalExhibition());
            		parameters.put("payroll.pension.retirement.total.partiality", payrollPensionRetirement.getTotalPartiality());
            		parameters.put("payroll.pension.retirement.daily.amount", payrollPensionRetirement.getDailyAmount());
            		parameters.put("payroll.pension.retirement.income.accumulate", payrollPensionRetirement.getIncomeAccumulate());
            		parameters.put("payroll.pension.retirement.income.no.accumulate", payrollPensionRetirement.getIncomeNoAccumulate());
            	}
            	
            	//##### Cargando separacion indemnizacion
            	if(!UValidator.isNullOrEmpty(apiPayrollPerception.getPayrollSeparationCompensation())){
            		PayrollSeparationCompensation payrollSeparationCompensation = apiPayrollPerception.getPayrollSeparationCompensation();
            		parameters.put("payroll.separation.compensation.total.paid", payrollSeparationCompensation.getTotalPaid());
            		parameters.put("payroll.separation.compensation.exercises.number.service", payrollSeparationCompensation.getExercisesNumberService());
            		parameters.put("payroll.separation.compensation.last.salary", payrollSeparationCompensation.getLastSalary());
            		parameters.put("payroll.separation.compensation.income.accumulate", payrollSeparationCompensation.getIncomeAccumulate());
            		parameters.put("payroll.separation.compensation.income.no.accumulate", payrollSeparationCompensation.getIncomeNoAccumulate());
            	}
            }
    		
    		//##### Cargando las deducciones
            ApiPayrollDeduction apiPayrollDeduction = payroll.getPayrollDeduction();
            if(!UValidator.isNullOrEmpty(apiPayrollDeduction)){
                parameters.put("deduction.deductions", apiPayrollDeduction.getPayrollDeductions());
                parameters.put("deduction.total.other.deductions", apiPayrollDeduction.getTotalOtherDeductions());
        		parameters.put("deduction.total.taxes.withheld", apiPayrollDeduction.getTotalTaxesWithheld());
        		
        		BigDecimal totalDeductions = UValue.bigDecimal(NULL_VALUE);
        		for (PayrollDeduction item : apiPayrollDeduction.getPayrollDeductions()) {
        			totalDeductions = totalDeductions.add(UValue.bigDecimal(item.getAmount()));
				}
        		parameters.put("deduction.total.deductions", UValue.bigDecimalString(UValue.bigDecimalString(totalDeductions)));
        		
        		//##### Plantilla subreport deducciones
        		InputStream subreportDeductions = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-deductions.jasper");
            	parameters.put("payroll.subreport.deductions", subreportDeductions);
            }
            
            //##### Cargando otros pagos
    		List<ApiPayrollOtherPayment> payrollOtherPayments = payroll.getPayrollOtherPayments();
    		if(!payrollOtherPayments.isEmpty()){
    			List<EmploymentSubsidy> employmentSubsidies = new ArrayList<>();
        		List<CompensationCreditBalances> compensationCreditBalances = new ArrayList<>();
        		
    			for (ApiPayrollOtherPayment item : payrollOtherPayments) {
    				//##### Subsidio al empleado
    				EmploymentSubsidy employmentSubsidy = item.getEmploymentSubsidy();
    				if(!UValidator.isNullOrEmpty(employmentSubsidy)){
    					employmentSubsidies.add(employmentSubsidy);
    				}
    				
    				//##### Compensacion saldos a favor
    				CompensationCreditBalances compensationCreditBalance = item.getCompensationCreditBalance();
    				if(!UValidator.isNullOrEmpty(compensationCreditBalance)){
    					compensationCreditBalances.add(compensationCreditBalance);
    				}
				}
    			
    			parameters.put("other.payment.payments", payrollOtherPayments);
    			
    			if(!employmentSubsidies.isEmpty()){
    				parameters.put("other.payment.employment.subsidies", employmentSubsidies);
    				
    				//##### Plantilla subreport subcidio causado
        			InputStream subreportEmploymentSubsidy = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-other-payments-employment-subsidy.jasper");
                	parameters.put("payroll.subreport.employment.subsidy", subreportEmploymentSubsidy);    				
    			}
    			
    			if(!compensationCreditBalances.isEmpty()){
    				parameters.put("other.payment.compensation.creditBalances", compensationCreditBalances);
    				
    				//##### Plantilla subreport compensacion
        			InputStream subreportCompensationCreditBalance = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-other-payments-compensation-credit-balances.jasper");
                	parameters.put("payroll.subreport.compensation.credit.balance", subreportCompensationCreditBalance); 
    			}
    			
    			//##### Plantilla subreport otros pagos
    			InputStream subreportOtherPayments = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-other-payments.jasper");
            	parameters.put("payroll.subreport.other.payments", subreportOtherPayments);
    		}
            
    		//##### Cargando incapacidades
    		if(!payroll.getPayrollInabilities().isEmpty()){
    			parameters.put("inability.inabilities", payroll.getPayrollInabilities());
    			
    			//##### Plantilla subreport incapacidades
    			InputStream subreportOtherPayments = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-inabilities.jasper");
            	parameters.put("payroll.subreport.inabilities", subreportOtherPayments);
    		}
    		
    		//##### Cargando cfdi relacionados
            if(cfdi.getVoucher().getRelatedCfdi() != null){
            	parameters.put("related.cfdi.uuids", cfdi.getVoucher().getRelatedCfdi().getUuids());
            	parameters.put("related.cfdi.relationship.type.code", cfdi.getVoucher().getRelatedCfdi().getRelationshipTypeCode());
            	parameters.put("related.cfdi.relationship.type.value", cfdi.getVoucher().getRelatedCfdi().getRelationshipTypeValue());
            	
            	//##### Plantilla subreport cfdi relacionados
    			InputStream subreportOtherPayments = ReportManager.class.getResourceAsStream(PAYROLL_TEMPLATE_PATH + "cfdi-payroll-subreport-related-cfdi.jasper");
            	parameters.put("payroll.subreport.related.cfdi", subreportOtherPayments);
            }
            
            //##### Cargando informacion auxiliar
            if(!UValidator.isNullOrEmpty(cfdi.getLegend())){
            	parameters.put("legend", cfdi.getLegend());
            }
            
            exportReportToPdf(is, parameters, pdfPath);
           
            userTools.log("MEGA-CFDI API > [INFO] PDF GENERATED SUCCESSFULY");
            
            return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
    
}