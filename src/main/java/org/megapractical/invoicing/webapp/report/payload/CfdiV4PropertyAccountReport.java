package org.megapractical.invoicing.webapp.report.payload;

import org.megapractical.invoicing.api.wrapper.ApiConcept.ConceptPropertyAccount;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class CfdiV4PropertyAccountReport {
	private String conceptNumber;
	private String number;
	
	public CfdiV4PropertyAccountReport(ConceptPropertyAccount item) {
		this.conceptNumber = item.getConceptNumber();
		this.number = item.getNumber();
	}

	/*Getters and Setters*/
	public String getConceptNumber() {
		return conceptNumber;
	}

	public void setConceptNumber(String conceptNumber) {
		this.conceptNumber = conceptNumber;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
}