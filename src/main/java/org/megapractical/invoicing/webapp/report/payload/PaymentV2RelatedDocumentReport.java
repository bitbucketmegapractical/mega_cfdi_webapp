package org.megapractical.invoicing.webapp.report.payload;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.RelatedDocument;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class PaymentV2RelatedDocumentReport {
	private String documentId;
	private String serie;
	private String sheet;
	private String currency;
	private String changeType;
	private String partiality;
	private String amountPartialityBefore;
	private String amountPaid;
	private String difference;
	private String taxObject;
	private List<CfdiV4TaxReport> withhelds;
	private List<CfdiV4TaxReport> transferreds;
	private String paymentNumber;
	private String relatedDocumentNumber;

	public static PaymentV2RelatedDocumentReport convert(RelatedDocument item) {
		String emptyValue = "-";
		
		PaymentV2RelatedDocumentReport rdReport = new PaymentV2RelatedDocumentReport();
		rdReport.setPaymentNumber(item.getPaymentNumber());
		rdReport.setRelatedDocumentNumber(item.getRelatedDocumentNumber());
		rdReport.setDocumentId(item.getDocumentId());
		
		String currencyCode = item.getCurrencyEntity().getCodigo();
		String currencyValue = item.getCurrencyEntity().getValor();
		Integer decimal = item.getCurrencyEntity().getDecimales();
		rdReport.setCurrency(String.format("%s %s", currencyCode, currencyValue));
		
		String serieVal = !UValidator.isNullOrEmpty(item.getSerie()) ? item.getSerie() : emptyValue;
		rdReport.setSerie(serieVal);

		String partialityVal = !UValidator.isNullOrEmpty(item.getPartiality())
				? UValue.integerString(item.getPartiality())
				: emptyValue;
		rdReport.setPartiality(partialityVal);
		
		String amountPartialityBeforeVal = !UValidator.isNullOrEmpty(item.getAmountPartialityBefore())
				? UValue.bigDecimalString(item.getAmountPartialityBefore(), decimal)
				: emptyValue;
		rdReport.setAmountPartialityBefore(amountPartialityBeforeVal);
		
		String amountPaidVal = !UValidator.isNullOrEmpty(item.getAmountPaid())
				? UValue.bigDecimalString(item.getAmountPaid(), decimal)
				: emptyValue;
		rdReport.setAmountPaid(amountPaidVal);
		
		String differenceVal = !UValidator.isNullOrEmpty(item.getDifference())
				? UValue.bigDecimalString(item.getDifference(), decimal)
				: emptyValue;
		rdReport.setDifference(differenceVal);
		
		String documentRelatedChangeType = !UValidator.isNullOrEmpty(item.getChangeType())
				? UValue.bigDecimalString(item.getChangeType())
				: emptyValue;
		rdReport.setChangeType(documentRelatedChangeType);
		
		String sheetVal = !UValidator.isNullOrEmpty(item.getSheet()) ? item.getSheet() : emptyValue;
		rdReport.setSheet(sheetVal);
		rdReport.setTaxObject(item.getTaxObject().replace("(", "").replace(")", ""));
		
		List<CfdiV4TaxReport> taxTransferreds = new ArrayList<>();
		item.getTransferreds().forEach(transferred -> {
			CfdiV4TaxReport taxReport = new CfdiV4TaxReport(transferred, decimal);
			taxTransferreds.add(taxReport);
		});
		rdReport.setTransferreds(taxTransferreds);
		
		List<CfdiV4TaxReport> taxWithhelds = new ArrayList<>();
		item.getWithhelds().forEach(withheld -> {
			CfdiV4TaxReport taxReport = new CfdiV4TaxReport(withheld, decimal);
			taxWithhelds.add(taxReport);
		});
		rdReport.setWithhelds(taxWithhelds);
		
		return rdReport;
	}
	
	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getSheet() {
		return sheet;
	}

	public void setSheet(String sheet) {
		this.sheet = sheet;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public String getPartiality() {
		return partiality;
	}

	public void setPartiality(String partiality) {
		this.partiality = partiality;
	}

	public String getAmountPartialityBefore() {
		return amountPartialityBefore;
	}

	public void setAmountPartialityBefore(String amountPartialityBefore) {
		this.amountPartialityBefore = amountPartialityBefore;
	}

	public String getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(String amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getDifference() {
		return difference;
	}

	public void setDifference(String difference) {
		this.difference = difference;
	}

	public String getTaxObject() {
		return taxObject;
	}

	public void setTaxObject(String taxObject) {
		this.taxObject = taxObject;
	}

	public List<CfdiV4TaxReport> getWithhelds() {
		if (withhelds == null) {
			withhelds = new ArrayList<>();
		}
		return withhelds;
	}

	public void setWithhelds(List<CfdiV4TaxReport> withhelds) {
		this.withhelds = withhelds;
	}
	
	public Stream<CfdiV4TaxReport> getTaxesWithhelds() {
		return (withhelds == null) ? Stream.empty() : withhelds.stream();
	}

	public List<CfdiV4TaxReport> getTransferreds() {
		if (transferreds == null) {
			transferreds = new ArrayList<>();
		}
		return transferreds;
	}

	public void setTransferreds(List<CfdiV4TaxReport> transferreds) {
		this.transferreds = transferreds;
	}
	
	public Stream<CfdiV4TaxReport> getTaxesTransferred() {
		return (transferreds == null) ? Stream.empty() : transferreds.stream();
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public String getRelatedDocumentNumber() {
		return relatedDocumentNumber;
	}

	public void setRelatedDocumentNumber(String relatedDocumentNumber) {
		this.relatedDocumentNumber = relatedDocumentNumber;
	}
	
}