package org.megapractical.invoicing.webapp.report;

import java.util.Map;

import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class CfdiV4ReportTemplateServiceImpl implements CfdiV4ReportTemplateService {

	private static final String INVOICE_TEMPLATE_PATH = "/report/template/invoice/cfdi/4.0/";
	private static final String COMPLEMENT_PAYMENT_TEMPLATE_PATH = "/report/template/invoice/complements/payment/2.0/";

	@Override
	public void cfdiV4Template(Map<String, Object> parameters) {
		setCfdiV4Template(parameters);
		setConceptsJasper(parameters);
		setCustomsInformationJasper(parameters);
		setPropertyAccountJapser(parameters);
		setTransferredJasper(parameters);
		setWithheldJasper(parameters);
		setRelatedCfdiJasper(parameters);
	}
	
	@Override
	public void cfdiV4PaymentTemplate(Map<String, Object> parameters) {
		setCfdiV4PaymentTemplate(parameters);
		setPaymentsJasper(parameters);
		setRelatedDocumentsJasper(parameters);
		setPaymentTransferredJasper(parameters);
		setPaymentWithheldJasper(parameters);
		setPaymentRelatedDocumentsTransferredJasper(parameters);
		setPaymentRelatedDocumentsWithheldJasper(parameters);
		setRelatedCfdiJasper(parameters);
	}
	
	/**
	 * Plantilla principal CFDI V4
	 * @param parameters
	 */
	public void setCfdiV4Template(Map<String, Object> parameters) {
		val templateJasper = "cfdi-invoice.jasper";
		val templatePath = String.format("%s%s", INVOICE_TEMPLATE_PATH, templateJasper);
		val template = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(templatePath);
		parameters.put("template", template);
	}

	/**
	 * Plantilla subreport conceptos
	 * 
	 * @param parameters
	 */
	private void setConceptsJasper(Map<String, Object> parameters) {
		val conceptsJasper = "cfdi-invoice-subreport-concepts.jasper";
		val subreportConceptsPath = String.format("%s%s", INVOICE_TEMPLATE_PATH, conceptsJasper);
		val subreportConcepts = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(subreportConceptsPath);
		parameters.put("subreport.concept", subreportConcepts);
	}

	/**
	 * Plantilla subreport informacion aduanera
	 * 
	 * @param parameters
	 */
	private void setCustomsInformationJasper(Map<String, Object> parameters) {
		val customsInformationJasper = "cfdi-invoice-subreport-concepts-customs-information.jasper";
		val customsInformationPath = String.format("%s%s", INVOICE_TEMPLATE_PATH, customsInformationJasper);
		val subreportCustomsInformation = CfdiV4ReportTemplateServiceImpl.class
				.getResourceAsStream(customsInformationPath);
		parameters.put("subreport.customs.information", subreportCustomsInformation);
	}

	/**
	 * Plantilla subreport cuenta predial
	 * 
	 * @param parameters
	 */
	private void setPropertyAccountJapser(Map<String, Object> parameters) {
		val propertyAccountJapser = "cfdi-invoice-subreport-concepts-property-account.jasper";
		val propertyAccountPath = String.format("%s%s", INVOICE_TEMPLATE_PATH, propertyAccountJapser);
		val subreportPropertyAccount = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(propertyAccountPath);
		parameters.put("subreport.property.account", subreportPropertyAccount);
	}

	/**
	 * Plantilla subreport traslados
	 * 
	 * @param parameters
	 */
	private void setTransferredJasper(Map<String, Object> parameters) {
		val transferredJasper = "cfdi-invoice-subreport-transferred.jasper";
		val transferredPath = String.format("%s%s", INVOICE_TEMPLATE_PATH, transferredJasper);
		val subreportTransferred = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(transferredPath);
		parameters.put("subreport.transferred", subreportTransferred);
	}

	/**
	 * Plantilla subreport retenciones
	 * 
	 * @param parameters
	 */
	private void setWithheldJasper(Map<String, Object> parameters) {
		val withheldJasper = "cfdi-invoice-subreport-withheld.jasper";
		val withheldJasperPath = String.format("%s%s", INVOICE_TEMPLATE_PATH, withheldJasper);
		val subreportWithheld = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(withheldJasperPath);
		parameters.put("subreport.withheld", subreportWithheld);
	}

	/**
	 * Plantilla subreport cfdi relacionados
	 * 
	 * @param parameters
	 */
	private void setRelatedCfdiJasper(Map<String, Object> parameters) {
		val relatedCfdiJasper = "cfdi-invoice-subreport-related-cfdi.jasper";
		val relatedCfdiPath = String.format("%s%s", INVOICE_TEMPLATE_PATH, relatedCfdiJasper);
		val subreportRelatedCfdi = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(relatedCfdiPath);
		parameters.put("subreport.related.cfdi", subreportRelatedCfdi);
	}
	
	/**
	 * Plantilla principal CFDI V4 Complemento Pago V2
	 * @param parameters
	 */
	public void setCfdiV4PaymentTemplate(Map<String, Object> parameters) {
		val templateJasper = "complement-payment.jasper";
		val templatePath = String.format("%s%s", COMPLEMENT_PAYMENT_TEMPLATE_PATH, templateJasper);
		val template = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(templatePath);
		parameters.put("template", template);
	}
	
	/**
	 * Plantilla subreport pagos
	 * 
	 * @param parameters
	 */
	private void setPaymentsJasper(Map<String, Object> parameters) {
		val paymentsJasper = "complement-payment-subreport-payments.jasper";
		val paymentsPath = String.format("%s%s", COMPLEMENT_PAYMENT_TEMPLATE_PATH, paymentsJasper);
		val subreportPayments = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(paymentsPath);
		parameters.put("subreport.payment", subreportPayments);
	}
	
	/**
	 * Plantilla subreport documentos relacionados al pago
	 * 
	 * @param parameters
	 */
	private void setRelatedDocumentsJasper(Map<String, Object> parameters) {
		val relatedDocumentsJasper = "complement-payment-subreport-related-documents.jasper";
		val relatedDocumentsPath = String.format("%s%s", COMPLEMENT_PAYMENT_TEMPLATE_PATH, relatedDocumentsJasper);
		val relatedDocuments = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(relatedDocumentsPath);
		parameters.put("subreport.related.document", relatedDocuments);
	}
	
	/**
	 * Plantilla subreport traslados de los pago
	 * 
	 * @param parameters
	 */
	private void setPaymentTransferredJasper(Map<String, Object> parameters) {
		val transferredJasper = "complement-payment-subreport-transferred.jasper";
		val transferredPath = String.format("%s%s", COMPLEMENT_PAYMENT_TEMPLATE_PATH, transferredJasper);
		val subreportTransferred = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(transferredPath);
		parameters.put("subreport.payment.transferred", subreportTransferred);
	}

	/**
	 * Plantilla subreport retenciones de los pago
	 * 
	 * @param parameters
	 */
	private void setPaymentWithheldJasper(Map<String, Object> parameters) {
		val withheldJasper = "complement-payment-subreport-withheld.jasper";
		val withheldJasperPath = String.format("%s%s", COMPLEMENT_PAYMENT_TEMPLATE_PATH, withheldJasper);
		val subreportWithheld = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(withheldJasperPath);
		parameters.put("subreport.payment.withheld", subreportWithheld);
	}
	
	/**
	 * Plantilla subreport traslados de los documentos relacionados
	 * 
	 * @param parameters
	 */
	private void setPaymentRelatedDocumentsTransferredJasper(Map<String, Object> parameters) {
		val transferredJasper = "complement-payment-subreport-related-document-transferred.jasper";
		val transferredPath = String.format("%s%s", COMPLEMENT_PAYMENT_TEMPLATE_PATH, transferredJasper);
		val subreportTransferred = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(transferredPath);
		parameters.put("subreport.payment.related.document.transferred", subreportTransferred);
	}
	
	/**
	 * Plantilla subreport retenciones de los documentos relacionados
	 * 
	 * @param parameters
	 */
	private void setPaymentRelatedDocumentsWithheldJasper(Map<String, Object> parameters) {
		val withheldJasper = "complement-payment-subreport-related-document-withheld.jasper";
		val withheldJasperPath = String.format("%s%s", COMPLEMENT_PAYMENT_TEMPLATE_PATH, withheldJasper);
		val subreportWithheld = CfdiV4ReportTemplateServiceImpl.class.getResourceAsStream(withheldJasperPath);
		parameters.put("subreport.payment.related.document.withheld", subreportWithheld);
	}

}