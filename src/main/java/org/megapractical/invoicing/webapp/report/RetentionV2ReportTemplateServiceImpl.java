package org.megapractical.invoicing.webapp.report;

import java.util.Map;

import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class RetentionV2ReportTemplateServiceImpl implements RetentionV2ReportTemplateService {
	
	private static final String RET_TEMPLATE_PATH = "/report/template/ret/2.0/";
	
	@Override
	public void retentionV2Template(Map<String, Object> parameters) {
		setRetV2Template(parameters);
		setWithheldJasper(parameters);
	}
	
	/**
	 * Plantilla principal Retenciones V2
	 * @param parameters
	 */
	public void setRetV2Template(Map<String, Object> parameters) {
		val templateJasper = "ret.jasper";
		val templatePath = String.format("%s%s", RET_TEMPLATE_PATH, templateJasper);
		val template = RetentionV2ReportTemplateServiceImpl.class.getResourceAsStream(templatePath);
		parameters.put("template", template);
	}
	
	/**
	 * Plantilla subreport impuesto retenidos
	 * 
	 * @param parameters
	 */
	private void setWithheldJasper(Map<String, Object> parameters) {
		val conceptsJasper = "ret-subreport-withheld.jasper";
		val subreportConceptsPath = String.format("%s%s", RET_TEMPLATE_PATH, conceptsJasper);
		val subreportConcepts = RetentionV2ReportTemplateServiceImpl.class.getResourceAsStream(subreportConceptsPath);
		parameters.put("subreport.withheld", subreportConcepts);
	}

}