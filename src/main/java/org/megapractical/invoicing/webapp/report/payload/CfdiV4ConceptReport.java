package org.megapractical.invoicing.webapp.report.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.sat.complement.concept.iedu.InstEducativas;

/**
 * <p><b>NOTA IMPORTANTE!</b></p>
 * <p>Las clases usadas en los reportes, no deben utilizar Lombok</p>
 * @author Maikel Guerra Ferrer
 *
 */
public class CfdiV4ConceptReport {
	private String conceptNumber;
	private String identificationNumber;
	private String description;
	private String quantity;
	private String measurementUnit;
	private String unit;
	private String unitValue;
	private String amount;
	private String discount;
	private String keyProductService;
	private String objImp;
	private List<CfdiV4TaxReport> transferreds;
	private List<CfdiV4TaxReport> withhelds;
	private EducationalInstitution iedu;
	
	public static final CfdiV4ConceptReport convert(ApiConcept item, Integer decimal) {
		String mu = String.format("%s %s", item.getMeasurementUnitCode(), item.getMeasurementUnit());
		String kps = String.format("%s %s", item.getKeyProductServiceCode(), item.getKeyProductService());

		CfdiV4ConceptReport conceptReport = new CfdiV4ConceptReport();
		conceptReport.setConceptNumber(item.getConceptNumber());
		conceptReport.setIdentificationNumber(item.getIdentificationNumber());
		conceptReport.setDescription(UValue.revertSingleReplace(item.getDescription()));
		conceptReport.setQuantity(UValue.bigDecimalString(item.getQuantity(), decimal));
		conceptReport.setMeasurementUnit(mu);
		conceptReport.setUnit(item.getUnit());
		conceptReport.setUnitValue(UValue.bigDecimalString(item.getUnitValue(), decimal));
		conceptReport.setAmount(UValue.bigDecimalString(item.getAmount(), decimal));
		conceptReport.setDiscount(UValue.bigDecimalString(item.getDiscount(), decimal));
		conceptReport.setKeyProductService(kps);
		conceptReport.setObjImp(item.getObjImp());
		
		List<CfdiV4TaxReport> transferreds = new ArrayList<>();
		item.getTaxesTransferred().forEach(transferred -> {
			CfdiV4TaxReport conceptTax = new CfdiV4TaxReport(transferred, decimal);
			transferreds.add(conceptTax);
		});
		conceptReport.setTransferreds(transferreds);
		
		List<CfdiV4TaxReport> withhelds = new ArrayList<>();
		item.getTaxesWithheld().forEach(withheld -> {
			CfdiV4TaxReport conceptTax = new CfdiV4TaxReport(withheld, decimal);
			withhelds.add(conceptTax);
		});
		conceptReport.setWithhelds(withhelds);
		
		if (item.getIedu() != null) {
			EducationalInstitution iedu = new EducationalInstitution(item.getIedu());
			conceptReport.setIedu(iedu);
		}

		return conceptReport;
	}
	
	public static class EducationalInstitution {
		private String version;
		private String studentName;
		private String curp;
		private String educationalLevel;
		private String autRvoe;
		private String paymentRfc;
		
		public EducationalInstitution(InstEducativas iedu) {
			this.version = iedu.getVersion();
			this.studentName = iedu.getNombreAlumno();
			this.curp = iedu.getCURP();
			this.educationalLevel = iedu.getNivelEducativo();
			this.autRvoe = iedu.getAutRVOE();
			this.paymentRfc = iedu.getRfcPago();
		}

		/*Getters and Setters*/
		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getStudentName() {
			return studentName;
		}

		public void setStudentName(String studentName) {
			this.studentName = studentName;
		}

		public String getCurp() {
			return curp;
		}

		public void setCurp(String curp) {
			this.curp = curp;
		}

		public String getEducationalLevel() {
			return educationalLevel;
		}

		public void setEducationalLevel(String educationalLevel) {
			this.educationalLevel = educationalLevel;
		}

		public String getAutRvoe() {
			return autRvoe;
		}

		public void setAutRvoe(String autRvoe) {
			this.autRvoe = autRvoe;
		}

		public String getPaymentRfc() {
			return paymentRfc;
		}

		public void setPaymentRfc(String paymentRfc) {
			this.paymentRfc = paymentRfc;
		}
	}

	public String getConceptNumber() {
		return conceptNumber;
	}

	public void setConceptNumber(String conceptNumber) {
		this.conceptNumber = conceptNumber;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnitValue() {
		return unitValue;
	}

	public void setUnitValue(String unitValue) {
		this.unitValue = unitValue;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getKeyProductService() {
		return keyProductService;
	}

	public void setKeyProductService(String keyProductService) {
		this.keyProductService = keyProductService;
	}

	public String getObjImp() {
		return objImp;
	}

	public void setObjImp(String objImp) {
		this.objImp = objImp;
	}

	public List<CfdiV4TaxReport> getTransferreds() {
		if (transferreds == null) {
			transferreds = new ArrayList<>();
		}
		return transferreds;
	}

	public void setTransferreds(List<CfdiV4TaxReport> transferreds) {
		this.transferreds = transferreds;
	}

	public List<CfdiV4TaxReport> getWithhelds() {
		if (withhelds == null) {
			withhelds = new ArrayList<>();
		}
		return withhelds;
	}

	public void setWithhelds(List<CfdiV4TaxReport> withhelds) {
		this.withhelds = withhelds;
	}

	public EducationalInstitution getIedu() {
		return iedu;
	}

	public void setIedu(EducationalInstitution iedu) {
		this.iedu = iedu;
	}
	
}