package org.megapractical.invoicing.webapp.persistence.interfaces;

public interface IPersistenceDAO {

	public Object persist(Object object);
	public Object update(Object object);
	public void delete(Object object);
	
}
