package org.megapractical.invoicing.webapp.persistence.implementations;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class PersistenceDAO implements IPersistenceDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	@Transactional
	public Object persist(Object object) {
		try {
			
			if(!UValidator.isNullOrEmpty(object)){
				entityManager.persist(object);
				entityManager.flush();
				return object;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Transactional
	public Object update(Object object) {
		try {
			
			if(!UValidator.isNullOrEmpty(object)){
				entityManager.merge(object);
				entityManager.flush();
				return object;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Transactional
	public void delete(Object object) {
		try {
			
			if(!UValidator.isNullOrEmpty(object)){
				entityManager.remove(entityManager.merge(object));
				entityManager.flush();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}	
}