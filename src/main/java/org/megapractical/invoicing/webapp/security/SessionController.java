package org.megapractical.invoicing.webapp.security;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.SesionEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.ISessionJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IUserJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.json.JSession;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.Gson;

@Controller
public class SessionController {

	@Autowired
	AppSettings appSettings;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	UserTools userTools;
	
	@Resource
	IUserJpaRepository iUserJpaRepository;
	
	@Resource
	ISessionJpaRepository iSessionJpaRepository;
	
	private static Integer SESSION_TIMEOUT;
	private static String SESSION_VALIDATION_TIME;
	
	private UsuarioEntity user;
	
	public final void init(){
		try {
			
			SESSION_TIMEOUT = Integer.valueOf(appSettings.getPropertyValue("cfdi.session.timeout"));
			SESSION_VALIDATION_TIME = appSettings.getPropertyValue("cfdi.session.validate.time");
			
			String username = null;
			if(userTools.isRoot()){
				username = userTools.getRootLogged().getCorreoElectronico();
			}else if(userTools.isAdmon()){
				username = userTools.getAdmonLogged().getCorreoElectronico();
			}else if(userTools.isUser() || userTools.isMaster()){
				username = userTools.getCurrentUser().getCorreoElectronico();
			}
			this.user = iUserJpaRepository.findByCorreoElectronico(username);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public final HttpSession sessionHandler(){
		try {
			
			RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
		    ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
		    HttpServletRequest request = attributes.getRequest();
		    return request.getSession(true);
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String urlReferer(){
		try {
			
			//HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
		    ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
		    HttpServletRequest request = attributes.getRequest();
			
		    String[] url = null;
			try {
				
				url = request.getHeader("referer").split(request.getContextPath());
				
			} catch (Exception e) {
				url = request.getRequestURL().toString().split(request.getContextPath());
			}
			return url[1];
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String currentUrl() {
		try {
		
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			String[] url = request.getRequestURL().toString().split(request.getContextPath());
			String currentUrl = url[1] + (!UValidator.isNullOrEmpty(request.getQueryString()) ? "?" + request.getQueryString() : "");
			
			return currentUrl;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public SesionEntity sessionInstance(){
		try {
			
			init();
			return iSessionJpaRepository.findByUsuario(this.user);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean sessionCreate(){
		try {
			
			init();
			
			SesionEntity session = iSessionJpaRepository.findByUsuario(this.user);
			
			if(UValidator.isNullOrEmpty(session) || (UValidator.isNullOrEmpty(session.getUrlActiva()) || !session.getUrlActiva().equals("/loginSuccess"))){
				if(!UValidator.isNullOrEmpty(session)){
					persistenceDAO.delete(session);
				}
				
				session = new SesionEntity();
				session.setUsuario(this.user);
				session.setSesion(sessionHandler().getId());
				session.setUrlActiva(urlReferer());
				session.setFechaActiva(new Date());
				session.setHoraActiva(new Date());
				session.setUltimaFechaActiva(new Date());
				session.setUltimaHoraActiva(new Date());
				session.setTiempoInactivo(0);
				session.setSesionActiva(true);
				session.setActualizadaPorFuncionalidad(Boolean.TRUE);
				session.setActualizadaPorIntervaloTiempo(Boolean.FALSE);
				
				persistenceDAO.persist(session);
				
				this.user.setFechaUltimoAcceso(new Date());
				this.user.setHoraUltimoAcceso(new Date());
				this.user = (UsuarioEntity) persistenceDAO.update(this.user);
				
			}
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void sessionUpdate(){
		SesionEntity session = null;
		try {
			
			init();
			
			session = iSessionJpaRepository.findByUsuario(this.user);
			
			if(sessionValidate(session)){
				session.setUrlActiva(urlReferer());
				session.setUltimaFechaActiva(session.getFechaActiva());
				session.setUltimaHoraActiva(session.getHoraActiva());
				session.setTiempoInactivo(UDateTime.elapsedTime(session.getFechaActiva(), session.getHoraActiva(), SESSION_VALIDATION_TIME));
				session.setFechaActiva(new Date());
				session.setHoraActiva(new Date());
				session.setSesionActiva(Boolean.TRUE);
				session.setActualizadaPorFuncionalidad(Boolean.TRUE);
				session.setActualizadaPorIntervaloTiempo(Boolean.FALSE);
			}else{
				if(!UValidator.isNullOrEmpty(session)){
					session.setActualizadaPorFuncionalidad(Boolean.FALSE);
					session.setActualizadaPorIntervaloTiempo(Boolean.FALSE);
					session.setSesionActiva(Boolean.FALSE);
				}
			}
			
			if(!UValidator.isNullOrEmpty(session)){
				persistenceDAO.update(session);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sessionUpdate(SesionEntity session){
		try {
			
			init();
			
			session.setUrlActiva(urlReferer());
			if(!session.getActualizadaPorFuncionalidad()){
				session.setUltimaFechaActiva(session.getFechaActiva());
				session.setUltimaHoraActiva(session.getHoraActiva());
				session.setTiempoInactivo(UDateTime.elapsedTime(session.getFechaActiva(), session.getHoraActiva(), SESSION_VALIDATION_TIME));
			}			
			session.setFechaActiva(new Date());
			session.setHoraActiva(new Date());
			session.setSesionActiva(Boolean.TRUE);
			session.setActualizadaPorFuncionalidad(Boolean.FALSE);
			session.setActualizadaPorIntervaloTiempo(Boolean.TRUE);
			
			persistenceDAO.update(session);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sessionDestroy(){
		try {
			
			init();
			
			SesionEntity session = iSessionJpaRepository.findByUsuario(user);
			
			if(!UValidator.isNullOrEmpty(session)){
				persistenceDAO.delete(session);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sessionDestroy(UsuarioEntity user){
		try {
			
			SesionEntity session = iSessionJpaRepository.findByUsuario(user);
			
			if(!UValidator.isNullOrEmpty(session)){
				persistenceDAO.delete(session);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/sessionMonitor", method = RequestMethod.POST)
	public @ResponseBody String sessionMonitor(){
		
		JSession jSession = new JSession();
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			init();
			
			SesionEntity session = iSessionJpaRepository.findByUsuario(user);
			if(UValidator.isNullOrEmpty(session)){
				jSession.setSessionActive(false);
			}else{
				if(sessionValidate(session)){
					sessionUpdate(session);
					
					jSession.setSessionId(UBase64.base64Encode(session.getSesion()));
					jSession.setSessionUserId(UBase64.base64Encode(session.getUsuario().getId().toString()));
					jSession.setSessionDate(UBase64.base64Encode(UDate.formattedDate(session.getFechaActiva(), "dd/MM/yyyy")));
					jSession.setSessionTime(UBase64.base64Encode(UDate.formattedTime(session.getHoraActiva())));
					jSession.setSessionActive(session.getSesionActiva());
				}else{
					persistenceDAO.delete(session);
					jSession.setSessionActive(Boolean.FALSE);
					session.setActualizadaPorFuncionalidad(Boolean.FALSE);
					session.setActualizadaPorIntervaloTiempo(Boolean.FALSE);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}

		jsonInString = gson.toJson(jSession);
		return jsonInString;
	}
	
	public Boolean sessionValidate(SesionEntity session){
		try {
			
			init();
			
			if(!UValidator.isNullOrEmpty(session)){
				
				Date sessionDate = session.getFechaActiva();
				Date sessionTime = session.getHoraActiva();
				
				Integer idleTime = UDateTime.elapsedTime(sessionDate, sessionTime, SESSION_VALIDATION_TIME);
				
				if(UDateTime.isEqualLocalDate(sessionDate, new Date()) && session.getSesion().equalsIgnoreCase(sessionHandler().getId()) && session.getSesionActiva()){
					return idleTime < SESSION_TIMEOUT;
				}
			}
			return false;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}