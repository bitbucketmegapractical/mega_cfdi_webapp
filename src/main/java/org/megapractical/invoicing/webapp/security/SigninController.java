package org.megapractical.invoicing.webapp.security;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.CodigoActivacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioContratoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IActivationCodeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.Layout;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.val;
import lombok.var;

@Controller
class SigninController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	AppSettings appSettings;

	@Autowired
	UserTools userTools;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Resource
	private IActivationCodeJpaRepository iActivationCodeJpaRepository;
	
	@Resource
	private IStampJpaRepository iStampJpaRepository;

	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/login")
	public String login(Model model) {
		if (!model.containsAttribute("showlockscreen")) {
			model.addAttribute("showlockscreen", false);
		}
		return "/login/login";
	}
	
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/loginFailure")
	public String loginFailure(Model model) throws IOException{
		model.addAttribute("failure", true);
		model.addAttribute("failureMessage", UProperties.getMessage("ERR1007", userTools.getLocale()));
		return "/login/login";
	}

	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/accessDenied")
	public String accessDenied() {
		return "/denied/AccessDenied";
	}
	
	@RequestMapping({"/", "/dashboard"})
	public String home() {
		HtmlHelper.functionalitySelected("MyCFDIs", null);
		appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
		if (userTools.isAssociatedAccount()) {
			return "home/DashboardAssociated";
		}
		return "home/Dashboard";
	}

	@RequestMapping(value = "/userGuaranteedAccess")
	public String guaranteedAccess(HttpServletRequest request, HttpServletResponse response, RedirectAttributes modelAndView) {
		String view = null;
		try {
			
			if (userTools.isUser()) {
				String access = evaluateTestPeriod();
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				if (UValidator.isNullOrEmpty(access)) {
					sessionController.sessionDestroy();
					if (auth != null) {
						new SecurityContextLogoutHandler().logout(request, response, auth);
					}
					modelAndView.addFlashAttribute("failure", true);
					modelAndView.addFlashAttribute("failureMessage", UProperties.getMessage("ERR1008", userTools.getLocale()));
					view = "redirect:/login?periodInvalid";
				} else if (access.equals("test_period_ended")) {
					sessionController.sessionDestroy();
					if (auth != null) {
						new SecurityContextLogoutHandler().logout(request, response, auth);
					}
					modelAndView.addFlashAttribute("failure", true);
					modelAndView.addFlashAttribute("failureMessage", UProperties.getMessage("ERR1009", userTools.getLocale()));
					view = "redirect:/login?periodEnded";
				} else if (access.equals("guaranteed_access")) {
					userTools.log("[INFO] USER HAS LOGGED IN");
					
					appLog.logOperation(OperationCode.ACCESO_EXITOSO_ID3);
					return "redirect:/dashboard";
				}
			} else if (userTools.isMaster()) {
				evaluateConsigneePeriod();
				
				userTools.log("[INFO] USER HAS LOGGED IN");
				
				appLog.logOperation(OperationCode.ACCESO_EXITOSO_ID3);
				return "redirect:/dashboard";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return view;
	}
	
	@RequestMapping(value = "/loginSuccess")
	public String loginSuccess(HttpServletRequest request, HttpServletResponse response, RedirectAttributes modelAndView) {
		String view = null;
		try {
			
			if (sessionController.sessionCreate()) {
				if (userTools.isAdmon() || userTools.isRoot()) {
					appLog.logOperation(OperationCode.ACCESO_EXITOSO_ID3);
					return "redirect:/managementDashboard";
				}else if (userTools.isUser() || userTools.isMaster()) {
					return "redirect:/userGuaranteedAccess";
				}
			}else{
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				new SecurityContextLogoutHandler().logout(request, response, auth);
				modelAndView.addFlashAttribute("failure", true);
				modelAndView.addFlashAttribute("failureMessage", UProperties.getMessage("ERR1011", userTools.getLocale()));
				view = "redirect:/login?sessionError";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}

	@GetMapping(value = "/logout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response, String cause, RedirectAttributes modelAndView) {
		var view = "redirect:/login?logout";
		
		var print = "[INFO] USER HAS LOGGED OUT";
		val printLocked = "[INFO] USER HAS LOGGED OUT BY INACTIVITY TIME";
		
		val auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			if (cause != null && cause.equals("lock")) {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				
				String username;
				if (principal instanceof UserDetails) {
					username = ((UserDetails) principal).getUsername();
				} else {
					username = principal.toString();
				}
				
				appLog.logOperation(OperationCode.BLOQUEAR_SESION_DE_USUARIO_ID6);
				
				print = printLocked;
				
				modelAndView.addFlashAttribute("showlockscreen", true);
				modelAndView.addFlashAttribute("lockedusername", username);
				
				view = "redirect:/login?lock";
			} else {
				appLog.logOperation(OperationCode.CERRAR_SESION_ID5);
			}
			
			sessionController.sessionDestroy();
			
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		
		userTools.log(print);
		
		return view;
	}

	public String evaluateTestPeriod() {
		try {					
			
			String testPeriod = appSettings.getPropertyValue("cfdi.test.period");
			Integer period = Integer.valueOf(testPeriod);
			
			UsuarioEntity loggedUser = userTools.getCurrentUser();
			CodigoActivacionEntity activationCode = iActivationCodeJpaRepository.findByUsuario(loggedUser);
			Date activationDate = activationCode.getFechaActivacion();
			
			if (UValidator.isNullOrEmpty(activationDate)) {
				return null;
			}
			
			LocalDate finalTestPeriod = UDateTime.localDateFromDate(activationDate).plusDays(period);
			/*if (finalTestPeriod.isAfter(UDateTime.getLocalDateToday())) {
				return "guaranteed_access";
			}else{
				return "test_period_ended";
			}*/
			
			if (!finalTestPeriod.isAfter(UDateTime.getLocalDateToday())) {
				TimbreEntity stamp = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(userTools.getContribuyenteActive());
				if (stamp != null) {
					stamp.setHabilitado(Boolean.FALSE);
					persistenceDAO.update(stamp);
				}
			}
		
			return "guaranteed_access";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void evaluateConsigneePeriod() {
		if (userTools.isConsignee()) {
			userTools.log("[INFO] VERIFIYING CONSIGNEE CONTRACT PERIOD...");
			
			if (!userTools.isConsigneeActive()) {
				//##### Cargando consignatario
				ConsignatarioEntity consignee = userTools.consigneeByContribuyente();
				if (!UValidator.isNullOrEmpty(consignee)) {
					userTools.log("[INFO] DISABLING CONSIGNEE BY END CONTRACT PERIOD...");
					
					consignee.setActivo(Boolean.FALSE);
					consignee.setContratoFinalizado(Boolean.TRUE);
					persistenceDAO.update(consignee);
					
					appLog.logOperation(OperationCode.DESHABILITAR_CONSIGNATARIO_ID77);
					
					//##### Cargando contrato activo del consigntario
					ConsignatarioContratoEntity consigneeContract = userTools.consigneeContractActive(consignee);
					if (!UValidator.isNullOrEmpty(consigneeContract)) {
						userTools.log("[INFO] DISABLING CONSIGNEE CONTRACT BY END PERIOD...");
						
						consigneeContract.setActivo(Boolean.FALSE);
						persistenceDAO.update(consigneeContract);
						
						appLog.logOperation(OperationCode.DESHABILITAR_CONSIGNATARIO_CONTRATO_ID78);
					}
				}
			}
		}
	}
}