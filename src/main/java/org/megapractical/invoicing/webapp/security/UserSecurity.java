package org.megapractical.invoicing.webapp.security;

import java.util.Collection;
import static java.util.stream.Collectors.toList;

import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


@SuppressWarnings("serial")
public class UserSecurity implements UserDetails {
    private final UsuarioEntity user;

    public UserSecurity(UsuarioEntity user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
		return user.getAuthorities().stream().map(SimpleGrantedAuthority::new).collect(toList());
    }

    @Override
    public String getUsername() {
        return user.getCorreoElectronico();
    }
    
    @Override
    public String getPassword() {
        return user.getClave();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.isActivo();
    }
}