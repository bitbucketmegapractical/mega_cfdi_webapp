package org.megapractical.invoicing.webapp.security;

import org.megapractical.invoicing.dal.data.repository.IUserJpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
	private final IUserJpaRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return userRepository.findByCorreoElectronicoAndActivoTrue(email)
							 .map(UserSecurity::new)
							 .orElseThrow(() -> new UsernameNotFoundException("User Not Found!"));
	}
}