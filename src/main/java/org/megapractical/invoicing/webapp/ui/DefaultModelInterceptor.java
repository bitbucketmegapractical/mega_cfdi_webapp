package org.megapractical.invoicing.webapp.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.CuentaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;



@Component
public class DefaultModelInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	AppSettings appSettings;
	
	@Autowired
	private UserTools userTools;
	
	@Resource
	private IStampJpaRepository iStampJpaRepository;

	@Resource
	private IAccountJpaRepository iAccountJpaRepository;
	
	
	@Override
	@Transactional
	public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView) throws Exception {
		if (modelAndView != null && modelAndView.getViewName() != null && modelAndView.getViewName().startsWith("redirect:")){
			return;
		}
		
		// Tiempo de inactividad de sesion
		String sessionTimeOut = appSettings.getPropertyValue("cfdi.session.timeout");
		
		// Activar/Transferir timbres
		String activateTransferStamp = appSettings.getPropertyValue("cfdi.stamp.activate.transfer");
		
		ContribuyenteEntity contribuyente = userTools.getContribuyenteActive();
		
		if(modelAndView != null && contribuyente == null && (userTools.isAdmon() || userTools.isRoot())){
			LayoutViewModel layoutViewModel = new LayoutViewModel();
			layoutViewModel.setCurrentView(modelAndView.getViewName());
			
			String eviroment = appSettings.getPropertyValue("cfdi.environment");
			eviroment = eviroment.equals("1") ? "Ambiente producción" : "Ambiente desarrollo";
			modelAndView.getModelMap().addAttribute("enviroment", eviroment);
		}else if (modelAndView != null && contribuyente != null) {
			LayoutViewModel layoutViewModel = new LayoutViewModel();
			layoutViewModel.setCurrentView(modelAndView.getViewName());
			
			List<String> rfcList = new ArrayList<String>();
			if(contribuyente.getRfcActivo() != null && !contribuyente.getRfcActivo().isEmpty()){
				layoutViewModel.setRfcActiveByUser(contribuyente.getRfcActivo());
				
				String taxpayerRfc = contribuyente.getRfc();
				if (!rfcList.contains(taxpayerRfc)) {
					rfcList.add(taxpayerRfc);
				}
				
				String taxpayerRfcActive = contribuyente.getRfcActivo();
				if (!rfcList.contains(taxpayerRfcActive)) {
					rfcList.add(taxpayerRfcActive);
				}
				
				//List<CuentaEntity> cuentaSource = contribuyente.getListOfCuentaByContribuyenteAsociado();
				List<CuentaEntity> cuentaSource = iAccountJpaRepository.findByContribuyenteAndEliminadoFalse(contribuyente);
				for (CuentaEntity cuenta : cuentaSource) {
					String rfc = cuenta.getContribuyenteAsociado().getRfc();
					if (!rfcList.contains(rfc)) {
						rfcList.add(rfc);
					}
				}
			}
			
			//*************************************************							
			//Obtencion de los dias transcurridos y el dia actual para la vista								
									
			int daysLeft = userTools.getDaysLeft();
			int currentDay = userTools.getCurrentDay();
			int totalDays = userTools.getTotalDays();				
			
			String daysStyle = "color: #428bca; font-size: 200%;";
			String daysText="";
			
			if(userTools.isUser()){
				if(userTools.haveDaysLeft()) {
					if(currentDay == 1)  {
						daysText = Integer.toString(currentDay)+ " día transcurrido de " + Integer.toString(totalDays) + " días totales";
					}else {
						daysText = Integer.toString(currentDay)+ " días transcurridos de " + Integer.toString(totalDays) + " días totales";
					}
				}else {
					daysText = "Se ha agotado el periodo de " + Integer.toString(totalDays) + " días";
				}
			}
			
			
			if(userTools.isUser()){
				//para usuarios gratuitos se muestra el texto rojo cuando falte el 40%
				if(daysLeft <= totalDays * 0.4) {
					daysStyle = "color: #d9534f; font-size: 200%;";
				}
			}/*else {
				//Para usuarios NO gratuitos se muestra en rojo el texto cuando falte el 178% de tiempo total para que resten 65 días en un año en rojo
				if(daysLeft <= totalDays * 0.178) {
					daysStyle = "color: #d9534f; font-size: 200%;";
				}
			}*/
			
			if(userTools.isUser()) {
				modelAndView.getModelMap().addAttribute("daysStyle", daysStyle);
				modelAndView.getModelMap().addAttribute("daysText", daysText);
			}
			
							
			//valida que no haya subido dos archivos de nomina
			
			String buttonNominaClass = "btn btn-success";
			String textFileUploadExceeded = "";
			
			TimbreEntity stamp = iStampJpaRepository.findByContribuyente(userTools.getContribuyenteActive());
			if(stamp!=null){
				if( !userTools.canUploadPayroll() && stamp.getTimbresDisponibles() <= 0  )  {
					buttonNominaClass = "btn btn-success disabled";
					textFileUploadExceeded = "Ha excedido el límite de archivos permitidos para esta cuenta.";
				}
			}else {
				if( !userTools.canUploadPayroll() )  {
					buttonNominaClass = "btn btn-success disabled";
					textFileUploadExceeded = "Ha excedido el límite de archivos permitidos para esta cuenta.";
				}
			}
											
			modelAndView.getModelMap().addAttribute("buttonNominaClass", buttonNominaClass);
			modelAndView.getModelMap().addAttribute("textFileUploadExceeded", textFileUploadExceeded);
			
			//***************************************************
			
			// Determinando si usa el sistema gratuito
			if(userTools.isUser()){
								
				//##### Total de días establecidos para el periodo de pruebas 
				Integer testPeriod = userTools.testPeriod();
				
				//##### Total de dias restantes de prueba del usuario
				Integer period = userTools.determinePeriod();
				
				String periodFree = period == 0 ? "Último día de pruebas" : (period == 1 ? period + " día de prueba" : period + " días de prueba");
				modelAndView.getModelMap().addAttribute("periodFree", periodFree);
				
				//period == 20 ? "fa fa-battery-full" : (period >= 11 ? "fa fa-battery-three-quarters" : (period == 10 ? "fa fa-battery-half" : (period >= 1 ? "fa fa-battery-quarter" : "fa fa-battery-empty")));
				String periodClass = null;
				String periodStyle = null;
				Integer percentage = null;
				
				if(period == testPeriod){
					periodClass = "fa fa-battery-full";
					periodStyle = "margin-left: 2px;";
				}else{
					percentage = period * 100 / testPeriod;
					if(percentage > 50){
						periodClass = "fa fa-battery-three-quarters";
					}else if(percentage == 50){
						periodClass = "fa fa-battery-half";
					}else if(percentage >= 1){
						periodClass = "fa fa-battery-quarter";
					}else{
						periodClass = "fa fa-battery-empty";
					}
				}
				
				
				//modelAndView.getModelMap().addAttribute("periodFreeClass", periodClass);
						
				if(!UValidator.isNullOrEmpty(percentage)){
					//periodStyle = percentage <= 5 ? "margin-left: 2px; color: red; font-weight: bold;" : "margin-left: 2px;";
				}
				//modelAndView.getModelMap().addAttribute("periodFreeStyle", periodStyle);
			}
						
			modelAndView.getModelMap().addAttribute("layoutViewModel", layoutViewModel);
			modelAndView.getModelMap().addAttribute("masterAccountRfc", contribuyente.getRfc());
			modelAndView.getModelMap().addAttribute("currentUserRfcList", rfcList);
			modelAndView.getModelMap().addAttribute("actualRFC", contribuyente.getRfcActivo());
			modelAndView.getModelMap().addAttribute("currentView", layoutViewModel.getCurrentView());			
		}
		
		if(modelAndView != null){
			modelAndView.getModelMap().addAttribute("sessionTimeOut", sessionTimeOut);
			modelAndView.getModelMap().addAttribute("activateTransferStamp", activateTransferStamp);
		}
	}
}
