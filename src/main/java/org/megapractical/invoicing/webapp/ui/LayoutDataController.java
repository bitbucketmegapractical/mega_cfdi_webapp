package org.megapractical.invoicing.webapp.ui;

import javax.validation.Valid;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.SesionEntity;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class LayoutDataController {

	@Autowired
	UserTools userTools;
	
	@Autowired
	SessionController sessionController;

	@Transactional
	@RequestMapping(value = "/activateAccount", method = RequestMethod.POST)
	public String processRegistration(@Valid LayoutViewModel layoutViewModel, BindingResult errors, Model model, RedirectAttributes attributes) {
		try {
			
			if (layoutViewModel.getRfcActiveByUser() != null && !layoutViewModel.getRfcActiveByUser().isEmpty()){
				ContribuyenteEntity taxpayer = userTools.getContribuyenteActive();
				taxpayer.setRfcActivo(layoutViewModel.getRfcActiveByUser());
			}else{
				attributes.addFlashAttribute("message", "Debe seleccionar una cuenta RFC.");
			}
			
			String redirect = "/";
			SesionEntity session = sessionController.sessionInstance();
			if(!UValidator.isNullOrEmpty(session)){
				redirect = session.getUrlActiva();
			}
			//return "redirect:" + redirect;	
			return "redirect:/";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
}