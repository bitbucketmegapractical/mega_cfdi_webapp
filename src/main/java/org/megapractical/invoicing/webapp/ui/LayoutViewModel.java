package org.megapractical.invoicing.webapp.ui;

public class LayoutViewModel {
	private String rfcActiveByUser;
	private String currentView;

	public String getRfcActiveByUser() {
		return rfcActiveByUser;
	}

	public void setRfcActiveByUser(String rfcActiveByUser) {
		this.rfcActiveByUser = rfcActiveByUser;
	}

	public String getCurrentView() {
		return currentView;
	}

	public void setCurrentView(String currentView) {
		this.currentView = currentView;
	}
}
