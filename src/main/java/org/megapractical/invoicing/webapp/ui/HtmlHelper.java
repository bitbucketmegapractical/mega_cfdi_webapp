package org.megapractical.invoicing.webapp.ui;

import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UValidator;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import lombok.val;
import lombok.var;

@Scope("session")
public class HtmlHelper {

	// Areas: MyCFDIs - EmitterConfig
	private static final String[] navs = { 
		"MyCFDIs", "EmitterConfig", "EmitterAdvancedOptions", 
		"Payroll", "Retention", "Payment", "Cancellation" 
	};
	private static final String[] managementNavs = { "MngGeneral" };
	
	private static final String LAST_ACTIVE_AREA = "lastActiveArea";
	private static final String LAST_ACTIVE_FNC = "lastActiveFnc";
	
	public static String isAreaSelected(String area) {
		String cssClass = "active";
		String result = "";

		String defaultArea = "MyCFDIs";
		String defaultManagementArea = "MngGeneral";
		String lastActiveArea = null;

		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
					.getRequest();

			String contextPath = request.getContextPath();
			String currentUrl = request.getRequestURL().toString();
			if ((currentUrl.endsWith(contextPath + "/") || currentUrl.endsWith("/dashboard")
					|| currentUrl.endsWith("/managementDashboard"))) {
				if (area.equals(defaultArea) || area.equals(defaultManagementArea)) {
					request.getSession().setAttribute(LAST_ACTIVE_AREA, area);
					request.getSession().setAttribute(LAST_ACTIVE_FNC, null);
					return "active";
				}
			} else {
				if (request.getSession().getAttribute(LAST_ACTIVE_AREA) != null) {
					lastActiveArea = (String) request.getSession().getAttribute(LAST_ACTIVE_AREA);
					if (lastActiveArea.equals(area)) {
						result = cssClass;
					}
				} else {
					for (String item : navs) {
						if (item.equals(area)) {
							result = cssClass;
							break;
						}
					}
					for (String item : managementNavs) {
						if (item.equals(area)) {
							result = cssClass;
							break;
						}
					}
				}
			}

			// result = currentUrl.contains(area) ? cssClass : "";
			if (result.equals(cssClass)) {
				request.getSession().setAttribute(LAST_ACTIVE_AREA, area);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String isFncSelected(String functionality){
		String result = "";
		try {
			String cssClass = "nav-fnc-active";
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			
			if (request.getSession().getAttribute(LAST_ACTIVE_FNC) != null){
				String fnc = (String) request.getSession().getAttribute(LAST_ACTIVE_FNC);
				result = fnc.equalsIgnoreCase(functionality) ? cssClass : "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static void functionalitySelected(){
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			request.getSession().setAttribute(LAST_ACTIVE_AREA, null);
			request.getSession().setAttribute(LAST_ACTIVE_FNC, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void functionalitySelected(String functionalityNode, String functionality) {
		try {
			if (!UValidator.isNullOrEmpty(functionalityNode) && !UValidator.isNullOrEmpty(functionality)) {
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes()).getRequest();

				var isValidNode = false;
				for (val item : navs) {
					if (item.equals(functionalityNode)) {
						isValidNode = true;
						break;
					}
				}

				if (isValidNode) {
					request.getSession().setAttribute(LAST_ACTIVE_AREA, functionalityNode);
				}
				request.getSession().setAttribute(LAST_ACTIVE_FNC, functionality);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void functionalitySelected(String functionality) {
		try {
			if (!UValidator.isNullOrEmpty(functionality)) {
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes()).getRequest();
				request.getSession().setAttribute(LAST_ACTIVE_FNC, functionality);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String currentUrl(HttpServletRequest request) {
		return request.getRequestURL().toString() + "?" + request.getQueryString();
	}
}
