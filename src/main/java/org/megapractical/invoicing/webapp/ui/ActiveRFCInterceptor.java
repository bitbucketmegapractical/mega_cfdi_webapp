package org.megapractical.invoicing.webapp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class ActiveRFCInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private UserTools userTools;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (handler instanceof HandlerMethod) {
			HandlerMethod handlerMethod = (HandlerMethod) handler;

			ActiveRFCRequired annotation = handlerMethod.getMethodAnnotation(ActiveRFCRequired.class);
			
			//UsuarioEntity user = userTools.getCurrentUser();
			//CredencialEntity credential = userTools.getCredentialByUser(user);
			ContribuyenteEntity contribuyente = userTools.getContribuyenteActive();
			
			if (annotation != null && (contribuyente.getRfcActivo() == null || contribuyente.getRfcActivo().trim().isEmpty())) {
				String rootUrl = request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());
				response.sendRedirect(rootUrl + "/Errors/NoActiveRFCError/");
			}
		}
		return true;
	}
}
