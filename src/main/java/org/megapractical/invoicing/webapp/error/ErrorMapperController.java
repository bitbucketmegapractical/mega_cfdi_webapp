package org.megapractical.invoicing.webapp.error;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ErrorMapperController {
	
	@RequestMapping(value = "/Error/NoActiveRFCError/", method = RequestMethod.GET)
	public String list(Integer page, String sortProperty, String sortDirection, String searchString, Model model) {
		return "/errors/no-active-rfc";
	}
}
