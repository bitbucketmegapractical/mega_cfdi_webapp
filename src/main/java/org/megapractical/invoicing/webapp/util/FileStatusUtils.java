package org.megapractical.invoicing.webapp.util;

import org.megapractical.invoicing.dal.bean.datatype.FileStatus;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class FileStatusUtils {
	
	private FileStatusUtils() {		
	}
	
	public static String status(FileStatus fileStatus) {
		if (fileStatus == null) {
			return "No procesado";
		}
		
		switch (fileStatus) {
			case C:
				return "En cola";
			case P:
				return "Procesándose";	
			case G:
				return "Procesado";	
			default:
				return "No procesado";
		}
	}
	
}