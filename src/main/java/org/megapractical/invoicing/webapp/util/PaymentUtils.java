package org.megapractical.invoicing.webapp.util;

public final class PaymentUtils {
	
	private PaymentUtils() {		
	}
	
	/**
	 * Current version
	 */
	public static final String VERSION = "2.0";
	
	/**
	 * Package complemento de recepcion de pagos
	 */
	public static final String PAYMENT_PACKAGE = "org.megapractical.invoicing.sat.complement.payments";
	
	/**
	 * Payment file code
	 */
	public static final String PAYMENT_FILE_TXT_CODE = "CP20TXT";
	
}