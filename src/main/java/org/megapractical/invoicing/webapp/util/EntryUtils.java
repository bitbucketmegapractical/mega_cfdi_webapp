package org.megapractical.invoicing.webapp.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class EntryUtils {
	
	private EntryUtils() {		
	}
	
	public static boolean contains(String[] source, String value) {
	    return Arrays.stream(source).anyMatch(value::equals);
	}
	
	public static boolean contains(String[] source, List<String> values) {
	    return Arrays.stream(source).anyMatch(values::contains);
	}
	
	public static int posInArray(String[] source, String value) {
	    return IntStream.range(0, source.length)
	            		.filter(i -> source[i].equals(value))
	            		.findFirst()
	            		.orElse(-1);
	}
	
	public static String nextElement(String[] source, int position) {
	    if (position >= source.length - 1) {
	        return null; // Si la posición es igual o mayor a la longitud del array, devuelve null
	    }
	    return source[position + 1];
	}
	
}