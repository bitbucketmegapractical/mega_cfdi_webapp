package org.megapractical.invoicing.webapp.util;

import java.io.IOException;
import java.util.Properties;

import lombok.val;

public final class UProperties {
	
	private UProperties() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	private static final String LOCALE = "es_MX";
	
	public static String getMessage(String key, String lang) {
		try {
		
			String fileMessage = "i18n/" + (lang.equals(LOCALE) ? "messages_es_MX.properties" : "messages_en_US.properties");
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(fileMessage));
			return properties.getProperty(key);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static String genericUnexpectedError(String lang){
		return getMessage("ERR1012", lang);
	}
	
	public static String env() {
		try {
			
			val properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
			return properties.getProperty("spring.profiles.active").toUpperCase();
			
		} catch (IOException e) {
			System.out.println(String.format("#environmentPrint error - %s", e.getMessage()));
		}
		return null;
	}
	
}