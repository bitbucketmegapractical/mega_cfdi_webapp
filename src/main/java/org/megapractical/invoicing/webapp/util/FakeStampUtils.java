package org.megapractical.invoicing.webapp.util;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;

/**
 * This class should only be used in the development environment
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public final class FakeStampUtils {

	private FakeStampUtils() {
	}

	public static final String JSON_TMP_PATH = "/json_tmp/";
	private static final ObjectMapper objectMapper = new ObjectMapper();

	public static StampResponse cfdiFakeStampError() {
		try {
			val filePath = JSON_TMP_PATH + "cfdi-stamp-error-401.json";
			return stampResponseError(filePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static StampResponse stampResponseError(String filePath) {
		try {
			val is = FakeStampUtils.class.getResourceAsStream(filePath);
			val response = objectMapper.readValue(is, SwStampError.class);
			return StampResponse.error(response.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	static class SwStampError {
		private int statusCode;
		private String status;
		private String message;
		private String messageDetail;
	}

}