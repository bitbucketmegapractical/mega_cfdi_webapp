package org.megapractical.invoicing.webapp.util;

public final class RfcUtils {
	
	private RfcUtils() {		
	}
	
	/**
	 * Regular expression to validate RFC
	 */
	public static final String RFC_EXPRESSION = "[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?";
	
	
	/**
	 * Moral Person
	 */
	public static final Integer RFC_MIN_LENGTH = 12;
	
	
	/**
	 * Natural person
	 */
	public static final Integer RFC_MAX_LENGTH = 13;
	
	/**
	 * Rfc error code
	 */
	public static final String RFC_ERROR_CODE = "ERR1004";
	
}