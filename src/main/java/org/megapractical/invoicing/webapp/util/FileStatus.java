package org.megapractical.invoicing.webapp.util;

public enum FileStatus {
	QUEUED("C"), 
	PROCESSING("P"),
	GENERATED("G");
	
	private final String value;

	FileStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FileStatus fromValue(String v) {
        return valueOf(v);
    }
}