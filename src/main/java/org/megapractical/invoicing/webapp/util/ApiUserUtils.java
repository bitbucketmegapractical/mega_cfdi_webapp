package org.megapractical.invoicing.webapp.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.val;
import lombok.var;

public final class ApiUserUtils {

	private ApiUserUtils() {		
	}
	
	public static void main(String[] args) {
		generateApiUserCredentials();
	}
	
	private static void generateApiUserCredentials() {
		var apiKey = RandomStringUtils.randomAlphabetic(36);
		System.out.println("apiKey: " + apiKey);
		val apiKeyCrypt = new BCryptPasswordEncoder(12).encode(apiKey);
		System.out.println("apiKeyCrypt: " + apiKeyCrypt);
	}

}