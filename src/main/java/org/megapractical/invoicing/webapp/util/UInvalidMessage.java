package org.megapractical.invoicing.webapp.util;

import java.util.regex.Pattern;

public final class UInvalidMessage {
	
	private UInvalidMessage() {
	}
	
	public static String satInvalidCatalogMessage(String key, String lang, String[] values){
		try {
		
			// values[0] : Error code
			// values[1] : Field name
			// values[2] : Catalog name
			
			String message = UProperties.getMessage(key, lang);
			if(message != null){
				String[] messageSplitted = message.split(Pattern.quote("{0}"));
				message = values[0].concat(": ").concat(messageSplitted[0]).concat(values[1]).concat(messageSplitted[1]).concat(" ").concat(values[2]);
			}
			return message;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String systemInvalidCatalogMessage(String key, String lang, String[] values){
		try {
		
			// values[0] : Field name
			// values[1] : Catalog name
			
			String message = UProperties.getMessage(key, lang);
			if(message != null){
				String[] messageSplitted = message.split(Pattern.quote("{0}"));
				message = messageSplitted[0].concat(values[0]).concat(messageSplitted[1]).concat(" ").concat(values[1]);
			}
			return message;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String systemInvalidLength(String message, String field, Integer length){
		try {
			
			String result = null;
			
			String[] messageSplitted = message.split(Pattern.quote("{0}"));
			result = messageSplitted[0] + field;
			
			messageSplitted = messageSplitted[1].split(Pattern.quote("{1}"));
			result = result + messageSplitted[0] + length + messageSplitted[1];  
			
			return result;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String systemInvalidLength(String message, String field, Integer minLength, Integer maxLength){
		try {
			
			String result = null;
			
			String[] messageSplitted = message.split(Pattern.quote("{0}"));
			result = messageSplitted[0] + field;
			
			messageSplitted = messageSplitted[1].split(Pattern.quote("{1}"));
			result = result + messageSplitted[0] + minLength;
			
			messageSplitted = messageSplitted[1].split(Pattern.quote("{2}"));
			result = result + messageSplitted[0] + maxLength + messageSplitted[1];
			
			return result;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
