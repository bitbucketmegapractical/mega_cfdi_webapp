package org.megapractical.invoicing.webapp.util;

import java.io.File;

import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.wrapper.ApiFileDetail;
import org.megapractical.invoicing.api.wrapper.ApiPathDetail;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.payload.UploadFilePath;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class FilePathUtils {

	private FilePathUtils() {
	}

	public static UploadFilePath uploadPath(String rfc, ApiFileDetail fileDetails, String pathBase) {
		val path = rfc + File.separator + UDate.date().replace("-", "") + File.separator + fileDetails.getFileName();
		// Directorio para el archivo actual
		val absolutePath = pathBase + File.separator + path;
		UFile.createDirectory(absolutePath);

		val uploadPath = absolutePath + File.separator + fileDetails.getFileName() + "." + fileDetails.getFileExt();
		return UploadFilePath.builder().absolutePath(absolutePath).uploadPath(uploadPath).dbPath(path).build();
	}

	public static ApiPathDetail configurePaths(String filePath, String fileName, boolean createSeparate) {
		return configurePaths(filePath, fileName, null, createSeparate);
	}
	
	public static ApiPathDetail configurePaths(String filePath, String fileName, String dbPath, boolean createSeparate) {
		String xmlFolder = null;
		String xmlDb = null;
		String pdfFolder = null;
		String pdfDb = null;

		if (createSeparate) {
			xmlFolder = filePath + File.separator + fileName + File.separator + "XML";
			xmlDb = dbPath != null ? dbPath + File.separator + fileName + File.separator + "XML" : null;

			val xmlAbsolutePath = new File(xmlFolder);
			if (!xmlAbsolutePath.exists()) {
				xmlAbsolutePath.mkdirs();
			}

			pdfFolder = filePath + File.separator + fileName + File.separator + "PDF";
			pdfDb = dbPath != null ? dbPath + File.separator + fileName + File.separator + "PDF" : null;

			val pdfAbsolutePath = new File(pdfFolder);
			if (!pdfAbsolutePath.exists()) {
				pdfAbsolutePath.mkdirs();
			}
		} else {
			val xmlPdf = filePath + File.separator + fileName;
			
			if (dbPath != null) {
				xmlDb = dbPath + File.separator + fileName;
				pdfDb = dbPath + File.separator + fileName;
			}

			val paymentXmlPdfAbsolutePath = new File(xmlPdf);
			if (!paymentXmlPdfAbsolutePath.exists()) {
				paymentXmlPdfAbsolutePath.mkdirs();
			}

			xmlFolder = xmlPdf;
			pdfFolder = xmlPdf;
		}

		return ApiPathDetail.builder().xmlPath(xmlFolder).xmlDb(xmlDb).pdfPath(pdfFolder).pdfDb(pdfDb).build();
	}

}