package org.megapractical.invoicing.webapp.util;

public final class VoucherUtils {
	
	private VoucherUtils() {		
	}
	
	public static final String VOUCHER_VERSION_DEFAULT = "4.0";
	
	public static final String VOUCHER_TYPE_CODE_I = "I";
	
	public static final String VOUCHER_TYPE_CODE_E = "E";
	
	public static final String VOUCHER_TYPE_CODE_N = "N";
	
	public static final String VOUCHER_TYPE_CODE_P = "P";
	
	public static final String VOUCHER_TYPE_CODE_T = "T";
	
	/**
	 * Expresion regular para validar el tipo de cambio
	 */
	public static final String CHANGE_TYPE_EXPRESSION = "[0-9]{1,18}(.[0-9]{1,6})?";
	
}