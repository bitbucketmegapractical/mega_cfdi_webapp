package org.megapractical.invoicing.webapp.exposition.sftp.file;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
public class SftpFileDownloadException extends RuntimeException {

	public SftpFileDownloadException(String message) {
		super(message);
	}

}