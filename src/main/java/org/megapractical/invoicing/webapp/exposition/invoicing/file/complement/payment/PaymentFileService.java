package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import java.util.List;
import java.util.Map;

import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentBaseTax;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentRelatedDocument;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentWithheld;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPaymentTotal;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPaymentTotal;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment.WRelatedDocument;
import org.megapractical.invoicing.webapp.wrapper.WTaxes;

public interface PaymentFileService {

	void populatePayment(String paymentId, ComplementPayment cmpPayment, Map<String, WPayment> paymentsMap);

	void populatePaymentTransferreds(String paymentId, ComplementPaymentBaseTax itax,
			Map<String, List<WTaxes>> paymentTransferreds);

	void populatePaymentWithhelds(String paymentId, ComplementPaymentWithheld itax,
			Map<String, List<WTaxes>> paymentWithhelds);

	void populateRelatedDocument(String paymentId, ComplementPaymentRelatedDocument data,
			Map<String, List<WRelatedDocument>> relatedDocuments);

	void populateRelDocTransferreds(String docId, ComplementPaymentBaseTax itax,
			Map<String, List<WTaxes>> relDocTransferreds);

	void populateRelDocWithhelds(String docId, ComplementPaymentBaseTax itax,
			Map<String, List<WTaxes>> relDocWithhelds);

	WPaymentTotal paymentTotal(ComplementPaymentTotal complementPaymentTotal);

}