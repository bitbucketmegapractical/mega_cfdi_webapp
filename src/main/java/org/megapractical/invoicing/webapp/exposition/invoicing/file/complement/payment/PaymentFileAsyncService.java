package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import java.util.concurrent.Future;

import org.megapractical.invoicing.api.wrapper.ApiPaymentStamp;

public interface PaymentFileAsyncService {
	
	Future<Boolean> processFileAsync(ApiPaymentStamp paymentStamp);
	
}