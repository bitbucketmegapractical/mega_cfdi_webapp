package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.util.List;

public interface InvoiceAutocompleteService {
	
	List<String> taxRegimeSource();
	
	List<String> cfdiUseSource();
	
	List<String> residencySource();
	
	List<String> measurementUnitSource();
	
	List<String> keyProductServiceSource();
	
	List<String> voucherTypeSource();
	
	List<String> expeditionPlaceSource();
	
	List<String> paymentWaySource();
	
	List<String> paymentMethodSource();
	
	List<String> currencySource();
	
	List<String> stringTypeSource();
	
	List<String> exportationSource();
	
}