package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyValidatorResponse {
	private MonedaEntity currency;
	private JError error;
	private boolean hasError;
	
	public CurrencyValidatorResponse(MonedaEntity currency) {
		this.currency = currency;
		this.hasError = Boolean.FALSE;
	}
	
	public CurrencyValidatorResponse(MonedaEntity currency, JError error) {
		this.currency = currency;
		this.error = error;
		this.hasError = error != null;
	}
	
	public CurrencyValidatorResponse(MonedaEntity currency, boolean hasError) {
		this.currency = currency;
		this.hasError = hasError;
	}
	
}