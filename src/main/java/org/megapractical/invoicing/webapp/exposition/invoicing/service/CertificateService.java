package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;

public interface CertificateService {
	
	ContribuyenteCertificadoEntity findByTaxpayer(ContribuyenteEntity taxpayer);
	
}