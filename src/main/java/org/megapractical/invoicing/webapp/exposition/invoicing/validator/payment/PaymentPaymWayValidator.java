package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentPaymWayValidator {
	
	FormaPagoEntity getPaymentWay(String paymentWay);
	
	FormaPagoEntity getPaymentWay(String paymentWay, String field, List<JError> errors);
	
}