package org.megapractical.invoicing.webapp.exposition.cancellation;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.smarterweb.SWCancellationClient;
import org.megapractical.invoicing.api.smarterweb.SWCancellationResponse;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UZipper;
import org.megapractical.invoicing.api.wrapper.ApiCancellation;
import org.megapractical.invoicing.api.wrapper.ApiCancellationRequest;
import org.megapractical.invoicing.api.wrapper.ApiFileDetail;
import org.megapractical.invoicing.dal.bean.datatype.CancellationType;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoCancelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaEntity;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.payload.UploadFilePath;
import org.megapractical.invoicing.webapp.exposition.service.CfdiService;
import org.megapractical.invoicing.webapp.exposition.service.PayrollService;
import org.megapractical.invoicing.webapp.exposition.service.nomenclator.FileStatusService;
import org.megapractical.invoicing.webapp.util.FilePathUtils;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
class CancellationProcessingService {
	
	private final AppSettings appSettings;
	private final FileStatusService fileStatusService;
	private final CancellationFileService cancellationFileService;
	private final CfdiService cfdiService;
	private final PayrollService payrollService;
	private final CancellationLogService cancellationLogService;
	private final CancellationResponseCodeService cancellationResponseCodeService;
	private final CancellationNotificationService cancellationNotificationService;
	
	public void processingFile(CancellationFileData cancellationFileData) {
		// Actualizando estado del archivo
		updateCancellationFile(cancellationFileData, FileStatus.PROCESSING);
		
		// Parseando el fichero
		val uploadFilePath = cancellationFileData.getUploadFilePath();
		UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > PARSING " + uploadFilePath);
		val cancellationParser = CancellationFileParserService.parseFile(uploadFilePath.getUploadPath());
		
		// Obteniendo lista de cfdi/ret a cancelar
		val cancellationRequests = cancellationParser.getCancellationRequests();
		UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > TOTAL UUID TO CANCELL " + cancellationRequests.size());
		val cfdiCancellationRequests = filterCancellationRequests(cancellationRequests, CancellationType.CFDI);
		val retCancellationRequests = filterCancellationRequests(cancellationRequests, CancellationType.RET);
		val recipients = cancellationParser.getRecipients();
		val errors = cancellationParser.getParserErrors();
		
		// Datos del emisor/certificado
		val cancellationData = populateCancellationData(cancellationFileData);
		
		// Listado de cancelaciones
		val uuidCancellations = new ArrayList<SWCancellationResponse>();
		val retCancellations = new ArrayList<SWCancellationResponse>();
		
		// Fake responses
		val useFakeCancellationService = appSettings.getBooleanValue("cfdi.use.fake.cancellation.service");
		val useFakeCheckStatus = appSettings.getBooleanValue("cfdi.use.fake.check.status");
		UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > USING FAKE CANCELLATION SERVICE: " + useFakeCancellationService);
		UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > USING FAKE CHECK STATUS: " + useFakeCheckStatus);
		
		// Cancelando Cfdis
		cfdiCancellationRequests.forEach(cancellationRequest -> {
			populateCancellationRequest(cancellationData, cancellationRequest, useFakeCheckStatus);
			UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > VERIFYING UUID " + cancellationRequest.getUuid());
			val allowedToCancellation = verifyUuid(cancellationRequest, errors);
			if (allowedToCancellation) {
				SWCancellationResponse cancellationResponse;
				if (!useFakeCancellationService) {
					UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > CANCELLING UUID " + cancellationRequest.getUuid());
					cancellationResponse = SWCancellationClient.cancellationRequest(cancellationRequest);
				} else {
					UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > CANCELLING UUID " + cancellationRequest.getUuid() + " USING FAKE CANCELLATION SERVICE");
					cancellationResponse = CancellationFakeService.cfdiCancellationFake(cancellationRequest);
				}
				
				UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > CANCELLATION RESPONSE " + cancellationResponse);
				if (cancellationResponse.isSuccess()) {
					uuidCancellations.add(cancellationResponse);
				} else {
					errors.add(CancellationErrorWritter.populateError(cancellationResponse));
				}
			}
		});
		
		retCancellationRequests.forEach(cancellationRequest -> {
			throw new RuntimeException("Not implemented!");
		});
		
		// Acuses de cancelacion de los cfdis
		cfdiCancellation(cancellationFileData, uuidCancellations);
		// Acuses de cancelacion de las retenciones
		retCancellation(cancellationFileData, retCancellations);
		// Resumen de cancelacion
		cancellationSummary(cancellationFileData, uuidCancellations, retCancellations, errors);
		// Compactando los acuses
		zippingAcuses(cancellationFileData, uuidCancellations, retCancellations);
		// Posibles errores
		verifyErrors(cancellationFileData, errors);
		// Actualizando estado del archivo
		updateCancellationFile(cancellationFileData, FileStatus.GENERATED);
		// Enviando notificacion
		cancellationNotification(cancellationFileData, recipients);
		// Procesar archivos en cola
		processingQueuedFiles(cancellationFileData);
	}
	
	private List<ApiCancellationRequest> filterCancellationRequests(List<ApiCancellationRequest> cancellationRequests,
																	CancellationType type) {
		return cancellationRequests.stream().filter(i -> i.getType().equals(type)).collect(toList());
	}
	
	private ApiCancellation populateCancellationData(CancellationFileData cancellationFileData) {
		return ApiCancellation
				.builder()
				.emitterRfc(cancellationFileData.getTaxpayer().getRfcActivo())
				.b64Cer(UBase64.base64Encode(cancellationFileData.getCertificate().getCertificado()))
				.b64Key(UBase64.base64Encode(cancellationFileData.getCertificate().getLlavePrivada()))
				.password(UBase64.base64Decode(cancellationFileData.getCertificate().getClavePrivada()))
				.environment(appSettings.getEnvironment())
				.build();
	}
	
	private void populateCancellationRequest(ApiCancellation cancellationData,
											 final ApiCancellationRequest cancellationRequest,
											 boolean useCheckStatusFake) {
		cancellationRequest.setEmitterRfc(cancellationData.getEmitterRfc());
		cancellationRequest.setB64Cer(cancellationData.getB64Cer());
		cancellationRequest.setB64Key(cancellationData.getB64Key());
		cancellationRequest.setPassword(cancellationData.getPassword());
		cancellationRequest.setEnvironment(cancellationData.getEnvironment());
		cancellationRequest.setUseCheckStatusFake(useCheckStatusFake);
	}
	
	private boolean verifyUuid(ApiCancellationRequest cancellationRequest, final List<String> errors) {
		val emitterRfc = cancellationRequest.getEmitterRfc();
		val receiverRfc = cancellationRequest.getReceiverRfc();
		val uuid = cancellationRequest.getUuid();
		String errorMessage = null;
		
		CfdiEntity cfdi = null;
		NominaEntity payroll = null;
		var isCancel = false;
		var isCancellInProcess = false;
		var isCancellRequest = false;
		
		cfdi = cfdiService.findForCancellation(emitterRfc, receiverRfc, uuid);
		if (cfdi != null) {
			UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > FOUND CFDI WITH UUID " + uuid);
			isCancel = cfdi.isCancelado();
			isCancellInProcess = cfdi.isCancelacionEnProceso();
			isCancellRequest = cfdi.isSolicitudCancelacion();
		} else {
			payroll = payrollService.findForCancellation(emitterRfc, uuid);
			if (payroll != null) {
				UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > FOUND PAYROLL WITH UUID " + uuid);
				isCancel = payroll.isCancelado();
				isCancellInProcess = payroll.isCancelacionEnProceso();
				isCancellRequest = payroll.isSolicitudCancelacion();
			}
		}
		
		if (AssertUtils.nonNull(cfdi) || AssertUtils.nonNull(payroll)) {
			if (!isCancel && !isCancellInProcess && !isCancellRequest) {
				UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > UUID " + uuid + " IS CANCELLABLE");
				return true;
			} else {
				errorMessage = CancellationErrorWritter.uuidCancelledError(uuid);
				UPrint.logWithLine(UProperties.env(), "[ERROR] CANCELLATION PROCESSING SERVICE > UUID " + uuid + " HAS BEEN PREVIOUSLY CANCELED OR IS IN THE PROCESS OF BEING CANCELED");
			}
		} else {
			errorMessage = CancellationErrorWritter.uuidNotFoundError(uuid);
			UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > UUID " + uuid + " DOESN'T EXIST");
		}
		
		if (AssertUtils.hasValue(errorMessage)) {
			errors.add(errorMessage);
		}
		return false;
	}
	
	private void cfdiCancellation(CancellationFileData cancellationFileData, List<SWCancellationResponse> cancellations) {
		cancellations.forEach(item -> {
			val cfdi = cfdiService.findByUuid(item.getUuid());
			if (cfdi != null) {
				cfdi.setCancelado(true);
				cfdi.setAcuseCancelacion(item.getAcuseXml());
				cfdi.setFechaCancelado(item.getCancellationDate());
				cfdi.setHoraCancelado(item.getCancellationTime());
				cfdiService.save(cfdi);
			} else {
				val payroll = payrollService.findByUuid(item.getUuid());
				if (payroll != null) {
					payroll.setCancelado(true);
					payroll.setAcuseCancelacion(item.getAcuseXml());
					payroll.setFechaCancelado(item.getCancellationDate());
					payroll.setHoraCancelado(item.getCancellationTime());
					payrollService.save(payroll);
				}
			}
			
			cancellationAcuseStorage(cancellationFileData, item);
			cancellationLogService.cancellationLog(item, cancellationFileData, CancellationType.CFDI);
		});
	}
	
	private void retCancellation(CancellationFileData cancellationFileData,
								 List<SWCancellationResponse> retCancellations) {
		if (AssertUtils.nonNull(retCancellations)) {
			throw new RuntimeException("Not implemented!");
		}
	}
	
	private void cancellationAcuseStorage(CancellationFileData cancellationFileData,
										  SWCancellationResponse cancellationResponse) {
		if (AssertUtils.hasValue(cancellationResponse.getAcuseXml())) {
			val acusesPath = cancellationFileData.getAcusesPath();
			val cancellationAcusesPath = acusesPath + File.separator + cancellationResponse.getUuid() + ".xml";
			CancellationAcuseWritter.writeAcuse(cancellationAcusesPath, cancellationResponse.getAcuseXml());
		}
	}
	
	private void cancellationSummary(CancellationFileData cancellationFileData,
		 	   						 List<SWCancellationResponse> cfdiCancellations, 
		 	   						 List<SWCancellationResponse> retCancellations,
		 	   						 List<String> errors) {
		if (AssertUtils.nonNull(cfdiCancellations) || AssertUtils.nonNull(retCancellations)
				|| AssertUtils.nonNull(errors)) {
			val cancellationSummary = new ArrayList<String>();
			
			Stream.concat(cfdiCancellations.stream(), retCancellations.stream())
				  .map(cancellationResponseCodeService::details)
				  .forEach(cancellationSummary::add);
			
			errors.forEach(cancellationSummary::add);
			
			val acusesPath = cancellationFileData.getAcusesPath();
			val cancellationSummaryPath = acusesPath + File.separator + "ResumenCancelacion" + ".txt";
			CancellationSummaryWritter.writeSummary(cancellationSummaryPath, cancellationSummary);
		}
	}
	
	private void zippingAcuses(CancellationFileData cancellationFileData,
						 	   List<SWCancellationResponse> cfdiCancellations, 
						 	   List<SWCancellationResponse> retCancellations) {
		if (AssertUtils.nonNull(cfdiCancellations) || AssertUtils.nonNull(retCancellations)) {
			val dbPath = cancellationFileData.getUploadFilePath().getDbPath();
			val cancellationFileName = cancellationFileData.getCancellationFile().getNombre();
			val zipDirectory = cancellationFileData.getAcusesPath();
			val zipFile = zipDirectory + ".zip";
			val zipDbPath = dbPath + File.separator + cancellationFileName + ".zip";

			try {
				UPrint.logWithLine(UProperties.env(),
						"[INFO] CANCELLATION PROCESSING SERVICE > ZIPPING DIRECTORY: " + zipDirectory);

				UZipper.zipping(new File(zipDirectory), zipFile);
				// Actualizando ruta de compactado
				cancellationFileData.getCancellationFile().setRutaAcuses(zipDbPath);

				UPrint.logWithLine(UProperties.env(), "[INFO] CANCELLATION PROCESSING SERVICE > ZIPPING DIRECTORY FINISHED");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void verifyErrors(CancellationFileData cancellationFileData, List<String> errors) {
		if (AssertUtils.nonNull(errors)) {
			val folderAbsolutePath = cancellationFileData.getUploadFilePath().getAbsolutePath(); 
			val dbPath = cancellationFileData.getUploadFilePath().getDbPath();
			
			// Generando ruta de almacenamiento del error
			val errorPath = folderAbsolutePath + File.separator + "ERROR";
			UFile.createDirectory(errorPath);

			// Fichero de error a generar
			val cancellationFile = cancellationFileData.getCancellationFile();
			val fileErrorPath = errorPath + File.separator + cancellationFile.getNombre() + "_ERROR.txt";

			// Generando fichero de error
			CancellationErrorWritter.writeError(fileErrorPath, errors);

			// Actualizando la ruta de error en el archivo
			val errorDbPath = dbPath + File.separator + "ERROR" + File.separator + cancellationFile.getNombre()
					+ "_ERROR.txt";
			cancellationFile.setRutaError(errorDbPath);
		}
	}
	
	private void updateCancellationFile(CancellationFileData cancellationFileData, FileStatus fileStatus) {
		// Cambiando estado del archivo
		val status = fileStatusService.findByCode(fileStatus);
		cancellationFileData.getCancellationFile().setEstadoArchivo(status);
		cancellationFileService.save(cancellationFileData.getCancellationFile());
	}
	
	private void cancellationNotification(CancellationFileData cancellationFileData, List<String> recipients) {
		val cancellationNotification = CancellationNotification
										.builder()
										.taxpayer(cancellationFileData.getTaxpayer())
										.cancellationFile(cancellationFileData.getCancellationFile())
										.fileName(cancellationFileData.getFileDetail().getOriginalFilename())
										.recipients(recipients)
										.subject(CancellationConstants.NOTIF_SUBJECT)
										.build();
		cancellationNotificationService.sendNotification(cancellationNotification);
	}
	
	private void processingQueuedFiles(CancellationFileData cancellationFileData) {
		val cancellationRepository = appSettings.getPropertyValue(CancellationConstants.CANCELLATION_PATH);
		val taxpayer = cancellationFileData.getTaxpayer();
		val cancellationQueuedFiles = cancellationFileService.findQueuedFiles(taxpayer);
		if (AssertUtils.nonNull(cancellationQueuedFiles)) {
			val cancellationFile = cancellationQueuedFiles.get(0);
			val fileDetails = loadFileDetails(cancellationFile);
			if (AssertUtils.nonNull(fileDetails)) {
				val uploadFilePath = FilePathUtils.uploadPath(taxpayer.getRfcActivo(), fileDetails, cancellationRepository);
				val acusesPath = CancellationHelper.createAcusesDirectory(cancellationFile, uploadFilePath);
				updateQueuedFile(cancellationFileData, cancellationFile, uploadFilePath, fileDetails, acusesPath);
				processingFile(cancellationFileData);
			}
		}
	}

	private void updateQueuedFile(final CancellationFileData cancellationFileData,
								  ArchivoCancelacionEntity cancellationFile,
								  UploadFilePath uploadFilePath,
								  ApiFileDetail fileDetails,
								  String acusesPath) {
		cancellationFileData.setCancellationFile(cancellationFile);
		cancellationFileData.setUploadFilePath(uploadFilePath);
		cancellationFileData.setFileDetail(fileDetails);
		cancellationFileData.setAcusesPath(acusesPath);
	}
	
	private ApiFileDetail loadFileDetails(ArchivoCancelacionEntity cancellationFile) {
		try {
			val file = new File(cancellationFile.getRuta());
			return UFile.fileDetails(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}