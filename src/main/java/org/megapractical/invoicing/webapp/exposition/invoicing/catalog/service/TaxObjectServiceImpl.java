package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service;

import org.megapractical.invoicing.dal.bean.jpa.ObjetoImpuestoEntity;
import org.megapractical.invoicing.dal.data.repository.IObjetoImpuestoJpaRepository;
import org.springframework.stereotype.Service;

@Service
public class TaxObjectServiceImpl implements TaxObjectService {

	private final IObjetoImpuestoJpaRepository iObjetoImpuestoJpaRepository;

	public TaxObjectServiceImpl(IObjetoImpuestoJpaRepository iObjetoImpuestoJpaRepository) {
		this.iObjetoImpuestoJpaRepository = iObjetoImpuestoJpaRepository;
	}

	@Override
	public ObjetoImpuestoEntity findByCode(String code) {
		return iObjetoImpuestoJpaRepository.findByCodigoAndEliminadoFalse(code);
	}

	@Override
	public ObjetoImpuestoEntity findByValue(String value) {
		return iObjetoImpuestoJpaRepository.findByValorAndEliminadoFalse(value);
	}

	@Override
	public String description(ObjetoImpuestoEntity entity) {
		if (entity != null) {
			return String.format("%s (%s)", entity.getCodigo(), entity.getValor());
		}
		return null;
	}

}