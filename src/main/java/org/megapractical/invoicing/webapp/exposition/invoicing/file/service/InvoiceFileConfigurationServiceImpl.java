package org.megapractical.invoicing.webapp.exposition.invoicing.file.service;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.IInvoiceFileConfigurationJpaRepository;
import org.springframework.stereotype.Service;

@Service
public class InvoiceFileConfigurationServiceImpl implements InvoiceFileConfigurationService {

	private final IInvoiceFileConfigurationJpaRepository iInvoiceFileConfigurationJpaRepository;
	
	public InvoiceFileConfigurationServiceImpl(
			IInvoiceFileConfigurationJpaRepository iInvoiceFileConfigurationJpaRepository) {
		this.iInvoiceFileConfigurationJpaRepository = iInvoiceFileConfigurationJpaRepository;
	}

	@Override
	public ArchivoFacturaConfiguracionEntity findByTaxpayer(ContribuyenteEntity taxpayer) {
		return iInvoiceFileConfigurationJpaRepository.findByContribuyente(taxpayer);
	}

	@Override
	public ArchivoFacturaConfiguracionEntity save(ArchivoFacturaConfiguracionEntity entity) {
		if (entity != null) {
			return iInvoiceFileConfigurationJpaRepository.save(entity);
		}
		return null;
	}

}