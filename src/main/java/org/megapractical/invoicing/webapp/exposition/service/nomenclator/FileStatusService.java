package org.megapractical.invoicing.webapp.exposition.service.nomenclator;

import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.megapractical.invoicing.webapp.util.FileStatus;

public interface FileStatusService {
	
	EstadoArchivoEntity findByCode(FileStatus code);
	
}