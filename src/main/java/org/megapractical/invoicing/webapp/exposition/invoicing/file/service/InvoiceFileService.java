package org.megapractical.invoicing.webapp.exposition.invoicing.file.service;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface InvoiceFileService {
	
	List<ArchivoFacturaEntity> findQueuedFiles(ContribuyenteEntity taxpayer);
	
	List<ArchivoFacturaEntity> findByTaxpayerAndStatus(ContribuyenteEntity taxpayer, FileStatus status);
	
	Page<ArchivoFacturaEntity> getFiles(ContribuyenteEntity taxpayer, String name, FileStatus status, String fileType, Pageable pageable);
	
	List<ArchivoFacturaEntity> getFiles(ContribuyenteEntity taxpayer, FileStatus status, String fileType);
	
	ArchivoFacturaEntity findByTaxpayerAndName(ContribuyenteEntity taxpayer, String fileName);
	
	ArchivoFacturaEntity save(ArchivoFacturaEntity entity);
	
	ArchivoFacturaEntity findOne(Long id, FileStatus status);
	
	void delete(Long id);
	
}