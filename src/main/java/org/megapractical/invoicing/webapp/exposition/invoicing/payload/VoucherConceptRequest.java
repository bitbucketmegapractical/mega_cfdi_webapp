package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.wrapper.WConcept;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoucherConceptRequest {
	private List<WConcept> wConcepts;
	private MonedaEntity currency;
}