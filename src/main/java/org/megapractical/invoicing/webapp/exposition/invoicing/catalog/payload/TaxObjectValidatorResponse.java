package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.dal.bean.jpa.ObjetoImpuestoEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaxObjectValidatorResponse {
	private ObjetoImpuestoEntity taxObject;
	private JError error;
	private boolean hasError;
	
	public TaxObjectValidatorResponse(ObjetoImpuestoEntity taxObject) {
		this.taxObject = taxObject;
		this.hasError = Boolean.FALSE;
	}
	
	public TaxObjectValidatorResponse(ObjetoImpuestoEntity taxObject, JError error) {
		this.taxObject = taxObject;
		this.error = error;
		this.hasError = error != null;
	}
	
	public TaxObjectValidatorResponse(ObjetoImpuestoEntity taxObject, boolean hasError) {
		this.taxObject = taxObject;
		this.hasError = hasError;
	}
	
}