package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher.JVoucherResponse;

public interface VoucherValidator {
	
	JVoucherResponse getVoucher(String voucherData, String relatedCfdiCheck, ContribuyenteEntity taxpayer, List<JError> errors);
	
}