package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.datatype.StorageType;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.s3.S3Service;
import org.megapractical.invoicing.webapp.json.JCfdiNotification;
import org.megapractical.invoicing.webapp.json.JCfdiNotification.CfdiNotification;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WCfdiNotification;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class CfdiNotificationService {
	
	private final AppSettings appSettings;
	private final UserTools userTools;
	private final CfdiService cfdiService;
	private final S3Service s3Service;
	private final MailService mailService;
	
	private static final String ERROR_MESSAGE = "Ha ocurrido un error inesperado enviando el correo electrónico";
	
	public void sendNotification(final JCfdiNotification cfdiNotificationJson) {
		JResponse response = null;
		String errorMessage = null;
		try {
			if (!UValidator.isNullOrEmpty(cfdiNotificationJson)) {
				val cfdiNotification = cfdiNotificationJson.getCfdiNotification();
				val uuid = UBase64.base64Decode(cfdiNotification.getUuid());
				val cfdiEntity = cfdiService.findByUuid(uuid);
				if (cfdiEntity != null) {
					val wCfdiNotification = populateNotification(cfdiNotification, uuid, cfdiEntity);
					if (mailService.sendCfdiToRecipients(wCfdiNotification)) {
						val toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.email.send.title", userTools.getLocale());
						val toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.email.send.message", userTools.getLocale());
						val toastr = Toastr.success(toastrTitle, toastrMessage);
						
						val notification = JNotification.toastrNotification(toastr);
						response = JResponse.success(notification);
					} else {
						errorMessage = ERROR_MESSAGE;
					}
				} else {
					errorMessage = "CFDI no encontrado";
				}
			} else {
				errorMessage = ERROR_MESSAGE;
			}
		} catch (Exception e) {
			e.printStackTrace();
			errorMessage = ERROR_MESSAGE;
		}
		
		if (errorMessage != null) {
			val notification = JNotification.pageMessageError(errorMessage);
			response = JResponse.error(notification);
		}
		cfdiNotificationJson.setResponse(response);
	}

	private WCfdiNotification populateNotification(CfdiNotification cfdiNotification, String uuid,
			CfdiEntity cfdiEntity) throws IOException {
		String xmlName = null;
		String pdfName = null;
		InputStream xmlInputStream = null;
		InputStream pdfInputStream = null;
		
		val storage = cfdiEntity.getStorage();
		if (StorageType.SERVER.equals(storage)) {
			val repositoryPath = appSettings.getPropertyValue("cfdi.repository");
			val absolutePath = repositoryPath + File.separator;
			
			xmlName = uuid + ".xml";
			xmlInputStream = loadXmlFromServer(cfdiEntity, xmlInputStream, absolutePath);
			pdfName = uuid + ".pdf";
			pdfInputStream = loadPdfFromServer(cfdiEntity, pdfInputStream, absolutePath);
		} else if (StorageType.S3.equals(storage)) {
			xmlName = cfdiEntity.getXmlFileNameFromS3();
			xmlInputStream = downloadXmlFromS3(cfdiEntity);
			pdfName = cfdiEntity.getPdfFileNameFromS3();
			pdfInputStream = downloadPdfFromS3(cfdiEntity);
		}
		
		val recipientTo = UBase64.base64Decode(cfdiNotification.getRecipientTo());
		
		val recipients = new ArrayList<String>();
		recipients.add(recipientTo);
		
		if (!UValidator.isNullOrEmpty(cfdiNotification.getRecipients())) {
			cfdiNotification.getRecipients().stream()
											.map(UBase64::base64Decode)
											.filter(email -> !recipientTo.equalsIgnoreCase(email))
											.forEach(recipients::add);
		}
		
		val wCfdiNotification = new WCfdiNotification();
		wCfdiNotification.setUuid(uuid);
		wCfdiNotification.setRecipients(recipients);
		wCfdiNotification.setXmlName(xmlName);
		wCfdiNotification.setXmlInputStream(xmlInputStream);
		wCfdiNotification.setPdfName(pdfName);
		wCfdiNotification.setPdfInputStream(pdfInputStream);
		wCfdiNotification.setEmitterRfc(cfdiEntity.getContribuyente().getRfc());
		wCfdiNotification.setEmitterName(cfdiEntity.getContribuyente().getNombreRazonSocial());
		wCfdiNotification.setReceiverName(cfdiEntity.getReceptorRazonSocial());
		wCfdiNotification.setReceiverRfc(cfdiEntity.getReceptorRfc());
		return wCfdiNotification;
	}

	private InputStream loadXmlFromServer(CfdiEntity cfdiEntity, InputStream xmlInputStream, String absolutePath)
			throws FileNotFoundException {
		if (!UValidator.isNullOrEmpty(cfdiEntity.getRutaXml())) {
			val xmlPath = absolutePath + cfdiEntity.getRutaXml();
			xmlInputStream = new FileInputStream(new File(xmlPath));
		}
		return xmlInputStream;
	}
	
	private InputStream loadPdfFromServer(CfdiEntity cfdiEntity, InputStream pdfInputStream, String absolutePath)
			throws FileNotFoundException {
		if (!UValidator.isNullOrEmpty(cfdiEntity.getRutaPdf())) {
			val pdfPath = absolutePath + cfdiEntity.getRutaPdf();
			pdfInputStream = new FileInputStream(new File(pdfPath));
		}
		return pdfInputStream;
	}

	private InputStream downloadXmlFromS3(CfdiEntity cfdiEntity) throws IOException, FileNotFoundException {
		val s3XmlPath = cfdiEntity.getRutaXml();
		val xmlFileName = cfdiEntity.getXmlFileNameFromS3();
		val xmlFileByte = s3Service.getObject(s3XmlPath);
		val s3PdfFileTmp = UFile.byteToFile(xmlFileName, xmlFileByte);
		return new FileInputStream(s3PdfFileTmp);
	}
	
	private InputStream downloadPdfFromS3(CfdiEntity cfdiEntity) throws IOException, FileNotFoundException {
		val s3PdfPath = cfdiEntity.getRutaPdf();
		val pdfFileName = cfdiEntity.getPdfFileNameFromS3();
		val pdfFileByte = s3Service.getObject(s3PdfPath);
		val s3PdfFileTmp = UFile.byteToFile(pdfFileName, pdfFileByte);
		return new FileInputStream(s3PdfFileTmp);
	}
	
}