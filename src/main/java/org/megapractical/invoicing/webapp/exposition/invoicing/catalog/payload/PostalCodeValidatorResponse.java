package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostalCodeValidatorResponse {
	private CodigoPostalEntity postalCode;
	private JError error;
	private boolean hasError;
	
	public PostalCodeValidatorResponse(CodigoPostalEntity postalCode) {
		this.postalCode = postalCode;
		this.hasError = Boolean.FALSE;
	}
	
	public PostalCodeValidatorResponse(CodigoPostalEntity postalCode, JError error) {
		this.postalCode = postalCode;
		this.error = error;
		this.hasError = error != null;
	}
	
	public PostalCodeValidatorResponse(CodigoPostalEntity postalCode, boolean hasError) {
		this.postalCode = postalCode;
		this.hasError = hasError;
	}
	
}