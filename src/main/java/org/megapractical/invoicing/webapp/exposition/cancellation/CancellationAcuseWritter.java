package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.io.FileWriter;

import org.megapractical.invoicing.api.util.UFile;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
final class CancellationAcuseWritter {
	
	private CancellationAcuseWritter() {		
	}
	
	public static void writeAcuse(String cancellationAcusePath, String cancellationAcuseXml) {
		try {
			val errorFile = UFile.createFileIfNotExist(cancellationAcusePath);
			if (errorFile != null) {
				try (val writer = new CSVWriter(new FileWriter(errorFile), '|', ICSVWriter.NO_QUOTE_CHARACTER,
						ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END)) {
					writer.writeNext(new String[] { cancellationAcuseXml });
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}