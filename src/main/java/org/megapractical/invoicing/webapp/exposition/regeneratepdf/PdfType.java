package org.megapractical.invoicing.webapp.exposition.regeneratepdf;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public enum PdfType {
	CFDI, PAYROLL, PAYMENT, RETENTION
}