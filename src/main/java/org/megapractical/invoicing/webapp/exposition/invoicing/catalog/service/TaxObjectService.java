package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service;

import org.megapractical.invoicing.dal.bean.jpa.ObjetoImpuestoEntity;

public interface TaxObjectService {
	
	ObjetoImpuestoEntity findByCode(String code);
	
	ObjetoImpuestoEntity findByValue(String value);
	
	String description(ObjetoImpuestoEntity entity);
	
}