package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service;

import org.megapractical.invoicing.dal.bean.jpa.ExportacionEntity;
import org.megapractical.invoicing.dal.data.repository.IExportationJpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ExportationServiceImpl implements ExportationService {

	private final IExportationJpaRepository iExportationJpaRepository;

	public ExportationServiceImpl(IExportationJpaRepository iExportationJpaRepository) {
		this.iExportationJpaRepository = iExportationJpaRepository;
	}

	@Override
	public ExportacionEntity findByCode(String code) {
		return iExportationJpaRepository.findByCodigoAndEliminadoFalse(code);
	}

	@Override
	public ExportacionEntity findByValue(String value) {
		return iExportationJpaRepository.findByValorAndEliminadoFalse(value);
	}

	@Override
	public String description(ExportacionEntity entity) {
		if (entity != null) {
			return String.format("%s (%s)", entity.getCodigo(), entity.getValor());
		}
		return null;
	}

}