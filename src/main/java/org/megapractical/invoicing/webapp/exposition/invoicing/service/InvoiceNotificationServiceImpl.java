package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IContactJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICredentialJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.InvoiceNotificationRequest;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.wrapper.WCfdiNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class InvoiceNotificationServiceImpl implements InvoiceNotificationService {
	
	@Autowired
	private MailService mailService;
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ICredentialJpaRepository iCredentialJpaRepository;
	
	@Autowired
	private IAccountJpaRepository iAccountJpaRepository;
	
	@Autowired
	private IContactJpaRepository iContactJpaRepository;
	
	@Override
	public void sendNotification(InvoiceNotificationRequest notificationRequest) {
		val taxpayer = notificationRequest.getTaxpayer();
		
		String emitterEmail = null;
		val credential = iCredentialJpaRepository.findByContribuyenteAndEliminadoFalse(taxpayer);
		if (!UValidator.isNullOrEmpty(credential)) {
			emitterEmail = credential.getUsuario().getCorreoElectronico();
		} else {
			// Verificar si es una cuenta asociada
			val account = iAccountJpaRepository.findByContribuyenteAsociadoAndEliminadoFalse(taxpayer);
			if (!UValidator.isNullOrEmpty(account)) {
				emitterEmail = account.getCorreoElectronico();
			}
		}
		
		val contacts = iContactJpaRepository.findByContribuyenteAndEliminadoFalse(taxpayer);
		
		val cfdiNotification = new WCfdiNotification();
		cfdiNotification.setXmlPath(notificationRequest.getXmlPath());
		cfdiNotification.setPdfPath(notificationRequest.getPdfPath());
		cfdiNotification.setUuid(notificationRequest.getUuid());
		cfdiNotification.setCfdi(notificationRequest.getCfdiStored());
		cfdiNotification.setEmitterRfc(notificationRequest.getEmitterRfc());
		cfdiNotification.setEmitterName(taxpayer.getNombreRazonSocial());
		cfdiNotification.setEmitterEmail(emitterEmail);
		cfdiNotification.setReceiverRfc(notificationRequest.getReceiverRfc());
		cfdiNotification.setReceiverName(notificationRequest.getReceiverName());
		cfdiNotification.setReceiverEmail(notificationRequest.getReceiverEmail());
		cfdiNotification.getContacts().addAll(contacts);
		cfdiNotification.setPreferences(notificationRequest.getPreferences());
		
		userTools.log("SENDING EMAIL...");
		mailService.sendCfdi(cfdiNotification);
	}

}