package org.megapractical.invoicing.webapp.exposition.service;

import java.time.LocalDateTime;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.api.wrapper.ApiRetentionVoucher;
import org.megapractical.invoicing.dal.data.repository.IRetentionTaxTypeJpaRepository;
import org.megapractical.invoicing.sat.complement.dividendos.Dividendos;
import org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenaciondeAcciones;
import org.megapractical.invoicing.sat.complement.intereses.Intereses;
import org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.CPais;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros;
import org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones.CfdiRetenRelacionados;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
@Scope("session")
public class RetentionBuilderService {

	// Nueva retencion
	private Retenciones retencion;
	private Retenciones.Periodo periodo;
	private Retenciones.Emisor emisor;
	private Retenciones.Receptor receptor;
	private Retenciones.Receptor.Nacional receptorNacional;
	private Retenciones.Receptor.Extranjero receptorExtranjero;
	private Retenciones.Totales totales;
	private Retenciones.Totales.ImpRetenidos impuestosRetenidos;
	private Retenciones.Complemento complemento;

	// Complemento Dividendos
	private Dividendos dividendo;
	private Dividendos.DividOUtil dividendoUtilidad;
	private Dividendos.Remanente dividendoRemanente;

	// Complemento Enajenacion de acciones
	private EnajenaciondeAcciones enajenacion;

	// Complemento Intereses
	private Intereses intereses;

	// Complemento Operaciones con Derivados
	private OperacionesconDerivados operacionesDerivados;

	// Complemento Pagos a Extranjeros
	private PagosaExtranjeros pagosExtranjeros;
	private PagosaExtranjeros.Beneficiario beneficiario;
	private PagosaExtranjeros.NoBeneficiario noBeneficiario;

	// Complemento Sector Financiero
	private SectorFinanciero sectorFinanciero;

	@Resource
	private IRetentionTaxTypeJpaRepository iRetentionTaxTypeJpaRepository;

	public void retencionInstance() {
		try {

			// Nueva retencion
			retencion = new Retenciones();
			periodo = new Retenciones.Periodo();
			emisor = new Retenciones.Emisor();
			receptor = new Retenciones.Receptor();
			receptorNacional = new Retenciones.Receptor.Nacional();
			receptorExtranjero = new Retenciones.Receptor.Extranjero();
			totales = new Retenciones.Totales();
			impuestosRetenidos = new Retenciones.Totales.ImpRetenidos();
			complemento = new Retenciones.Complemento();

			// Complemento Dividendos
			dividendo = new Dividendos();
			dividendoUtilidad = new Dividendos.DividOUtil();
			dividendoRemanente = new Dividendos.Remanente();

			// Complemento Enajenacion de acciones
			enajenacion = new EnajenaciondeAcciones();

			// Complemento Intereses
			intereses = new Intereses();

			// Complemento Operaciones con Derivados
			operacionesDerivados = new OperacionesconDerivados();

			// Complemento Pagos a Extranjeros
			pagosExtranjeros = new PagosaExtranjeros();
			beneficiario = new PagosaExtranjeros.Beneficiario();
			noBeneficiario = new PagosaExtranjeros.NoBeneficiario();

			// Complemento Sector Financiero
			sectorFinanciero = new SectorFinanciero();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ApiRetentionVoucher retentionBuild(final ApiRetention apiRetention) {
		if (!UValidator.isNullOrEmpty(apiRetention)) {
			// Inicializando la retención
			retencionInstance();
			// Retencion
			fillRetention(apiRetention);
			// Periodo
			fillPeriodo(apiRetention);
			// Emisor
			fillEmitter(apiRetention);
			// Receptor
			fillReceiver(apiRetention);
			// Documentos relacionados
			fillRelatedDocument(apiRetention);
			// Impuestos
			fillTaxes(apiRetention);
			// Totales
			fillTotals(apiRetention);
			// Complemento Dividendos
			fillDividend(apiRetention);
			// Complemento Enajenación de acciones
			fillDisposalOfShares(apiRetention);
			// Complemento Intereses
			fillInterest(apiRetention);
			// Complemento Operaciones con derivados
			fillDerivativesOperations(apiRetention);
			// Complemento Pagos a extranjeros
			fillPaymentsToForeigners(apiRetention);
			// Complemento Sector financiero
			fillFinancialSector(apiRetention);
			
			if (!complemento.getAny().isEmpty()) {
				retencion.setComplemento(complemento);
			}

			return new ApiRetentionVoucher(retencion, apiRetention);
		}
		return null;
	}

	private void fillRetention(final ApiRetention apiRetention) {
		retencion.setVersion(apiRetention.getVersion());
		retencion.setFolioInt(apiRetention.getFolio());
		retencion.setCveRetenc(apiRetention.getRetentionKey());
		retencion.setFechaExp(UDateTime.format(LocalDateTime.now()));
		retencion.setLugarExpRetenc(apiRetention.getExpeditionPlace());
		
		if (!UValidator.isNullOrEmpty(apiRetention.getDescription())) {
			retencion.setDescRetenc(apiRetention.getDescription());
		}
	}

	private void fillFinancialSector(final ApiRetention apiRetention) {
		if (retencion.getCveRetenc().equals("25")) {
			sectorFinanciero.setVersion("1.0");
			sectorFinanciero.setIdFideicom(apiRetention.getFideicomId());
			sectorFinanciero.setNomFideicom(apiRetention.getFideicomName());
			sectorFinanciero.setDescripFideicom(apiRetention.getFideicomDescription());

			complemento.getAny().add(sectorFinanciero);
		}
	}

	private void fillPeriodo(final ApiRetention apiRetention) {
		periodo.setMesIni(apiRetention.getInitialMonth());
		periodo.setMesFin(apiRetention.getEndMonth());
		periodo.setEjercicio(apiRetention.getYear());
		retencion.setPeriodo(periodo);
	}

	private void fillEmitter(final ApiRetention apiRetention) {
		emisor.setRfcE(apiRetention.getEmitterRfc());
		emisor.setNomDenRazSocE(apiRetention.getEmitterBusinessName());
		emisor.setRegimenFiscalE(apiRetention.getEmitterTaxRegime());
		retencion.setEmisor(emisor);
	}

	private void fillReceiver(final ApiRetention apiRetention) {
		val receiverNationality = apiRetention.getReceiverNationality();
		if (receiverNationality.equalsIgnoreCase("nacional")) {
			receptorNacional.setRfcR(apiRetention.getReceiverNatRfc());
			receptorNacional.setNomDenRazSocR(apiRetention.getReceiverNatBusinessName());
			receptorNacional.setCurpR(apiRetention.getReceiverNatCurp());
			receptorNacional.setDomicilioFiscalR(apiRetention.getReceiverNatPostalCode());
			receptor.setNacional(receptorNacional);
		} else if (receiverNationality.equalsIgnoreCase("extranjero")) {// Receptor Extranjero
			receptorExtranjero.setNomDenRazSocR(apiRetention.getReceiverExtBusinessName());
			receptorExtranjero.setNumRegIdTribR(apiRetention.getReceiverExtNumber());
			receptor.setExtranjero(receptorExtranjero);
		}
		receptor.setNacionalidadR(receiverNationality);
		retencion.setReceptor(receptor);
	}

	private void fillRelatedDocument(ApiRetention apiRetention) {
		if (apiRetention.getRelatedDocumentType() != null && apiRetention.getRelatedDocumentUuid() != null) {
			val retRelatedCfdi = new CfdiRetenRelacionados();
			retRelatedCfdi.setTipoRelacion(apiRetention.getRelatedDocumentType());
			retRelatedCfdi.setUUID(apiRetention.getRelatedDocumentUuid());
			retencion.setCfdiRetenRelacionados(retRelatedCfdi);
		}
	}
	
	private void fillTaxes(final ApiRetention apiRetention) {
		if (apiRetention.getApiRetImpRetenidos() != null && !apiRetention.getApiRetImpRetenidos().isEmpty()) {
			apiRetention.getApiRetImpRetenidos().forEach(item -> {
				impuestosRetenidos = new Retenciones.Totales.ImpRetenidos();
				impuestosRetenidos.setBaseRet(UValue.bigDecimal(item.getBase(), 2));
				impuestosRetenidos.setImpuestoRet(item.getTax());
				impuestosRetenidos.setMontoRet(UValue.bigDecimal(item.getAmount(), 2));
				impuestosRetenidos.setTipoPagoRet(item.getPaymentType());
				totales.getImpRetenidos().add(impuestosRetenidos);
			});
		}
	}

	private void fillTotals(final ApiRetention apiRetention) {
		totales.setMontoTotOperacion(UValue.bigDecimal(apiRetention.getTotalOperationAmount()));
		totales.setMontoTotGrav(UValue.bigDecimal(apiRetention.getTotalAmountTaxed(), 2));
		totales.setMontoTotExent(UValue.bigDecimal(apiRetention.getTotalAmountExempt(), 2));
		totales.setMontoTotRet(UValue.bigDecimal(apiRetention.getTotalAmountWithheld(), 2));
		
		if (!UValidator.isNullOrEmpty(apiRetention.getQuarterlyProfit())) {
			totales.setUtilidadBimestral(UValue.bigDecimal(apiRetention.getQuarterlyProfit(), 2));
		}
		
		if (!UValidator.isNullOrEmpty(apiRetention.getCorrespondingISR())) {
			totales.setISRCorrespondiente(UValue.bigDecimal(apiRetention.getCorrespondingISR(), 2));
		}
		retencion.setTotales(totales);
	}

	private void fillDividend(final ApiRetention apiRetention) {
		if (retencion.getCveRetenc().equals("14")) {
			val dividendType = apiRetention.getCveTipDivOUtil();
			val dividendRemaining = apiRetention.getProporcionRem();
			if (!UValidator.isNullOrEmpty(dividendType) || !UValidator.isNullOrEmpty(dividendRemaining)) {
				dividendo.setVersion("1.0");

				if (!UValidator.isNullOrEmpty(dividendType)) {
					dividendoUtilidad.setCveTipDivOUtil(dividendType);

					val dividendAmountNat = apiRetention.getMontISRAcredRetMexico();
					dividendoUtilidad.setMontISRAcredRetMexico(UValue.bigDecimal(dividendAmountNat, 2));

					val dividendAmountExt = apiRetention.getMontISRAcredRetExtranjero();
					dividendoUtilidad.setMontISRAcredRetExtranjero(UValue.bigDecimal(dividendAmountExt, 2));

					val dividendAmountWithheldExt = apiRetention.getMontRetExtDivExt();
					if (!UValidator.isNullOrEmpty(dividendAmountWithheldExt)) {
						dividendoUtilidad.setMontRetExtDivExt(UValue.bigDecimal(dividendAmountWithheldExt));
					}

					dividendoUtilidad.setTipoSocDistrDiv(apiRetention.getTipoSocDistrDiv());

					val dividendIsr = apiRetention.getMontISRAcredNal();
					if (!UValidator.isNullOrEmpty(dividendIsr)) {
						dividendoUtilidad.setMontISRAcredNal(UValue.bigDecimal(dividendIsr, 2));
					}

					val dividendCumulativeNat = apiRetention.getMontDivAcumNal();
					if (!UValidator.isNullOrEmpty(dividendCumulativeNat)) {
						dividendoUtilidad.setMontDivAcumNal(UValue.bigDecimal(dividendCumulativeNat, 2));
					}

					val dividendCumulativeExt = apiRetention.getMontDivAcumExt();
					if (!UValidator.isNullOrEmpty(dividendCumulativeExt)) {
						dividendoUtilidad.setMontDivAcumExt(UValue.bigDecimal(dividendCumulativeExt, 2));
					}
					dividendo.setDividOUtil(dividendoUtilidad);
				}
			}

			if (!UValidator.isNullOrEmpty(dividendRemaining)) {
				dividendoRemanente.setProporcionRem(UValue.bigDecimal(dividendRemaining, 2));
				dividendo.setRemanente(dividendoRemanente);
			}
			complemento.getAny().add(dividendo);
		}
	}

	private void fillDisposalOfShares(final ApiRetention apiRetention) {
		if (retencion.getCveRetenc().equals("06")) {
			enajenacion.setVersion("1.0");

			enajenacion.setContratoIntermediacion(apiRetention.getContract());

			val profit = apiRetention.getProfit();
			val profitBig = UValue.bigDecimal(profit, 2);
			enajenacion.setGanancia(profitBig);

			val waste = apiRetention.getWaste();
			val wasteBig = UValue.bigDecimal(waste, 2);
			enajenacion.setPerdida(wasteBig);

			complemento.getAny().add(enajenacion);
		}
	}
	
	private void fillInterest(final ApiRetention apiRetention) {
		if (retencion.getCveRetenc().equals("16")) {
			intereses.setVersion("1.0");

			val financeSystem = apiRetention.getFinanceSystem();
			intereses.setSistFinanciero(financeSystem);

			val retirement = apiRetention.getRetirement();
			intereses.setRetiroAORESRetInt(retirement);

			val financialOperations = apiRetention.getFinancialOperations();
			intereses.setOperFinancDerivad(financialOperations);

			val interestAmount = apiRetention.getInterestAmount();
			val interestAmountBig = UValue.bigDecimal(interestAmount, 2);
			intereses.setMontIntNominal(interestAmountBig);

			val realInterest = apiRetention.getRealInterest();
			val realInterestBig = UValue.bigDecimal(realInterest, 2);
			intereses.setMontIntReal(realInterestBig);

			val interestWaste = apiRetention.getInterestWaste();
			val interestWasteBig = UValue.bigDecimal(interestWaste, 2);
			intereses.setPerdida(interestWasteBig);

			complemento.getAny().add(intereses);
		}
	}

	private void fillDerivativesOperations(final ApiRetention apiRetention) {
		if (retencion.getCveRetenc().equals("24")) {
			operacionesDerivados.setVersion("1.0");

			val profitAmount = apiRetention.getProfitAmount();
			val profitAmountBig = UValue.bigDecimal(profitAmount, 2);
			operacionesDerivados.setMontGanAcum(profitAmountBig);

			val deductibleWaste = apiRetention.getDeductibleWaste();
			val deductibleWasteBig = UValue.bigDecimal(deductibleWaste, 2);
			operacionesDerivados.setMontPerdDed(deductibleWasteBig);

			complemento.getAny().add(operacionesDerivados);
		}
	}
	
	private void fillPaymentsToForeigners(final ApiRetention apiRetention) {
		if (retencion.getCveRetenc().equals("18")) {
			pagosExtranjeros.setVersion("1.0");

			val isb = apiRetention.isBeneficiary() ? "SI" : "NO";
			pagosExtranjeros.setEsBenefEfectDelCobro(isb);

			val paymentConcept = apiRetention.getPaymentConcept();
			val conceptDescription = apiRetention.getConceptDescription();

			if (apiRetention.isBeneficiary()) {
				val beneficiaryRfc = apiRetention.getBeneficiaryRfc();
				beneficiario.setRFC(beneficiaryRfc);

				val beneficiaryCurp = apiRetention.getBeneficiaryCurp();
				beneficiario.setCURP(beneficiaryCurp);

				val beneficiaryBusinessName = apiRetention.getBeneficiaryBusinessName();
				beneficiario.setNomDenRazSocB(beneficiaryBusinessName);

				beneficiario.setConceptoPago(paymentConcept);
				beneficiario.setDescripcionConcepto(conceptDescription);
				pagosExtranjeros.setBeneficiario(beneficiario);
			} else {
				val noBeneficiaryCountry = apiRetention.getNoBeneficiaryCountry();
				CPais countryCatalogue = !UValidator.isNullOrEmpty(noBeneficiaryCountry)
						? CPais.fromValue(noBeneficiaryCountry)
						: null;
				noBeneficiario.setPaisDeResidParaEfecFisc(countryCatalogue);

				noBeneficiario.setConceptoPago(paymentConcept);
				noBeneficiario.setDescripcionConcepto(conceptDescription);
				pagosExtranjeros.setNoBeneficiario(noBeneficiario);
			}
			complemento.getAny().add(pagosExtranjeros);
		}
	}

}