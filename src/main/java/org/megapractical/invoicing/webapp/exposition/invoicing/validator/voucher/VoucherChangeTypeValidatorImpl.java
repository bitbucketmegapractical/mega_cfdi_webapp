package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherChangeTypeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherChangeTypeResponse.ChangeTypeResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherChangeTypeValidatorImpl implements VoucherChangeTypeValidator {
	
	@Autowired
	private UserTools userTools;
	
	/**
	 * <p>Tipo cambio</p>
	 * Atributo condicional para representar el tipo de cambio conforme con la moneda usada.
	 * Es requerido cuando la clave de moneda es distinta de MXN y de XXX.
	 * El valor debe reflejar el número de pesos mexicanos que equivalen a una unidad de la divisa 
	 * señalada en el atributo moneda. Si el valor está fuera del porcentaje aplicable a la moneda 
	 * tomado del catálogo c_Moneda, el emisor debe obtener del PAC que vaya a timbrar el CFDI, de manera no automática, 
	 * una clave de confirmación para ratificar que el valor es correcto e integrar dicha clave en el atributo Confirmacion.
	 */
	@Override
	public VoucherChangeTypeResponse getChangeType(JVoucher voucher, MonedaEntity currency, List<JError> errors) {
		if (currency != null) {
			val changeTypeResponse = getChangeType(voucher, currency);
			val changeType = changeTypeResponse.getChangeType();
			val error = changeTypeResponse.getError();
			
			var changeTypeError = false;
			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
				changeTypeError = true;
			}
			
			return new VoucherChangeTypeResponse(changeType, changeTypeError);
		}
		return VoucherChangeTypeResponse.empty();
	}
	
	private final ChangeTypeResponse getChangeType(JVoucher voucher, MonedaEntity currency) {
		val changeType = UValue.bigDecimalStrict(UBase64.base64Decode(voucher.getChangeType()));
		JError error = null;
		val field = "change-type";
		
		val currencyCode = currency.getCodigo();
		if (!currencyCode.equalsIgnoreCase("mxn") && !currencyCode.equalsIgnoreCase("xxx")) {
			if (UValidator.isNullOrEmpty(voucher.getChangeType())) {
				error = JError.required(field, userTools.getLocale());
			} else if (!UValidator.isDouble(UBase64.base64Decode(voucher.getChangeType()))) {
				error = JError.error(field, "ERR1003", userTools.getLocale());
			} else {
				if (changeType != null && !changeType.toString().matches(VoucherUtils.CHANGE_TYPE_EXPRESSION)) {
					val errorMessage = UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33116").value(), userTools.getLocale());
					error = JError.error(field, errorMessage);
				}
			}
		} else {
			if (!UValidator.isNullOrEmpty(changeType)) {
				error = JError.error(field, "ERR1006", userTools.getLocale());
			}
		}
		
		return new ChangeTypeResponse(changeType, error);
	}
	
}