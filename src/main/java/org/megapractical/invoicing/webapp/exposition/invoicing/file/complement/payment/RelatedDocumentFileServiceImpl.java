package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import java.math.BigDecimal;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CurrencyValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.TaxObjectValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service.TaxObjectService;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.CurrencyValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.TaxObjectValidator;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentRelatedDocument;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment.WRelatedDocument;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class RelatedDocumentFileServiceImpl implements RelatedDocumentFileService {

	private final CurrencyValidator currencyValidator;
	private final TaxObjectValidator taxObjectValidator;
	private final TaxObjectService taxObjectService;

	public RelatedDocumentFileServiceImpl(CurrencyValidator currencyValidator, 
										  TaxObjectService taxObjectService,
										  TaxObjectValidator taxObjectValidator) {
		this.currencyValidator = currencyValidator;
		this.taxObjectValidator = taxObjectValidator;
		this.taxObjectService = taxObjectService;
	}

	@Override
	public WRelatedDocument populateRelatedDocument(ComplementPaymentRelatedDocument data) {
		val documentIdError = validateDocumentId(data);

		val currencyResponse = validateCurrency(data);
		val currency = currencyResponse.getCurrency();
		val currencyError = currencyResponse.isHasError();

		val serieError = validateSerie(data);
		val sheetError = validateSheet(data);
		val partialityError = validatePartiality(data);

		val previousBalance = validatePreviousBalance(data, currency);
		val previousBalanceError = previousBalance == null;

		val amountPaid = validateAmountPaid(data, currency);
		val amountPaidError = amountPaid == null;

		val difference = validateDifference(data, currency, previousBalance, amountPaid);
		val differenceError = difference == null;
		
		val taxObjectResponse = validateTaxObject(data);
		val taxObjectError = taxObjectResponse.isHasError();
		
		if (documentIdError || currencyError || serieError || sheetError || partialityError || previousBalanceError
				|| amountPaidError || differenceError || taxObjectError) {
			return null;
		}
		
		return WRelatedDocument
				.builder()
				.documentId(data.getDocumentId())
				.serie(data.getSerie())
				.sheet(data.getSheet())
				.currency(UCatalog.moneda(currency.getCodigo()))
				.currencyEntity(currency)
				.changeType(UValue.bigDecimalStrict(data.getChangeType()))
				.partiality(UValue.integerValue(data.getPartiality()))
				.amountPartialityBefore(previousBalance)
				.amountPaid(amountPaid)
				.difference(difference)
				.taxObject(taxObjectService.description(taxObjectResponse.getTaxObject()))
				.taxObjectCode(data.getTaxObject())
				.build();
	}

	private boolean validateDocumentId(ComplementPaymentRelatedDocument relatedDocument) {
		val documentId = relatedDocument.getDocumentId();
		return UValidator.isNullOrEmpty(documentId) || (documentId.length() < 16 || documentId.length() > 36);
	}

	private CurrencyValidatorResponse validateCurrency(ComplementPaymentRelatedDocument relatedDocument) {
		val currency = relatedDocument.getCurrency();
		val currencyValidatorResponse = currencyValidator.validate(currency);
		val currencyEntity = currencyValidatorResponse.getCurrency();
		val hasError = currencyValidatorResponse.isHasError();

		return new CurrencyValidatorResponse(currencyEntity, hasError);
	}

	private boolean validateSerie(ComplementPaymentRelatedDocument relatedDocument) {
		val serie = relatedDocument.getSerie();
		return !UValidator.isNullOrEmpty(serie) && (serie.length() < 1 || serie.length() > 25);
	}

	private boolean validateSheet(ComplementPaymentRelatedDocument relatedDocument) {
		val sheet = relatedDocument.getSheet();
		return !UValidator.isNullOrEmpty(sheet) && (sheet.length() < 1 || sheet.length() > 40);
	}

	private boolean validatePartiality(ComplementPaymentRelatedDocument relatedDocument) {
		val partiality = relatedDocument.getPartiality();
		return UValidator.isNullOrEmpty(partiality) || !UValidator.isInteger(partiality);
	}

	private BigDecimal validatePreviousBalance(ComplementPaymentRelatedDocument relatedDocument,
			MonedaEntity currency) {
		if (currency == null) {
			return null;
		}

		BigDecimal previousBalanceBd = null;

		val previousBalance = relatedDocument.getAmountPartialityBefore();
		if (UValidator.isNullOrEmpty(previousBalance)) {
			return null;
		} else {
			previousBalanceBd = UValue.bigDecimalStrict(previousBalance, UTools.decimals(previousBalance));
			if (!UValidator.isValidDecimalPart(previousBalanceBd, currency.getDecimales())) {
				return null;
			}
		}
		return previousBalanceBd;
	}

	private BigDecimal validateAmountPaid(ComplementPaymentRelatedDocument relatedDocument, MonedaEntity currency) {
		if (currency == null) {
			return null;
		}

		BigDecimal amountPaidBd = null;
		val zero = UValue.bigDecimalStrict("0");

		val amountPaid = relatedDocument.getAmountPaid();
		if (UValidator.isNullOrEmpty(amountPaid)) {
			return null;
		} else {
			amountPaidBd = UValue.bigDecimalStrict(amountPaid, UTools.decimals(amountPaid));
			if (!UValidator.isValidDecimalPart(amountPaidBd, currency.getDecimales())
					|| amountPaidBd.compareTo(zero) < 1) {
				return null;
			}
		}
		return amountPaidBd;
	}

	private BigDecimal validateDifference(ComplementPaymentRelatedDocument relatedDocument, MonedaEntity currency,
			BigDecimal previousBalance, BigDecimal amountPaid) {
		if (currency == null || previousBalance == null || amountPaid == null) {
			return null;
		}

		BigDecimal differenceBd = null;

		val difference = relatedDocument.getDifference();
		if (UValidator.isNullOrEmpty(difference)) {
			return null;
		} else {
			differenceBd = UValue.bigDecimalStrict(difference, UTools.decimals(difference));
			if (!UValidator.isValidDecimalPart(differenceBd, currency.getDecimales())) {
				return null;
			} else {
				val diff = UValue.bigDecimal(difference, currency.getDecimales());
				val isValidDifference = validateDifference(diff, previousBalance, amountPaid);
				if (!isValidDifference) {
					return null;
				}
			}
		}
		return differenceBd;
	}

	private boolean validateDifference(BigDecimal difference, BigDecimal previousBalance, BigDecimal amountPaid) {
		val calc = previousBalance.subtract(amountPaid);
		return difference.equals(calc);
	}
	
	private TaxObjectValidatorResponse validateTaxObject(ComplementPaymentRelatedDocument relatedDocument) {
		val taxObject = relatedDocument.getTaxObject();
		return taxObjectValidator.validate(taxObject);
	}

}