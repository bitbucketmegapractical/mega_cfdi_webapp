package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentTargetAccountRfcValidatorImpl implements PaymentTargetAccountRfcValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Override
	public String getTargetAccountRfc(String targetAccountRfc, FormaPagoEntity paymentWay) {
		if (targetAccountRfc != null && paymentWay != null) {
			val rfcTargetAccountValid = paymentWay.getCuentaBeneficiarioRfc();
			if (rfcTargetAccountValid.equalsIgnoreCase("no")) {
				return null;
			}
		}
		return targetAccountRfc;
	}

	@Override
	public String getTargetAccountRfc(String targetAccountRfc, String field, FormaPagoEntity paymentWay,
			List<JError> errors) {
		JError error = null;
		if (targetAccountRfc != null && paymentWay != null) {
			val rfcTargetAccountValid = paymentWay.getCuentaBeneficiarioRfc();
			if (rfcTargetAccountValid.equalsIgnoreCase("no")) {
				error = JError.error(field, "ERR1006", userTools.getLocale());
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return targetAccountRfc;
	}

}