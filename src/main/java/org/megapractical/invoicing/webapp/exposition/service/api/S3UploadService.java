package org.megapractical.invoicing.webapp.exposition.service.api;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UZipper;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.ResourceData;
import org.megapractical.invoicing.webapp.exposition.service.CfdiService;
import org.megapractical.invoicing.webapp.exposition.service.PayrollService;
import org.megapractical.invoicing.webapp.exposition.service.api.payload.UploadResponse;
import org.megapractical.invoicing.webapp.exposition.service.s3.S3Service;
import org.megapractical.invoicing.webapp.tenant.bancomext.error.BancomextError;
import org.megapractical.invoicing.webapp.tenant.bancomext.error.BancomextError.BancomextErrorDetail;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextMailService;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextMailUtils;
import org.megapractical.invoicing.webapp.tenant.bancomext.payload.BancomextAdditional;
import org.megapractical.invoicing.webapp.tenant.bancomext.util.BancomextUtils;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextFileWrapper;
import org.megapractical.invoicing.webapp.tenant.bancomext.wrapper.BancomextS3Error;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class S3UploadService {
	
	private final AppSettings appSettings;
	private final ResourceService resourceService;
	private final S3Service s3Service;
	private final CfdiService cfdiService;
	private final PayrollService payrollService;
	private final BancomextMailService bancomextMailService;

	public String uploadToS3(BancomextFileWrapper fileWrapper, List<ResourceData> resourceDataSource) {
		val s3Errors = new ArrayList<BancomextS3Error>();
		if (!resourceDataSource.isEmpty()) {
			UPrint.logWithLine(UProperties.env(), "[INFO] S3 UPLOAD SERVICE > STARTING ASYNC UPLOAD TO S3");
			for (val resourceData : resourceDataSource) {
				try {
					val emitterRfc = getEmitterRfc(resourceData);
					val xmlUploadPath = uploadXml(emitterRfc, resourceData, s3Errors);
					val pdfUploadPath = uploadPdf(emitterRfc, resourceData, s3Errors);
					updateXmlPdfPath(resourceData, xmlUploadPath, pdfUploadPath);
					val apiCfdi = getApiCfdi(resourceData);
					sendEmail(resourceData, apiCfdi);
				} catch (Exception e) {
					UPrint.logWithLine(UProperties.env(), "[ERROR] S3 UPLOAD SERVICE > AN ERROR HAS OCURRED");
					UPrint.log(UProperties.env(), "XML PATH: " + resourceData.getXmlPath());
					UPrint.log(UProperties.env(), "PDF PATH: " + resourceData.getPdfPath());
					e.printStackTrace();
				}
			}
			UPrint.logWithLine(UProperties.env(), "[INFO] S3 UPLOAD SERVICE > UPLOAD TO S3 COMPLETED");
		} else {
			UPrint.logWithLine(UProperties.env(), "[INFO] S3 UPLOAD SERVICE > NO FILES TO UPLOAD");
		}
		return writeError(fileWrapper, s3Errors);
	}

	private String uploadXml(String emitterRfc, ResourceData resourceData, final List<BancomextS3Error> s3Errors) {
		String xmlUploadPath = null; 
		val xmlPath = resourceData.getXmlPath();
		val xmlUploadResponse = uploadToS3(emitterRfc, xmlPath);
		if (xmlUploadResponse.isUploaded()) {
			xmlUploadPath = xmlUploadResponse.getUploadPath();
			UPrint.logWithLine(UProperties.env(), "[INFO] S3 UPLOAD SERVICE > XML FILE UPLOADED TO " + xmlUploadPath);
		} else {
			UPrint.logWithLine(UProperties.env(), "[ERROR] S3 UPLOAD SERVICE > FAILED TO UPLOAD XML FILE");
			s3Errors.add(new BancomextS3Error(xmlUploadResponse.getFileName(), xmlUploadResponse.getError()));
		}
		return xmlUploadPath;
	}
	
	private String uploadPdf(String emitterRfc, ResourceData resourceData, final List<BancomextS3Error> s3Errors) {
		String pdfUploadPath = null;
		val pdfPath = resourceData.getPdfPath();
		val pdfUploadResponse = uploadToS3(emitterRfc, pdfPath);
		if (pdfUploadResponse.isUploaded()) {
			pdfUploadPath = pdfUploadResponse.getUploadPath();
			UPrint.logWithLine(UProperties.env(), "[INFO] S3 UPLOAD SERVICE > PDF FILE UPLOADED TO " + pdfUploadPath);
		} else {
			UPrint.logWithLine(UProperties.env(), "[ERROR] S3 UPLOAD SERVICE > FAILED TO UPLOAD PDF FILE");
			s3Errors.add(new BancomextS3Error(pdfUploadResponse.getFileName(), pdfUploadResponse.getError()));
		}
		return pdfUploadPath;
	}

	private UploadResponse uploadToS3(String emitterRfc, String filePath) {
		val path = Paths.get(filePath);
		val file = path.toFile();
		val folder = emitterRfc.toUpperCase();
		val fileName = file.getName();
		val key = folder + "/" + fileName;
		String errorMessage = null;
		
		try {
			s3Service.ensureFolderExists(folder);
			val objectResponse = s3Service.uploadFile(key, file);
			if (objectResponse != null) {
				return UploadResponse
						.builder()
						.fileName(fileName)
						.uploadPath(String.format("%s/%s", folder, fileName))
						.uploaded(true)
						.build();
			}
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
		return UploadResponse.error(fileName, errorMessage);
	}
	
	private String writeError(BancomextFileWrapper fileWrapper, List<BancomextS3Error> errors) {
		try {
			if (errors != null && !errors.isEmpty()) {
				// Fichero de error a generar
				val s3ErrorPath = BancomextUtils.s3ErrorPath(fileWrapper);
				val errorFile = fileWrapper.getFileDetail().getFileName() + "_S3_ERROR.txt";
				val fileErrorPath = s3ErrorPath + BancomextUtils.SEPARATOR + errorFile;
				val errorDetail = new BancomextErrorDetail(s3ErrorPath, fileErrorPath);
				
				UFile.createParentDirectory(fileErrorPath);
				BancomextError.writeS3Error(errorDetail, errors);
				
				if (UFile.totalFilesInDirectory(s3ErrorPath) > 0) {
					val zipFile = s3ErrorPath + ".zip";
					UPrint.logWithLine(UProperties.env(), "[INFO] S3 UPLOAD SERVICE > ZIPPING DIRECTORY: " + s3ErrorPath);
					UZipper.zipping(new File(s3ErrorPath), zipFile);
					return zipFile;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unused")
	private void uploadFile(List<ResourceData> resourceDataSource) {
		for (val resourceData : resourceDataSource) {
			try {
				val emitterRfc = getEmitterRfc(resourceData);
				val xmlPath = resourceData.getXmlPath();
				val xmlUploadResponse = resourceService.uploadFile(emitterRfc, xmlPath);
				val xmlUploadPath = xmlUploadResponse != null ? xmlUploadResponse.getUploadPath() : null;

				val pdfPath = resourceData.getPdfPath();
				val pdfUploadResponse = resourceService.uploadFile(emitterRfc, pdfPath);
				val pdfUploadPath = pdfUploadResponse != null ? pdfUploadResponse.getUploadPath() : null;

				if (xmlUploadPath != null && pdfUploadPath != null) {
					updateXmlPdfPath(resourceData, xmlUploadPath, pdfUploadPath);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unused")
	private void uploadFiles(List<ResourceData> resourceDataSource) {
		try {
			val uploadResponseList = resourceService.uploadFiles(resourceDataSource);
			resourceDataSource.forEach(resourceData -> {
				val xmlS3Path = getS3Path(uploadResponseList, resourceData.getXmlFileName());
				val pdfS3Path = getS3Path(uploadResponseList, resourceData.getPdfFileName());
				updateXmlPdfPath(resourceData, xmlS3Path, pdfS3Path);
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getEmitterRfc(ResourceData resourceData) {
		if (resourceData.getResourceCfdiData() != null) {
			return resourceData.getResourceCfdiData().getApiCfdi().getEmitter().getRfc();
		} else if (resourceData.getResourcePayrollData() != null) {
			return resourceData.getResourcePayrollData().getApiPayroll().getApiCfdi().getEmitter().getRfc();
		}
		return null;
	}
	
	private ApiCfdi getApiCfdi(ResourceData resourceData) {
		if (resourceData != null) {
			if (resourceData.getResourceCfdiData() != null) {
				return resourceData.getResourceCfdiData().getApiCfdi();
			} else if (resourceData.getResourcePayrollData() != null) {
				return resourceData.getResourcePayrollData().getApiPayroll().getApiCfdi();
			}
		}
		return null;
	}
	
	private void updateXmlPdfPath(ResourceData resourceData, String xmlUploadPath, String pdfUploadPath) {
		if (xmlUploadPath != null && pdfUploadPath != null) {
			if (resourceData.getResourceCfdiData() != null) {
				val cfdi = resourceData.getResourceCfdiData().getCfdi();
				cfdi.setRutaXml(xmlUploadPath);
				cfdi.setRutaPdf(pdfUploadPath);
				cfdiService.save(cfdi);
			} else if (resourceData.getResourcePayrollData() != null) {
				val payroll = resourceData.getResourcePayrollData().getPayroll();
				payroll.setRutaXml(xmlUploadPath);
				payroll.setRutaPdf(pdfUploadPath);
				payrollService.save(payroll);
			}
		}
	}
	
	private void sendEmail(ResourceData resourceData, ApiCfdi apiCfdi) {
		if (apiCfdi != null) {
			var recipients = "";
			if (apiCfdi.getAdditionals().get(BancomextAdditional.EMAIL) != null) {
				recipients = apiCfdi.getAdditionals().get(BancomextAdditional.EMAIL).toString();
			}
			
			// Adding bancomext@megapractical.com to recipients
			val includeBancomextMega = appSettings.getBooleanValue("send.email.to.bancomext.mega");
			if (includeBancomextMega) {
				recipients += ";bancomext@megapractical.com";
			}
			
			if (recipients.startsWith(";")) {
				recipients = recipients.substring(1);
			}
			
			apiCfdi.getAdditionals().put(BancomextAdditional.EMAIL, recipients);
			val sendTo = apiCfdi.getAdditionals().get(BancomextAdditional.EMAIL).toString();
			
			if (!UValidator.isNullOrEmpty(sendTo)) {
				UPrint.logWithLine(UProperties.env(), "[INFO] S3 UPLOAD SERVICE > PREPARING TO SEND EMAIL TO " + sendTo);
				val mailData = BancomextMailUtils.populateMailData(resourceData, apiCfdi);
				bancomextMailService.sendEmail(mailData);			
			} else {
				UPrint.logWithLine(UProperties.env(), "[INFO] S3 UPLOAD SERVICE > NO RECIPIENTS TO SEND EMAIL");
			}
		}
	}
	
	private String getS3Path(List<UploadResponse> uploadResponseList, String fileName) {
		return uploadResponseList.stream().filter(fileNameFilter(fileName)).map(UploadResponse::getUploadPath)
				.findFirst().orElse(null);
	}

	private Predicate<? super UploadResponse> fileNameFilter(String fileName) {
		return i -> i.getFileName().equals(fileName);
	}

}