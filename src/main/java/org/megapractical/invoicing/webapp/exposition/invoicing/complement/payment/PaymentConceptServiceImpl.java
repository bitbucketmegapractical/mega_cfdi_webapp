package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.dal.data.repository.IKeyProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IMeasurementUnitJpaRepository;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentConceptServiceImpl implements PaymentConceptService {
	
	private final IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository;
	private final IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository;
	
	private static final String CONCEPT_DESCRIPTION = "Pago";
	private static final String CONCEPT_KEY_PRODUCT_SERVICE = "84111506";
	private static final String CONCEPT_MEASUREMENT_UNIT = "ACT";
	private static final String CONCEPT_QUANTITY = "1";
	private static final String CONCEPT_UNIT_VALUE = "0";
	private static final String CONCEPT_AMOUNT = "0";
	private static final String CONCEPT_OBJETO_IMP = "01";
	
	public PaymentConceptServiceImpl(IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository,
			 						 IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository) {
		this.iKeyProductServiceJpaRepository = iKeyProductServiceJpaRepository;
		this.iMeasurementUnitJpaRepository = iMeasurementUnitJpaRepository;
	}
	
	@Override
	public ApiConcept paymentConcept() {
		val keyProductService = iKeyProductServiceJpaRepository
				.findByCodigoAndEliminadoFalse(CONCEPT_KEY_PRODUCT_SERVICE);
		val measurementUnit = iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(CONCEPT_MEASUREMENT_UNIT);
		val quantity = UValue.bigDecimalStrict(CONCEPT_QUANTITY);

		val concept = new ApiConcept();
		concept.setConceptNumber("1");
		concept.setKeyProductServiceEntity(keyProductService);
		concept.setKeyProductServiceCode(keyProductService.getCodigo());
		concept.setKeyProductService(keyProductService.getValor());
		concept.setQuantity(quantity);
		concept.setMeasurementUnitEntity(measurementUnit);
		concept.setMeasurementUnitCode(measurementUnit.getCodigo());
		concept.setMeasurementUnit(measurementUnit.getValor());
		concept.setDescription(CONCEPT_DESCRIPTION);
		concept.setUnitValue(UValue.bigDecimalStrict(CONCEPT_UNIT_VALUE));
		concept.setUnitValueString(CONCEPT_UNIT_VALUE);
		concept.setAmount(UValue.bigDecimalStrict(CONCEPT_AMOUNT));
		concept.setAmountString(CONCEPT_AMOUNT);
		concept.setObjImp(CONCEPT_OBJETO_IMP);
		return concept;
	}

}