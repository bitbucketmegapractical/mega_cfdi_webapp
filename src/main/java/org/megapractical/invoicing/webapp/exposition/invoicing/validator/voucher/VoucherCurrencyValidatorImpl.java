package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.CurrencyValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherCurrencyResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherCurrencyValidatorImpl implements VoucherCurrencyValidator {
	
	@Autowired
	private CurrencyValidator currencyValidator;
	
	@Override
	public VoucherCurrencyResponse getCurrency(JVoucher voucher, List<JError> errors) {
		var currency = UBase64.base64Decode(voucher.getCurrency());
		val currencyValidatorResponse = currencyValidator.validate(currency, "currency");
		val currencyEntity = currencyValidatorResponse.getCurrency();
		val error = currencyValidatorResponse.getError();
		
		var currencyError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			currencyError = true;
		}
		
		return new VoucherCurrencyResponse(currencyEntity, currencyError);
	}

}