package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import org.megapractical.invoicing.api.wrapper.ApiPaymentParser;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;

public interface PaymentFileStatisticService {
	
	ArchivoFacturaEstadisticaEntity managePaymentFileStatistics(ApiPaymentParser paymentParser);
	
}