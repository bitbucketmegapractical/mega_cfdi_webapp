package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.wrapper.ApiPaymentParser;
import org.megapractical.invoicing.api.wrapper.ApiPaymentStamp;

public interface PaymentParserService {

	ApiPaymentParser paymentParser(ApiPaymentStamp complementStamp);
	
}