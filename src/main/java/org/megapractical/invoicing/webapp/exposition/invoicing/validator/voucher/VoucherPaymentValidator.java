package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherPaymentMethodResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherPaymentWayResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;

public interface VoucherPaymentValidator {

	VoucherPaymentWayResponse getPaymentWay(JVoucher voucher, TipoComprobanteEntity voucherType, List<JError> errors);

	VoucherPaymentMethodResponse getPaymentMethod(JVoucher voucher, TipoComprobanteEntity voucherType, List<JError> errors);

}