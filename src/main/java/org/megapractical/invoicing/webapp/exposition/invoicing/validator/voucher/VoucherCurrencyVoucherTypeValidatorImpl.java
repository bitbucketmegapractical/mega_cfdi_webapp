package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.ArrayList;

import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class VoucherCurrencyVoucherTypeValidatorImpl implements VoucherCurrencyVoucherTypeValidator {
	
	@Autowired
	private VoucherCurrencyValidator voucherCurrencyValidator;
	
	@Autowired
	private VoucherTypeValidator voucherTypeValidator;
	
	@Override
	public JResponse validateCurrencyVoucherType(String currency, String voucherType) {
		val voucher = new JVoucher();
		voucher.setCurrency(currency);
		voucher.setVoucherType(voucherType);
		
		val errors = new ArrayList<JError>();
		voucherCurrencyValidator.getCurrency(voucher, errors);
		voucherTypeValidator.getVoucherType(voucher, errors);
		
		JResponse response = null;
		if (!errors.isEmpty()) {
			response = JResponse.markedError(errors);
		} else {
			response = JResponse.success();
		}
		return response;
	}

}