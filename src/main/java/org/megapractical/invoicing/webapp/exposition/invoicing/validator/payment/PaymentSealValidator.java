package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentSealValidator {

	String getPaymentSeal(String paymentSeal, String stringType);

	String getPaymentSeal(String paymentSeal, String field, String stringType, List<JError> errors);

}