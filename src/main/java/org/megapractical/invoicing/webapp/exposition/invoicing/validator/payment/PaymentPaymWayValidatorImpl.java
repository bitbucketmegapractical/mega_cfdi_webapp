package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.PaymentWayValidator;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentPaymWayValidatorImpl implements PaymentPaymWayValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private PaymentWayValidator paymentWayValidator;
	
	@Override
	public FormaPagoEntity getPaymentWay(String paymentWay) {
		FormaPagoEntity cmpPaymentWayEntity = null;
		if (!UValidator.isNullOrEmpty(paymentWay)) {
			val paymentWayValidatorResponse = paymentWayValidator.validate(paymentWay);
			cmpPaymentWayEntity = paymentWayValidatorResponse.getPaymentWay();
			val hasError = paymentWayValidatorResponse.isHasError();
			
			if (cmpPaymentWayEntity != null && !hasError
					&& cmpPaymentWayEntity.getCodigo().equalsIgnoreCase("99")) {
				return null;
			}
		}
		return cmpPaymentWayEntity;
	}

	@Override
	public FormaPagoEntity getPaymentWay(String paymentWay, String field, List<JError> errors) {
		FormaPagoEntity cmpPaymentWayEntity = null;
		JError error = null;
		
		if (!UValidator.isNullOrEmpty(paymentWay)) {
			val paymentWayValidatorResponse = paymentWayValidator.validate(paymentWay, field);
			cmpPaymentWayEntity = paymentWayValidatorResponse.getPaymentWay();
			error = paymentWayValidatorResponse.getError();
			
			if (cmpPaymentWayEntity != null && error == null
					&& cmpPaymentWayEntity.getCodigo().equalsIgnoreCase("99")) {
				error = JError.error(field, "ERR0155", userTools.getLocale());
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return cmpPaymentWayEntity;
	}

}