package org.megapractical.invoicing.webapp.exposition.invoicing.file.service;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;

public interface InvoiceFileConfigurationService {
	
	ArchivoFacturaConfiguracionEntity findByTaxpayer(ContribuyenteEntity taxpayer);
	
	ArchivoFacturaConfiguracionEntity save(ArchivoFacturaConfiguracionEntity entity);
	
}