package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentSourceAccountRfcValidator {

	String getSourceAccountRfc(String sourceAccountRfc, FormaPagoEntity paymentWay);

	String getSourceAccountRfc(String sourceAccountRfc, String field, FormaPagoEntity paymentWay, List<JError> errors);

}