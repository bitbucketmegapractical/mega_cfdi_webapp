package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.dal.bean.datatype.StorageType;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.AllowStamp;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class CfdiServiceImpl implements CfdiService {
	
	private final ITaxpayerJpaRepository iTaxpayerJpaRepository;
	private final ICfdiJpaRepository iCfdiJpaRepository;
	
	@Override
	public CfdiEntity findByInvoiceFile(ArchivoFacturaEntity invoiceFile) {
		return iCfdiJpaRepository.findFirstByArchivoFacturaAndCanceladoFalseOrderByIdDesc(invoiceFile);
	}

	@Override
	public boolean isAllowedToStamp(String rfc, String serie, String folio) {
		if (AssertUtils.hasValue(serie) && AssertUtils.hasValue(folio)) {
			val cfdiInDb = iCfdiJpaRepository.findByReceptorRfcAndSerieAndFolio(rfc, serie, folio).orElse(null);
			return cfdiInDb == null;
		}
		return true;
	}
	
	@Override
	public AllowStamp verifyCfdi(String rfc, String serie, String folio) {
		if (AssertUtils.hasValue(serie) && AssertUtils.hasValue(folio)) {
			val cfdiInDb = iCfdiJpaRepository.findByReceptorRfcAndSerieAndFolio(rfc, serie, folio).orElse(null);
			if (cfdiInDb != null) {
				return AllowStamp.notAllowed(cfdiInDb.getUuid());
			}
		}
		return AllowStamp.allowed();
	}
	
	@Override
	public CfdiEntity findByUuid(String uuid) {
		return iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalse(uuid);
	}
	
	@Override
	public CfdiEntity findForCancellation(String emitterRfc, String receiverRfc, String uuid) {
		return iCfdiJpaRepository.findByEmisorRfcAndReceptorRfcAndUuid(emitterRfc, receiverRfc, uuid).orElse(null);
	}
	
	@Override
	public CfdiEntity save(ApiCfdi apiCfdi) {
		val cfdi = mapToEntity(apiCfdi);
		return save(cfdi);
	}

	@Override
	public CfdiEntity save(CfdiEntity cfdi) {
		if (cfdi != null && cfdi.getUuid() != null) {
			return iCfdiJpaRepository.save(cfdi);
		}
		return cfdi;
	}
	
	private CfdiEntity mapToEntity(ApiCfdi apiCfdi) {
		if (apiCfdi != null) {
			val taxpayer = iTaxpayerJpaRepository.findByRfc(apiCfdi.getEmitter().getRfc());
			
			val cfdi = new CfdiEntity();
			cfdi.setContribuyente(taxpayer);
			cfdi.setEmisorRfc(taxpayer.getRfcActivo());
			cfdi.setReceptorRfc(apiCfdi.getReceiver().getRfc());
			cfdi.setReceptorRazonSocial(apiCfdi.getReceiver().getNombreRazonSocial());
			cfdi.setReceptorCorreoElectronico(apiCfdi.getReceiver().getCorreoElectronico());
			cfdi.setReceptorCurp(apiCfdi.getReceiver().getCurp());
			cfdi.setTipoComprobante(apiCfdi.getVoucher().getVoucherType().name());
			cfdi.setUuid(apiCfdi.getStampResponse().getUuid());
			cfdi.setFechaExpedicion(UDate.formattedDate(apiCfdi.getVoucher().getDateTimeExpedition()));
			cfdi.setFechaHoraExpedicion(apiCfdi.getVoucher().getDateTimeExpedition());
			cfdi.setFechaHoraTimbrado(apiCfdi.getVoucher().getDateTimeStamp());
			cfdi.setTotal(UValue.doubleValue(apiCfdi.getVoucher().getTotal()));
			cfdi.setSerie(apiCfdi.getVoucher().getSerie());
			cfdi.setFolio(apiCfdi.getVoucher().getFolio());
			cfdi.setRutaXml(apiCfdi.getXmlPath());
			cfdi.setRutaPdf(apiCfdi.getPdfPath());
			cfdi.setCancelado(false);			
			cfdi.setCancelacionEnProceso(false);
			cfdi.setSolicitudCancelacion(false);
			cfdi.setStorage(StorageType.S3);
			return cfdi;
		}
		return null;
	}

}