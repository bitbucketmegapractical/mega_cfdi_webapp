package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.megapractical.invoicing.dal.bean.jpa.ClaveProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ExportacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MetodoPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PaisEntity;
import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoCadenaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.megapractical.invoicing.dal.bean.jpa.UnidadMedidaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsoCfdiEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiUseJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICountryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IExportationJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IKeyProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IMeasurementUnitJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentMethodJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentWayJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPostalCodeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStringTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxRegimeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IVoucherTypeJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class InvoiceAutocompleteServiceImpl implements InvoiceAutocompleteService {
	
	@Autowired
	private ITaxRegimeJpaRepository iTaxRegimeJpaRepository;
	
	@Autowired
	private ICfdiUseJpaRepository iCfdiUseJpaRepository;
	
	@Autowired
	private ICountryJpaRepository iCountryJpaRepository;
	
	@Autowired
	private IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository;
	
	@Autowired
	private IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository;
	
	@Autowired
	private IVoucherTypeJpaRepository iVoucherTypeJpaRepository;
	
	@Autowired
	private IPostalCodeJpaRepository iPostalCodeJpaRepository;
	
	@Autowired
	private IPaymentWayJpaRepository iPaymentWayJpaRepository;
	
	@Autowired
	private IPaymentMethodJpaRepository iPaymentMethodJpaRepository;
	
	@Autowired
	private ICurrencyJpaRepository iCurrencyJpaRepository;
	
	@Autowired
	private IStringTypeJpaRepository iStringTypeJpaRepository;
	
	@Autowired
	private IExportationJpaRepository iExportationJpaRepository;
	
	@Override
	public List<String> taxRegimeSource() {
		val source = iTaxRegimeJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> cfdiUseSource() {
		val source = iCfdiUseJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> residencySource() {
		val source = iCountryJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> measurementUnitSource() {
		val source = iMeasurementUnitJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> keyProductServiceSource() {
		val source = iKeyProductServiceJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> voucherTypeSource() {
		val source = iVoucherTypeJpaRepository.findByEliminadoFalse();
		return source.stream().filter(voucherTypeFilter()).map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> expeditionPlaceSource() {
		val source = iPostalCodeJpaRepository.findByEliminadoFalse();
		return source.stream().map(CodigoPostalEntity::getCodigo).collect(Collectors.toList());
	}
	
	@Override
	public List<String> paymentWaySource() {
		val source = iPaymentWayJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> paymentMethodSource() {
		val source = iPaymentMethodJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> currencySource() {
		val source = iCurrencyJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> stringTypeSource() {
		val source = iStringTypeJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	@Override
	public List<String> exportationSource() {
		val source = iExportationJpaRepository.findByEliminadoFalse();
		return source.stream().map(this::entityMap).collect(Collectors.toList());
	}
	
	private static Predicate<TipoComprobanteEntity> voucherTypeFilter() {
		return p -> !p.getCodigo().equals("N") && !p.getCodigo().equals("P");
	}
	
	private final String entityMap(Object item) {
		var code = "";
		var value = "";
		
		if (item instanceof RegimenFiscalEntity) {
			code = ((RegimenFiscalEntity) item).getCodigo();
			value = ((RegimenFiscalEntity) item).getValor();
		}
		
		if (item instanceof UsoCfdiEntity) {
			code = ((UsoCfdiEntity) item).getCodigo();
			value = ((UsoCfdiEntity) item).getValor();
		}
		
		if (item instanceof PaisEntity) {
			code = ((PaisEntity) item).getCodigo();
			value = ((PaisEntity) item).getValor();
		}
		
		if (item instanceof UnidadMedidaEntity) {
			code = ((UnidadMedidaEntity) item).getCodigo();
			value = ((UnidadMedidaEntity) item).getValor();
		}
		
		if (item instanceof ClaveProductoServicioEntity) {
			code = ((ClaveProductoServicioEntity) item).getCodigo();
			value = ((ClaveProductoServicioEntity) item).getValor();
		}
		
		if (item instanceof TipoComprobanteEntity) {
			code = ((TipoComprobanteEntity) item).getCodigo();
			value = ((TipoComprobanteEntity) item).getValor();
		}
		
		if (item instanceof FormaPagoEntity) {
			code = ((FormaPagoEntity) item).getCodigo();
			value = ((FormaPagoEntity) item).getValor();
		}
		
		if (item instanceof MetodoPagoEntity) {
			code = ((MetodoPagoEntity) item).getCodigo();
			value = ((MetodoPagoEntity) item).getValor();
		}
		
		if (item instanceof MonedaEntity) {
			code = ((MonedaEntity) item).getCodigo();
			value = ((MonedaEntity) item).getValor();
		}
		
		if (item instanceof TipoCadenaPagoEntity) {
			code = ((TipoCadenaPagoEntity) item).getCodigo();
			value = ((TipoCadenaPagoEntity) item).getValor();
		}
		
		if (item instanceof ExportacionEntity) {
			code = ((ExportacionEntity) item).getCodigo();
			value = ((ExportacionEntity) item).getValor();
		}
		 
		return String.format("%s (%s)", code, value);
	}

}