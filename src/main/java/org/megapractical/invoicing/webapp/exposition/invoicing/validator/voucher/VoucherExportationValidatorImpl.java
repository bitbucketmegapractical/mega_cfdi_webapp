package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.ExportationValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.ExportationValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherExportationResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherExportationValidatorImpl implements VoucherExportationValidator {
	
	@Autowired
	private ExportationValidator exportationValidator;
	
	@Override
	public VoucherExportationResponse getExportation(String exportation) {
		val exportationValidatorResponse = exportationValidator.validate(exportation);
		return getExportation(exportationValidatorResponse);
	}

	@Override
	public VoucherExportationResponse getExportation(String exportation, String field) {
		val exportationValidatorResponse = exportationValidator.validate(exportation, field);
		return getExportation(exportationValidatorResponse);
	}
	
	@Override
	public VoucherExportationResponse getExportation(JVoucher voucher, String field, List<JError> errors) {
		var exportation = UBase64.base64Decode(voucher.getExportation());
		val exportationValidatorResponse = exportationValidator.validate(exportation, field);
		val exportationEntity = exportationValidatorResponse.getExportation();
		val error = exportationValidatorResponse.getError();
		
		var exportationError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			exportationError = true;
		}
		
		return new VoucherExportationResponse(exportationEntity, exportationError);
	}
	
	private VoucherExportationResponse getExportation(final ExportationValidatorResponse exportationValidatorResponse) {
		val exportationEntity = exportationValidatorResponse.getExportation();
		val hasError = exportationValidatorResponse.isHasError();
		return new VoucherExportationResponse(exportationEntity, hasError);
	}

}