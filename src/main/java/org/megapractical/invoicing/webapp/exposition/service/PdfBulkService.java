package org.megapractical.invoicing.webapp.exposition.service;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface PdfBulkService {
	
	void pdfFromXmlBulkGeneration();
	
}