package org.megapractical.invoicing.webapp.exposition.configuration;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.data.repository.IPreferencesJpaRepository;
import org.megapractical.invoicing.webapp.json.JPreferences;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.EmitterTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
@Scope("session")
public class EmitterPreferencesController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	EmitterTools emitterTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Resource
	IPreferencesJpaRepository iPreferencesJpaRepository;
	
	//##### Cargando preferencias del usuario
	@RequestMapping(value = "/emitterPreferenceLoad", method = RequestMethod.POST)
	public @ResponseBody String emitterPreferenceLoad(HttpServletRequest request){
		
		JPreferences preferences = new JPreferences();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			//if(userTools.isMaster()){
			if(userTools.haveDaysLeft()) {
				//##### Emisor activo
				ContribuyenteEntity taxpayer = emitterTools.getTaxpayerByActiveRfc();
				
				//##### Preferencias
				PreferenciasEntity preferencesEntity = iPreferencesJpaRepository.findByContribuyente(taxpayer);
				if(!UValidator.isNullOrEmpty(preferencesEntity)){
					preferences.setCurrency(UBase64.base64Encode(preferencesEntity.getMoneda().getCodigo().concat(" (").concat(preferencesEntity.getMoneda().getValor()).concat(")")));
					preferences.setIncreaseEndSheet(preferencesEntity.getIncrementarFolioFinal());
					preferences.setRegisterConcept(preferencesEntity.getRegistrarConcepto());
					preferences.setRegisterCustomer(preferencesEntity.getRegistrarCliente());
					preferences.setRegisterTaxRegime(preferencesEntity.getRegistrarRegimenFiscal());
					preferences.setReuseSheetCanceled(preferencesEntity.getReutilizarFolioCancelado());
					preferences.setSendToEmitterOnlyPdf(preferencesEntity.getEmisorPdf());
					preferences.setSendToEmitterOnlyXml(preferencesEntity.getEmisorXml());
					preferences.setSendToEmitterXmlAndPdf(preferencesEntity.getEmisorXmlPdf());
					preferences.setSendToReceiverOnlyPdf(preferencesEntity.getReceptorPdf());
					preferences.setSendToReceiverOnlyXml(preferencesEntity.getReceptorXml());
					preferences.setSendToReceiverXmlAndPdf(preferencesEntity.getReceptorXmlPdf());
					preferences.setSendToThirdPartiesOnlyPdf(preferencesEntity.getContactoPdf());
					preferences.setSendToThirdPartiesOnlyXml(preferencesEntity.getContactoXml());
					preferences.setSendToThirdPartiesXmlAndPdf(preferencesEntity.getContactoXmlPdf());
					preferences.setUpdateConcept(preferencesEntity.getActualizarConcepto());
					preferences.setUpdateCustomer(preferencesEntity.getActualizarCliente());
					preferences.setUpdateTaxRegime(preferencesEntity.getActualizarRegimenFiscal());
					preferences.setRegisterUpdatePostalCode(preferencesEntity.getRegistrarActualizarLugarExpedicion());
					preferences.setPreInvoicePdf(preferencesEntity.getPrefacturaPdf());
				}else{
					preferences = null;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jsonInString = gson.toJson(preferences);
		return jsonInString;
	}
	
	//##### Actualizando preferencias del usuario
	@RequestMapping(value = "/emitterPreferenceUpdate", method = RequestMethod.POST)
	public @ResponseBody String emitterPreferenceUpdate(HttpServletRequest request){
		String result = UBase64.base64Encode("PREFERENCES_UPDATED");
		try {
			
			//##### Emisor activo
			ContribuyenteEntity taxpayer = emitterTools.getTaxpayerByActiveRfc();
			
			//##### Preferencias
			PreferenciasEntity preferences = iPreferencesJpaRepository.findByContribuyente(taxpayer);
			if(!UValidator.isNullOrEmpty(preferences)){
				
				String option = UBase64.base64Decode(request.getParameter("option"));
				String value = UBase64.base64Decode(request.getParameter("optionValue"));
				
				if(option.equalsIgnoreCase("emisor_xml_pdf")){
					if(value.equalsIgnoreCase("on")){
						preferences.setEmisorXmlPdf(Boolean.TRUE);
						preferences.setEmisorXml(Boolean.FALSE);
						preferences.setEmisorPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setEmisorXmlPdf(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("emisor_xml")){
					if(value.equalsIgnoreCase("on")){
						preferences.setEmisorXml(Boolean.TRUE);
						preferences.setEmisorXmlPdf(Boolean.FALSE);
						preferences.setEmisorPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setEmisorXml(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("emisor_pdf")){
					if(value.equalsIgnoreCase("on")){
						preferences.setEmisorPdf(Boolean.TRUE);
						preferences.setEmisorXml(Boolean.FALSE);
						preferences.setEmisorXmlPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setEmisorPdf(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("receptor_xml_pdf")){
					if(value.equalsIgnoreCase("on")){
						preferences.setReceptorXmlPdf(Boolean.TRUE);
						preferences.setReceptorXml(Boolean.FALSE);
						preferences.setReceptorPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setReceptorXmlPdf(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("receptor_xml")){
					if(value.equalsIgnoreCase("on")){
						preferences.setReceptorXml(Boolean.TRUE);
						preferences.setReceptorXmlPdf(Boolean.FALSE);
						preferences.setReceptorPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setReceptorXml(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("receptor_pdf")){
					if(value.equalsIgnoreCase("on")){
						preferences.setReceptorPdf(Boolean.TRUE);
						preferences.setReceptorXml(Boolean.FALSE);
						preferences.setReceptorXmlPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setReceptorPdf(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("contacto_xml_pdf")){
					if(value.equalsIgnoreCase("on")){
						preferences.setContactoXmlPdf(Boolean.TRUE);
						preferences.setContactoXml(Boolean.FALSE);
						preferences.setContactoPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setContactoXmlPdf(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("contacto_xml")){
					if(value.equalsIgnoreCase("on")){
						preferences.setContactoXml(Boolean.TRUE);
						preferences.setContactoXmlPdf(Boolean.FALSE);
						preferences.setContactoPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setContactoXml(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("contacto_pdf")){
					if(value.equalsIgnoreCase("on")){
						preferences.setContactoPdf(Boolean.TRUE);
						preferences.setContactoXml(Boolean.FALSE);
						preferences.setContactoXmlPdf(Boolean.FALSE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setContactoPdf(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("registrar_cliente")){
					if(value.equalsIgnoreCase("on")){
						preferences.setRegistrarCliente(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setRegistrarCliente(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("actualizar_cliente")){
					if(value.equalsIgnoreCase("on")){
						preferences.setActualizarCliente(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setActualizarCliente(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("registrar_concepto")){
					if(value.equalsIgnoreCase("on")){
						preferences.setRegistrarConcepto(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setRegistrarConcepto(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("actualizar_concepto")){
					if(value.equalsIgnoreCase("on")){
						preferences.setActualizarConcepto(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setActualizarConcepto(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("registrar_regimen_fiscal")){
					if(value.equalsIgnoreCase("on")){
						preferences.setRegistrarRegimenFiscal(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setRegistrarRegimenFiscal(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("actualizar_regimen_fiscal")){
					if(value.equalsIgnoreCase("on")){
						preferences.setActualizarRegimenFiscal(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setActualizarRegimenFiscal(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("reutilizar_folio_cancelado")){
					if(value.equalsIgnoreCase("on")){
						preferences.setReutilizarFolioCancelado(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setReutilizarFolioCancelado(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("incrementar_folio_final")){
					if(value.equalsIgnoreCase("on")){
						preferences.setIncrementarFolioFinal(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setIncrementarFolioFinal(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("registrar_actualizar_lugar_expedicion")){
					if(value.equalsIgnoreCase("on")){
						preferences.setRegistrarActualizarLugarExpedicion(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setRegistrarActualizarLugarExpedicion(Boolean.FALSE);
					}
				}else if(option.equalsIgnoreCase("receptor_prefactura_pdf")){
					if(value.equalsIgnoreCase("on")){
						preferences.setPrefacturaPdf(Boolean.TRUE);
					}else if(value.equalsIgnoreCase("off")){
						preferences.setPrefacturaPdf(Boolean.FALSE);
					}
				}
				persistenceDAO.update(preferences);
				
				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.ACTUALIZACION_PREFERENCIAS_ID52);
				
				// Timeline :: Registro de accion
				timelineLog.timelineLog(EventTypeCode.ACTUALIZACION_PREFERENCIAS_ID27);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		}
		return !UValidator.isNullOrEmpty(result) ? result : UBase64.base64Encode("PREFERENCES_UPDATED_FAILED");
	}
}
