package org.megapractical.invoicing.webapp.exposition.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ClienteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PaisEntity;
import org.megapractical.invoicing.dal.data.repository.ICountryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICustomerJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.exposition.service.customer.CustomerService;
import org.megapractical.invoicing.webapp.json.JCustomer;
import org.megapractical.invoicing.webapp.json.JCustomer.Customer;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.notification.Toastr.ToastrType;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.ui.Paginator;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WCustomer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import lombok.val;

@Controller
@Scope("session")
public class EmitterCustomerController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	DataStoreService dataStore;
	
	@Autowired
	private CustomerService customerService;
	
	@Resource
	ICustomerJpaRepository iCustomerJpaRepository;
	
	@Resource
	ICountryJpaRepository iCountryJpaRepository;
	
	private static final String RFC_EXPRESSION = "[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?";
	private static final String SORT_PROPERTY = "rfc";
	private static final Integer INITIAL_PAGE = 0;
	private static final List<String> RESIDENT = Arrays.asList("Nacional", "Extranjero");
	
	private String configAction;
	private String unexpectedError;
	
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	@ModelAttribute("processingRequest")
	public String processingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@ModelAttribute("updatingRequest")
	public String updatingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.updating", userTools.getLocale());
	}
	
	// Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() throws IOException{
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	// Cargando residencia
	@ModelAttribute("residenteSource")
	public List<String> residenteSource(){
		return RESIDENT;
	}
	
	@RequestMapping(value = "/emitterCustomer", method = RequestMethod.GET)
	public String emitterCustomer(Model model, Integer page, String searchString) {
		try {
			
			this.unexpectedError = UProperties.genericUnexpectedError(userTools.getLocale());
			
			model.addAttribute("page", INITIAL_PAGE);
			
			HtmlHelper.functionalitySelected("EmitterConfig", "emitter-config-customer");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			return "/config/EmitterCustomer";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/emitterCustomer", method = RequestMethod.POST)
	public @ResponseBody String emitterCustomer(HttpServletRequest request) {
		
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JCustomer customer = new JCustomer();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchRfc = UBase64.base64Decode(request.getParameter("searchRfc"));
			if(UValidator.isNullOrEmpty(searchRfc)){
				searchRfc = "";
			}
			
			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Page<ClienteEntity> customerPage = null;
			Paginator<ClienteEntity> paginator = null;
			
			customerPage = iCustomerJpaRepository.findByContribuyenteAndRfcContainingIgnoreCaseAndEliminadoFalse(taxpayer, searchRfc, pageRequest);
			val pagination = JPagination.buildPaginaton(customerPage);
			customer.setPagination(pagination);
			
			for (val item : customerPage) {
				val postalCode = customerService.getPostalCodeEncoded(item);
				val taxRegime = customerService.getTaxRegimeEncoded(item);
				
				val customerObj = new Customer();
				customerObj.setId(UBase64.base64Encode(item.getId().toString()));
				customerObj.setIdEmitter(UBase64.base64Encode(item.getContribuyente().getId().toString()));
				customerObj.setRfc(UBase64.base64Encode(item.getRfc()));
				customerObj.setNameOrBusinessName(UBase64.base64Encode(item.getNombreRazonSocial()));
				customerObj.setCurp(UBase64.base64Encode(item.getCurp()));
				customerObj.setEmail(UBase64.base64Encode(item.getCorreoElectronico()));
				customerObj.setResident(UBase64.base64Encode(item.getResidente()));
				if(!UValidator.isNullOrEmpty(item.getPais())){
					customerObj.setResidency(UBase64.base64Encode(item.getPais().getCodigo().concat(" (").concat(item.getPais().getValor()).concat(")")));
				}
				customerObj.setIdentityRegistrationNumber(UBase64.base64Encode(item.getNumeroRegistroIdentidadFiscal()));
				customerObj.setPostalCode(postalCode);
				customerObj.setTaxRegime(taxRegime);
				
				customer.getCustomers().add(customerObj);
			}
			
			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if(!UValidator.isNullOrEmpty(searchRfc)){
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
			}
			
			if(customer.getCustomers().isEmpty()){
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}
			
			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		sessionController.sessionUpdate();
		
		customer.setResponse(response);
		jsonInString = gson.toJson(customer);
		return jsonInString;
	}

	@RequestMapping(value = "/emitterCustomer", params = { "customerDataLoad" })
	public @ResponseBody String emitterCustomerDataLoad(HttpServletRequest request) throws IOException {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JCustomer customer = new JCustomer();

		String jsonInString = null;
		Gson gson = new Gson();

		try {

			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);

			val customerEntity = iCustomerJpaRepository.findByIdAndEliminadoFalse(id);
			if (!UValidator.isNullOrEmpty(customerEntity)) {
				val postalCode = customerService.getPostalCodeEncoded(customerEntity);
				val taxRegime = customerService.getTaxRegimeEncoded(customerEntity);
				
				val customerObj = new Customer();
				customerObj.setRfc(UBase64.base64Encode(customerEntity.getRfc()));
				customerObj.setNameOrBusinessName(UBase64.base64Encode(customerEntity.getNombreRazonSocial()));
				customerObj.setCurp(UBase64.base64Encode(customerEntity.getCurp()));
				customerObj.setEmail(UBase64.base64Encode(customerEntity.getCorreoElectronico()));
				customerObj.setResident(UBase64.base64Encode(customerEntity.getResidente()));
				if (!UValidator.isNullOrEmpty(customerEntity.getPais())) {
					customerObj.setResidency(UBase64.base64Encode(customerEntity.getPais().getCodigo().concat(" (")
							.concat(customerEntity.getPais().getValor()).concat(")")));
				} else {
					customerObj.setResidency(null);
				}
				customerObj.setIdentityRegistrationNumber(
						UBase64.base64Encode(customerEntity.getNumeroRegistroIdentidadFiscal()));
				customerObj.setPostalCode(postalCode);
				customerObj.setTaxRegime(taxRegime);
				customer.setCustomer(customerObj);

				response.setSuccess(Boolean.TRUE);
			} else {
				notification.setMessage(UProperties.getMessage("ERR0079", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());

				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}

		} catch (Exception e) {
			e.printStackTrace();

			notification.setMessage(UProperties.getMessage("ERR0079", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());

			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}

		sessionController.sessionUpdate();

		customer.setResponse(response);
		jsonInString = gson.toJson(customer);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterCustomer", params={"customerRemove"})
    public @ResponseBody String emitterCustomerRemove(HttpServletRequest request) throws IOException {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JCustomer customer = new JCustomer();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
        	
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			ClienteEntity customerEntity = iCustomerJpaRepository.findByIdAndEliminadoFalse(id);
			if(!UValidator.isNullOrEmpty(customerEntity)){
				customerEntity.setEliminado(Boolean.TRUE);
				persistenceDAO.update(customerEntity);
				
				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.ELIMINAR_CLIENTE_ID47);
				
				// Timeline :: Registro de accion
				timelineLog.timelineLog(EventTypeCode.ELIMINAR_CLIENTE_ID22);
				
				response.setSuccess(Boolean.TRUE);
			}else{
				notification.setMessage(UProperties.getMessage("ERR0082", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}			
        	
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0082", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}		
		
		sessionController.sessionUpdate();
		
		customer.setResponse(response);
		jsonInString = gson.toJson(customer);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterCustomer", params={"customerAddEdit"})
    public @ResponseBody String emitterCustomerAddEdit(HttpServletRequest request) throws IOException {
		JCustomer customerJson = new JCustomer();
		Customer customer = new Customer();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			this.configAction = UBase64.base64Decode(request.getParameter("operation"));
			this.unexpectedError = this.configAction.equals("config") ? UProperties.getMessage("ERR0080", userTools.getLocale()) : UProperties.getMessage("ERR0081", userTools.getLocale());
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Boolean customerDeletedFinded = false;
			Boolean errorMarked = false;
			
			ClienteEntity customerEntity = new ClienteEntity();
			
			if(request.getParameter("objCustomer") != null){
				String objCustomer = request.getParameter("objCustomer");
				customer = gson.fromJson(objCustomer, Customer.class);
				
				//##### Id
				Long customerId = null;
				String index = UBase64.base64Decode(customer.getId());
				if(!UValidator.isNullOrEmpty(index)){
					customerId = UValue.longValue(index);
				}
				
				//##### RFC
				String rfc = UBase64.base64Decode(customer.getRfc());
				error = new JError();
				
				if(UValidator.isNullOrEmpty(rfc)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else if(rfc.toString().length() < 12 || !rfc.toString().matches(RFC_EXPRESSION)){
					error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
					errorMarked = true;
				}else{
					if(this.configAction.equals("config")){
						ClienteEntity customerObj = iCustomerJpaRepository.findByContribuyenteAndRfcIgnoreCaseAndEliminadoFalse(taxpayer, rfc);
						if(!UValidator.isNullOrEmpty(customerObj)){
							error.setMessage(UProperties.getMessage("ERR0083", userTools.getLocale()));
							errorMarked = true;
						}else{
							customerObj = iCustomerJpaRepository.findByContribuyenteAndRfcIgnoreCaseAndEliminadoTrue(taxpayer, rfc);
							if(!UValidator.isNullOrEmpty(customerObj)){
								customerEntity = customerObj;
								customerDeletedFinded = true;
							}else{
								customerEntity = new ClienteEntity();
							}							
						}
					}else if(this.configAction.equals("edit")){
						customerEntity = iCustomerJpaRepository.findByIdAndEliminadoFalse(customerId);
						String customerRfc = customerEntity.getRfc();
						if(!rfc.equalsIgnoreCase(customerRfc)){
							ClienteEntity customerObj = iCustomerJpaRepository.findByContribuyenteAndRfcIgnoreCaseAndEliminadoFalse(taxpayer, rfc);
							if(!UValidator.isNullOrEmpty(customerObj)){
								error.setMessage(UProperties.getMessage("ERR0083", userTools.getLocale()));
								errorMarked = true;
							}
						}
					}
				}
				if(errorMarked){
					error.setField("rfc");
					errors.add(error);
				}else{
					rfc = rfc.toUpperCase();
				}
				
				//##### Nombre o razon social
				String nameOrBusinessName = UValue.stringValue(UBase64.base64Decode(customer.getNameOrBusinessName()));
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(nameOrBusinessName)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}
				if(errorMarked){
					error.setField("name-business-name");
					errors.add(error);
				}else{
					nameOrBusinessName = nameOrBusinessName.toUpperCase();
				}
				
				//##### Email
				String email = UBase64.base64Decode(customer.getEmail());
				
				//##### Curp
				String curp = UValue.stringValue(UBase64.base64Decode(customer.getCurp()));
				if(!UValidator.isNullOrEmpty(curp)){
					curp = curp.toUpperCase();
				}
				
				//##### Residencia fiscal
				String resident = UBase64.base64Decode(customer.getResident());
				String residency = UBase64.base64Decode(customer.getResidency());
				error = new JError();
				errorMarked = false;
				
				PaisEntity residencyEntityByCode = null;
				
				if(resident.equals("-") || UValidator.isNullOrEmpty(resident)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					error.setField("resident");
					errors.add(error);
				}else{
					if(resident.equalsIgnoreCase("extranjero")){
						if(UValidator.isNullOrEmpty(residency)){
							error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
							errorMarked = true;
						}else{
							if(residency.equals("MEX (México)")){
								error.setMessage(UProperties.getMessage("ERR0085", userTools.getLocale()));
								errorMarked = true;
							}else{
								// Validar que sea un valor de catalogo
								String[] residencyCatalog = UCatalog.getCatalog(residency);  
								residency = residencyCatalog[0];
								String residencyValue = residencyCatalog[1];
								
								residencyEntityByCode = iCountryJpaRepository.findByCodigoAndEliminadoFalse(residency);
								PaisEntity residencyEntityByValue = iCountryJpaRepository.findByValorAndEliminadoFalse(residencyValue);
								if(residencyEntityByCode == null || residencyEntityByValue == null){
									String[] values = new String[]{"residencia fiscal", "c_Pais"};
									error.setMessage(UInvalidMessage.systemInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
									errorMarked = true;
								}
							}
						}
					}
				}
				if(errorMarked){
					error.setField("residency");
					errors.add(error);
				}
				
				//#####  Numero registro identidad fiscal:
				String identityRegistrationNumber = UValue.stringValue(UBase64.base64Decode(customer.getIdentityRegistrationNumber()));
				error = new JError();
				
				if(resident.equalsIgnoreCase("extranjero")){
					if(UValidator.isNullOrEmpty(identityRegistrationNumber)){
						error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
						error.setField("identity-registration-number");
						errors.add(error);
					}
				}
				
				if(!errors.isEmpty()){
					response.setError(Boolean.TRUE);
					response.setErrors(errors);
				}else{
					customerEntity.setContribuyente(taxpayer);
					customerEntity.setRfc(rfc);
					customerEntity.setNombreRazonSocial(nameOrBusinessName);
					customerEntity.setCorreoElectronico(email);
					customerEntity.setCurp(curp);
					customerEntity.setResidente(resident);
					customerEntity.setPais(residencyEntityByCode);
					customerEntity.setNumeroRegistroIdentidadFiscal(identityRegistrationNumber);
					customerEntity.setEliminado(Boolean.FALSE);
					
					WCustomer wCustomer = new WCustomer();
					wCustomer.setCustomer(customerEntity);
					wCustomer.setAction(this.configAction);
					wCustomer.setCustomerDeletedFinded(customerDeletedFinded);
					
					customerEntity = null;
					customerEntity = dataStore.customerStore(wCustomer);
					
					if(!UValidator.isNullOrEmpty(customerEntity)){
						OperationCode operationCode = null;
						EventTypeCode eventTypeCode = null;
						String toastrTitle = null;
						String toastrMessage = null;
						
						if(this.configAction.equals("edit")){
							operationCode = OperationCode.ACTUALIZACION_CLIENTE_ID46;
							eventTypeCode = EventTypeCode.ACTUALIZACION_CLIENTE_ID21;
							toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.customer.edit.title", userTools.getLocale());
							toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.customer.edit.message", userTools.getLocale());
						}else{						
							operationCode = OperationCode.REGISTRO_CLIENTE_ID45;
							eventTypeCode = EventTypeCode.REGISTRO_CLIENTE_ID20;
							toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.customer.config.title", userTools.getLocale());
							toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.customer.config.message", userTools.getLocale());
						}
						customer.setId(UBase64.base64Encode(customerEntity.getId().toString()));
						customerJson.setCustomer(customer);
						
						// Bitacora :: Registro de accion en bitacora
						appLog.logOperation(operationCode);
						
						// Timeline :: Registro de accion
						timelineLog.timelineLog(eventTypeCode);
						
						// Notificacion Toastr
						Toastr toastr = new Toastr();
						toastr.setTitle(toastrTitle);
						toastr.setMessage(toastrMessage);
						toastr.setType(ToastrType.SUCCESS.value());
						
						notification.setToastrNotification(Boolean.TRUE);
						notification.setToastr(toastr);
						
						response.setSuccess(Boolean.TRUE);
						response.setNotification(notification);
					}else{
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(this.unexpectedError);
						response.setNotification(notification);
						response.setError(Boolean.TRUE);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(this.unexpectedError);
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		customerJson.setResponse(response);
		jsonInString = gson.toJson(customerJson);
		
		return jsonInString;
	}
	
}