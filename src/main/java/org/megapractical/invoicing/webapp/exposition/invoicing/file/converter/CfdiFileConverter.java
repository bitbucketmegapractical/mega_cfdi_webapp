package org.megapractical.invoicing.webapp.exposition.invoicing.file.converter;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.webapp.json.JCfdiFile.CfdiFile;

public final class CfdiFileConverter {
	
	private CfdiFileConverter() {		
	}
	
	public static CfdiFile convert(ArchivoFacturaEntity entity) {
		if (entity == null) {
			return null;
		}
		
		return CfdiFile
				.builder()
				.id(UBase64.base64Encode(UValue.longString(entity.getId())))
				.cfdiType(UBase64.base64Encode("Pagos"))
				.fileType(UBase64.base64Encode(entity.getTipoArchivo().getValor()))
				.fileStatus(UBase64.base64Encode(entity.getEstadoArchivo().getValor()))
				.name(UBase64.base64Encode(entity.getNombre()))
				.path(UBase64.base64Encode(entity.getRuta()))
				.uploadDate(UBase64.base64Encode(UDate.formattedSingleDate(entity.getFechaCarga())))
				.compactedPath(UBase64.base64Encode(entity.getRutaCompactado()))
				.consolidatedPath(UBase64.base64Encode(entity.getRutaConsolidado()))
				.errorPath(UBase64.base64Encode(entity.getRutaError()))
				.build();
	}
	
}