package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiFileDetail;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoCancelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.payload.UploadFilePath;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class CancellationFileData {
	private ContribuyenteEntity taxpayer;
	private ContribuyenteCertificadoEntity certificate;
	private ArchivoCancelacionEntity cancellationFile;
	private UploadFilePath uploadFilePath;
	private ApiFileDetail fileDetail;
	private String acusesPath;
	private List<String> recipients;
}