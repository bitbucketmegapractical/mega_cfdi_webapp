package org.megapractical.invoicing.webapp.exposition.invoicing.validator.cfdiuse;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverCfdiUseResponse;
import org.megapractical.invoicing.webapp.json.JError;

public interface CfdiUseValidator {

	ReceiverCfdiUseResponse validate(String cfdiUse, String rfc);

	ReceiverCfdiUseResponse validate(String cfdiUse, String field, String rfc, List<JError> errors);

}