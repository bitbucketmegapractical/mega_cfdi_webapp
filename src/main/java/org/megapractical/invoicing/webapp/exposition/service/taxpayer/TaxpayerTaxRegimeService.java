package org.megapractical.invoicing.webapp.exposition.service.taxpayer;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;

public interface TaxpayerTaxRegimeService {
	
	ContribuyenteRegimenFiscalEntity taxRegime(ContribuyenteEntity taxpayer);
	
}