package org.megapractical.invoicing.webapp.exposition.cfdi.payload;

import org.megapractical.invoicing.sat.integration.v40.Comprobante;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AllowStamp {
	private Comprobante voucher;
	private boolean allowedToStamp;
	private String uuid;
	
	public static AllowStamp allowed() {
		return new AllowStamp(null, true, null);
	}
	
	public static AllowStamp allowed(Comprobante voucher) {
		return new AllowStamp(voucher, true, null);
	}
	
	public static AllowStamp notAllowed() {
		return new AllowStamp(null, false, null);
	}
	
	public static AllowStamp notAllowed(String uuid) {
		return new AllowStamp(null, false, uuid);
	}
	
}