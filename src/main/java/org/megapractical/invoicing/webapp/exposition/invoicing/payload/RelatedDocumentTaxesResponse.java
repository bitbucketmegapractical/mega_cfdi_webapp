package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.sat.integration.v40.Comprobante.Impuestos.Retenciones.Retencion;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Impuestos.Traslados.Traslado;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RelatedDocumentTaxesResponse {
	@Getter(AccessLevel.NONE)
	private List<Traslado> transferreds;
	@Getter(AccessLevel.NONE)
	private List<Retencion> withhelds;
	
	public List<Traslado> getTransferreds() {
		if (transferreds == null) {
			transferreds = new ArrayList<>();
		}
		return transferreds;
	}
	
	public List<Retencion> getWithhelds() {
		if (withhelds == null) {
			withhelds = new ArrayList<>();
		}
		return withhelds;
	}
	
}