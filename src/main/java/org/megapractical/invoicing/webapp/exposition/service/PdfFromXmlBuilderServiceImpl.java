package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.mapper.CfdiMapper;
import org.megapractical.invoicing.api.mapper.PayrollMapper;
import org.megapractical.invoicing.api.wrapper.ApiCfdiVoucher;
import org.megapractical.invoicing.api.wrapper.ApiPayrollVoucher;
import org.megapractical.invoicing.api.xml.XmlTransformer;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class PdfFromXmlBuilderServiceImpl implements PdfFromXmlBuilderService {
	
	private final CfdiMapper cfdiMapper;
	private final PayrollMapper payrollMapper;
	
	@Override
	public ApiCfdiVoucher buildCfdi(String xmlPath) {
		val response = XmlTransformer.transformToCfdi(xmlPath);
		if (response != null) {
			val voucher = response.getVoucher();
			val stampResponse = response.getStampResponse();
			val apiCfdi = cfdiMapper.map(voucher);
			if (apiCfdi != null) {
				apiCfdi.setStampResponse(stampResponse);
				return ApiCfdiVoucher
						.builder()
						.voucher(voucher)
						.cfdi(apiCfdi)
						.build();
			}
		}
		return null;
	}
	
	@Override
	public ApiPayrollVoucher buildPayroll(String xmlPath) {
		val response = XmlTransformer.transformToPayroll(xmlPath);
		if (response != null) {
			val voucher = response.getVoucher();
			val payroll = response.getPayroll();
			val stampResponse = response.getStampResponse();
			val apiPayroll = payrollMapper.map(voucher, payroll);
			if (apiPayroll != null) {
				apiPayroll.getApiCfdi().setStampResponse(stampResponse);
				return ApiPayrollVoucher
						.builder()
						.voucher(voucher)
						.payroll(apiPayroll)
						.cfdi(apiPayroll.getApiCfdi())
						.build();
			}
		}
		return null;
	}
	
}