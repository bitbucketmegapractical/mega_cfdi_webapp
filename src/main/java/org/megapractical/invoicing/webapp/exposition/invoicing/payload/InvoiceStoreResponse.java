package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceStoreResponse {
	private boolean stored;
	private String xmlDbPath;
	private String pdfDbPath;
}