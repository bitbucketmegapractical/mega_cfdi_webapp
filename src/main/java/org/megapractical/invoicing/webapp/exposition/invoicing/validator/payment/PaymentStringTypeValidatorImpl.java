package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.StringTypeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.StringTypeResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentStringTypeValidatorImpl implements PaymentStringTypeValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private StringTypeValidator stringTypeValidator;
	
	@Override
	public StringTypeResponse getStringType(String stringType, FormaPagoEntity paymentWay) {
		String stringTypeCode = null;
		if (!UValidator.isNullOrEmpty(stringType) && paymentWay != null) {
			val stringTypeValid = paymentWay.getTipoCadenaPago();
			if (!stringTypeValid.equalsIgnoreCase("no")) {
				val stringTypeValidatorResponse = stringTypeValidator.validate(stringType);
				stringTypeCode = stringTypeValidatorResponse.getStringTypeCode();
			}
		}
		return new StringTypeResponse(stringType, stringTypeCode);
	}

	@Override
	public StringTypeResponse getStringType(String stringType, String field, FormaPagoEntity paymentWay,
			List<JError> errors) {
		String stringTypeCode = null;
		JError error = null;
		
		if (!UValidator.isNullOrEmpty(stringType) && paymentWay != null) {
			val stringTypeValid = paymentWay.getTipoCadenaPago();
			if (stringTypeValid.equalsIgnoreCase("no")) {
				error = JError.error(field, "ERR1006", userTools.getLocale());
			} else {
				val stringTypeValidatorResponse = stringTypeValidator.validate(stringType, field);
				stringTypeCode = stringTypeValidatorResponse.getStringTypeCode();
				error = stringTypeValidatorResponse.getError();
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return new StringTypeResponse(stringType, stringTypeCode);
	}

}