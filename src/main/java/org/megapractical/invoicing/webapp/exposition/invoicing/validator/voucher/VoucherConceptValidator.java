package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherConceptRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherConceptResponse;
import org.megapractical.invoicing.webapp.json.JError;

public interface VoucherConceptValidator {
	
	VoucherConceptResponse getConcepts(VoucherConceptRequest voucherConceptRequest, List<JError> errors);
	
}