package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.dal.bean.jpa.ExportacionEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExportationValidatorResponse {
	private ExportacionEntity exportation;
	private JError error;
	private boolean hasError;
	
	public ExportationValidatorResponse(ExportacionEntity exportation) {
		this.exportation = exportation;
		this.hasError = Boolean.FALSE;
	}
	
	public ExportationValidatorResponse(ExportacionEntity exportation, JError error) {
		this.exportation = exportation;
		this.error = error;
		this.hasError = error != null;
	}
	
	public ExportationValidatorResponse(ExportacionEntity exportation, boolean hasError) {
		this.exportation = exportation;
		this.hasError = hasError;
	}
	
}