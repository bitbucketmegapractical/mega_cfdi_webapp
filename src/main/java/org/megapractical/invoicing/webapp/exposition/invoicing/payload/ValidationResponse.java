package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor	
public class ValidationResponse {
	private String data;
	private boolean error;
	
	public static final ValidationResponse error() {
		return ValidationResponse.builder().error(Boolean.TRUE).build();
	}
	
	public static final ValidationResponse empty() {
		return ValidationResponse.builder().error(Boolean.FALSE).build();
	}
	
}