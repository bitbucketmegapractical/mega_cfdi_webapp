package org.megapractical.invoicing.webapp.exposition.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.megapractical.common.datetime.DateUtils;
import org.megapractical.common.datetime.LocalDateUtils;
import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.common.ApiPayrollFileHeader.PayrollHeader;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;
import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction.PayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollEmitter;
import org.megapractical.invoicing.api.wrapper.ApiPayrollError;
import org.megapractical.invoicing.api.wrapper.ApiPayrollInability;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment.CompensationCreditBalances;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment.EmploymentSubsidy;
import org.megapractical.invoicing.api.wrapper.ApiPayrollParser;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPensionRetirement;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception.PayrollActionTitle;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception.PayrollExtraHour;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollSeparationCompensation;
import org.megapractical.invoicing.api.wrapper.ApiPayrollReceiver;
import org.megapractical.invoicing.api.wrapper.ApiPayrollReceiver.PayrollOutsourcing;
import org.megapractical.invoicing.api.wrapper.ApiPayrollVoucher;
import org.megapractical.invoicing.api.wrapper.ApiReceiver;
import org.megapractical.invoicing.dal.bean.datatype.StorageType;
import org.megapractical.invoicing.dal.bean.jpa.BancoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ClaveProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.EntidadFederativaEntity;
import org.megapractical.invoicing.dal.bean.jpa.OrigenRecursoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PeriodicidadPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.RiesgoPuestoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoArchivoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoContratoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoDeduccionEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoHoraEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoIncapacidadEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoJornadaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoOtroPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPercepcionEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoRegimenEntity;
import org.megapractical.invoicing.dal.bean.jpa.UnidadMedidaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsoCfdiEntity;
import org.megapractical.invoicing.dal.data.repository.IBankJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiUseJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IContractJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IDayTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IDeductionTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IFederalEntityJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IHourTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IInabilityTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IJobRiskJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IKeyProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IMeasurementUnitJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IOtherPaymentTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentFrequencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentMethodJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentWayJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollEntryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollFieldJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPerceptionTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPostalCodeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRegimeTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IResourceOriginJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IVoucherTypeJpaRepository;
import org.megapractical.invoicing.sat.complement.payroll.Nomina;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.CfdiRelacionados.CfdiRelacionado;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Complemento;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PayrollEntryBuilderService {
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Resource
	ICfdiUseJpaRepository iCfdiUseJpaRepository;
	
	@Resource
	IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository;
	
	@Resource
	IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository;
	
	@Resource
	IPayrollEntryJpaRepository iPayrollEntryJpaRepository;
	
	@Resource
	IVoucherTypeJpaRepository iVoucherTypeJpaRepository;
	
	@Resource
	IPaymentWayJpaRepository iPaymentWayJpaRepository;
	
	@Resource
	IPaymentMethodJpaRepository iPaymentMethodJpaRepositoy;
	
	@Resource
	ICurrencyJpaRepository iCurrencyJpaRepository;
	
	@Resource
	IPayrollTypeJpaRepository iPayrollTypeJpaRepository;
	
	@Resource
	IResourceOriginJpaRepository iResourceOriginJpaRepository;
	
	@Resource
	IContractJpaRepository iContractJpaRepository;
	
	@Resource
	IDayTypeJpaRepository iDayTypeJpaRepository;
	
	@Resource
	IRegimeTypeJpaRepository iRegimeTypeJpaRepository;
	
	@Resource
	IJobRiskJpaRepository iJobRiskJpaRepository;
	
	@Resource
	IPaymentFrequencyJpaRepository iPaymentFrequencyJpaRepository;
	
	@Resource
	IBankJpaRepository iBankJpaRepository;
	
	@Resource
	IFederalEntityJpaRepository iFederalEntityJpaRepository;
	
	@Resource
	IPerceptionTypeJpaRepository iPerceptionTypeJpaRepository;
	
	@Resource
	IHourTypeJpaRepository iHourTypeJpaRepository;
	
	@Resource
	IDeductionTypeJpaRepository iDeductionTypeJpaRepository;
	
	@Resource
	IOtherPaymentTypeJpaRepository iOtherPaymentTypeJpaRepository;
	
	@Resource
	IInabilityTypeJpaRepository iInabilityTypeJpaRepository;
	
	@Resource
	IPostalCodeJpaRepository iPostalCodeJpaRepository;
	
	@Resource
	IPayrollFieldJpaRepository iPayrollFieldJpaRepository;
		
	private static final String VOUCHER_VERSION = "4.0";
	private static final String PAYROLL_VERSION = "1.2";
	private static final String VOUCHER_CURRENCY_CODE = "MXN";
	private static final String VOUCHER_TYPE_CODE = "N";
	private static final String VOUCHER_PAYMENT_METHOD_CODE = "PUE";
	private static final String VOUCHER_EXPORTATION_CODE = "01";
	private static final String RELATED_CFDI_RELATIONSHIP_TYPE_CODE = "04";
	private static final String RECEIVER_CFDI_USE_CODE = "CN01";
	private static final String RECEIVER_TAX_REGIME = "605";
	private static final String CONCEPT_PRODUCT_SERVICE_CODE = "84111505";
	private static final String CONCEPT_QUANTITY = "1";
	private static final String CONCEPT_UNIT_CODE = "ACT";
	private static final String CONCEPT_DESCRIPTION = "Pago de nómina";
	private static final String CONCEPT_OBJ_IMP = "01";
	
	private static final String PAYROLL_FILE_TXT_CODE = "N12TXT";
	private static final String PAYROLL_FILE_CSV_CODE = "N12CSV";
	
	private static final String NULL = null;
	private final BigDecimal zeroDecimal = UValue.bigDecimal(NULL);
	
	private Comprobante comprobante;
	private Comprobante.CfdiRelacionados cfdiRelacionados;
	private Comprobante.Emisor comprobanteEmisor;
	private Comprobante.Receptor comprobanteReceptor;
	private Comprobante.Conceptos comprobanteConceptos;
	private Comprobante.Conceptos.Concepto comprobanteConcepto;
	
	// Complemento Nomina v1.2
	private Nomina nomina;
	private Nomina.Emisor nominaEmisor;
	private Nomina.Emisor.EntidadSNCF nominaEmisorEntidadSNCF;
	private Nomina.Receptor nominaReceptor;
	private Nomina.Receptor.SubContratacion nominaReceptorSubContratacion;
	private Nomina.Percepciones nominaPercepciones;	
	private Nomina.Percepciones.JubilacionPensionRetiro nominaPercepcionesJubilacionPensionRetiro;
	private Nomina.Percepciones.SeparacionIndemnizacion nominaPercepcionesSeparacionIndemnizacion;
	private Nomina.Percepciones.Percepcion nominaPercepcionesPercepcion;
	private Nomina.Percepciones.Percepcion.AccionesOTitulos nominaPercepcionesPercepcionAccionesOTitulos;
	private Nomina.Percepciones.Percepcion.HorasExtra nominaPercepcionesPercepcionHorasExtra;
	private Nomina.Deducciones nominaDeducciones;
	private Nomina.Deducciones.Deduccion nominaDeduccionesDeduccion;
	private Nomina.OtrosPagos nominaOtrosPagos;
	private Nomina.OtrosPagos.OtroPago nominaOtrosPagosOtroPago;
	private Nomina.OtrosPagos.OtroPago.SubsidioAlEmpleo nominaOtrosPagosOtroPagoSubsidioAlEmpleo;
	private Nomina.OtrosPagos.OtroPago.CompensacionSaldosAFavor nominaOtrosPagosOtroPagoCompensacionSaldosAFavor;
	private Nomina.Incapacidades nominaIncapacidades;
	private Nomina.Incapacidades.Incapacidad nominaIncapacidadesIncapacidad;
	
	private static HashMap<String, Nomina.Percepciones.Percepcion> perceptionMap = new HashMap<String, Nomina.Percepciones.Percepcion>();
	private HashMap<String, PayrollPerception> payrollPerceptionMap = new HashMap<String, PayrollPerception>();
	
	//##### CSV file
	private static List<PayrollPerception> payrollPerceptions = new ArrayList<>();
	private static List<Nomina.Percepciones.Percepcion> perceptionExtraHours = new ArrayList<>();
	
	private void voucherInstance() {
		try {
			
			this.comprobante = new Comprobante();
			this.cfdiRelacionados = new Comprobante.CfdiRelacionados();
			this.comprobanteEmisor = new Comprobante.Emisor();
			this.comprobanteReceptor = new Comprobante.Receptor();
			this.comprobanteConceptos = new Comprobante.Conceptos();
			this.comprobanteConcepto = new Comprobante.Conceptos.Concepto();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<ApiPayrollVoucher> payrollBuild(ApiPayrollParser payrollParser) {
		// Current environment
		val env = UProperties.env();
		
		userTools.log("[INFO] PAYROLL PARSER :: GENERATING VOUCHERS STARTING...");
		try {
			
			Integer line = 0;
			voucherInstance();
			
			//##### Determinando tipo de archivo
			TipoArchivoEntity fileType = payrollParser.getPayrollStamp().getPayrollFile().getTipoArchivo();
			String fileTypeCode = fileType.getCodigo();
			
			payrollParser.getStatistics().getPayrollFileStatistics().setFechaInicioGeneracionComprobante(new Date());
			payrollParser.getStatistics().getPayrollFileStatistics().setHoraInicioGeneracionComprobante(new Date());
			
			//##### Listado de comprobantes
			List<ApiPayrollVoucher> vouchers = new ArrayList<>();
			
			//##### Listado de tramas para generar los comprobantes
    		val entries = new ArrayList<String[]>();
    		if (payrollParser.getStatistics().getEntries() != null && !payrollParser.getStatistics().getEntries().isEmpty()) {
    			entries.addAll(payrollParser.getStatistics().getEntries());
    		}
    		
    		String receiverEmail = null;
			String legend = null;
			
			//##### Informacion asociada al comprobante
			ApiCfdi apiCfdi = null;
			ApiVoucher apiVoucher = null;
			ApiReceiver apiReceiver = null;
			List<ApiConcept> apiConcepts = null;
			ApiRelatedCfdi apiRelatedCfdi = null; 
			
			List<CfdiRelacionado> uuids = new ArrayList<>();
    		
			ApiPayroll apiPayroll = null;
			
			ApiPayrollPerception apiPayrollPerception = null;
			PayrollPerception payrollPerception = null;
			
			ApiPayrollDeduction wPayrollDeduction = null;
			PayrollDeduction payrollDeduction = null;
			
			List<ApiPayrollOtherPayment> apiPayrollOtherPayments = null;
			List<ApiPayrollInability> apiPayrollInabilities = null;
			ApiPayrollReceiver payrollReceiver = null;
			
			Boolean nextEntrieIsPrincipal = false;
			boolean endEntry = false;
			
			boolean validationError = false;
			String validationErrorField = null;
			String validationErrorCode = null;
			String validationErrorMessage = null;
			
			Integer errorPayroll = 0;
			
			String[] entry = null;
			for (int i = 0; i < entries.size(); i++) {
    			
    			try {
    				
    				//##### Trama
        			entry = entries.get(i);
    				
        			//##### Archivo TXT
    				if (fileTypeCode.equalsIgnoreCase(PAYROLL_FILE_TXT_CODE)) {
    					//##### @Entry(code = "CNOM", name = "Complemento nomina")
            			//##### @Description("Trama principal del complemento nomina.")
            			if (entry[0].equalsIgnoreCase("CNOM")) {
        	    			apiCfdi = new ApiCfdi();
        	    			apiVoucher = new ApiVoucher();
        	    			
        	    			apiConcepts = new ArrayList<>();
        	    			apiRelatedCfdi = new ApiRelatedCfdi();
        	    			
        	    			apiPayroll = new ApiPayroll();
        	    			
        	    			apiPayrollOtherPayments = null;
        	    			apiPayrollInabilities = null;
        	    			
        	    			this.nominaReceptor = null;
        	    			apiReceiver = null;
        	    			payrollReceiver = null;
        	    			
        	    			receiverEmail = null;
        	    			legend = null;
        	    			
        	    			uuids = new ArrayList<>();
        	    			
        	    			this.nominaPercepciones = null;
        	    			this.nominaPercepcionesPercepcion = null;
        	    			apiPayrollPerception = null;
        	    			payrollPerception = null;
        	    			
        	    			this.nominaDeducciones = null;
        	    			this.nominaDeduccionesDeduccion = null;
        	    			wPayrollDeduction = null;
        	    			payrollDeduction = null;
        	    			
        	    			this.nominaOtrosPagos = null;
        	    			this.nominaIncapacidades = null;
        	    			
        	    			validationError = false;
        	    			
        	    			//##### Informacion del comprobante
        	    			this.comprobante = new Comprobante();
        	    			
        	    			this.comprobante.setVersion(VOUCHER_VERSION);
        	    			apiVoucher.setVersion(VOUCHER_VERSION);
        	    			
        	    			this.comprobante.setTipoDeComprobante(UCatalog.tipoDeComprobante(VOUCHER_TYPE_CODE));
        	    			apiCfdi.setVoucherType(iVoucherTypeJpaRepository.findByCodigoAndEliminadoFalse(VOUCHER_TYPE_CODE));
        	    			
        	    			this.comprobante.setMoneda(UCatalog.moneda(VOUCHER_CURRENCY_CODE));
        	    			apiCfdi.setCurrency(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(VOUCHER_CURRENCY_CODE));
        	    			
        	    			this.comprobante.setMetodoPago(UCatalog.metodoPago(VOUCHER_PAYMENT_METHOD_CODE));
        	    			apiCfdi.setPaymentMethod(iPaymentMethodJpaRepositoy.findByCodigoAndEliminadoFalse(VOUCHER_PAYMENT_METHOD_CODE));
        	    			
        	    			String emitterSeal = UCertificateX509.certificateSeal(payrollParser.getPayrollStamp().getApiEmitter().getCertificado(), payrollParser.getPayrollStamp().getApiEmitter().getLlavePrivada(), payrollParser.getPayrollStamp().getApiEmitter().getPasswd());
        	    			this.comprobante.setSello(emitterSeal);
        	    			apiVoucher.setEmitterSeal(emitterSeal);
        	    			
        	    			this.comprobante.setCertificado(UCertificateX509.certificateBase64(payrollParser.getPayrollStamp().getApiEmitter().getCertificado()));
        	    			
        	    			String certificateEmitterNumber = UCertificateX509.certificateSerialNumber(payrollParser.getPayrollStamp().getApiEmitter().getCertificado());
        	    			this.comprobante.setNoCertificado(certificateEmitterNumber);
        	    			apiVoucher.setCertificateEmitterNumber(certificateEmitterNumber);
        	    			
        	    			this.comprobante.setFecha(UDate.formattedDate());
        	    			apiVoucher.setDateTimeExpedition(UDate.formattedDate());
        	    			
        	    			this.comprobante.setExportacion(VOUCHER_EXPORTATION_CODE);
        	    			apiVoucher.setExportation(VOUCHER_EXPORTATION_CODE);
        	    	        
        	    			//##### Informacion asociada al comprobante
        	    			apiCfdi.setEmitter(payrollParser.getPayrollStamp().getApiEmitter());
            			}
            			
            			//##### @Entry(code = "H01", name = "Informacion complementaria")
            			//##### @Description("Trama referida al correo del trabajar y la leyenda del comprobante.")
            			if (entry[0].equalsIgnoreCase("H01")) {
            				apiReceiver = new ApiReceiver();
            				
            				receiverEmail = UValue.stringValue(entry[1]);
            				apiReceiver.setCorreoElectronico(receiverEmail);
            				
            				legend = UValue.stringValue(entry[2]);
            				apiCfdi.setLegend(legend);
            			}
            			
            			//##### @Entry(code = "TA", name = "Comprobante")
            			//##### @Description("Trama referida a la informacion del comprobante.")
            			if (entry[0].equalsIgnoreCase("TA")) {
            				String serie = UValue.stringValue(entry[1]);
            				this.comprobante.setSerie(serie);
            				apiVoucher.setSerie(serie);
            				
            				String sheet = UValue.stringValue(entry[2]);
            				this.comprobante.setFolio(sheet);
            				apiVoucher.setFolio(sheet);
            				
            				BigDecimal subTotal = UValue.bigDecimalStrict(UValue.stringValue(entry[3]));
            				this.comprobante.setSubTotal(subTotal);
            				apiVoucher.setSubTotal(subTotal);
            				apiVoucher.setSubTotalString(UValue.bigDecimalString(UValue.stringValue(entry[3])));
            				
            				BigDecimal discount = UValue.bigDecimalStrict(UValue.stringValue(entry[4]));
            				this.comprobante.setDescuento(discount);
            				apiVoucher.setDiscount(discount);
            				apiVoucher.setDiscountString(UValue.bigDecimalString(UValue.stringValue(entry[4])));
            				
            				BigDecimal amount = UValue.bigDecimalStrict(UValue.stringValue(entry[5]));
            				this.comprobante.setTotal(amount);
            				apiVoucher.setTotal(amount);
            				apiVoucher.setTotalString(UValue.bigDecimalString(UValue.stringValue(entry[5])));
            				
            				String postalCode = UValue.stringValue(entry[6]);
            				this.comprobante.setLugarExpedicion(postalCode);
            				apiVoucher.setPostalCode(postalCode);
            				apiCfdi.setPostalCode(iPostalCodeJpaRepository.findByCodigoAndEliminadoFalse(postalCode));
            				
            				apiCfdi.setVoucher(apiVoucher);
            			}
            			
            			//##### @Entry(code = "TRS", name = "Comprobante :: CFDI relacionados")
            			//##### @Description("Trama referida a la informacion de los cfdi relacionados al comprobante.")
            			//##### @Type(type = "list")
            			if (entry[0].equalsIgnoreCase("TRS")) {
            				String uuid = UValue.stringValue(entry[1]);
            						
            				CfdiRelacionado relatedCfdi = new CfdiRelacionado();
        					relatedCfdi.setUUID(uuid);
        					uuids.add(relatedCfdi);
        					
        					RelatedCfdiUuid relatedCfdiUuid = new RelatedCfdiUuid();
        					relatedCfdiUuid.setUuid(uuid);
        					apiRelatedCfdi.getUuids().add(relatedCfdiUuid);
            			}
            			
            			//##### @Entry(code = "TRE", name = "Comprobante :: Receptor")
            			//##### @Description("Trama referida a la informacion del receptor del comprobante.")
            			if (entry[0].trim().equalsIgnoreCase("TRE")) {
            				this.comprobanteReceptor = new Comprobante.Receptor();
            				
            				String rfc = UValue.stringValue(entry[1]);
            				this.comprobanteReceptor.setRfc(rfc);
            				apiReceiver.setRfc(UValue.stringValueUppercase(rfc));
            				
            				String name = UValue.stringValue(entry[2]);
            				this.comprobanteReceptor.setNombre(name);
            				apiReceiver.setNombreRazonSocial(name);
            				
            				String address = UValue.stringValue(entry[3]);
            				this.comprobanteReceptor.setDomicilioFiscalReceptor(address);
            				apiReceiver.setDomicilioFiscal(address);
            				
            				this.comprobanteReceptor.setUsoCFDI(UCatalog.usoCFDI(RECEIVER_CFDI_USE_CODE));
            				UsoCfdiEntity cfdiUse = iCfdiUseJpaRepository.findByCodigoAndEliminadoFalse(RECEIVER_CFDI_USE_CODE);
            				apiReceiver.setUsoCFDI(cfdiUse);
            				
            				this.comprobanteReceptor.setRegimenFiscalReceptor(RECEIVER_TAX_REGIME);
            				apiReceiver.setTaxRegime(RECEIVER_TAX_REGIME);
            				
            				apiCfdi.setReceiver(apiReceiver);
            			}
            			
            			//##### @Entry(code = "TC", name = "Comprobante :: Concepto")
            			//##### @Description("Trama referida al concepto del comprobante.")
            			if (entry[0].trim().equalsIgnoreCase("TC")) {
            				this.comprobanteConcepto = new Comprobante.Conceptos.Concepto();
            				ApiConcept concept = new ApiConcept();
            				
            				this.comprobanteConcepto.setDescripcion(CONCEPT_DESCRIPTION);
            				concept.setDescription(CONCEPT_DESCRIPTION);
            				
            				this.comprobanteConcepto.setClaveProdServ(CONCEPT_PRODUCT_SERVICE_CODE);
            				ClaveProductoServicioEntity keyProductService = iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(CONCEPT_PRODUCT_SERVICE_CODE);
            				concept.setKeyProductServiceEntity(keyProductService);
            				concept.setKeyProductServiceCode(keyProductService.getCodigo());
            				concept.setKeyProductService(keyProductService.getValor());
            				
            				this.comprobanteConcepto.setClaveUnidad(CONCEPT_UNIT_CODE);
            				UnidadMedidaEntity unit = iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(CONCEPT_UNIT_CODE);
            				concept.setMeasurementUnitEntity(unit);
            				concept.setMeasurementUnitCode(unit.getCodigo());
            				concept.setMeasurementUnit(unit.getValor());
            				
            				BigDecimal quantity = UValue.bigDecimalStrict(CONCEPT_QUANTITY);
            				this.comprobanteConcepto.setCantidad(quantity);
            				concept.setQuantity(quantity);
            				
            				BigDecimal unitValue = UValue.bigDecimalStrict(UValue.stringValue(entry[1]));
            				this.comprobanteConcepto.setValorUnitario(unitValue);
            				concept.setUnitValue(unitValue);
            				concept.setUnitValueString(UValue.bigDecimalString(UValue.stringValue(entry[1])));
            				
            				BigDecimal amount = UValue.bigDecimalStrict(UValue.stringValue(entry[2]));
            				this.comprobanteConcepto.setImporte(amount);
            				concept.setAmount(amount);
            				concept.setAmountString(UValue.bigDecimalString(UValue.stringValue(entry[2])));
            				
            				BigDecimal discount = UValue.bigDecimalStrict(UValue.stringValue(entry[3]));
            				this.comprobanteConcepto.setDescuento(discount);
            				concept.setDiscount(discount);
            				concept.setDiscountString(UValue.bigDecimalString(UValue.stringValue(entry[3])));
            				
            				this.comprobanteConcepto.setObjetoImp(CONCEPT_OBJ_IMP);
            				concept.setObjImp(CONCEPT_OBJ_IMP);
            				
            				apiConcepts.add(concept);
            				apiCfdi.getConcepts().addAll(apiConcepts);
            			}
            			
            			//##### @Entry(code = "TNOM", name = "Nomina")
            			//##### @Description("Trama referida a la informacion de la nomina.")
            			if (entry[0].trim().equalsIgnoreCase("TNOM")) {
            				this.nomina = new Nomina();
            				this.nomina.setVersion(PAYROLL_VERSION);
            				
            				String payrollType = UValue.stringValue(entry[1]); 
            				this.nomina.setTipoNomina(UCatalog.payrollType(payrollType));
            				
            				TipoNominaEntity payrollTypeEntity = iPayrollTypeJpaRepository.findByCodigoAndEliminadoFalse(payrollType);
            				apiPayroll.setPayrollType(payrollTypeEntity);
            				apiPayroll.setPayrollTypeCode(payrollTypeEntity.getCodigo());
            				apiPayroll.setPayrollTypeValue(payrollTypeEntity.getValor());
            				
            				String paymentDate = UValue.stringValue(entry[2]);
            				this.nomina.setFechaPago(paymentDate);
            				apiPayroll.setPaymentDate(LocalDateUtils.parse(paymentDate));
            				apiPayroll.setPaymentDateString(DateUtils.customFormat(paymentDate));
            				    				
            				String paymentIntialDate = UValue.stringValue(entry[3]);
            				this.nomina.setFechaInicialPago(paymentIntialDate);
            				apiPayroll.setPaymentIntialDate(LocalDateUtils.parse(paymentIntialDate));
            				apiPayroll.setPaymentIntialDateString(DateUtils.customFormat(paymentIntialDate));
            				
            				String paymentEndDate = UValue.stringValue(entry[4]);
            				this.nomina.setFechaFinalPago(paymentEndDate);
            				apiPayroll.setPaymentEndDate(LocalDateUtils.parse(paymentEndDate));
            				apiPayroll.setPaymentEndDateString(DateUtils.customFormat(paymentEndDate));
            				
            				this.nomina.setNumDiasPagados(UValue.bigDecimalStrict(UValue.stringValue(entry[5])));
            				apiPayroll.setNumberDaysPaid(UValue.stringValue(entry[5]));
            				
            				BigDecimal totalPerceptions = UValue.bigDecimalStrict(UValue.stringValue(entry[6]));
            				this.nomina.setTotalPercepciones(totalPerceptions);
            				apiPayroll.setTotalPerceptions(UValue.bigDecimalString(UValue.stringValue(entry[6])));
            				
            				BigDecimal totalDeductions = UValue.bigDecimalStrict(UValue.stringValue(entry[7]));
            				this.nomina.setTotalDeducciones(totalDeductions);
            				apiPayroll.setTotalDeductions(UValue.bigDecimalString(UValue.stringValue(entry[7])));
            				
            				BigDecimal totalOtherPayments = UValue.bigDecimalStrict(UValue.stringValue(entry[8]));
            				this.nomina.setTotalOtrosPagos(totalOtherPayments);
            				apiPayroll.setTotalOtherPayments(UValue.bigDecimalString(UValue.stringValue(entry[8])));
            				
            				apiPayroll.setStorageType(StorageType.SERVER);
            			}
            			
            			//##### @Entry(code = "TNOME", name = "Nomina :: Emisor")
            			//##### @Description("Trama referida a la informacion del emisor de la nomina.")
            			if (entry[0].trim().equalsIgnoreCase("TNOME")) {
            				this.nominaEmisor = new Nomina.Emisor();
            				ApiPayrollEmitter payrollEmitter = new ApiPayrollEmitter();
            				
            				String curp = UValue.stringValue(entry[1]);
            				this.nominaEmisor.setCurp(curp);
            				payrollEmitter.setCurp(curp);
            				
            				String employerRegistration = UValue.stringValue(entry[2]);
            				this.nominaEmisor.setRegistroPatronal(employerRegistration);
            				payrollEmitter.setEmployerRegistration(employerRegistration);
            				
            				String rfcPatronOrigin = UValue.stringValue(entry[3]);
            				this.nominaEmisor.setRfcPatronOrigen(rfcPatronOrigin);
            				payrollEmitter.setRfcPatronOrigin(rfcPatronOrigin);
            				
            				if (!UValidator.isNullOrEmpty(UValue.stringValue(entry[4]))) {
            					this.nominaEmisorEntidadSNCF = new Nomina.Emisor.EntidadSNCF();
            					
            					String resourceOrigin = UValue.stringValue(entry[4]);
            					this.nominaEmisorEntidadSNCF.setOrigenRecurso(UCatalog.sourceResource(resourceOrigin));
            					
            					OrigenRecursoEntity resourceOriginEntity = iResourceOriginJpaRepository.findByCodigoAndEliminadoFalse(resourceOrigin);
            					payrollEmitter.setResourceOrigin(resourceOriginEntity);
            					payrollEmitter.setResourceOriginCode(resourceOriginEntity.getCodigo());
            					payrollEmitter.setResourceOriginValue(resourceOriginEntity.getValor());
            					
            					BigDecimal ownResourceAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[5]));
            					this.nominaEmisorEntidadSNCF.setMontoRecursoPropio(ownResourceAmount);
            					payrollEmitter.setOwnResourceAmount(UValue.bigDecimalString(UValue.stringValue(entry[5])));
            					
                				this.nominaEmisor.setEntidadSNCF(nominaEmisorEntidadSNCF);
            				}
            				
            				apiPayroll.setPayrollEmitter(payrollEmitter);
            			}
            			
            			//##### @Entry(code = "TNOMR", name = "Nomina :: Receptor")
            			//##### @Description("Trama referida a la informacion del receptor de la nomina.")
            			if (entry[0].trim().equalsIgnoreCase("TNOMR")) {
            				this.nominaReceptor = new Nomina.Receptor();
            				payrollReceiver = new ApiPayrollReceiver();
            				
            				String curp = UValue.stringValue(entry[1]);
            				this.nominaReceptor.setCurp(curp);
            				payrollReceiver.setCurp(curp);
            				
            				String socialSecurityNumber = UValue.stringValue(entry[2]);
            				this.nominaReceptor.setNumSeguridadSocial(socialSecurityNumber);
            				payrollReceiver.setSocialSecurityNumber(socialSecurityNumber);
            				
            				String startDateEmploymentRelationship = UValue.stringValue(entry[3]);
            				if (!UValidator.isNullOrEmpty(startDateEmploymentRelationship)) {
	            				this.nominaReceptor.setFechaInicioRelLaboral(startDateEmploymentRelationship);
	            				payrollReceiver.setStartDateEmploymentRelationship(LocalDateUtils.parse(startDateEmploymentRelationship));
	            				payrollReceiver.setStartDateEmploymentRelationshipString(DateUtils.customFormat(startDateEmploymentRelationship));
            				}
            				
            				String age = UValue.stringValue(entry[4]);
            				this.nominaReceptor.setAntigüedad(age);
            				payrollReceiver.setAge(UTools.jobAge(age));
            				
            				String contractType = UValue.stringValue(entry[5]);
            				this.nominaReceptor.setTipoContrato(contractType);
            				
            				TipoContratoEntity contractTypeEntity = iContractJpaRepository.findByCodigoAndEliminadoFalse(contractType);
            				payrollReceiver.setContractType(contractTypeEntity);
            				payrollReceiver.setContractTypeCode(contractTypeEntity.getCodigo());
            				payrollReceiver.setContractTypeValue(contractTypeEntity.getValor());
            				
            				String unionized = UValue.stringValue(entry[6]);
            				this.nominaReceptor.setSindicalizado(unionized);
            				payrollReceiver.setUnionized(unionized);
            				
            				String dayType = UValue.stringValue(entry[7]);
            				if (AssertUtils.hasValue(dayType)) {
            					this.nominaReceptor.setTipoJornada(dayType);
            					
            					TipoJornadaEntity dayTypeEntity = iDayTypeJpaRepository.findByCodigoAndEliminadoFalse(dayType);
            					payrollReceiver.setDayType(dayTypeEntity);
            					payrollReceiver.setDayTypeCode(dayTypeEntity.getCodigo());
            					payrollReceiver.setDayTypeValue(dayTypeEntity.getValor());
            				}
            				
            				String regimeType = UValue.stringValue(entry[8]);
            				this.nominaReceptor.setTipoRegimen(regimeType);
            				
            				TipoRegimenEntity regimeTypeEntity = iRegimeTypeJpaRepository.findByCodigoAndEliminadoFalse(regimeType);
            				payrollReceiver.setRegimeType(regimeTypeEntity);
            				payrollReceiver.setRegimeTypeCode(regimeTypeEntity.getCodigo());
            				payrollReceiver.setRegimeTypeValue(regimeTypeEntity.getValor());
            				
            				String employeeNumber = UValue.stringValue(entry[9]);
            				this.nominaReceptor.setNumEmpleado(employeeNumber);
            				payrollReceiver.setEmployeeNumber(employeeNumber);
            				
            				String department = UValue.stringValue(entry[10]);
            				this.nominaReceptor.setDepartamento(department);
            				payrollReceiver.setDepartment(department);
            				
            				String jobTitle = UValue.stringValue(entry[11]); 
            				this.nominaReceptor.setPuesto(jobTitle);
            				payrollReceiver.setJobTitle(jobTitle);
            				
            				if (!UValidator.isNullOrEmpty(UValue.stringValue(entry[12]))) {
	            				String jobRisk = UValue.stringValue(entry[12]);
	            				RiesgoPuestoEntity jobRiskEntity = iJobRiskJpaRepository.findByCodigoAndEliminadoFalse(jobRisk);
	            				if (!UValidator.isNullOrEmpty(jobRiskEntity)) {
	            					this.nominaReceptor.setRiesgoPuesto(jobRisk);
	            					
	            					payrollReceiver.setJobRisk(jobRiskEntity);
	            					payrollReceiver.setJobRiskCode(jobRiskEntity.getCodigo());
	            					payrollReceiver.setJobRiskValue(jobRiskEntity.getValor());
	            				} else {
	            					validationErrorField = "RiesgoPuesto";
	            					validationErrorCode = "NOM179";
	            					validationErrorMessage = "El valor del campo RiesgoPuesto no cumple con un valor del catálogo c_RiesgoPuesto";
	            					validationError = true;
	            				}
            				}
            				
            				String paymentFrequency = UValue.stringValue(entry[13]);
            				this.nominaReceptor.setPeriodicidadPago(paymentFrequency);
            				
            				PeriodicidadPagoEntity paymentFrequencyEntity = iPaymentFrequencyJpaRepository.findByCodigoAndEliminadoFalse(paymentFrequency);
            				payrollReceiver.setPaymentFrequency(paymentFrequencyEntity);
            				payrollReceiver.setPaymentFrequencyCode(paymentFrequencyEntity.getCodigo());
            				payrollReceiver.setPaymentFrequencyValue(paymentFrequencyEntity.getValor());
            				
            				String bankAccount = UValue.stringValue(entry[15]);
            				if (!UValidator.isNullOrEmpty(bankAccount)) {
            					if (bankAccount.length() < 18) {
            						String bank = UValue.stringValue(entry[14]);
            						this.nominaReceptor.setBanco(bank);
            						
            						if (!UValidator.isNullOrEmpty(bank)) {
	            						BancoEntity bankEntity = iBankJpaRepository.findByCodigoAndEliminadoFalse(bank);
	            						payrollReceiver.setBank(bankEntity);
	            						payrollReceiver.setBankCode(bankEntity.getCodigo());
	            						payrollReceiver.setBankValue(bankEntity.getValor());
            						}
            					}
            				}
            				
            				this.nominaReceptor.setCuentaBancaria(bankAccount);
            				payrollReceiver.setBankAccount(bankAccount);
            				
            				BigDecimal shareContributionBaseSalary = UValue.bigDecimalStrict(UValue.stringValue(entry[16]));
            				this.nominaReceptor.setSalarioBaseCotApor(shareContributionBaseSalary);
            				payrollReceiver.setShareContributionBaseSalary(UValue.bigDecimalString(UValue.stringValue(entry[16])));
            				
            				BigDecimal integratedDailySalary = UValue.bigDecimalStrict(UValue.stringValue(entry[17]));
            				this.nominaReceptor.setSalarioDiarioIntegrado(integratedDailySalary);
            				payrollReceiver.setIntegratedDailySalary(UValue.bigDecimalString(UValue.stringValue(entry[17])));
            				
            				String federalEntityCode = UValue.stringValue(entry[18]);
            				this.nominaReceptor.setClaveEntFed(UCatalog.estate(federalEntityCode));
            				
            				EntidadFederativaEntity federalEntity = iFederalEntityJpaRepository.findByCodigoAndEliminadoFalse(federalEntityCode);
            				payrollReceiver.setFederalEntity(federalEntity);
            				payrollReceiver.setFederalEntityCode(federalEntity.getCodigo());
            				payrollReceiver.setFederalEntityValue(federalEntity.getValor());
            			}
            			
            			//##### @Entry(code = "TNOMRS", name = "Nomina :: Receptor :: Subcontrataciones")
            			//##### @Description("Trama referida a la informacion de las subcontrataciones.")
            			//##### @Type(type = "list")
            			if (entry[0].trim().equalsIgnoreCase("TNOMRS")) {
            				this.nominaReceptorSubContratacion = new Nomina.Receptor.SubContratacion();
            				PayrollOutsourcing payrollOutsourcing = new PayrollOutsourcing();
            				
            				String rfcBoss = UValue.stringValue(entry[1]);
            				this.nominaReceptorSubContratacion.setRfcLabora(rfcBoss);
            				payrollOutsourcing.setRfcBoss(rfcBoss);
            				
            				BigDecimal percentageTime = UValue.bigDecimalStrict(UValue.stringValue(entry[2]));
            				this.nominaReceptorSubContratacion.setPorcentajeTiempo(percentageTime);
            				payrollOutsourcing.setPercentageTime(UValue.bigDecimalString(UValue.stringValue(entry[2])));
            				
            				if (!UValidator.isNullOrEmpty(this.nominaReceptor)) {
            					this.nominaReceptor.getSubContratacions().add(nominaReceptorSubContratacion);
            				}
            				
            				if (!UValidator.isNullOrEmpty(payrollReceiver)) {
            					payrollReceiver.getPayrollOutsourcings().add(payrollOutsourcing);
            				}
            			}
            			
            			//##### @Entry(code = "TPERS", name = "Nomina :: Percepciones")
            			//##### @Description("Trama referida a la informacion de los totales de percepciones.")
            			if (entry[0].trim().equalsIgnoreCase("TPERS")) {
            				this.nominaPercepciones = new Nomina.Percepciones();
            				apiPayrollPerception = new ApiPayrollPerception();
            				
            				BigDecimal totalSalaries = UValue.bigDecimalStrict(UValue.stringValue(entry[1]));
            				this.nominaPercepciones.setTotalSueldos(totalSalaries);
            				apiPayrollPerception.setTotalSalaries(UValue.bigDecimalString(UValue.stringValue(entry[1])));
            				
            				BigDecimal totalSeparationCompensation = UValue.bigDecimalStrict(UValue.stringValue(entry[2]));
            				this.nominaPercepciones.setTotalSeparacionIndemnizacion(totalSeparationCompensation);
            				apiPayrollPerception.setTotalSeparationCompensation(UValue.bigDecimalString(UValue.stringValue(entry[2])));
            				
            				BigDecimal totalPensionRetirement = UValue.bigDecimalStrict(UValue.stringValue(entry[3]));
            				this.nominaPercepciones.setTotalJubilacionPensionRetiro(totalPensionRetirement);
            				apiPayrollPerception.setTotalPensionRetirement(UValue.bigDecimalString(UValue.stringValue(entry[3])));
            				
            				BigDecimal totalTaxed = UValue.bigDecimalStrict(UValue.stringValue(entry[4]));
            				this.nominaPercepciones.setTotalGravado(totalTaxed);
            				apiPayrollPerception.setTotalTaxed(UValue.bigDecimalString(UValue.stringValue(entry[4])));
            				
            				BigDecimal totalExempt = UValue.bigDecimalStrict(UValue.stringValue(entry[5]));
            				this.nominaPercepciones.setTotalExento(totalExempt);
            				apiPayrollPerception.setTotalExempt(UValue.bigDecimalString(UValue.stringValue(entry[5])));
            			}
            			
            			//##### @Entry(code = "TPERC", name = "Nomina :: Percepciones :: Percepcion")
            			//##### @Description("Trama referida a la informacion de una percepcion.")
            			//##### @Type(type = "list")
            			if (entry[0].trim().equalsIgnoreCase("TPERC")) {
            				if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
            					if (!UValidator.isNullOrEmpty(this.nominaPercepcionesPercepcion)) {
            						this.nominaPercepciones.getPercepcions().add(this.nominaPercepcionesPercepcion);
            					}
            					
            					if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
            						if (!UValidator.isNullOrEmpty(payrollPerception)) {
            							apiPayrollPerception.getPayrollPerceptions().add(payrollPerception);
            						}
                				}
            				}
            				
            				this.nominaPercepcionesPercepcion = new Nomina.Percepciones.Percepcion();
            				payrollPerception = new PayrollPerception();
            				
            				String perceptionType = UValue.stringValue(entry[1]);
            				this.nominaPercepcionesPercepcion.setTipoPercepcion(perceptionType);
            				
            				TipoPercepcionEntity perceptionTypeEntity = iPerceptionTypeJpaRepository.findByCodigoAndEliminadoFalse(perceptionType);
            				payrollPerception.setPerceptionType(perceptionTypeEntity);
            				payrollPerception.setPerceptionTypeCode(perceptionTypeEntity.getCodigo());
            				payrollPerception.setPerceptionTypeValue(perceptionTypeEntity.getValor());
            				
            				String key = UValue.stringValue(entry[2]);
            				this.nominaPercepcionesPercepcion.setClave(key);
            				payrollPerception.setKey(key);
            				
            				String concept = UValue.stringValue(entry[3]);
            				this.nominaPercepcionesPercepcion.setConcepto(concept);
            				payrollPerception.setConcept(concept);
            				
            				BigDecimal taxableAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[4]));
            				this.nominaPercepcionesPercepcion.setImporteGravado(taxableAmount);
            				payrollPerception.setTaxableAmount(UValue.bigDecimalString(UValue.stringValue(entry[4])));
            				
            				BigDecimal exemptAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[5]));
            				this.nominaPercepcionesPercepcion.setImporteExento(exemptAmount);
            				payrollPerception.setExemptAmount(UValue.bigDecimalString(UValue.stringValue(entry[5])));
            				
            				if (!UValidator.isNullOrEmpty(UValue.stringValue(entry[6])) && !UValidator.isNullOrEmpty(UValue.stringValue(entry[7]))) {
                				this.nominaPercepcionesPercepcionAccionesOTitulos = new Nomina.Percepciones.Percepcion.AccionesOTitulos();
                				PayrollActionTitle payrollActionTitle = new PayrollActionTitle();
                				
                				BigDecimal marketValue = UValue.bigDecimalStrict(UValue.stringValue(entry[6]));
                				this.nominaPercepcionesPercepcionAccionesOTitulos.setValorMercado(marketValue);
                				payrollActionTitle.setMarketValue(UValue.bigDecimalString(UValue.stringValue(entry[6])));
                				
                				BigDecimal priceGranted = UValue.bigDecimal(UValue.stringValue(entry[7]));
                				this.nominaPercepcionesPercepcionAccionesOTitulos.setPrecioAlOtorgarse(priceGranted);
                				payrollActionTitle.setPriceGranted(UValue.bigDecimalString(UValue.stringValue(entry[7])));
                				
                				this.nominaPercepcionesPercepcion.setAccionesOTitulos(this.nominaPercepcionesPercepcionAccionesOTitulos);
                				payrollPerception.setPayrollActionTitle(payrollActionTitle);
            				}
            			}
            			
            			//##### @Entry(code = "THE", name = "Nomina :: Percepciones :: Percepcion :: Horas extras")
            			//##### @Description("Trama referida a la informacion de las horas extras.")
            			//##### @Type(type = "list")
            			if (entry[0].trim().equalsIgnoreCase("THE")) {
            				this.nominaPercepcionesPercepcionHorasExtra = new Nomina.Percepciones.Percepcion.HorasExtra();
            				PayrollExtraHour payrollExtraHour = new PayrollExtraHour();
            				
            				Integer days = UValue.integerValue(UValue.stringValue(entry[1]));
            				this.nominaPercepcionesPercepcionHorasExtra.setDias(days);
            				payrollExtraHour.setDays(UValue.integerString(days));
            				
            				String hourType = UValue.stringValue(entry[2]);
            				this.nominaPercepcionesPercepcionHorasExtra.setTipoHoras(hourType);
            				
            				TipoHoraEntity hourTypeEntity = iHourTypeJpaRepository.findByCodigoAndEliminadoFalse(hourType); 
            				payrollExtraHour.setHourType(hourTypeEntity);
            				payrollExtraHour.setHourTypeCode(hourTypeEntity.getCodigo());
            				payrollExtraHour.setHourTypeValue(hourTypeEntity.getValor());
            				
            				Integer extraHour = UValue.integerValue(UValue.stringValue(entry[3])); 
            				this.nominaPercepcionesPercepcionHorasExtra.setHorasExtra(extraHour);
            				payrollExtraHour.setExtraHour(UValue.integerString(extraHour));
            				
            				BigDecimal amountPaid = UValue.bigDecimalStrict(UValue.stringValue(entry[4]));
            				this.nominaPercepcionesPercepcionHorasExtra.setImportePagado(amountPaid);
            				payrollExtraHour.setAmountPaid(UValue.bigDecimalString(UValue.stringValue(entry[4])));
            				
            				if (!UValidator.isNullOrEmpty(this.nominaPercepcionesPercepcion)) {
            					this.nominaPercepcionesPercepcion.getHorasExtras().add(this.nominaPercepcionesPercepcionHorasExtra);
            				}
            				
            				if (!UValidator.isNullOrEmpty(payrollPerception)) {
            					payrollPerception.getPayrollExtraHours().add(payrollExtraHour);
            				}
            			}
            			
            			//##### @Entry(code = "TJPR", name = "Nomina :: Percepciones :: Jubilacion pension retiro")
            			//##### @Description("Trama referida a la informacion de jubilacion pension retiro.")
            			if (entry[0].trim().equalsIgnoreCase("TJPR")) {
            				//##### Validando si existe una percepcion sin agregar
            				if (!UValidator.isNullOrEmpty(this.nominaPercepcionesPercepcion)) {
            					if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
                					this.nominaPercepciones.getPercepcions().add(this.nominaPercepcionesPercepcion);
                					if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
                						if (!UValidator.isNullOrEmpty(payrollPerception)) {
                							apiPayrollPerception.getPayrollPerceptions().add(payrollPerception);
                						}
                    				}
                					
                					this.nominaPercepcionesPercepcion = null;
                    				payrollPerception = null;
                				}
            				}
            				
            				this.nominaPercepcionesJubilacionPensionRetiro = new Nomina.Percepciones.JubilacionPensionRetiro();
            				PayrollPensionRetirement payrollPensionRetirement = new PayrollPensionRetirement();
            				
            				BigDecimal totalExhibition = UValue.bigDecimalStrict(UValue.stringValue(entry[1]));
            				if (!UValidator.isNullOrEmpty(totalExhibition)) {
            					this.nominaPercepcionesJubilacionPensionRetiro.setTotalUnaExhibicion(totalExhibition);
            					payrollPensionRetirement.setTotalExhibition(UValue.bigDecimalString(UValue.stringValue(entry[1])));
            				}
            				
            				BigDecimal totalPartiality = UValue.bigDecimalStrict(UValue.stringValue(entry[2]));
            				if (!UValidator.isNullOrEmpty(totalPartiality)) {
            					this.nominaPercepcionesJubilacionPensionRetiro.setTotalParcialidad(totalPartiality);
            					payrollPensionRetirement.setTotalPartiality(UValue.bigDecimalString(UValue.stringValue(entry[2])));
            				}
            				
            				BigDecimal dailyAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[3]));
            				if (!UValidator.isNullOrEmpty(dailyAmount)) {
            					this.nominaPercepcionesJubilacionPensionRetiro.setMontoDiario(dailyAmount);
            					payrollPensionRetirement.setDailyAmount(UValue.bigDecimalString(UValue.stringValue(entry[3])));
            				}
            				
            				BigDecimal incomeAccumulate = UValue.bigDecimalStrict(UValue.stringValue(entry[4]));
            				if (!UValidator.isNullOrEmpty(incomeAccumulate)) {
            					this.nominaPercepcionesJubilacionPensionRetiro.setIngresoAcumulable(incomeAccumulate);
            					payrollPensionRetirement.setIncomeAccumulate(UValue.bigDecimalString(UValue.stringValue(entry[4])));
            				}
            				
            				BigDecimal incomeNoAccumulate = UValue.bigDecimalStrict(UValue.stringValue(entry[5]));
            				if (!UValidator.isNullOrEmpty(incomeNoAccumulate)) {
            					this.nominaPercepcionesJubilacionPensionRetiro.setIngresoNoAcumulable(incomeNoAccumulate);
            					payrollPensionRetirement.setIncomeNoAccumulate(UValue.bigDecimalString(UValue.stringValue(entry[5])));
            				}
            				
            				if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
            					this.nominaPercepciones.setJubilacionPensionRetiro(this.nominaPercepcionesJubilacionPensionRetiro);
            				}
            				
            				if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
            					apiPayrollPerception.setPayrollPensionRetirement(payrollPensionRetirement);
            				}
            			}
            			
            			//##### @Entry(code = "TSI", name = "Nomina :: Percepciones :: Separacion indemnizacion")
            			//##### @Description("Trama referida a la informacion de separacion indemnizacion.")
            			if (entry[0].trim().equalsIgnoreCase("TSI")) {
            				//##### Validando si existe una percepcion sin agregar
            				if (!UValidator.isNullOrEmpty(this.nominaPercepcionesPercepcion)) {
            					if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
                					this.nominaPercepciones.getPercepcions().add(this.nominaPercepcionesPercepcion);
                					if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
                						if (!UValidator.isNullOrEmpty(payrollPerception)) {
                							apiPayrollPerception.getPayrollPerceptions().add(payrollPerception);
                						}
                    				}
                					
                					this.nominaPercepcionesPercepcion = null;
                    				payrollPerception = null;
                				}
            				}
            				
            				this.nominaPercepcionesSeparacionIndemnizacion = new Nomina.Percepciones.SeparacionIndemnizacion();
            				PayrollSeparationCompensation payrollSeparationCompensation = new PayrollSeparationCompensation();
            				
            				BigDecimal totalPaid = UValue.bigDecimalStrict(UValue.stringValue(entry[1]));
            				this.nominaPercepcionesSeparacionIndemnizacion.setTotalPagado(totalPaid);
            				payrollSeparationCompensation.setTotalPaid(UValue.bigDecimalString(UValue.stringValue(entry[1])));
            				
            				Integer exercisesNumberService = UValue.integerValue(UValue.stringValue(entry[2]));
            				this.nominaPercepcionesSeparacionIndemnizacion.setNumAñosServicio(exercisesNumberService);
            				payrollSeparationCompensation.setExercisesNumberService(UValue.integerString(exercisesNumberService));
            				
            				BigDecimal lastSalary = UValue.bigDecimalStrict(UValue.stringValue(entry[3]));
            				this.nominaPercepcionesSeparacionIndemnizacion.setUltimoSueldoMensOrd(lastSalary);
            				payrollSeparationCompensation.setLastSalary(UValue.bigDecimalString(UValue.stringValue(entry[3])));
            				
            				BigDecimal incomeAccumulate = UValue.bigDecimalStrict(UValue.stringValue(entry[4]));
            				this.nominaPercepcionesSeparacionIndemnizacion.setIngresoAcumulable(incomeAccumulate);
            				payrollSeparationCompensation.setIncomeAccumulate(UValue.bigDecimalString(UValue.stringValue(entry[4])));
            				
            				BigDecimal incomeNoAccumulate = UValue.bigDecimalStrict(UValue.stringValue(entry[5]));
            				this.nominaPercepcionesSeparacionIndemnizacion.setIngresoNoAcumulable(incomeNoAccumulate);
            				payrollSeparationCompensation.setIncomeNoAccumulate(UValue.bigDecimalString(UValue.stringValue(entry[5])));
            				
            				if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
            					this.nominaPercepciones.setSeparacionIndemnizacion(this.nominaPercepcionesSeparacionIndemnizacion);
            				}
            				
            				if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
            					apiPayrollPerception.setPayrollSeparationCompensation(payrollSeparationCompensation);
            				}
            			}
            			
            			//##### @Entry(code = "TDEDS", name = "Nomina :: Deducciones")
            			//##### @Description("Trama referida a la informacion de los totales de deducciones.")
            			if (entry[0].trim().equalsIgnoreCase("TDEDS")) {
            				//##### Validando si existe una percepcion sin agregar
            				if (!UValidator.isNullOrEmpty(this.nominaPercepcionesPercepcion)) {
            					if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
                					this.nominaPercepciones.getPercepcions().add(this.nominaPercepcionesPercepcion);
                					if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
                						if (!UValidator.isNullOrEmpty(payrollPerception)) {
                							apiPayrollPerception.getPayrollPerceptions().add(payrollPerception);
                						}
                    				}
                					
                					this.nominaPercepcionesPercepcion = null;
                    				payrollPerception = null;
                				}
            				}
            				
            				this.nominaDeducciones = new Nomina.Deducciones();
            				wPayrollDeduction = new ApiPayrollDeduction();
            				
            				BigDecimal totalOtherDeductions = UValue.bigDecimalStrict(UValue.stringValue(entry[1]));
            				this.nominaDeducciones.setTotalOtrasDeducciones(totalOtherDeductions);
            				wPayrollDeduction.setTotalOtherDeductions(UValue.bigDecimalString(UValue.stringValue(entry[1])));
            				
            				BigDecimal totalTaxesWithheld = UValue.bigDecimalStrict(UValue.stringValue(entry[2]));
            				if (!UValidator.isNullOrEmpty(totalTaxesWithheld)) {
            					this.nominaDeducciones.setTotalImpuestosRetenidos(totalTaxesWithheld);
            					wPayrollDeduction.setTotalTaxesWithheld(UValue.bigDecimalString(UValue.stringValue(entry[2])));
            				}
            			}
            			
            			//##### @Entry(code = "TDEDU", name = "Deduccion")
            			//##### @Description("Trama referida a la informacion de una deduccion.")
            			//##### @Type(type = "list")
            			if (entry[0].trim().equalsIgnoreCase("TDEDU")) {
            				this.nominaDeduccionesDeduccion = new Nomina.Deducciones.Deduccion();
            				payrollDeduction = new PayrollDeduction();
            				
            				String deductionType = UValue.stringValue(entry[1]);
            				this.nominaDeduccionesDeduccion.setTipoDeduccion(deductionType);
            				
            				TipoDeduccionEntity deductionTypeEntity = iDeductionTypeJpaRepository.findByCodigoAndEliminadoFalse(deductionType);
            				payrollDeduction.setDeductionType(deductionTypeEntity);
            				payrollDeduction.setDeductionTypeCode(deductionTypeEntity.getCodigo());
            				payrollDeduction.setDeductionTypeValue(deductionTypeEntity.getValor());
            				
            				String key = UValue.stringValue(entry[2]);
            				this.nominaDeduccionesDeduccion.setClave(key);
            				payrollDeduction.setKey(key);
            				
            				String concept = UValue.stringValue(entry[3]);
            				this.nominaDeduccionesDeduccion.setConcepto(concept);
            				payrollDeduction.setConcept(concept);
            				
            				BigDecimal amount = UValue.bigDecimalStrict(UValue.stringValue(entry[4]));
            				this.nominaDeduccionesDeduccion.setImporte(amount);
            				payrollDeduction.setAmount(UValue.bigDecimalString(UValue.stringValue(entry[4])));
            				
            				if (!UValidator.isNullOrEmpty(this.nominaDeducciones)) {
            					this.nominaDeducciones.getDeduccions().add(this.nominaDeduccionesDeduccion);
            				}
            				
            				if (!UValidator.isNullOrEmpty(wPayrollDeduction)) {
            					wPayrollDeduction.getPayrollDeductions().add(payrollDeduction);
            				}
            			}
            			
            			//##### @Entry(code = "TOP", name = "Nomina :: Otros Pagos")
            			//##### @Description("Trama referida a la informacion de otros pagos.")
            			//##### @Type(type = "list")
            			if (entry[0].trim().equalsIgnoreCase("TOP")) {
            				//##### Validando si existe una percepcion sin agregar
            				if (!UValidator.isNullOrEmpty(this.nominaPercepcionesPercepcion)) {
            					if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
                					this.nominaPercepciones.getPercepcions().add(this.nominaPercepcionesPercepcion);
                					if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
                						if (!UValidator.isNullOrEmpty(payrollPerception)) {
                							apiPayrollPerception.getPayrollPerceptions().add(payrollPerception);
                						}
                    				}
                					
                					this.nominaPercepcionesPercepcion = null;
                    				payrollPerception = null;
                				}
            				}
            				
            				this.nominaOtrosPagosOtroPago = new Nomina.OtrosPagos.OtroPago();
            				ApiPayrollOtherPayment payrollOtherPayment = new ApiPayrollOtherPayment(); 
            				
            				String otherPaymentType = UValue.stringValue(entry[1]);
            				this.nominaOtrosPagosOtroPago.setTipoOtroPago(otherPaymentType);
            				
            				TipoOtroPagoEntity otherPaymentTypeEntity = iOtherPaymentTypeJpaRepository.findByCodigoAndEliminadoFalse(otherPaymentType);
            				payrollOtherPayment.setOtherPaymentType(otherPaymentTypeEntity);
            				payrollOtherPayment.setOtherPaymentTypeCode(otherPaymentTypeEntity.getCodigo());
            				payrollOtherPayment.setOtherPaymentTypeValue(otherPaymentTypeEntity.getValor());
            				
            				String key = UValue.stringValue(entry[2]);
            				this.nominaOtrosPagosOtroPago.setClave(key);
            				payrollOtherPayment.setKey(key);
            				
            				String concept = UValue.stringValue(entry[3]);
            				this.nominaOtrosPagosOtroPago.setConcepto(concept);
            				payrollOtherPayment.setConcept(concept);
            				
            				BigDecimal amount = UValue.bigDecimalStrict(UValue.stringValue(entry[4]));
            				this.nominaOtrosPagosOtroPago.setImporte(amount);
            				payrollOtherPayment.setAmount(UValue.bigDecimalString(UValue.stringValue(entry[4])));
            				
            				BigDecimal subsidyCaused = UValue.bigDecimalStrict(UValue.stringValue(entry[5]));
            				if (!UValidator.isNullOrEmpty(subsidyCaused)) {
            					this.nominaOtrosPagosOtroPagoSubsidioAlEmpleo = new Nomina.OtrosPagos.OtroPago.SubsidioAlEmpleo();
            					this.nominaOtrosPagosOtroPagoSubsidioAlEmpleo.setSubsidioCausado(subsidyCaused);
            					
            					EmploymentSubsidy employmentSubsidy = new EmploymentSubsidy();
            					employmentSubsidy.setSubsidyCaused(UValue.bigDecimalString(UValue.stringValue(entry[5])));
                				payrollOtherPayment.setEmploymentSubsidy(employmentSubsidy);
                				
                				this.nominaOtrosPagosOtroPago.setSubsidioAlEmpleo(this.nominaOtrosPagosOtroPagoSubsidioAlEmpleo);
            				}
            				
            				if (!UValidator.isNullOrEmpty(UValue.stringValue(entry[6])) || !UValidator.isNullOrEmpty(UValue.stringValue(entry[7])) || !UValidator.isNullOrEmpty(UValue.stringValue(entry[8]))) {
            					this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor = new Nomina.OtrosPagos.OtroPago.CompensacionSaldosAFavor();
                				CompensationCreditBalances compensationCreditBalance = new CompensationCreditBalances();
                				
                				BigDecimal positiveBalance = UValue.bigDecimalStrict(UValue.stringValue(entry[6]));
                				if (!UValidator.isNullOrEmpty(positiveBalance)) {
                					this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor.setSaldoAFavor(positiveBalance);
                					compensationCreditBalance.setPositiveBalance(UValue.bigDecimalString(UValue.stringValue(entry[6])));
                				}
                				
                				String exercise = UValue.stringValue(entry[7]);
                				if (!UValidator.isNullOrEmpty(exercise)) {
                					this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor.setAño(UValue.shortValue(exercise));
                					compensationCreditBalance.setExercise(exercise);
                				}
                				
                				BigDecimal remainingPositiveBalance = UValue.bigDecimalStrict(UValue.stringValue(entry[8]));
                				if (!UValidator.isNullOrEmpty(remainingPositiveBalance)) {
                					this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor.setRemanenteSalFav(remainingPositiveBalance);
                					compensationCreditBalance.setRemainingPositiveBalance(UValue.bigDecimalString(UValue.stringValue(entry[8])));
                					payrollOtherPayment.setCompensationCreditBalance(compensationCreditBalance);
                				}
                				
                				this.nominaOtrosPagosOtroPago.setCompensacionSaldosAFavor(this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor);
            				}
            				
            				if (UValidator.isNullOrEmpty(this.nominaOtrosPagos)) {
            					this.nominaOtrosPagos = new Nomina.OtrosPagos();
            					apiPayrollOtherPayments = new ArrayList<>();
            				}
            				this.nominaOtrosPagos.getOtroPagos().add(this.nominaOtrosPagosOtroPago);
            				
            				apiPayrollOtherPayments.add(payrollOtherPayment);
            			}
            			
            			//##### @Entry(code = "TI", name = "Nomina :: Incapacidades")
            			//##### @Description("Trama referida a la informacion de las incapacidades.")
            			//##### @Type(type = "list")
            			if (entry[0].trim().equalsIgnoreCase("TI")) {
            				//##### Validando si existe una percepcion sin agregar
            				if (!UValidator.isNullOrEmpty(this.nominaPercepcionesPercepcion)) {
            					if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
                					this.nominaPercepciones.getPercepcions().add(this.nominaPercepcionesPercepcion);
                					if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
                						if (!UValidator.isNullOrEmpty(payrollPerception)) {
                							apiPayrollPerception.getPayrollPerceptions().add(payrollPerception);
                						}
                    				}
                					
                					this.nominaPercepcionesPercepcion = null;
                    				payrollPerception = null;
                				}
            				}
            				
            				this.nominaIncapacidadesIncapacidad = new Nomina.Incapacidades.Incapacidad();
            				ApiPayrollInability payrollInability = new ApiPayrollInability();
            				
            				Integer inabilityDays = UValue.integerValue(UValue.stringValue(entry[1]));
            				this.nominaIncapacidadesIncapacidad.setDiasIncapacidad(inabilityDays);
            				payrollInability.setInabilityDays(UValue.integerString(inabilityDays));
            				
            				String inabilityType = UValue.stringValue(entry[2]);
            				this.nominaIncapacidadesIncapacidad.setTipoIncapacidad(inabilityType);
            				TipoIncapacidadEntity inabilityTypeEntity = iInabilityTypeJpaRepository.findByCodigoAndEliminadoFalse(inabilityType);
            				if (!UValidator.isNullOrEmpty(inabilityTypeEntity)) {
            					payrollInability.setInabilityType(inabilityTypeEntity);
                				payrollInability.setInabilityTypeCode(inabilityTypeEntity.getCodigo());
                				payrollInability.setInabilityTypeValue(inabilityTypeEntity.getValor());
            				} else {            					
            					validationErrorField = "TipoIncapacidad";
            					validationErrorCode = "NOM224";
            					validationErrorMessage = "El valor del atributo TipoIncapacidad no cumple con un valor del catálogo c_TIpoIncapacidad";
            					validationError = true;
            				}
            				
            				BigDecimal monetaryAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[3]));
            				this.nominaIncapacidadesIncapacidad.setImporteMonetario(monetaryAmount);
            				payrollInability.setMonetaryAmount(UValue.bigDecimalString(UValue.stringValue(entry[3])));
            				
            				if (UValidator.isNullOrEmpty(this.nominaIncapacidades)) {
            					this.nominaIncapacidades = new Nomina.Incapacidades();
            					apiPayrollInabilities = new ArrayList<>();
            				}
            				this.nominaIncapacidades.getIncapacidads().add(this.nominaIncapacidadesIncapacidad);
            				apiPayrollInabilities.add(payrollInability);
            			}
            			
            			//##### Incrementando la linea
            			line ++;
            			
            			//##### Verificamdo si existe una trama siguiente
            			nextEntrieIsPrincipal = isNextEntryPrincipal(entries, i);
    				} else {
    					//##### Archivo CSV
    					apiCfdi = new ApiCfdi();
    	    			apiVoucher = new ApiVoucher();
    	    			
    	    			apiConcepts = new ArrayList<>();
    	    			apiRelatedCfdi = new ApiRelatedCfdi();
    	    			
    	    			apiPayroll = new ApiPayroll();
    	    			
    	    			apiPayrollOtherPayments = null;
    	    			apiPayrollInabilities = null;
    	    			
    	    			this.nominaReceptor = null;
    	    			apiReceiver = null;
    	    			payrollReceiver = null;
    	    			
    	    			receiverEmail = null;
    	    			legend = null;
    	    			
    	    			uuids = new ArrayList<>();
    	    			
    	    			this.nominaPercepciones = null;
    	    			this.nominaPercepcionesPercepcion = null;
    	    			apiPayrollPerception = null;
    	    			payrollPerception = null;
    	    			
    	    			this.nominaDeducciones = null;
    	    			this.nominaDeduccionesDeduccion = null;
    	    			wPayrollDeduction = null;
    	    			payrollDeduction = null;
    	    			
    	    			this.nominaOtrosPagos = null;
    	    			this.nominaIncapacidades = null;
    	    			
    	    			//##### Informacion del comprobante
    	    			this.comprobante = new Comprobante();
    	    			
    	    			this.comprobante.setVersion(VOUCHER_VERSION);
    	    			apiVoucher.setVersion(VOUCHER_VERSION);
    	    			
    	    			this.comprobante.setTipoDeComprobante(UCatalog.tipoDeComprobante(VOUCHER_TYPE_CODE));
    	    			apiCfdi.setVoucherType(iVoucherTypeJpaRepository.findByCodigoAndEliminadoFalse(VOUCHER_TYPE_CODE));
    	    			
    	    			this.comprobante.setMoneda(UCatalog.moneda(VOUCHER_CURRENCY_CODE));
    	    			apiCfdi.setCurrency(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(VOUCHER_CURRENCY_CODE));
    	    			
    	    			this.comprobante.setMetodoPago(UCatalog.metodoPago(VOUCHER_PAYMENT_METHOD_CODE));
    	    			apiCfdi.setPaymentMethod(iPaymentMethodJpaRepositoy.findByCodigoAndEliminadoFalse(VOUCHER_PAYMENT_METHOD_CODE));
    	    			
    	    			String emitterSeal = UCertificateX509.certificateSeal(payrollParser.getPayrollStamp().getApiEmitter().getCertificado(), payrollParser.getPayrollStamp().getApiEmitter().getLlavePrivada(), payrollParser.getPayrollStamp().getApiEmitter().getPasswd());
    	    			this.comprobante.setSello(emitterSeal);
    	    			apiVoucher.setEmitterSeal(emitterSeal);
    	    			
    	    			this.comprobante.setCertificado(UCertificateX509.certificateBase64(payrollParser.getPayrollStamp().getApiEmitter().getCertificado()));
    	    			
    	    			String certificateEmitterNumber = UCertificateX509.certificateSerialNumber(payrollParser.getPayrollStamp().getApiEmitter().getCertificado());
    	    			this.comprobante.setNoCertificado(certificateEmitterNumber);
    	    			apiVoucher.setCertificateEmitterNumber(certificateEmitterNumber);
    	    			
    	    			this.comprobante.setFecha(UDate.formattedDate());
    	    			apiVoucher.setDateTimeExpedition(UDate.formattedDate());
    	    	        
    	    			//##### Informacion asociada al comprobante
    	    			apiCfdi.setEmitter(payrollParser.getPayrollStamp().getApiEmitter());
    	    			
    	    			perceptionMap = new HashMap<String, Nomina.Percepciones.Percepcion>();
    	    			payrollPerceptionMap = new HashMap<String, PayrollPerception>();
    	    			payrollPerceptions = new ArrayList<>();
    	    			perceptionExtraHours = new ArrayList<>();
    	    			
    					for (int j = 0; j < payrollParser.getStatistics().getPayrollFileHeader().getPayrollHeaders().size(); j++) {
							
    						PayrollHeader header = payrollParser.getStatistics().getPayrollFileHeader().getPayrollHeaders().get(j);
    						String entryName = header.getEntry(); 
    						Integer startPosition = header.getStartPosition();
    						Integer endPosition = header.getEndPosition();
    						
    						//##### Validar que la trama no este vacia
    						Integer count = 0;
    						Integer posInEntry = startPosition;
    						Integer entryLength = endPosition - startPosition + 1;
    						Boolean entryIsNotEmpty = true;   						
    						while (count < entryLength) {
    							String value = entry[posInEntry];
    							if (!UValidator.isNullOrEmpty(value)) {
    								entryIsNotEmpty = false;
    								break;
    							}
    							
    							count ++;
    							posInEntry ++;
							}
    						
    						if (!entryIsNotEmpty) {
    							//##### @Entry(code = "H01", name = "Informacion complementaria")
    							//##### @Description("Trama referida al correo del trabajar y la leyenda del comprobante.")
    							if (entryName.equalsIgnoreCase("H01")) {
    								apiReceiver = new ApiReceiver();
    								
    								receiverEmail = UValue.stringValue(entry[startPosition]);
    								apiReceiver.setCorreoElectronico(receiverEmail);
    								
    								legend = UValue.stringValue(entry[endPosition]);
    								apiCfdi.setLegend(legend);
    							}
    							
    							//##### @Entry(code = "TRE", name = "Comprobante :: Receptor")
    							//##### @Description("Trama referida a la informacion del receptor del comprobante.")
    							if (entryName.equalsIgnoreCase("TRE")) {
    								this.comprobanteReceptor = new Comprobante.Receptor();
    								
    								String rfc = UValue.stringValue(entry[startPosition]);
    								this.comprobanteReceptor.setRfc(rfc);
    								apiReceiver.setRfc(UValue.stringValueUppercase(rfc));
    								
    								String name = UValue.stringValue(entry[endPosition]);
    								this.comprobanteReceptor.setNombre(name);
    								apiReceiver.setNombreRazonSocial(name);
    								
    								this.comprobanteReceptor.setUsoCFDI(UCatalog.usoCFDI(RECEIVER_CFDI_USE_CODE));
    								UsoCfdiEntity cfdiUse = iCfdiUseJpaRepository.findByCodigoAndEliminadoFalse(RECEIVER_CFDI_USE_CODE);
    								apiReceiver.setUsoCFDI(cfdiUse);
    								
    								apiCfdi.setReceiver(apiReceiver);
    							}
    							
    							//##### @Entry(code = "TC", name = "Comprobante :: Concepto")
    							//##### @Description("Trama referida al concepto del comprobante.")
    							if (entryName.equalsIgnoreCase("TC")) {
    								this.comprobanteConcepto = new Comprobante.Conceptos.Concepto();
    								ApiConcept concept = new ApiConcept();
    								
    								this.comprobanteConcepto.setDescripcion(CONCEPT_DESCRIPTION);
    								concept.setDescription(CONCEPT_DESCRIPTION);
    								
    								this.comprobanteConcepto.setClaveProdServ(CONCEPT_PRODUCT_SERVICE_CODE);
    								ClaveProductoServicioEntity keyProductService = iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(CONCEPT_PRODUCT_SERVICE_CODE);
    								concept.setKeyProductServiceEntity(keyProductService);
    								concept.setKeyProductServiceCode(keyProductService.getCodigo());
    								concept.setKeyProductService(keyProductService.getValor());
    								
    								this.comprobanteConcepto.setClaveUnidad(CONCEPT_UNIT_CODE);
    								UnidadMedidaEntity unit = iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(CONCEPT_UNIT_CODE);
    								concept.setMeasurementUnitEntity(unit);
    								concept.setMeasurementUnitCode(unit.getCodigo());
    								concept.setMeasurementUnit(unit.getValor());
    								
    								BigDecimal quantity = UValue.bigDecimalStrict(CONCEPT_QUANTITY);
    								this.comprobanteConcepto.setCantidad(quantity);
    								concept.setQuantity(quantity);
    								
    								BigDecimal unitValue = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition]));
    								this.comprobanteConcepto.setValorUnitario(unitValue);
    								concept.setUnitValue(unitValue);
    								concept.setUnitValueString(UValue.bigDecimalString(UValue.stringValue(entry[startPosition])));
    								
    								BigDecimal amount = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 1]));
    								this.comprobanteConcepto.setImporte(amount);
    								concept.setAmount(amount);
    								concept.setAmountString(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 1])));
    								
    								BigDecimal discount = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								this.comprobanteConcepto.setDescuento(discount);
    								concept.setDiscount(discount);
    								concept.setDiscountString(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    								
    								apiConcepts.add(concept);
    								apiCfdi.getConcepts().addAll(apiConcepts);
    							}
    							
    							//##### @Entry(code = "TNOM", name = "Nomina")
    							//##### @Description("Trama referida a la informacion de la nomina.")
    							if (entryName.equalsIgnoreCase("TNOM")) {
    								this.nomina = new Nomina();
    								this.nomina.setVersion(PAYROLL_VERSION);
    								
    								String payrollType = UValue.stringValue(entry[startPosition]); 
    								this.nomina.setTipoNomina(UCatalog.payrollType(payrollType));
    								
    								TipoNominaEntity payrollTypeEntity = iPayrollTypeJpaRepository.findByCodigoAndEliminadoFalse(payrollType);
    								apiPayroll.setPayrollType(payrollTypeEntity);
    								apiPayroll.setPayrollTypeCode(payrollTypeEntity.getCodigo());
    								apiPayroll.setPayrollTypeValue(payrollTypeEntity.getValor());
    								
    								String paymentDate = UValue.stringValue(entry[startPosition + 1]);
    								this.nomina.setFechaPago(paymentDate);
    								apiPayroll.setPaymentDate(LocalDateUtils.parse(paymentDate));
    								apiPayroll.setPaymentDateString(DateUtils.customFormat(paymentDate));
    													
    								String paymentIntialDate = UValue.stringValue(entry[startPosition + 2]);
    								this.nomina.setFechaInicialPago(paymentIntialDate);
    								apiPayroll.setPaymentIntialDate(LocalDateUtils.parse(paymentIntialDate));
    								apiPayroll.setPaymentIntialDateString(UDate.formattedDate(UDate.date(paymentIntialDate), appSettings.getPropertyValue("cfdi.date.single.format")));
    								
    								String paymentEndDate = UValue.stringValue(entry[startPosition + 3]);
    								this.nomina.setFechaFinalPago(paymentEndDate);
    								apiPayroll.setPaymentEndDate(LocalDateUtils.parse(paymentEndDate));
    								apiPayroll.setPaymentEndDateString(DateUtils.customFormat(paymentEndDate));
    								
    								this.nomina.setNumDiasPagados(UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 4])));
    								apiPayroll.setNumberDaysPaid(UValue.stringValue(entry[startPosition + 4]));
    								
    								BigDecimal totalPerceptions = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 5]));
    								this.nomina.setTotalPercepciones(totalPerceptions);
    								apiPayroll.setTotalPerceptions(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 5])));
    								
    								BigDecimal totalDeductions = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 6]));
    								this.nomina.setTotalDeducciones(totalDeductions);
    								apiPayroll.setTotalDeductions(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 6])));
    								
    								BigDecimal totalOtherPayments = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								this.nomina.setTotalOtrosPagos(totalOtherPayments);
    								apiPayroll.setTotalOtherPayments(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    							}
    							
    							//##### @Entry(code = "TNOME", name = "Nomina :: Emisor")
    							//##### @Description("Trama referida a la informacion del emisor de la nomina.")
    							if (entryName.equalsIgnoreCase("TNOME")) {
    								this.nominaEmisor = new Nomina.Emisor();
    								ApiPayrollEmitter payrollEmitter = new ApiPayrollEmitter();
    								
    								String curp = UValue.stringValue(entry[startPosition]);
    								this.nominaEmisor.setCurp(curp);
    								payrollEmitter.setCurp(curp);
    								
    								String employerRegistration = UValue.stringValue(entry[startPosition + 1]);
    								this.nominaEmisor.setRegistroPatronal(employerRegistration);
    								payrollEmitter.setEmployerRegistration(employerRegistration);
    								
    								String rfcPatronOrigin = UValue.stringValue(entry[startPosition + 2]);
    								this.nominaEmisor.setRfcPatronOrigen(rfcPatronOrigin);
    								payrollEmitter.setRfcPatronOrigin(rfcPatronOrigin);
    								
    								if (!UValidator.isNullOrEmpty(UValue.stringValue(entry[startPosition + 3]))) {
    									this.nominaEmisorEntidadSNCF = new Nomina.Emisor.EntidadSNCF();
    									
    									String resourceOrigin = UValue.stringValue(entry[startPosition + 3]);
    									this.nominaEmisorEntidadSNCF.setOrigenRecurso(UCatalog.sourceResource(resourceOrigin));
    									
    									OrigenRecursoEntity resourceOriginEntity = iResourceOriginJpaRepository.findByCodigoAndEliminadoFalse(resourceOrigin);
    									payrollEmitter.setResourceOrigin(resourceOriginEntity);
    									payrollEmitter.setResourceOriginCode(resourceOriginEntity.getCodigo());
    									payrollEmitter.setResourceOriginValue(resourceOriginEntity.getValor());
    									
    									BigDecimal ownResourceAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    									this.nominaEmisorEntidadSNCF.setMontoRecursoPropio(ownResourceAmount);
    									payrollEmitter.setOwnResourceAmount(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    									
    									this.nominaEmisor.setEntidadSNCF(nominaEmisorEntidadSNCF);
    								}
    								
    								apiPayroll.setPayrollEmitter(payrollEmitter);
    							}
    							
    							//##### @Entry(code = "TNOMR", name = "Nomina :: Receptor")
    							//##### @Description("Trama referida a la informacion del receptor de la nomina.")
    							if (entryName.equalsIgnoreCase("TNOMR")) {
    								this.nominaReceptor = new Nomina.Receptor();
    								payrollReceiver = new ApiPayrollReceiver();
    								
    								String curp = UValue.stringValue(entry[startPosition]);
    								this.nominaReceptor.setCurp(curp);
    								payrollReceiver.setCurp(curp);
    								
    								String socialSecurityNumber = UValue.stringValue(entry[startPosition + 1]);
    								this.nominaReceptor.setNumSeguridadSocial(socialSecurityNumber);
    								payrollReceiver.setSocialSecurityNumber(socialSecurityNumber);
    								
    								String startDateEmploymentRelationship = UValue.stringValue(entry[startPosition + 2]);
    								if (!UValidator.isNullOrEmpty(startDateEmploymentRelationship)) {
    									this.nominaReceptor.setFechaInicioRelLaboral(startDateEmploymentRelationship);
    									payrollReceiver.setStartDateEmploymentRelationship(LocalDateUtils.parse(startDateEmploymentRelationship));
    									payrollReceiver.setStartDateEmploymentRelationshipString(DateUtils.customFormat(startDateEmploymentRelationship));
    								}
    								
    								String age = UValue.stringValue(entry[startPosition + 3]);
    								this.nominaReceptor.setAntigüedad(age);
    								payrollReceiver.setAge(UTools.jobAge(age));
    								
    								String contractType = UValue.stringValue(entry[startPosition + 4]);
    								this.nominaReceptor.setTipoContrato(contractType);
    								
    								TipoContratoEntity contractTypeEntity = iContractJpaRepository.findByCodigoAndEliminadoFalse(contractType);
    								payrollReceiver.setContractType(contractTypeEntity);
    								payrollReceiver.setContractTypeCode(contractTypeEntity.getCodigo());
    								payrollReceiver.setContractTypeValue(contractTypeEntity.getValor());
    								
    								String unionized = UValue.stringValue(entry[startPosition + 5]);
    								this.nominaReceptor.setSindicalizado(unionized);
    								payrollReceiver.setUnionized(unionized);
    								
    								String dayType = UValue.stringValue(entry[startPosition + 6]);
    								this.nominaReceptor.setTipoJornada(dayType);
    								
    								TipoJornadaEntity dayTypeEntity = iDayTypeJpaRepository.findByCodigoAndEliminadoFalse(dayType);
    								payrollReceiver.setDayType(dayTypeEntity);
    								payrollReceiver.setDayTypeCode(dayTypeEntity.getCodigo());
    								payrollReceiver.setDayTypeValue(dayTypeEntity.getValor());
    								
    								String regimeType = UValue.stringValue(entry[startPosition + 7]);
    								this.nominaReceptor.setTipoRegimen(regimeType);
    								
    								TipoRegimenEntity regimeTypeEntity = iRegimeTypeJpaRepository.findByCodigoAndEliminadoFalse(regimeType);
    								payrollReceiver.setRegimeType(regimeTypeEntity);
    								payrollReceiver.setRegimeTypeCode(regimeTypeEntity.getCodigo());
    								payrollReceiver.setRegimeTypeValue(regimeTypeEntity.getValor());
    								
    								String employeeNumber = UValue.stringValue(entry[startPosition + 8]);
    								this.nominaReceptor.setNumEmpleado(employeeNumber);
    								payrollReceiver.setEmployeeNumber(employeeNumber);
    								
    								String department = UValue.stringValue(entry[startPosition + 9]);
    								this.nominaReceptor.setDepartamento(department);
    								payrollReceiver.setDepartment(department);
    								
    								String jobTitle = UValue.stringValue(entry[startPosition + 10]); 
    								this.nominaReceptor.setPuesto(jobTitle);
    								payrollReceiver.setJobTitle(jobTitle);
    								
    								String jobRisk = UValue.stringValue(entry[startPosition + 11]);
    								this.nominaReceptor.setRiesgoPuesto(jobRisk);
    								
    								if (!UValidator.isNullOrEmpty(jobRisk)) {
	    								RiesgoPuestoEntity jobRiskEntity = iJobRiskJpaRepository.findByCodigoAndEliminadoFalse(jobRisk);
	    								payrollReceiver.setJobRisk(jobRiskEntity);
	    								payrollReceiver.setJobRiskCode(jobRiskEntity.getCodigo());
	    								payrollReceiver.setJobRiskValue(jobRiskEntity.getValor());
    								}
    								
    								String paymentFrequency = UValue.stringValue(entry[startPosition + 12]);
    								this.nominaReceptor.setPeriodicidadPago(paymentFrequency);
    								
    								PeriodicidadPagoEntity paymentFrequencyEntity = iPaymentFrequencyJpaRepository.findByCodigoAndEliminadoFalse(paymentFrequency);
    								payrollReceiver.setPaymentFrequency(paymentFrequencyEntity);
    								payrollReceiver.setPaymentFrequencyCode(paymentFrequencyEntity.getCodigo());
    								payrollReceiver.setPaymentFrequencyValue(paymentFrequencyEntity.getValor());
    								
    								String bankAccount = UValue.stringValue(entry[startPosition + 14]);
    								if (!UValidator.isNullOrEmpty(bankAccount)) {
    									if (bankAccount.length() < 18) {
    										String bank = UValue.stringValue(entry[startPosition + 13]);
    										this.nominaReceptor.setBanco(bank);
    										
    										BancoEntity bankEntity = iBankJpaRepository.findByCodigoAndEliminadoFalse(bank);
    										payrollReceiver.setBank(bankEntity);
    										payrollReceiver.setBankCode(bankEntity.getCodigo());
    										payrollReceiver.setBankValue(bankEntity.getValor());
    									}
    								}
    								
    								this.nominaReceptor.setCuentaBancaria(bankAccount);
    								payrollReceiver.setBankAccount(bankAccount);
    								
    								BigDecimal shareContributionBaseSalary = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 15]));
    								this.nominaReceptor.setSalarioBaseCotApor(shareContributionBaseSalary);
    								payrollReceiver.setShareContributionBaseSalary(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 15])));
    								
    								BigDecimal integratedDailySalary = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 16]));
    								this.nominaReceptor.setSalarioDiarioIntegrado(integratedDailySalary);
    								payrollReceiver.setIntegratedDailySalary(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 16])));
    								
    								String federalEntityCode = UValue.stringValue(entry[endPosition]);
    								this.nominaReceptor.setClaveEntFed(UCatalog.estate(federalEntityCode));
    								
    								EntidadFederativaEntity federalEntity = iFederalEntityJpaRepository.findByCodigoAndEliminadoFalse(federalEntityCode);
    								payrollReceiver.setFederalEntity(federalEntity);
    								payrollReceiver.setFederalEntityCode(federalEntity.getCodigo());
    								payrollReceiver.setFederalEntityValue(federalEntity.getValor());
    							}
    							
    							//##### @Entry(code = "TNOMRS", name = "Nomina :: Receptor :: Subcontrataciones")
    							//##### @Description("Trama referida a la informacion de las subcontrataciones.")
    							//##### @Type(type = "list")
    							if (entryName.equalsIgnoreCase("TNOMRS")) {
    								this.nominaReceptorSubContratacion = new Nomina.Receptor.SubContratacion();
    								PayrollOutsourcing payrollOutsourcing = new PayrollOutsourcing();
    								
    								String rfcBoss = UValue.stringValue(entry[startPosition]);
    								this.nominaReceptorSubContratacion.setRfcLabora(rfcBoss);
    								payrollOutsourcing.setRfcBoss(rfcBoss);
    								
    								BigDecimal percentageTime = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								this.nominaReceptorSubContratacion.setPorcentajeTiempo(percentageTime);
    								payrollOutsourcing.setPercentageTime(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    								
    								if (!UValidator.isNullOrEmpty(this.nominaReceptor)) {
    									this.nominaReceptor.getSubContratacions().add(nominaReceptorSubContratacion);
    								}
    								
    								if (!UValidator.isNullOrEmpty(payrollReceiver)) {
    									payrollReceiver.getPayrollOutsourcings().add(payrollOutsourcing);
    								}
    							}
    							
    							//##### @Entry(code = "TPERS", name = "Nomina :: Percepciones")
    							//##### @Description("Trama referida a la informacion de los totales de percepciones.")
    							if (entryName.equalsIgnoreCase("TPERS")) {
    								this.nominaPercepciones = new Nomina.Percepciones();
    								apiPayrollPerception = new ApiPayrollPerception();
    								
    								BigDecimal totalSalaries = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition]));
    								this.nominaPercepciones.setTotalSueldos(totalSalaries);
    								apiPayrollPerception.setTotalSalaries(UValue.bigDecimalString(UValue.stringValue(entry[startPosition])));
    								
    								BigDecimal totalSeparationCompensation = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 1]));
    								this.nominaPercepciones.setTotalSeparacionIndemnizacion(totalSeparationCompensation);
    								apiPayrollPerception.setTotalSeparationCompensation(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 1])));
    								
    								BigDecimal totalPensionRetirement = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 2]));
    								this.nominaPercepciones.setTotalJubilacionPensionRetiro(totalPensionRetirement);
    								apiPayrollPerception.setTotalPensionRetirement(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 2])));
    								
    								BigDecimal totalTaxed = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 3]));
    								this.nominaPercepciones.setTotalGravado(totalTaxed);
    								apiPayrollPerception.setTotalTaxed(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 3])));
    								
    								BigDecimal totalExempt = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								this.nominaPercepciones.setTotalExento(totalExempt);
    								apiPayrollPerception.setTotalExempt(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    							}
    							
    							//##### @Entry(code = "TPERC", name = "Nomina :: Percepciones :: Percepcion")
    							//##### @Description("Trama referida a la información de una percepcion.")
    							//##### @Type(type = "list")
    							if (entryName.equalsIgnoreCase("TPERC")) {
    								this.nominaPercepcionesPercepcion = new Nomina.Percepciones.Percepcion();
    								payrollPerception = new PayrollPerception();
    								
    								String perceptionType = UValue.stringValue(entry[startPosition]);
    								this.nominaPercepcionesPercepcion.setTipoPercepcion(perceptionType);
    								
    								TipoPercepcionEntity perceptionTypeEntity = iPerceptionTypeJpaRepository.findByCodigoAndEliminadoFalse(perceptionType);
    								payrollPerception.setPerceptionType(perceptionTypeEntity);
    								payrollPerception.setPerceptionTypeCode(perceptionTypeEntity.getCodigo());
    								payrollPerception.setPerceptionTypeValue(perceptionTypeEntity.getValor());
    								
    								String key = UValue.stringValue(entry[startPosition + 1]);
    								this.nominaPercepcionesPercepcion.setClave(key);
    								payrollPerception.setKey(key);
    								
    								String concept = UValue.stringValue(entry[startPosition + 2]);
    								this.nominaPercepcionesPercepcion.setConcepto(concept);
    								payrollPerception.setConcept(concept);
    								
    								BigDecimal taxableAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 3]));
    								this.nominaPercepcionesPercepcion.setImporteGravado(taxableAmount);
    								payrollPerception.setTaxableAmount(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 3])));
    								
    								BigDecimal exemptAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 4]));
    								this.nominaPercepcionesPercepcion.setImporteExento(exemptAmount);
    								payrollPerception.setExemptAmount(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 4])));
    								
    								if (!UValidator.isNullOrEmpty(UValue.stringValue(entry[startPosition + 5])) && !UValidator.isNullOrEmpty(UValue.stringValue(entry[startPosition + 5]))) {
    									this.nominaPercepcionesPercepcionAccionesOTitulos = new Nomina.Percepciones.Percepcion.AccionesOTitulos();
    									PayrollActionTitle payrollActionTitle = new PayrollActionTitle();
    									
    									BigDecimal marketValue = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 5]));
    									this.nominaPercepcionesPercepcionAccionesOTitulos.setValorMercado(marketValue);
    									payrollActionTitle.setMarketValue(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 5])));
    									
    									BigDecimal priceGranted = UValue.bigDecimal(UValue.stringValue(entry[startPosition + 6]));
    									this.nominaPercepcionesPercepcionAccionesOTitulos.setPrecioAlOtorgarse(priceGranted);
    									payrollActionTitle.setPriceGranted(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 6])));
    									
    									this.nominaPercepcionesPercepcion.setAccionesOTitulos(this.nominaPercepcionesPercepcionAccionesOTitulos);
    									payrollPerception.setPayrollActionTitle(payrollActionTitle);
    								}
    								
    								String perceptionIdentifier = UValue.stringValue(entry[endPosition]);
    								if (!UValidator.isNullOrEmpty(perceptionIdentifier) && perceptionTypeEntity.getCodigo().equalsIgnoreCase("019")) {
    									//##### Es una percepcion con horas extra
    									perceptionMap.put(perceptionIdentifier, this.nominaPercepcionesPercepcion);
    									payrollPerceptionMap.put(perceptionIdentifier, payrollPerception);
    								} else {
    									if ((!UValidator.isNullOrEmpty(taxableAmount) && taxableAmount.compareTo(zeroDecimal) == 1) || 
    											(!UValidator.isNullOrEmpty(exemptAmount) && exemptAmount.compareTo(zeroDecimal) == 1)) {
    										if (!UValidator.isNullOrEmpty(this.nominaPercepcionesPercepcion)) {
        	            						this.nominaPercepciones.getPercepcions().add(this.nominaPercepcionesPercepcion);
        	            					}
        									
        									if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
        	            						if (!UValidator.isNullOrEmpty(payrollPerception)) {
        	            							apiPayrollPerception.getPayrollPerceptions().add(payrollPerception);
        	            						}
        	                				}
    									}
    								}
    							}
    							
    							//##### @Entry(code = "THE", name = "Nomina :: Percepciones :: Percepcion :: Horas extras")
    							//##### @Description("Trama referida a la información de las horas extras.")
    							//##### @Type(type = "list")
    							if (entryName.equalsIgnoreCase("THE")) {
    								this.nominaPercepcionesPercepcionHorasExtra = new Nomina.Percepciones.Percepcion.HorasExtra();
    								PayrollExtraHour payrollExtraHour = new PayrollExtraHour();
    								
    								Integer days = UValue.integerValue(UValue.stringValue(entry[startPosition]));
    								this.nominaPercepcionesPercepcionHorasExtra.setDias(days);
    								payrollExtraHour.setDays(UValue.integerString(days));
    								
    								String hourType = UValue.stringValue(entry[startPosition + 1]);
    								this.nominaPercepcionesPercepcionHorasExtra.setTipoHoras(hourType);
    								
    								TipoHoraEntity hourTypeEntity = iHourTypeJpaRepository.findByCodigoAndEliminadoFalse(hourType); 
    								payrollExtraHour.setHourType(hourTypeEntity);
    								payrollExtraHour.setHourTypeCode(hourTypeEntity.getCodigo());
    								payrollExtraHour.setHourTypeValue(hourTypeEntity.getValor());
    								
    								Integer extraHour = UValue.integerValue(UValue.stringValue(entry[startPosition + 2])); 
    								this.nominaPercepcionesPercepcionHorasExtra.setHorasExtra(extraHour);
    								payrollExtraHour.setExtraHour(UValue.integerString(extraHour));
    								
    								BigDecimal amountPaid = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 3]));
    								this.nominaPercepcionesPercepcionHorasExtra.setImportePagado(amountPaid);
    								payrollExtraHour.setAmountPaid(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 3])));
    								
    								String perceptionIdentifier = UValue.stringValue(entry[endPosition]);
									if (perceptionMap.containsKey(perceptionIdentifier) && payrollPerceptionMap.containsKey(perceptionIdentifier)) {
										Nomina.Percepciones.Percepcion percepcion = perceptionMap.get(perceptionIdentifier);
				    					percepcion.getHorasExtras().add(this.nominaPercepcionesPercepcionHorasExtra);
				    					perceptionExtraHours.add(percepcion);
				    					
				    					PayrollPerception payrollPerceptionObj = payrollPerceptionMap.get(perceptionIdentifier);
				    					if (!UValidator.isNullOrEmpty(payrollPerceptionObj)) {
				    						payrollPerceptionObj.getPayrollExtraHours().add(payrollExtraHour);
        									payrollPerceptions.add(payrollPerceptionObj);
        								}
									}
    							}
    							
    							//##### @Entry(code = "TJPR", name = "Nomina :: Percepciones :: Jubilacion pension retiro")
    							//##### @Description("Trama referida a la información de jubilacion pension retiro.")
    							if (entryName.equalsIgnoreCase("TJPR")) {
    								this.nominaPercepcionesJubilacionPensionRetiro = new Nomina.Percepciones.JubilacionPensionRetiro();
    								PayrollPensionRetirement payrollPensionRetirement = new PayrollPensionRetirement();
    								
    								BigDecimal totalExhibition = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition]));
    								if (!UValidator.isNullOrEmpty(totalExhibition)) {
    									this.nominaPercepcionesJubilacionPensionRetiro.setTotalUnaExhibicion(totalExhibition);
    									payrollPensionRetirement.setTotalExhibition(UValue.bigDecimalString(UValue.stringValue(entry[startPosition])));
    								}
    								
    								BigDecimal totalPartiality = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 1]));
    								if (!UValidator.isNullOrEmpty(totalPartiality)) {
    									this.nominaPercepcionesJubilacionPensionRetiro.setTotalParcialidad(totalPartiality);
    									payrollPensionRetirement.setTotalPartiality(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 1])));
    								}
    								
    								BigDecimal dailyAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 2]));
    								if (!UValidator.isNullOrEmpty(dailyAmount)) {
    									this.nominaPercepcionesJubilacionPensionRetiro.setMontoDiario(dailyAmount);
    									payrollPensionRetirement.setDailyAmount(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 2])));
    								}
    								
    								BigDecimal incomeAccumulate = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 3]));
    								if (!UValidator.isNullOrEmpty(incomeAccumulate)) {
    									this.nominaPercepcionesJubilacionPensionRetiro.setIngresoAcumulable(incomeAccumulate);
    									payrollPensionRetirement.setIncomeAccumulate(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 3])));
    								}
    								
    								BigDecimal incomeNoAccumulate = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								if (!UValidator.isNullOrEmpty(incomeNoAccumulate)) {
    									this.nominaPercepcionesJubilacionPensionRetiro.setIngresoNoAcumulable(incomeNoAccumulate);
    									payrollPensionRetirement.setIncomeNoAccumulate(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    								}
    								
    								if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
    									this.nominaPercepciones.setJubilacionPensionRetiro(this.nominaPercepcionesJubilacionPensionRetiro);
    								}
    								
    								if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
    									apiPayrollPerception.setPayrollPensionRetirement(payrollPensionRetirement);
    								}
    							}
    							
    							//##### @Entry(code = "TSI", name = "Nomina :: Percepciones :: Separacion indemnizacion")
    							//##### @Description("Trama referida a la informacion de separacion indemnizacion.")
    							if (entryName.equalsIgnoreCase("TSI")) {
    								this.nominaPercepcionesSeparacionIndemnizacion = new Nomina.Percepciones.SeparacionIndemnizacion();
    								PayrollSeparationCompensation payrollSeparationCompensation = new PayrollSeparationCompensation();
    								
    								BigDecimal totalPaid = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition]));
    								this.nominaPercepcionesSeparacionIndemnizacion.setTotalPagado(totalPaid);
    								payrollSeparationCompensation.setTotalPaid(UValue.bigDecimalString(UValue.stringValue(entry[startPosition])));
    								
    								Integer exercisesNumberService = UValue.integerValue(UValue.stringValue(entry[startPosition + 1]));
    								this.nominaPercepcionesSeparacionIndemnizacion.setNumAñosServicio(exercisesNumberService);
    								payrollSeparationCompensation.setExercisesNumberService(UValue.integerString(exercisesNumberService));
    								
    								BigDecimal lastSalary = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 2]));
    								this.nominaPercepcionesSeparacionIndemnizacion.setUltimoSueldoMensOrd(lastSalary);
    								payrollSeparationCompensation.setLastSalary(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 2])));
    								
    								BigDecimal incomeAccumulate = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 3]));
    								this.nominaPercepcionesSeparacionIndemnizacion.setIngresoAcumulable(incomeAccumulate);
    								payrollSeparationCompensation.setIncomeAccumulate(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 3])));
    								
    								BigDecimal incomeNoAccumulate = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								this.nominaPercepcionesSeparacionIndemnizacion.setIngresoNoAcumulable(incomeNoAccumulate);
    								payrollSeparationCompensation.setIncomeNoAccumulate(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    								
    								if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
    									this.nominaPercepciones.setSeparacionIndemnizacion(this.nominaPercepcionesSeparacionIndemnizacion);
    								}
    								
    								if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
    									apiPayrollPerception.setPayrollSeparationCompensation(payrollSeparationCompensation);
    								}
    							}
    							
    							//##### @Entry(code = "TDEDS", name = "Nomina :: Deducciones")
    							//##### @Description("Trama referida a la informacion de los totales de deducciones.")
    							if (entryName.equalsIgnoreCase("TDEDS")) {
    								this.nominaDeducciones = new Nomina.Deducciones();
    								wPayrollDeduction = new ApiPayrollDeduction();
    								
    								BigDecimal totalOtherDeductions = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition]));
    								this.nominaDeducciones.setTotalOtrasDeducciones(totalOtherDeductions);
    								wPayrollDeduction.setTotalOtherDeductions(UValue.bigDecimalString(UValue.stringValue(entry[startPosition])));
    								
    								BigDecimal totalTaxesWithheld = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								if (!UValidator.isNullOrEmpty(totalTaxesWithheld)) {
    									this.nominaDeducciones.setTotalImpuestosRetenidos(totalTaxesWithheld);
    									wPayrollDeduction.setTotalTaxesWithheld(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    								}
    							}
    							
    							//##### @Entry(code = "TDEDU", name = "Deduccion")
    							//##### @Description("Trama referida a la informacion de una deduccion.")
    							//##### @Type(type = "list")
    							if (entryName.equalsIgnoreCase("TDEDU")) {
    								this.nominaDeduccionesDeduccion = new Nomina.Deducciones.Deduccion();
    								payrollDeduction = new PayrollDeduction();
    								
    								String deductionType = UValue.stringValue(entry[startPosition]);
    								this.nominaDeduccionesDeduccion.setTipoDeduccion(deductionType);
    								
    								TipoDeduccionEntity deductionTypeEntity = iDeductionTypeJpaRepository.findByCodigoAndEliminadoFalse(deductionType);
    								payrollDeduction.setDeductionType(deductionTypeEntity);
    								payrollDeduction.setDeductionTypeCode(deductionTypeEntity.getCodigo());
    								payrollDeduction.setDeductionTypeValue(deductionTypeEntity.getValor());
    								
    								String key = UValue.stringValue(entry[startPosition + 1]);
    								this.nominaDeduccionesDeduccion.setClave(key);
    								payrollDeduction.setKey(key);
    								
    								String concept = UValue.stringValue(entry[startPosition + 2]);
    								this.nominaDeduccionesDeduccion.setConcepto(concept);
    								payrollDeduction.setConcept(concept);
    								
    								BigDecimal amount = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								this.nominaDeduccionesDeduccion.setImporte(amount);
    								payrollDeduction.setAmount(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    								
    								if (!UValidator.isNullOrEmpty(this.nominaDeducciones)) {
    									this.nominaDeducciones.getDeduccions().add(this.nominaDeduccionesDeduccion);
    								}
    								
    								if (!UValidator.isNullOrEmpty(wPayrollDeduction)) {
    									wPayrollDeduction.getPayrollDeductions().add(payrollDeduction);
    								}
    							}
    							
    							//##### @Entry(code = "TOP", name = "Nomina :: Otros Pagos")
    							//##### @Description("Trama referida a la información de otros pagos.")
    							//##### @Type(type = "list")
    							if (entryName.equalsIgnoreCase("TOP")) {
    								this.nominaOtrosPagosOtroPago = new Nomina.OtrosPagos.OtroPago();
    								ApiPayrollOtherPayment payrollOtherPayment = new ApiPayrollOtherPayment(); 
    								
    								String otherPaymentType = UValue.stringValue(entry[startPosition]);
    								this.nominaOtrosPagosOtroPago.setTipoOtroPago(otherPaymentType);
    								
    								TipoOtroPagoEntity otherPaymentTypeEntity = iOtherPaymentTypeJpaRepository.findByCodigoAndEliminadoFalse(otherPaymentType);
    								payrollOtherPayment.setOtherPaymentType(otherPaymentTypeEntity);
    								payrollOtherPayment.setOtherPaymentTypeCode(otherPaymentTypeEntity.getCodigo());
    								payrollOtherPayment.setOtherPaymentTypeValue(otherPaymentTypeEntity.getValor());
    								
    								String key = UValue.stringValue(entry[startPosition + 1]);
    								this.nominaOtrosPagosOtroPago.setClave(key);
    								payrollOtherPayment.setKey(key);
    								
    								String concept = UValue.stringValue(entry[startPosition + 2]);
    								this.nominaOtrosPagosOtroPago.setConcepto(concept);
    								payrollOtherPayment.setConcept(concept);
    								
    								BigDecimal amount = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 3]));
    								this.nominaOtrosPagosOtroPago.setImporte(amount);
    								payrollOtherPayment.setAmount(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 3])));
    								
    								BigDecimal subsidyCaused = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 4]));
    								if (!UValidator.isNullOrEmpty(subsidyCaused)) {
    									this.nominaOtrosPagosOtroPagoSubsidioAlEmpleo = new Nomina.OtrosPagos.OtroPago.SubsidioAlEmpleo();
    									this.nominaOtrosPagosOtroPagoSubsidioAlEmpleo.setSubsidioCausado(subsidyCaused);
    									
    									EmploymentSubsidy employmentSubsidy = new EmploymentSubsidy();
    									employmentSubsidy.setSubsidyCaused(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 4])));
    									payrollOtherPayment.setEmploymentSubsidy(employmentSubsidy);
    									
    									this.nominaOtrosPagosOtroPago.setSubsidioAlEmpleo(this.nominaOtrosPagosOtroPagoSubsidioAlEmpleo);
    								}
    								
    								if (!UValidator.isNullOrEmpty(UValue.stringValue(entry[startPosition + 5])) || !UValidator.isNullOrEmpty(UValue.stringValue(entry[startPosition + 6])) || !UValidator.isNullOrEmpty(UValue.stringValue(entry[endPosition]))) {
    									this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor = new Nomina.OtrosPagos.OtroPago.CompensacionSaldosAFavor();
        								CompensationCreditBalances compensationCreditBalance = new CompensationCreditBalances();
        								
        								BigDecimal positiveBalance = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 5]));
        								if (!UValidator.isNullOrEmpty(positiveBalance)) {
        									this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor.setSaldoAFavor(positiveBalance);
        									compensationCreditBalance.setPositiveBalance(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 5])));
        								}
        								
        								String exercise = UValue.stringValue(entry[startPosition + 6]);
        								if (!UValidator.isNullOrEmpty(exercise)) {
        									this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor.setAño(UValue.shortValue(exercise));
        									compensationCreditBalance.setExercise(exercise);
        								}
        								
        								BigDecimal remainingPositiveBalance = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
        								if (!UValidator.isNullOrEmpty(remainingPositiveBalance)) {
        									this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor.setRemanenteSalFav(remainingPositiveBalance);
        									compensationCreditBalance.setRemainingPositiveBalance(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
        									payrollOtherPayment.setCompensationCreditBalance(compensationCreditBalance);
        								}
        								
        								this.nominaOtrosPagosOtroPago.setCompensacionSaldosAFavor(this.nominaOtrosPagosOtroPagoCompensacionSaldosAFavor);
    								}
    								
    								if (UValidator.isNullOrEmpty(this.nominaOtrosPagos)) {
    									this.nominaOtrosPagos = new Nomina.OtrosPagos();
    									apiPayrollOtherPayments = new ArrayList<>();
    								}
    								this.nominaOtrosPagos.getOtroPagos().add(this.nominaOtrosPagosOtroPago);
    								
    								apiPayrollOtherPayments.add(payrollOtherPayment);
    							}
    							
    							//##### @Entry(code = "TI", name = "Nomina :: Incapacidades")
    							//##### @Description("Trama referida a la informacion de las incapacidades.")
    							//##### @Type(type = "list")
    							if (entryName.equalsIgnoreCase("TI")) {
    								this.nominaIncapacidadesIncapacidad = new Nomina.Incapacidades.Incapacidad();
    								ApiPayrollInability payrollInability = new ApiPayrollInability();
    								
    								Integer inabilityDays = UValue.integerValue(UValue.stringValue(entry[startPosition]));
    								this.nominaIncapacidadesIncapacidad.setDiasIncapacidad(inabilityDays);
    								payrollInability.setInabilityDays(UValue.integerString(inabilityDays));
    								
    								String inabilityType = UValue.stringValue(entry[startPosition + 1]);
    								this.nominaIncapacidadesIncapacidad.setTipoIncapacidad(inabilityType);
    								TipoIncapacidadEntity inabilityTypeEntity = iInabilityTypeJpaRepository.findByCodigoAndEliminadoFalse(inabilityType);
    								payrollInability.setInabilityType(inabilityTypeEntity);
    								payrollInability.setInabilityTypeCode(inabilityTypeEntity.getCodigo());
    								payrollInability.setInabilityTypeValue(inabilityTypeEntity.getValor());
    								
    								BigDecimal monetaryAmount = UValue.bigDecimalStrict(UValue.stringValue(entry[endPosition]));
    								this.nominaIncapacidadesIncapacidad.setImporteMonetario(monetaryAmount);
    								payrollInability.setMonetaryAmount(UValue.bigDecimalString(UValue.stringValue(entry[endPosition])));
    								
    								if (UValidator.isNullOrEmpty(this.nominaIncapacidades)) {
    									this.nominaIncapacidades = new Nomina.Incapacidades();
    									apiPayrollInabilities = new ArrayList<>();
    								}
    								this.nominaIncapacidades.getIncapacidads().add(this.nominaIncapacidadesIncapacidad);
    								apiPayrollInabilities.add(payrollInability);
    							}
    							
    							//##### @Entry(code = "TRS", name = "Comprobante :: CFDI relacionados")
    							//##### @Description("Trama referida a la informacion de los cfdi relacionados al comprobante.")
    							//##### @Type(type = "list")
    							if (entryName.equalsIgnoreCase("TRS")) {
    								String uuid = UValue.stringValue(entry[startPosition]);
    										
    								CfdiRelacionado relatedCfdi = new CfdiRelacionado();
    								relatedCfdi.setUUID(uuid);
    								uuids.add(relatedCfdi);
    								
    								RelatedCfdiUuid relatedCfdiUuid = new RelatedCfdiUuid();
    								relatedCfdiUuid.setUuid(uuid);
    								apiRelatedCfdi.getUuids().add(relatedCfdiUuid);
    							}
    							
    							//##### @Entry(code = "TA", name = "Comprobante")
    							//##### @Description("Trama referida a la informacion del comprobante.")
    							if (entryName.equalsIgnoreCase("TA")) {
    								String serie = UValue.stringValue(entry[startPosition]);
    								this.comprobante.setSerie(serie);
    								apiVoucher.setSerie(serie);
    								
    								String sheet = UValue.stringValue(entry[startPosition + 1]);
    								this.comprobante.setFolio(sheet);
    								apiVoucher.setFolio(sheet);
    								
    								BigDecimal subTotal = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 2]));
    								this.comprobante.setSubTotal(subTotal);
    								apiVoucher.setSubTotal(subTotal);
    								apiVoucher.setSubTotalString(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 2])));
    								
    								BigDecimal discount = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 3]));
    								this.comprobante.setDescuento(discount);
    								apiVoucher.setDiscount(discount);
    								apiVoucher.setDiscountString(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 3])));
    								
    								BigDecimal amount = UValue.bigDecimalStrict(UValue.stringValue(entry[startPosition + 4]));
    								this.comprobante.setTotal(amount);
    								apiVoucher.setTotal(amount);
    								apiVoucher.setTotalString(UValue.bigDecimalString(UValue.stringValue(entry[startPosition + 4])));
    								
    								String postalCode = UValue.stringValue(entry[endPosition]);
    								this.comprobante.setLugarExpedicion(postalCode);
    								apiVoucher.setPostalCode(postalCode);
    								apiCfdi.setPostalCode(iPostalCodeJpaRepository.findByCodigoAndEliminadoFalse(postalCode));
    								
    								apiCfdi.setVoucher(apiVoucher);
    							}
    							
    							//##### @Entry(code = "CEND", name = "Fin de comprobante")
    							//##### @Description("Trama referida al fin del comprobante.")
    							endEntry = false;
    							if (entryName.equalsIgnoreCase("CEND")) {
    								endEntry = true;
    								break;
    							}
    						}
						}
    				}
        			
    				if (validationError) {
    					ApiPayrollError error = new ApiPayrollError();
    		    		error.setDescription("Error de validación de contenido de la nómina.");
    		    		if (!UValidator.isNullOrEmpty(apiReceiver)) {
    		    			error.setReceiverRfc(apiReceiver.getRfc());
    			    		error.setReceiverCurp(apiReceiver.getCurp());
    			    		error.setReceiverName(apiReceiver.getNombreRazonSocial());
    		    		}
    		    		error.setEntry(iPayrollEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(entry[0]));
    		    		error.setField(iPayrollFieldJpaRepository.findByNombreAndEliminadoFalse(validationErrorField));
    		    		error.setValidationErrorCode(validationErrorCode);
    		    		error.setValidationErrorMessage(validationErrorMessage);
    		    		
    		    		payrollParser.getErrors().add(error);
    		    		
    		    		errorPayroll ++;
    		    		payrollParser.getStatistics().getPayrollFileStatistics().setNominasError(errorPayroll);
    		    		
    		    		try {
    		    			
    		    			Integer count = i + 1;
    		    			if (fileTypeCode.equalsIgnoreCase(PAYROLL_FILE_TXT_CODE)) {
    		    				while (count < entries.size()) {
    				    			entry = entries.get(count);
    				    			if (entry[0].equalsIgnoreCase("CNOM")) {
    				    				break;
    				    			} else {
    				    				i ++;
    				    				count ++;
    				    			}
    							}
    			    		}
    		    			
    					} catch (Exception e) {
    						e.printStackTrace();
    					}
    		    		
    				} else if ((fileTypeCode.equalsIgnoreCase(PAYROLL_FILE_TXT_CODE) && (UValidator.isNullOrEmpty(nextEntrieIsPrincipal) || nextEntrieIsPrincipal)) ||
        					(fileTypeCode.equalsIgnoreCase(PAYROLL_FILE_CSV_CODE) && (UValidator.isNullOrEmpty(endEntry) || endEntry))) {
    					//##### Registrando Nomina :: Emisor
        				if (!UValidator.isNullOrEmpty(this.nominaEmisor)) {
        					this.nomina.setEmisor(this.nominaEmisor);
        				}
        				
        				//##### Registrando Nomina :: Receptor
        				this.nomina.setReceptor(this.nominaReceptor);
        				apiPayroll.setPayrollReceiver(payrollReceiver);
        				
        				//##### Registrando Nomina :: Percepciones
        				if (!UValidator.isNullOrEmpty(this.nominaPercepciones)) {
        					//##### Registrando percepciones con horas extra (Fichero CSV)
        					if (!perceptionExtraHours.isEmpty()) {
        						perceptionExtraHours.forEach(perception -> this.nominaPercepciones.getPercepcions().add(perception));
        						for (PayrollPerception item : payrollPerceptions) {
        							apiPayrollPerception.getPayrollPerceptions().add(item);
								}
        					}
        					this.nomina.setPercepciones(this.nominaPercepciones);
        				}
        				
        				if (!UValidator.isNullOrEmpty(apiPayrollPerception)) {
        					apiPayroll.setPayrollPerception(apiPayrollPerception);
        				}
        				
        				//##### Registrando Nomina :: Deducciones
        				if (!UValidator.isNullOrEmpty(this.nominaDeducciones)) {
        					this.nomina.setDeducciones(this.nominaDeducciones);
        				}
        				
        				if (!UValidator.isNullOrEmpty(wPayrollDeduction)) {
        					apiPayroll.setPayrollDeduction(wPayrollDeduction);
        				}
        				
        				//##### Registrando Nomina :: Otros pagos
        				if (!UValidator.isNullOrEmpty(this.nominaOtrosPagos) && !this.nominaOtrosPagos.getOtroPagos().isEmpty()) {
        					this.nomina.setOtrosPagos(this.nominaOtrosPagos);
        					
        					if (!apiPayrollOtherPayments.isEmpty()) {
        						apiPayroll.setPayrollOtherPayments(apiPayrollOtherPayments);
        					}
        				}
        				
        				//##### Registrando Nomina :: Incapacidades
        				if (!UValidator.isNullOrEmpty(this.nominaIncapacidades) && !this.nominaIncapacidades.getIncapacidads().isEmpty()) {
        					this.nomina.setIncapacidades(this.nominaIncapacidades);
        					
        					if (!apiPayrollInabilities.isEmpty()) {
        						apiPayroll.setPayrollInabilities(apiPayrollInabilities);
        					}
        				}
        				
        				//##### Registrando Comprobante :: Emisor
        				this.comprobanteEmisor = new Comprobante.Emisor();
        				this.comprobanteEmisor.setRfc(apiCfdi.getEmitter().getRfc());
        				this.comprobanteEmisor.setNombre(apiCfdi.getEmitter().getNombreRazonSocial());
        				this.comprobanteEmisor.setRegimenFiscal(apiCfdi.getEmitter().getRegimenFiscal().getCodigo());
        				this.comprobante.setEmisor(this.comprobanteEmisor);
        				
        				//##### Registrando Comprobante :: Receptor
        				this.comprobante.setReceptor(this.comprobanteReceptor);
        				
        				//##### Registrando Comprobante :: Conceptos
        				this.comprobanteConceptos = new Comprobante.Conceptos();
        				this.comprobanteConceptos.getConceptos().add(comprobanteConcepto);
        				this.comprobante.setConceptos(comprobanteConceptos);
        				
        				//##### Registrando Comprobante :: CFDI relacionados
        		        if (!uuids.isEmpty()) {
        		        	this.cfdiRelacionados.setTipoRelacion(RELATED_CFDI_RELATIONSHIP_TYPE_CODE);
        		        	this.cfdiRelacionados.getCfdiRelacionados().addAll(uuids);
        		        	comprobante.setCfdiRelacionados(this.cfdiRelacionados);
        		        }

        		        //##### Registrando Comprobante :: Complemento Nomina
        		        Complemento complemento = new Complemento();
        		        complemento.getAnies().add(this.nomina);
        		        this.comprobante.getComplementos().add(complemento);
        		        
        		        //##### Llenando wrapper de comprobantes
        		        ApiPayrollVoucher wPayrollVoucher = new ApiPayrollVoucher();
        		        wPayrollVoucher.setVoucher(comprobante);
        		        wPayrollVoucher.setCfdi(apiCfdi);
        		        wPayrollVoucher.setPayroll(apiPayroll);
    					vouchers.add(wPayrollVoucher);
        			}
    				
				} catch (Exception e) {
		    		UPrint.logWithLine(env, "[ERROR] MEGA-CFDI BUSINESS WEBAPP > PAYROLL PARSER :: ERROR TRYING TO GENERATE A VOUCHER");
		    		e.printStackTrace();
		    		val error = new ApiPayrollError();
		    		error.setDescription("Error al intentar generar el comprobante de nómina.");
		    		if (!UValidator.isNullOrEmpty(apiReceiver)) {
		    			error.setReceiverRfc(apiReceiver.getRfc());
			    		error.setReceiverCurp(apiReceiver.getCurp());
			    		error.setReceiverName(apiReceiver.getNombreRazonSocial());
		    		}
		    		error.setEntry(iPayrollEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(entry[0]));
		    		error.setVoucherGenerateError(Boolean.TRUE);
		    		error.setVoucherGenerateErrorException(e.getStackTrace().toString());
		    		
		    		payrollParser.getErrors().add(error);
		    		
		    		errorPayroll ++;
		    		payrollParser.getStatistics().getPayrollFileStatistics().setNominasError(errorPayroll);
		    		
		    		try {
		    			
		    			Integer count = i + 1;
		    			if (fileTypeCode.equalsIgnoreCase(PAYROLL_FILE_TXT_CODE)) {
		    				while (count < entries.size()) {
				    			entry = entries.get(count);
				    			if (entry[0].equalsIgnoreCase("CNOM")) {
				    				break;
				    			} else {
				    				i ++;
				    				count ++;
				    			}
							}
			    		}
		    			
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}    			
    		}
    		
    		payrollParser.getStatistics().getPayrollFileStatistics().setFechaFinGeneracionComprobante(new Date());
    		payrollParser.getStatistics().getPayrollFileStatistics().setHoraFinGeneracionComprobante(new Date());
    		
    		UPrint.logWithLine(env, "[INFO] MEGA-CFDI BUSINESS WEBAPP > PAYROLL PARSER :: GENERATING VOUCHERS FINISHED...");
    		
    		return vouchers;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	private Boolean isNextEntryPrincipal(List<String[]> entries, int pos) {
		Boolean nextEntrieIsPrincipal = false;
		try {
			
			String[] nextEntry = entries.get(pos + 1);
			val entryEntity = iPayrollEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(nextEntry[0]);
			nextEntrieIsPrincipal = entryEntity.isPrincipal();
			
		} catch (Exception e) {
			nextEntrieIsPrincipal = null;
		}
		return nextEntrieIsPrincipal;
	}
	
}