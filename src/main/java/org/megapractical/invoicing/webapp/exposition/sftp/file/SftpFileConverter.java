package org.megapractical.invoicing.webapp.exposition.sftp.file;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;
import org.megapractical.invoicing.webapp.exposition.sftp.file.JSftpFile.SftpFile;
import org.megapractical.invoicing.webapp.util.FileStatusUtils;

/**
 * @author Maikel Guerra Ferrer
 *
 */
final class SftpFileConverter {

	private SftpFileConverter() {		
	}
	
	public static SftpFile convert(ArchivoStfpEntity entity) {
		if (entity == null) {
			return null;
		}
		
		return SftpFile
				.builder()
				.id(UBase64.base64Encode(UValue.longString(entity.getId())))				
				.name(UBase64.base64Encode(entity.getNombre()))
				.fileStatus(UBase64.base64Encode(FileStatusUtils.status(entity.getStatus())))
				.uploadDate(UBase64.base64Encode(UDateTime.format(entity.getFechaCarga())))
				.area(UBase64.base64Encode(entity.getArea().name()))
				.sftpPath(UBase64.base64Encode(entity.getRutaDescarga()))
				.errorValidationPath(UBase64.base64Encode(entity.getRutaErrorValidacion()))
				.errorContentPath(UBase64.base64Encode(entity.getRutaErrorContenido()))
				.errorS3Path(UBase64.base64Encode(entity.getRutaErrorS3()))
				.build();
	}
	
}