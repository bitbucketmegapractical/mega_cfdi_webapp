package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentCertificateValidation {
	
	String getCertificate(String paymentCertificate, String stringType);
	
	String getCertificate(String paymentCertificate, String field, String stringType, List<JError> errors);
	
}