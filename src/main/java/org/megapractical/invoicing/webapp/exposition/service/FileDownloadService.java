package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.dal.bean.datatype.StorageType;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.FileDownload;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.FileDownload.FileDownloadData;
import org.megapractical.invoicing.webapp.exposition.service.s3.S3Service;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class FileDownloadService {

	private final AppSettings appSettings;
	private final S3Service s3Service;
	
	private static final String CONTENTTYPE_XML = "application/xml";
	private static final String CONTENTTYPE_PDF = "application/pdf";
	
	public FileDownload download(FileDownloadData data) {
		try {
			if (data != null) {
				String path = null;
				byte[] fileByte = null;
				String fileName = null;
				String contentType = null;
				
				val fileType = data.getFileType();
				val storage = data.getStorageType();
				val xmlPath = data.getXmlPath();
				val pdfPath = data.getPdfPath();
				val xmlFileNameFromS3 = data.getXmlFileNameFromS3();
				val pdfFileNameFromS3 = data.getPdfFileNameFromS3();
				
				if (StorageType.SERVER.equals(storage)) {
					val repositoryPath = appSettings.getPropertyValue("cfdi.repository");
					val absolutePath = repositoryPath + File.separator;
					if (fileType.equalsIgnoreCase("xml")) {
						path = absolutePath + xmlPath;
						contentType = CONTENTTYPE_XML;
					} else if (fileType.equalsIgnoreCase("pdf")) {
						path = absolutePath + pdfPath;
						contentType = CONTENTTYPE_PDF;
					}
					
					val file = new File(path);
					fileByte = UFile.fileToByte(file);
					fileName = file.getName();
				} else if (StorageType.S3.equals(storage)) {
					String s3Path = null;
					if (fileType.equalsIgnoreCase("xml")) {
						s3Path = xmlPath;
						fileName = xmlFileNameFromS3;
						contentType = CONTENTTYPE_XML;
					} else if (fileType.equalsIgnoreCase("pdf")) {
						s3Path = pdfPath;
						fileName = pdfFileNameFromS3;
						contentType = CONTENTTYPE_PDF;
					}
					fileByte = s3Service.getObject(s3Path);
				}
				
				if (fileByte != null) {
					return FileDownload
							.builder()
							.fileByte(fileByte)
							.fileName(fileName)
							.contentType(contentType)
							.build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}