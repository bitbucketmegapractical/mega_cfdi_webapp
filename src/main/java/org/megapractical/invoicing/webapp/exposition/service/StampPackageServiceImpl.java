package org.megapractical.invoicing.webapp.exposition.service;

import java.util.stream.Collectors;

import org.megapractical.invoicing.dal.data.repository.IStampPackageJpaRepository;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JStampPackage;
import org.megapractical.invoicing.webapp.json.JStampPackage.StampPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class StampPackageServiceImpl implements StampPackageService {

	@Autowired
	private IStampPackageJpaRepository iStampPackageJpaRepository;
	
	@Override
	public JStampPackage stampPackageInfo() {
		val stampPackageSource = iStampPackageJpaRepository.findByEliminadoFalseOrderByCantidad();
		val stampPackages = stampPackageSource.stream().map(StampPackage::new).collect(Collectors.toList());
		val response = JResponse.success();
		return new JStampPackage(stampPackages, response);
	}

}