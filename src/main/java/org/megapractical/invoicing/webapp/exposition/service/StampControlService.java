package org.megapractical.invoicing.webapp.exposition.service;

import java.util.Date;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreControlEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class StampControlService {

	@Autowired
	UserTools userTools;

	@Autowired
	IPersistenceDAO persistenceDAO;

	@Autowired
	AppLog appLog;

	@Autowired
	TimelineLog timelineLog;

	@Resource
	private IStampJpaRepository iStampJpaRepository;

	public TimbreEntity stampDetails() {
		val taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
		return iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayer);
	}

	public TimbreEntity stampDetails(ContribuyenteEntity taxpayer) {
		return iStampJpaRepository.findByContribuyente(taxpayer);
	}

	public boolean stampStatus() {
		val stamp = stampDetails();
		return !UValidator.isNullOrEmpty(stamp);
	}
	
	public boolean stampStatus(ContribuyenteEntity taxpayer) {
		val stamp = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayer);
		return !UValidator.isNullOrEmpty(stamp);
	}

	public TimbreEntity stampControl(ContribuyenteEntity taxpayer) {
		stampControlUpdate(taxpayer);

		userTools.log("[INFO] UPDATING USER'S STAMP INFORMATION...");

		var stamp = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayer);
		if (!UValidator.isNullOrEmpty(stamp)) {
			val consumedStamp = stamp.getTimbresConsumidos() + 1;
			var availableStamp = stamp.getTimbresDisponibles() - 1;
			if (availableStamp < 0) {
				availableStamp = 0;
			}
			val enabled = availableStamp > 0;

			stamp.setContribuyente(taxpayer);
			stamp.setTimbresConsumidos(consumedStamp);
			stamp.setTimbresDisponibles(availableStamp);
			stamp.setHabilitado(enabled);
			stamp = (TimbreEntity) persistenceDAO.update(stamp);

			// Bitacora :: Registro de accion en bitacora
			appLog.logOperation(OperationCode.REGISTRO_CONTROL_TIMBRE_ID51);

			// Timeline :: Registro de accion
			timelineLog.timelineLog(EventTypeCode.REGISTRO_TIMBRADO_ID26);
		}
		return stamp;
	}

	public TimbreEntity stampControlAsync(ContribuyenteEntity taxpayer, UsuarioEntity user, String urlAcceso,
			String ip) {
		try {

			stampControlUpdate(taxpayer);
			userTools.log("[INFO] UPDATING USER'S STAMP INFORMATION...");

			var stamp = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayer);
			if (!UValidator.isNullOrEmpty(stamp)) {
				val consumedStamp = stamp.getTimbresConsumidos() + 1;
				val availableStamp = stamp.getTimbresDisponibles() - 1;
				val enabled = availableStamp > 0;

				stamp.setContribuyente(taxpayer);
				stamp.setTimbresConsumidos(consumedStamp);
				stamp.setTimbresDisponibles(availableStamp);
				stamp.setHabilitado(enabled);
				stamp = (TimbreEntity) persistenceDAO.update(stamp);

				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.REGISTRO_CONTROL_TIMBRE_ID51, user, urlAcceso, ip);

				// Timeline :: Registro de accion
				timelineLog.timelineLog(EventTypeCode.REGISTRO_TIMBRADO_ID26, taxpayer, user, ip);
			}
			return stamp;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void stampControlUpdate(ContribuyenteEntity taxpayer) {
		userTools.log("[INFO] REGISTERING USER'S STAMP CONTROL INFORMATION...");
		val stampControl = new TimbreControlEntity();
		stampControl.setContribuyente(taxpayer);
		stampControl.setFechaTimbrado(new Date());
		stampControl.setHoraTimbrado(new Date());
		stampControl.setCantidad(1);
		persistenceDAO.persist(stampControl);
	}
}
