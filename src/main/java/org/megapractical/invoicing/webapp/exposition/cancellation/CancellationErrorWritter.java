package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.io.FileWriter;
import java.util.List;

import org.megapractical.invoicing.api.smarterweb.SWCancellationResponse;
import org.megapractical.invoicing.api.util.ApiFileErrorWritter;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
final class CancellationErrorWritter {
	
	private CancellationErrorWritter() {		
	}
	
	public static String populateError(SWCancellationResponse cancellationResponse) {
		if (!cancellationResponse.isSuccess()) {
			val errorBuilder = new StringBuilder();
			errorBuilder.append("Error de cancelación: ");
			errorBuilder.append(cancellationResponse.getUuid());
			errorBuilder.append(" - " + cancellationResponse.getErrorMessage());
			return errorBuilder.toString();
		}
		return null;
	}
	
	public static String uuidCancelledError(String uuid) {
		val errorBuilder = new StringBuilder();
		errorBuilder.append("Error de validación: ");
		errorBuilder.append("El uuid " + uuid);
		errorBuilder.append(" ha sido cancelado previamente o está en proceso de cancelación");
		return errorBuilder.toString();
	}
	
	public static String uuidNotFoundError(String uuid) {
		val errorBuilder = new StringBuilder();
		errorBuilder.append("Error de validación: ");
		errorBuilder.append("El uuid " + uuid + " no existe");
		return errorBuilder.toString();
	}
	
	public static void writeError(String fileErrorPath, List<String> errors) {
		try {
			val errorFile = ApiFileErrorWritter.errorFile(fileErrorPath);
			if (errorFile != null) {
				try (val writer = new CSVWriter(new FileWriter(errorFile), '|', ICSVWriter.NO_QUOTE_CHARACTER,
						ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END)) {
					for (val error : errors) {
						val sentence = new StringBuilder();
						sentence.append(error);
						
						val errorSet = new String[] { sentence.toString() };
						writer.writeNext(errorSet);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}