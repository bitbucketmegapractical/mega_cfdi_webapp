package org.megapractical.invoicing.webapp.exposition.service;

public interface SessionService {
	
	String getIp();
	
}