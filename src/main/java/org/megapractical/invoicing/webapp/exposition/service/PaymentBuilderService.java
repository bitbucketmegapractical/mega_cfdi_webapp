package org.megapractical.invoicing.webapp.exposition.service;

import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPaymentParser;

public interface PaymentBuilderService {
	
	List<ApiCfdi> build(ApiPaymentParser paymentParser);
	
}