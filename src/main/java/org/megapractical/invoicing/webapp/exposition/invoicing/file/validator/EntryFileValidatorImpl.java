package org.megapractical.invoicing.webapp.exposition.invoicing.file.validator;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.common.EntryBase;
import org.megapractical.invoicing.dal.bean.common.FieldBase;
import org.springframework.stereotype.Service;

@Service
public class EntryFileValidatorImpl implements EntryFileValidator {
	
	@Override
	public boolean validateContent(EntryBase entry, FieldBase field, String value) {
		try {
			if (entry == null || field == null || value == null) {
				return false;
			}
			
			value = value.trim();
			
			//##### Valida si la trama y el campo son requeridos
			//##### El campo debe traer valor
			if (entry.isRequerido() && field.isRequerido() && value.isEmpty()) {
				return false;
			}
			
			//##### Valida que la longitud sea igual a la longitud del campo
			if (!UValidator.isNullOrEmpty(field.getLongitud())) {
				if (Boolean.TRUE.equals(entry.isRequerido())) {
					if (Boolean.TRUE.equals(field.isRequerido())) {
						return value.length() == field.getLongitud();
					} else {
						if (!UValidator.isNullOrEmpty(value)) {
							return value.length() == field.getLongitud();
						}
					}					
				} else {
					if (!UValidator.isNullOrEmpty(value)) {
						return value.length() == field.getLongitud();
					}
				}
			}
			
			//##### Valida que la longitud sea mayor o igual a la longitud minima del campo
			//##### No se tiene en cuenta la longitud maxima
			if (!UValidator.isNullOrEmpty(field.getLongitudMinima()) && UValidator.isNullOrEmpty(field.getLongitudMaxima())) {
				if (Boolean.TRUE.equals(entry.isRequerido())) {
					if (Boolean.TRUE.equals(field.isRequerido())) {
						return value.length() >= field.getLongitudMinima();
					} else {
						if (!UValidator.isNullOrEmpty(value)) {
							return value.length() >= field.getLongitudMinima();
						}
					}
				} else {
					if (!UValidator.isNullOrEmpty(value)) {
						return value.length() >= field.getLongitudMinima();
					}
				}
			}
			
			//##### Valida que la longitud sea mayor o igual a la longitud minima
			//##### y menor o igual a la longitd maxima del campo
			if (!UValidator.isNullOrEmpty(field.getLongitudMinima()) && !UValidator.isNullOrEmpty(field.getLongitudMaxima())) {
				if (Boolean.TRUE.equals(entry.isRequerido())) {
					if (Boolean.TRUE.equals(field.isRequerido())) {
						return value.length() >= field.getLongitudMinima() && value.length() <= field.getLongitudMaxima();
					} else {
						if (!UValidator.isNullOrEmpty(value)) {
							return value.length() >= field.getLongitudMinima() && value.length() <= field.getLongitudMaxima();
						}
					}
				} else {
					if (!UValidator.isNullOrEmpty(value)) {
						return value.length() >= field.getLongitudMinima() && value.length() <= field.getLongitudMaxima();
					}
				}
			}
			
			//##### Valida que la longitud sea menor o igual a la longitud maxima del campo
			//##### No se tiene en cuenta la longitud minima
			if (!UValidator.isNullOrEmpty(field.getLongitudMaxima()) && UValidator.isNullOrEmpty(field.getLongitudMinima())) {
				if (Boolean.TRUE.equals(entry.isRequerido())) {
					if (Boolean.TRUE.equals(field.isRequerido())) {
						return value.length() <= field.getLongitudMaxima();
					} else {
						if (!UValidator.isNullOrEmpty(value)) {
							return value.length() <= field.getLongitudMaxima();
						}
					}
				} else {
					if (!UValidator.isNullOrEmpty(value)) {
						return value.length() <= field.getLongitudMaxima();
					}
				}
			}
			
			//##### Valida por expresion regular
			if (!UValidator.isNullOrEmpty(field.getValidacion())) {
				if (Boolean.TRUE.equals(entry.isRequerido())) {
					if (Boolean.TRUE.equals(field.isRequerido())) {
						return value.matches(field.getValidacion());
					} else {
						if (!UValidator.isNullOrEmpty(value)) {
							return value.matches(field.getValidacion());
						}
					}
				} else {
					if (!UValidator.isNullOrEmpty(value)) {
						return value.matches(field.getValidacion());
					}
				}
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}