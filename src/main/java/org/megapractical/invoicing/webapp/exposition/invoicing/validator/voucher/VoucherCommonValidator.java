package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ValidationResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;

public interface VoucherCommonValidator {
	
	ValidationResponse getValidationResponse(JVoucher voucher, List<JError> errors);
	
}