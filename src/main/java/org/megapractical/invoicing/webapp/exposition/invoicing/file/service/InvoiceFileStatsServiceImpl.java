package org.megapractical.invoicing.webapp.exposition.invoicing.file.service;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;
import org.megapractical.invoicing.dal.data.repository.IInvoiceFileStatisticsJpaRepository;
import org.springframework.stereotype.Service;

@Service
public class InvoiceFileStatsServiceImpl implements InvoiceFileStatsService {

	private final IInvoiceFileStatisticsJpaRepository iInvoiceFileStatisticsJpaRepository;
	
	public InvoiceFileStatsServiceImpl(IInvoiceFileStatisticsJpaRepository iInvoiceFileStatisticsJpaRepository) {
		this.iInvoiceFileStatisticsJpaRepository = iInvoiceFileStatisticsJpaRepository;
	}

	@Override
	public ArchivoFacturaEstadisticaEntity findByInvoiceFile(ArchivoFacturaEntity invoiceFile) {
		return iInvoiceFileStatisticsJpaRepository.findByArchivoFactura(invoiceFile);
	}

	@Override
	public ArchivoFacturaEstadisticaEntity save(ArchivoFacturaEstadisticaEntity entity) {
		if (entity != null) {
			return iInvoiceFileStatisticsJpaRepository.save(entity);
		}
		return null;
	}

}