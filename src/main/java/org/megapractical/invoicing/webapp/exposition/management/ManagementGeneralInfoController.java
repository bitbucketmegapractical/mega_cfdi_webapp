package org.megapractical.invoicing.webapp.exposition.management;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.BitacoraEntity;
import org.megapractical.invoicing.dal.data.repository.ILogJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IProfileJpaRepository;
import org.megapractical.invoicing.webapp.json.JBitacora;
import org.megapractical.invoicing.webapp.json.JBitacora.Bitacora;
import org.megapractical.invoicing.webapp.json.JManagementGeneral;
import org.megapractical.invoicing.webapp.json.JManagementGeneral.GeneralInfo;
import org.megapractical.invoicing.webapp.json.JManagementGeneral.LineData;
import org.megapractical.invoicing.webapp.json.JManagementGeneral.LineData.DataSet;
import org.megapractical.invoicing.webapp.json.JManagementGeneral.StatisticsMonth;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
@Scope("session")
public class ManagementGeneralInfoController {
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Resource
	IProfileJpaRepository iProfileJpaRepository;
	
	@Resource
	ILogJpaRepository iLogJpaRepository;
	
	private static final String ROLE_USER = "ROLE_USR";
	private static final String ROLE_MASTER = "ROLE_MST";
	
	private static final String CFDI_FREE = "CFDI FREE";
	private static final String CFDI_PLUS = "CFDI PLUS";
	
	private static final String CFDI_ADM = "ADMON";
	private static final String CFDI_ROOT = "ROOT";
	
	private static final String USER_REGISTERED_CFDI_FREE_CODE = "REGISTRO_CUENTA_USUARIO";
	private static final String USER_REGISTERED_CFDI_PLUS_CODE = "ACTIVACION_PAQUETE_TIMBRE";
	
	private static final String CFDI_GENERATED_CFDI_FREE_CODE = "GENERACION_CFDIV33_FREE";
	private static final String CFDI_GENERATED_CFDI_PLUS_CODE = "GENERACION_CFDIV33_SIN_COMPLEMENTO_MASTER";
	private static final String CFDI_GENERATED_CFDI_PLUS_CODE1 = "GENERACION_CFDIV33_CON_COMPLEMENTO_MASTER";
	
	private static final String CFDI_CANCELED_CFDI_FREE_CODE = "CANCELACION_CFDIV33_FREE";
	private static final String CFDI_CANCELED_CFDI_PLUS_CODE = "CANCELACION_CFDIV33_SIN_COMPLEMENTO_MASTER";
	private static final String CFDI_CANCELED_CFDI_PLUS_CODE1 = "CANCELACION_CFDIV33_CON_COMPLEMENTO_MASTER";
	
	private Integer logYear;
	private List<StatisticsMonth> statisticsMonths;
	
	@RequestMapping(value = "/managementGeneralInfo", method = RequestMethod.POST)
	public @ResponseBody String managementGeneralInfo() {
		
		JManagementGeneral management = new JManagementGeneral();
		JBitacora log = new JBitacora();
		
		JResponse response = new JResponse();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			List<BitacoraEntity> surceList = iLogJpaRepository.findByMesIsNullOrDiaIsNull();
			for (BitacoraEntity item : surceList) {
				Integer month = UDateTime.monthOfDate(item.getFecha());
				item.setMes(month);
				
				Integer day = UDateTime.dayOfDate(item.getFecha());
				item.setDia(day);
				
				persistenceDAO.update(item);
			}
			
			logYear = UDateTime.yearOfDate(new Date());
			
			statisticsMonths = new ArrayList<>();
			List<Integer> logs = iLogJpaRepository.findDistinctMonthsInLog(logYear);
			for (Integer item : logs) {
				String monthName = UDateTime.monthOfDate(item, false);
				String monthNameAbbreviation = UDateTime.monthOfDate(item, true);
				
				StatisticsMonth statisticsMonth = new StatisticsMonth();
				statisticsMonth.setMonthName(monthName);
				statisticsMonth.setMonthNameAbbreviation(monthNameAbbreviation);
				statisticsMonth.setMonth(item);
				
				if(!statisticsMonths.contains(statisticsMonth)){
					statisticsMonths.add(statisticsMonth);
				}				
			}		
			
			//##### Cargando informacion general
			management.setGeneralInfo(getGeneralInfo());
			
			//##### Cargando bitacora del dia
			log.setLogDate(UBase64.base64Encode(UDate.formattedSingleDate(new Date())));
			log.getLogs().addAll(getLog());
			management.setLog(log);
			
			//##### Cargando estadisticas de usuarios 
			String statisticsType = "user";
			management.setLineData(getLineData(statisticsType));
			
			//##### Cargando estadisticas de timbrado
			statisticsType = "stamp";
			management.setLineData1(getLineData(statisticsType));
			
			//##### Actualizando ultima fecha y hora
			management.setLastUpdate(UBase64.base64Encode(UDate.formattedDateTime()));
			
			response.setSuccess(Boolean.TRUE);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		management.setResponse(response);
		jsonInString = gson.toJson(management);
		
		return jsonInString;
	}
	
	@RequestMapping(value = "/managementGeneralInfoReload", method = RequestMethod.POST)
	public @ResponseBody String managementGeneralInfoReload() {

		JManagementGeneral management = new JManagementGeneral();
		JBitacora log = new JBitacora();
		
		JResponse response = new JResponse();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			//##### Cargando informacion general
			management.setGeneralInfo(getGeneralInfo());
			
			//##### Cargando bitacora del dia
			log.setLogDate(UBase64.base64Encode(UDate.formattedSingleDate(new Date())));
			log.getLogs().addAll(getLog());
			management.setLog(log);
			
			//##### Cargando estadisticas de usuarios 
			String statisticsType = "user";
			management.setLineData(getLineData(statisticsType));
			
			//##### Cargando estadisticas de timbrado
			statisticsType = "stamp";
			management.setLineData1(getLineData(statisticsType));
			
			//##### Actualizando ultima fecha y hora
			management.setLastUpdate(UBase64.base64Encode(UDate.formattedDateTime()));
			
			response.setSuccess(Boolean.TRUE);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		management.setResponse(response);
		jsonInString = gson.toJson(management);
		
		return jsonInString;
	}
	
	public GeneralInfo getGeneralInfo(){
		try {
			
			GeneralInfo generalInfo = new GeneralInfo();
			
			//##### Total Usuarios Mega-CFDI Plus
			Long userFree = iProfileJpaRepository.countByRol_CodigoEquals(ROLE_USER);
			String totalUserFree = UBase64.base64Encode(UValue.longString(userFree));
			generalInfo.setMegaCfdiFreeRegisteredUsers(totalUserFree);
			
			//##### Total Usuarios Mega-CFDI Free
			Long userMaster = iProfileJpaRepository.countByRol_CodigoEquals(ROLE_MASTER);
			String totalUserMaster = UBase64.base64Encode(UValue.longString(userMaster));
			generalInfo.setMegaCfdiPlusRegisteredUsers(totalUserMaster);
			
			Integer masterVisitors = 0;
			Integer freeVisitors = 0;
			
			Integer freeRegisteredUser = 0;
			Integer masterRegisteredUser = 0;
			
			Integer freeCfdiGenerated = 0;
			Integer plusCfdiGenerated = 0;
			
			Integer freeCfdiCanceled = 0;
			Integer plusCfdiCanceled = 0;
			
			//##### Bitacora del dia
			List<BitacoraEntity> logList = iLogJpaRepository.findByFechaEquals(UDateTime.getToday());
			for (BitacoraEntity item : logList) {
				//##### Visitas en el dia
				if(item.getTipoOperacion().getCodigo().equalsIgnoreCase("ACCESO_EXITOSO")){
					String authority =  item.getUsuario().getAuthority();
					if(authority.equalsIgnoreCase("role_mst")){
						masterVisitors ++;
					}else if(authority.equalsIgnoreCase("role_usr")){
						freeVisitors ++;
					}
				}
				
				//##### Registro de usuarios
				if(item.getTipoOperacion().getCodigo().equalsIgnoreCase("REGISTRO_CUENTA_USUARIO")){
					freeRegisteredUser ++;
				}else if(item.getTipoOperacion().getCodigo().equalsIgnoreCase("ACTUALIZACION_PERFIL_USUARIO_A_MASTER")){
					masterRegisteredUser ++;
				}
				
				//##### CFDI generados 
				if(item.getTipoOperacion().getCodigo().equalsIgnoreCase("GENERACION_CFDIV33_FREE")){
					freeCfdiGenerated ++;
				}else if(item.getTipoOperacion().getCodigo().equalsIgnoreCase("GENERACION_CFDIV33_SIN_COMPLEMENTO_MASTER") || item.getTipoOperacion().getCodigo().equalsIgnoreCase("GENERACION_CFDIV33_CON_COMPLEMENTO_MASTER")){
					plusCfdiGenerated ++;
				}
				
				//##### CFDI cancelados
				if(item.getTipoOperacion().getCodigo().equalsIgnoreCase("CANCELACION_CFDIV33_FREE")){
					freeCfdiCanceled ++;
				}else if(item.getTipoOperacion().getCodigo().equalsIgnoreCase("CANCELACION_CFDIV33_SIN_COMPLEMENTO_MASTER") || item.getTipoOperacion().getCodigo().equalsIgnoreCase("CANCELACION_CFDIV33_CON_COMPLEMENTO_MASTER")){
					plusCfdiCanceled ++;
				}				
			}
			
			//##### Visitas usuarios free
			generalInfo.setCfdiFreeVisitsToday(UBase64.base64Encode(freeVisitors.toString()));
			
			//##### Visitas usuario master
			generalInfo.setCfdiPlusVisitsToday(UBase64.base64Encode(masterVisitors.toString()));
			
			//##### Registro usuarios free
			generalInfo.setCfdiFreeRegisteredUsersToday(UBase64.base64Encode(freeRegisteredUser.toString()));
			
			//##### Registro usuarios master (Activacion de timbres)
			generalInfo.setCfdiPlusRegisteredUsersToday(UBase64.base64Encode(masterRegisteredUser.toString()));
			
			//##### CFDI generados free
			generalInfo.setCfdiFreeGeneratedInvoicesToday(UBase64.base64Encode(freeCfdiGenerated.toString()));
			
			//##### CFDI generaods master
			generalInfo.setCfdiPlusGeneratedInvoicesToday(UBase64.base64Encode(plusCfdiGenerated.toString()));
			
			//##### CFDI cancelados free
			generalInfo.setCfdiFreeCanceledInvoicesToday(UBase64.base64Encode(freeCfdiCanceled.toString()));
			
			//##### CFDI cancelados master
			generalInfo.setCfdiPlusCanceledInvoicesToday(UBase64.base64Encode(plusCfdiCanceled.toString()));
			
			return generalInfo;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public LineData getLineData(String type){
		try {
			
			//##### Grafica estadisticas de usuario
			LineData lineData = new LineData();
			
			List<String> lineDataMonths = new ArrayList<>();
			
			List<DataSet> dataSets = new ArrayList<>();
			List<String> dataSource = new ArrayList<>();
			
			for (StatisticsMonth item : statisticsMonths) {
				lineDataMonths.add(UBase64.base64Encode(item.getMonthNameAbbreviation()));
			}
			
			if(type.equals("user")){
				//##### Usuarios Plus
				DataSet dataSet = new DataSet();
				dataSet.setLabel(UBase64.base64Encode("Mega-CFDI Plus"));
				dataSet.setBackgroundColor(UBase64.base64Encode("rgba(26,179,148,0.5)"));
				dataSet.setBorderColor(UBase64.base64Encode("rgba(26,179,148,0.7)"));
				dataSet.setPointBackgroundColor(UBase64.base64Encode("rgba(26,179,148,1)"));
				dataSet.setPointBorderColor(UBase64.base64Encode("#fff"));
				
				for (StatisticsMonth item : statisticsMonths) {
					Long value = iLogJpaRepository.countByTipoOperacion_CodigoAndEjercicioAndMes(USER_REGISTERED_CFDI_PLUS_CODE, logYear, item.getMonth());
					dataSource.add(UBase64.base64Encode(UValue.longString(value)));
				}
				
				dataSet.getData().addAll(dataSource);
				dataSets.add(dataSet);
				
				//##### Usuario Free
				dataSet = new DataSet();
				dataSet.setLabel(UBase64.base64Encode("Mega-CFDI Free"));
				dataSet.setBackgroundColor(UBase64.base64Encode("rgba(220,220,220,0.5)"));
				dataSet.setBorderColor(UBase64.base64Encode("rgba(220,220,220,1)"));
				dataSet.setPointBackgroundColor(UBase64.base64Encode("rgba(220,220,220,1)"));
				dataSet.setPointBorderColor(UBase64.base64Encode("#fff"));
				
				dataSource = new ArrayList<>();
				for (StatisticsMonth item : statisticsMonths) {
					Long value = iLogJpaRepository.countByTipoOperacion_CodigoAndEjercicioAndMes(USER_REGISTERED_CFDI_FREE_CODE, logYear, item.getMonth());
					dataSource.add(UBase64.base64Encode(UValue.longString(value)));
				}
				
				dataSet.getData().addAll(dataSource);
				dataSets.add(dataSet);
			}else if(type.equals("stamp")){
				//##### Generados MEGA-CFDI Free
				DataSet dataSet = new DataSet();
				dataSet.setLabel(UBase64.base64Encode("Generados MEGA-CFDI Free"));
				dataSet.setBackgroundColor(UBase64.base64Encode("rgba(35,198,200,0.5)"));
				dataSet.setBorderColor(UBase64.base64Encode("rgba(35,198,200,0.7)"));
				dataSet.setPointBackgroundColor(UBase64.base64Encode("rgba(35,198,200,1)"));
				dataSet.setPointBorderColor(UBase64.base64Encode("#fff"));
				
				for (StatisticsMonth item : statisticsMonths) {
					Long value = iLogJpaRepository.countByTipoOperacion_CodigoAndEjercicioAndMes(CFDI_GENERATED_CFDI_FREE_CODE, logYear, item.getMonth());
					dataSource.add(UBase64.base64Encode(UValue.longString(value)));
				}
				
				dataSet.getData().addAll(dataSource);
				dataSets.add(dataSet);
				
				//##### Cancelados MEGA-CFDI Free
				dataSet = new DataSet();
				dataSet.setLabel(UBase64.base64Encode("Cancelados MEGA-CFDI Free"));
				dataSet.setBackgroundColor(UBase64.base64Encode("rgba(248,172,89,0.5)"));
				dataSet.setBorderColor(UBase64.base64Encode("rgba(248,172,89,1)"));
				dataSet.setPointBackgroundColor(UBase64.base64Encode("rgba(248,172,89,1)"));
				dataSet.setPointBorderColor(UBase64.base64Encode("#fff"));
				
				dataSource = new ArrayList<>();
				for (StatisticsMonth item : statisticsMonths) {
					Long value = iLogJpaRepository.countByTipoOperacion_CodigoAndEjercicioAndMes(CFDI_CANCELED_CFDI_FREE_CODE, logYear, item.getMonth());
					dataSource.add(UBase64.base64Encode(UValue.longString(value)));
				}
				
				dataSet.getData().addAll(dataSource);
				dataSets.add(dataSet);
				
				//##### Generados MEGA-CFDI Plus
				dataSet = new DataSet();
				dataSet.setLabel(UBase64.base64Encode("Generados MEGA-CFDI Plus"));
				dataSet.setBackgroundColor(UBase64.base64Encode("rgba(26,179,148,0.5"));
				dataSet.setBorderColor(UBase64.base64Encode("rgba(26,179,148,0.7)"));
				dataSet.setPointBackgroundColor(UBase64.base64Encode("rgba(26,179,148,1)"));
				dataSet.setPointBorderColor(UBase64.base64Encode("#fff"));
				
				dataSource = new ArrayList<>();
				for (StatisticsMonth item : statisticsMonths) {
					Long value = iLogJpaRepository.countByTipoOperacion_CodigoAndEjercicioAndMes(CFDI_GENERATED_CFDI_PLUS_CODE, logYear, item.getMonth());
					Long value1 = iLogJpaRepository.countByTipoOperacion_CodigoAndEjercicioAndMes(CFDI_GENERATED_CFDI_PLUS_CODE1, logYear, item.getMonth());
					Long result = value + value1;
					dataSource.add(UBase64.base64Encode(UValue.longString(result)));
				}
				
				dataSet.getData().addAll(dataSource);
				dataSets.add(dataSet);
				
				//##### Cancelados MEGA-CFDI Plus
				dataSet = new DataSet();
				dataSet.setLabel(UBase64.base64Encode("Cancelados MEGA-CFDI Plus"));
				dataSet.setBackgroundColor(UBase64.base64Encode("rgba(237,85,101,0.5)"));
				dataSet.setBorderColor(UBase64.base64Encode("rgba(237,85,101,1)"));
				dataSet.setPointBackgroundColor(UBase64.base64Encode("rgba(237,85,101,1)"));
				dataSet.setPointBorderColor(UBase64.base64Encode("#fff"));
				
				dataSource = new ArrayList<>();
				for (StatisticsMonth item : statisticsMonths) {
					Long value = iLogJpaRepository.countByTipoOperacion_CodigoAndEjercicioAndMes(CFDI_CANCELED_CFDI_PLUS_CODE, logYear, item.getMonth());
					Long value1 = iLogJpaRepository.countByTipoOperacion_CodigoAndEjercicioAndMes(CFDI_CANCELED_CFDI_PLUS_CODE1, logYear, item.getMonth());
					Long result = value + value1;
					dataSource.add(UBase64.base64Encode(UValue.longString(result)));
				}
				
				dataSet.getData().addAll(dataSource);
				dataSets.add(dataSet);
			}
			
			lineData.getMonths().addAll(lineDataMonths);
			lineData.getDataSets().addAll(dataSets);
			
			return lineData;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Bitacora> getLog(){
		try {
			List<Bitacora> logs = new ArrayList<>();
			
			List<BitacoraEntity> sourceList = iLogJpaRepository.findByFechaEqualsOrderByFechaDescHoraDesc(UDateTime.getToday());
			for (BitacoraEntity item : sourceList) {
				Bitacora log = new Bitacora();
				
				try {
					
					String username = item.getUsuario().getCorreoElectronico();
					String usernameLower = UBase64.base64Encode(username.toLowerCase());
					log.setUsername(usernameLower);
					
					String operationDescription = UBase64.base64Encode(item.getTipoOperacion().getValor());
					log.setOperationDescription(operationDescription);
					
					String operationImg = UBase64.base64Encode(item.getTipoOperacion().getImagen());
					log.setOperationImg(operationImg);
					
					String product = userTools.isRoot(username) ? UBase64.base64Encode(CFDI_ROOT) : (userTools.isAdmon(username) ? UBase64.base64Encode(CFDI_ADM) : (userTools.isMaster(username) ? UBase64.base64Encode(CFDI_PLUS) : UBase64.base64Encode(CFDI_FREE)));
					log.setProduct(product);
					
					String time = UBase64.base64Encode(UDate.formattedShortTime(item.getHora()).toLowerCase());
					log.setTime(time);
					
					logs.add(log);
					
				} catch (Exception e) {
					log = null;
				}
				
			}
			return logs;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
