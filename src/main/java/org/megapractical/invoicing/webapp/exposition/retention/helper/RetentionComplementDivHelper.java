package org.megapractical.invoicing.webapp.exposition.retention.helper;

import java.util.Arrays;
import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.dal.data.repository.IRetentionDividendTypeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.helper.ErrorHelper;
import org.megapractical.invoicing.webapp.exposition.retention.payload.RetentionComplementResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRetention.RetDiv;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.springframework.stereotype.Component;

import lombok.val;

@Component
public class RetentionComplementDivHelper {
	
	private final ErrorHelper errorHelper;
	private final IRetentionDividendTypeJpaRepository iRetentionDividendTypeJpaRepository;
	
	public RetentionComplementDivHelper(ErrorHelper errorHelper, IRetentionDividendTypeJpaRepository iRetentionDividendTypeJpaRepository) {
		this.iRetentionDividendTypeJpaRepository = iRetentionDividendTypeJpaRepository;
		this.errorHelper = errorHelper;
	}

	public RetentionComplementResponse populate(RetentionData retentionData, String retentionKey, final ApiRetention apiRetention, final List<JError> errors) {
		val retDiv = retentionData.getRetDiv();
		if (!UValidator.isNullOrEmpty(retDiv)) {
			// Tipo dividendo
			populateDividendType(retDiv, apiRetention, errors);
			// Importe territorio nacional
			populateDividendAmountNat(retDiv, apiRetention, errors);
			// Importe extranjero
			populateDividendAmountExt(retDiv, apiRetention, errors);
			// Monto sobre dividendo extranjero
			populateDividendAmountWithheldExt(retDiv, apiRetention, errors);
			// Tipo sociedad
			populateDividendSocieties(retDiv, apiRetention, errors);
			// Monto ISR nacional
			populateDividendIsr(retDiv, apiRetention, errors);
			// Monto acumulable nacional
			populateDividendCumulativeNat(retDiv, apiRetention, errors);
			// Monto acumulable extranjero
			populateDividendCumulativeExt(retDiv, apiRetention, errors);
			// Remaining
			populateDividendRemaining(retDiv, apiRetention, errors);
			
			return RetentionComplementResponse.ok();
		} else if (retentionKey.equalsIgnoreCase("14")) {
			return RetentionComplementResponse.error();
		}
		return RetentionComplementResponse.empty();
	}
	
	private void populateDividendType(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val field = "dividend-cve-tip-div-outil";
		String errorCode = null;
		
		val dividendType = UBase64.base64Decode(retDiv.getCveTipDivOUtil());
		if (!UValidator.isNullOrEmpty(dividendType)) {
			val dividenTypeEntity = iRetentionDividendTypeJpaRepository.findByCodigoAndEliminadoFalse(dividendType);
			if (!UValidator.isNullOrEmpty(dividenTypeEntity)) {
				apiRetention.setCveTipDivOUtil(dividendType);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
		} else {
			errorCode = JError.REQUIRED_FIELD;
		}
		errorHelper.evaluateError(field, errorCode, errors);
	}
	
	private void populateDividendAmountNat(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val field = "dividend-mont-isr-acred-ret-mexico";
		String errorCode = null;
		
		val dividendAmountNat = UBase64.base64Decode(retDiv.getMontISRAcredRetMexico());
		if (!UValidator.isNullOrEmpty(dividendAmountNat)) {
			val bgValidate = UValue.bigDecimalStrict(dividendAmountNat);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setMontISRAcredRetMexico(dividendAmountNat);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
		} else {
			errorCode = JError.REQUIRED_FIELD;
		}
		errorHelper.evaluateError(field, errorCode, errors);
	}
	
	private void populateDividendAmountExt(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val field = "dividend-mont-isr-acred-ret-extranjero";
		String errorCode = null;
		
		val dividendAmountExt = UBase64.base64Decode(retDiv.getMontISRAcredRetExtranjero());
		if (!UValidator.isNullOrEmpty(dividendAmountExt)) {
			val bgValidate = UValue.bigDecimalStrict(dividendAmountExt);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setMontISRAcredRetExtranjero(dividendAmountExt);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
		} else {
			errorCode = JError.REQUIRED_FIELD;
		}
		errorHelper.evaluateError(field, errorCode, errors);
	}
	
	private void populateDividendAmountWithheldExt(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val dividendAmountWithheldExt = UBase64.base64Decode(retDiv.getMontRetExtDivExt());
		if (!UValidator.isNullOrEmpty(dividendAmountWithheldExt)) {
			val field = "dividend-mont-ret-ext-div-ext";
			String errorCode = null;
			
			val bgValidate = UValue.bigDecimalStrict(dividendAmountWithheldExt);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setMontRetExtDivExt(dividendAmountWithheldExt);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
			errorHelper.evaluateError(field, errorCode, errors);
		}
	}
	
	private void populateDividendSocieties(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val field = "dividend-tipo-soc-distr-div";
		String errorCode = null;
		
		val dividendSocieties = UBase64.base64Decode(retDiv.getTipoSocDistrDiv());
		if (!UValidator.isNullOrEmpty(dividendSocieties)) {
			val societyValidate = Arrays.asList("Sociedad Nacional", "Sociedad Extranjera")
										.stream()
										.filter(dividendSocieties::equalsIgnoreCase)
										.findAny()
										.orElse(null);
			
			if (!UValidator.isNullOrEmpty(societyValidate)) {
				apiRetention.setTipoSocDistrDiv(dividendSocieties);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
		} else {
			errorCode = JError.REQUIRED_FIELD;
		}
		errorHelper.evaluateError(field, errorCode, errors);
	}
	
	private void populateDividendIsr(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val dividendIsr = UBase64.base64Decode(retDiv.getMontISRAcredNal());
		if (!UValidator.isNullOrEmpty(dividendIsr)) {
			val field = "dividend-mont-isr-acred-nal";
			String errorCode = null;
			
			val bgValidate = UValue.bigDecimalStrict(dividendIsr);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setMontISRAcredNal(dividendIsr);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
			errorHelper.evaluateError(field, errorCode, errors);
		}
	}
	
	private void populateDividendCumulativeNat(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val dividendCumulativeNat = UBase64.base64Decode(retDiv.getMontDivAcumNal());
		if (!UValidator.isNullOrEmpty(dividendCumulativeNat)) {
			val field = "dividend-mont-div-acum-nal";
			String errorCode = null;
			
			val bgValidate = UValue.bigDecimalStrict(dividendCumulativeNat);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setMontDivAcumNal(dividendCumulativeNat);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
			errorHelper.evaluateError(field, errorCode, errors);
		}
	}
	
	private void populateDividendCumulativeExt(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val dividendCumulativeExt = UBase64.base64Decode(retDiv.getMontDivAcumExt());
		if (!UValidator.isNullOrEmpty(dividendCumulativeExt)) {
			val field = "dividend-mont-div-acum-ext";
			String errorCode = null;
			
			val bgValidate = UValue.bigDecimalStrict(dividendCumulativeExt);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setMontDivAcumExt(dividendCumulativeExt);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
			errorHelper.evaluateError(field, errorCode, errors);
		}
	}
	
	private void populateDividendRemaining(RetDiv retDiv, final ApiRetention apiRetention, final List<JError> errors) {
		val dividendRemaining = UBase64.base64Decode(retDiv.getProporcionRem());
		if (!UValidator.isNullOrEmpty(dividendRemaining)) {
			val field = "dividend-proporcion-rem";
			String errorCode = null;
			
			val bgValidate = UValue.bigDecimalStrict(dividendRemaining);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setProporcionRem(dividendRemaining);
			} else {
				errorCode = JError.INVALID_VALUE;
			}
			errorHelper.evaluateError(field, errorCode, errors);
		}
	}
	
}