package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import org.megapractical.invoicing.webapp.json.JResponse;

public interface PaymentRelatedDocumentService {
	
	JResponse validate(String relatedDocumentData, String paymentCurrency, String amountStr);
	
}