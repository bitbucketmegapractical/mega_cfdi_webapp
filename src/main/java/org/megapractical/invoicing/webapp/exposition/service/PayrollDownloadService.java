package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.webapp.exposition.cfdi.payload.FileDownload;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.FileDownload.FileDownloadData;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class PayrollDownloadService {

	private final PayrollService payrollService;
	private final FileDownloadService fileDownloadService;
	
	public FileDownload download(String uuid, String fileType) {
		try {
			val payrollEntity = payrollService.findByUuid(uuid);
			if (payrollEntity != null) {
				val downloadData = FileDownloadData
						.builder()
						.fileType(fileType)
						.storageType(payrollEntity.getStorage())
						.xmlPath(payrollEntity.getRutaXml())
						.pdfPath(payrollEntity.getRutaPdf())
						.xmlFileNameFromS3(payrollEntity.getXmlFileNameFromS3())
						.pdfFileNameFromS3(payrollEntity.getPdfFileNameFromS3())
						.build();
				return fileDownloadService.download(downloadData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}