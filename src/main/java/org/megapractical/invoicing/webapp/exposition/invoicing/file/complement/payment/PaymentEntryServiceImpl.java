package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoTramaEntity;
import org.megapractical.invoicing.dal.data.repository.IPaymentEntryJpaRepository;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentEntryServiceImpl implements PaymentEntryService {

	private final IPaymentEntryJpaRepository iPaymentEntryJpaRepository;

	public PaymentEntryServiceImpl(IPaymentEntryJpaRepository iPaymentEntryJpaRepository) {
		this.iPaymentEntryJpaRepository = iPaymentEntryJpaRepository;
	}

	@Override
	public ComplementoPagoTramaEntity findByName(String entryName) {
		return iPaymentEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(entryName);
	}

	@Override
	public boolean isPrincipal(String entryName) {
		val entry = findByName(entryName);
		return entry != null && entry.isPrincipal();
	}

}