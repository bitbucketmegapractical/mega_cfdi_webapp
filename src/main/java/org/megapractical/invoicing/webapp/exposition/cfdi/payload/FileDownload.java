package org.megapractical.invoicing.webapp.exposition.cfdi.payload;

import org.megapractical.invoicing.dal.bean.datatype.StorageType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileDownload {
	private byte[] fileByte;
	private String fileName;
	private String contentType;
	
	public String getHeader() {
		return "attachment; filename=\"" + fileName +"\"";
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class FileDownloadData {
		private String fileType;
		private StorageType storageType;
		private String xmlPath;
		private String pdfPath;
		private String xmlFileNameFromS3;
		private String pdfFileNameFromS3;
	}
}