package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.webapp.json.JResponse;

public interface StampErrorService {
	
	JResponse unexpectedError();
	
	JResponse stampError(StampResponse stampResponse);
	
	JResponse swStampError(StampResponse stampResponse);
	
}