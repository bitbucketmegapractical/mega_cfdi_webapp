package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.common.ApiVoucherTaxes;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MetodoPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.PostalCodeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.PostalCodeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherSerieSheetResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.relatedcfdi.RelatedCfdiValidator;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRelatedCfdi.JRelatedCfdiRequest;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.json.JVoucher.JVoucherResponse;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherValidatorImpl implements VoucherValidator {
	
	@Autowired
	private VoucherSerieSheetValidator voucherSerieSheetValidator;
	
	@Autowired
	private VoucherTypeValidator voucherTypeValidator;
	
	@Autowired
	private PostalCodeValidator postalCodeValidator;
	
	@Autowired
	private VoucherPaymentValidator voucherPaymentValidator;
	
	@Autowired
	private VoucherCurrencyValidator voucherCurrencyValidator;
	
	@Autowired
	private VoucherChangeTypeValidator voucherChangeTypeValidator;
	
	@Autowired
	private VoucherExportationValidator voucherExportationValidator;
	
	@Autowired
	private RelatedCfdiValidator relatedCfdiValidator;
	
	@Autowired
	private VoucherTotalsValidator voucherTotalsValidator;
	
	@Override
	public JVoucherResponse getVoucher(String voucherData, String relatedCfdiCheck, ContribuyenteEntity taxpayer, List<JError> errors) {
		val voucher = GsonClient.deserialize(voucherData, JVoucher.class);
		val apiVoucher = new ApiVoucher();
		
		val serieSheetResponse = voucherSerieSheetValidator.getSerieSheetResponse(voucher, taxpayer, errors);
		val serieSheetError = serieSheetResponse.isError();
		
		val voucherTypeResponse = voucherTypeValidator.getVoucherType(voucher, errors);
		val voucherType = voucherTypeResponse.getVoucherType();
		val voucherTypeError = voucherTypeResponse.isError();
		
		val expeditionPlaceResponse = getExpeditionPlace(voucher, errors);
		val expeditionPlace = expeditionPlaceResponse.getPostalCode();
		val expeditionPlaceError = expeditionPlaceResponse.isError();
		
		val paymentWayResponse = voucherPaymentValidator.getPaymentWay(voucher, voucherType, errors);
		val paymentWay = paymentWayResponse.getPaymentWay();
		val paymentWayError = paymentWayResponse.isError();
		
		val paymentMethodResponse = voucherPaymentValidator.getPaymentMethod(voucher, voucherType, errors);
		val paymentMethod = paymentMethodResponse.getPaymentMethod();
		val paymentMethodError = paymentMethodResponse.isError();
		
		val currencyResponse = voucherCurrencyValidator.getCurrency(voucher, errors);
		val currency = currencyResponse.getCurrency();
		val currencyError = currencyResponse.isError();
		
		val chageTypeResponse = voucherChangeTypeValidator.getChangeType(voucher, currency, errors);
		val changeType = chageTypeResponse.getChangeType();
		val chageTypeError = chageTypeResponse.isError();
		
		val exportationResponse = voucherExportationValidator.getExportation(voucher, "exportation", errors);
		val exportation = exportationResponse.getExportation();
		val exportationError = exportationResponse.isError();
		
		val relatedCfdiRequest = new JRelatedCfdiRequest(relatedCfdiCheck, voucher.getRelatedCFDI());
		val relatedCfdiResponse = relatedCfdiValidator.getRelatedCfdi(relatedCfdiRequest, errors);
		val relatedCfdiError = relatedCfdiResponse.isError();
		
		val voucherTotalsError = voucherTotalsValidator.validateTotals(voucher.getSubTotal(), voucher.getTotal(), errors);
		
		val voucherErrors = Arrays.asList(serieSheetError, voucherTypeError, expeditionPlaceError, 
										  paymentWayError, paymentMethodError, currencyError, chageTypeError,
										  exportationError, relatedCfdiError, voucherTotalsError);
		val hasError = hasVoucherError(voucherErrors);
		if (!hasError) {
			val confirmationCode = UBase64.base64Decode(voucher.getConfirmationCode());
			val voucherPaymentConditions = UBase64.base64Decode(voucher.getPaymentConditions());
			val voucherTotalsSubtotal = UValue.bigDecimalStrict(UBase64.base64Decode(voucher.getSubTotal()));
			val voucherTotalsDiscount = UValue.bigDecimalStrict(UBase64.base64Decode(voucher.getDiscount()));
			val voucherTotalsTaxesTransferred = UValue.bigDecimalStrict(UBase64.base64Decode(voucher.getTotalTaxesTransferred()));
			val voucherTotalsTaxesWithheld = UValue.bigDecimalStrict(UBase64.base64Decode(voucher.getTotalTaxesWithheld()));
			val voucherTotalsTotal = UValue.bigDecimalStrict(UBase64.base64Decode(voucher.getTotal()));
			
			apiVoucher.setVersion(VoucherUtils.VOUCHER_VERSION_DEFAULT);
			apiVoucher.setDateTimeExpedition(UDate.formattedDate());
			apiVoucher.setSubTotal(UValue.moneyFormat(voucherTotalsSubtotal.toString()));
			apiVoucher.setTotal(UValue.moneyFormat(voucherTotalsTotal.toString()));
			apiVoucher.setPaymentConditions(UValue.stringValue(voucherPaymentConditions));
			apiVoucher.setConfirmation(UValue.stringValue(confirmationCode));
			apiVoucher.setRelatedCfdi(relatedCfdiResponse.getApiRelatedCfdi());
			apiVoucher.setExportation(exportation.getCodigo());
			
			setVoucherType(voucherType, apiVoucher);
			setCurrency(currency, apiVoucher);
			setExpeditionPlace(expeditionPlace, apiVoucher);
			setSerieSheet(serieSheetResponse, apiVoucher);
			setPaymentWay(paymentWay, apiVoucher);
			setPaymentMethod(paymentMethod, apiVoucher);
			setChangeType(changeType, apiVoucher);
			setTotalDiscount(voucherTotalsDiscount, apiVoucher);
			setTaxes(voucherTotalsTaxesTransferred, voucherTotalsTaxesWithheld, apiVoucher);
			
			return JVoucherResponse
					.builder()
					.apiVoucher(apiVoucher)
					.serieSheet(serieSheetResponse.getSerieSheet())
					.voucherType(voucherType)
					.expeditionPlace(expeditionPlace)
					.paymentWay(paymentWay)
					.paymentMethod(paymentMethod)
					.currency(currency)
					.error(hasError)
					.build();
		}
		return JVoucherResponse.empty();
	}

	private final boolean hasVoucherError(List<Boolean> errors) {
		return errors.stream().anyMatch(Boolean::booleanValue);
	}
	
	private void setVoucherType(final TipoComprobanteEntity voucherType, ApiVoucher apiVoucher) {
		if (voucherType != null) {
			apiVoucher.setVoucherType(UCatalog.tipoDeComprobante(voucherType.getCodigo()));
		}
	}
	
	private void setCurrency(final MonedaEntity currency, final ApiVoucher apiVoucher) {
		if (currency != null) {
			apiVoucher.setCurrency(UCatalog.moneda(currency.getCodigo()));
		}
	}
	
	private PostalCodeResponse getExpeditionPlace(JVoucher voucher, List<JError> errors) {
		var postalCode = UBase64.base64Decode(voucher.getPostalCode());
		val validatorResponse = postalCodeValidator.validate(postalCode, "postal-code");
		val postalCodeEntity = validatorResponse.getPostalCode();
		val error = validatorResponse.getError();
		
		var expeditionPlaceError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			expeditionPlaceError = true;
		}
		
		return new PostalCodeResponse(postalCodeEntity, expeditionPlaceError);
	}
	
	private void setExpeditionPlace(final CodigoPostalEntity expeditionPlace, final ApiVoucher apiVoucher) {
		if (expeditionPlace != null) {
			apiVoucher.setPostalCode(UValue.stringValue(expeditionPlace.getCodigo()));
		}
	}
	
	private void setSerieSheet(final VoucherSerieSheetResponse serieSheetResponse, final ApiVoucher apiVoucher) {
		val serieSheet = serieSheetResponse.getSerieSheet();
		var serie = serieSheetResponse.getSerie();
		val sheetStr = serieSheetResponse.getSheetStr();
		val allowIndicateSerieSheet = serieSheetResponse.isAllowIndicateSerieSheet();
		val allowIndicateSheet = serieSheetResponse.isAllowIndicateSheet();

		if (!UValidator.isNullOrEmpty(serieSheet)) {
			serie = serieSheet.getSerie().toUpperCase();
		}

		apiVoucher.setSerie(UValue.stringValue(serie));
		apiVoucher.setFolio(sheetStr);

		if (allowIndicateSerieSheet) {
			apiVoucher.setIndicateSerie(Boolean.TRUE);
			apiVoucher.setIndicateSheet(Boolean.TRUE);
		} else if (allowIndicateSheet) {
			apiVoucher.setIndicateSerie(Boolean.FALSE);
			apiVoucher.setIndicateSheet(Boolean.TRUE);
		} else {
			if (!UValidator.isNullOrEmpty(serieSheet)) {
				apiVoucher.setIndicateSerie(Boolean.FALSE);
				apiVoucher.setIndicateSheet(Boolean.FALSE);
			} else if (!UValidator.isNullOrEmpty(serie) && !UValidator.isNullOrEmpty(sheetStr)) {
				apiVoucher.setIndicateSerie(Boolean.TRUE);
				apiVoucher.setIndicateSheet(Boolean.TRUE);
			}
		}
	}
	
	private void setPaymentWay(final FormaPagoEntity paymentWay, final ApiVoucher apiVoucher) {
		if (paymentWay != null) {
			apiVoucher.setPaymentWay(UCatalog.formaPago(paymentWay.getCodigo()));
		}
	}

	private void setPaymentMethod(final MetodoPagoEntity paymentMethod, final ApiVoucher apiVoucher) {
		if (paymentMethod != null) {
			apiVoucher.setPaymentMethod(UCatalog.metodoPago(paymentMethod.getCodigo()));
		}
	}

	private void setChangeType(final BigDecimal changeType, final ApiVoucher apiVoucher) {
		if (changeType != null) {
			apiVoucher.setChangeType(
					UValue.bigDecimalStrict(changeType.toString(), UTools.decimals(changeType.toString())));
		}
	}

	private void setTotalDiscount(final BigDecimal voucherTotalsDiscount, final ApiVoucher apiVoucher) {
		if (voucherTotalsDiscount != null) {
			apiVoucher.setDiscount(UValue.bigDecimal(voucherTotalsDiscount.toString(),
					UTools.decimals(voucherTotalsDiscount.toString())));
		}
	}
	
	private void setTaxes(final BigDecimal voucherTotalsTaxesTransferred, final BigDecimal voucherTotalsTaxesWithheld,
			final ApiVoucher apiVoucher) {
		val voucherTax = new ApiVoucherTaxes();
		if (voucherTotalsTaxesTransferred != null) {
			voucherTax.setTotalTaxTransferred(UValue.bigDecimalStrict(voucherTotalsTaxesTransferred.toString(),
					UTools.decimals(voucherTotalsTaxesTransferred.toString())));
		}

		if (voucherTotalsTaxesWithheld != null) {
			voucherTax.setTotalTaxWithheld(UValue.bigDecimalStrict(voucherTotalsTaxesWithheld.toString(),
					UTools.decimals(voucherTotalsTaxesWithheld.toString())));
		}
		apiVoucher.setTaxes(voucherTax);
	}

}