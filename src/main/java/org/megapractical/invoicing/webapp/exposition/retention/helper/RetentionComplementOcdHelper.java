package org.megapractical.invoicing.webapp.exposition.retention.helper;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.webapp.exposition.retention.payload.RetentionComplementResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRetention.RetOcd;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import lombok.val;
import lombok.var;

@Component
public class RetentionComplementOcdHelper {

	private final UserTools userTools;

	public RetentionComplementOcdHelper(UserTools userTools) {
		this.userTools = userTools;
	}

	public RetentionComplementResponse populate(RetentionData retentionData, String retentionKey,
			final ApiRetention apiRetention, final List<JError> errors) {
		val retOcd = retentionData.getRetOcd();
		if (!UValidator.isNullOrEmpty(retOcd)) {
			// Ganancia acumulable
			populateOcdProfitAmount(retOcd, apiRetention, errors);
			// Pérdida deducible
			populateOcdDeductibleWaste(retOcd, apiRetention, errors);

			return RetentionComplementResponse.ok();
		} else if (retentionKey.equalsIgnoreCase("24")) {
			return RetentionComplementResponse.error();
		}
		return RetentionComplementResponse.empty();
	}
	
	private void populateOcdProfitAmount(RetOcd retOcd, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;

		val ocdProfitAmount = UBase64.base64Decode(retOcd.getProfitAmount());
		if (!UValidator.isNullOrEmpty(ocdProfitAmount)) {
			val bgValidate = UValue.bigDecimalStrict(ocdProfitAmount);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setProfitAmount(ocdProfitAmount);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}

		if (errorMarked) {
			error.setField("ocd-profit-amount");
			errors.add(error);
		}
	}

	private void populateOcdDeductibleWaste(RetOcd retOcd, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;

		val ocdDeductibleWaste = UBase64.base64Decode(retOcd.getDeductibleWaste());
		if (!UValidator.isNullOrEmpty(ocdDeductibleWaste)) {
			val bgValidate = UValue.bigDecimalStrict(ocdDeductibleWaste);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setDeductibleWaste(ocdDeductibleWaste);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}

		if (errorMarked) {
			error.setField("ocd-deductible-waste");
			errors.add(error);
		}
	}

}