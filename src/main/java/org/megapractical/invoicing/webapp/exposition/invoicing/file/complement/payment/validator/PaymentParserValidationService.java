package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.validator;

import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiPaymentError;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoFicheroEntity;

public interface PaymentParserValidationService {
	
	ComplementoPagoFicheroEntity getPaymentDataFile();
	
	Integer getLength(String[] entry);
	
	boolean entryValidate(String[] entry);
	
	boolean isEmpty(String[] entry);
	
	List<ApiPaymentError> validateField(String[] entry);
	
}