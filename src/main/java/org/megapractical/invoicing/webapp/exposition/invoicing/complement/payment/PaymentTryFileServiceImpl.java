package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiEmitter;
import org.megapractical.invoicing.api.wrapper.ApiFileDetail;
import org.megapractical.invoicing.api.wrapper.ApiPaymentStamp;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.PaymentFileAsyncService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.converter.CfdiFileConverter;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.converter.InvoiceFileStatsConverter;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.payload.UploadFilePath;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.service.InvoiceFileService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.service.InvoiceFileStatsService;
import org.megapractical.invoicing.webapp.exposition.service.CfdiService;
import org.megapractical.invoicing.webapp.exposition.service.SessionService;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.exposition.service.nomenclator.FileStatusService;
import org.megapractical.invoicing.webapp.exposition.service.nomenclator.FileTypeService;
import org.megapractical.invoicing.webapp.exposition.service.taxpayer.TaxpayerTaxRegimeService;
import org.megapractical.invoicing.webapp.json.JCfdiFile;
import org.megapractical.invoicing.webapp.json.JCfdiFile.CfdiFile;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.FilePathUtils;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.megapractical.invoicing.webapp.util.PaymentUtils;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class PaymentTryFileServiceImpl implements PaymentTryFileService {

	@Autowired
	private AppSettings appSettings;
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private InvoiceFileService invoiceFileService;
	
	@Autowired
	private InvoiceFileStatsService invoiceFileStatsService;
	
	@Autowired
	private CfdiService cfdiService;
	
	@Autowired	
	private StampControlService stampControlService;
	
	@Autowired
	private FileTypeService fileTypeService;
	
	@Autowired
	private FileStatusService fileStatusService;
	
	@Autowired
	private PaymentTryService paymentTryService;
	
	@Autowired
	private SessionService sessionService;
	
	@Autowired
	private TaxpayerTaxRegimeService taxpayerTaxRegimeService;
	
	@Autowired
	private PaymentFileAsyncService paymentFileAsyncService;
	
	private static final Integer INITIAL_PAGE = 0;
	private static final String SORT_PROPERTY = "id";
	
	@Override
	public JCfdiFile paymentTry(ContribuyenteEntity taxpayer, String pageStr, String searchBy) {
		val cfdiFile = loadGeneratedFiles(taxpayer, pageStr, searchBy);
		cfdiFile.setProcessingFiles(getProcessingFiles(taxpayer));
		cfdiFile.setQueuedFiles(getQueuedFiles(taxpayer));
		return cfdiFile;
	}
	
	@Override
	public JCfdiFile loadGeneratedFiles(ContribuyenteEntity taxpayer, String pageStr, String searchBy) {
		var page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
		page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
		
		val sort = Sort.by(Sort.Order.desc(SORT_PROPERTY));
		val pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);

		val paymentFilePage = invoiceFileService.getFiles(taxpayer, searchBy, FileStatus.GENERATED,
				PaymentUtils.PAYMENT_FILE_TXT_CODE, pageRequest);
		val pagination = JPagination.buildPaginaton(paymentFilePage);
		val generatedFiles = getGeneratedFiles(paymentFilePage);
		
		val notification = new JNotification();
		var searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
		if (!UValidator.isNullOrEmpty(searchBy)) {
			notification.setSearch(Boolean.TRUE);
			searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
		}

		if (generatedFiles.isEmpty()) {
			notification.setSearchResultMessage(searchResultEmpty);
			notification.setPanelMessage(Boolean.TRUE);
			notification.setDismiss(Boolean.FALSE);
			notification.setCssStyleClass(CssStyleClass.INFO.value());
		}
		
		val response = new JResponse();
		response.setSuccess(Boolean.TRUE);
		response.setNotification(notification);
		
		return JCfdiFile
				.builder()
				.pagination(pagination)
				.generatedFiles(generatedFiles)
				.response(response)
				.build();
	}
	
	@Override
	public JCfdiFile uploadFile(ContribuyenteEntity taxpayer, ContribuyenteCertificadoEntity certificate, MultipartFile multipartFile, boolean sendMail) {
		val response = new JResponse();
		val errors = new ArrayList<JError>();
		
		if (stampControlService.stampStatus(taxpayer) || userTools.canUploadPayments()) {
			val fileDetails = loadFileDetails(taxpayer, multipartFile, errors);
			if (!fileDetails.isHasError()) {
				val cfdiRepository = appSettings.getPropertyValue("cfdi.repository");
				val uploadFilePath = FilePathUtils.uploadPath(taxpayer.getRfc(), fileDetails, cfdiRepository);
				try {
					UFile.writeFile(UFile.fileToByte(fileDetails.getFile()), uploadFilePath.getUploadPath());
					val invoiceFile = populateInvoiceFile(taxpayer, fileDetails, uploadFilePath.getDbPath(), sendMail);
					if (!isAnotherFileProcessing(taxpayer)) {
						processFile(taxpayer, certificate, invoiceFile, uploadFilePath);
					} else {
						invoiceFileService.save(invoiceFile);
					}
					successResponse(response);
				} catch (IOException e) {
					writeFileError(response, errors);
				}
			}
		} else {
			uploadLimitError(response);
		}
		return JCfdiFile.builder().response(response).build();
	}

	@Override
	public byte[] downloadFile(ContribuyenteEntity taxpayer, String fileName, String fieType,
			HttpServletResponse response) {
		val repositoryPath = appSettings.getPropertyValue("cfdi.repository");
		val paymentFile = invoiceFileService.findByTaxpayerAndName(taxpayer, fileName);
		if (!UValidator.isNullOrEmpty(paymentFile)) {
			var absolutePath = repositoryPath + File.separator;
			if (fieType.equalsIgnoreCase("compacted")) {
				absolutePath = absolutePath + paymentFile.getRutaCompactado();
			} else if (fieType.equalsIgnoreCase("consolidated")) {
				absolutePath = absolutePath + paymentFile.getRutaConsolidado();
			} else if (fieType.equalsIgnoreCase("error")) {
				absolutePath = absolutePath + paymentFile.getRutaError();
			}

			val file = new File(absolutePath);
			byte[] fileByte;
			try {
				fileByte = UFile.fileToByte(file);
				
				response.setContentType("application/xml");
				response.setContentLength(fileByte.length);
				response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

				return fileByte;
			} catch (IOException e) {
				return new byte[0];
			}
		}
		return new byte[0];
	}
	
	@Override
	public boolean exists(Long id) {
		return invoiceFileService.findOne(id, FileStatus.QUEUED) != null;
	}
	
	@Override
	public void deleteQueuedFile(Long id) {
		invoiceFileService.delete(id);
	}
	
	private List<CfdiFile> getGeneratedFiles(Page<ArchivoFacturaEntity> paymentFilePage) {
		return paymentFilePage.stream()
							  .map(CfdiFileConverter::convert)
							  .filter(Objects::nonNull)
							  .collect(Collectors.toList());
	}
	
	private List<CfdiFile> getProcessingFiles(ContribuyenteEntity taxpayer) {
		val processingFiles = new ArrayList<CfdiFile>();
		
		val files = invoiceFileService.getFiles(taxpayer, FileStatus.PROCESSING, PaymentUtils.PAYMENT_FILE_TXT_CODE);
		files.forEach(item -> {
			val cfdiFile = new CfdiFile();
			cfdiFile.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
			cfdiFile.setName(UBase64.base64Encode(item.getNombre()));
			
			val fileStats = invoiceFileStatsService.findByInvoiceFile(item);
			cfdiFile.setCfdiFileStatistics(InvoiceFileStatsConverter.convert(fileStats));
			stimatedTime(item, fileStats, cfdiFile);
			
			processingFiles.add(cfdiFile);
		});
		
		return processingFiles;
	}
	
	private List<CfdiFile> getQueuedFiles(ContribuyenteEntity taxpayer) {
		val queuedFiles = new ArrayList<CfdiFile>();

		val files = invoiceFileService.getFiles(taxpayer, FileStatus.QUEUED, PaymentUtils.PAYMENT_FILE_TXT_CODE);
		files.forEach(item -> {
			val cfdiFile = new CfdiFile();
			cfdiFile.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
			cfdiFile.setCfdiType(UBase64.base64Encode("Pagos"));
			cfdiFile.setName(UBase64.base64Encode(item.getNombre()));
			cfdiFile.setUploadDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaCarga())));

			if (!UValidator.isNullOrEmpty(item.getTimbresDisponibles())) {
				cfdiFile.setStampAvailable(item.getTimbresDisponibles());
			}

			queuedFiles.add(cfdiFile);
		});

		return queuedFiles;
	}
	
	private void stimatedTime(ArchivoFacturaEntity invoiceFile, ArchivoFacturaEstadisticaEntity stats,
			final CfdiFile cfdiFile) {
		if (stats != null && stats.getFacturas() != null && stats.getFacturasTimbradas() != null) {
			val totalInvoice = stats.getFacturas();
			val totalInvoiceStamped = stats.getFacturasTimbradas();
			val totalInvoiceError = stats.getFacturasError() != null ? stats.getFacturasError() : 0;
			val remainingInvoice = totalInvoice - (totalInvoiceStamped + totalInvoiceError);

			val cfdi = cfdiService.findByInvoiceFile(invoiceFile);
			if (cfdi != null) {
				val voucherExpeditionDateTime = cfdi.getFechaHoraExpedicion();
				val expeditionDateTime = UDate.formattedDate(voucherExpeditionDateTime);

				val voucherStampedDateTime = cfdi.getFechaHoraTimbrado();
				val stampedDateTime = UDate.formattedDate(voucherStampedDateTime);

				determineStimatedTime(cfdiFile, remainingInvoice, expeditionDateTime, stampedDateTime);
			}
		}
	}

	private void determineStimatedTime(final CfdiFile cfdiFile, final int remainingInvoice,
			final Date expeditionDateTime, final Date stampedDateTime) {
		Integer stimatedTime = null;
		var isMinute = true;
		try {
			stimatedTime = UDateTime.durationBetweenDatesInMinutes(expeditionDateTime, stampedDateTime);
			if (stimatedTime == 0) {
				stimatedTime = UDateTime.durationBetweenDatesInMilliseconds(expeditionDateTime, stampedDateTime);
				stimatedTime = stimatedTime / 1000;
				isMinute = false;
			}
		} catch (Exception e) {
			stimatedTime = null;
		}

		if (stimatedTime != null) {
			determineStimatedTime(cfdiFile, remainingInvoice, stimatedTime, isMinute);
		}
	}

	private void determineStimatedTime(final CfdiFile cfdiFile, final int remainingInvoice, Integer stimatedTime,
			boolean isMinute) {
		String result = null;
		Integer minutes = null;

		if (isMinute) {
			minutes = remainingInvoice * stimatedTime;
			if (minutes == 0) {
				result = "Finalizado";
			}
		} else {
			val seconds = remainingInvoice * stimatedTime;
			if (seconds >= 60) {
				minutes = remainingInvoice * stimatedTime / 60;
			} else {
				if (seconds == 0) {
					result = "Finalizado";
				} else {
					result = seconds + (seconds == 1 ? " segundo" : " segundos");
				}
			}
		}

		result = determineStimatedTime(result, minutes);
		cfdiFile.setStimatedTime(UBase64.base64Encode(result));
	}

	private String determineStimatedTime(String result, Integer minutes) {
		if (UValidator.isNullOrEmpty(result)) {
			if (minutes == 1) {
				result = minutes + " minuto";
			} else if (minutes > 1 && minutes <= 59) {
				result = minutes + " minutos";
			} else if (minutes == 60) {
				result = "1 hora";
			} else if (minutes > 60) {
				Integer hours = minutes / 60;
				minutes = minutes % 60;

				if (hours == 1) {
					result = "1 hora";
				} else if (hours > 1) {
					result = hours + " horas";
				}

				if (minutes == 1) {
					result = result + " " + minutes + " minuto";
				} else {
					result = result + " " + minutes + " minutos";
				}
			}
		}
		return result;
	}
	
	private ApiFileDetail loadFileDetails(ContribuyenteEntity taxpayer, MultipartFile multipartFile,
			final List<JError> errors) {
		ApiFileDetail fileDetails = null;
		val error = new JError();
		var errorMarked = false;

		if (multipartFile.isEmpty()) {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		} else {
			fileDetails = UFile.fileDetails(multipartFile);
			
			val fileName = fileDetails.getFileName().toUpperCase();
			if (!UValidator.isNullOrEmpty(fileName)) {
				val invoiceFile = invoiceFileService.findByTaxpayerAndName(taxpayer, fileName);
				if (!UValidator.isNullOrEmpty(invoiceFile)) {
					error.setMessage(UProperties.getMessage("ERR0161", userTools.getLocale()));
					errorMarked = true;
				} else {
					fileDetails.setFileName(fileName);
				}
			} else {
				error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
				errorMarked = true;
			}
			
			val fileExt = fileDetails.getFileExt();
			if (fileExt == null || !fileExt.equalsIgnoreCase("txt")) {
				error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
				errorMarked = true;
			}
		}

		if (errorMarked) {
			error.setField("complement-file");
			errors.add(error);
			return ApiFileDetail.withError();
		}
		return fileDetails;
	}
	
	private ArchivoFacturaEntity populateInvoiceFile(ContribuyenteEntity taxpayer, ApiFileDetail fileDetails, String path, boolean sendMail) {
		val fileName = fileDetails.getFileName();
		val fileExt = fileDetails.getFileExt();
		val dbPath = path + File.separator + fileName + "." + fileExt;
		val fileTypeCode = fileExt.equalsIgnoreCase("txt") ? PaymentUtils.PAYMENT_FILE_TXT_CODE : null;
		val fileType = fileTypeService.findByCode(fileTypeCode);
		val fileStatus = fileStatusService.findByCode(FileStatus.QUEUED);
		
		val invoiceFile = new ArchivoFacturaEntity();
		invoiceFile.setContribuyente(taxpayer); 
		invoiceFile.setTipoArchivo(fileType);
		invoiceFile.setEstadoArchivo(fileStatus);
		invoiceFile.setNombre(fileName);
		invoiceFile.setRuta(dbPath);
		invoiceFile.setFechaCarga(new Date());
		invoiceFile.setEnviarCorreoCliente(sendMail);
		invoiceFile.setEjercicio(UValue.integerString(UDateTime.yearOfDate(new Date())));
		invoiceFile.setEliminado(Boolean.FALSE);
		
		return invoiceFile;
	}
	
	private boolean isAnotherFileProcessing(ContribuyenteEntity taxpayer) {
		val processingFiles = invoiceFileService.findByTaxpayerAndStatus(taxpayer, FileStatus.PROCESSING);
		return !processingFiles.isEmpty();
	}
	
	public void processFile(ContribuyenteEntity taxpayer, ContribuyenteCertificadoEntity certificate,
			ArchivoFacturaEntity invoiceFile, UploadFilePath uploadFilePath) {
		val apiEmitter = populateEmitter(taxpayer, certificate);
		val paymentStamp = populatePaymentStamp(taxpayer, certificate, invoiceFile, uploadFilePath, apiEmitter);
		// Async processing
		paymentFileAsyncService.processFileAsync(paymentStamp);
	}
	
	private ApiEmitter populateEmitter(ContribuyenteEntity taxpayer, ContribuyenteCertificadoEntity certificate) {
		val apiEmitter = new ApiEmitter();
		apiEmitter.setNombreRazonSocial(taxpayer.getNombreRazonSocial());
		apiEmitter.setRfc(taxpayer.getRfc());
		apiEmitter.setRegimenFiscal(taxpayerTaxRegimeService.taxRegime(taxpayer).getRegimenFiscal());
		apiEmitter.setCertificado(certificate.getCertificado());
		apiEmitter.setLlavePrivada(certificate.getLlavePrivada());
		apiEmitter.setPasswd(UBase64.base64Decode(certificate.getClavePrivada()));
		return apiEmitter;
	}
	
	private ApiPaymentStamp populatePaymentStamp(ContribuyenteEntity taxpayer,
			ContribuyenteCertificadoEntity certificate, ArchivoFacturaEntity invoiceFile, UploadFilePath uploadFilePath,
			final ApiEmitter apiEmitter) {
		val user = paymentTryService.getCurrentUser();
		val stampDetail = paymentTryService.getStampDetails();
		val stampProperties = paymentTryService.getStampProperties(certificate);
		val invoiceFileConfiguration = paymentTryService.getFileConfiguration(taxpayer);
		val preferences = userTools.getPreferences(taxpayer); 

		// Cargando informacion para procesar el fichero
		val paymentStamp = new ApiPaymentStamp();
		paymentStamp.setTaxpayer(taxpayer);
		paymentStamp.setUser(user);
		paymentStamp.setIp(sessionService.getIp());
		paymentStamp.setUrlAcceso("payment/PaymentMasive");
		paymentStamp.setApiEmitter(apiEmitter);
		paymentStamp.setInvoiceFile(invoiceFile);
		paymentStamp.setPaymentFileConfig(invoiceFileConfiguration);
		paymentStamp.setStampProperties(stampProperties);
		paymentStamp.setStampDetail(stampDetail);
		paymentStamp.setPreferences(preferences);
		paymentStamp.setCertificate(certificate);
		paymentStamp.setAbsolutePath(uploadFilePath.getAbsolutePath());
		paymentStamp.setUploadPath(uploadFilePath.getUploadPath());
		paymentStamp.setDbPath(uploadFilePath.getDbPath());
		return paymentStamp;
	}
	
	private void uploadLimitError(final JResponse response) {
		val errorMessage = UProperties.getMessage("ERR0163", userTools.getLocale());
		val notification = JNotification.modalPanelMessageError(errorMessage);
		
		response.setUploadLimit(true);
		response.setSuccess(false);
		response.setNotification(notification);
		response.setError(Boolean.TRUE);
	}
	
	private void writeFileError(final JResponse response, final List<JError> errors) {
		val errorMessage = UProperties.getMessage("ERR0162", userTools.getLocale());
		val notification = JNotification.modalPanelMessageError(errorMessage);
		
		response.setNotification(notification);
		response.setError(Boolean.TRUE);
		response.setErrors(errors);
	}
	
	private void successResponse(final JResponse response) {
		// Notificacion Toastr
		val title = UProperties.getMessage("mega.cfdi.notification.toastr.paymentComplement.file.upload.title", userTools.getLocale());
		val message = UProperties.getMessage("mega.cfdi.notification.toastr.file.uploaded.message", userTools.getLocale());
		val toastr = Toastr.success(title, message);
		val notification = JNotification.toastrNotification(toastr);
		
		response.setNotification(notification);
		response.setSuccess(Boolean.TRUE);
	}

}