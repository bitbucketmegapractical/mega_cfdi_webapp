package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.megapractical.invoicing.api.smarterweb.SWCancellationResponse;
import org.megapractical.invoicing.api.wrapper.ApiCancellationRequest;

import lombok.val;

/**
 * This is a test service that should only be used in the development
 * environment to simulate the cancellation of a CFDI/Ret
 * 
 * @author Maikel Guerra Ferrer
 *
 */
final class CancellationFakeService {

	private CancellationFakeService() {
	}
	
	public static SWCancellationResponse cfdiCancellationFake(ApiCancellationRequest apiCancellationRequest) {
		return SWCancellationResponse
				.builder()
				.emitterRfc(apiCancellationRequest.getEmitterRfc())
				.receiverRfc(apiCancellationRequest.getReceiverRfc())
				.uuid(apiCancellationRequest.getUuid())
				.reason(apiCancellationRequest.getReason())
				.substitutionUuid(apiCancellationRequest.getSubstitutionUuid())
				.voucherTotal(apiCancellationRequest.getVoucherTotal())
				.isCancelable("Cancelable sin aceptación") 
				.status("Vigente")
				.statusCode("200")
				.responseUuidStatusCode("201")
				.acuseXml(fakeAcuse(apiCancellationRequest.getUuid()))
				.success(true)
				.cancellationDate(LocalDate.now())
				.cancellationTime(LocalTime.now())
				.build();
	}
	
	private static String fakeAcuse(String uuid) {
		val now = LocalDateTime.now().toString();
		return "<?xml version=\"1.0\" encoding=\"utf-8\"?><Acuse xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
				+ "Fecha=\""+now+"\" RfcEmisor=\"EKU9003173C9\"><Folios xmlns=\"http://cancelacfd.sat.gob.mx\">"
				+ "<UUID>"+uuid+"</UUID><EstatusUUID>201</EstatusUUID></Folios><Signature Id=\"SelloSAT\" xmlns=\"http://www.w3.org/2000/09/xmldsig#\">"
				+ "<SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" />"
				+ "<SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#hmac-sha512\" />"
				+ "<Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\">"
				+ "<XPath>not(ancestor-or-self::*[local-name()='Signature'])</XPath></Transform></Transforms>"
				+ "<DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha512\" />"
				+ "<DigestValue>0OvJ/wH5ZiomD1Jmy+HNgVEfFaY912kdZQqmhlNNCNf63ChKHRszG3Hty1FUCaYdzXS07P7/stbmKC2e14p6hQ==</DigestValue></Reference></SignedInfo>"
				+ "<SignatureValue>QPxwDEdVs8eja+lobBxf4jRGOyLqRmWXLJQjqKlUlXH2o4QR3zqM7mZS5ft8K0j73d7KMn40qweA4/cbntfuCQ==</SignatureValue>"
				+ "<KeyInfo><KeyName>BF66E582888CC845</KeyName><KeyValue><RSAKeyValue>"
				+ "<Modulus>n5YsGT0w5Z70ONPbqszhExfJU+KY3Bscftc2jxUn4wxpSjEUhnCuTd88OK5QbDW3Mupoc61jr83lRhUCjchFAmCigpC10rEntTfEU+7qtX8ud/jJJDB1a9lTIB6bhBN//X8IQDjhmHrfKvfen3p7RxLrFoxzWgpwKriuGI5wUlU=</Modulus>"
				+ "<Exponent>AQAB</Exponent></RSAKeyValue></KeyValue></KeyInfo></Signature></Acuse>";
	}

}