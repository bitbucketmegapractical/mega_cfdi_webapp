package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.util.Date;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.wrapper.ApiPayrollFileStatistics;
import org.megapractical.invoicing.api.wrapper.ApiPayrollParser;
import org.megapractical.invoicing.api.wrapper.ApiPayrollStamp;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEstadisticaEntity;
import org.megapractical.invoicing.webapp.filereader.FileReader;
import org.megapractical.invoicing.webapp.payload.PayrollParserRequest;
import org.megapractical.invoicing.webapp.payload.PayrollParserResponse;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class PayrollParserService {
	
	@Autowired
	PayrollParserTxtService payrollParserTxtService;
	
	@Autowired
	private PayrollParserCsvService payrollParserCsvService;
	
	private static final String PAYROLL_FILE_TXT_CODE = "N12TXT";
	private static final String PAYROLL_FILE_CSV_CODE = "N12CSV";
	
	//##### Procesando fichero
	ApiPayrollParser payrollParser(ApiPayrollStamp payrollStamp) {
		try {
			
			// Current environment
			val env = UProperties.env();
			
			//##### Ruta del fichero
			val payrollFilePath = payrollStamp.getUploadPath();
			
			//##### Determinando el encoding
			val encoding = UFile.fileEncoding(new File(payrollFilePath));
			if (encoding != null && !encoding.equalsIgnoreCase("utf-8") && !encoding.equalsIgnoreCase("utf8")) {
				UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > PROCESS ENCODING UTF-8 FILE STARTING...");
				
				if (Boolean.FALSE.equals(UFile.fileUtf8(new File(payrollFilePath)))) {
					return null;
				}
				
				UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > PROCESS ENCODING ENCODING UTF-8 FILE FINISHED...");
			}
			
			UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > PARSER FILE STARTING...");
			
			//##### Contenido del fichero
        	val entries = FileReader.readFile(payrollFilePath);
        	
        	val payrollParser = new ApiPayrollParser();
        	payrollParser.setPayrollStamp(payrollStamp);
        	
        	var statistics = new ApiPayrollFileStatistics();
        	statistics.setPayrollFileStatistics(new ArchivoNominaEstadisticaEntity());
        	statistics.getPayrollFileStatistics().setFechaInicioAnalisis(new Date());
        	statistics.getPayrollFileStatistics().setHoraInicioAnalisis(new Date());
        	
        	val request = new PayrollParserRequest();
        	request.setPayrollParser(payrollParser);
        	request.setStatistics(statistics);
        	request.setEntries(entries);
        	
        	PayrollParserResponse payrollParserResponse = null;
        	
        	//##### Determinando tipo de archivo a parsear
			val fileType = payrollStamp.getPayrollFile().getTipoArchivo();
			if (fileType.getCodigo().equalsIgnoreCase(PAYROLL_FILE_TXT_CODE)) {
				payrollParserResponse = payrollParserTxtService.parse(request);
			} else if (fileType.getCodigo().equalsIgnoreCase(PAYROLL_FILE_CSV_CODE)) {
				payrollParserResponse = payrollParserCsvService.parse(request);
			}
			
			if (payrollParserResponse != null) {
				return payrollParserResponse.getPayrollParser();
			}
        	
		} catch (Exception e) {
			System.out.println(String.format("#payrollParser error - %s", e.getMessage()));
		}
		return null;
	}
	
}