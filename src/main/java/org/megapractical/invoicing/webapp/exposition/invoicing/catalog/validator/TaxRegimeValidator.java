package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.TaxRegimeValidatorResponse;

public interface TaxRegimeValidator {
	
	TaxRegimeValidatorResponse validate(String taxRegime);
	
	TaxRegimeValidatorResponse validate(String taxRegime, String field);
	
	TaxRegimeValidatorResponse validate(String personType, String taxRegime, String field);
	
}