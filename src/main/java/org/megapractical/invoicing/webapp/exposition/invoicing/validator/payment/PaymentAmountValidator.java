package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.math.BigDecimal;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentAmountValidator {
	
	BigDecimal getAmount(String amountStr, MonedaEntity currency);
	
	BigDecimal getAmount(String amountStr, String field, MonedaEntity currency, List<JError> errors);
	
}