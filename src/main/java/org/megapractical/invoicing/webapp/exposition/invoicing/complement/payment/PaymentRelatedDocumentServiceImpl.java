package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CurrencyValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.CurrencyValidator;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentRelatedDocument;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class PaymentRelatedDocumentServiceImpl implements PaymentRelatedDocumentService {

	@Autowired
	private UserTools userTools;

	@Autowired
	private CurrencyValidator currencyValidator;
	
	private static final String INVALID_FORMAT_ERROR = "ERR1003";

	@Override
	public JResponse validate(String relatedDocumentData, String paymentCurrency, String amountStr) {
		val errors = new ArrayList<JError>();

		val paymentCurrencyResponse = validateCurrency(paymentCurrency, "cmp-payment-currency", errors);
		val paymentCurrencyEntity = paymentCurrencyResponse.getCurrency();
		val amount = getAmount(amountStr, paymentCurrencyEntity, errors);
		
		if (!paymentCurrencyResponse.isHasError() && !UValidator.isNullOrEmpty(amount)) {
			val relatedDocument = GsonClient.deserialize(relatedDocumentData, ComplementPaymentRelatedDocument.class);
			
			validateDocumentId(relatedDocument, errors);

			val currency = UBase64.base64Decode(relatedDocument.getCurrency());
			val currencyResponse = validateCurrency(currency, "cmp-payment-related-document-currency", errors);
			val currencyEntity = currencyResponse.getCurrency();
			
			val changeType = validateChangeType(relatedDocument, paymentCurrencyEntity, currencyEntity, errors);
			validateSerie(relatedDocument, errors);
			validateSheet(relatedDocument, errors);
			validatePartiality(relatedDocument, errors);
			
			val previousBalance = validatePreviousBalance(relatedDocument, currencyEntity, errors);
			val amountPaid = validateAmountPaid(relatedDocument, currencyEntity, paymentCurrencyEntity, amount, changeType, errors);
			validateOutstandingBalance(relatedDocument, currencyEntity, previousBalance, amountPaid, errors);
		}

		val response = new JResponse();
		if (!errors.isEmpty()) {
			response.setError(Boolean.TRUE);
			response.setErrors(errors);
		} else {
			response.setSuccess(Boolean.TRUE);
		}
		return response;
	}

	private CurrencyValidatorResponse validateCurrency(String currency, String field, List<JError> errors) {
		val currencyValidatorResponse = validateCurrency(currency, field);
		val currencyEntity = currencyValidatorResponse.getCurrency();
		val error = currencyValidatorResponse.getError();

		var hasError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			hasError = true;
		}
		return new CurrencyValidatorResponse(currencyEntity, hasError);
	}

	private CurrencyValidatorResponse validateCurrency(String currency, String field) {
		return currencyValidator.validate(currency, field);
	}

	private BigDecimal getAmount(String amountStr, MonedaEntity currency, List<JError> errors) {
		JError error = null;

		BigDecimal amount = null;
		if (!UValidator.isNullOrEmpty(currency)) {
			if (!UValidator.isDouble(amountStr)) {
				error = JError.error("cmp-payment-amount", INVALID_FORMAT_ERROR, userTools.getLocale());
			} else {
				amount = UValue.bigDecimalStrict(amountStr, UTools.decimalValue(amountStr));
				if (!UValidator.isValidDecimalPart(amount, currency.getDecimales())) {
					error = JError.error("cmp-payment-amount", "ERR0129", userTools.getLocale());
					amount = null;
				}
			}
			
			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
			}
		}
		return amount;
	}

	private void validateDocumentId(ComplementPaymentRelatedDocument relatedDocument, List<JError> errors) {
		JError error = null;

		val documentId = UBase64.base64Decode(relatedDocument.getDocumentId());
		if (UValidator.isNullOrEmpty(documentId)) {
			error = JError.required("cmp-payment-related-document-id", userTools.getLocale());
		} else if (documentId.length() < 16 || documentId.length() > 36) {
			error = JError.error("cmp-payment-related-document-id", "ERR0130", userTools.getLocale());
		}

		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
	}

	private BigDecimal validateChangeType(ComplementPaymentRelatedDocument relatedDocument, 
										  MonedaEntity paymentCurrency,
										  MonedaEntity currency, 
										  List<JError> errors) {
		BigDecimal changeTypeDecimal = null;
		JError error = null;
		val field = "cmp-payment-related-document-change-type";
		
		if (!UValidator.isNullOrEmpty(currency)) {
			val changeType = UBase64.base64Decode(relatedDocument.getChangeType());
			if (UValidator.isNullOrEmpty(changeType)) {
				error = JError.required(field, userTools.getLocale());
			} else if (!UValidator.isDouble(changeType)) {
				error = JError.error(field, INVALID_FORMAT_ERROR, userTools.getLocale());
			} else if (!paymentCurrency.getCodigo().equalsIgnoreCase(currency.getCodigo())) {
				changeTypeDecimal = UValue.bigDecimalStrict(changeType);
				if (!changeTypeDecimal.toString().matches(VoucherUtils.CHANGE_TYPE_EXPRESSION)) {
					error = JError.errorSource(field, "CFDI33116", userTools.getLocale());
				}
			} else if (!changeType.equals("1")) {
				error = JError.errorSource(field, "CRP20238", userTools.getLocale());
			}

			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
			}
		}
		
		return changeTypeDecimal;
	}

	private void validateSerie(ComplementPaymentRelatedDocument relatedDocument, List<JError> errors) {
		JError error = null;

		val serie = UBase64.base64Decode(relatedDocument.getSerie());
		if (!UValidator.isNullOrEmpty(serie) && (serie.length() < 1 || serie.length() > 25)) {
			error = JError.error("cmp-payment-related-document-serie", "ERR0131", userTools.getLocale());
		}

		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
	}

	private void validateSheet(ComplementPaymentRelatedDocument relatedDocument, List<JError> errors) {
		JError error = null;

		val sheet = UBase64.base64Decode(relatedDocument.getSheet());
		if (!UValidator.isNullOrEmpty(sheet) && (sheet.length() < 1 || sheet.length() > 40)) {
			error = JError.error("cmp-payment-related-document-sheet", "ERR0132", userTools.getLocale());
		}

		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
	}

	private void validatePartiality(ComplementPaymentRelatedDocument relatedDocument, List<JError> errors) {
		JError error = null;
		val field = "cmp-payment-related-document-partiality";

		val partiality = UBase64.base64Decode(relatedDocument.getPartiality());
		if (UValidator.isNullOrEmpty(partiality)) {
			error = JError.required(field, userTools.getLocale());
		} else if (!UValidator.isInteger(partiality)) {
			error = JError.error(field, INVALID_FORMAT_ERROR, userTools.getLocale());
		}

		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
	}

	private BigDecimal validatePreviousBalance(ComplementPaymentRelatedDocument relatedDocument, 
											   MonedaEntity currency, 
											   List<JError> errors) {
		BigDecimal previousBalanceBigDecimal = null;
		JError error = null;
		val field = "cmp-payment-related-document-previous-balance";

		val previousBalance = UBase64.base64Decode(relatedDocument.getAmountPartialityBefore());
		if (UValidator.isNullOrEmpty(previousBalance)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			previousBalanceBigDecimal = UValue.bigDecimalStrict(previousBalance, UTools.decimals(previousBalance));
			if (!UValidator.isValidDecimalPart(previousBalanceBigDecimal, currency.getDecimales())) {
				error = JError.error(field, "ERR0133", userTools.getLocale());
			}
		}

		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}

		return previousBalanceBigDecimal;
	}
	
	private BigDecimal validateAmountPaid(ComplementPaymentRelatedDocument relatedDocument, 
										  MonedaEntity currency,
										  MonedaEntity paymentCurrency,
										  BigDecimal amount,
										  BigDecimal changeType,
										  List<JError> errors) {
		BigDecimal amountPaidBigDecimal = null;
		JError error = null;
		val field = "cmp-payment-related-document-amount-paid";
		
		val amountPaid = UBase64.base64Decode(relatedDocument.getAmountPaid());
		if (UValidator.isNullOrEmpty(amountPaid)) {
			error = JError.required(field, userTools.getLocale());
			errors.add(error);
		} else {
			amountPaidBigDecimal = getAmountPaid(amountPaid, currency, paymentCurrency, amount, changeType, errors);
		}
		return amountPaidBigDecimal;
	}
	
	private BigDecimal getAmountPaid(String amountPaid, 
										  MonedaEntity currency, 
										  MonedaEntity paymentCurrency,
										  BigDecimal amount, 
										  BigDecimal changeType, 
										  List<JError> errors) {
		JError error = null;
		val field = "cmp-payment-related-document-amount-paid";
		
		val amountPaidBigDecimal = UValue.bigDecimalStrict(amountPaid, UTools.decimals(amountPaid));
		if (!UValidator.isValidDecimalPart(amountPaidBigDecimal, currency.getDecimales())) {
			error = JError.error(field, "ERR0134", userTools.getLocale());
		} else {
			if (amount.compareTo(amountPaidBigDecimal) < 0) {
				error = JError.error(field, "ERR0154", userTools.getLocale());
			} else {
				if (!UValidator.isNullOrEmpty(changeType)) {
					val result = UValue.bigDecimal(UValue.bigDecimalString(amount.multiply(changeType)), paymentCurrency.getDecimales());
					if (!result.equals(amountPaidBigDecimal)) {
						error = JError.error(field, "ERR0140", userTools.getLocale());
					}
				}
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		
		return amountPaidBigDecimal;
	}
	
	private void validateOutstandingBalance(ComplementPaymentRelatedDocument relatedDocument, 
											MonedaEntity currency,
											BigDecimal previousBalance, 
											BigDecimal amountPaid, 
											List<JError> errors) {
		JError error = null;
		val field = "cmp-payment-related-document-outstanding-balance";
		
		val outstandingBalance = UBase64.base64Decode(relatedDocument.getDifference());
		if (UValidator.isNullOrEmpty(outstandingBalance)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			val differenceBigDecimal = UValue.bigDecimalStrict(outstandingBalance, UTools.decimals(outstandingBalance));
			if (!UValidator.isValidDecimalPart(differenceBigDecimal, currency.getDecimales())) {
				error = JError.error(field, "ERR0135", userTools.getLocale());
			} else {
				val difference = UValue.bigDecimal(outstandingBalance, currency.getDecimales());
				validateOutstandingBalance(difference, previousBalance, amountPaid, errors);
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
	}
	
	private void validateOutstandingBalance(BigDecimal difference, 
											BigDecimal previousBalance, 
											BigDecimal amountPaid,
											List<JError> errors) {
		if (!UValidator.isNullOrEmpty(amountPaid)) {
			val calc = previousBalance.subtract(amountPaid);
			if (!difference.equals(calc)) {
				val error = JError.error("cmp-payment-related-document-outstanding-balance", "ERR0137", userTools.getLocale());
				errors.add(error);
			}
		}
	}

}