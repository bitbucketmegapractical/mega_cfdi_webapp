package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.ImpuestosP.RetencionesP;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.ImpuestosP.TrasladosP;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentTaxesResponse {
	@Getter(AccessLevel.NONE)
	private List<TrasladosP> transferreds;
	@Getter(AccessLevel.NONE)
	private List<RetencionesP> withhelds;
	
	public List<TrasladosP> getTransferreds() {
		if (transferreds == null) {
			transferreds = new ArrayList<>();
		}
		return transferreds;
	}
	
	public List<RetencionesP> getWithhelds() {
		if (withhelds == null) {
			withhelds = new ArrayList<>();
		}
		return withhelds;
	}
	
}