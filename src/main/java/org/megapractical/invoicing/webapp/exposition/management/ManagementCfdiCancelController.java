package org.megapractical.invoicing.webapp.exposition.management;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.client.CancelationProperties;
import org.megapractical.invoicing.api.client.CancelationProperties.CfdiCancelation;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiCancelationRequestJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.CancelationService;
import org.megapractical.invoicing.webapp.json.JCfdi;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.Layout;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.opencsv.CSVReader;

@Controller
@Scope("session")
public class ManagementCfdiCancelController {
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
		
	@Autowired
	UserTools userTools;
	
	@Autowired
	PropertiesTools propertiesTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	CancelationService cancelationService;
	
	@Resource
	ICfdiJpaRepository iCfdiJpaRepository;
	
	@Resource
	ICfdiCancelationRequestJpaRepository iCfdiCancelationRequestJpaRepository;
	
	@Resource
	ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	
	@Resource
	ITaxpayerJpaRepository iTaxpayerJpaRepository;
	
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	@ModelAttribute("processingRequest")
	public String processingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@Layout(value = "layouts/admon")
	@RequestMapping(value = "/cancelationCfdi", method = RequestMethod.GET)
	public String init(Model model) {
		try {
			
			if(userTools.isRoot()){
				HtmlHelper.functionalitySelected("MngGeneral", "cancelation-cfdi");
				appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
				return "/management/CfdiCancel";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/cancelationCfdi", method = RequestMethod.POST)
	public @ResponseBody String cancelationCfdi(HttpServletRequest request) throws IOException{
		JError error = new JError();
		List<JError> errors = new ArrayList<>();
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		JCfdi cfdi = new JCfdi();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			//##### Uuid	
			String uuid = UBase64.base64Decode(request.getParameter("uuid"));
			
			//##### Forzar cancelación
			String forceCancelationStr = request.getParameter("forceCancelation");
			Boolean forceCancelation = Boolean.valueOf(forceCancelationStr);
			
			//##### Verificando que el usuario sea root
			if(userTools.isRoot() && !UValidator.isNullOrEmpty(uuid)){
				//##### Obteniendo el CFDI
				CfdiEntity cfdiEntity = iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull(uuid);
				if(UValidator.isNullOrEmpty(cfdiEntity) && forceCancelation) {
					cfdiEntity = iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoTrueAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull(uuid);
				}
				
				ContribuyenteEntity emitter = null;
				byte[] certificateByte = null;
				byte[] privateKeyByte = null;
				String passwd = null;
				
				if(!UValidator.isNullOrEmpty(cfdiEntity)) {
					//##### Cargando emisor
					emitter = cfdiEntity.getContribuyente();
					
					//##### Cargando certificado del emisor
					ContribuyenteCertificadoEntity certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(emitter);
					
					//##### Verificando vigencia del certificado
					if(UValidator.isNullOrEmpty(certificate) || UDateTime.isBeforeLocalDate(certificate.getFechaExpiracion(), new Date())){
						notification.setPageMessage(true);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(UProperties.getMessage("ERR0004", userTools.getLocale()));
						
						response.setNotification(notification);
						response.setError(Boolean.TRUE);
					}else {
						certificateByte = certificate.getCertificado();
						privateKeyByte = certificate.getLlavePrivada();
						passwd = UBase64.base64Decode(certificate.getClavePrivada());
					}
				}else {
					notification.setPageMessage(true);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					notification.setMessage(UProperties.getMessage("mega.cfdi.information.management.cancelation.request.blank", userTools.getLocale()));
					
					response.setNotification(notification);
					response.setError(Boolean.TRUE);
				}
				
				if(!response.isError()){
					OperationCode operationCode = OperationCode.CANCELACION_CFDIV33_SIN_COMPLEMENTO_MASTER_ID18;
					EventTypeCode eventTypeCode = EventTypeCode.CANCELACION_CFDIV33_SIN_COMPLEMENTO_MASTER_ID7;
					
					userTools.log("[INFO] INITIALIZING CANCELATION REQUEST...");
					
					CancelationProperties properties = new CancelationProperties();
					properties.setEmitterId(emitter.getId().toString());
					properties.setSessionId(propertiesTools.getSessionId());
					properties.setSourceSystem("CFDI_PLUS");
					properties.setCorrelationId(propertiesTools.getCorrelationId());
					properties.setEnvironment(propertiesTools.getEnvironment());
					properties.setEmitterRfc(emitter.getRfc());
					properties.setCertificate(certificateByte);
					properties.setPrivateKey(privateKeyByte);
					properties.setPasswd(passwd);
					properties.setServices(propertiesTools.getServices());

					CfdiCancelation cfdiCancelation = new CfdiCancelation();
					cfdiCancelation.setReceiverRfc(cfdiEntity.getReceptorRfc());
					cfdiCancelation.setVoucherTotal(UValue.decimalStringForce(UValue.doubleString(cfdiEntity.getTotal())));
					cfdiCancelation.setUuid(uuid);
					properties.setCfdiCancelation(cfdiCancelation);
					cancelationService.cancelationAsync(properties);
					
					cfdiEntity.setCancelacionEnProceso(Boolean.TRUE);
					persistenceDAO.update(cfdiEntity);
					
					// Mensaje de notificacion
					notification.setPageMessage(true);
					notification.setCssStyleClass(CssStyleClass.INFO.value());
					notification.setMessage(UProperties.getMessage("mega.cfdi.information.management.cancelation.request", userTools.getLocale()));
					notification.setDismiss(Boolean.TRUE);
					
					cfdi.setCancelationInProgress(Boolean.TRUE);
					response.setNotification(notification);
					response.setSuccess(Boolean.TRUE);
					
					// Bitacora :: Registro de accion en bitacora
					appLog.logOperation(operationCode);
					
					// Timeline :: Registro de accion
					timelineLog.timelineLog(eventTypeCode);
				}
			}else {
				notification.setPageMessage(true);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0058", userTools.getLocale()));
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			error.setMessage(UProperties.getMessage("ERR0058", userTools.getLocale()));
			errors.add(error);
			
			notification.setPageMessage(true);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(error.getMessage());
			
			response.setErrors(errors);
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		cfdi.setResponse(response);
		jsonInString = gson.toJson(cfdi);
		return jsonInString;
	}
	
	@Layout(value = "layouts/admon")
	@RequestMapping(value = "/cancelationCfdiMasive", method = RequestMethod.GET)
	public String initMasiveCancelation(Model model) {
		try {
			
			HtmlHelper.functionalitySelected("AdvancedOptions", "cancelation-cfdi-masive");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			return "/management/CfdiCancelMasive";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/cancelationCfdiMasive", params={"uploadFile"})
	public @ResponseBody String cfdiMasiveCancelation(HttpServletRequest request, @RequestParam(value="fileUploaded") MultipartFile multipartUploadedFile) throws IOException{
		JError error = new JError();
		List<JError> errors = new ArrayList<>();
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		JCfdi cfdi = new JCfdi();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			//##### Archivo con UUIDs
			File invoiceFileUpload = null;
			
			String originalFilename = null;
			String invoiceFileName = null;
			String invoiceFileExt = null;
			
			Boolean errorMarked = false;
			
			if(userTools.isRoot()){
				if (multipartUploadedFile.isEmpty()) {
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					originalFilename = multipartUploadedFile.getOriginalFilename();
					invoiceFileExt = UFile.getExtension(originalFilename);
					
					if(UValidator.isNullOrEmpty(invoiceFileExt) || !invoiceFileExt.equalsIgnoreCase("txt")){
						error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
						errorMarked = true;
					}else{
						invoiceFileName = UFile.getName(originalFilename).toUpperCase();
						invoiceFileUpload = UFile.multipartFileToFile(multipartUploadedFile);
					}
				}
				
				OperationCode operationCode = OperationCode.CANCELACION_MASIVA_CFDIV33_ID83;
				EventTypeCode eventTypeCode = EventTypeCode.CANCELACION_MASIVA_CFDIV33_ID47;
				
				userTools.log("[INFO] INITIALIZING CANCELATION REQUEST...");
				
				if(!errorMarked){
					//##### Leyendo fichero
					CSVReader reader = new CSVReader(new FileReader(invoiceFileUpload));
					
					//##### Contenido del fichero
		        	List<String[]> entries = reader.readAll();
		        	
		        	//##### Cerrando el fichero
		        	reader.close();
		        	
		        	//##### Forzar cancelación
		    		String forceCancelationStr = request.getParameter("forceCancelation");
		    		Boolean forceCancelation = Boolean.valueOf(forceCancelationStr);
		        	
		        	List<CancelationProperties> cancelationProperties = new ArrayList<>();
		        	for (int i = 0; i < entries.size(); i ++){
		        		//##### Informacion de la línea actual
		        		String[] item = entries.get(i);
		        		String uuid = item[0];
		        		
		        		//##### Obteniendo el CFDI
		    			CfdiEntity cfdiEntity = iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull(uuid);
		    			if(UValidator.isNullOrEmpty(cfdiEntity) && forceCancelation) {
		    				cfdiEntity = iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoTrueAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull(uuid);
		    			}
		        		
						ContribuyenteEntity emitter = null;
						byte[] certificateByte = null;
						byte[] privateKeyByte = null;
						String passwd = null;
						
						if(!UValidator.isNullOrEmpty(cfdiEntity)){
							//##### Cargando emisor
							emitter = cfdiEntity.getContribuyente();
							
							//##### Cargando certificado del emisor
							ContribuyenteCertificadoEntity certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(emitter);
							
							//##### Verificando vigencia del certificado
							if(UValidator.isNullOrEmpty(certificate) || UDateTime.isBeforeLocalDate(certificate.getFechaExpiracion(), new Date())){
								notification.setPageMessage(true);
								notification.setCssStyleClass(CssStyleClass.ERROR.value());
								notification.setMessage(UProperties.getMessage("ERR0004", userTools.getLocale()));
								
								response.setNotification(notification);
								response.setError(Boolean.TRUE);
							}else {
								certificateByte = certificate.getCertificado();
								privateKeyByte = certificate.getLlavePrivada();
								passwd = UBase64.base64Decode(certificate.getClavePrivada());
							}
							
							if(!response.isError()){
								CfdiCancelation cfdiCancelation = new CfdiCancelation();
								cfdiCancelation.setReceiverRfc(cfdiEntity.getReceptorRfc());
								cfdiCancelation.setVoucherTotal(UValue.decimalStringForce(UValue.doubleString(cfdiEntity.getTotal())));
								cfdiCancelation.setUuid(uuid);
								
								String environment = propertiesTools.getEnvironment();
								
								CancelationProperties properties = new CancelationProperties();
								properties.setEmitterId(emitter.getId().toString());
								properties.setSessionId(propertiesTools.getSessionId());
								properties.setSourceSystem("CFDI_PLUS");
								properties.setCorrelationId(propertiesTools.getCorrelationId());
								properties.setEnvironment(environment);
								properties.setEmitterRfc(emitter.getRfc());
								properties.setCertificate(certificateByte);
								properties.setPrivateKey(privateKeyByte);
								properties.setPasswd(passwd);
								properties.setCfdiCancelation(cfdiCancelation);
								properties.setServices(propertiesTools.getServices());
								cancelationProperties.add(properties);
								
								cfdiEntity.setCancelacionEnProceso(Boolean.TRUE);
								persistenceDAO.update(cfdiEntity);
							}
						}
		        	}
		        	
		        	String message = UProperties.getMessage("mega.cfdi.information.management.cancelation.masive.request", userTools.getLocale());
		        	String cssStyle = CssStyleClass.INFO.value();
		        	if(!cancelationProperties.isEmpty()) {
						cancelationService.cancelationAsync(cancelationProperties);
						
						Integer uuidsSize = cancelationProperties.size();
						
						// Bitacora :: Registro de accion en bitacora
						appLog.logOperation(operationCode, uuidsSize.toString());
						
						// Timeline :: Registro de accion
						timelineLog.timelineLog(eventTypeCode, uuidsSize.toString());
					}else {
						message = UProperties.getMessage("mega.cfdi.information.management.cancelation.masive.request.blank", userTools.getLocale());
						cssStyle = CssStyleClass.ERROR.value();
					}
		        	
		        	// Mensaje de notificacion
					notification.setPageMessage(true);
					notification.setCssStyleClass(cssStyle);
					notification.setMessage(message);
					notification.setDismiss(Boolean.TRUE);
					
					response.setNotification(notification);
					response.setSuccess(Boolean.TRUE);
				}else{
					notification.setPageMessage(true);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					notification.setMessage(UProperties.getMessage("ERR0058", userTools.getLocale()));
					
					response.setNotification(notification);
					response.setError(Boolean.TRUE);
				}
			}else {
				notification.setPageMessage(true);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0058", userTools.getLocale()));
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			error.setMessage(UBase64.base64Encode(UProperties.getMessage("ERR0058", userTools.getLocale())));
			errors.add(error);
			
			notification.setPageMessage(true);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(error.getMessage());
			
			response.setErrors(errors);
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		cfdi.setResponse(response);
		jsonInString = gson.toJson(cfdi);
		return jsonInString;
	}
}