package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import java.util.Date;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.service.InvoiceFileConfigurationService;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.CertificateService;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class PaymentTryServiceImpl implements PaymentTryService {

	@Autowired
	private UserTools userTools;
	
	@Autowired
	private PropertiesTools propertiesTools;

	@Autowired
	private CertificateService certificateService;
	
	@Autowired
	private StampControlService stampControlService;
	
	@Autowired
	private InvoiceFileConfigurationService invoiceFileConfigurationService;

	@Override
	public ContribuyenteEntity getCurrentTaxpayer() {
		val activeRfc = userTools.getContribuyenteActive().getRfcActivo();
		return userTools.getContribuyenteActive(activeRfc);
	}

	@Override
	public UsuarioEntity getCurrentUser() {
		return userTools.getCurrentUser();
	}

	@Override
	public ContribuyenteCertificadoEntity getCertificate(ContribuyenteEntity taxpayer) {
		val certificate = certificateService.findByTaxpayer(taxpayer);
		// Verifying validity
		if (certificate == null || UDateTime.isBeforeLocalDate(certificate.getFechaExpiracion(), new Date())) {
			return null;
		}
		return certificate;
	}
	
	@Override
	public TimbreEntity getStampDetails() {
		return stampControlService.stampDetails();
	}
	
	@Override
	public StampProperties getStampProperties(ContribuyenteCertificadoEntity certificate) {
		val stampProperties = new StampProperties();
		stampProperties.setEmitterId(propertiesTools.getEmitterId());
		stampProperties.setSessionId(propertiesTools.getSessionId());
		stampProperties.setSourceSystem(propertiesTools.getSourceSystem());
		stampProperties.setCorrelationId(propertiesTools.getCorrelationId());
		stampProperties.setCertificate(certificate.getCertificado());
		stampProperties.setPrivateKey(certificate.getLlavePrivada());
		stampProperties.setPasswd(UBase64.base64Decode(certificate.getClavePrivada()));
		stampProperties.setEnvironment(propertiesTools.getEnvironment());
		stampProperties.setServices(propertiesTools.getServices());
		return stampProperties;
	}

	@Override
	public ArchivoFacturaConfiguracionEntity getFileConfiguration(ContribuyenteEntity taxpayer) {
		var invoiceFileConfiguration = invoiceFileConfigurationService.findByTaxpayer(taxpayer);
		if (invoiceFileConfiguration == null) {
			invoiceFileConfiguration = configInvoiceFileConfiguration(taxpayer);
		}
		return invoiceFileConfiguration;
	}
	
	private ArchivoFacturaConfiguracionEntity configInvoiceFileConfiguration(ContribuyenteEntity taxpayer) {
		val entity = new ArchivoFacturaConfiguracionEntity();
		entity.setContribuyente(taxpayer);
		entity.setEnvioXmlPdf(Boolean.FALSE);
		entity.setEnvioXml(Boolean.FALSE);
		entity.setEnvioPdf(Boolean.FALSE);
		entity.setConsolidado1x(Boolean.FALSE);
		entity.setConsolidado2x(Boolean.FALSE);
		entity.setDirectorioXmlPdfUnico(Boolean.TRUE);
		entity.setDirectorioXmlPdfSeparado(Boolean.FALSE);
		entity.setCompactado(Boolean.TRUE);
		entity.setNotificacion(Boolean.FALSE);
		
		return invoiceFileConfigurationService.save(entity);
	}

}