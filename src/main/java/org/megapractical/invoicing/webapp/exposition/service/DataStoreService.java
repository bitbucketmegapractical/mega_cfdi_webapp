package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment;
import org.megapractical.invoicing.api.wrapper.ApiPayrollFileConfiguration;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.api.wrapper.ApiRetentionFileConfiguration;
import org.megapractical.invoicing.dal.bean.datatype.StorageType;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.ClienteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoDocumentoRelacionadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteCfdiRelacionadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteCfdiRelacionadoUuidEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteComplementoPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteConceptoCuentaPredialEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteConceptoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteConceptoImpuestoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteConceptoInformacionAduaneraEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteNodoConceptoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteNodoEmisorEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteNodoReceptorEntity;
import org.megapractical.invoicing.dal.bean.jpa.ConceptoTipoImpuestoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContactoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteConceptoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteLugarExpedicionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaCfdiRelacionadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaCfdiRelacionadoUuidEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaComplementoPagoDocumentoRelacionadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaComplementoPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaConceptoCuentaPredialEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaConceptoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaConceptoImpuestoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaConceptoInformacionAduaneraEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaNodoConceptoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaNodoEmisorEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaNodoReceptorEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionComplementoEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoFactorEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoImpuestoEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiRelationshipTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IConceptTaxTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICustomerJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IFactorTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IKeyProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IMeasurementUnitJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentMethodJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentWayJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPreferencesJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionComplementJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStringTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerConceptJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerPostalCode;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.InformacionAduanera;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.PaymentUtils;
import org.megapractical.invoicing.webapp.wrapper.WAccount;
import org.megapractical.invoicing.webapp.wrapper.WConcept;
import org.megapractical.invoicing.webapp.wrapper.WContact;
import org.megapractical.invoicing.webapp.wrapper.WCustomer;
import org.megapractical.invoicing.webapp.wrapper.WRetention;
import org.megapractical.invoicing.webapp.wrapper.WSerieSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@Scope("session")
public class DataStoreService {
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	UserTools userTools; 
	
	@Resource
	IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository; 
	
	@Resource
	ITaxpayerJpaRepository iTaxpayerJpaRepository;
	
	@Resource
	IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository;
	
	@Resource
	ITaxTypeJpaRepository iTaxTypeJpaRepository;
	
	@Resource
	IConceptTaxTypeJpaRepository iConceptTaxType;
	
	@Resource
	IFactorTypeJpaRepository iFactorTypeJpaRepository;
	
	@Resource
	ICfdiRelationshipTypeJpaRepository iCfdiRelationshipTypeJpaRepository;
	
	@Resource
	IPreferencesJpaRepository iPreferencesJpaRepository;
	
	@Resource
	ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;
	
	@Resource
	ICustomerJpaRepository iCustomerJpaRepository;
	
	@Resource
	ITaxpayerConceptJpaRepository iTaxpayerConceptJpaRepository;
	
	@Resource
	IPaymentWayJpaRepository iPaymentWayJpaRepository;
	
	@Resource
	ICurrencyJpaRepository iCurrencyJpaRepository;
	
	@Resource
	IPaymentMethodJpaRepository iPaymentMethodJpaRepository;
	
	@Resource
	IStringTypeJpaRepository iStringTypeJpaRepository;
	
	@Resource
	ITaxpayerPostalCode iTaxpayerPostalCode;
	
	@Resource
	IRetentionJpaRepository iRetentionJpaRepository;
	
	@Resource
	IRetentionComplementJpaRepository iRetentionComplementJpaRepository;
	
	@Transactional
	public CfdiEntity cfdiStore(ApiCfdi apiCfdi){
		try {
			
			//##### Determinando si el comprobante trae complemento
			val voucherWithComplement = apiCfdi.getComplement() != null;
			String complementCode = null;
			if (voucherWithComplement) {
				complementCode = apiCfdi.getComplement().getPackageClass();
			}
			
			//##### Contribuyente emisor del comprobante
			ContribuyenteEntity taxpayerEmitter = iTaxpayerJpaRepository.findByRfc(apiCfdi.getEmitter().getRfc());
			
			//##### Preferencias del emisor
			PreferenciasEntity preferences = apiCfdi.getPreferences();
			Boolean customerRegister = preferences.getRegistrarCliente();
			Boolean customerUpdate = preferences.getActualizarCliente();
			Boolean conceptRegister = preferences.getRegistrarConcepto();
			Boolean conceptUpdate = preferences.getActualizarConcepto();
			Boolean taxRegimeRegister = preferences.getRegistrarRegimenFiscal();
			Boolean taxRegimeUpdate = preferences.getActualizarRegimenFiscal();
			Boolean taxpayerPostalCode = preferences.getRegistrarActualizarLugarExpedicion();
			
			userTools.log("[INFO] REGISTERING VOUCHER...");
			
			//##### Registrando el comprobante
			ComprobanteEntity voucher = new ComprobanteEntity();
			voucher.setVersion(apiCfdi.getVoucher().getVersion());
			if(!UValidator.isNullOrEmpty(apiCfdi.getVoucher().getSerie())){
				voucher.setSerie(apiCfdi.getVoucher().getSerie().toUpperCase());
			}
			if(!UValidator.isNullOrEmpty(apiCfdi.getVoucher().getFolio())){
				voucher.setFolio(apiCfdi.getVoucher().getFolio());
			}
			voucher.setFechaHoraExpedicion(apiCfdi.getVoucher().getDateTimeExpedition());
			voucher.setTipoComprobante(apiCfdi.getVoucherType());
			voucher.setFormaPago(apiCfdi.getPaymentWay());
			voucher.setMetodoPago(apiCfdi.getPaymentMethod());
			voucher.setMoneda(apiCfdi.getCurrency());
			if(apiCfdi.getVoucher().getChangeType() != null){
				voucher.setTipoCambio(UValue.doubleValue(apiCfdi.getVoucher().getChangeType()));
			}
			voucher.setCondicionesPago(apiCfdi.getVoucher().getPaymentConditions());
			if(apiCfdi.getVoucher().getDiscount() != null){
				voucher.setDescuento(UValue.doubleValue(apiCfdi.getVoucher().getDiscount()));
			}
			voucher.setSubtotal(UValue.doubleValue(apiCfdi.getVoucher().getSubTotal()));
			voucher.setTotal(UValue.doubleValue(apiCfdi.getVoucher().getTotal()));
			voucher.setCodigoPostal(apiCfdi.getPostalCode());
			voucher.setClaveConfirmacionPac(apiCfdi.getVoucher().getConfirmation());
			voucher.setFechaHoraTimbrado(apiCfdi.getVoucher().getDateTimeStamp());
			voucher.setFolioFiscal(apiCfdi.getVoucher().getTaxSheet());
			voucher.setNumeroCertificadoEmisor(apiCfdi.getVoucher().getCertificateEmitterNumber());
			voucher.setNumeroCertificadoSat(apiCfdi.getVoucher().getCertificateSatNumber());
			voucher.setSelloEmisor(apiCfdi.getVoucher().getEmitterSeal());
			voucher.setSelloSat(apiCfdi.getVoucher().getSatSeal());
			voucher.setCadenaOriginal(apiCfdi.getVoucher().getOriginalString());
			voucher.setQr(apiCfdi.getQr());
			if(!UValidator.isNullOrEmpty(apiCfdi.getLegend())){
				voucher.setLeyenda(apiCfdi.getLegend());
			}
			persistenceDAO.persist(voucher);
			
			//##### Registrando informacion de los cfdi relacionados al comprobante
			if(apiCfdi.getVoucher().getRelatedCfdi() != null){
				
				userTools.log("[INFO] RELATED CFDI REGISTERING...");
				
				ComprobanteCfdiRelacionadoEntity voucherRelatedCfdi = new ComprobanteCfdiRelacionadoEntity();
				voucherRelatedCfdi.setComprobante(voucher);
				voucherRelatedCfdi.setCfdiTipoRelacion(iCfdiRelationshipTypeJpaRepository.findByCodigoAndEliminadoFalse(apiCfdi.getVoucher().getRelatedCfdi().getRelationshipTypeCode()));
				persistenceDAO.persist(voucherRelatedCfdi);
				
				for (RelatedCfdiUuid item : apiCfdi.getVoucher().getRelatedCfdi().getUuids()) {
					ComprobanteCfdiRelacionadoUuidEntity uuid = new ComprobanteCfdiRelacionadoUuidEntity();
					uuid.setComprobanteCfdiRelacionado(voucherRelatedCfdi);
					uuid.setUudi(item.getUuid());
					persistenceDAO.persist(uuid);
				}
			}
			
			userTools.log("[INFO] REGISTERING EMITTER NODE...");
			
			//##### Registrando relacion del comprobante y el emisor
			ComprobanteNodoEmisorEntity voucherEmitterNode = new ComprobanteNodoEmisorEntity();
			voucherEmitterNode.setComprobante(voucher);
			voucherEmitterNode.setNombreRazonSocial(apiCfdi.getEmitter().getNombreRazonSocial());
			voucherEmitterNode.setRfc(apiCfdi.getEmitter().getRfc());
			voucherEmitterNode.setRegimenFiscal(apiCfdi.getEmitter().getRegimenFiscal());
			persistenceDAO.persist(voucherEmitterNode);
			
			userTools.log("[INFO] REGISTERING RECEIVER NODE...");
			
			//##### Registrando relacion del comprobante y el receptor
			ComprobanteNodoReceptorEntity voucherReceiverNode = new ComprobanteNodoReceptorEntity();
			voucherReceiverNode.setComprobante(voucher);
			voucherReceiverNode.setNombreRazonSocial(apiCfdi.getReceiver().getNombreRazonSocial());
			voucherReceiverNode.setRfc(apiCfdi.getReceiver().getRfc());
			voucherReceiverNode.setNumeroRegistroIdentidadFiscal(apiCfdi.getReceiver().getNumRegIdTrib());
			voucherReceiverNode.setPais(apiCfdi.getReceiver().getResidenciaFiscal());
			voucherReceiverNode.setUsoCfdi(apiCfdi.getReceiver().getUsoCFDI());
			voucherReceiverNode.setCorreoElectronico(apiCfdi.getReceiver().getCorreoElectronico());
			persistenceDAO.persist(voucherReceiverNode);
			
			//##### Registrando/Actualizando cliente en catalogo
			if(customerRegister || customerUpdate){
				
				ClienteEntity customer = new ClienteEntity();
				
				Long customerSelected = apiCfdi.getReceiver().getCustomerSelected();
				if(!UValidator.isNullOrEmpty(customerSelected)){
					customer = iCustomerJpaRepository.findByIdAndEliminadoFalse(customerSelected);
				}
				
				customer.setContribuyente(taxpayerEmitter);
				customer.setRfc(apiCfdi.getReceiver().getRfc().toUpperCase());
				customer.setNombreRazonSocial(apiCfdi.getReceiver().getNombreRazonSocial().toUpperCase());
				if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getCorreoElectronico())){
					customer.setCorreoElectronico(apiCfdi.getReceiver().getCorreoElectronico());
				}
				if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getCurp())){
					customer.setCurp(apiCfdi.getReceiver().getCurp().toUpperCase());
				}
				customer.setResidente(apiCfdi.getReceiver().getResidente());
				if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getResidenciaFiscal())){
					customer.setPais(apiCfdi.getReceiver().getResidenciaFiscal());
				}
				if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getNumRegIdTrib())){
					customer.setNumeroRegistroIdentidadFiscal(apiCfdi.getReceiver().getNumRegIdTrib().toUpperCase());
				}
				customer.setEliminado(Boolean.FALSE);
				
				if(!UValidator.isNullOrEmpty(customerSelected)){
					if(customerUpdate){
						userTools.log("[INFO] UPDATING CUSTOMER IN CATALOG...");
						persistenceDAO.update(customer);
					}					
				}else{
					if(customerRegister){
						userTools.log("[INFO] REGISTERING CUSTOMER IN CATALOG...");
						
						Boolean persist = true;
						ClienteEntity customerObj = iCustomerJpaRepository.findByContribuyenteAndRfcIgnoreCaseAndEliminadoFalse(taxpayerEmitter, customer.getRfc());
						if(UValidator.isNullOrEmpty(customerObj)){
							customerObj = iCustomerJpaRepository.findByContribuyenteAndRfcIgnoreCaseAndEliminadoTrue(taxpayerEmitter, customer.getRfc());
							if(!UValidator.isNullOrEmpty(customerObj)){
								persist = false;
							}else{
								persist = true;
							}
						}else{
							persist = false;
						}
						
						if(persist){
							persistenceDAO.persist(customer);
						}else{
							customerObj.setNombreRazonSocial(apiCfdi.getReceiver().getNombreRazonSocial().toUpperCase());
							if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getCorreoElectronico())){
								customerObj.setCorreoElectronico(apiCfdi.getReceiver().getCorreoElectronico());
							}
							if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getCurp())){
								customerObj.setCurp(apiCfdi.getReceiver().getCurp().toUpperCase());
							}
							customerObj.setResidente(apiCfdi.getReceiver().getResidente());
							if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getResidenciaFiscal())){
								customerObj.setPais(apiCfdi.getReceiver().getResidenciaFiscal());
							}
							if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getNumRegIdTrib())){
								customerObj.setNumeroRegistroIdentidadFiscal(apiCfdi.getReceiver().getNumRegIdTrib().toUpperCase());
							}
							customerObj.setEliminado(Boolean.FALSE);
							
							userTools.log("[INFO] UPDATING CUSTOMER IN CATALOG...");
							
							persistenceDAO.update(customerObj);
						}
					}
				}				
			}
			
			//###### Registrando los conceptos del comprobante
			List<ApiConcept> conceptList = new ArrayList<>();
			conceptList.addAll(apiCfdi.getConcepts());
			for (ApiConcept item : conceptList) {
				
				userTools.log("[INFO] REGISTERING CONCEPT...");
				
				ComprobanteConceptoEntity voucherConcept = new ComprobanteConceptoEntity();
				voucherConcept.setNoIdentificacion(item.getIdentificationNumber());
				voucherConcept.setDescripcion(item.getDescription());
				voucherConcept.setCantidad(UValue.doubleValue(item.getQuantity()));
				voucherConcept.setUnidadMedida(iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(item.getMeasurementUnitCode()));
				voucherConcept.setUnidad(item.getUnit());
				voucherConcept.setValorUnitario(UValue.doubleValue(item.getUnitValue()));
				voucherConcept.setImporte(UValue.doubleValue(item.getAmount()));
				if(item.getDiscount() != null){
					voucherConcept.setDescuento(UValue.doubleValue(item.getDiscount()));
				}
				voucherConcept.setClaveProductoServicio(iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(item.getKeyProductServiceCode()));
				persistenceDAO.persist(voucherConcept);
				
				userTools.log("[INFO] REGISTERING CONCEPT NODE...");
				
				//##### Registrando relacion de comprobante y conceptos
				ComprobanteNodoConceptoEntity comprobanteNodoConceptoEntity = new ComprobanteNodoConceptoEntity();
				comprobanteNodoConceptoEntity.setComprobante(voucher);
				comprobanteNodoConceptoEntity.setComprobanteConcepto(voucherConcept);
				persistenceDAO.persist(comprobanteNodoConceptoEntity);
				
				//##### Registrando informacion de los impuestos trasladados del concepto
				if(item.getTaxesTransferred() != null && !item.getTaxesTransferred().isEmpty()){
					
					userTools.log("[INFO] REGISTERING CONCEPT TRANSFERRED TAXES...");
					
					for (ApiConceptTaxes tax : item.getTaxesTransferred()) {
						TipoImpuestoEntity taxType = iTaxTypeJpaRepository.findByValor(tax.getTax());
						
						if(!UValidator.isNullOrEmpty(tax.getRateOrFeeBigDecimal())){
							BigDecimal rateOrFee = UValue.bigDecimalWithSixDecimals(tax.getRateOrFeeBigDecimal().toString());
							tax.setRateOrFeeBigDecimal(rateOrFee);
						}
						
						ConceptoTipoImpuestoEntity conceptTaxType = iConceptTaxType.findByCodigoAndEliminadoFalse("T");
						
						TipoFactorEntity factorType = iFactorTypeJpaRepository.findByValorAndEliminadoFalse(tax.getFactor());
						
						ComprobanteConceptoImpuestoEntity conceptTax = new ComprobanteConceptoImpuestoEntity();
						conceptTax.setComprobanteConcepto(voucherConcept);
						conceptTax.setConceptoTipoImpuesto(conceptTaxType);
						
						if(!UValidator.isNullOrEmpty(tax.getRateOrFeeBigDecimal())){
							conceptTax.setTasaCuota(UValue.doubleValue(tax.getRateOrFeeBigDecimal()));
						}
						
						conceptTax.setBase(UValue.doubleValue(tax.getBaseBigDecimal()));
						conceptTax.setTipoImpuesto(taxType);
						conceptTax.setTipoFactor(factorType);
						
						if(!UValidator.isNullOrEmpty(tax.getAmountBigDecimal())){
							conceptTax.setImporte(UValue.doubleValue(tax.getAmountBigDecimal()));
						}
						
						conceptTax.setEliminado(Boolean.FALSE);
						persistenceDAO.persist(conceptTax);
					}
				}
				
				//##### Registrando informacion de los impuestos retenidos del concepto
				if(item.getTaxesWithheld() != null && !item.getTaxesWithheld().isEmpty()){
					
					userTools.log("[INFO] REGISTERING CONCEPT WITHHELD TAXES...");
					
					for (ApiConceptTaxes tax : item.getTaxesWithheld()) {
						TipoImpuestoEntity taxType = iTaxTypeJpaRepository.findByCodigo(tax.getTax());
						
						BigDecimal rateOrFee = UValue.bigDecimalWithSixDecimals(tax.getRateOrFeeBigDecimal().toString());
						tax.setRateOrFeeBigDecimal(rateOrFee);
						
						ConceptoTipoImpuestoEntity conceptTaxType = iConceptTaxType.findByCodigoAndEliminadoFalse("R");
						
						TipoFactorEntity factorType = iFactorTypeJpaRepository.findByValorAndEliminadoFalse(tax.getFactor());
						
						ComprobanteConceptoImpuestoEntity conceptTax = new ComprobanteConceptoImpuestoEntity();
						conceptTax.setComprobanteConcepto(voucherConcept);
						conceptTax.setConceptoTipoImpuesto(conceptTaxType);
						conceptTax.setTasaCuota(UValue.doubleValue(tax.getRateOrFeeBigDecimal()));
						conceptTax.setBase(UValue.doubleValue(tax.getBaseBigDecimal()));
						conceptTax.setTipoImpuesto(taxType);
						conceptTax.setTipoFactor(factorType);
						conceptTax.setImporte(UValue.doubleValue(tax.getAmountBigDecimal()));
						conceptTax.setEliminado(Boolean.FALSE);
						persistenceDAO.persist(conceptTax);
					}
				}
				
				//##### Registrando informacion aduanera del concepto
				if(!item.getCustomsInformation().isEmpty()){
					userTools.log("[INFO] REGISTERING CONCEPT CUSTOMS INFORMATION...");
					
					for (InformacionAduanera ia : item.getCustomsInformation()) {
						ComprobanteConceptoInformacionAduaneraEntity customsInformation = new ComprobanteConceptoInformacionAduaneraEntity();
						customsInformation.setComprobanteConcepto(voucherConcept);
						customsInformation.setNumeroPedimento(ia.getNumeroPedimento());
						persistenceDAO.persist(customsInformation);
					}
				}
				
				//##### Registrando informacion de cuenta predial del concepto
				if(item.getPropertyAccount() != null){
					userTools.log("[INFO] REGISTERING CONCEPT PROPERTY ACCOUNT...");
					
					ComprobanteConceptoCuentaPredialEntity propertyAccount = new ComprobanteConceptoCuentaPredialEntity();
					propertyAccount.setComprobanteConcepto(voucherConcept);
					propertyAccount.setNumero(item.getPropertyAccount().getNumero());
					persistenceDAO.persist(propertyAccount);
				}
				
				//##### Registrando partes del concepto
				
				
				//##### Registrando/Actualizando concepto en catalogo
				if((conceptRegister || conceptUpdate) && !voucherWithComplement){
					
					ContribuyenteConceptoEntity concept = new ContribuyenteConceptoEntity();
					
					Long conceptSelected = item.getConceptSelected();
					if(!UValidator.isNullOrEmpty(conceptSelected)){
						concept = iTaxpayerConceptJpaRepository.findByIdAndEliminadoFalse(conceptSelected);
					}
					
					concept.setContribuyente(taxpayerEmitter);
					concept.setDescripcion(item.getDescription().toUpperCase());
					concept.setUnidadMedida(item.getMeasurementUnitEntity());
					concept.setClaveProductoServicio(item.getKeyProductServiceEntity());
					if(!UValidator.isNullOrEmpty(item.getIdentificationNumber())){
						concept.setNoIdentificacion(item.getIdentificationNumber().toUpperCase());
					}
					if(!UValidator.isNullOrEmpty(item.getUnit())){
						concept.setUnidad(item.getUnit());
					}
					concept.setCantidad(UValue.doubleValue("1"));
					concept.setValorUnitario(UValue.doubleValue(item.getUnitValue()));
					if(!UValidator.isNullOrEmpty(item.getDiscount())){
						concept.setDescuento(UValue.doubleValueStrict(UValue.bigDecimalString(item.getDiscount())));
					}
					concept.setImporte(UValue.doubleValue(item.getUnitValue()));
					concept.setEliminado(Boolean.FALSE);
					
					if(!UValidator.isNullOrEmpty(conceptSelected)){
						if(conceptRegister){
							userTools.log("[INFO] UPDATING CONCEPT IN CATALOG...");
							persistenceDAO.update(concept);
						}					
					}else{
						Boolean persist = false;
						Boolean update = false;
						ContribuyenteConceptoEntity conceptObj = null;
						
						//##### Determinando si el concepto existe (activo)
						conceptObj = iTaxpayerConceptJpaRepository.findByContribuyenteAndDescripcionIgnoreCaseAndEliminadoFalse(taxpayerEmitter, item.getDescription());
						if(UValidator.isNullOrEmpty(conceptObj)){
							//##### Determinando si el concepto existe (eliminado)
							conceptObj = iTaxpayerConceptJpaRepository.findByContribuyenteAndDescripcionIgnoreCaseAndEliminadoTrue(taxpayerEmitter, item.getDescription());
							if(!UValidator.isNullOrEmpty(conceptObj)){
								persist = false;
								update = true;
							}else{
								persist = true;
							}
						}else{
							persist = false;
						}
						
						//##### Determinando si la opcion de registrar conceptos esta habilitada
						if(conceptRegister){
							//##### Si el concepto existe (eliminado o no) no se registra
							if(persist){
								userTools.log("[INFO] REGISTERING CONCEPT IN CATALOG...");
								persistenceDAO.persist(concept);
							}
						}else if(conceptUpdate || update){
							//##### Determinando si la opcion de actualizar concepto esta habilitada
							//##### o si se quiere registrar un concepto que ya ha sido eliminado previamente, este se actualiza
							if(!UValidator.isNullOrEmpty(conceptObj)){
								conceptObj.setUnidadMedida(item.getMeasurementUnitEntity());
								conceptObj.setClaveProductoServicio(item.getKeyProductServiceEntity());
								if(!UValidator.isNullOrEmpty(item.getIdentificationNumber())){
									conceptObj.setNoIdentificacion(item.getIdentificationNumber().toUpperCase());
								}
								if(!UValidator.isNullOrEmpty(item.getUnit())){
									conceptObj.setUnidad(item.getUnit());
								}
								conceptObj.setCantidad(UValue.doubleValue("1"));
								conceptObj.setValorUnitario(UValue.doubleValue(item.getUnitValue()));
								if(!UValidator.isNullOrEmpty(item.getDiscount())){
									conceptObj.setDescuento(UValue.doubleValueStrict(UValue.bigDecimalString(item.getDiscount())));
								}
								conceptObj.setImporte(UValue.doubleValue(item.getUnitValue()));
								conceptObj.setEliminado(Boolean.FALSE);
								
								userTools.log("[INFO] UPDATING CONCEPT IN CATALOG...");
								
								persistenceDAO.update(conceptObj);
							}
						}
					}
				}
			}
			
			//##### Certificado del emisor
			if(UValidator.isNullOrEmpty(apiCfdi.getCertificate())){
				userTools.log("[INFO] REGISTERING EMITTER CERTIFICATE...");
				
				String rfc = taxpayerEmitter.getRfc();
				
				String emitterPath = appSettings.getPropertyValue("cfdi.cert.path");
				String filePath = emitterPath + File.separator + rfc;
				
				File fileFolder = new File(filePath); 
				if(!fileFolder.exists()){
					fileFolder.mkdirs();
				}
				
				// Certificado
				String cerFileName = rfc + ".cer";
				String cerPath = filePath + File.separator + cerFileName;
				UFile.writeFile(apiCfdi.getEmitter().getCertificado(), cerPath);
				
				// Llave privada
				String privateKeyFileName = rfc + ".key";
				String privateKeyPath = filePath + File.separator + privateKeyFileName;
				UFile.writeFile(apiCfdi.getEmitter().getLlavePrivada(), privateKeyPath);
				
				String certificateNumber = UCertificateX509.certificateSerialNumber(apiCfdi.getEmitter().getCertificado());
				
				ContribuyenteCertificadoEntity certificate = new ContribuyenteCertificadoEntity();
				certificate.setContribuyente(taxpayerEmitter);
				certificate.setClavePrivada(UBase64.base64Encode(apiCfdi.getEmitter().getPasswd()));
				certificate.setFechaCreacion(apiCfdi.getEmitter().getCertificateCreate());
				certificate.setFechaExpiracion(apiCfdi.getEmitter().getCertificateExpired());
				certificate.setHabilitado(true);
				certificate.setNumero(certificateNumber);
				certificate.setCertificado(apiCfdi.getEmitter().getCertificado());
				certificate.setRutaCertificado(cerPath);
				certificate.setLlavePrivada(apiCfdi.getEmitter().getLlavePrivada());
				certificate.setRutaLlavePrivada(privateKeyPath);
				persistenceDAO.persist(certificate);
			}
			
			//##### Regimen fiscal
			ContribuyenteRegimenFiscalEntity taxRegime = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(taxpayerEmitter);			
			if(taxRegimeRegister || taxRegimeUpdate){
				if(UValidator.isNullOrEmpty(taxRegime)){
					if(taxRegimeRegister){
						userTools.log("[INFO] REGISTERING TAX REGIME...");
						
						taxRegime = new ContribuyenteRegimenFiscalEntity();
						taxRegime.setContribuyente(taxpayerEmitter);
						taxRegime.setRegimenFiscal(apiCfdi.getEmitter().getRegimenFiscal());
						persistenceDAO.persist(taxRegime);
					}					
				}else{
					if(taxRegimeUpdate){
						userTools.log("[INFO] UPDATING TAX REGIME...");
						
						taxRegime.setRegimenFiscal(apiCfdi.getEmitter().getRegimenFiscal());
						persistenceDAO.update(taxRegime);
					}
				}
			}
			
			//##### Serie y folio
			if(!UValidator.isNullOrEmpty(apiCfdi.getSerieSheet())){
				userTools.log("[INFO] UPDATING SERIE AND SHEET...");
				
				ContribuyenteSerieFolioEntity serieSheet = apiCfdi.getSerieSheet();				
				Integer sheet = serieSheet.getFolioActual() + 1;
				serieSheet.setFolioActual(sheet);
				
				persistenceDAO.update(serieSheet);
			}
			
			//##### Lugar de expedicion
			if(taxpayerPostalCode){
				ContribuyenteLugarExpedicionEntity taxpayerPostalCodeEntity = iTaxpayerPostalCode.findByContribuyenteAndEliminadoFalse(taxpayerEmitter);
				if(UValidator.isNullOrEmpty(taxpayerPostalCodeEntity)){
					
					userTools.log("[INFO] REGISTERING TAXPAYER POSTAL CODE...");
					
					taxpayerPostalCodeEntity = new ContribuyenteLugarExpedicionEntity();
					taxpayerPostalCodeEntity.setContribuyente(taxpayerEmitter);
					taxpayerPostalCodeEntity.setCodigoPostal(apiCfdi.getPostalCode());
					taxpayerPostalCodeEntity.setEliminado(Boolean.FALSE);
					taxpayerPostalCodeEntity.setFechaUltimaModificacion(new Date());
					
					persistenceDAO.persist(taxpayerPostalCodeEntity);
				}else{
					userTools.log("[INFO] UPDATING TAXPAYER POSTAL CODE...");
					
					taxpayerPostalCodeEntity.setCodigoPostal(apiCfdi.getPostalCode());
					taxpayerPostalCodeEntity.setFechaUltimaModificacion(new Date());
					
					persistenceDAO.update(taxpayerPostalCodeEntity);
				}
			}
			
			//##### Registrando complementos
			if(voucherWithComplement){
				
				//##### Complemento de recepcion de pagos
				if(complementCode.equalsIgnoreCase(PaymentUtils.PAYMENT_PACKAGE)){
					ApiPayment apiPayment = apiCfdi.getComplement().getApiPayment();
					for (Payment item : apiPayment.getPayments()) {
						userTools.log("[INFO] REGISTERING PAYMENT COMPLEMENT...");
						
						ComplementoPagoEntity cmpPagoEntity = new ComplementoPagoEntity();
						cmpPagoEntity.setVersion(apiPayment.getVersion());
						cmpPagoEntity.setFechaPago(item.getPaymentDate());
						cmpPagoEntity.setFormaPago(iPaymentWayJpaRepository.findByCodigoAndEliminadoFalse(item.getPaymentWay().value()));
						cmpPagoEntity.setMoneda(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(item.getCurrency().value()));
						cmpPagoEntity.setMonto(UValue.doubleValue(item.getAmount()));
						
						if(!UValidator.isNullOrEmpty(item.getChangeType())){
							cmpPagoEntity.setTipoCambio(UValue.doubleValue(item.getChangeType()));
						}
						
						if(!UValidator.isNullOrEmpty(item.getOperationNumber())){
							cmpPagoEntity.setNumeroOperacion(item.getOperationNumber());
						}
						
						if(!UValidator.isNullOrEmpty(item.getSourceAccountRfc())){
							cmpPagoEntity.setRfcCuentaOrigen(item.getSourceAccountRfc());
						}
						
						if(!UValidator.isNullOrEmpty(item.getBankName())){
							cmpPagoEntity.setBancoOrdenante(item.getBankName());
						}
						
						if(!UValidator.isNullOrEmpty(item.getPayerAccount())){
							cmpPagoEntity.setCuentaOrdenante(item.getPayerAccount());
						}
						
						if(!UValidator.isNullOrEmpty(item.getTargetAccountRfc())){
							cmpPagoEntity.setRfcCuentaDestino(item.getTargetAccountRfc());
						}
						
						if(!UValidator.isNullOrEmpty(item.getReceiverAccount())){
							cmpPagoEntity.setCuentaBeneficiario(item.getReceiverAccount());
						}
						
						if(!UValidator.isNullOrEmpty(item.getStringType())){
							cmpPagoEntity.setTipoCadenaPago(iStringTypeJpaRepository.findByCodigoAndEliminadoFalse(UCatalog.getCode(item.getStringType())));
						}
						
						if(!UValidator.isNullOrEmpty(item.getOriginalString())){
							cmpPagoEntity.setCadenaOriginal(item.getOriginalString());
						}
						
						if(!UValidator.isNullOrEmpty(item.getPaymentCertificate())){
							cmpPagoEntity.setCertificadoPago(UValue.byteToString(item.getPaymentCertificate()));
						}
						
						if(!UValidator.isNullOrEmpty(item.getPaymentSeal())){
							cmpPagoEntity.setSelloPago(UValue.byteToString(item.getPaymentSeal()));
						}
						persistenceDAO.persist(cmpPagoEntity);
						
						item.getRelatedDocuments().forEach(document -> {
							userTools.log("[INFO] REGISTERING RELATED DOCUMENT ASOCIATED TO A PAYMENT COMPLEMENT...");
							
							val cmpPagoDocumentoRelacionadoEntity = new ComplementoPagoDocumentoRelacionadoEntity();
							cmpPagoDocumentoRelacionadoEntity.setComplementoPago(cmpPagoEntity);
							cmpPagoDocumentoRelacionadoEntity.setIdentificador(document.getDocumentId());
							cmpPagoDocumentoRelacionadoEntity.setMoneda(iCurrencyJpaRepository
									.findByCodigoAndEliminadoFalse(document.getCurrency().value()));
							if (!UValidator.isNullOrEmpty(document.getSerie())) {
								cmpPagoDocumentoRelacionadoEntity.setSerie(document.getSerie());
							}
							if (!UValidator.isNullOrEmpty(document.getSheet())) {
								cmpPagoDocumentoRelacionadoEntity.setFolio(document.getSheet());
							}
							if (!UValidator.isNullOrEmpty(document.getChangeType())) {
								cmpPagoDocumentoRelacionadoEntity
								.setTipoCambio(UValue.doubleValue(document.getChangeType()));
							}
							if (!UValidator.isNullOrEmpty(document.getPartiality())) {
								cmpPagoDocumentoRelacionadoEntity.setNumeroParcialidad(document.getPartiality());
							}
							if (!UValidator.isNullOrEmpty(document.getAmountPartialityBefore())) {
								cmpPagoDocumentoRelacionadoEntity.setMontoParcialidadAnterior(
										UValue.doubleValue(document.getAmountPartialityBefore()));
							}
							if (!UValidator.isNullOrEmpty(document.getAmountPaid())) {
								cmpPagoDocumentoRelacionadoEntity
								.setImportePagado(UValue.doubleValue(document.getAmountPaid()));
							}
							if (!UValidator.isNullOrEmpty(document.getDifference())) {
								cmpPagoDocumentoRelacionadoEntity
								.setImporteSaldoInsoluto(UValue.doubleValue(document.getDifference()));
							}
							cmpPagoDocumentoRelacionadoEntity.setObjetoImpuesto(document.getTaxObjectCode());
							persistenceDAO.persist(cmpPagoDocumentoRelacionadoEntity);
						});
						
						userTools.log("[INFO] REGISTERING RELATIONSHIP BETWEN VOUCHER AND PAYMENT COMPLEMENT...");
						
						//##### Registrando relacion de complemento y comprobante
						val comprobanteCmpPagoEntity = new ComprobanteComplementoPagoEntity();
						comprobanteCmpPagoEntity.setComprobante(voucher);
						comprobanteCmpPagoEntity.setComplementoPago(cmpPagoEntity);
						persistenceDAO.persist(comprobanteCmpPagoEntity);
					}
				}
			}
			
			userTools.log("[INFO] REGISTERING CFDI...");
			
			//##### Registrando el CFDI
			val cfdi = new CfdiEntity();
			cfdi.setContribuyente(taxpayerEmitter);
			cfdi.setEmisorRfc(taxpayerEmitter.getRfcActivo());
			cfdi.setReceptorRazonSocial(apiCfdi.getReceiver().getNombreRazonSocial().toUpperCase());
			cfdi.setReceptorRfc(apiCfdi.getReceiver().getRfc().toUpperCase());
			
			if (!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getCorreoElectronico())) {
				cfdi.setReceptorCorreoElectronico(apiCfdi.getReceiver().getCorreoElectronico());
			}
			
			if (!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getCurp())) {
				cfdi.setReceptorCurp(apiCfdi.getReceiver().getCurp().toUpperCase());
			}
			
			cfdi.setTipoComprobante(apiCfdi.getVoucher().getVoucherType().name());
			cfdi.setUuid(apiCfdi.getStampResponse().getUuid());
			cfdi.setFechaExpedicion(UDate.formattedDate(apiCfdi.getVoucher().getDateTimeExpedition()));
			cfdi.setFechaHoraExpedicion(apiCfdi.getVoucher().getDateTimeExpedition());
			cfdi.setFechaHoraTimbrado(apiCfdi.getVoucher().getDateTimeStamp());
			cfdi.setTotal(UValue.doubleValue(apiCfdi.getVoucher().getTotal()));
			
			if (!UValidator.isNullOrEmpty(apiCfdi.getVoucher().getSerie())) {
				cfdi.setSerie(apiCfdi.getVoucher().getSerie().toUpperCase());
			}
			cfdi.setFolio(apiCfdi.getVoucher().getFolio());
			
			cfdi.setRutaXml(apiCfdi.getXmlDbPath());
			cfdi.setRutaPdf(apiCfdi.getPdfDbPath());
			cfdi.setCancelado(Boolean.FALSE);			
			cfdi.setCancelacionEnProceso(Boolean.FALSE);
			cfdi.setSolicitudCancelacion(Boolean.FALSE);
			cfdi.setStorage(StorageType.SERVER);
			persistenceDAO.persist(cfdi);
			
			return cfdi;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ContribuyenteEntity taxpayerAccountStore(WAccount wAccount){
		ContribuyenteEntity taxpayerAsociated = null;
		try {
			
			if(!UValidator.isNullOrEmpty(wAccount)){
				if(wAccount.getAction().equals("config") && !wAccount.getAccountDeletedFinded()){
					userTools.log("[INFO] REGISTERING ASSOCICATED ACCOUNT...");
					
					// Registrando contribuyente (Asociado)
					taxpayerAsociated = (ContribuyenteEntity) persistenceDAO.persist(wAccount.getTaxpayerAsociated());
					
					userTools.log("[INFO] REGISTERING TAX REGIME...");
					
					// Registrando Contribuyente - Regimen Fiscal
					persistenceDAO.persist(wAccount.getTaxpayerTaxRegime());
					
					userTools.log("[INFO] REGISTERING PERSON TYPE...");
					
					// Registrando Contribuyente - Tipo persona
					persistenceDAO.persist(wAccount.getTaxpayerPersonType());
					
					// Registrando informacion del cer, key y passwd
					if(!UValidator.isNullOrEmpty(wAccount.getTaxpayerCertificate())){
						userTools.log("[INFO] REGISTERING CERTIFICATE...");
						
						persistenceDAO.persist(wAccount.getTaxpayerCertificate());
					}
					
					userTools.log("[INFO] REGISTERING ACCOUNT...");
					
					// Registrando cuenta
					persistenceDAO.persist(wAccount.getTaxpayerAccount());
					
					userTools.log("[INFO] REGISTERING PREFERENCES...");
					
					// Registrando preferencias
					persistenceDAO.persist(wAccount.getPreferences());
					
				}else if(wAccount.getAction().equals("edit") || wAccount.getAccountDeletedFinded()){

					userTools.log("[INFO] UPDATING ASSOCICATED ACCOUNT...");
					
					// Actualizando contribuyente (Asociado)
					taxpayerAsociated = (ContribuyenteEntity) persistenceDAO.update(wAccount.getTaxpayerAsociated());
					
					userTools.log("[INFO] UPDATING TAX REGIME...");
					
					// Actualizando Contribuyente - Regimen Fiscal
					persistenceDAO.update(wAccount.getTaxpayerTaxRegime());
					
					userTools.log("[INFO] UPDATING PERSON TYPE...");
					
					// Actualizando Contribuyente - Tipo persona
					persistenceDAO.update(wAccount.getTaxpayerPersonType());
					
					if(!UValidator.isNullOrEmpty(wAccount.getTaxpayerCertificate())){
						userTools.log("[INFO] UPDATING CERTIFICATE...");
					
						// Actualizando informacion del cer, key y passwd
						persistenceDAO.update(wAccount.getTaxpayerCertificate());
					}
					
					userTools.log("[INFO] UPDATING ACCOUNT...");
					
					// Actualizando cuenta
					persistenceDAO.update(wAccount.getTaxpayerAccount());
					
					if(!UValidator.isNullOrEmpty(wAccount.getPreferences())){
						userTools.log("[INFO] UPDATING PREFERENCES...");
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return taxpayerAsociated;
	}
	
	public PrefacturaEntity preInvoiceStore(ApiCfdi apiCfdi){
		try {
			
			//##### Determinando si el comprobante trae complemento
			val preInvoiceWithComplement = UValidator.isNullOrEmpty(apiCfdi.getComplement());
			String complementCode = null;
			if(preInvoiceWithComplement){
				complementCode = apiCfdi.getComplement().getPackageClass();
			}
			
			//##### Contribuyente emisor del comprobante
			ContribuyenteEntity taxpayerEmitter = iTaxpayerJpaRepository.findByRfc(apiCfdi.getEmitter().getRfc());
			
			userTools.log("[INFO] REGISTERING PREINVOICE...");
			
			//##### Registrando la prefactura
			PrefacturaEntity preInvoice = new PrefacturaEntity();
			preInvoice.setVersion(apiCfdi.getVoucher().getVersion());
			if(!UValidator.isNullOrEmpty(apiCfdi.getVoucher().getSerie())){
				preInvoice.setSerie(apiCfdi.getVoucher().getSerie().toUpperCase());
			}
			if(!UValidator.isNullOrEmpty(apiCfdi.getVoucher().getFolio())){
				preInvoice.setFolio(apiCfdi.getVoucher().getFolio());
			}
			preInvoice.setFechaExpedicion(new Date());
			preInvoice.setFechaHoraExpedicion(apiCfdi.getVoucher().getDateTimeExpedition());
			preInvoice.setTipoComprobante(apiCfdi.getVoucherType());
			preInvoice.setFormaPago(apiCfdi.getPaymentWay());
			preInvoice.setMetodoPago(apiCfdi.getPaymentMethod());
			preInvoice.setMoneda(apiCfdi.getCurrency());
			if(apiCfdi.getVoucher().getChangeType() != null){
				preInvoice.setTipoCambio(UValue.doubleValue(apiCfdi.getVoucher().getChangeType()));
			}
			preInvoice.setCondicionesPago(apiCfdi.getVoucher().getPaymentConditions());
			if(apiCfdi.getVoucher().getDiscount() != null){
				preInvoice.setDescuento(UValue.doubleValue(apiCfdi.getVoucher().getDiscount()));
			}
			preInvoice.setSubtotal(UValue.doubleValue(apiCfdi.getVoucher().getSubTotal()));
			preInvoice.setTotal(UValue.doubleValue(apiCfdi.getVoucher().getTotal()));
			preInvoice.setCodigoPostal(apiCfdi.getPostalCode());
			preInvoice.setClaveConfirmacionPac(apiCfdi.getVoucher().getConfirmation());
			if(!UValidator.isNullOrEmpty(apiCfdi.getLegend())){
				preInvoice.setLeyenda(apiCfdi.getLegend());
			}

			preInvoice.setContribuyente(taxpayerEmitter);
			preInvoice.setReceptorRazonSocial(apiCfdi.getReceiver().getNombreRazonSocial().toUpperCase());
			preInvoice.setReceptorRfc(apiCfdi.getReceiver().getRfc().toUpperCase());
			if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getCorreoElectronico())){
				preInvoice.setReceptorCorreoElectronico(apiCfdi.getReceiver().getCorreoElectronico());
			}
			if(!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getCurp())){
				preInvoice.setReceptorCurp(apiCfdi.getReceiver().getCurp().toUpperCase());
			}
			preInvoice.setRutaPdf(apiCfdi.getPdfPath());
			preInvoice.setEliminado(Boolean.FALSE);
			persistenceDAO.persist(preInvoice);
			
			//##### Registrando informacion de los cfdi relacionados de la prefactura
			if(apiCfdi.getVoucher().getRelatedCfdi() != null){
				
				userTools.log("[INFO] REGISTERING PREINVOICE RELATED CFDI...");
				
				PrefacturaCfdiRelacionadoEntity preInvoiceRelatedCfdi = new PrefacturaCfdiRelacionadoEntity();
				preInvoiceRelatedCfdi.setPrefactura(preInvoice);
				preInvoiceRelatedCfdi.setCfdiTipoRelacion(iCfdiRelationshipTypeJpaRepository.findByCodigoAndEliminadoFalse(apiCfdi.getVoucher().getRelatedCfdi().getRelationshipTypeCode()));
				persistenceDAO.persist(preInvoiceRelatedCfdi);
				
				for (RelatedCfdiUuid item : apiCfdi.getVoucher().getRelatedCfdi().getUuids()) {
					PrefacturaCfdiRelacionadoUuidEntity uuid = new PrefacturaCfdiRelacionadoUuidEntity();
					uuid.setPrefacturaCfdiRelacionado(preInvoiceRelatedCfdi);
					uuid.setUudi(item.getUuid());
					persistenceDAO.persist(uuid);
				}
			}
			
			userTools.log("[INFO] REGISTERING PREINVOICE EMITTER NODE...");
			
			//##### Registrando relacion de la prefactura y el emisor
			PrefacturaNodoEmisorEntity preInvoiceEmitterNode = new PrefacturaNodoEmisorEntity();
			preInvoiceEmitterNode.setPrefactura(preInvoice);
			preInvoiceEmitterNode.setNombreRazonSocial(apiCfdi.getEmitter().getNombreRazonSocial());
			preInvoiceEmitterNode.setRfc(apiCfdi.getEmitter().getRfc());
			preInvoiceEmitterNode.setRegimenFiscal(apiCfdi.getEmitter().getRegimenFiscal());
			persistenceDAO.persist(preInvoiceEmitterNode);
			
			userTools.log("[INFO] REGISTERING PREINVOICE RECEIVER NODE...");
			
			//##### Registrando relacion de la prefactura y el receptor
			PrefacturaNodoReceptorEntity preInvoiceReceiverNode = new PrefacturaNodoReceptorEntity();
			preInvoiceReceiverNode.setPrefactura(preInvoice);
			preInvoiceReceiverNode.setNombreRazonSocial(apiCfdi.getReceiver().getNombreRazonSocial());
			preInvoiceReceiverNode.setRfc(apiCfdi.getReceiver().getRfc());
			preInvoiceReceiverNode.setNumeroRegistroIdentidadFiscal(apiCfdi.getReceiver().getNumRegIdTrib());
			preInvoiceReceiverNode.setPais(apiCfdi.getReceiver().getResidenciaFiscal());
			preInvoiceReceiverNode.setUsoCfdi(apiCfdi.getReceiver().getUsoCFDI());
			preInvoiceReceiverNode.setCorreoElectronico(apiCfdi.getReceiver().getCorreoElectronico());
			persistenceDAO.persist(preInvoiceReceiverNode);
			
			//###### Registrando los conceptos de la prefactura
			List<ApiConcept> conceptList = new ArrayList<>();
			conceptList.addAll(apiCfdi.getConcepts());
			for (ApiConcept item : conceptList) {
				
				userTools.log("[INFO] PREINVOICE REGISTERING CONCEPT...");
				
				PrefacturaConceptoEntity preInvoiceConcept = new PrefacturaConceptoEntity();
				
				if(!UValidator.isNullOrEmpty(item.getIdentificationNumber())){
					preInvoiceConcept.setNoIdentificacion(item.getIdentificationNumber());
				}
				
				preInvoiceConcept.setDescripcion(item.getDescription());
				preInvoiceConcept.setCantidad(UValue.doubleValue(item.getQuantity()));
				preInvoiceConcept.setUnidadMedida(iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(item.getMeasurementUnitCode()));
				
				if(!UValidator.isNullOrEmpty(item.getUnit())){
					preInvoiceConcept.setUnidad(item.getUnit());
				}
				
				preInvoiceConcept.setValorUnitario(UValue.doubleValue(item.getUnitValue()));
				preInvoiceConcept.setImporte(UValue.doubleValue(item.getAmount()));
				if(item.getDiscount() != null){
					preInvoiceConcept.setDescuento(UValue.doubleValue(item.getDiscount()));
				}
				preInvoiceConcept.setClaveProductoServicio(iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(item.getKeyProductServiceCode()));
				persistenceDAO.persist(preInvoiceConcept);
				
				userTools.log("[INFO] REGISTERING PREINVOICE CONCEPT NODE...");
				
				//##### Registrando relacion de comprobante y conceptos
				PrefacturaNodoConceptoEntity preInvoiceNodoConceptoEntity = new PrefacturaNodoConceptoEntity();
				preInvoiceNodoConceptoEntity.setPrefactura(preInvoice);
				preInvoiceNodoConceptoEntity.setPrefacturaConcepto(preInvoiceConcept);
				persistenceDAO.persist(preInvoiceNodoConceptoEntity);
				
				//##### Registrando informacion de los impuestos trasladados del concepto
				if(item.getTaxesTransferred() != null && !item.getTaxesTransferred().isEmpty()){
					userTools.log("[INFO] REGISTERING PREINVOICE CONCEPT TRANSFERRED TAXES...");
					
					for (ApiConceptTaxes tax : item.getTaxesTransferred()) {
						String taxValue = UCatalog.getValue(tax.getTax());
						TipoImpuestoEntity taxType = iTaxTypeJpaRepository.findByValor(taxValue);
						
						if(!UValidator.isNullOrEmpty(tax.getRateOrFeeBigDecimal())){
							BigDecimal rateOrFee = UValue.bigDecimalWithSixDecimals(tax.getRateOrFeeBigDecimal().toString());
							tax.setRateOrFeeBigDecimal(rateOrFee);
						}
						
						ConceptoTipoImpuestoEntity conceptTaxType = iConceptTaxType.findByCodigoAndEliminadoFalse("T");
						
						TipoFactorEntity factorType = iFactorTypeJpaRepository.findByValorAndEliminadoFalse(tax.getFactor());
						
						PrefacturaConceptoImpuestoEntity conceptTax = new PrefacturaConceptoImpuestoEntity();
						conceptTax.setPrefacturaConcepto(preInvoiceConcept);
						conceptTax.setConceptoTipoImpuesto(conceptTaxType);
						
						if(!UValidator.isNullOrEmpty(tax.getRateOrFeeBigDecimal())){
							conceptTax.setTasaCuota(UValue.doubleValue(tax.getRateOrFeeBigDecimal()));
						}
						
						conceptTax.setBase(UValue.doubleValue(tax.getBaseBigDecimal()));
						conceptTax.setTipoImpuesto(taxType);
						conceptTax.setTipoFactor(factorType);
						
						if(!UValidator.isNullOrEmpty(tax.getAmountBigDecimal())){
							conceptTax.setImporte(UValue.doubleValue(tax.getAmountBigDecimal()));
						}
						
						conceptTax.setEliminado(Boolean.FALSE);
						persistenceDAO.persist(conceptTax);
					}
				}
				
				//##### Registrando informacion de los impuestos retenidos del concepto
				if(item.getTaxesWithheld() != null && !item.getTaxesWithheld().isEmpty()){
					userTools.log("[INFO] REGISTERING PREINVOICE CONCEPT WITHHELD TAXES...");
					
					for (ApiConceptTaxes tax : item.getTaxesWithheld()) {
						TipoImpuestoEntity taxType = iTaxTypeJpaRepository.findByCodigo(tax.getTax());
						
						BigDecimal rateOrFee = UValue.bigDecimalWithSixDecimals(tax.getRateOrFeeBigDecimal().toString());
						tax.setRateOrFeeBigDecimal(rateOrFee);
						
						ConceptoTipoImpuestoEntity conceptTaxType = iConceptTaxType.findByCodigoAndEliminadoFalse("R");
						
						TipoFactorEntity factorType = iFactorTypeJpaRepository.findByValorAndEliminadoFalse(tax.getFactor());
						
						PrefacturaConceptoImpuestoEntity conceptTax = new PrefacturaConceptoImpuestoEntity();
						conceptTax.setPrefacturaConcepto(preInvoiceConcept);
						conceptTax.setConceptoTipoImpuesto(conceptTaxType);
						conceptTax.setTasaCuota(UValue.doubleValue(tax.getRateOrFeeBigDecimal()));
						conceptTax.setBase(UValue.doubleValue(tax.getBaseBigDecimal()));
						conceptTax.setTipoImpuesto(taxType);
						conceptTax.setTipoFactor(factorType);
						conceptTax.setImporte(UValue.doubleValue(tax.getAmountBigDecimal()));
						conceptTax.setEliminado(Boolean.FALSE);
						persistenceDAO.persist(conceptTax);
					}
				}
				
				//##### Registrando informacion aduanera del concepto
				if(item.getCustomsInformation() != null){
					userTools.log("[INFO] REGISTERING PREINVOICE CONCEPT CUSTOMS INFORMATION...");
					
					for (InformacionAduanera ia : item.getCustomsInformation()) {
						PrefacturaConceptoInformacionAduaneraEntity customsInformation = new PrefacturaConceptoInformacionAduaneraEntity();
						customsInformation.setPrefacturaConcepto(preInvoiceConcept);
						customsInformation.setNumeroPedimento(ia.getNumeroPedimento());
						persistenceDAO.persist(customsInformation);
					}
				}
				
				//##### Registrando informacion de cuenta predial del concepto
				if(item.getPropertyAccount() != null){
					userTools.log("[INFO] REGISTERING PREINVOICE CONCEPT PROPERTY ACCOUNT...");
					
					PrefacturaConceptoCuentaPredialEntity propertyAccount = new PrefacturaConceptoCuentaPredialEntity();
					propertyAccount.setPrefacturaConcepto(preInvoiceConcept);
					propertyAccount.setNumero(item.getPropertyAccount().getNumero());
					persistenceDAO.persist(propertyAccount);
				}
			}
			
			//##### Registrando complementos
			if(preInvoiceWithComplement){
				//##### Complemento de recepcion de pagos
				if(complementCode.equalsIgnoreCase(PaymentUtils.PAYMENT_PACKAGE)){
					ApiPayment apiPayment = apiCfdi.getComplement().getApiPayment();
					for (Payment item : apiPayment.getPayments()) {
						userTools.log("[INFO] REGISTERING PREINVOICE PAYMENT COMPLEMENT...");
						
						PrefacturaComplementoPagoEntity cmpPagoEntity = new PrefacturaComplementoPagoEntity();
						cmpPagoEntity.setVersion(apiPayment.getVersion());
						cmpPagoEntity.setFechaPago(item.getPaymentDate());
						cmpPagoEntity.setFormaPago(iPaymentWayJpaRepository.findByCodigoAndEliminadoFalse(item.getPaymentWay().value()));
						cmpPagoEntity.setMoneda(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(item.getCurrency().value()));
						cmpPagoEntity.setMonto(UValue.doubleValue(item.getAmount()));
						
						if(!UValidator.isNullOrEmpty(item.getChangeType())){
							cmpPagoEntity.setTipoCambio(UValue.doubleValue(item.getChangeType()));
						}
						
						if(!UValidator.isNullOrEmpty(item.getOperationNumber())){
							cmpPagoEntity.setNumeroOperacion(item.getOperationNumber());
						}
						
						if(!UValidator.isNullOrEmpty(item.getSourceAccountRfc())){
							cmpPagoEntity.setRfcCuentaOrigen(item.getSourceAccountRfc());
						}
						
						if(!UValidator.isNullOrEmpty(item.getBankName())){
							cmpPagoEntity.setBancoOrdenante(item.getBankName());
						}
						
						if(!UValidator.isNullOrEmpty(item.getPayerAccount())){
							cmpPagoEntity.setCuentaOrdenante(item.getPayerAccount());
						}
						
						if(!UValidator.isNullOrEmpty(item.getTargetAccountRfc())){
							cmpPagoEntity.setRfcCuentaDestino(item.getTargetAccountRfc());
						}
						
						if(!UValidator.isNullOrEmpty(item.getReceiverAccount())){
							cmpPagoEntity.setCuentaBeneficiario(item.getReceiverAccount());
						}
						
						if(!UValidator.isNullOrEmpty(item.getStringType())){
							cmpPagoEntity.setTipoCadenaPago(iStringTypeJpaRepository.findByCodigoAndEliminadoFalse(UCatalog.getCode(item.getStringType())));
						}
						
						if(!UValidator.isNullOrEmpty(item.getOriginalString())){
							cmpPagoEntity.setCadenaOriginal(item.getOriginalString());
						}
						
						if(!UValidator.isNullOrEmpty(item.getPaymentCertificate())){
							cmpPagoEntity.setCertificadoPago(UValue.byteToString(item.getPaymentCertificate()));
						}
						
						if(!UValidator.isNullOrEmpty(item.getPaymentSeal())){
							cmpPagoEntity.setSelloPago(UValue.byteToString(item.getPaymentSeal()));
						}
						persistenceDAO.persist(cmpPagoEntity);
						
						item.getRelatedDocuments().forEach(document -> {
							userTools.log("[INFO] REGISTERING PREINVOICE RELATED DOCUMENT ASOCIATED TO A PAYMENT COMPLEMENT...");
							
							val cmpPagoDocumentoRelacionadoEntity = new PrefacturaComplementoPagoDocumentoRelacionadoEntity();
							cmpPagoDocumentoRelacionadoEntity.setPrefacturaComplementoPago(cmpPagoEntity);
							cmpPagoDocumentoRelacionadoEntity.setIdentificador(document.getDocumentId());
							cmpPagoDocumentoRelacionadoEntity.setMoneda(iCurrencyJpaRepository
									.findByCodigoAndEliminadoFalse(document.getCurrency().value()));
							
							if (!UValidator.isNullOrEmpty(document.getSerie())) {
								cmpPagoDocumentoRelacionadoEntity.setSerie(document.getSerie());
							}
							if (!UValidator.isNullOrEmpty(document.getSheet())) {
								cmpPagoDocumentoRelacionadoEntity.setFolio(document.getSheet());
							}
							if (!UValidator.isNullOrEmpty(document.getChangeType())) {
								cmpPagoDocumentoRelacionadoEntity
								.setTipoCambio(UValue.doubleValue(document.getChangeType()));
							}
							if (!UValidator.isNullOrEmpty(document.getPartiality())) {
								cmpPagoDocumentoRelacionadoEntity.setNumeroParcialidad(document.getPartiality());
							}
							if (!UValidator.isNullOrEmpty(document.getAmountPartialityBefore())) {
								cmpPagoDocumentoRelacionadoEntity.setMontoParcialidadAnterior(
										UValue.doubleValue(document.getAmountPartialityBefore()));
							}
							if (!UValidator.isNullOrEmpty(document.getAmountPaid())) {
								cmpPagoDocumentoRelacionadoEntity
								.setImportePagado(UValue.doubleValue(document.getAmountPaid()));
							}
							if (!UValidator.isNullOrEmpty(document.getDifference())) {
								cmpPagoDocumentoRelacionadoEntity
								.setImporteSaldoInsoluto(UValue.doubleValue(document.getDifference()));
							}
							cmpPagoDocumentoRelacionadoEntity.setObjetoImpuesto(document.getTaxObjectCode());
							persistenceDAO.persist(cmpPagoDocumentoRelacionadoEntity);
						});
						
						userTools.log("[INFO] REGISTERING RELATIONSHIP BETWEN PREINVOICE AND PAYMENT COMPLEMENT...");
						
						//##### Registrando relacion de complemento y prefactura
						val comprobanteCmpPagoEntity = new PrefacturaComplementoPagoEntity();
						comprobanteCmpPagoEntity.setPrefactura(preInvoice);
						persistenceDAO.persist(comprobanteCmpPagoEntity);	
					}
				}
			}
			
			return preInvoice;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ContribuyenteConceptoEntity taxpayerConceptStore(WConcept wConcept){
		ContribuyenteConceptoEntity taxpayerConcept = null;
		String print = null;
		try {
			
			if(!UValidator.isNullOrEmpty(wConcept)){
				taxpayerConcept = wConcept.getTaxpayerConcept();
				if(wConcept.getAction().equals("config") && !wConcept.getConceptDeletedFinded()){

					userTools.log("[INFO] REGISTERING NEW CONCEPT...");
				
					taxpayerConcept = (ContribuyenteConceptoEntity) persistenceDAO.persist(taxpayerConcept);
					
					print = "[INFO] CONCEPT REGISTERED SUCCESSFULY";
				} else if(wConcept.getAction().equals("edit") || wConcept.getConceptDeletedFinded()) {
					userTools.log("[INFO] UPDATING CONCEPT...");
				
					taxpayerConcept = (ContribuyenteConceptoEntity) persistenceDAO.update(taxpayerConcept);
					
					print = "[INFO] CONCEPT UPDATED SUCCESSFULY";
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			print = "[ERROR] UNEXPECTED ERROR ATTEMPTING TO CREATE OR UPDATE A CONCEPT";
		}
		
		if (!UValidator.isNullOrEmpty(print)) {
			userTools.log(print);
		}
		
		return taxpayerConcept;
	}
	
	public ClienteEntity customerStore(WCustomer wCustomer){
		ClienteEntity customer = null;
		String print = null;
		try {
			
			if(!UValidator.isNullOrEmpty(wCustomer)){
				if(wCustomer.getAction().equals("config") && !wCustomer.getCustomerDeletedFinded()){
					userTools.log("[INFO] REGISTERING NEW CUSTOMER...");
					
					customer = (ClienteEntity) persistenceDAO.persist(wCustomer.getCustomer());
					
					print = "[INFO] CUSTOMER REGISTERED SUCCESSFULY";
				}else if(wCustomer.getAction().equals("edit") || wCustomer.getCustomerDeletedFinded()){
					
					userTools.log("[INFO] UPDATING CUSTOMER...");
					
					customer = (ClienteEntity) persistenceDAO.update(wCustomer.getCustomer());
					
					print = "[INFO] CUSTOMER UPDATED SUCCESSFULY";
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			print = "[ERROR] UNEXPECTED ERROR ATTEMPTING TO CREATE OR UPDATE A CUSTOMER";
		}
		
		if (!UValidator.isNullOrEmpty(print)) {
			userTools.log(print);
		}
		
		return customer;
	}
	
	public ContactoEntity contactStore(WContact wContact){
		ContactoEntity contact = null;
		String print = null;
		try {
			
			if(!UValidator.isNullOrEmpty(wContact)){
				if(wContact.getAction().equals("config") && !wContact.getContactDeletedFinded()){
					userTools.log("[INFO] REGISTERING NEW CONTACT...");
					
					contact = (ContactoEntity) persistenceDAO.persist(wContact.getContact());
					
					print = "[INFO] CONTACT REGISTERED SUCCESSFULY";
				}else if(wContact.getAction().equals("edit") || wContact.getContactDeletedFinded()){
					userTools.log("[INFO] UPDATING CONTACT...");
					
					contact = (ContactoEntity) persistenceDAO.update(wContact.getContact());
					
					print = "[INFO] CONTACT UPDATED SUCCESSFULY";
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			print = "[ERROR] UNEXPECTED ERROR ATTEMPTING TO CREATE OR UPDATE A CONTACT";
		}
		
		if(!UValidator.isNullOrEmpty(print)){
			userTools.log(print);
		}
		
		return contact;
	}
	
	public ContribuyenteSerieFolioEntity serieSheetStore(WSerieSheet wSerieSheet){
		ContribuyenteSerieFolioEntity serieSheet = null;
		String print = null;
		try {
			
			if(!UValidator.isNullOrEmpty(wSerieSheet)){
				if(wSerieSheet.getAction().equals("config") && !wSerieSheet.getSerieSheetDeletedFinded()){
					userTools.log("[INFO] REGISTERING NEW SERIE SHEET...");
					
					serieSheet = (ContribuyenteSerieFolioEntity) persistenceDAO.persist(wSerieSheet.getSerieSheet());
					
					print = "[INFO] SERIE SHEET REGISTERED SUCCESSFULY";
				}else if(wSerieSheet.getAction().equals("edit") || wSerieSheet.getSerieSheetDeletedFinded()){
					userTools.log("[INFO] UPDATING SERIE SHEET...");
					
					serieSheet = (ContribuyenteSerieFolioEntity) persistenceDAO.update(wSerieSheet.getSerieSheet());
					
					print = "[INFO] SERIE SHEET UPDATED SUCCESSFULY";
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			print = "[ERROR] UNEXPECTED ERROR ATTEMPTING TO CREATE OR UPDATE A SERIE SHEET";
		}
		
		if(!UValidator.isNullOrEmpty(print)){
			userTools.log(print);
		}
		
		return serieSheet;
	}
	
	public ArchivoNominaConfiguracionEntity payrollFileConfigurationStore(ApiPayrollFileConfiguration apiPayrollFileConfiguration){
		
		ArchivoNominaConfiguracionEntity payrollFileConfiguration = null;
		String print = null;
		
		try {
			
			if(!UValidator.isNullOrEmpty(apiPayrollFileConfiguration)){
				if(apiPayrollFileConfiguration.getAction().equals("config")){
					userTools.log("[INFO] REGISTERING PAYROLL FILE CONFIGURATION...");
					
					payrollFileConfiguration = (ArchivoNominaConfiguracionEntity) persistenceDAO.persist(apiPayrollFileConfiguration.getPayrollFileConfiguration());
					
					print = "[INFO] PAYROLL FILE CONFIGURATION REGISTERED SUCCESSFULY";
				}else if(apiPayrollFileConfiguration.getAction().equals("edit")){
					userTools.log("[INFO] UPDATING PAYROLL FILE CONFIGURATION...");
					
					payrollFileConfiguration = (ArchivoNominaConfiguracionEntity) persistenceDAO.update(apiPayrollFileConfiguration.getPayrollFileConfiguration());
					
					print = "[INFO] PAYROLL FILE CONFIGURATION UPDATED SUCCESSFULY";
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			print = "[ERROR] UNEXPECTED ERROR ATTEMPTING TO CREATE OR UPDATE A PAYROLL FILE CONFIGURATION";
		}
		
		if(!UValidator.isNullOrEmpty(print)){
			userTools.log(print);
		}
		
		return payrollFileConfiguration;
	}
	
	public ArchivoRetencionConfiguracionEntity retentionFileConfigurationStore(ApiRetentionFileConfiguration apiRetentionFileConfiguration){
		
		ArchivoRetencionConfiguracionEntity retentionFileConfiguration = null;
		String print = null;
		
		try {
			
			if(!UValidator.isNullOrEmpty(apiRetentionFileConfiguration)){
				if(apiRetentionFileConfiguration.getAction().equals("config")){
					userTools.log("[INFO] REGISTERING RETENTION FILE CONFIGURATION...");
					
					retentionFileConfiguration = (ArchivoRetencionConfiguracionEntity) persistenceDAO.persist(apiRetentionFileConfiguration.getRetentionFileConfiguration());
					
					print = "[INFO] RETENTION FILE CONFIGURATION REGISTERED SUCCESSFULY";
				}else if(apiRetentionFileConfiguration.getAction().equals("edit")){
					userTools.log("[INFO] UPDATING RETENTION FILE CONFIGURATION...");
					
					retentionFileConfiguration = (ArchivoRetencionConfiguracionEntity) persistenceDAO.update(apiRetentionFileConfiguration.getRetentionFileConfiguration());
					
					print = "[INFO] RETENTION FILE CONFIGURATION UPDATED SUCCESSFULY";
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			print = "[ERROR] UNEXPECTED ERROR ATTEMPTING TO CREATE OR UPDATE A RETENTION FILE CONFIGURATION";
		}
		
		if(!UValidator.isNullOrEmpty(print)){
			userTools.log(print);
		}
		
		return retentionFileConfiguration;
	}
	
	public RetencionEntity retentionStore(final WRetention wRetention) {
		if(!UValidator.isNullOrEmpty(wRetention)) {
			final ApiRetention apiRetention = wRetention.getApiRetention();
			RetencionEntity retention = new RetencionEntity();
			retention.setContribuyente(wRetention.getTaxpayer());
			
			String receiverBusinessName = apiRetention.getReceiverNationality().equalsIgnoreCase("nacional")
					? apiRetention.getReceiverNatBusinessName()
					: apiRetention.getReceiverExtBusinessName();
			retention.setReceptorRazonSocial(receiverBusinessName);
			
			retention.setReceptorRfc(apiRetention.getReceiverNatRfc());
			retention.setReceptorCurp(apiRetention.getReceiverNatCurp());
			retention.setReceptorNumeroIdentificacion(apiRetention.getReceiverExtNumber());
			retention.setReceptorCorreoElectronico(apiRetention.getReceiverEmail());
			retention.setUuid(apiRetention.getStampResponse().getUuid());
			retention.setFechaHoraExpedicion(apiRetention.getStampResponse().getDateExpedition());
			retention.setFechaHoraTimbrado(apiRetention.getStampResponse().getDateStamp());
			retention.setFechaExpedicion(UDate.formattedDate(apiRetention.getStampResponse().getDateExpedition()));
			retention.setClaveRetencion(apiRetention.getRetentionKey());
			
			RetencionComplementoEntity retComplement = iRetentionComplementJpaRepository.findByCodigoAndEliminadoFalse(apiRetention.getRetentionKey());
			if(!UValidator.isNullOrEmpty(retComplement)) {
				retention.setComplemento(retComplement.getValor());
			}
			
			retention.setPeriodoMesInicial(UValue.integerValue(apiRetention.getInitialMonth()));
			retention.setPeriodoMesFinal(UValue.integerValue(apiRetention.getEndMonth()));
			retention.setPeriodoEjercicio(UValue.integerValue(apiRetention.getYear()));
			retention.setFolio(apiRetention.getFolio());
			retention.setRutaXml(apiRetention.getXmlPath());
			retention.setRutaPdf(apiRetention.getPdfPath());
			retention.setCancelado(Boolean.FALSE);
			
			retention = iRetentionJpaRepository.save(retention);
			return retention;
		}
		return null;
	}
	
}