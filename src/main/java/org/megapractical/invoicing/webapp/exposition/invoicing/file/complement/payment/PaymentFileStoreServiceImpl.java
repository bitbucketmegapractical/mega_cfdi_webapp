package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.InvoiceStoreRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.InvoiceStoreService;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.PaymentUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
@Scope("session")
public class PaymentFileStoreServiceImpl implements PaymentFileStoreService {

	private final UserTools userTools;
	private final DataStoreService dataStoreService;
	private final InvoiceStoreService invoiceStoreService;
	
	public PaymentFileStoreServiceImpl(UserTools userTools, DataStoreService dataStoreService,
			InvoiceStoreService invoiceStoreService) {
		this.userTools = userTools;
		this.dataStoreService = dataStoreService;
		this.invoiceStoreService = invoiceStoreService;
	}

	@Override
	public boolean store(ContribuyenteEntity taxpayer, ApiCfdi apiCfdi) {
		InvoiceStoreRequest storeRequest = InvoiceStoreRequest
											.builder()
											.taxpayer(taxpayer)
											.emitterRfc(taxpayer.getRfcActivo())
											.receiverRfc(apiCfdi.getReceiver().getRfc())
											.receiverName(apiCfdi.getReceiver().getNombreRazonSocial())
											.receiverEmail(apiCfdi.getReceiver().getCorreoElectronico())
											.uuid(apiCfdi.getVoucher().getTaxSheet())
											.apiCfdi(apiCfdi)
											.preferences(userTools.getPreferences(taxpayer))
											.complementCode(PaymentUtils.PAYMENT_PACKAGE)
											.build();
		return store(storeRequest);
	}
	
	private boolean store(InvoiceStoreRequest storeRequest) {
		val apiCfdi = storeRequest.getApiCfdi();

		val cfdiStored = dataStoreService.cfdiStore(apiCfdi);
		if (!UValidator.isNullOrEmpty(cfdiStored)) {
			// Storing XML
			UFile.writeFile(apiCfdi.getStampResponse().getXml(), apiCfdi.getXmlPath());
			// Generating and Storing PDF
			invoiceStoreService.generateAndStorePdf(apiCfdi, apiCfdi.getPdfPath(), PaymentUtils.PAYMENT_PACKAGE);
			
			return true;
		}
		return false;
	}

}