package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.BaseTax;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.PaymentWithheld;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.RelatedDocument;
import org.megapractical.invoicing.api.wrapper.ApiPayment.PaymentTotal;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service.CurrencyService;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.PaymentTotalsResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.RelatedDocumentResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.StringTypeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.WPaymentRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentAmountValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentBankValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentCertificateValidation;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentChangeTypeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentCurrencyValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentDateValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentOriginalStringValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentPayerAccountValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentPaymWayValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentReceiverAccountValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentSealValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentSourceAccountRfcValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentStringTypeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentTargetAccountRfcValidator;
import org.megapractical.invoicing.webapp.exposition.service.WebCatalogService;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JComplementPayment;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentBaseTax;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentRelatedDocument;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentWithheld;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPaymentTotal;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.PaymentUtils;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment.WRelatedDocument;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPaymentTotal;
import org.megapractical.invoicing.webapp.wrapper.WTaxes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private WebCatalogService webCatalog;

	@Autowired
	private UserTools userTools;
	
	@Autowired
	private PaymentDateValidator paymentDateValidator;
	
	@Autowired
	private PaymentPaymWayValidator paymentPaymWayValidator;
	
	@Autowired
	private CurrencyService currencyService;
	
	@Autowired
	private PaymentCurrencyValidator paymentCurrencyValidator;
	
	@Autowired
	private PaymentChangeTypeValidator paymentChangeTypeValidator;
	
	@Autowired
	private PaymentAmountValidator paymentAmountValidator;
	
	@Autowired
	private PaymentSourceAccountRfcValidator paymentSourceAccountRfcValidator;
	
	@Autowired
	private PaymentBankValidator paymentBankValidator;
	
	@Autowired
	private PaymentPayerAccountValidator paymentPayerAccountValidator;
	
	@Autowired
	private PaymentTargetAccountRfcValidator paymentTargetAccountRfcValidator;
	
	@Autowired
	private PaymentReceiverAccountValidator paymentReceiverAccountValidator;
	
	@Autowired
	private PaymentStringTypeValidator paymentStringTypeValidator;

	@Autowired
	private PaymentCertificateValidation paymentCertificateValidation;
	
	@Autowired
	private PaymentOriginalStringValidator paymentOriginalStringValidator;
	
	@Autowired
	private PaymentSealValidator paymentSealValidator;

	private static final String EMPTY_STRING = "";

	@Override
	public boolean isPayment(String packageClass) {
		return PaymentUtils.PAYMENT_PACKAGE.equalsIgnoreCase(packageClass);
	}

	@Override
	public ApiPayment getPayment(WPaymentTotal wPaymentTotal, List<WPayment> payments) {
		val paymentTotal = mapPaymentTotal(wPaymentTotal);
		val apiCmpPayment = new ApiPayment(paymentTotal);
		
		var paymentNumber = 1;
		for (val item : payments) {
			val relatedDocuments = getRelatedDocuments(item, paymentNumber);

			val payment = populatePayment(item, paymentNumber);
			payment.getRelatedDocuments().addAll(relatedDocuments);

			apiCmpPayment.getPayments().add(payment);
			paymentNumber ++;
		}
		return apiCmpPayment;
	}
	
	@Override
	public JComplementPayment loadPaymentData(List<WPayment> payments, Integer index) {
		JComplementPayment cmpPaymentJson = new JComplementPayment();
		JResponse response = null;

		try {

			val wPayment = payments.get(index);
			val cmpPayment = getComplementPayment(wPayment);
			val relatedDocuments = getRelatedDocuments(payments, index);

			cmpPayment.getRelatedDocuments().addAll(relatedDocuments);
			cmpPaymentJson.setComplementPayment(cmpPayment);

			response = JResponse.success();

		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();

			val notification = JNotification.pageMessageError(UProperties.getMessage("ERR0149", userTools.getLocale()));
			response = JResponse.error(notification);
		}

		cmpPaymentJson.setResponse(response);
		return cmpPaymentJson;
	}

	@Override
	public JComplementPayment addOrEditPayment(String packageClass,
											   String paymentData, 
											   boolean isEdit, 
											   Integer index,
											   List<WPayment> payments) {
		JResponse response = null;
		val errors = new ArrayList<JError>();
		if (packageClass.equalsIgnoreCase(PaymentUtils.PAYMENT_PACKAGE)) {
			val payment = GsonClient.deserialize(paymentData, ComplementPayment.class);
			
			// Feha y hora del pago
			val paymentDate = getPaymentDate(payment, errors);
			// Forma de pago
			val cmpPaymentWay = getPaymentWay(payment, errors);
			// Moneda
			val cmpPaymentCurrency = getCurrency(payment, errors);
			// Tipo cambio
			val cmpChangeType = getChangeType(payment, cmpPaymentCurrency, errors);
			// Monto (Importe del pago)
			val amount = getAmount(payment, cmpPaymentCurrency, errors);
			// Numero de operacion
			val operationNumber = UBase64.base64Decode(payment.getOperationNumber());
			// RFC emisor cuenta origen
			val sourceAccountRfc = getSourceAccountRfc(payment, cmpPaymentWay, errors);
			// Nombre de banco
			val bankName = getBankName(payment, cmpPaymentWay, sourceAccountRfc, errors);
			// Cuenta ordenante
			val payerAccount = getPayerAccount(payment, cmpPaymentWay, errors);
			// RFC emisor cuenta beneficiaria
			val targetAccountRfc = getTargetAccountRfc(payment, cmpPaymentWay, errors);
			// Cuenta beneficiaria
			val receiverAccount = getReceiverAccount(payment, cmpPaymentWay, errors);
			// Tipo cadena de pago
			val stringTypeResponse = getStringType(payment, cmpPaymentWay, errors);
			val stringType = stringTypeResponse.getStringType();
			val stringTypeCode = stringTypeResponse.getStringTypeCode();
			// Certificado de pago
			val paymentCertificate = getCertificate(payment, stringType, errors);
			// Cadena de pago
			val originalString = getOriginalString(payment, stringType, errors);
			// Sello de pago
			val paymentSeal = getPaymentSeal(payment, stringType, errors);
			// Documentos relacionados al pago
			val relatedDocumentResponse = getRelatedDocuments(payment);
			
			if (!relatedDocumentResponse.isError() && errors.isEmpty()) {
				val wPaymentRequest = WPaymentRequest
										.builder()
										.paymentDate(paymentDate)
										.cmpPaymentWay(cmpPaymentWay)
										.cmpPaymentCurrency(cmpPaymentCurrency)
										.cmpChangeType(cmpChangeType)
										.amount(amount)
										.operationNumber(operationNumber)
										.sourceAccountRfc(sourceAccountRfc)
										.bankName(bankName)
										.payerAccount(payerAccount)
										.targetAccountRfc(targetAccountRfc)
										.receiverAccount(receiverAccount)
										.stringType(stringType)
										.stringTypeCode(stringTypeCode)
										.paymentCertificate(paymentCertificate)
										.originalString(originalString)
										.paymentSeal(paymentSeal)
										.transferreds(payment.getTransferreds())
										.withhelds(payment.getWithhelds())
										.build();
				// Informacion del pago
				val wComplementPayment = populatePayment(wPaymentRequest);
				// Informacion de documentos relacionados
				val wRelatedDocuments = getWRelatedDocuments(relatedDocumentResponse.getRelatedDocuments());
				
				if (!isEdit) {
					wComplementPayment.getRelatedDocuments().addAll(wRelatedDocuments);
					payments.add(wComplementPayment);
				} else {
					payments.get(index).setPaymentDate(wComplementPayment.getPaymentDate());
					payments.get(index).setPaymentWay(wComplementPayment.getPaymentWay());
					payments.get(index).setCurrency(wComplementPayment.getCurrency());
					payments.get(index).setAmount(wComplementPayment.getAmount());
					payments.get(index).setChangeType(wComplementPayment.getChangeType());
					payments.get(index).setOperationNumber(wComplementPayment.getOperationNumber());
					payments.get(index).setSourceAccountRfc(wComplementPayment.getSourceAccountRfc());
					payments.get(index).setPayerAccount(wComplementPayment.getPayerAccount());
					payments.get(index).setBankName(wComplementPayment.getBankName());
					payments.get(index).setTargetAccountRfc(wComplementPayment.getTargetAccountRfc());
					payments.get(index).setReceiverAccount(wComplementPayment.getReceiverAccount());
					payments.get(index).setStringType(wComplementPayment.getStringType());
					payments.get(index).setOriginalString(wComplementPayment.getOriginalString());
					payments.get(index).setPaymentCertificate(wComplementPayment.getPaymentCertificate());
					payments.get(index).setPaymentSeal(wComplementPayment.getPaymentSeal());
					// Documentos relacionados
					payments.get(index).setRelatedDocuments(new ArrayList<>());
					payments.get(index).getRelatedDocuments().addAll(wRelatedDocuments);
					// Impuestos
					payments.get(index).setTransferreds(wComplementPayment.getTransferreds());
					payments.get(index).setWithhelds((wComplementPayment.getWithhelds()));
				}
				
				response = JResponse.success();
			} else {
				response = relatedDocumentResponse.getResponse();
				if (response == null) {
					response = JResponse.markedError(errors);
				} else {
					response.setErrors(errors);
					response.setError(Boolean.TRUE);
				}
			}
		} else {
			val errorMessage = UProperties.getMessage("ERR0146", userTools.getLocale());
			val notification = JNotification.modalPanelMessageError(errorMessage);
			response = JResponse.error(notification);
		}
		
		return new JComplementPayment(response);
	}
	
	@Override
	public PaymentTotalsResponse populatePaymentTotals(String data, List<JError> errors) {
		val paymentTotal = GsonClient.deserialize(data, ComplementPaymentTotal.class);
		if (paymentTotal != null) {
			WPaymentTotal wPaymentTotal = null;
			JError error = null;
			val field = "monto-total-pagos";
			
			val totalAmountPayments = UBase64.base64Decode(paymentTotal.getTotalAmountPayments());
			if (UValidator.isNullOrEmpty(totalAmountPayments)) {
				error = JError.required(field, userTools.getLocale());
			} else {
				val totalWithheldIva = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalWithheldIva()));
				val totalWithheldIsr = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalWithheldIsr()));
				val totalWithheldIeps = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalWithheldIeps()));
				val totalTransferredBaseIva16 = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalTransferredBaseIva16()));
				val totalTransferredTaxIva16 = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalTransferredTaxIva16()));
				val totalTransferredBaseIva8 = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalTransferredBaseIva8()));
				val totalTransferredTaxIva8 = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalTransferredTaxIva8()));
				val totalTransferredBaseIva0 = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalTransferredBaseIva0()));
				val totalTransferredTaxIva0 = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalTransferredTaxIva0()));
				val totalTransferredBaseIvaExempt = UValue.bigDecimalStrict(UBase64.base64Decode(paymentTotal.getTotalTransferredBaseIvaExempt()));
				val totalAmountPay = UValue.bigDecimalStrict(totalAmountPayments);
				
				wPaymentTotal = WPaymentTotal
								.builder()
								.totalWithheldIva(totalWithheldIva)
								.totalWithheldIsr(totalWithheldIsr)
								.totalWithheldIeps(totalWithheldIeps)
								.totalTransferredBaseIva16(totalTransferredBaseIva16)
								.totalTransferredTaxIva16(totalTransferredTaxIva16)
								.totalTransferredBaseIva8(totalTransferredBaseIva8)
								.totalTransferredTaxIva8(totalTransferredTaxIva8)
								.totalTransferredBaseIva0(totalTransferredBaseIva0)
								.totalTransferredTaxIva0(totalTransferredTaxIva0)
								.totalTransferredBaseIvaExempt(totalTransferredBaseIvaExempt)
								.totalAmountPayments(totalAmountPay)
								.build();
			}
			
			var paymentTotalsError = false;
			if (error != null) {
				errors.add(error);
				paymentTotalsError = true;
			}
			
			return new PaymentTotalsResponse(wPaymentTotal, paymentTotalsError);
		}
		return PaymentTotalsResponse.empty();
	}
	
	@Override
	public WPayment populatePayment(final WPaymentRequest request) {
		val wPayment = new WPayment();
		wPayment.setPaymentDate(request.getPaymentDate());
		wPayment.setPaymentWay(UCatalog.formaPago(request.getCmpPaymentWay().getCodigo()));
		wPayment.setPaymentWayCode(request.getCmpPaymentWay().getCodigo());
		wPayment.setPaymentWayValue(request.getCmpPaymentWay().getValor());
		wPayment.setCurrency(UCatalog.moneda(request.getCmpPaymentCurrency().getCodigo()));
		wPayment.setCurrencyEntity(currencyService.findByCode(request.getCmpPaymentCurrency().getCodigo()));
		wPayment.setAmount(request.getAmount());
		wPayment.setChangeType(request.getCmpChangeType());
		wPayment.setOperationNumber(request.getOperationNumber());
		wPayment.setSourceAccountRfc(request.getSourceAccountRfc());
		wPayment.setPayerAccount(request.getPayerAccount());
		wPayment.setBankName(request.getBankName());
		wPayment.setTargetAccountRfc(request.getTargetAccountRfc());
		wPayment.setReceiverAccount(request.getReceiverAccount());
		wPayment.setStringTypeCode(request.getStringTypeCode());
		wPayment.setStringType(request.getStringType());
		wPayment.setOriginalString(request.getOriginalString());
		wPayment.setPaymentCertificate(UValue.stringToByte(request.getPaymentCertificate()));
		wPayment.setPaymentSeal(UValue.stringToByte(request.getPaymentSeal()));
		
		if (request.getTransferreds() != null && !request.getTransferreds().isEmpty()) {
			val transferreds = new ArrayList<WTaxes>();
			for (val itax : request.getTransferreds()) {
				val transferred = getWTax(itax);
				transferreds.add(transferred);
			}
			wPayment.getTransferreds().addAll(transferreds);
		}
		
		if (request.getWithhelds() != null && !request.getWithhelds().isEmpty()) {
			val withhelds = new ArrayList<WTaxes>();
			for (val itax : request.getWithhelds()) {
				val withheld = WTaxes
								.builder()
								.tax(UBase64.base64Decode(itax.getTax()))
								.amount(UValue.bigDecimalStrict(UBase64.base64Decode(itax.getAmount())))
								.build();
				withhelds.add(withheld);
			}
			wPayment.getWithhelds().addAll(withhelds);
		}
		
		return wPayment;
	}

	private PaymentTotal mapPaymentTotal(WPaymentTotal paymentTotal) {
		return PaymentTotal
				.builder()
				.totalWithheldIva(paymentTotal.getTotalWithheldIva())
				.totalWithheldIsr(paymentTotal.getTotalWithheldIsr())
				.totalWithheldIeps(paymentTotal.getTotalWithheldIeps())
				.totalTransferredBaseIva16(paymentTotal.getTotalTransferredBaseIva16())
				.totalTransferredTaxIva16(paymentTotal.getTotalTransferredTaxIva16())
				.totalTransferredBaseIva8(paymentTotal.getTotalTransferredBaseIva8())
				.totalTransferredTaxIva8(paymentTotal.getTotalTransferredTaxIva8())
				.totalTransferredBaseIva0(paymentTotal.getTotalTransferredBaseIva0())
				.totalTransferredTaxIva0(paymentTotal.getTotalTransferredTaxIva0())
				.totalTransferredBaseIvaExempt(paymentTotal.getTotalTransferredBaseIvaExempt())
				.totalAmountPayments(paymentTotal.getTotalAmountPayments())
				.build();
	}
	
	private final List<RelatedDocument> getRelatedDocuments(WPayment item, Integer paymentNumber) {
		val relatedDocuments = new ArrayList<RelatedDocument>();
		var relatedDocumentNumber = 1;
		for (val document : item.getRelatedDocuments()) {
			val relatedDocument = populateRelatedDocument(document, paymentNumber, relatedDocumentNumber);
			
			if (document.getTransferreds() != null && !document.getTransferreds().isEmpty()) {
				val transferreds = new ArrayList<BaseTax>();
				for (val itemTransferred : document.getTransferreds()) {
					val transferred = getBaseTax(itemTransferred, relatedDocumentNumber); 
					transferreds.add(transferred);
				}
				relatedDocument.getTransferreds().addAll(transferreds);
			}
			
			if (document.getWithhelds() != null && !document.getWithhelds().isEmpty()) {
				val withheldList = new ArrayList<BaseTax>();
				for (val itemWithheld : item.getWithhelds()) {
					val withheld = getBaseTax(itemWithheld, relatedDocumentNumber);
					withheldList.add(withheld);
				}
				relatedDocument.getWithhelds().addAll(withheldList);
			}
			
			relatedDocuments.add(relatedDocument);
			relatedDocumentNumber ++;
		}
		return relatedDocuments;
	}

	private RelatedDocument populateRelatedDocument(final WRelatedDocument document, 
													Integer paymentNumber,
													int relatedDocumentNumber) {
		return RelatedDocument
				.builder()
				.documentId(document.getDocumentId())
				.currency(document.getCurrency())
				.currencyEntity(document.getCurrencyEntity())
				.changeType(document.getChangeType())
				.serie(document.getSerie())
				.sheet(document.getSheet())
				.partiality(document.getPartiality())
				.amountPartialityBefore(document.getAmountPartialityBefore())
				.amountPaid(document.getAmountPaid())
				.difference(document.getDifference())
				.taxObject(document.getTaxObject())
				.taxObjectCode(document.getTaxObjectCode())
				.paymentNumber(UValue.integerString(paymentNumber))
				.relatedDocumentNumber(UValue.integerString(relatedDocumentNumber))
				.build();
	}

	private BaseTax getBaseTax(final WTaxes wTax, int number) {
		return BaseTax
				.builder()
				.base(UValue.bigDecimalString(wTax.getBase()))
				.baseBigDecimal(wTax.getBase())
				.tax(wTax.getTax())
				.rateOrFee(UValue.bigDecimalString(wTax.getRateOrFee()))
				.rateOrFeeBigDecimal(wTax.getRateOrFee())
				.factor(wTax.getFactorType().value())
				.factorType(wTax.getFactorType())
				.amount(UValue.bigDecimalString(wTax.getAmount()))
				.amountBigDecimal(wTax.getAmount())
				.number(UValue.integerString(number))
				.build();
	}
	
	private PaymentWithheld getPaymentWithheld(final WTaxes wTax, int number) {
		return PaymentWithheld
				.builder()
				.tax(wTax.getTax())
				.amount(UValue.bigDecimalString(wTax.getAmount()))
				.amountBigDecimal(wTax.getAmount())
				.number(UValue.integerString(number))
				.build();
	}

	private final Payment populatePayment(WPayment item, Integer paymentNumber) {
		val payment = Payment
						.builder()
						.paymentDate(item.getPaymentDate())
						.paymentWay(item.getPaymentWay())
						.paymentWayCode(item.getPaymentWayCode())
						.paymentWayValue(item.getPaymentWayValue())
						.amount(item.getAmount())
						.currency(item.getCurrency())
						.currencyEntity(item.getCurrencyEntity())
						.changeType(item.getChangeType())
						.operationNumber(item.getOperationNumber())
						.sourceAccountRfc(item.getSourceAccountRfc())
						.payerAccount(item.getPayerAccount())
						.bankName(item.getBankName())
						.targetAccountRfc(item.getTargetAccountRfc())
						.receiverAccount(item.getReceiverAccount())
						.stringType(item.getStringType())
						.stringTypeCode(item.getStringTypeCode())
						.originalString(item.getOriginalString())
						.paymentCertificate(item.getPaymentCertificate())
						.paymentSeal(item.getPaymentSeal())
						.paymentNumber(UValue.integerString(paymentNumber))
						.build();
		
		if (item.getTransferreds() != null && !item.getTransferreds().isEmpty()) {
			val transferreds = new ArrayList<BaseTax>();
			for (val itemTransferred : item.getTransferreds()) {
				val transferred = getBaseTax(itemTransferred, paymentNumber); 
				transferreds.add(transferred);
			}
			payment.getTransferreds().addAll(transferreds);
		}
		
		if (item.getWithhelds() != null && !item.getWithhelds().isEmpty()) {
			val withheldList = new ArrayList<PaymentWithheld>();
			for (val itemWithheld : item.getWithhelds()) {
				val withheld = getPaymentWithheld(itemWithheld, paymentNumber);
				withheldList.add(withheld);
			}
			payment.getWithhelds().addAll(withheldList);
		}
		
		return payment;
	}

	private ComplementPayment getComplementPayment(WPayment wPayment) {
		val cmpPayment = new ComplementPayment();
		cmpPayment.setPaymentDate(UBase64.base64Encode(wPayment.getPaymentDate()));
		cmpPayment
				.setPaymentWay(UBase64.base64Encode(webCatalog.paymentWay(wPayment.getPaymentWay())));
		cmpPayment.setAmount(UBase64.base64Encode(UValue.bigDecimalString(wPayment.getAmount())));
		cmpPayment.setCurrency(UBase64.base64Encode(webCatalog.currency(wPayment.getCurrency())));

		val changeType = UValidator.isNullOrEmpty(wPayment.getChangeType())
				? UValue.bigDecimalString(wPayment.getChangeType())
				: EMPTY_STRING;
		cmpPayment.setChangeType(UBase64.base64Encode(changeType));

		val operationNumber = !UValidator.isNullOrEmpty(wPayment.getOperationNumber())
				? wPayment.getOperationNumber()
				: EMPTY_STRING;
		cmpPayment.setOperationNumber(UBase64.base64Encode(operationNumber));

		val sourceAccountRfc = !UValidator.isNullOrEmpty(wPayment.getSourceAccountRfc())
				? wPayment.getSourceAccountRfc()
				: EMPTY_STRING;
		cmpPayment.setSourceAccountRfc(UBase64.base64Encode(sourceAccountRfc));

		val payerAccount = !UValidator.isNullOrEmpty(wPayment.getPayerAccount())
				? wPayment.getPayerAccount()
				: EMPTY_STRING;
		cmpPayment.setPayerAccount(UBase64.base64Encode(payerAccount));

		val bankName = !UValidator.isNullOrEmpty(wPayment.getBankName()) ? wPayment.getBankName()
				: EMPTY_STRING;
		cmpPayment.setBank(UBase64.base64Encode(bankName));

		val targetAccountRfc = !UValidator.isNullOrEmpty(wPayment.getTargetAccountRfc())
				? wPayment.getTargetAccountRfc()
				: EMPTY_STRING;
		cmpPayment.setTargetAccountRfc(UBase64.base64Encode(targetAccountRfc));

		val receiverAccount = !UValidator.isNullOrEmpty(wPayment.getReceiverAccount())
				? wPayment.getReceiverAccount()
				: EMPTY_STRING;
		cmpPayment.setReceiverAccount(UBase64.base64Encode(receiverAccount));

		val stringType = !UValidator.isNullOrEmpty(wPayment.getStringType())
				? wPayment.getStringType()
				: EMPTY_STRING;
		cmpPayment.setStringType(UBase64.base64Encode(stringType));

		val originalString = !UValidator.isNullOrEmpty(wPayment.getOriginalString())
				? wPayment.getOriginalString()
				: EMPTY_STRING;
		cmpPayment.setOriginalString(UBase64.base64Encode(originalString));

		val paymentCertificate = !UValidator.isNullOrEmpty(wPayment.getPaymentCertificate())
				? UValue.byteToString(wPayment.getPaymentCertificate())
				: EMPTY_STRING;
		cmpPayment.setCertificate(UBase64.base64Encode(paymentCertificate));

		val paymentSeal = !UValidator.isNullOrEmpty(wPayment.getPaymentSeal())
				? UValue.byteToString(wPayment.getPaymentSeal())
				: EMPTY_STRING;
		cmpPayment.setSeal(UBase64.base64Encode(paymentSeal));
		
		val transferreds = wPayment.getTransferreds();
		if (transferreds != null && !transferreds.isEmpty()) {
			val transferredTaxes = transferreds.stream().map(this::mapBaseTax).collect(Collectors.toList());
			cmpPayment.setTransferreds(transferredTaxes);
		}
		
		val withhelds = wPayment.getWithhelds();
		if (withhelds != null && !withhelds.isEmpty()) {
			val withheldTaxes = withhelds.stream().map(this::mapPaymentWithheld).collect(Collectors.toList());
			cmpPayment.setWithhelds(withheldTaxes);
		}
		
		return cmpPayment;
	}

	private ComplementPaymentBaseTax mapBaseTax(WTaxes item) {
		return ComplementPaymentBaseTax
						.builder()
						.base(UBase64.base64Encode(UValue.bigDecimalString(item.getBase())))
						.tax(UBase64.base64Encode(item.getTax()))
						.factorType(UBase64.base64Encode(item.getFactorType().value()))
						.rateOrFee(UBase64.base64Encode(UValue.bigDecimalString(item.getRateOrFee())))
						.amount(UBase64.base64Encode(UValue.bigDecimalString(item.getAmount())))
						.build();
	}
	
	private ComplementPaymentWithheld mapPaymentWithheld(WTaxes item) {
		return ComplementPaymentWithheld
				.builder()
				.tax(UBase64.base64Encode(item.getTax()))
				.amount(UBase64.base64Encode(UValue.bigDecimalString(item.getAmount())))
				.build();
	}
	
	private List<ComplementPaymentRelatedDocument> getRelatedDocuments(List<WPayment> payments, Integer index) {
		val relatedDocuments = new ArrayList<ComplementPaymentRelatedDocument>();
		val wRelatedDocuments = payments.get(index).getRelatedDocuments();
		for (val document : wRelatedDocuments) {
			val documentRelated = new ComplementPaymentRelatedDocument();

			documentRelated.setDocumentId(UBase64.base64Encode(document.getDocumentId()));
			documentRelated.setCurrency(UBase64.base64Encode(webCatalog.currency(document.getCurrency())));
			
			val documentRelatedChangeType = !UValidator.isNullOrEmpty(document.getChangeType())
					? UValue.bigDecimalString(document.getChangeType())
					: EMPTY_STRING;
			documentRelated.setChangeType(UBase64.base64Encode(documentRelatedChangeType));

			val serie = !UValidator.isNullOrEmpty(document.getSerie()) ? document.getSerie() : EMPTY_STRING;
			documentRelated.setSerie(UBase64.base64Encode(serie));

			val sheet = !UValidator.isNullOrEmpty(document.getSheet()) ? document.getSheet() : EMPTY_STRING;
			documentRelated.setSheet(UBase64.base64Encode(sheet));

			val partiality = !UValidator.isNullOrEmpty(document.getPartiality())
					? UValue.integerString(document.getPartiality())
					: EMPTY_STRING;
			documentRelated.setPartiality(UBase64.base64Encode(partiality));

			val amountPartialityBefore = !UValidator.isNullOrEmpty(document.getAmountPartialityBefore())
					? UValue.bigDecimalString(document.getAmountPartialityBefore())
					: EMPTY_STRING;
			documentRelated.setAmountPartialityBefore(UBase64.base64Encode(amountPartialityBefore));

			val amountPaid = !UValidator.isNullOrEmpty(document.getAmountPaid())
					? UValue.bigDecimalString(document.getAmountPaid())
					: EMPTY_STRING;
			documentRelated.setAmountPaid(UBase64.base64Encode(amountPaid));

			val difference = !UValidator.isNullOrEmpty(document.getDifference())
					? UValue.bigDecimalString(document.getDifference())
					: EMPTY_STRING;
			documentRelated.setDifference(UBase64.base64Encode(difference));
			
			val transferreds = document.getTransferreds();
			if (transferreds != null && !transferreds.isEmpty()) {
				val transferredTaxes = transferreds.stream().map(this::mapBaseTax).collect(Collectors.toList());
				documentRelated.setTransferreds(transferredTaxes);
			}
			
			val withhelds = document.getWithhelds();
			if (withhelds != null && !withhelds.isEmpty()) {
				val withheldTaxes = withhelds.stream().map(this::mapBaseTax).collect(Collectors.toList());
				documentRelated.setWithhelds(withheldTaxes);
			}

			relatedDocuments.add(documentRelated);
		}
		return relatedDocuments;
	}
	
	private String getPaymentDate(final ComplementPayment cmpPayment, List<JError> errors) {
		val field = "cmp-payment-payment-date";
		val paymentDate = UBase64.base64Decode(cmpPayment.getPaymentDate());
		return paymentDateValidator.validate(paymentDate, field, errors);
	}
	
	private FormaPagoEntity getPaymentWay(final ComplementPayment cmpPayment, List<JError> errors) {
		val field = "cmp-payment-payment-way";
		val cmpPaymentWay = UBase64.base64Decode(cmpPayment.getPaymentWay());
		return paymentPaymWayValidator.getPaymentWay(cmpPaymentWay, field, errors);
	}
	
	private MonedaEntity getCurrency(final ComplementPayment cmpPayment, List<JError> errors) {
		val field = "cmp-payment-currency";
		val cmpPaymentCurrency = UBase64.base64Decode(cmpPayment.getCurrency());
		return paymentCurrencyValidator.getCurrency(cmpPaymentCurrency, field, errors);
	}
	
	private BigDecimal getChangeType(ComplementPayment cmpPayment, MonedaEntity currency, List<JError> errors) {
		val changeType = UBase64.base64Decode(cmpPayment.getChangeType());
		val field = "cmp-payment-change-type";
		return paymentChangeTypeValidator.getChangeType(changeType, field, currency, errors);
	}
	
	private BigDecimal getAmount(ComplementPayment cmpPayment, MonedaEntity currency, List<JError> errors) {
		val field = "cmp-payment-amount";
		val amountStr = UBase64.base64Decode(cmpPayment.getAmount());
		return paymentAmountValidator.getAmount(amountStr, field, currency, errors);
	}
	
	private String getSourceAccountRfc(final ComplementPayment cmpPayment, 
									   final FormaPagoEntity cmpPaymentWay,
									   final List<JError> errors) {
		val field = "cmp-payment-rfc-source-account";
		val sourceAccountRfc = UBase64.base64Decode(cmpPayment.getSourceAccountRfc());
		return paymentSourceAccountRfcValidator.getSourceAccountRfc(sourceAccountRfc, field, cmpPaymentWay, errors);
	}
	
	private String getBankName(final ComplementPayment cmpPayment,
							   final FormaPagoEntity cmpPaymentWay,
							   final String sourceAccountRfc,
							   final List<JError> errors) {
		val field = "cmp-payment-bank";
		val bankName = UBase64.base64Decode(cmpPayment.getBank());
		return paymentBankValidator.getBankName(bankName, field, cmpPaymentWay, sourceAccountRfc, errors);
	}
	
	private String getPayerAccount(final ComplementPayment cmpPayment,
								   final FormaPagoEntity cmpPaymentWay,
								   final List<JError> errors) {
		val field = "cmp-payment-source-account";
		val payerAccount = UBase64.base64Decode(cmpPayment.getPayerAccount());
		return paymentPayerAccountValidator.getPayerAccount(payerAccount, field, cmpPaymentWay, errors);
	}
	
	private String getTargetAccountRfc(final ComplementPayment cmpPayment,
									   final FormaPagoEntity cmpPaymentWay,
									   final List<JError> errors) {
		val field = "cmp-payment-rfc-target-account";
		val targetAccountRfc = UBase64.base64Decode(cmpPayment.getTargetAccountRfc());
		return paymentTargetAccountRfcValidator.getTargetAccountRfc(targetAccountRfc, field, cmpPaymentWay, errors);
	}
	
	private String getReceiverAccount(final ComplementPayment cmpPayment,
									  final FormaPagoEntity cmpPaymentWay,
									  final List<JError> errors) {
		val field = "cmp-payment-target-account";
		val receiverAccount = UBase64.base64Decode(cmpPayment.getReceiverAccount());
		return paymentReceiverAccountValidator.getReceiverAccount(receiverAccount, field, cmpPaymentWay, errors);
	}
	
	private StringTypeResponse getStringType(final ComplementPayment cmpPayment,
			  					 			 final FormaPagoEntity cmpPaymentWay,
			  					 			 final List<JError> errors) {
		val field = "cmp-payment-string-type";
		val stringType = UBase64.base64Decode(cmpPayment.getStringType());
		return paymentStringTypeValidator.getStringType(stringType, field, cmpPaymentWay, errors);
	}
	
	private String getCertificate(final ComplementPayment cmpPayment, 
								  final String stringType,
								  final List<JError> errors) {
		val field = "cmp-payment-certificate";
		val paymentCertificate = UBase64.base64Decode(cmpPayment.getCertificate());
		return paymentCertificateValidation.getCertificate(paymentCertificate, field, stringType, errors);
	}
	
	private String getOriginalString(final ComplementPayment cmpPayment, 
									 final String stringType,
									 final List<JError> errors) {
		val field = "cmp-payment-string";
		val originalString = UBase64.base64Decode(cmpPayment.getOriginalString());							
		return paymentOriginalStringValidator.getOriginalString(originalString, field, stringType, errors);
	}
	
	private String getPaymentSeal(final ComplementPayment cmpPayment, 
								  final String stringType,
								  final List<JError> errors) {
		val field = "cmp-payment-seal";
		val paymentSeal = UBase64.base64Decode(cmpPayment.getSeal());
		return paymentSealValidator.getPaymentSeal(paymentSeal, field, stringType, errors);
	}
	
	private RelatedDocumentResponse getRelatedDocuments(final ComplementPayment cmpPayment) {
		boolean hasError = false;
		
		val relatedDocuments = cmpPayment.getRelatedDocuments();
		if (relatedDocuments.isEmpty()) {
			hasError = true;
		} else {
			val invalidCount = relatedDocuments.stream()
											   .map(ComplementPaymentRelatedDocument::getAmountPaid)
											   .map(UBase64::base64Encode)
											   .filter(UValidator::isNullOrEmpty)
											   .count();
			hasError = invalidCount > 0;
		}
		
		JResponse response = null;
		if (hasError) {
			val errorMessage = UProperties.getMessage("ERR0142", userTools.getLocale());
			val notification = JNotification.modalPanelMessageError(errorMessage);
			response = JResponse.error(notification);
		}
		
		return new RelatedDocumentResponse(relatedDocuments, response, hasError);
	}
	
	private List<WRelatedDocument> getWRelatedDocuments(List<ComplementPaymentRelatedDocument> relatedDocuments) {
		val wRelatedDocuments = new ArrayList<WRelatedDocument>();
		relatedDocuments.forEach(item -> {
			val currency = UBase64.base64Decode(item.getCurrency());
			val currencyCode = UCatalog.getCode(currency);
			val currencyEntity = currencyService.findByCode(currencyCode);
			
			val wRelatedDocument = new WRelatedDocument();
			wRelatedDocument.setDocumentId(UBase64.base64Decode(item.getDocumentId()));
			wRelatedDocument.setCurrency(UCatalog.moneda(currencyCode));
			wRelatedDocument.setCurrencyEntity(currencyEntity);
			
			val changeType = UBase64.base64Decode(item.getChangeType());
			wRelatedDocument.setChangeType(UValue.bigDecimalStrict(changeType));
			
			wRelatedDocument.setSerie(UBase64.base64Decode(item.getSerie()));
			wRelatedDocument.setSheet(UBase64.base64Decode(item.getSheet()));
			
			val partiality = UBase64.base64Decode(item.getPartiality());
			wRelatedDocument.setPartiality(UValue.integerValue(partiality));
			
			val amountPartialityBefore = UBase64.base64Decode(item.getAmountPartialityBefore());
			wRelatedDocument.setAmountPartialityBefore(UValue.bigDecimalStrict(amountPartialityBefore));
			
			val amountPaid = UBase64.base64Decode(item.getAmountPaid());
			wRelatedDocument.setAmountPaid(UValue.bigDecimalStrict(amountPaid));
			
			val difference = UBase64.base64Decode(item.getDifference());
			wRelatedDocument.setDifference(UValue.bigDecimalStrict(difference));
			
			wRelatedDocument.setTaxObject(UBase64.base64Decode(item.getTaxObject()));
			wRelatedDocument.setTaxObjectCode(UCatalog.getCode(UBase64.base64Decode(item.getTaxObject())));
			
			if (item.getTransferreds() != null && !item.getTransferreds().isEmpty()) {
				val transferreds = new ArrayList<WTaxes>();
				item.getTransferreds().forEach(itax -> {
					val transferred = getWTax(itax);
					transferreds.add(transferred);
				});
				wRelatedDocument.setTransferreds(transferreds);
			}
			
			if (item.getWithhelds() != null && !item.getWithhelds().isEmpty()) {
				val withhelds = new ArrayList<WTaxes>();
				item.getWithhelds().forEach(itax -> {
					val withheld = getWTax(itax);
					withhelds.add(withheld);
				});
				wRelatedDocument.setWithhelds(withhelds);
			}
			
			wRelatedDocuments.add(wRelatedDocument);
		});
		return wRelatedDocuments;
	}

	private WTaxes getWTax(ComplementPaymentBaseTax itax) {
		return WTaxes
				.builder()
				.base(UValue.bigDecimalStrict(UBase64.base64Decode(itax.getBase())))
				.tax(UBase64.base64Decode(itax.getTax()))
				.factorType(UCatalog.factorType(UBase64.base64Decode(itax.getFactorType())))
				.rateOrFee(UValue.bigDecimalStrict(UBase64.base64Decode(itax.getRateOrFee())))
				.amount(UValue.bigDecimalStrict(UBase64.base64Decode(itax.getAmount())))
				.build();
	}

}