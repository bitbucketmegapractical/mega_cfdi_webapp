package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.io.FileWriter;
import java.util.List;

import org.megapractical.invoicing.api.util.UFile;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
final class CancellationSummaryWritter {
	
	private CancellationSummaryWritter() {		
	}
	
	public static void writeSummary(String summaryPath, List<String> summaryList) {
		try {
			val summaryFile = UFile.createFileIfNotExist(summaryPath);
			if (summaryFile != null) {
				try (val writer = new CSVWriter(new FileWriter(summaryFile), '|', ICSVWriter.NO_QUOTE_CHARACTER,
						ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END)) {
					for (val detail : summaryList) {
						val sentence = new StringBuilder();
						sentence.append(detail);
						
						val summarySet = new String[] { sentence.toString() };
						writer.writeNext(summarySet);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}