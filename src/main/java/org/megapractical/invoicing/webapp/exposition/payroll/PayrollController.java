package org.megapractical.invoicing.webapp.exposition.payroll;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiEmitter;
import org.megapractical.invoicing.api.wrapper.ApiPayrollFileConfiguration;
import org.megapractical.invoicing.api.wrapper.ApiPayrollStamp;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEstadisticaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoArchivoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IFileStatusJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IFileTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollFileConfigurationJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollFileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollFileStatisticsJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.payroll.file.service.PayrollFileService;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.exposition.service.PayrollStampService;
import org.megapractical.invoicing.webapp.exposition.service.SessionService;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JCustomResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JPayrollFile;
import org.megapractical.invoicing.webapp.json.JPayrollFile.PayrollFile;
import org.megapractical.invoicing.webapp.json.JPayrollFile.PayrollFile.PayrollFileStatistics;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.EmitterTools;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import lombok.val;

@Controller
@Scope("session")
public class PayrollController {

	@Autowired
	AppLog appLog;

	@Autowired
	TimelineLog timelineLog;

	@Autowired
	AppSettings appSettings;

	@Autowired
	UserTools userTools;

	@Autowired
	EmitterTools emitterTools;

	@Autowired
	PropertiesTools propertiesTools;

	@Autowired
	DataStoreService dataStore;

	@Autowired
	StampControlService stampControl;

	@Autowired
	SessionController sessionController;
	
	@Autowired
	private SessionService sessionService;

	@Autowired
	MailService mailService;

	@Autowired
	IPersistenceDAO persistenceDAO;

	@Autowired
	PayrollStampService payrollStampService;
	
	@Autowired
	private PayrollFileService payrollFileService;

	@Resource
	IPayrollFileConfigurationJpaRepository iPayrollFileConfigurationJpaRepository;

	@Resource
	ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;

	@Resource
	IPayrollFileJpaRepository iPayrollFileJpaRepository;

	@Resource
	IPayrollTypeJpaRepository iPayrollTypeJpaRepository;

	@Resource
	IFileStatusJpaRepository iFileStatusJpaRepository;

	@Resource
	IFileTypeJpaRepository iFileTypeJpaRepository;

	@Resource
	ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;

	@Resource
	IPayrollFileStatisticsJpaRepository iPayrollFileStatisticsJpaRepository;

	private static final String SORT_PROPERTY = "id";
	private static final Integer INITIAL_PAGE = 0;

	// ##### Estado de archivo: En cola
	private static final String FILE_STATUS_QUEUED = "C";

	// ##### Estado de archivo: Procesando
	private static final String FILE_STATUS_PROCESSING = "P";

	// ##### Estado de archivo: Generado
	private static final String FILE_STATUS_GENERATED = "G";

	// ##### Tipo Archivo CSV
	private static final String FILE_TYPE_CSV = "N12CSV";

	// ##### Tipo Archivo TXT
	private static final String FILE_TYPE_TXT = "N12TXT";

	// ##### Contribuyente
	private ContribuyenteEntity taxpayer;

	// ##### Usuario
	private UsuarioEntity user;

	// ##### Certificado del emisor
	private ContribuyenteCertificadoEntity certificate;
	private byte[] certificateByte;
	private byte[] privateKeyByte;
	private String keyPasswd;

	// ##### Informacion de timbres del emisor
	private TimbreEntity stampDetail;

	// ##### Informacion para el timbrado
	private StampProperties stampProperties;

	// ##### Confirguracion del archivo de nomina
	private ArchivoNominaConfiguracionEntity payrollFileConfiguration;

	@ModelAttribute("requiredMessage")
	public String requiredMessage() {
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}

	@ModelAttribute("processingRequest")
	public String processingRequest() {
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}

	@ModelAttribute("loadingRequest")
	public String loadingRequest() {
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}

	@ModelAttribute("emptyInformation")
	public String emptyInformation() {
		return UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
	}

	@ModelAttribute("availableStampError")
	public String availableStampError() {
		return UProperties.getMessage("ERR0163", userTools.getLocale());
	}

	// ##### Cargando tipo de nomina
	public List<String> payrollTypeSource() {
		List<String> payrollTypeSource = new ArrayList<>();
		try {

			Iterable<TipoNominaEntity> payrollTypeList = iPayrollTypeJpaRepository.findByEliminadoFalse();
			payrollTypeList.forEach(item -> payrollTypeSource.add(item.getValor()));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return payrollTypeSource;
	}

	// ##### Inicializando informacion
	@GetMapping("/payrollStamp")
	public String init(Model model) {
		try {

			// ##### Cargando el contribuyente por el rfc activo
			this.taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());

			// ##### Cargando el usuario
			this.user = userTools.getCurrentUser();

			// ##### Verificando que el contribuyente sea usuario master
			// if(userTools.isMaster(this.taxpayer)){
			// if(userTools.haveDaysLeft()){
			// ##### Cargando certificado del contribuyente
			this.certificate = new ContribuyenteCertificadoEntity();
			this.certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);

			// ##### Verificando vigencia del certificado
			if (UValidator.isNullOrEmpty(this.certificate)
					|| UDateTime.isBeforeLocalDate(this.certificate.getFechaExpiracion(), new Date())) {
				return "/denied/ResourceDeniedByCertificateExpiration";
			}

			// ##### Cargando informacion inicial
			initial(model);

			HtmlHelper.functionalitySelected("Payroll", "payroll-file-inbox");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			/*
			 * }else{ return "/home/Dashboard"; //return "/denied/ResourceDenied"; }
			 */
			return "/payroll/PayrollMaster";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}

	private void initial(Model model) {
		initialContent();
		
		model.addAttribute("page", INITIAL_PAGE);
		model.addAttribute("payrollTypeSource", payrollTypeSource());
	}

	private void initialContent() {
		try {

			// ##### Cargando informacion de timbres del emisor
			this.stampDetail = stampControl.stampDetails();

			// ##### Cargando informacion del certificado
			this.certificateByte = this.certificate.getCertificado();
			this.privateKeyByte = this.certificate.getLlavePrivada();
			this.keyPasswd = UBase64.base64Decode(this.certificate.getClavePrivada());

			// ##### Cargando informacion para el timbrado
			this.stampProperties = new StampProperties();
			this.stampProperties.setEmitterId(propertiesTools.getEmitterId());
			this.stampProperties.setSessionId(propertiesTools.getSessionId());
			this.stampProperties.setSourceSystem(propertiesTools.getSourceSystem());
			this.stampProperties.setCorrelationId(propertiesTools.getCorrelationId());
			// this.stampProperties.setVoucher(CfdiBuilderService.voucherBuild(apiCfdi));
			this.stampProperties.setCertificate(this.certificateByte);
			this.stampProperties.setPrivateKey(this.privateKeyByte);
			this.stampProperties.setPasswd(this.keyPasswd);
			this.stampProperties.setEnvironment(propertiesTools.getEnvironment());
			this.stampProperties.setServices(propertiesTools.getServices());

			// ##### Cargando configuracion del archivo de nomina del contribuyente
			this.payrollFileConfiguration = iPayrollFileConfigurationJpaRepository.findByContribuyente(this.taxpayer);
			if (UValidator.isNullOrEmpty(this.payrollFileConfiguration)) {
				configPayrollFileConfiguration();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void configPayrollFileConfiguration() {
		try {

			val entity = new ArchivoNominaConfiguracionEntity();
			entity.setContribuyente(this.taxpayer);
			entity.setEnvioXmlPdf(Boolean.FALSE);
			entity.setEnvioXml(Boolean.FALSE);
			entity.setEnvioPdf(Boolean.FALSE);
			entity.setConsolidado1x(Boolean.FALSE);
			entity.setConsolidado2x(Boolean.FALSE);
			entity.setDirectorioXmlPdfUnico(Boolean.TRUE);
			entity.setDirectorioXmlPdfSeparado(Boolean.FALSE);
			entity.setCompactado(Boolean.TRUE);
			entity.setNotificacion(Boolean.FALSE);

			val apiPayrollFileConfiguration = new ApiPayrollFileConfiguration();
			apiPayrollFileConfiguration.setPayrollFileConfiguration(entity);
			apiPayrollFileConfiguration.setAction("config");

			this.payrollFileConfiguration = dataStore.payrollFileConfigurationStore(apiPayrollFileConfiguration);

			// ##### Bitacora
			appLog.logOperation(OperationCode.REGISTRO_ARCHIVO_NOMINA_CONFIGURACION_SISTEMA_ID68);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/payrollStamp", params = { "payrolFilelList" })
	public @ResponseBody String payrollFilelList(HttpServletRequest request) {

		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JPayrollFile payrollFile = new JPayrollFile();

		String jsonInString = null;
		Gson gson = new Gson();

		String action = null;

		try {

			// ##### Listado de archivos con estado generado (con paginado)
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;

			String searchName = UBase64.base64Decode(request.getParameter("searchName"));
			if (UValidator.isNullOrEmpty(searchName)) {
				searchName = "";
			}

			String searchPeriod = UBase64.base64Decode(request.getParameter("searchPeriod"));
			if (UValidator.isNullOrEmpty(searchPeriod)) {
				searchPeriod = "";
			}

			String searchYear = UBase64.base64Decode(request.getParameter("searchYear"));
			if (UValidator.isNullOrEmpty(searchYear)) {
				searchYear = "";
			}

			// ##### Determinando la accion
			action = UBase64.base64Decode(request.getParameter("action"));

			EstadoArchivoEntity fileStatus = iFileStatusJpaRepository
					.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);

			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);

			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);

			Page<ArchivoNominaEntity> payrollFilePage = null;

			payrollFilePage = iPayrollFileJpaRepository
					.findByContribuyenteAndNombreContainingIgnoreCaseAndPeriodoContainingIgnoreCaseAndEjercicioContainingIgnoreCaseAndEstadoArchivoAndEliminadoFalseOrderByIdDesc(
							taxpayer, searchName, searchPeriod, searchYear, fileStatus, pageRequest);

			val pagination = JPagination.buildPaginaton(payrollFilePage);
			payrollFile.setPagination(pagination);

			for (ArchivoNominaEntity item : payrollFilePage) {
				PayrollFile payrollFileObj = new PayrollFile();
				payrollFileObj.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
				payrollFileObj.setPayrollType(UBase64.base64Encode(item.getTipoNomina().getValor()));
				payrollFileObj.setFileType(UBase64.base64Encode(item.getTipoArchivo().getValor()));
				payrollFileObj.setFileStatus(UBase64.base64Encode(item.getEstadoArchivo().getValor()));
				payrollFileObj.setName(UBase64.base64Encode(item.getNombre()));
				payrollFileObj.setPath(UBase64.base64Encode(item.getRuta()));
				payrollFileObj.setUploadDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaCarga())));
				payrollFileObj.setPeriod(UBase64.base64Encode(item.getPeriodo()));
				payrollFileObj.setYear(UBase64.base64Encode(item.getEjercicio()));
				payrollFileObj.setCompactedPath(UBase64.base64Encode(item.getRutaCompactado()));
				payrollFileObj.setConsolidatedPath(UBase64.base64Encode(item.getRutaConsolidado()));
				payrollFileObj.setErrorPath(UBase64.base64Encode(item.getRutaError()));

				payrollFile.getGeneratedPayrollFiles().add(payrollFileObj);
			}

			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if (!UValidator.isNullOrEmpty(searchName) || !UValidator.isNullOrEmpty(searchPeriod)
					|| !UValidator.isNullOrEmpty(searchYear)) {
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search",
						userTools.getLocale());
			}

			if (payrollFile.getGeneratedPayrollFiles().isEmpty()) {
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}

			// ##### Listado de archivos con estado procesando
			List<ArchivoNominaEntity> processingPayrollFiles = iPayrollFileJpaRepository
					.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(taxpayer,
							FILE_STATUS_PROCESSING);
			for (ArchivoNominaEntity item : processingPayrollFiles) {
				PayrollFile payrollFileObj = new PayrollFile();
				payrollFileObj.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
				payrollFileObj.setName(UBase64.base64Encode(item.getNombre()));

				ArchivoNominaEstadisticaEntity payrollFileStatisticsEntity = iPayrollFileStatisticsJpaRepository
						.findByArchivoNomina(item);
				if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity)) {
					PayrollFileStatistics payrollFileStatistics = new PayrollFileStatistics();
					payrollFileStatistics
							.setPayrollFileId(UBase64.base64Encode(payrollFileStatisticsEntity.getId().toString()));

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getNominas())) {
						payrollFileStatistics.setTotalPayroll(
								UBase64.base64Encode(UValue.integerString(payrollFileStatisticsEntity.getNominas())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getNominasTimbradas())) {
						payrollFileStatistics.setTotalPayrollStamped(UBase64
								.base64Encode(UValue.integerString(payrollFileStatisticsEntity.getNominasTimbradas())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getNominasError())) {
						payrollFileStatistics.setTotalPayrollError(UBase64
								.base64Encode(UValue.integerString(payrollFileStatisticsEntity.getNominasError())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getFechaInicioAnalisis())) {
						payrollFileStatistics.setParserStartDate(UBase64.base64Encode(
								UDate.formattedSingleDate(payrollFileStatisticsEntity.getFechaInicioAnalisis())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getHoraInicioAnalisis())) {
						payrollFileStatistics.setParserStartTime(UBase64.base64Encode(
								UDate.formattedTime(payrollFileStatisticsEntity.getHoraInicioAnalisis())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getFechaFinAnalisis())) {
						payrollFileStatistics.setParserEndDate(UBase64.base64Encode(
								UDate.formattedSingleDate(payrollFileStatisticsEntity.getFechaFinAnalisis())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getHoraFinAnalisis())) {
						payrollFileStatistics.setParserEndTime(UBase64
								.base64Encode(UDate.formattedTime(payrollFileStatisticsEntity.getHoraFinAnalisis())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getFechaInicioGeneracionComprobante())) {
						payrollFileStatistics
								.setVoucherGeneratingStartDate(UBase64.base64Encode(UDate.formattedSingleDate(
										payrollFileStatisticsEntity.getFechaInicioGeneracionComprobante())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getHoraInicioGeneracionComprobante())) {
						payrollFileStatistics.setVoucherGeneratingStartTime(UBase64.base64Encode(
								UDate.formattedTime(payrollFileStatisticsEntity.getHoraInicioGeneracionComprobante())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getFechaFinGeneracionComprobante())) {
						payrollFileStatistics.setVoucherGeneratingEndDate(UBase64.base64Encode(UDate
								.formattedSingleDate(payrollFileStatisticsEntity.getFechaFinGeneracionComprobante())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getHoraFinGeneracionComprobante())) {
						payrollFileStatistics.setVoucherGeneratingEndTime(UBase64.base64Encode(
								UDate.formattedTime(payrollFileStatisticsEntity.getHoraFinGeneracionComprobante())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getFechaInicioTimbrado())) {
						payrollFileStatistics.setVoucherStampingStartDate(UBase64.base64Encode(
								UDate.formattedSingleDate(payrollFileStatisticsEntity.getFechaInicioTimbrado())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getHoraInicioTimbrado())) {
						payrollFileStatistics.setVoucherStampingStartTime(UBase64.base64Encode(
								UDate.formattedTime(payrollFileStatisticsEntity.getHoraInicioTimbrado())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getFechaFinTimbrado())) {
						payrollFileStatistics.setVoucherStampingEndDate(UBase64.base64Encode(
								UDate.formattedSingleDate(payrollFileStatisticsEntity.getFechaFinTimbrado())));
					}

					if (!UValidator.isNullOrEmpty(payrollFileStatisticsEntity.getHoraFinTimbrado())) {
						payrollFileStatistics.setVoucherStampingEndTime(UBase64
								.base64Encode(UDate.formattedTime(payrollFileStatisticsEntity.getHoraFinTimbrado())));
					}

					payrollFileObj.setPayrollFileStatistics(payrollFileStatistics);
				}
				payrollFile.getProcessingPayrollFiles().add(payrollFileObj);
			}

			// ##### Listado de archivos con estado en cola
			List<ArchivoNominaEntity> queuedPayrollFiles = iPayrollFileJpaRepository
					.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(taxpayer,
							FILE_STATUS_QUEUED);
			for (ArchivoNominaEntity item : queuedPayrollFiles) {
				PayrollFile payrollFileObj = new PayrollFile();
				payrollFileObj.setId(UBase64.base64Encode(UValue.longString(item.getId())));
				payrollFileObj.setPayrollType(UBase64.base64Encode(item.getTipoNomina().getValor()));
				payrollFileObj.setName(UBase64.base64Encode(item.getNombre()));
				payrollFileObj.setUploadDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaCarga())));
				if (!UValidator.isNullOrEmpty(item.getTimbresDisponibles())) {
					payrollFileObj.setStampAvailable(item.getTimbresDisponibles());
				}

				payrollFile.getQueuedPayrollFiles().add(payrollFileObj);
			}

			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!action.equalsIgnoreCase("async")) {
			sessionController.sessionUpdate();
		}

		payrollFile.setResponse(response);
		jsonInString = gson.toJson(payrollFile);
		return jsonInString;
	}

	@RequestMapping(value = "/payrollStamp", params = { "payrollFileGeneratedList" })
	public @ResponseBody String payrollFileGeneratedList(HttpServletRequest request) {

		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JPayrollFile payrollFile = new JPayrollFile();

		String jsonInString = null;
		Gson gson = new Gson();

		try {

			// ##### Listado de archivos con estado generado (con paginado)
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;

			String searchName = UBase64.base64Decode(request.getParameter("searchName"));
			if (UValidator.isNullOrEmpty(searchName)) {
				searchName = "";
			}

			String searchPeriod = UBase64.base64Decode(request.getParameter("searchPeriod"));
			if (UValidator.isNullOrEmpty(searchPeriod)) {
				searchPeriod = "";
			}

			String searchYear = UBase64.base64Decode(request.getParameter("searchYear"));
			if (UValidator.isNullOrEmpty(searchYear)) {
				searchYear = "";
			}

			EstadoArchivoEntity fileStatus = iFileStatusJpaRepository
					.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);

			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);

			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);

			Page<ArchivoNominaEntity> payrollFilePage = null;

			payrollFilePage = iPayrollFileJpaRepository
					.findByContribuyenteAndNombreContainingIgnoreCaseAndPeriodoContainingIgnoreCaseAndEjercicioContainingIgnoreCaseAndEstadoArchivoAndEliminadoFalseOrderByIdDesc(
							taxpayer, searchName, searchPeriod, searchYear, fileStatus, pageRequest);
			val pagination = JPagination.buildPaginaton(payrollFilePage);
			payrollFile.setPagination(pagination);

			for (ArchivoNominaEntity item : payrollFilePage) {
				PayrollFile payrollFileObj = new PayrollFile();
				payrollFileObj.setId(UBase64.base64Encode(UValue.longString(item.getId())));
				payrollFileObj.setPayrollType(UBase64.base64Encode(item.getTipoNomina().getValor()));
				payrollFileObj.setFileType(UBase64.base64Encode(item.getTipoArchivo().getValor()));
				payrollFileObj.setFileStatus(UBase64.base64Encode(item.getEstadoArchivo().getValor()));
				payrollFileObj.setName(UBase64.base64Encode(item.getNombre()));
				payrollFileObj.setPath(UBase64.base64Encode(item.getRuta()));
				payrollFileObj.setUploadDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaCarga())));
				payrollFileObj.setPeriod(UBase64.base64Encode(item.getPeriodo()));
				payrollFileObj.setYear(UBase64.base64Encode(item.getEjercicio()));
				payrollFileObj.setStampAvailable(item.getTimbresDisponibles());
				payrollFileObj.setCompactedPath(UBase64.base64Encode(item.getRutaCompactado()));
				payrollFileObj.setConsolidatedPath(UBase64.base64Encode(item.getRutaConsolidado()));
				payrollFileObj.setErrorPath(UBase64.base64Encode(item.getRutaError()));

				payrollFile.getGeneratedPayrollFiles().add(payrollFileObj);
			}

			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if (!UValidator.isNullOrEmpty(searchName) || !UValidator.isNullOrEmpty(searchPeriod)
					|| !UValidator.isNullOrEmpty(searchYear)) {
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search",
						userTools.getLocale());
			}

			if (payrollFile.getGeneratedPayrollFiles().isEmpty()) {
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}

			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);

		} catch (Exception e) {
			e.printStackTrace();
		}

		sessionController.sessionUpdate();

		payrollFile.setResponse(response);
		jsonInString = gson.toJson(payrollFile);
		return jsonInString;
	}

	@Transactional
	@RequestMapping(value = "/payrollStamp", params = { "uploadFile" })
	public @ResponseBody String uploadFile(Model model, HttpServletRequest request,
			@RequestParam(value = "payrollFile") MultipartFile multipartPayrollFile) {

		JPayrollFile payrollFile = new JPayrollFile();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();

		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		boolean errorMarked = false;
		boolean payrollUploadError = false;

		Gson gson = new Gson();

		try {

			if (stampControl.stampStatus(this.taxpayer) || userTools.canUploadPayroll()) {
				// ##### Periodo
				String period = UBase64.base64Decode(request.getParameter("period"));
				if (UValidator.isNullOrEmpty(period)) {
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					error.setField("period");
					errors.add(error);
					payrollUploadError = true;
				} else {
					period = period.toUpperCase();
				}

				// ##### Tipo de nomina
				String payrollType = UBase64.base64Decode(request.getParameter("payrollType"));
				error = new JError();

				TipoNominaEntity payrollTypeEntity = null;

				if (UValidator.isNullOrEmpty(payrollType)) {
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				} else {
					payrollTypeEntity = iPayrollTypeJpaRepository.findByValorAndEliminadoFalse(payrollType);
					if (UValidator.isNullOrEmpty(payrollTypeEntity)) {
						String[] values = new String[] { "Tipo de nómina", "c_TipoNomina" };
						error.setMessage(
								UInvalidMessage.systemInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
						errorMarked = true;
					}
				}

				if (errorMarked) {
					error.setField("payroll-type");
					errors.add(error);
					payrollUploadError = true;
				}

				// ##### Archivo nomina
				File payrollFileUpload = null;

				String originalFilename = null;
				String payrollFileName = null;
				String payrollFileExt = null;

				error = new JError();

				if (multipartPayrollFile.isEmpty()) {
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				} else {
					originalFilename = multipartPayrollFile.getOriginalFilename();
					payrollFileExt = UFile.getExtension(originalFilename);

					if (UValidator.isNullOrEmpty(payrollFileExt)
							|| (!payrollFileExt.equalsIgnoreCase("txt") && !payrollFileExt.equalsIgnoreCase("csv"))) {
						error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
						errorMarked = true;
					} else {
						payrollFileName = UFile.getName(originalFilename).toUpperCase();
						ArchivoNominaEntity payrollFileEntity = iPayrollFileJpaRepository
								.findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(this.taxpayer,
										payrollFileName);
						if (!UValidator.isNullOrEmpty(payrollFileEntity)) {
							error.setMessage(UProperties.getMessage("ERR0161", userTools.getLocale()));
							errorMarked = true;
						} else {
							payrollFileUpload = UFile.multipartFileToFile(multipartPayrollFile);
						}
					}
				}

				if (errorMarked) {
					error.setField("payroll-file");
					errors.add(error);
					payrollUploadError = true;
				}

				if (!payrollUploadError) {
					// ##### Ruta de las nominas
					String payrollPath = appSettings.getPropertyValue("cfdi.payroll.path");

					// ##### Directorio para el archivo actual
					String folderPath = this.taxpayer.getRfcActivo() + File.separator + UDate.date().replace("-", "")
							+ File.separator + payrollFileName;
					String folderAbsolutePath = payrollPath + File.separator + folderPath;
					File payrollAbsolutePath = new File(folderAbsolutePath);
					if (!payrollAbsolutePath.exists()) {
						payrollAbsolutePath.mkdirs();
					}

					// ##### Cargando las configuraciones de archivo del usuario
					this.payrollFileConfiguration = iPayrollFileConfigurationJpaRepository
							.findByContribuyente(this.taxpayer);

					// ##### Subiendo el archivo al servidor
					String payrollUploadPath = folderAbsolutePath + File.separator + payrollFileName + "."
							+ payrollFileExt;
					UFile.writeFile(UFile.fileToByte(payrollFileUpload), payrollUploadPath);

					// ##### Generando informacion del archivo
					ArchivoNominaEntity payrollFileEntity = new ArchivoNominaEntity();
					payrollFileEntity.setContribuyente(this.taxpayer);
					payrollFileEntity.setTipoNomina(payrollTypeEntity);

					String fileTypeCode = payrollFileExt.equalsIgnoreCase("txt") ? FILE_TYPE_TXT : FILE_TYPE_CSV;
					TipoArchivoEntity fileType = iFileTypeJpaRepository.findByCodigoAndEliminadoFalse(fileTypeCode);
					payrollFileEntity.setTipoArchivo(fileType);

					EstadoArchivoEntity fileStatus = iFileStatusJpaRepository
							.findByCodigoAndEliminadoFalse(FILE_STATUS_QUEUED);
					payrollFileEntity.setEstadoArchivo(fileStatus);

					payrollFileEntity.setNombre(payrollFileName);

					String payrollDbPath = folderPath + File.separator + payrollFileName + "." + payrollFileExt;
					payrollFileEntity.setRuta(payrollDbPath);

					payrollFileEntity.setFechaCarga(new Date());
					payrollFileEntity.setPeriodo(period);
					payrollFileEntity.setEjercicio(UValue.integerString(UDateTime.yearOfDate(new Date())));
					payrollFileEntity.setEliminado(Boolean.FALSE);

					// ##### Verificar si hay algun otro archivo procesandose
					boolean anotherPayrollFileProcessing = false;
					List<ArchivoNominaEntity> payrollFileList = iPayrollFileJpaRepository
							.findByContribuyenteAndEliminadoFalse(this.taxpayer);
					for (ArchivoNominaEntity item : payrollFileList) {
						if (item.getEstadoArchivo().getCodigo().equalsIgnoreCase("P")) {
							anotherPayrollFileProcessing = true;
							break;
						}
					}

					boolean canUploadPayroll = userTools.canUploadPayroll();

					if (!anotherPayrollFileProcessing) {
						// ##### Cargando informacion del emisor para la generacion de los comprobantes
						// y el timbrado
						ApiEmitter apiEmitter = new ApiEmitter();
						apiEmitter
								.setRfc(UValue.stringValueUppercase(userTools.getContribuyenteActive().getRfcActivo()));
						apiEmitter.setNombreRazonSocial(
								userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo())
										.getNombreRazonSocial());
						apiEmitter.setCorreoElectronico(
								UValue.stringValue(userTools.getCurrentUser().getCorreoElectronico()));

						ContribuyenteRegimenFiscalEntity taxpayerTaxRegime = iTaxpayerTaxRegimeJpaRepository
								.findByContribuyente(this.taxpayer);
						apiEmitter.setRegimenFiscal(taxpayerTaxRegime.getRegimenFiscal());

						apiEmitter.setCertificado(this.certificateByte);
						apiEmitter.setLlavePrivada(this.privateKeyByte);
						apiEmitter.setPasswd(this.keyPasswd);

						// ##### Cargando informacion para procesar el fichero
						ApiPayrollStamp payrollStamp = new ApiPayrollStamp();
						payrollStamp.setTaxpayer(this.taxpayer);
						payrollStamp.setUser(user);
						payrollStamp.setIp(sessionService.getIp());
						payrollStamp.setUrlAcceso("payroll/PayrollMaster");
						payrollStamp.setApiEmitter(apiEmitter);
						payrollStamp.setPayrollFileConfiguration(this.payrollFileConfiguration);
						payrollStamp.setPayrollFile(payrollFileEntity);
						payrollStamp.setStampProperties(this.stampProperties);
						payrollStamp.setStampDetail(this.stampDetail);
						payrollStamp.setAbsolutePath(folderAbsolutePath);
						payrollStamp.setUploadPath(payrollUploadPath);
						payrollStamp.setDbPath(folderPath);

						if (canUploadPayroll || stampControl.stampStatus(taxpayer)) {
							// ##### Procesar el fichero
							payrollStampService.payrollProcessingAsync(payrollStamp, canUploadPayroll);

							// Incrementamos tamaño de archivos dubidos
							ContribuyenteEntity tp = userTools.getContribuyenteActive();
							int archivosSubidos = tp.getNominasSubidas();
							archivosSubidos++;
							tp.setNominasSubidas(archivosSubidos);
							persistenceDAO.update(tp);
							// Validar si puede subir archivos

							response.setSuccess(Boolean.TRUE);

							// ##### Notificacion Toastr
							Toastr toastr = new Toastr();
							toastr.setTitle(UProperties.getMessage(
									"mega.cfdi.notification.toastr.payroll.file.upload.title", userTools.getLocale()));
							toastr.setMessage(
									UProperties.getMessage("mega.cfdi.notification.toastr.payroll.file.upload.message",
											userTools.getLocale()));
							toastr.setType(Toastr.ToastrType.SUCCESS.value());

							// ##### Mensaje de notificacion
							notification = new JNotification();
							notification.setToastrNotification(Boolean.TRUE);
							notification.setToastr(toastr);
							if (!userTools.canUploadPayroll())
								response.setUploadLimit(true);
							response.setNotification(notification);

						} else {
							notification.setMessage(UProperties.getMessage("ERR0184", userTools.getLocale()));
							notification.setModalPanelMessage(Boolean.TRUE);
							notification.setCssStyleClass(CssStyleClass.ERROR.value());

							response.setUploadLimit(true);
							response.setNotification(notification);
							response.setError(Boolean.TRUE);
							response.setErrors(errors);
						}
					} else {
						if (canUploadPayroll || stampControl.stampStatus(taxpayer)) {
							// ##### Registrando archivo en DB
							persistenceDAO.persist(payrollFileEntity);

							// Incrementamos tamaño de archivos subidos
							ContribuyenteEntity tp = userTools.getContribuyenteActive();
							int archivosSubidos = tp.getNominasSubidas();
							archivosSubidos++;
							tp.setNominasSubidas(archivosSubidos);
							persistenceDAO.update(tp);
							// Validar si puede subir archivos

							response.setSuccess(true);
							// ##### Notificacion Toastr

							Toastr toastr = new Toastr();
							toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.generate.cfdi.title",
									userTools.getLocale()));
							toastr.setMessage(UProperties.getMessage(
									"mega.cfdi.notification.toastr.generate.cfdi.message", userTools.getLocale()));
							toastr.setType(Toastr.ToastrType.SUCCESS.value());

							// ##### Mensaje de notificacion
							notification = new JNotification();
							notification.setToastrNotification(Boolean.TRUE);
							notification.setToastr(toastr);
							if (!userTools.canUploadPayroll())
								response.setUploadLimit(true);
							response.setNotification(notification);
						} else {
							notification.setMessage(UProperties.getMessage("ERR0184", userTools.getLocale()));
							notification.setModalPanelMessage(Boolean.TRUE);
							notification.setCssStyleClass(CssStyleClass.ERROR.value());

							response.setUploadLimit(true);
							response.setNotification(notification);
							response.setError(Boolean.TRUE);
							response.setErrors(errors);
						}
					}
				} else {
					response.setError(Boolean.TRUE);
					response.setErrors(errors);
				}
			} else {
				response.setSuccess(false);
				// ##### Notificacion Toastr

				notification.setMessage(UProperties.getMessage("ERR0163", userTools.getLocale()));
				notification.setModalPanelMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				response.setUploadLimit(true);

				response.setNotification(notification);
				response.setError(Boolean.TRUE);
				response.setErrors(errors);
			}
		} catch (Exception e) {
			notification.setMessage(UProperties.getMessage("ERR0162", userTools.getLocale()));
			notification.setModalPanelMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());

			response.setNotification(notification);
			response.setError(Boolean.TRUE);
			response.setErrors(errors);
		}

		sessionController.sessionUpdate();

		payrollFile.setResponse(response);

		val jsonInString = gson.toJson(payrollFile);
		return jsonInString;
	}

	@ResponseBody
	@RequestMapping(value = "/payroll/file/download/{name}/{type}", method = RequestMethod.GET)
	public ModelAndView download(@PathVariable(value = "name") String name, @PathVariable(value = "type") String type,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {

			String repositoryPath = appSettings.getPropertyValue("cfdi.payroll.path");
			ArchivoNominaEntity payrollFile = iPayrollFileJpaRepository
					.findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(this.taxpayer, name);
			if (!UValidator.isNullOrEmpty(payrollFile)) {
				String absolutePath = repositoryPath + File.separator;
				if (type.equalsIgnoreCase("compacted")) {
					absolutePath = absolutePath + payrollFile.getRutaCompactado();
				} else if (type.equalsIgnoreCase("consolidated")) {
					absolutePath = absolutePath + payrollFile.getRutaConsolidado();
				} else if (type.equalsIgnoreCase("error")) {
					absolutePath = absolutePath + payrollFile.getRutaError();
				}

				File file = new File(absolutePath);
				byte[] fileByte = UFile.fileToByte(file);

				response.setContentType("application/xml");
				response.setContentLength(fileByte.length);
				response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

				FileCopyUtils.copy(fileByte, response.getOutputStream());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		sessionController.sessionUpdate();

		return null;
	}
	
	@PostMapping(value = "/payrollStamp", params = { "queuedFileRemove" })
	@ResponseBody
	public String queuedFileRemove(HttpServletRequest request) {
		JResponse response = null;
		
		val fileId = Long.valueOf(UBase64.base64Decode(request.getParameter("id")));
		val exists = payrollFileService.findOne(fileId, FileStatus.QUEUED) != null;
		if (exists) {
			payrollFileService.delete(fileId);
			val toastr = Toastr.success("Eliminar archivo", "El archivo ha sido eliminado satisfactoriamente");
			val notification = JNotification.toastrNotification(toastr);
			response = JResponse.success(notification);
		} else {
			val notification = JNotification.pageMessageError("El archivo especificado no existe");
			response = JResponse.error(notification);
		}
		
		sessionController.sessionUpdate();
		val customResponse = new JCustomResponse(response);
		return GsonClient.response(customResponse);
	}
	
}