package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentOriginalStringValidatorImpl implements PaymentOriginalStringValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Override
	public String getOriginalString(String originalString, String stringType) {
		if (UValidator.isNullOrEmpty(originalString)) {
			if (!UValidator.isNullOrEmpty(stringType)) {
				return null;
			}
		} else {
			if (UValidator.isNullOrEmpty(stringType)) {
				return null;
			}
		}
		return originalString;
	}

	@Override
	public String getOriginalString(String originalString, String field, String stringType, List<JError> errors) {
		JError error = null;
		if (UValidator.isNullOrEmpty(originalString)) {
			if (!UValidator.isNullOrEmpty(stringType)) {
				error = JError.required(field, userTools.getLocale());
			}
		} else {
			if (UValidator.isNullOrEmpty(stringType)) {
				error = JError.error(field, "ERR1006", userTools.getLocale());
			}
		}

		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return originalString;
	}

}