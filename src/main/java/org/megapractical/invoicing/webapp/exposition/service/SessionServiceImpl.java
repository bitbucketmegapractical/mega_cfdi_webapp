package org.megapractical.invoicing.webapp.exposition.service;

import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import lombok.val;

@Service
public class SessionServiceImpl implements SessionService {

	@Override
	public String getIp() {
		val request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		return request.getRemoteAddr();
	}

}