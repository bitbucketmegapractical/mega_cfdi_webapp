package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentDateValidator {
	
	String validate(String paymentDate);
	
	String validate(String paymentDate, String field, List<JError> errors);
	
}