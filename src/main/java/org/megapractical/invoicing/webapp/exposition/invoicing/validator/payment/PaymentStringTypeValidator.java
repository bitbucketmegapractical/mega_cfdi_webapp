package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.StringTypeResponse;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentStringTypeValidator {

	StringTypeResponse getStringType(String stringType, FormaPagoEntity paymentWay);
	
	StringTypeResponse getStringType(String stringType, String field, FormaPagoEntity paymentWay, List<JError> errors);

}