package org.megapractical.invoicing.webapp.exposition.invoicing.validator.emitter;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiEmitter;
import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.megapractical.invoicing.dal.data.repository.IPersonTypeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.TaxRegimeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.EmitterTypeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.TaxRegimeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ValidationResponse;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JEmitter;
import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterCertificateRequest;
import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterCertificateValidationResponse;
import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterCertificateValidationResponse.JEmitterCertificateDataResponse;
import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterCertificateValidationResponse.JEmitterCertificateDatesResponse;
import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterCertificateValidationResponse.JEmitterCertificateFileResponse;
import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.support.EmitterTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.RfcUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class EmitterValidatorImpl implements EmitterValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private EmitterTools emitterTools;
	
	@Autowired
	private TaxRegimeValidator taxRegimeValidator;
	
	@Autowired
	private IPersonTypeJpaRepository iPersonTypeJpaRepository;
	
	private static final String ERR1001 = "ERR1001";
	private static final String CERTIFICATE_FIELD = "certificate";
	
	@Override
	public JEmitterResponse getEmitter(String emitterData, 
									   JEmitterCertificateRequest emitterCertificateRequest,
									   List<JError> errors) {
		val emitter = GsonClient.deserialize(emitterData, JEmitter.class);
		
		val rfcResponse = getRfc(emitter, errors);
		val rfcError = rfcResponse.isError();
		
		val emitterTypeResponse = getEmitterType(emitter, errors);
		val emitterTypeError = emitterTypeResponse.isError();
		
		val taxRegimeResponse = getTaxRegime(emitter, errors);
		val taxRegime = taxRegimeResponse.getTaxRegime();
		val taxRegimeError = taxRegimeResponse.isError();
		
		val emitterCertificateResponse = getEmitterCertificate(emitter, taxRegime, emitterCertificateRequest, errors);
		var emitterCertificateError = false;
		val emitterCertificateDataResponse = emitterCertificateResponse.getEmitterCertificate();
		if (emitterCertificateDataResponse != null) {
			emitterCertificateError = emitterCertificateDataResponse.isError();
		}
		
		if (!rfcError && !emitterTypeError && !taxRegimeError && !emitterCertificateError) {
			val emitterCertificate = emitterCertificateResponse.getEmitterCertificate();
			
			val apiEmitter = new ApiEmitter();
			apiEmitter.setRfc(UValue.stringValueUppercase(userTools.getContribuyenteActive().getRfcActivo()));
			apiEmitter.setNombreRazonSocial(userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo()).getNombreRazonSocial());
			apiEmitter.setCorreoElectronico(UValue.stringValue(userTools.getCurrentUser().getCorreoElectronico()));
			apiEmitter.setRegimenFiscal(taxRegime);
			apiEmitter.setCert(emitterCertificate.getCertificate());
			apiEmitter.setCertificado(emitterCertificate.getCertificateByte());
			apiEmitter.setCertificateCreate(emitterCertificate.getCreatedAt());
			apiEmitter.setCertificateExpired(emitterCertificate.getExpiredOn());
			apiEmitter.setPrivateKey(emitterCertificate.getPrivateKey());
			apiEmitter.setLlavePrivada(emitterCertificate.getPrivateKeyByte());
			apiEmitter.setPasswd(emitterCertificate.getKeyPasswd());
			apiEmitter.setLogo(emitterTools.getLogo());
			
			return new JEmitterResponse(apiEmitter);
		}
		return JEmitterResponse.empty();
	}
	
	private final ValidationResponse getRfc(JEmitter emitter, List<JError> errors) {
		String errorCode = null;
		
		val rfc = UBase64.base64Decode(emitter.getRfc());
		if (UValidator.isNullOrEmpty(rfc)) {
			errorCode = ERR1001;
		} else { 
			val rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			if (rfc.length() < RfcUtils.RFC_MIN_LENGTH || rfc.length() > RfcUtils.RFC_MAX_LENGTH || !rfc.matches(RfcUtils.RFC_EXPRESSION)) {
				errorCode = RfcUtils.RFC_ERROR_CODE;
			} else if (!rfc.equals(rfcActive)) {
				errorCode = "ERR0001";
			}
		}
		
		var rfcError = false;
		if (!UValidator.isNullOrEmpty(errorCode)) {
			val error = JError.error("emitter-rfc-active", errorCode, userTools.getLocale());
			errors.add(error);
			rfcError = true;
		}
		
		return new ValidationResponse(rfc, rfcError);
	}
	
	private final EmitterTypeResponse getEmitterType(JEmitter emitter, List<JError> errors) {
		String errorCode = null;
		TipoPersonaEntity emitterTypeEntity = null;
		
		val emitterType = UBase64.base64Decode(emitter.getPersonType());
		if (UValidator.isNullOrEmpty(emitterType)) {
			errorCode = ERR1001;
		} else {
			emitterTypeEntity = iPersonTypeJpaRepository.findByValorAndEliminadoFalse(emitterType);
			if (emitterTypeEntity == null) {
				errorCode = "ERR0002";
			}
		}
		
		var emitterTypeError = false;
		if (!UValidator.isNullOrEmpty(errorCode)) {
			val error = JError.error("emitter-person-type", errorCode, userTools.getLocale());
			errors.add(error);
			emitterTypeError = true;
		}
		
		return new EmitterTypeResponse(emitterTypeEntity, emitterTypeError);
	}
	
	private final TaxRegimeResponse getTaxRegime(JEmitter emitter, List<JError> errors) {
		var taxRegime = UBase64.base64Decode(emitter.getTaxRegime());
		val field = "emitter-tax-regime";
		val validatorResponse = taxRegimeValidator.validate(taxRegime, field);
		var taxRegimeEntity = validatorResponse.getTaxRegime();
		var error = validatorResponse.getError();
		
		if (!validatorResponse.isHasError() && taxRegimeEntity != null) {
			val emitterType = UBase64.base64Decode(emitter.getPersonType());
			val taxRegimeCode = taxRegimeEntity.getCodigo();
			val validatorByPersonTypeResponse = taxRegimeValidator.validate(emitterType, taxRegimeCode, field);
			taxRegimeEntity = validatorByPersonTypeResponse.getTaxRegime();
			error = validatorByPersonTypeResponse.getError();
		}
		
		var taxRegimeError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			taxRegimeError = true;
		}
		
		return new TaxRegimeResponse(taxRegimeEntity, taxRegimeError);
	}
	
	private final JEmitterCertificateValidationResponse getEmitterCertificate(JEmitter emitter,
																			  RegimenFiscalEntity emitterType,
																			  JEmitterCertificateRequest emitterCertificateRequest, 
																			  List<JError> errors) {
		JEmitterCertificateValidationResponse emitterCertificateValidationResponse = null;
		val emitterCertificate = emitterCertificateRequest.getEmitterCertificate();
		if (UValidator.isNullOrEmpty(emitterCertificate)) {
			val certificateData = getCertificate(emitterCertificateRequest.getCertificateMultipart(), errors);
			val certificateError = certificateData.isError();
			
			val privateKeyData = getPrivateKey(emitterCertificateRequest.getPrivateKeyMultipart(), errors);
			val privateKeyError = privateKeyData.isError();
			
			if (!certificateError && !privateKeyError) {
				emitterCertificateValidationResponse = getCertificateData(emitter, emitter.getRfc(), emitterType, certificateData, privateKeyData, errors);
			} else {
				val emitterCertificateDataResponse = JEmitterCertificateDataResponse.builder().error(Boolean.TRUE).build();
				emitterCertificateValidationResponse = JEmitterCertificateValidationResponse
														.builder()
														.emitterCertificate(emitterCertificateDataResponse)
														.build();
			}
		} else {
			val emitterCertificateDataResponse = JEmitterCertificateDataResponse
													.builder()
													.certificateByte(emitterCertificate.getCertificado())
													.privateKeyByte(emitterCertificate.getLlavePrivada())
													.keyPasswd(UBase64.base64Decode(emitterCertificate.getClavePrivada()))
													.createdAt(emitterCertificate.getFechaCreacion())
													.expiredOn(emitterCertificate.getFechaExpiracion())
													.build();
			
			emitterCertificateValidationResponse = JEmitterCertificateValidationResponse
													.builder()
													.emitterCertificate(emitterCertificateDataResponse)
													.build();
		}
		return emitterCertificateValidationResponse;
	}
	
	private final JEmitterCertificateFileResponse getCertificate(Optional<MultipartFile> certificateMultipart, List<JError> errors) {
		File certificate = null;
		String errorCode = null;
		var isError = false;
		if (certificateMultipart.isPresent()) {
			val multipartFileCertificate = certificateMultipart.get();
			val certificateName = multipartFileCertificate.getOriginalFilename();
			val certificateExt = UFile.getExtension(certificateName);
			if (certificateExt == null || !certificateExt.equalsIgnoreCase("cer")) {
				errorCode = "ERR1005";
			} else {
				certificate = UFile.multipartFileToFile(multipartFileCertificate);
			}
		} else {
			errorCode = ERR1001;
		}
		
		if (!UValidator.isNullOrEmpty(errorCode)) {
			val error = JError.error(CERTIFICATE_FIELD, errorCode, userTools.getLocale());
			errors.add(error);
			isError = true;
		}
		
		return new JEmitterCertificateFileResponse(certificate, isError);
	}
	
	private final JEmitterCertificateFileResponse getPrivateKey(Optional<MultipartFile> privateKeyMultipart, List<JError> errors) {
		File privateKey = null;
		String errorCode = null;
		var isError = false;
		if (privateKeyMultipart.isPresent()) {
			val multipartFilePrivateKey = privateKeyMultipart.get();
			val privateKeyName = multipartFilePrivateKey.getOriginalFilename();
			val privateKeyExt = UFile.getExtension(privateKeyName);
			if (privateKeyExt == null || !privateKeyExt.equalsIgnoreCase("key")) {
				errorCode = "ERR1005";
			} else {
				privateKey = UFile.multipartFileToFile(multipartFilePrivateKey);
			}
		} else {
			errorCode = ERR1001;
		}
		
		if (!UValidator.isNullOrEmpty(errorCode)) {
			val error = JError.error("private-key", errorCode, userTools.getLocale());
			errors.add(error);
			isError = true;
		}
		
		return new JEmitterCertificateFileResponse(privateKey, isError);
	}
	
	private final JEmitterCertificateValidationResponse getCertificateData(JEmitter emitter, 
																		   String emitterRfc,
																		   RegimenFiscalEntity emitterType, 
																		   JEmitterCertificateFileResponse certificateResponse,
																		   JEmitterCertificateFileResponse privateKeyResponse, 
																		   List<JError> errors) {
		JError error = null;
		JResponse response = null;
		JEmitterCertificateDataResponse emitterCertificateDataResponse = null;
		
		val passwd = UBase64.base64Decode(emitter.getPasswd());
		if (UValidator.isNullOrEmpty(passwd)) {
			error = JError.required("passwd", userTools.getLocale());
		} else {
			val certificateDates = getCertificateDates(certificateResponse, privateKeyResponse, passwd, emitterType, emitterRfc);
			response = certificateDates.getResponse();
			error = certificateDates.getError();
			
			emitterCertificateDataResponse = JEmitterCertificateDataResponse
												.builder()
												.certificate(certificateResponse.getFile())
												.certificateByte(getCertificateByte(certificateResponse.getFile()))
												.privateKey(privateKeyResponse.getFile())
												.privateKeyByte(getCertificateByte(privateKeyResponse.getFile()))
												.keyPasswd(passwd)
												.createdAt(certificateDates.getCreatedAt())
												.expiredOn(certificateDates.getExpiredOn())
												.error(error != null)
												.build();
		}
		
		var isError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			isError = true;
		}
		
		return JEmitterCertificateValidationResponse
				.builder()
				.emitterCertificate(emitterCertificateDataResponse)
				.response(response)
				.error(isError)
				.build();
	}
	
	private final JEmitterCertificateDatesResponse getCertificateDates(JEmitterCertificateFileResponse certificateResponse,
																	   JEmitterCertificateFileResponse privateKeyResponse,
																	   String passwd,
																	   RegimenFiscalEntity emitterType, 
																	   String emitterRfc) {
		JError error = null;
		
		if (!certificateResponse.isError() && !privateKeyResponse.isError()) {
			val certificate = certificateResponse.getFile();
			val privateKey = privateKeyResponse.getFile();
			val certificateValidate = UCertificateX509.certificateValidate(certificate, privateKey, passwd);
			if (!certificateValidate.equals("VALIDATE_SUCCESSFULY")) {
				error = determineCertificateValidationError(certificateValidate);
				return JEmitterCertificateDatesResponse.builder().error(error).build();
			} else {
				val certificateDatesResponse = getCertificateDates(certificate, emitterType, emitterRfc);
				val expiredOn = certificateDatesResponse.getExpiredOn();
				if (UValidator.isNullOrEmpty(expiredOn) || UDateTime.isBeforeLocalDate(expiredOn, new Date())) {
					error = JError.error(CERTIFICATE_FIELD, "ERR0004", userTools.getLocale());
					val notification = JNotification.pageMessageError(error.getMessage());
					val response = JResponse.builder().notification(notification).build();
					certificateDatesResponse.setResponse(response);
				}
				return certificateDatesResponse;
			}
		}
		return JEmitterCertificateDatesResponse.empty();
	}
	
	private final JError determineCertificateValidationError(String certificateValidate) {
		if (certificateValidate.equals("CERTIFICATE_AND_PRIVATE_KEY_DOES_NOT_CORRESPOND")) {
			return JError.error(CERTIFICATE_FIELD, "ERR0005", userTools.getLocale());
		} else if (certificateValidate.equals("BAD_PADDING_EXCEPTION")) {
			return JError.error("passwd", "ERR0006", userTools.getLocale());
		}
		return null;
	}
	
	private final JEmitterCertificateDatesResponse getCertificateDates(File certificate, 
										   							   RegimenFiscalEntity emitterType, 
										   							   String emitterRfc) {
		JError error = null;
		Date createdAt = null;
		Date expiredOn = null;
		// Validando que el certificado no sea fiel
		if (UCertificateX509.isFiel(certificate)) {
			// Es fiel
			error = JError.error(CERTIFICATE_FIELD, "ERR0003", userTools.getLocale());
		} else {
			createdAt = UCertificateX509.certificateCreationDate(certificate);
			expiredOn = UCertificateX509.certificateExpirationDate(certificate);
			if (UDateTime.isBeforeLocalDate(expiredOn, new Date())) {
				error = JError.error(CERTIFICATE_FIELD, "ERR0004", userTools.getLocale());
			} else {
				if (emitterType != null && ((emitterType.getCodigo().equals("PF")
						&& !emitterRfc.equals(UCertificateX509.getCertPersonRFC(certificate)))
						|| (emitterType.getCodigo().equals("PM") && !emitterRfc.equals(
								UCertificateX509.getCertLegalRepresentativePersonRFC(certificate))))) {
					error = JError.error(CERTIFICATE_FIELD, "ERR0078", userTools.getLocale());
				}
			}
		}
		
		return JEmitterCertificateDatesResponse
				.builder()
				.createdAt(createdAt)
				.expiredOn(expiredOn)
				.error(error)
				.build();
	}
	
	private final byte[] getCertificateByte(File file) {
		try {
			return UFile.fileToByte(file);
		} catch (IOException e) {
			return new byte[0];
		}
	}

}