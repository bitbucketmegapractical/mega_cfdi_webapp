package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.math.BigDecimal;
import java.util.List;

import org.megapractical.invoicing.api.util.UCalculation;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherTotalsRequest;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherTotalsValidatorImpl implements VoucherTotalsValidator {
	
	@Autowired
	private UserTools userTools;
	
	private static final String VOUCHER_SUBTOTAL_FIELD = "voucher-subtotal";
	private static final String INVALID_FORMAT_CODE = "ERR1003";
	
	@Override
	public boolean validateTotals(String subTotal, String total, List<JError> errors) {
		var isError = false;
		if (UValidator.isNullOrEmpty(subTotal)) {
			val error = JError.required(VOUCHER_SUBTOTAL_FIELD, userTools.getLocale());
			errors.add(error);
			isError = true;
		}
		
		if (UValidator.isNullOrEmpty(total)) {
			val error = JError.required("voucher-total", userTools.getLocale());
			errors.add(error);
			isError = true;
		}
		
		return isError;
	}

	@Override
	public boolean validateTotals(VoucherTotalsRequest voucherTotalsRequest, List<JError> errors) {
		val subtotalError = validateSubtotal(voucherTotalsRequest, errors);
		val discountError = validateDiscount(voucherTotalsRequest, subtotalError, errors);
		val taxTransferredError = validateTotalTaxTransferred(voucherTotalsRequest, errors);
		val taxWithheldError = validateTotalTaxWithheld(voucherTotalsRequest, errors);
		val totalError = validateTotal(voucherTotalsRequest, errors);
		
		return subtotalError || discountError || taxTransferredError || taxWithheldError || totalError;
	}
	
	private boolean validateSubtotal(VoucherTotalsRequest voucherTotalsRequest, List<JError> errors) {
		JError error = null;
		
		val voucherType = voucherTotalsRequest.getVoucherType();
		val currency = voucherTotalsRequest.getCurrency();
		val voucherSubtotal = voucherTotalsRequest.getVoucherSubtotal();
		val voucherTotalsSubtotal = voucherTotalsRequest.getVoucherTotalsSubtotal();
		
		if (!UValidator.isDouble(voucherTotalsSubtotal)) {
			error = JError.error(VOUCHER_SUBTOTAL_FIELD, INVALID_FORMAT_CODE, userTools.getLocale());
		} else {
			BigDecimal subtotal = UValue.bigDecimalStrict(voucherTotalsSubtotal.toString(), UTools.decimalValue(voucherTotalsSubtotal.toString()));
			if (!UValidator.isValidDecimalPart(subtotal, currency.getDecimales())) {
				error = JError.error(VOUCHER_SUBTOTAL_FIELD, Validator.ErrorSource.fromValue("CFDI33106").value(), userTools.getLocale());
			} else {
				error = validateSubtotal(voucherSubtotal, voucherTotalsSubtotal, voucherType);								
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			return true;
		}
		return false;
	}
	
	private JError validateSubtotal(BigDecimal voucherSubtotal, BigDecimal voucherTotalsSubtotal, String voucherType) {
		JError error = null;
		
		//CFDI33107: El TipoDeComprobante es I,E o N, el importe registrado en el campo Subtotal no es igual a la suma de los importes de los conceptos registrados.
		//CFDI33108: El TipoDeComprobante es T o P y el importe no es igual a 0, o cero con decimales.
		if (voucherType.equals(VoucherUtils.VOUCHER_TYPE_CODE_I) || voucherType.equals(VoucherUtils.VOUCHER_TYPE_CODE_E) || voucherType.equals(VoucherUtils.VOUCHER_TYPE_CODE_N)) {
			if (!voucherTotalsSubtotal.equals(voucherSubtotal)) {
				error = JError.error(VOUCHER_SUBTOTAL_FIELD, Validator.ErrorSource.fromValue("CFDI33107").value(), userTools.getLocale());
			}
		} else if (voucherType.equals(VoucherUtils.VOUCHER_TYPE_CODE_T) || voucherType.equals(VoucherUtils.VOUCHER_TYPE_CODE_P)) {
			String valueNull = null;
			val zeroDecimal = UValue.bigDecimal(valueNull); 
			if (!voucherTotalsSubtotal.equals(BigDecimal.ZERO) && !voucherTotalsSubtotal.equals(zeroDecimal)) {
				error = JError.error(VOUCHER_SUBTOTAL_FIELD, Validator.ErrorSource.fromValue("CFDI33108").value(), userTools.getLocale());
			}
		}
		return error;
	}
	
	private boolean validateDiscount(VoucherTotalsRequest voucherTotalsRequest, 
									 boolean subtotalError,
									 List<JError> errors) {
		JError error = null;
		val field = "voucher-discount";
		
		val discountByConcept = voucherTotalsRequest.isDiscountByConcept();
		val voucherTotalsDiscount = voucherTotalsRequest.getVoucherTotalsDiscount();
		val voucherTotalsSubtotal = voucherTotalsRequest.getVoucherTotalsSubtotal();
		val currency = voucherTotalsRequest.getCurrency();
		
		if (!discountByConcept && !UValidator.isNullOrEmpty(voucherTotalsDiscount)) {
			error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33110").value(), userTools.getLocale());
		} else if (discountByConcept && UValidator.isNullOrEmpty(voucherTotalsDiscount)) {
			error = JError.error(field, "ERR0025", userTools.getLocale());
		} else if (discountByConcept && !UValidator.isNullOrEmpty(voucherTotalsDiscount)) {
			if (!UValidator.isDouble(voucherTotalsDiscount)) {
				error = JError.error(field, INVALID_FORMAT_CODE, userTools.getLocale());
			} else {
				error = validateDiscount(voucherTotalsDiscount, voucherTotalsSubtotal, subtotalError, currency);
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			return true;
		}
		return false;
	}
	
	private JError validateDiscount(BigDecimal voucherTotalsDiscount, 
									BigDecimal voucherTotalsSubtotal,
									boolean subtotalError, 
									MonedaEntity currency) {
		JError error = null;
		val field = "voucher-discount";
		
		val discount = UValue.bigDecimalStrict(voucherTotalsDiscount.toString(), UTools.decimalValue(voucherTotalsDiscount.toString()));
		if (!UValidator.isValidDecimalPart(discount, currency.getDecimales())) {
			error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33111").value(), userTools.getLocale());
		} else {
			if (!subtotalError) {
				if (voucherTotalsDiscount.compareTo(voucherTotalsSubtotal) > 0) {
					error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33109").value(), userTools.getLocale());
				}
			} else {
				if (!voucherTotalsDiscount.equals(discount)) {
					error = JError.error(field, "ERR0027", userTools.getLocale());
				}
			}
		}
		return error;
	}
	
	private boolean validateTotalTaxTransferred(VoucherTotalsRequest voucherTotalsRequest, List<JError> errors) {
		JError error = null;
		val field = "voucher-tax-transferred-total";
		
		val conceptsTax = voucherTotalsRequest.getConceptsTax();
		val currency = voucherTotalsRequest.getCurrency();
		
		if (conceptsTax != null && conceptsTax.getTotalTaxTransferred() != null) {
			val voucherTotalsTaxesTransferred = conceptsTax.getTotalTaxTransferred();
			if (!UValidator.isDouble(voucherTotalsTaxesTransferred)) {
				error = JError.error(field, INVALID_FORMAT_CODE, userTools.getLocale());
			} else {
				val voucherTaxTransferred = UValue.bigDecimalStrict(voucherTotalsTaxesTransferred.toString(), UTools.decimalValue(voucherTotalsTaxesTransferred.toString()));
				if (!UValidator.isValidDecimalPart(voucherTaxTransferred, currency.getDecimales())) {
					error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33182").value(), userTools.getLocale());
				}
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			return true;
		}
		return false;
	}
	
	private boolean validateTotalTaxWithheld(VoucherTotalsRequest voucherTotalsRequest, List<JError> errors) {
		JError error = null;
		val field = "voucher-tax-withheld-total";
		
		val conceptsTax = voucherTotalsRequest.getConceptsTax();
		val currency = voucherTotalsRequest.getCurrency();
		
		if (conceptsTax != null && conceptsTax.getTotalTaxWithheld() != null) {
			val voucherTotalsTaxesWithheld = conceptsTax.getTotalTaxWithheld();
			if (!UValidator.isDouble(voucherTotalsTaxesWithheld)) {
				error = JError.error(field, INVALID_FORMAT_CODE, userTools.getLocale());
			} else {
				val voucherTaxWithheld = UValue.bigDecimalStrict(voucherTotalsTaxesWithheld.toString(), UTools.decimalValue(voucherTotalsTaxesWithheld.toString()));
				if (!UValidator.isValidDecimalPart(voucherTaxWithheld, currency.getDecimales())) {
					error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33180").value(), userTools.getLocale());
				}
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			return true;
		}
		return false;
	}
	
	private boolean validateTotal(VoucherTotalsRequest voucherTotalsRequest, List<JError> errors) {
		JError error = null;
		val field = "voucher-total";
		
		val conceptsTax = voucherTotalsRequest.getConceptsTax();
		val voucherSubtotal = voucherTotalsRequest.getVoucherSubtotal();
		val voucherTotalsDiscount = voucherTotalsRequest.getVoucherTotalsDiscount();
		val voucherTotalsTaxesTransferred = conceptsTax.getTotalTaxTransferred();
		val voucherTotalsTaxesWithheld = conceptsTax.getTotalTaxWithheld();
		val voucherTotalsTotal = voucherTotalsRequest.getVoucherTotalsTotal();
		val currency = voucherTotalsRequest.getCurrency();
		
		if (!UValidator.isDouble(voucherTotalsTotal)) {
			error = JError.error(field, INVALID_FORMAT_CODE, userTools.getLocale());
		} else {
			val total = UValue.bigDecimalStrict(voucherTotalsTotal.toString(), UTools.decimalValue(voucherTotalsTotal.toString()));
			if (!UValidator.isValidDecimalPart(total, currency.getDecimales())) {
				error = JError.error(field, "ERR0030", userTools.getLocale());
			} else {
				val voucherTotal = UCalculation.total(voucherSubtotal, voucherTotalsDiscount, voucherTotalsTaxesTransferred, voucherTotalsTaxesWithheld, currency.getDecimales());
				if (!voucherTotal.equals(total)) {
					error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33118").value(), userTools.getLocale());
				}
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			return true;
		}
		return false;
	}

}