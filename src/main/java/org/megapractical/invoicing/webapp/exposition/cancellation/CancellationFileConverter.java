package org.megapractical.invoicing.webapp.exposition.cancellation;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoCancelacionEntity;
import org.megapractical.invoicing.webapp.json.JCfdiFile.CfdiFile;

/**
 * @author Maikel Guerra Ferrer
 *
 */
final class CancellationFileConverter {
	
	private CancellationFileConverter() {		
	}
	
	public static CfdiFile convert(ArchivoCancelacionEntity entity) {
		if (entity == null) {
			return null;
		}
		
		return CfdiFile
				.builder()
				.id(UBase64.base64Encode(UValue.longString(entity.getId())))
				.cfdiType(UBase64.base64Encode("Cancelaciones"))
				.fileType(UBase64.base64Encode(entity.getTipoArchivo().getValor()))
				.fileStatus(UBase64.base64Encode(entity.getEstadoArchivo().getValor()))
				.name(UBase64.base64Encode(entity.getNombre()))
				.path(UBase64.base64Encode(entity.getRuta()))
				.uploadDate(UBase64.base64Encode(UDateTime.format(entity.getFechaCarga())))
				.acusesPath(UBase64.base64Encode(entity.getRutaAcuses()))
				.errorPath(UBase64.base64Encode(entity.getRutaError()))
				.build();
	}
	
}