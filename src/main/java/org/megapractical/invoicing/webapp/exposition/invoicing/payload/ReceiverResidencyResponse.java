package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import org.megapractical.invoicing.dal.bean.jpa.PaisEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReceiverResidencyResponse {
	private String resident;
	private PaisEntity residency;
	private boolean error;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ResidencyResponse {
		private PaisEntity residency;
		private JError error;
	}
	
}