package org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload;

import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JTaxes.JTax;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AmountValidatorResponse {
	private JTax tax;
	private JError error;
}