package org.megapractical.invoicing.webapp.exposition.invoicing.validator.receiver;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JReceiver.JReceiverResponse;

public interface ReceiverValidator {
	
	JReceiverResponse getReceiver(String receiverData, List<JError> errors);
	
}