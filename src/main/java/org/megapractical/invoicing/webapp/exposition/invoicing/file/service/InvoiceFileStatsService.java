package org.megapractical.invoicing.webapp.exposition.invoicing.file.service;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;

public interface InvoiceFileStatsService {

	ArchivoFacturaEstadisticaEntity findByInvoiceFile(ArchivoFacturaEntity invoiceFile);
	
	ArchivoFacturaEstadisticaEntity save(ArchivoFacturaEstadisticaEntity entity);
	
}