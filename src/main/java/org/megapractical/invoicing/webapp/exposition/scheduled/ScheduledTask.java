package org.megapractical.invoicing.webapp.exposition.scheduled;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.RandomStringUtils;
import org.megapractical.invoicing.api.client.CancelationProperties;
import org.megapractical.invoicing.api.client.CancelationProperties.CfdiCancelation;
//import org.megapractical.invoicing.api.client.CfdiStatusProperties;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiSolicitudCancelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComprobanteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.TareaProgramadaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TareaProgramadaLogEntity;
import org.megapractical.invoicing.dal.bean.jpa.TareaProgramadaLogErrorEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiCancelationRequestJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IScheduledTaskJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.async.AsyncService;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WCfdiCheckStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class ScheduledTask {
	
	@Autowired
	AppSettings appSettings;

	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	AsyncService asyncService;
	
	@Autowired
	PropertiesTools propertiesTools;
	
	@Resource
	IScheduledTaskJpaRepository iScheduledTaskJpaRepository; 
	
	@Resource
	ICfdiCancelationRequestJpaRepository iCfdiCancelationRequestJpaRepository;
	
	@Resource
	ICfdiJpaRepository iCfdiJpaRepository;
	
	@Resource
	IProductServiceJpaRepository iProductoServicioJpaRepository;
	
	@Resource
	ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	
	private static final String LANG_DEFAULT = "es_MX";
	
	private static final String ST_CCS = "ST_CCS";
	private static final String ST_CIPC = "ST_CIPC";
	
	//##### INFO ##############################################################################
	//##### Cron expression fields: second, minute, hour, day of month, month, day(s) of week
    //##### (*) means match any
    //##### */X means "every X"
    //##### ? ("no specific value")
	//##########################################################################################
	
	//@Scheduled(cron = "0 0 0/3 * * ?", zone="America/Mexico_City")
	//@Scheduled(fixedRate=180*60*1000, initialDelay=10*60*1000)
	@Scheduled(fixedRate=3600000)
	public void cfdiCheckStatus() throws IOException {
		Date now = new Date();
		Boolean scheduledTaskError = false;
		String scheduledTaskErrorTrace = null;
		
    	TareaProgramadaEntity scheduledTask = null;
    	
    	try {
			    		
    		scheduledTask = iScheduledTaskJpaRepository.findByCodigoAndHabilitadoTrue(ST_CCS);
    		if(!UValidator.isNullOrEmpty(scheduledTask)) {
    			
    			List<CfdiSolicitudCancelacionEntity> sourceList = iCfdiCancelationRequestJpaRepository.findBySolicitudActivaTrue();
    			if(!sourceList.isEmpty()) {
    				String environment = appSettings.getPropertyValue("cfdi.environment");
    				for (CfdiSolicitudCancelacionEntity item : sourceList) {
						String uuid = item.getUuid();
						CfdiEntity cfdi = iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndRutaPdfNotNull(uuid);
						if(!UValidator.isNullOrEmpty(cfdi)) {
							String sessionId = cfdi.getContribuyente().getId().toString();
							String emitterId = sessionId;
							String emitterRfc = cfdi.getContribuyente().getRfc();
							String receiverRfc = cfdi.getReceptorRfc();
							
							String sourceSystem = "CFDI_PLUS";
							ProductoServicioEntity productoServicioEntity = iProductoServicioJpaRepository.findByCodigoAndHabilitadoTrue(userTools.getContribuyenteActive(emitterRfc).getProductoServicioActivo());
							if(!UValidator.isNullOrEmpty(productoServicioEntity)) {
								sourceSystem = productoServicioEntity.getCodigo();
							}
							
							String correlationId = UValue.stringValueUppercase(RandomStringUtils.randomAlphabetic(12));
							
							/*CfdiStatusProperties properties = new CfdiStatusProperties();
							properties.setEmitterId(emitterId);
							properties.setSessionId(sessionId);
							properties.setSourceSystem(sourceSystem);
							properties.setCorrelationId(correlationId);
							properties.setEnvironment(environment);
							properties.setEmitterRfc(emitterRfc);
							properties.setUuid(uuid);
							properties.setServices(propertiesTools.getServices());
							if(propertiesTools.getServices().equals("sw")) {
								properties.setTaxpayer(cfdi.getContribuyente());
								properties.setReceiverRfc(receiverRfc);
								properties.setVoucherTotal(UValue.decimalStringForce(UValue.doubleString(cfdi.getTotal())));
							}
							
							WCfdiCheckStatus wCfdiCheckStatus = new WCfdiCheckStatus();
							wCfdiCheckStatus.setCfdiCancelationRequest(item);
							wCfdiCheckStatus.setProperties(properties);
							asyncService.cfdiCheckStatusAsync(wCfdiCheckStatus);*/
						}
					}
    			}
    			
    			TareaProgramadaLogEntity scheduledTaskLog = new TareaProgramadaLogEntity();
        		scheduledTaskLog.setTareaProgramada(scheduledTask);
        		scheduledTaskLog.setFechaEjecutada(now);
        		scheduledTaskLog.setHoraEjecutada(now);
        		persistenceDAO.persist(scheduledTaskLog);
    		}else {
    			//##### La tarea no existe o no esta habilitada
    			scheduledTaskError = true;
    			scheduledTaskErrorTrace = UProperties.getMessage("ERRSTE001", LANG_DEFAULT);
    			scheduledTaskErrorTrace = scheduledTaskErrorTrace.replace("{1}", ST_CCS);
    		}
    		
		} catch (Exception e) {
			//##### Ha ocurrido un error inesperado
			scheduledTaskError = true;
			scheduledTaskErrorTrace = UProperties.getMessage("ERRSTE002", LANG_DEFAULT);
			scheduledTaskErrorTrace = scheduledTaskErrorTrace.replace("{1}", ST_CCS).replace("{2}", e.getMessage());
		}
    	
    	if(scheduledTaskError) {
    		try {
				
    			TareaProgramadaLogErrorEntity scheduledTaskLogError = new TareaProgramadaLogErrorEntity();
    			scheduledTaskLogError.setTareaProgramada(scheduledTask);
    			scheduledTaskLogError.setFechaError(now);
    			scheduledTaskLogError.setHoraError(now);
    			scheduledTaskLogError.setTrazaError(scheduledTaskErrorTrace);
    			persistenceDAO.persist(scheduledTaskLogError);
    			
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
	}
	
	@Scheduled(fixedRate=3600000)
	public void cancelationInProgressCheck() throws IOException {
		Date now = new Date();
		Boolean scheduledTaskError = false;
		String scheduledTaskErrorTrace = null;
		
    	TareaProgramadaEntity scheduledTask = null;
    	
    	try {
			    		
    		scheduledTask = iScheduledTaskJpaRepository.findByCodigoAndHabilitadoTrue(ST_CIPC);
    		if(!UValidator.isNullOrEmpty(scheduledTask)) {    			    			    			
    			List<CancelationProperties> cancelationProperties = new ArrayList<>();
    			
    			//##### Listado de CFDIs
    			List<CfdiEntity> cfdis = iCfdiJpaRepository.findByCanceladoFalseAndCancelacionEnProcesoTrueAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull();
    			for (CfdiEntity item : cfdis) {
	        		//##### UUID
    				String uuid = item.getUuid();
	        		
					ContribuyenteEntity emitter = null;
					byte[] certificateByte = null;
					byte[] privateKeyByte = null;
					String passwd = null;
					
					//##### Cargando emisor
					emitter = item.getContribuyente();
					
					//##### Cargando certificado del emisor
					ContribuyenteCertificadoEntity certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(emitter);
					
					//##### Verificando vigencia del certificado
					Boolean isError = false;
					if(UValidator.isNullOrEmpty(certificate) || UDateTime.isBeforeLocalDate(certificate.getFechaExpiracion(), new Date())){
						isError = true;
					}else {
						certificateByte = certificate.getCertificado();
						privateKeyByte = certificate.getLlavePrivada();
						passwd = UBase64.base64Decode(certificate.getClavePrivada());
					}
					
					if(!isError){
						CfdiCancelation cfdiCancelation = new CfdiCancelation();
						cfdiCancelation.setReceiverRfc(item.getReceptorRfc());
						cfdiCancelation.setVoucherTotal(UValue.decimalStringForce(UValue.doubleString(item.getTotal())));
						cfdiCancelation.setUuid(uuid);
						
						String correlationId = UValue.stringValueUppercase(RandomStringUtils.randomAlphabetic(12));
						
						CancelationProperties properties = new CancelationProperties();
						properties.setEmitterId(emitter.getId().toString());
						properties.setSessionId(emitter.getId().toString());
						properties.setSourceSystem("CFDI_PLUS");
						properties.setCorrelationId(correlationId);
						properties.setEnvironment(appSettings.getPropertyValue("cfdi.environment"));
						properties.setEmitterRfc(emitter.getRfc());
						properties.setCertificate(certificateByte);
						properties.setPrivateKey(privateKeyByte);
						properties.setPasswd(passwd);
						properties.setCfdiCancelation(cfdiCancelation);
						properties.setServices(propertiesTools.getServices());
						cancelationProperties.add(properties);
					}
	        	}
    			
    			if(!cancelationProperties.isEmpty()) {
					asyncService.cancelationAsync(cancelationProperties);
				}
    			
    			TareaProgramadaLogEntity scheduledTaskLog = new TareaProgramadaLogEntity();
        		scheduledTaskLog.setTareaProgramada(scheduledTask);
        		scheduledTaskLog.setFechaEjecutada(now);
        		scheduledTaskLog.setHoraEjecutada(now);
        		persistenceDAO.persist(scheduledTaskLog);
    		}else {
    			//##### La tarea no existe o no esta habilitada
    			scheduledTaskError = true;
    			scheduledTaskErrorTrace = UProperties.getMessage("ERRSTE001", LANG_DEFAULT);
    			scheduledTaskErrorTrace = scheduledTaskErrorTrace.replace("{1}", ST_CIPC);
    		}
    		
		} catch (Exception e) {
			//##### Ha ocurrido un error inesperado
			scheduledTaskError = true;
			scheduledTaskErrorTrace = UProperties.getMessage("ERRSTE002", LANG_DEFAULT);
			scheduledTaskErrorTrace = scheduledTaskErrorTrace.replace("{1}", ST_CIPC).replace("{2}", e.getMessage());
		}
    	
    	if(scheduledTaskError) {
    		try {
				
    			TareaProgramadaLogErrorEntity scheduledTaskLogError = new TareaProgramadaLogErrorEntity();
    			scheduledTaskLogError.setTareaProgramada(scheduledTask);
    			scheduledTaskLogError.setFechaError(now);
    			scheduledTaskLogError.setHoraError(now);
    			scheduledTaskLogError.setTrazaError(scheduledTaskErrorTrace);
    			persistenceDAO.persist(scheduledTaskLogError);
    			
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
	}
}
