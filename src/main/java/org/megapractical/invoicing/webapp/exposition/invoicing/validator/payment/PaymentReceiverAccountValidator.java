package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentReceiverAccountValidator {

	String getReceiverAccount(String receiverAccount, FormaPagoEntity paymentWay);
	
	String getReceiverAccount(String receiverAccount, String field, FormaPagoEntity paymentWay, List<JError> errors);

}