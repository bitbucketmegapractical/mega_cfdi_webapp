package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.AllowStamp;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface CfdiService {
	
	CfdiEntity findByInvoiceFile(ArchivoFacturaEntity invoiceFile);
	
	boolean isAllowedToStamp(String rfc, String serie, String folio);
	
	AllowStamp verifyCfdi(String rfc, String serie, String folio);
	
	CfdiEntity findByUuid(String uuid);
	
	CfdiEntity findForCancellation(String emitterRfc, String receiverRfc, String uuid);
	
	CfdiEntity save(ApiCfdi apiCfdi);
	
	CfdiEntity save(CfdiEntity cfdi);
	
}