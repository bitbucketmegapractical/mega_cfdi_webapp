package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoucherSerieSheetResponse {
	private ContribuyenteSerieFolioEntity serieSheet;
	private String serie;
	private String sheetStr;
	private boolean allowIndicateSerieSheet;
	private boolean allowIndicateSheet;
	private boolean error;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor	
	public static class JVoucherSerieSheetResponse {
		private ContribuyenteSerieFolioEntity serieSheet;
		private JError error;
	}
	
}