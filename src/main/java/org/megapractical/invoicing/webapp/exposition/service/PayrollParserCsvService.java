package org.megapractical.invoicing.webapp.exposition.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.common.ApiPayrollFileHeader;
import org.megapractical.invoicing.api.common.ApiPayrollFileHeader.PayrollHeader;
import org.megapractical.invoicing.api.common.ApiPayrollFileSubHeader;
import org.megapractical.invoicing.api.common.ApiPayrollFileSubHeader.PayrollSubHeader;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiPayrollError;
import org.megapractical.invoicing.dal.bean.jpa.NominaTramaEntity;
import org.megapractical.invoicing.dal.data.repository.IPayrollEntryJpaRepository;
import org.megapractical.invoicing.webapp.payload.PayrollParserRequest;
import org.megapractical.invoicing.webapp.payload.PayrollParserResponse;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class PayrollParserCsvService {
	
	@Autowired
	PayrollParserValidationService payrollParserValidationService;
	
	@Resource
	IPayrollEntryJpaRepository iPayrollEntryJpaRepository;
	
	private HashMap<String, List<PayrollSubHeader>> payrollSubHeaderMap = new HashMap<>();
	
	public PayrollParserResponse parse(PayrollParserRequest request) {
		// Current environment
		val env = UProperties.env();
				
		//##### Contenido final despues de parsear
    	List<String[]> entriesFinally = new ArrayList<>();
    	
    	//##### Indica la línea del fichero
    	Integer line = 0;
    	
    	//##### Indica la cantidad de nominas del fichero
    	Integer singleEntriesTotal = 0;
    	
    	boolean errorSet = false;
    	
    	List<String[]> entriesAux = new ArrayList<>();
    	
    	ApiPayrollFileHeader payrollFileHeader = new ApiPayrollFileHeader();
    	ApiPayrollFileSubHeader payrollFileSubHeader = new ApiPayrollFileSubHeader();
    	List<PayrollHeader> payrollHeaders = new ArrayList<>();
		
    	val payrollErrors = new ArrayList<ApiPayrollError>();
    	var errorMarked = false;
    	
    	val payrollParser = request.getPayrollParser();
    	val statistics = request.getStatistics();
    	val entries = request.getEntries();
    	
    	for (int i = 0; i < entries.size(); i ++) {
    		//##### Informacion de la línea actual
    		val item = entries.get(i);
    		
    		//##### Incrementenado línea
			line ++;
			
			errorSet = false;
			
			//##### Leyendo header
			if (line == 1) {
				String lineStartsWith = item[0].toLowerCase().trim(); 
				if (!lineStartsWith.startsWith("h01")) {							
					val payrollError = new ApiPayrollError();
					payrollError.setLine(line);
					payrollError.setDescription("El header del fichero no es válido.");
					payrollErrors.add(payrollError);
					
					break;
				} else {
					UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > CSV FILE > READING HEADER...");
					
					var contextPos = 0;
					for (int j = 0; j < item.length; j++) {
						val header = new PayrollHeader();
						
						String headerEntry = null;
						String context = item[j];
						
						if (!UValidator.isNullOrEmpty(context)) {
							String value = context.toLowerCase().trim();
							headerEntry = payrollFileHeader(value);
							
							if (!UValidator.isNullOrEmpty(headerEntry)) {
								header.setEntry(headerEntry);
								header.setStartPosition(contextPos);
							}
						}
						
						boolean isEnd = false;
						if (item[contextPos].equalsIgnoreCase("CEND")) {
							isEnd = true;
						}
						
						if (!isEnd) {
							try {
								
								contextPos = j + 1;
								while (UValidator.isNullOrEmpty(item[contextPos]) && !isEnd) {
									if (!isEnd) {
										contextPos ++;
									}
								}
									
							} catch (Exception e) {
								isEnd = true;
								errorMarked = true;
								e.printStackTrace();
							}
						}
						
						j = !isEnd ? contextPos - 1 : contextPos;
						header.setEndPosition(j);
						payrollHeaders.add(header);
						payrollFileHeader.setPayrollHeaders(payrollHeaders);
						
						if (isEnd) {
							break;
						}
					}
					
					statistics.setPayrollFileHeader(payrollFileHeader);
				}
			}
			
			if (!errorMarked) {
				if (line == 2) {
					UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > CSV FILE > READING SUBHEADER...");
					
					payrollFileSubHeader();
					Integer pos = 0;
					
					for (PayrollHeader header : payrollHeaders) {
						List<PayrollSubHeader> subHeaderList = new ArrayList<>();
						if (payrollSubHeaderMap.containsKey(header.getEntry())) {
							subHeaderList = payrollSubHeaderMap.get(header.getEntry());
						}
						
						int subHeaderSize = subHeaderList.size();
						int headerSize = header.getEndPosition() - header.getStartPosition() + 1;
						
						if (subHeaderSize != headerSize) {
							val payrollError = new ApiPayrollError();
							payrollError.setLine(line);
							payrollError.setDescription("La cantidad de campos del subheader no es válida.");
							payrollErrors.add(payrollError);
							
							errorSet = true;
							break;
						}
						
						for (int j = 0; j < subHeaderSize; j ++) {
							val subHeaderWrapper = subHeaderList.get(j);
							val value = item[pos].toLowerCase().trim();
							
							if (UValidator.isNullOrEmpty(value) || !value.equalsIgnoreCase(subHeaderWrapper.getFieldName())) {
								val payrollError = new ApiPayrollError();
								payrollError.setLine(line);
								payrollError.setFieldValue(value);
								payrollError.setFieldPosition(subHeaderWrapper.getFieldPosition());
								payrollError.setDescription("No se reconoce el valor del campo.");
								payrollErrors.add(payrollError);
								
								errorSet = true;
								break;
							}
							pos ++;
						}
						
						if (errorSet) {
							break;
						}
						
						payrollFileSubHeader.getPayrollSubHeaders().addAll(subHeaderList);
					}
					
					if (errorSet) {
						break;
					}
					
					statistics.setPayrollFileSubHeader(payrollFileSubHeader);
				} else if (line >= 3) {
					if (UValidator.isNullOrEmpty(UValue.stringValue(item[0]))) {
						entriesFinally.addAll(entriesAux);
						statistics.setEntries(entriesFinally);
						statistics.setSingleEntrie(singleEntriesTotal);
						break;
					} else {
						UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > CSV FILE > READING FILE LINE > " + line);
						
						//##### Aumentando el valor de las tramas principales
						//##### para contabilizar el total de comprobantes del fichero
	        			singleEntriesTotal ++;
	        			
	        			//##### Determinando longitud del contenido
	        			int itemSize = item.length - 1;
						while(UValidator.isNullOrEmpty(item[itemSize])) {
							itemSize --;
						}
	        			
						//##### Determinando la longitud del contenido de header
						int headerSize = statistics.getPayrollFileHeader().getPayrollHeaders().get(statistics.getPayrollFileHeader().getPayrollHeaders().size() - 1).getEndPosition();
						
						if (itemSize == headerSize) {
							//##### Cargando contenido de la trama
							entriesAux.add(item);
							
							//##### La linea actual es la linea final del archivo
							if (line == entries.size()) {
								entriesFinally.addAll(entriesAux);
								statistics.setEntries(entriesFinally);
								statistics.setSingleEntrie(singleEntriesTotal);
								break;
							}
						} else {
							val payrollError = new ApiPayrollError();
							payrollError.setLine(line);
							payrollError.setDescription("No se corresponde la cantidad de campos de la línea con la del subheader.");
							payrollErrors.add(payrollError);
						}
					}
				}
			} else {
				val payrollError = new ApiPayrollError();
				payrollError.setLine(line);
				payrollError.setDescription("El header del fichero no es válido.");
				payrollErrors.add(payrollError);
				
				break;
			}
    	}
		
    	statistics.getPayrollFileStatistics().setNominas(singleEntriesTotal);
    	statistics.getPayrollFileStatistics().setFechaFinAnalisis(new Date());
    	statistics.getPayrollFileStatistics().setHoraFinAnalisis(new Date());
    	
    	payrollParser.setPayrollDataFile(payrollParserValidationService.getPayrollDataFile());
    	payrollParser.setErrors(payrollErrors);
    	payrollParser.setStatistics(statistics);
    	
    	UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > PARSER FILE FINISHED...");
		
		val payrollParserResponse = new PayrollParserResponse();
		payrollParserResponse.setPayrollParser(payrollParser);
		
    	return payrollParserResponse;
	}
	
	private String payrollFileHeader(String value) {
		if (!UValidator.isNullOrEmpty(value)) {
			NominaTramaEntity payrollEntity = iPayrollEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(value);
			if (!UValidator.isNullOrEmpty(payrollEntity)) {
				return payrollEntity.getNombre();
			}
		}
		return null;
	}
	
	private List<PayrollSubHeader> payrollFileSubHeader(String[] entry) {
		val payrollSubHeaderList = new ArrayList<PayrollSubHeader>();
		for (int i = 0; i < entry.length; i++) {
			String fieldName = entry[i];
			
			val payrollSubHeader = new PayrollSubHeader();
			payrollSubHeader.setFieldName(fieldName);
			payrollSubHeader.setFieldPosition(i);
			
			payrollSubHeaderList.add(payrollSubHeader);
		}
		return payrollSubHeaderList;
	}
	
	private void payrollFileSubHeader() {
		//##### Trama H01
		String[] h01 = new String[]{"correoelectronico", "leyenda"};
		payrollSubHeaderMap.put("H01", payrollFileSubHeader(h01));
		
		//##### Trama TA
		String[] ta = new String[]{"serie",	"folio", "subtotal", "descuento", "total", "lugarexpedicion", "exportacion"};
		payrollSubHeaderMap.put("TA", payrollFileSubHeader(ta));
		
		//##### Trama TRS
		String[] trs = new String[]{"uuid"};
		payrollSubHeaderMap.put("TRS", payrollFileSubHeader(trs));
		
		//##### Trama TRE
		String[] tre = new String[]{"rfc", "nombre", "domiciliofiscal", "regimenfiscal", "usocfdi", "objetoimp"};
		payrollSubHeaderMap.put("TRE", payrollFileSubHeader(tre));
		
		//##### Trama TC
		String[] tc = new String[]{"valorunitario", "importe", "descuento"};
		payrollSubHeaderMap.put("TC", payrollFileSubHeader(tc));
		
		//##### Trama TNOM
		String[] tnom = new String[]{"tiponomina", "fechapago", "fechainicialpago", "fechafinalpago", "numdiaspagados", "totalpercepciones", "totaldeducciones", "totalotrospagos"};
		payrollSubHeaderMap.put("TNOM", payrollFileSubHeader(tnom));
		
		//##### Trama TNOME
		String[] tnome = new String[]{"curp", "registropatronal", "rfcpatronorigen", "origenrecurso", "montorecursopropio"};
		payrollSubHeaderMap.put("TNOME", payrollFileSubHeader(tnome));
		
		//##### Trama TNOMR
		String[] tnomr = new String[]{"curp", "numseguridadsocial", "fechainiciorellaboral", "antiguedad", "tipocontrato", "sindicalizado", 
				"tipojornada", "tiporegimen", "numempleado", "departamento", "puesto", "riesgopuesto", "periodicidadpago", 
				"banco", "cuentabancaria", "salariobasecotapor", "salariodiariointegrado", "claveentfed"};
		payrollSubHeaderMap.put("TNOMR", payrollFileSubHeader(tnomr));
		
		//##### Trama TNOMRS
		String[] tnomrs = new String[]{"rfclabora", "porcentajetiempo"};
		payrollSubHeaderMap.put("TNOMRS", payrollFileSubHeader(tnomrs));
		
		//##### Trama TPERS
		String[] tpers = new String[]{"totalsueldos", "totalseparacionindemnizacion", "totaljubilacionpensionretiro", "totalgravado", "totalexento"};
		payrollSubHeaderMap.put("TPERS", payrollFileSubHeader(tpers));
		
		//##### Trama TPERC
		String[] tperc = new String[]{"tipopercepcion", "clave", "concepto", "importegravado", "importeexento", "valormercado", "precioalotorgarse", "identificadorpercepcion"};
		payrollSubHeaderMap.put("TPERC", payrollFileSubHeader(tperc));
		
		//##### Trama THE
		String[] the = new String[]{"dias", "tipohoras", "horasextra", "importepagado", "identificadorpercepcion"};
		payrollSubHeaderMap.put("THE", payrollFileSubHeader(the));
		
		//##### Trama TJPR
		String[] tjpr = new String[]{"totalunaexhibicion", "totalparcialidad", "montodiario", "ingresoacumulable", "ingresonoacumulable"};
		payrollSubHeaderMap.put("TJPR", payrollFileSubHeader(tjpr));
		
		//##### Trama TSI
		String[] tsi = new String[]{"totalpagado", "numannosservicio", "ultimosueldomensord", "ingresoacumulable", "ingresonoacumulable"};
		payrollSubHeaderMap.put("TSI", payrollFileSubHeader(tsi));
		
		//##### Trama TDEDS
		String[] tdeds = new String[]{"totalotrasdeducciones", "totalImpuestosRetenidos"};
		payrollSubHeaderMap.put("TDEDS", payrollFileSubHeader(tdeds));
		
		//##### Trama TDEDU
		String[] tdedu = new String[]{"tipodeduccion", "clave", "concepto", "importe"};
		payrollSubHeaderMap.put("TDEDU", payrollFileSubHeader(tdedu));
		
		//##### Trama TOP
		String[] top = new String[]{"tipootropago", "clave", "concepto", "importe", "subsidiocausado", "saldoafavor", "ejercicio", "remanentesalfav"};
		payrollSubHeaderMap.put("TOP", payrollFileSubHeader(top));
		
		//##### Trama TI
		String[] ti = new String[]{"diasincapacidad", "tipoincapacidad", "importemonetario"};
		payrollSubHeaderMap.put("TI", payrollFileSubHeader(ti));
		
		//##### Trama CEND
		String[] cend = new String[]{"finrecibonomina"};
		payrollSubHeaderMap.put("CEND", payrollFileSubHeader(cend));
	}
	
}