package org.megapractical.invoicing.webapp.exposition.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.megapractical.invoicing.api.converter.ApiFileErrorConverter;
import org.megapractical.invoicing.api.wrapper.ApiPayrollError;
import org.megapractical.invoicing.dal.bean.jpa.NominaCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaFicheroEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaTramaEntity;
import org.megapractical.invoicing.dal.data.repository.IPayrollDataFileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollEntryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollFieldJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.validator.EntryFileValidator;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.val;

@Service
@Scope("session")
public class PayrollParserValidationService {
	
	private final EntryFileValidator entryFileValidator;
	private final IPayrollDataFileJpaRepository iPayrollDataFileJpaRepository;
	private final IPayrollEntryJpaRepository iPayrollEntryJpaRepository;
	private final IPayrollFieldJpaRepository iPayrollFieldJpaRepository;
	
	public PayrollParserValidationService(EntryFileValidator entryFileValidator,
										  IPayrollDataFileJpaRepository iPayrollDataFileJpaRepository,
										  IPayrollEntryJpaRepository iPayrollEntryJpaRepository,
										  IPayrollFieldJpaRepository iPayrollFieldJpaRepository,
										  List<NominaTramaEntity> payrollEntriesList) {
		this.entryFileValidator = entryFileValidator;
		this.iPayrollDataFileJpaRepository = iPayrollDataFileJpaRepository;
		this.iPayrollEntryJpaRepository = iPayrollEntryJpaRepository;
		this.iPayrollFieldJpaRepository = iPayrollFieldJpaRepository;
		this.payrollEntriesList = payrollEntriesList;
	}

	private static final String PAYROLL_FILE_CODE = "NOMV12";
	
	@Getter
	private NominaFicheroEntity payrollDataFile;
	private List<NominaTramaEntity> payrollEntriesList = new ArrayList<>();
	
	//##### Inicializando contenido
	@PostConstruct
	private void init() {
		payrollDataFile = iPayrollDataFileJpaRepository.findByCodigoIgnoreCaseAndEliminadoFalse(PAYROLL_FILE_CODE);
		payrollEntriesList = iPayrollEntryJpaRepository.findByNominaFicheroAndEliminadoFalse(payrollDataFile);
	}
	
	//##### Longitud de la trama
	public Integer entriesLength(String[] entry) {
		Integer length = null;
		try {
			
			for (NominaTramaEntity item : payrollEntriesList) {
				if (item.getNombre().equalsIgnoreCase(entry[0])) {
					return item.getLongitud();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return length;
	}
	
	//##### Validacion de trama
	public boolean entriesValidate(String[] entry) {
		try {
			
			return entry.length == entriesLength(entry);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	//##### Valida si una trama esta vacia
	//##### NOTA: Los valores que indican si la trama trae 
	//##### contenido comienzan en la posicion 1 de la misma
	public boolean isEntryEmpty(String[] entry) {
		boolean isEmpty = false;
		try {
			
			val entryLength = entriesLength(entry) - 1;
			for (int i = 1; i <= entryLength; i ++) {
				isEmpty = entry[i].isEmpty();
				if (!isEmpty) break;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isEmpty;
	}
	
	//##### Validacion de campo
	public List<ApiPayrollError> entryValidate(String[] entry) {
		val payrollErrors = new ArrayList<ApiPayrollError>();
		try {
			
			Integer entryLength = entriesLength(entry) - 1;
			NominaTramaEntity payrollEntry = iPayrollEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(entry[0]);
			
			val fields = iPayrollFieldJpaRepository.findByNominaTramaAndEliminadoFalse(payrollEntry);
			if (!fields.isEmpty()) {
				for (int i = 1; i <= entryLength; i ++) {
					NominaCampoEntity payrollField = iPayrollFieldJpaRepository.findByNominaTramaAndPosicionAndEliminadoFalse(payrollEntry, i);
					val validation = entryFileValidator.validateContent(payrollEntry, payrollField, entry[i]);
					if (!validation) {
						val payrollError = ApiFileErrorConverter.convert(payrollField, entry, i);
						payrollErrors.add(payrollError);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payrollErrors;
	}
	
}