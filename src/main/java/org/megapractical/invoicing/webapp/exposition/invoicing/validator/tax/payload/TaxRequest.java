package org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload;

import org.megapractical.invoicing.webapp.json.JRateOrFee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaxRequest {
	private String requestData;
	private String selectedTax;
	private String action;
	private String currency;
	private JRateOrFee rateOrFeeJson;
	private String baseField;
	private String taxTypeField;
	private String factorTypeField;
	private String rateOrFeeField;
	private String amountField;
}