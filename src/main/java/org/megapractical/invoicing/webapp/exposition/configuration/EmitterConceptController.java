package org.megapractical.invoicing.webapp.exposition.configuration;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCalculation;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ClaveProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteConceptoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UnidadMedidaEntity;
import org.megapractical.invoicing.dal.data.repository.IKeyProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IMeasurementUnitJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerConceptJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.json.JConcept;
import org.megapractical.invoicing.webapp.json.JConceptParser;
import org.megapractical.invoicing.webapp.json.JConceptParser.ConceptParserError;
import org.megapractical.invoicing.webapp.json.JEmitterConcept;
import org.megapractical.invoicing.webapp.json.JEmitterConcept.Concept;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.notification.Toastr.ToastrType;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.ui.Paginator;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WConcept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import lombok.val;

@Controller
@Scope("session")
public class EmitterConceptController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	DataStoreService dataStore;
	
	@Resource
	ITaxpayerConceptJpaRepository iTaxpayerConceptJpaRepository;
	
	@Resource
	private IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository;
	
	@Resource
	private IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository;
	
	private static final String SORT_PROPERTY = "descripcion";
	private static final Integer INITIAL_PAGE = 0;
	
	private MonedaEntity currency;
	private String configAction;
	private String unexpectedError;
	
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	@ModelAttribute("processingRequest")
	public String processingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@ModelAttribute("updatingRequest")
	public String updatingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.updating", userTools.getLocale());
	}
	
	// Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() throws IOException{
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	@RequestMapping(value = "/emitterConcept", method = RequestMethod.GET)
	public String emitterConcept(Model model, Integer page, String searchString) {
		try {
			
			this.currency = userTools.getPreferences().getMoneda();
			this.unexpectedError = UProperties.genericUnexpectedError(userTools.getLocale());
			
			String currency = UProperties.getMessage("mega.cfdi.information.user.default.currency", userTools.getLocale());
			String[] messageSplitted = currency.split(Pattern.quote("{0}"));
			currency = messageSplitted[0].concat(this.currency.getValor());
			
			model.addAttribute("currencyDefault", this.currency);
			model.addAttribute("page", INITIAL_PAGE);
			
			HtmlHelper.functionalitySelected("EmitterConfig", "emitter-config-concept");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			return "/config/EmitterConcept";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/emitterConcept", method = RequestMethod.POST)
	public @ResponseBody String emitterConcept(HttpServletRequest request) {
		
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JEmitterConcept concept = new JEmitterConcept();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchDescription = UBase64.base64Decode(request.getParameter("searchDescription"));
			if(UValidator.isNullOrEmpty(searchDescription)){
				searchDescription = "";
			}
			
			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Page<ContribuyenteConceptoEntity> conceptPage = null;
			Paginator<ContribuyenteConceptoEntity> paginator = null;
			
			conceptPage = iTaxpayerConceptJpaRepository.findByContribuyenteAndDescripcionContainingIgnoreCaseAndEliminadoFalse(taxpayer, searchDescription, pageRequest);
			val pagination = JPagination.buildPaginaton(conceptPage);
			concept.setPagination(pagination);
			
			//##### Para cuando se carga directamente los conceptos
			//##### desde el catalogo de conceptos en generar CFDI
			if(UValidator.isNullOrEmpty(this.currency)){
				this.currency = userTools.getPreferences().getMoneda();
			}
			
			for (ContribuyenteConceptoEntity item : conceptPage) {
				String measurementUnit = item.getUnidadMedida().getCodigo().concat(" (").concat(item.getUnidadMedida().getValor()).concat(")");
				String keyProductService = item.getClaveProductoServicio().getCodigo().concat(" (").concat(item.getClaveProductoServicio().getValor()).concat(")");
				
				Concept conceptObj = new Concept();
				conceptObj.setId(UBase64.base64Encode(item.getId().toString()));
				conceptObj.setMeasurementUnit(UBase64.base64Encode(measurementUnit));
				conceptObj.setKeyProductService(UBase64.base64Encode(keyProductService));
				conceptObj.setIdentificationNumber(UBase64.base64Encode(item.getNoIdentificacion()));
				conceptObj.setDescription(UBase64.base64Encode(item.getDescripcion().toUpperCase()));
				conceptObj.setUnit(UBase64.base64Encode(item.getUnidad()));
				conceptObj.setUnitValue(UBase64.base64Encode(UValue.bigDecimalString(UValue.bigDecimal(item.getValorUnitario().toString()))));
				
				if(!UValidator.isNullOrEmpty(item.getDescuento())){
					if(!UValidator.isNullOrEmpty(this.currency)){
						conceptObj.setDiscount(UBase64.base64Encode(UValue.bigDecimalString(UValue.bigDecimal(UValue.doubleString(item.getDescuento()), this.currency.getDecimales()))));
					}
				}
				
				concept.getConcepts().add(conceptObj);
			}
			
			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if(!UValidator.isNullOrEmpty(searchDescription)){
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
			}
			
			if(concept.getConcepts().isEmpty()){
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}
			
			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		sessionController.sessionUpdate();
		
		concept.setResponse(response);
		jsonInString = gson.toJson(concept);
		return jsonInString;
	}
	
	/*@RequestMapping(value = "/emitterConcept/action/{action}", method = RequestMethod.GET)
	public String emitterRfcAccount(Model model, @PathVariable(value="action") String action){
		try {
			
			this.configAction = action;
			return "/config/CfdiMasterEmitterConceptConfig";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/
	
	@RequestMapping(value="/emitterConcept", params={"conceptDataLoad"})
	public @ResponseBody String emitterConceptDataLoad(HttpServletRequest request) throws IOException{
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JConcept concept = new JConcept();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			ContribuyenteConceptoEntity taxpayerConcept = iTaxpayerConceptJpaRepository.findByIdAndEliminadoFalse(id);
			if(!UValidator.isNullOrEmpty(taxpayerConcept)){
				
				UnidadMedidaEntity measurementUnit = iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(taxpayerConcept.getUnidadMedida().getCodigo());
				String measurementUnitString = measurementUnit.getCodigo().concat(" (").concat(measurementUnit.getValor()).concat(")");
				
				ClaveProductoServicioEntity keyProductService = iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(taxpayerConcept.getClaveProductoServicio().getCodigo());
				String keyProductServiceString = keyProductService.getCodigo().concat(" (").concat(keyProductService.getValor()).concat(")");
				
				concept.setDescription(UBase64.base64Encode(taxpayerConcept.getDescripcion()));
				concept.setMeasurementUnit(UBase64.base64Encode(measurementUnitString));
				concept.setKeyProductService(UBase64.base64Encode(keyProductServiceString));
				concept.setIdentificationNumber(!UValidator.isNullOrEmpty(taxpayerConcept.getNoIdentificacion()) ? UBase64.base64Encode(taxpayerConcept.getNoIdentificacion()) : "");
				concept.setUnit(!UValidator.isNullOrEmpty(taxpayerConcept.getUnidad()) ? UBase64.base64Encode(taxpayerConcept.getUnidad()) : "");
				concept.setQuantity(UBase64.base64Encode(UValue.bigDecimalString(UValue.bigDecimal(UValue.doubleString(taxpayerConcept.getCantidad()), this.currency.getDecimales()))));
				concept.setUnitValue(UBase64.base64Encode(UValue.bigDecimalString(UValue.bigDecimal(UValue.doubleString(taxpayerConcept.getValorUnitario()), this.currency.getDecimales()))));
				concept.setDiscount(!UValidator.isNullOrEmpty(taxpayerConcept.getDescuento()) ? UBase64.base64Encode(UValue.bigDecimalString(UValue.bigDecimal(UValue.doubleString(taxpayerConcept.getDescuento()), this.currency.getDecimales()))) : "");
				concept.setAmount(UBase64.base64Encode(UValue.bigDecimalString(UValue.bigDecimal(UValue.doubleString(taxpayerConcept.getImporte()), this.currency.getDecimales()))));
				
				response.setSuccess(Boolean.TRUE);
			}else{
				notification.setMessage(UProperties.getMessage("ERR0049", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0049", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		concept.setResponse(response);
		jsonInString = gson.toJson(concept);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterConcept", params={"conceptRemove"})
    public @ResponseBody String emitterConceptRemove(HttpServletRequest request) throws IOException {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JConcept concept = new JConcept();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
        	
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			ContribuyenteConceptoEntity taxpayerConcept = iTaxpayerConceptJpaRepository.findByIdAndEliminadoFalse(id);
			if(!UValidator.isNullOrEmpty(taxpayerConcept)){
				taxpayerConcept.setEliminado(Boolean.TRUE);
				persistenceDAO.update(taxpayerConcept);
				
				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.ELIMINAR_CONCEPTO_ID41);
				
				// Timeline :: Registro de accion
				timelineLog.timelineLog(EventTypeCode.ELIMINAR_CONCEPTO_ID19);
				
				response.setSuccess(Boolean.TRUE);
			}else{
				notification.setMessage(UProperties.getMessage("ERR0075", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
        	
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0075", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}		
		
		sessionController.sessionUpdate();
		
		concept.setResponse(response);
		jsonInString = gson.toJson(concept);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterConcept", params={"conceptAddEdit"})
    public @ResponseBody String emitterConceptAddEdit(HttpServletRequest request) throws IOException {
		JConcept concept = new JConcept();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			this.configAction = UBase64.base64Decode(request.getParameter("operation"));
			this.unexpectedError = this.configAction.equals("config") ? UProperties.getMessage("ERR0051", userTools.getLocale()) : UProperties.getMessage("ERR0052", userTools.getLocale());
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Boolean conceptDeletedFinded = false;
			Boolean errorMarked = false;
			
			ContribuyenteConceptoEntity taxpayerConcept = new ContribuyenteConceptoEntity();
			
			if(request.getParameter("objConcept") != null){
				String objConcept = request.getParameter("objConcept");
				concept = gson.fromJson(objConcept, JConcept.class);
				
				// Id
				Long conceptId = null;
				String index = UBase64.base64Decode(concept.getIndex());
				if(!UValidator.isNullOrEmpty(index)){
					conceptId = UValue.longValue(index);
				}
				
				// Descripcion
				String description = UBase64.base64Decode(concept.getDescription());
				error = new JError();
				
				if(UValidator.isNullOrEmpty(description)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					if(this.configAction.equals("config")){
						ContribuyenteConceptoEntity taxpayerConceptEntity = iTaxpayerConceptJpaRepository.findByContribuyenteAndDescripcionIgnoreCaseAndEliminadoFalse(taxpayer, description);
						if(!UValidator.isNullOrEmpty(taxpayerConceptEntity)){
							error.setMessage(UProperties.getMessage("ERR0010", userTools.getLocale()));
							errorMarked = true;
						}else{
							taxpayerConceptEntity = iTaxpayerConceptJpaRepository.findByContribuyenteAndDescripcionIgnoreCaseAndEliminadoTrue(taxpayer, description);
							if(!UValidator.isNullOrEmpty(taxpayerConceptEntity)){
								taxpayerConcept = taxpayerConceptEntity;
								conceptDeletedFinded = true;
							}else{
								taxpayerConcept = new ContribuyenteConceptoEntity();
							}							
						}
					}else if(this.configAction.equals("edit")){
						taxpayerConcept = iTaxpayerConceptJpaRepository.findByIdAndEliminadoFalse(conceptId);
						String taxpayerConceptDescription = taxpayerConcept.getDescripcion();
						if(!description.equalsIgnoreCase(taxpayerConceptDescription)){
							ContribuyenteConceptoEntity taxpayerConceptEntity = null;
							taxpayerConceptEntity = iTaxpayerConceptJpaRepository.findByContribuyenteAndDescripcionIgnoreCaseAndEliminadoFalse(taxpayer, description);
							if(!UValidator.isNullOrEmpty(taxpayerConceptEntity)){
								error.setMessage(UProperties.getMessage("ERR0010", userTools.getLocale()));
								errorMarked = true;
							}
						}
					}
				}
				if(errorMarked){
					error.setField("description");
					errors.add(error);
				}
				
				// Unidad de medida
				String measurementUnit = UBase64.base64Decode(concept.getMeasurementUnit());
				UnidadMedidaEntity measurementUnitEntityByCode = null;
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(measurementUnit)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					String[] measurementUnitCatalog = UCatalog.getCatalog(measurementUnit);
					String measurementUnitCode = measurementUnitCatalog[0];
					String measurementUnitValue = measurementUnitCatalog[1];
					measurementUnitEntityByCode = iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(measurementUnitCode);
					UnidadMedidaEntity measurementUnitEntityByValue = iMeasurementUnitJpaRepository.findByValorAndEliminadoFalse(measurementUnitValue);
					if(measurementUnitEntityByCode == null || measurementUnitEntityByValue == null){
						String[] values = new String[]{"CFDI33145", "UnidadMedida", "c_ClaveUnidad"};
						error.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
						errorMarked = true;
					}
				}
				if(errorMarked){
					error.setField("measurement-unit");
					errors.add(error);
				}
				
				// Clave producto servicio
				String keyProductService = UBase64.base64Decode(concept.getKeyProductService());
				ClaveProductoServicioEntity keyProductServiceEntityByCode = null;
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(keyProductService)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					String[] keyProductServiceCatalog = UCatalog.getCatalog(keyProductService);
					String keyProductServiceCode = keyProductServiceCatalog[0];
					String keyProductServiceValue = keyProductServiceCatalog[1];
					keyProductServiceEntityByCode = iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(keyProductServiceCode);
					ClaveProductoServicioEntity keyProductServiceEntityByValue = iKeyProductServiceJpaRepository.findByValorAndEliminadoFalse(keyProductServiceValue);
					if(keyProductServiceEntityByCode == null || keyProductServiceEntityByValue == null){
						String[] values = new String[]{"CFDI33142", "ClaveProdServ", "c_ClaveProdServ"};
						error.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
						errorMarked = true;
					}
				}
				if(errorMarked){
					error.setField("key-product-service");
					errors.add(error);
				}
				
				// Numero de identificacion
				String identificationNumber = UValue.stringValue(UBase64.base64Decode(concept.getIdentificationNumber()));
				
				// Unidad
				String unit = UValue.stringValue(UBase64.base64Decode(concept.getUnit()));
				
				// Valor unitario/Importe
				String unitValue = UBase64.base64Decode(concept.getUnitValue());
				BigDecimal unitValueBigDecimal = UValue.bigDecimalStrict(unitValue);
				Double amount = null;
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(unitValue)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					if(!UValidator.isDouble(unitValue)){
						error.setMessage(UProperties.getMessage("ERR1003", userTools.getLocale()));
						errorMarked = true;
					}else{
						String zeroValue = null;
						BigDecimal BigDecimalZero = UValue.bigDecimal(zeroValue, this.currency.getDecimales());
						BigDecimal unitValueValid = UValue.bigDecimalStrict(unitValue, UTools.decimalValue(unitValue.toString()));
						
						if(unitValueValid.compareTo(BigDecimalZero) != 1){
							error.setMessage(UProperties.getMessage("ERR0071", userTools.getLocale()));
							errorMarked = true;
						}else{
							
							if(!UValidator.isValidDecimalPart(unitValueValid, this.currency.getDecimales())){
								error.setMessage(UProperties.getMessage("ERR0017", userTools.getLocale()));
								errors.add(error);
								errorMarked = true;
							}
							amount = UValue.doubleValue(unitValue);
						}
					}
				} 
				if(errorMarked){
					error.setField("unit-value");
					errors.add(error);
				}
				
				// Descuento
				String discount = UValue.stringValue(UBase64.base64Decode(concept.getDiscount()));
				error = new JError();
				errorMarked = false;
				
				if(!UValidator.isNullOrEmpty(discount)){
					if(!UValidator.isDouble(discount)){
						error.setMessage(UProperties.getMessage("ERR1003", userTools.getLocale()));
						errorMarked = true;
					}else{
						BigDecimal discountValid = UValue.bigDecimalStrict(discount, UTools.decimalValue(discount.toString()));
						if(!UValidator.isValidDecimalPart(discountValid, this.currency.getDecimales())){
							error.setMessage(UProperties.getMessage("ERR0019", userTools.getLocale()));
							errors.add(error);
							errorMarked = true;
						}else{
							// Calculando importe
							BigDecimal quantityBigDecimal = UValue.bigDecimal("1");
							BigDecimal discountBigDecimal = UValue.bigDecimal(discount);
							BigDecimal amountValid = UValue.bigDecimal(UCalculation.amount(quantityBigDecimal, unitValueBigDecimal).toString(), this.currency.getDecimales()) ;
							amount = UValue.doubleValue(amountValid);
						}
					}
				}
				if(errorMarked){
					error.setField("discount");
					errors.add(error);
				}
				
				if(!errors.isEmpty()){
					response.setError(Boolean.TRUE);
					response.setErrors(errors);
				}else{
					
					taxpayerConcept.setContribuyente(taxpayer);
					taxpayerConcept.setDescripcion(description);
					taxpayerConcept.setUnidadMedida(measurementUnitEntityByCode);
					taxpayerConcept.setClaveProductoServicio(keyProductServiceEntityByCode);
					taxpayerConcept.setNoIdentificacion(identificationNumber);
					taxpayerConcept.setUnidad(unit);
					taxpayerConcept.setCantidad(UValue.doubleValue("1"));
					taxpayerConcept.setValorUnitario(UValue.doubleValue(unitValue));
					taxpayerConcept.setDescuento(UValue.doubleValueStrict(discount));
					taxpayerConcept.setImporte(amount);
					taxpayerConcept.setEliminado(Boolean.FALSE);
					
					WConcept wConcept = new WConcept();
					wConcept.setTaxpayerConcept(taxpayerConcept);
					wConcept.setAction(this.configAction);
					wConcept.setConceptDeletedFinded(conceptDeletedFinded);
					
					taxpayerConcept = null;
					taxpayerConcept = dataStore.taxpayerConceptStore(wConcept);
					
					if(!UValidator.isNullOrEmpty(taxpayerConcept)){
						OperationCode operationCode = null;
						EventTypeCode eventTypeCode = null;
						String toastrTitle = null;
						String toastrMessage = null;
						
						if(this.configAction.equals("edit")){
							operationCode = OperationCode.ACTUALIZACION_CONCEPTO_ID40;
							eventTypeCode = EventTypeCode.ACTUALIZACION_CONCEPTO_ID18;
							toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.concept.edit.title", userTools.getLocale());
							toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.concept.edit.message", userTools.getLocale());
						}else{						
							operationCode = OperationCode.REGISTRO_CONCEPTO_ID39;
							eventTypeCode = EventTypeCode.REGISTRO_CONCEPTO_ID17;
							toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.concept.config.title", userTools.getLocale());
							toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.concept.config.message", userTools.getLocale());
						}
						concept.setIndex(UBase64.base64Encode(taxpayerConcept.getId().toString()));
						
						// Bitacora :: Registro de accion en bitacora
						appLog.logOperation(operationCode);
						
						// Timeline :: Registro de accion
						timelineLog.timelineLog(eventTypeCode);
						
						// Notificacion Toastr
						Toastr toastr = new Toastr();
						toastr.setTitle(toastrTitle);
						toastr.setMessage(toastrMessage);
						toastr.setType(ToastrType.SUCCESS.value());
						
						notification.setToastrNotification(Boolean.TRUE);
						notification.setToastr(toastr);
						
						response.setSuccess(Boolean.TRUE);
						response.setNotification(notification);
					}else{
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(this.unexpectedError);
						response.setNotification(notification);
						response.setError(Boolean.TRUE);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(this.unexpectedError);
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		concept.setResponse(response);
		jsonInString = gson.toJson(concept);
		
		return jsonInString;
	}
	
	@RequestMapping(value = "/emitterConcept", params={"uploadFile"})
    public @ResponseBody String uploadFile(Model model, HttpServletRequest request, @RequestParam(value="fileUploaded") MultipartFile multipartConceptFile) throws IOException{
		JConceptParser parserJson = new JConceptParser();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		Boolean errorMarked = false;
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			//##### Archivo
			File conceptFileUpload = null;
			
			String originalFilename = null;
			String conceptFileName = null;
			String conceptFileExt = null;
			
			error = new JError();
			
			if (multipartConceptFile.isEmpty()) {
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errorMarked = true;
			}else{
				originalFilename = multipartConceptFile.getOriginalFilename();
				conceptFileExt = UFile.getExtension(originalFilename);
				
				if(UValidator.isNullOrEmpty(conceptFileExt) || (!conceptFileExt.equalsIgnoreCase("csv"))){
					error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
					errorMarked = true;
				}else{
					conceptFileName = UFile.getName(originalFilename).toUpperCase();
					conceptFileUpload = UFile.multipartFileToFile(multipartConceptFile);
				}
			}
			if(errorMarked){
				error.setField("concept-file");
				errors.add(error);
			}else{
				//##### Ruta temporal de archivos
				String tmpPath = appSettings.getPropertyValue("cfdi.tmp.dir");
				
				File absolutePath = new File(tmpPath); 
				if(!absolutePath.exists()){
					absolutePath.mkdirs();
				}
				
				//##### Directorio para el archivo actual
				String folderPath = tmpPath + File.separator + conceptFileName + "." + conceptFileExt;
				
				UFile.writeFile(UFile.fileToByte(conceptFileUpload), folderPath);
				
				String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
				ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
				
				parserJson = conceptFileParser(folderPath, taxpayer);
				
				File fileToDelete = new File(folderPath);
				fileToDelete.delete();
				
				List<JConcept> concepts = parserJson.getConcepts();				
				if(!concepts.isEmpty()){
					for (JConcept item : concepts) {
						ContribuyenteConceptoEntity conceptEntity = new ContribuyenteConceptoEntity();
						conceptEntity.setContribuyente(taxpayer);
						conceptEntity.setDescripcion(item.getDescription());
						
						ClaveProductoServicioEntity keyProductService = iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(item.getKeyProductService());
						conceptEntity.setClaveProductoServicio(keyProductService);
						
						UnidadMedidaEntity measurementUnit = iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(item.getMeasurementUnit());
						conceptEntity.setUnidadMedida(measurementUnit);
						
						if(!UValidator.isNullOrEmpty(item.getIdentificationNumber())){
							conceptEntity.setNoIdentificacion(item.getIdentificationNumber());
						}
						
						if(!UValidator.isNullOrEmpty(item.getUnit())){
							conceptEntity.setUnidad(item.getUnit());
						}
						
						conceptEntity.setCantidad(UValue.doubleValue("1"));
						conceptEntity.setValorUnitario(UValue.doubleValue(item.getUnitValue()));
						conceptEntity.setImporte(UValue.doubleValue(item.getUnitValue()));
						conceptEntity.setEliminado(Boolean.FALSE);
						
						persistenceDAO.persist(conceptEntity);
					}
					
					notification.setCssStyleClass(CssStyleClass.SUCCESS.value());
					notification.setMessage("Se registraron " + parserJson.getTotalConceptRegistered() + " conceptos de " + parserJson.getTotalConceptInFile());
					
					response.setError(Boolean.TRUE);
				}else{
					notification.setCssStyleClass(CssStyleClass.WARNING.value());
					notification.setMessage("No se registró ningún concepto.");
				}
				
				if(!UValidator.isNullOrEmpty(parserJson.getErrorParserMessage())){
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					notification.setMessage(parserJson.getErrorParserMessage());
				}
				
				notification.setPageMessage(Boolean.TRUE);
				response.setNotification(notification);
				response.setSuccess(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		parserJson.setResponse(response);
		jsonInString = gson.toJson(parserJson);
		
		return jsonInString;
	}
	
	private JConceptParser conceptFileParser(String conceptFilePath, ContribuyenteEntity taxpayer){
		try {
			
			//##### Cargando el fichero
    		File file = new File(conceptFilePath);
    		
    		//##### Leyendo fichero
			//CSVReader reader = new CSVReader(new FileReader(file), '|');
    		CSVParser parser = new CSVParserBuilder()
								.withSeparator('|')
								.withIgnoreQuotations(true)
							    .build();
			CSVReader reader = new CSVReaderBuilder(new FileReader(file))
								.withCSVParser(parser)
								.build();
        	
        	//##### Contenido del fichero
        	List<String[]> entries = reader.readAll();
        	
        	//##### Cerrando el fichero
        	reader.close();
        	
        	//##### Indica la línea del fichero
        	Integer line = 0;
        	
        	//##### Indica la cantidad de conceptos a registrar
        	Integer totalConceptRegistered = 0;
        	
        	boolean errorSet = false;
        	
        	JConceptParser conceptParser = new JConceptParser();
        	List<JConcept> concepts = new ArrayList<>();
        	List<ConceptParserError> conceptParserErrors = new ArrayList<>();
        	
			Integer totalConceptInFile = entries.size() - 1;
			
        	for (int i = 0; i < entries.size(); i ++){
        		
        		//##### Informacion de la línea actual
        		String[] item = entries.get(i);
        		
        		//##### Incrementenado línea
				line ++;
				
				errorSet = false;
				
				if(i == 0){
					//Descripcion	ClaveUnidad	ClaveProductoServicio	NumeroIdentificacion	Unidad	ValorUnitario
					if(!item[0].trim().equals("Descripcion"))
						errorSet = true;
					if(!item[1].trim().equals("ClaveUnidad"))
						errorSet = true;
					if(!item[2].trim().equals("ClaveProductoServicio"))
						errorSet = true;
					if(!item[3].trim().equals("NumeroIdentificacion"))
						errorSet = true;
					if(!item[4].trim().equals("Unidad"))
						errorSet = true;
					if(!item[5].trim().equals("ValorUnitario"))
						errorSet = true;
				}
				
				if(!errorSet){
					if(i > 0){
						String description = item[0].trim();
						String claveUnidad = item[1].trim();
						String claveProductoServicio = item[2].trim();
						
						String numeroIdentificacion = null;
						if(!UValidator.isNullOrEmpty(item[3].trim())){
							numeroIdentificacion = item[3].trim();
						}
						
						String unidad = null;
						if(!UValidator.isNullOrEmpty(item[4].trim())){
							unidad = item[4].trim();
						}
						
						String valorUnitario = item[5].trim();
						
						if(!UValidator.isNullOrEmpty(description)){
							ContribuyenteConceptoEntity concept = iTaxpayerConceptJpaRepository.findByContribuyenteAndDescripcionIgnoreCaseAndEliminadoFalse(taxpayer, description);
							if(UValidator.isNullOrEmpty(concept)){
								JConcept conceptJson = new JConcept();
								conceptJson.setDescription(description);
								conceptJson.setMeasurementUnit(claveUnidad);
								conceptJson.setKeyProductService(claveProductoServicio);
								conceptJson.setIdentificationNumber(numeroIdentificacion);
								conceptJson.setUnit(unidad);
								conceptJson.setUnitValue(valorUnitario);
								
								concepts.add(conceptJson);
								
								totalConceptRegistered ++;
							}else{
								ConceptParserError error = new ConceptParserError();
								error.setConceptDescription(description);
								error.setErrorMessage(UProperties.getMessage("ERR0010", userTools.getLocale()));
								
								conceptParserErrors.add(error);
							}
						}
					}
				}else{
					conceptParser.setErrorParserMessage(UProperties.getMessage("ERR0182", userTools.getLocale()));
				}
        	}
			        	        	
        	conceptParser.setConcepts(concepts);
        	conceptParser.setTotalConceptInFile(Integer.toString(totalConceptInFile));
        	conceptParser.setTotalConceptRegistered(Integer.toString(totalConceptRegistered));
        	
        	return conceptParser;
        	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}