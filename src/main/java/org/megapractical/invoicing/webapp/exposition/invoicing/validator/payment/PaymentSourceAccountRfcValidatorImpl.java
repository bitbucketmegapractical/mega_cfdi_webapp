package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.RfcUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentSourceAccountRfcValidatorImpl implements PaymentSourceAccountRfcValidator {

	@Autowired
	private UserTools userTools;

	/**
	 * Expresion regular para validar RFC de la cuenta ordenante en el complemto de
	 * pago
	 */
	private static final String COMPLEMENT_PAYMENT_SOURCE_ACCOUNT_RFC = "XEXX010101000|[A-Z&amp;Ñ]{3}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]?";

	@Override
	public String getSourceAccountRfc(String sourceAccountRfc, FormaPagoEntity paymentWay) {
		if (sourceAccountRfc != null && paymentWay != null) {
			val rfcSourceAccountValid = paymentWay.getCuentaOrdenanteRfc();
			if (rfcSourceAccountValid.equalsIgnoreCase("no") || sourceAccountRfc.length() < RfcUtils.RFC_MIN_LENGTH
					|| sourceAccountRfc.length() > RfcUtils.RFC_MAX_LENGTH
					|| !sourceAccountRfc.matches(COMPLEMENT_PAYMENT_SOURCE_ACCOUNT_RFC)) {
				return null;
			}
		}
		return sourceAccountRfc;
	}

	@Override
	public String getSourceAccountRfc(String sourceAccountRfc, String field, FormaPagoEntity paymentWay,
			List<JError> errors) {
		JError error = null;
		if (sourceAccountRfc != null && paymentWay != null) {
			val rfcSourceAccountValid = paymentWay.getCuentaOrdenanteRfc();
			if (rfcSourceAccountValid.equalsIgnoreCase("no")) {
				error = JError.error(field, "ERR1006", userTools.getLocale());
			} else if (sourceAccountRfc.length() < RfcUtils.RFC_MIN_LENGTH
					|| sourceAccountRfc.length() > RfcUtils.RFC_MAX_LENGTH
					|| !sourceAccountRfc.matches(COMPLEMENT_PAYMENT_SOURCE_ACCOUNT_RFC)) {
				error = JError.error(field, "ERR1013", userTools.getLocale());
			}
		}

		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return sourceAccountRfc;
	}

}