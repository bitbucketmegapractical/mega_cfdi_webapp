package org.megapractical.invoicing.webapp.exposition.invoicing.file.converter;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;
import org.megapractical.invoicing.webapp.json.JCfdiFile.CfdiFile.CfdiFileStatistics;

import lombok.val;

public final class InvoiceFileStatsConverter {

	private InvoiceFileStatsConverter() {
	}

	public static CfdiFileStatistics convert(ArchivoFacturaEstadisticaEntity entity) {
		if (entity == null) {
			return null;
		}

		val stats = new CfdiFileStatistics();
		stats.setInvoiceFileId(UBase64.base64Encode(entity.getId().toString()));

		if (!UValidator.isNullOrEmpty(entity.getFacturas())) {
			stats.setTotalInvoice(UBase64.base64Encode(UValue.integerString(entity.getFacturas())));
		}

		if (!UValidator.isNullOrEmpty(entity.getFacturasTimbradas())) {
			stats.setTotalInvoiceStamped(UBase64.base64Encode(UValue.integerString(entity.getFacturasTimbradas())));
		}

		if (!UValidator.isNullOrEmpty(entity.getFacturasError())) {
			stats.setTotalInvoiceError(UBase64.base64Encode(UValue.integerString(entity.getFacturasError())));
		}

		if (!UValidator.isNullOrEmpty(entity.getFechaInicioAnalisis())) {
			stats.setParserStartDate(UBase64.base64Encode(UDate.formattedSingleDate(entity.getFechaInicioAnalisis())));
		}

		if (!UValidator.isNullOrEmpty(entity.getHoraInicioAnalisis())) {
			stats.setParserStartTime(UBase64.base64Encode(UDate.formattedTime(entity.getHoraInicioAnalisis())));
		}

		if (!UValidator.isNullOrEmpty(entity.getFechaFinAnalisis())) {
			stats.setParserEndDate(UBase64.base64Encode(UDate.formattedSingleDate(entity.getFechaFinAnalisis())));
		}

		if (!UValidator.isNullOrEmpty(entity.getHoraFinAnalisis())) {
			stats.setParserEndTime(UBase64.base64Encode(UDate.formattedTime(entity.getHoraFinAnalisis())));
		}

		if (!UValidator.isNullOrEmpty(entity.getFechaInicioGeneracionComprobante())) {
			stats.setVoucherGeneratingStartDate(
					UBase64.base64Encode(UDate.formattedSingleDate(entity.getFechaInicioGeneracionComprobante())));
		}

		if (!UValidator.isNullOrEmpty(entity.getHoraInicioGeneracionComprobante())) {
			stats.setVoucherGeneratingStartTime(
					UBase64.base64Encode(UDate.formattedTime(entity.getHoraInicioGeneracionComprobante())));
		}

		if (!UValidator.isNullOrEmpty(entity.getFechaFinGeneracionComprobante())) {
			stats.setVoucherGeneratingEndDate(
					UBase64.base64Encode(UDate.formattedSingleDate(entity.getFechaFinGeneracionComprobante())));
		}

		if (!UValidator.isNullOrEmpty(entity.getHoraFinGeneracionComprobante())) {
			stats.setVoucherGeneratingEndTime(
					UBase64.base64Encode(UDate.formattedTime(entity.getHoraFinGeneracionComprobante())));
		}

		if (!UValidator.isNullOrEmpty(entity.getFechaInicioTimbrado())) {
			stats.setVoucherStampingStartDate(
					UBase64.base64Encode(UDate.formattedSingleDate(entity.getFechaInicioTimbrado())));
		}

		if (!UValidator.isNullOrEmpty(entity.getHoraInicioTimbrado())) {
			stats.setVoucherStampingStartTime(
					UBase64.base64Encode(UDate.formattedTime(entity.getHoraInicioTimbrado())));
		}

		if (!UValidator.isNullOrEmpty(entity.getFechaFinTimbrado())) {
			stats.setVoucherStampingEndDate(
					UBase64.base64Encode(UDate.formattedSingleDate(entity.getFechaFinTimbrado())));
		}

		if (!UValidator.isNullOrEmpty(entity.getHoraFinTimbrado())) {
			stats.setVoucherStampingEndTime(UBase64.base64Encode(UDate.formattedTime(entity.getHoraFinTimbrado())));
		}

		return stats;
	}

}