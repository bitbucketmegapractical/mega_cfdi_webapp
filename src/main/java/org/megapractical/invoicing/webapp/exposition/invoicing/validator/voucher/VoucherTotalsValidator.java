package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherTotalsRequest;
import org.megapractical.invoicing.webapp.json.JError;

public interface VoucherTotalsValidator {
	
	boolean validateTotals(String subTotal, String total, List<JError> errors);
	
	boolean validateTotals(VoucherTotalsRequest voucherTotalsRequest, List<JError> errors);
	
}