package org.megapractical.invoicing.webapp.exposition.sftp.file;

import org.megapractical.invoicing.webapp.exposition.cfdi.payload.FileDownload;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.SftpUtils;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class SftpFileDownloadService {
	
	private final SftpFileService sftpFileService;
	private final SftpUtils sftpUtils;
	
	private static final String CONTENTTYPE_ZIP = "application/zip";
	
	public FileDownload download(Long id, String type, String sftp) {
		try {
			val sftpFile = sftpFileService.findById(id);
			if (sftpFile != null) {
				String sftpPath = null;
				String fileName = null;
				if ("content".equals(type)) {
					sftpPath = sftpFile.getRutaDescarga();
					fileName = sftpFile.getContentFileName();
				} else if ("errorValidation".equals(type)) {
					sftpPath = sftpFile.getRutaErrorValidacion();
					fileName = sftpFile.getErrorValidationFileName();
				} else if ("errorContent".equals(type)) {
					sftpPath = sftpFile.getRutaErrorContenido();
					fileName = sftpFile.getErrorContentFileName();
				} else if ("errorS3".equals(type)) {
					sftpPath = sftpFile.getRutaErrorS3();
					fileName = sftpFile.getErrorS3FileName();
				}
				
				val fileByte = sftpUtils.downloadFile(sftp, sftpPath);
				if (fileByte != null && fileByte.length > 0) {
					return FileDownload
							.builder()
							.fileByte(fileByte)
							.fileName(fileName)
							.contentType(CONTENTTYPE_ZIP)
							.build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}