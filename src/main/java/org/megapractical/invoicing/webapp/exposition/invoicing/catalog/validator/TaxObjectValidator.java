package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.TaxObjectValidatorResponse;

public interface TaxObjectValidator {
	
	TaxObjectValidatorResponse validate(String taxObject);
	
	TaxObjectValidatorResponse validate(String taxObject, String field);
	
}