package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload;

import java.math.BigDecimal;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentBaseTax;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentWithheld;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WPaymentRequest {
	private String paymentDate;
	private FormaPagoEntity cmpPaymentWay;
	private MonedaEntity cmpPaymentCurrency;
	private BigDecimal cmpChangeType;
	private BigDecimal amount;
	private String operationNumber;
	private String sourceAccountRfc;
	private String bankName;
	private String payerAccount;
	private String targetAccountRfc;
	private String receiverAccount;
	private String stringType;
	private String stringTypeCode;
	private String paymentCertificate;
	private String originalString;
	private String paymentSeal;
	private List<ComplementPaymentWithheld> withhelds;
	private List<ComplementPaymentBaseTax> transferreds;
}