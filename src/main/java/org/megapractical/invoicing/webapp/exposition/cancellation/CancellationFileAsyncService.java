package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@EnableAsync
@Service
@RequiredArgsConstructor
class CancellationFileAsyncService {
	
	private final CancellationProcessingService cancellationProcessingService;
	
	@Async
	public Future<Boolean> processFileAsync(CancellationFileData cancellationFileData) {
		cancellationProcessingService.processingFile(cancellationFileData);
		return new AsyncResult<>(Boolean.TRUE);
	}
	
}