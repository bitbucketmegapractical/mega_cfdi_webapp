package org.megapractical.invoicing.webapp.exposition.retention.helper;

import java.math.BigDecimal;
import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.webapp.exposition.retention.payload.RetentionComplementResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRetention.RetInt;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import lombok.val;
import lombok.var;

@Component
public class RetentionComplementIntHelper {
	
	private final UserTools userTools;

	public RetentionComplementIntHelper(UserTools userTools) {
		this.userTools = userTools;
	}
	
	public RetentionComplementResponse populate(RetentionData retentionData, String retentionKey, final ApiRetention apiRetention, final List<JError> errors) {
		val retInt = retentionData.getRetInt();
		if (!UValidator.isNullOrEmpty(retInt)) {
			// Sistema financiero
			populateIntFinanceSystem(retInt, apiRetention, errors);
			// Intereses retirados en periodo
			populateIntRetirement(retInt, apiRetention, errors);
			// Operaciones financieras derivadas
			populateIntFinancialOperations(retInt, apiRetention, errors);
			// Importe interés nominal
			populateIntInterestAmount(retInt, apiRetention, errors);
			// Monto intereses reales
			populateIntRealInterest(retInt, apiRetention, errors);
			// Pérdida
			populateIntInterestWaste(retInt, apiRetention, errors);
			
			return RetentionComplementResponse.ok();
		} else if (retentionKey.equalsIgnoreCase("16")) {
			return RetentionComplementResponse.error();
		}
		return RetentionComplementResponse.empty();
	}
	
	private void populateIntFinanceSystem(RetInt retInt, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val intFinanceSystem = UBase64.base64Decode(retInt.getFinanceSystem());
		if (!UValidator.isNullOrEmpty(intFinanceSystem)) {
			if (intFinanceSystem.equals("SI") || intFinanceSystem.equals("NO")) {
				apiRetention.setFinanceSystem(intFinanceSystem);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("int-finance-system");
			errors.add(error);
		}
	}
	
	private void populateIntRetirement(RetInt retInt, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val intRetirement = UBase64.base64Decode(retInt.getRetirement());
		if (!UValidator.isNullOrEmpty(intRetirement)) {
			if (intRetirement.equals("SI") || intRetirement.equals("NO")) {
				apiRetention.setRetirement(intRetirement);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("int-retirement");
			errors.add(error);
		}
	}
	
	private void populateIntFinancialOperations(RetInt retInt, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val intFinancialOperations = UBase64.base64Decode(retInt.getFinancialOperations());
		if (!UValidator.isNullOrEmpty(intFinancialOperations)) {
			if (intFinancialOperations.equals("SI") || intFinancialOperations.equals("NO")) {
				apiRetention.setFinancialOperations(intFinancialOperations);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("int-financial-operations");
			errors.add(error);
		}
	}
	
	private void populateIntInterestAmount(RetInt retInt, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val intInterestAmount = UBase64.base64Decode(retInt.getInterestAmount());
		if (!UValidator.isNullOrEmpty(intInterestAmount)) {
			BigDecimal bgValidate = UValue.bigDecimalStrict(intInterestAmount);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setInterestAmount(intInterestAmount);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("int-interest-amount");
			errors.add(error);
		}
	}
	
	private void populateIntRealInterest(RetInt retInt, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val intRealInterest = UBase64.base64Decode(retInt.getRealInterest());
		if (!UValidator.isNullOrEmpty(intRealInterest)) {
			BigDecimal bgValidate = UValue.bigDecimalStrict(intRealInterest);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setRealInterest(intRealInterest);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("int-real-interest");
			errors.add(error);
		}
	}
	
	private void populateIntInterestWaste(RetInt retInt, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val intInterestWaste = UBase64.base64Decode(retInt.getInterestWaste());
		if (!UValidator.isNullOrEmpty(intInterestWaste)) {
			val bgValidate = UValue.bigDecimalStrict(intInterestWaste);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setInterestWaste(intInterestWaste);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("int-interest-waste");
			errors.add(error);
		}
	}
	
}