package org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax;

import java.math.BigDecimal;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.AmountValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.BaseValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.RateOrFeeValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.TaxValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRateOrFee;
import org.megapractical.invoicing.webapp.json.JTaxes;

public interface TaxValidator {

	BaseValidatorResponse validateBase(String base, String field, MonedaEntity currency, boolean isTransferred);

	JError validateTaxType(String taxType, String field, boolean isTransferred);

	JError validateFactorType(String factorType, 
							  String rateOrFee, 
							  String amount,
							  String field,
							  boolean isAutomaticCalculation,
							  boolean isTransferred);
	
	RateOrFeeValidatorResponse validateRateOrFee(JRateOrFee rateOrFeeJson, 
												 String rateOrFee, 
												 String factorType,
												 String taxType,
												 String field,
												 boolean isTransferred);
	
	AmountValidatorResponse validateAmount(BigDecimal base, 
										   BigDecimal rateOrFee, 
										   String factorType, 
										   String amount,
										   String field,
										   boolean isTransferred, 
										   boolean isAutomaticCalculation, 
										   MonedaEntity currency);
	
	TaxValidatorResponse validateTax(JTaxes taxes, 
									 String factorType, 
									 String taxType, 
									 BigDecimal rateOrFee,
									 boolean isTransferred);

}