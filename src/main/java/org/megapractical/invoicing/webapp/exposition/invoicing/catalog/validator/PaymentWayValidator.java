package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.PaymentWayValidatorResponse;

public interface PaymentWayValidator {
	
	PaymentWayValidatorResponse validate(String paymentWay);
	
	PaymentWayValidatorResponse validate(String paymentWay, String field);
	
}