package org.megapractical.invoicing.webapp.exposition.invoicing.validator.residency;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverResidencyResponse;
import org.megapractical.invoicing.webapp.json.JError;

public interface ResidencyValidator {

	ReceiverResidencyResponse validate(String resident, String residency);

	ReceiverResidencyResponse validate(String resident, String residency, String field, List<JError> errors);

}