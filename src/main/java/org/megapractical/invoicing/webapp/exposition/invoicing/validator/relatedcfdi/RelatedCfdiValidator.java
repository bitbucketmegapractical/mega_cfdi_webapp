package org.megapractical.invoicing.webapp.exposition.invoicing.validator.relatedcfdi;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRelatedCfdi.JRelatedCfdiRequest;
import org.megapractical.invoicing.webapp.json.JRelatedCfdi.JRelatedCfdiResponse;

public interface RelatedCfdiValidator {
	
	JRelatedCfdiResponse getRelatedCfdi(JRelatedCfdiRequest relatedCfdiRequest, List<JError> errors);
	
}