package org.megapractical.invoicing.webapp.exposition.service;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.megapractical.common.datetime.DateTimeFormat;
import org.megapractical.common.datetime.LocalDateUtils;
import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.dal.bean.jpa.NominaEntity;
import org.megapractical.invoicing.dal.data.repository.IPayrollJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.AllowStamp;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JPayroll;
import org.megapractical.invoicing.webapp.json.JPayroll.Payroll;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class PayrollServiceImpl implements PayrollService {
	
	private final UserTools userTools;
	private final ITaxpayerJpaRepository iTaxpayerJpaRepository;
	private final IPayrollJpaRepository iPayrollJpaRepository;
	
	private static final Integer INITIAL_PAGE = 0;
	private static final String SORT_PROPERTY = "id";
	
	@Override
	public boolean isAllowedToStamp(String curp, String serie, String folio) {
		val payrollInDb = iPayrollJpaRepository.findByCurpAndSerieAndFolio(curp, serie, folio).orElse(null);
		return payrollInDb == null;
	}
	
	@Override
	public AllowStamp verifyPayroll(String curp, String serie, String folio) {
		val payrollInDb = iPayrollJpaRepository.findByCurpAndSerieAndFolio(curp, serie, folio).orElse(null);
		if (payrollInDb != null) {
			return AllowStamp.notAllowed(payrollInDb.getUuid());
		}
		return AllowStamp.allowed();
	}
	
	@Override
	public NominaEntity save(ApiPayroll apiPayroll) {
		val payroll = mapToEntity(apiPayroll);
		return save(payroll);
	}

	@Override
	public NominaEntity save(NominaEntity payroll) {
		if (payroll != null) {
			return iPayrollJpaRepository.save(payroll);
		}
		return null;
	}
	
	@Override
	public JPayroll payrollList(String emitterRfc, String pageStr, int paginateSize, String searchBy, String date) {
		var page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
		page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
		
		val sort = Sort.by(Sort.Order.desc(SORT_PROPERTY));
		val pageRequest = PageRequest.of(page, paginateSize, sort);
		
		var stampDate = "";
		if (AssertUtils.hasValue(date)) {
			val ld = LocalDateUtils.parse(date, DateTimeFormat.CUSTOM_FORMAT);
			if (ld != null) {
				stampDate = ld.toString();
			}
		}
		
		val payrollPage = iPayrollJpaRepository.findAll(emitterRfc, searchBy, stampDate, pageRequest);
		val pagination = JPagination.buildPaginaton(payrollPage);
		val payrolls = getPayrolls(payrollPage);
		
		val notification = new JNotification();
		var searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
		if (!UValidator.isNullOrEmpty(searchBy)) {
			notification.setSearch(Boolean.TRUE);
			searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
		}

		if (payrolls.isEmpty()) {
			notification.setSearchResultMessage(searchResultEmpty);
			notification.setPanelMessage(Boolean.TRUE);
			notification.setDismiss(Boolean.FALSE);
			notification.setCssStyleClass(CssStyleClass.INFO.value());
		}
		
		val response = new JResponse();
		response.setSuccess(Boolean.TRUE);
		response.setNotification(notification);
		
		return JPayroll.builder().pagination(pagination).payrolls(payrolls).response(response).build();
	}
	
	@Override
	public NominaEntity findByUuid(String uuid) {
		return iPayrollJpaRepository.findByUuidIgnoreCaseAndCanceladoFalse(uuid);
	}
	
	@Override
	public NominaEntity findForCancellation(String emitterRfc, String uuid) {
		return iPayrollJpaRepository.findByEmisorRfcAndUuid(emitterRfc, uuid).orElse(null);
	}
	
	private List<Payroll> getPayrolls(Page<NominaEntity> payrollPage) {
		if (payrollPage != null) {
			return payrollPage.stream().map(Payroll::convert).filter(Objects::nonNull).collect(toList());
		}
		return Collections.emptyList();
	}
	
	private NominaEntity mapToEntity(ApiPayroll apiPayroll) {
		if (apiPayroll != null) {
			val apiCfdi = apiPayroll.getApiCfdi();
			val taxpayer = iTaxpayerJpaRepository.findByRfc(apiCfdi.getEmitter().getRfc());

			var payroll = new NominaEntity();
			payroll.setContribuyente(taxpayer);
			payroll.setEmisorRfc(taxpayer.getRfcActivo());
			payroll.setTipoNomina(apiPayroll.getPayrollType());
            payroll.setVersion(ApiPayroll.VERSION);
            payroll.setFechaPago(apiPayroll.getPaymentDate());
            payroll.setFechaInicialPago(apiPayroll.getPaymentIntialDate());
            payroll.setFechaFinalPago(apiPayroll.getPaymentEndDate());
            payroll.setNumeroDiasPagados(UValue.doubleValueStrict(apiPayroll.getNumberDaysPaid()));
            payroll.setCurp(apiPayroll.getPayrollReceiver().getCurp());
            payroll.setNumeroEmpleado(apiPayroll.getPayrollReceiver().getEmployeeNumber());
            payroll.setTotalPercepciones(UValue.doubleValueStrict(apiPayroll.getTotalPerceptions()));
            payroll.setTotalDeducciones(UValue.doubleValueStrict(apiPayroll.getTotalDeductions()));
            payroll.setTotalOtrosPagos(UValue.doubleValueStrict(apiPayroll.getTotalOtherPayments()));
            payroll.setRutaXml(apiPayroll.getXmlPath());
            payroll.setRutaPdf(apiPayroll.getPdfPath());
            payroll.setUuid(apiCfdi.getVoucher().getTaxSheet());
            payroll.setSerie(apiCfdi.getVoucher().getSerie());
            payroll.setFolio(apiCfdi.getVoucher().getFolio());
            payroll.setFechaHoraExpedicion(apiCfdi.getVoucher().getDateTimeExpedition());
            payroll.setFechaHoraTimbrado(apiCfdi.getVoucher().getDateTimeStamp());
            payroll.setCancelado(false);
            payroll.setCancelacionEnProceso(false);
            payroll.setSolicitudCancelacion(false);
            payroll.setStorage(apiPayroll.getStorageType());
            return payroll;
		}
		return null;
	}

}