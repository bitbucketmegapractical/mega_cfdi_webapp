package org.megapractical.invoicing.webapp.exposition.retention.helper;

import java.util.List;

import org.megapractical.invoicing.api.util.MoneyUtils;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.webapp.exposition.helper.ErrorHelper;
import org.megapractical.invoicing.webapp.exposition.retention.payload.RetentionComplementResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRetention.RetEna;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.springframework.stereotype.Component;

import lombok.val;

@Component
public class RetentionComplementEnaHelper {
	
	private final ErrorHelper errorHelper;
	
	public RetentionComplementEnaHelper(ErrorHelper errorHelper) {
		this.errorHelper = errorHelper;
	}

	public RetentionComplementResponse populate(RetentionData retentionData, String retentionKey, final ApiRetention apiRetention, final List<JError> errors) {
		val retEna = retentionData.getRetEna();
		if (!UValidator.isNullOrEmpty(retEna)) {
			// Contrato de intermedicación
			populateEnaContract(retEna, apiRetention, errors);
			// Ganancia
			populateEnaProfit(retEna, apiRetention, errors);
			// Pérdida
			populateEnaWaste(retEna, apiRetention, errors);
			
			return RetentionComplementResponse.ok();
		} else if (retentionKey.equalsIgnoreCase("06")) {
			return RetentionComplementResponse.error();
		}
		return RetentionComplementResponse.empty();
	}
	
	private void populateEnaContract(RetEna retEna, final ApiRetention apiRetention, final List<JError> errors) {
		val enaContract = UBase64.base64Decode(retEna.getContract());
		if (!UValidator.isNullOrEmpty(enaContract)) {
			apiRetention.setContract(enaContract);
		} else {
			errorHelper.requiredFieldError("ena-contract", errors);
		}
	}
	
	private void populateEnaProfit(RetEna retEna, final ApiRetention apiRetention, final List<JError> errors) {
		val field = "ena-profit";
		String errorCode = null;
		
		val enaProfit = UBase64.base64Decode(retEna.getProfit());
		if (!UValidator.isNullOrEmpty(enaProfit)) {
			val bgValidate = UValue.bigDecimalStrict(enaProfit);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setProfit(MoneyUtils.format(enaProfit));
			} else {
				errorCode = JError.INVALID_VALUE;
			}
		} else {
			errorCode = JError.REQUIRED_FIELD;
		}
		errorHelper.evaluateError(field, errorCode, errors);
	}
	
	private void populateEnaWaste(RetEna retEna, final ApiRetention apiRetention, final List<JError> errors) {
		val field = "ena-waste";
		String errorCode = null;
		
		val enaWaste = UBase64.base64Decode(retEna.getWaste());
		if (!UValidator.isNullOrEmpty(enaWaste)) {
			val bgValidate = UValue.bigDecimalStrict(enaWaste);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setWaste(MoneyUtils.format(enaWaste));
			} else {
				errorCode = JError.INVALID_VALUE;
			}
		} else {
			errorCode = JError.REQUIRED_FIELD;
		}
		errorHelper.evaluateError(field, errorCode, errors);
	}
	
}