package org.megapractical.invoicing.webapp.exposition.retention.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RetentionComplementResponse {
	private boolean retentionWithComplement;
	private boolean sectionError;
	
	public static RetentionComplementResponse ok() {
		return RetentionComplementResponse.builder().retentionWithComplement(true).sectionError(false).build();
	}
	
	public static RetentionComplementResponse error() {
		return RetentionComplementResponse.builder().retentionWithComplement(false).sectionError(true).build();
	}
	
	public static RetentionComplementResponse empty() {
		return RetentionComplementResponse.builder().retentionWithComplement(false).sectionError(false).build();
	}
}