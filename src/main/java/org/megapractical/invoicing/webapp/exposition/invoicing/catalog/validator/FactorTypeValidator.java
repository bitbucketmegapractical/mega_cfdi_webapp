package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CatalogValidatorResponse;

public interface FactorTypeValidator {
	
	CatalogValidatorResponse validate(String factorType, String rateOrFee, boolean isTransferred);
	
}