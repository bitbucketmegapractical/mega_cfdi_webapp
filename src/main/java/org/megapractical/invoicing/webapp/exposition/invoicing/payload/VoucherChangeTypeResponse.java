package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import java.math.BigDecimal;

import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoucherChangeTypeResponse {
	private BigDecimal changeType;
	private boolean error;
	
	public static VoucherChangeTypeResponse empty() {
		return VoucherChangeTypeResponse.builder().error(Boolean.FALSE).build();
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ChangeTypeResponse {
		private BigDecimal changeType;
		private JError error;
	}
}