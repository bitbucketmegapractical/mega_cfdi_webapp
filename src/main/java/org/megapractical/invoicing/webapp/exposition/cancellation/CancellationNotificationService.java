package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.time.LocalDateTime;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.dal.bean.jpa.NotificacionCancelacionEntity;
import org.megapractical.invoicing.dal.data.repository.ICancellationNotificationJpaRepository;
import org.megapractical.invoicing.webapp.exposition.cancellation.CancellationNotification.CancellationNotificationData;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.tenant.bancomext.mail.BancomextMailService;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
class CancellationNotificationService {
	
	private final MailService mailService;
	private final BancomextMailService bancomextMailService;
	private final ICancellationNotificationJpaRepository iCancellationNotificationJpaRepository;
	
	private static final String BANCOMEXT_RFC = "BNC8507311M4";
	
	public void sendNotification(CancellationNotification cancellationNotification) {
		val taxpayer = cancellationNotification.getTaxpayer();
		if (BANCOMEXT_RFC.equals(taxpayer.getRfcActivo())) {
			sendBancomextNotification(cancellationNotification);
		} else {
			sendCancellationNotification(cancellationNotification);
		}
	}
	
	private void sendCancellationNotification(CancellationNotification cancellationNotification) {
		val recipients = cancellationNotification.getRecipients();
		recipients.forEach(recipient -> {
			val notificationData = populateNotificationData(recipient, cancellationNotification);
			val notificationResponse = mailService.sendCancellationNotification(notificationData);
			save(cancellationNotification, notificationResponse, recipient);
		});
	}
	
	private void sendBancomextNotification(CancellationNotification cancellationNotification) {
		val recipients = cancellationNotification.getRecipients();
		recipients.forEach(recipient -> {
			val notificationData = populateNotificationData(recipient, cancellationNotification);
			val notificationResponse = bancomextMailService.sendCancellationNotification(notificationData);
			save(cancellationNotification, notificationResponse, recipient);
		});
	}
	
	private CancellationNotificationData populateNotificationData(String recipient, 
																  CancellationNotification cancellationNotification) {
		val taxPaayer = cancellationNotification.getTaxpayer();
		return CancellationNotificationData
				.builder()
				.emitterRfc(taxPaayer.getRfcActivo())
				.emitterName(taxPaayer.getNombreRazonSocial())
				.fileName(cancellationNotification.getFileName())
				.recipient(recipient)
				.subject(cancellationNotification.getSubject())
				.build();
	}
	
	private void save(CancellationNotification cancellationNotification,
					  CancellationNotificationResponse notificationResponse, 
					  String recipient) {
		if (AssertUtils.nonNull(cancellationNotification) && AssertUtils.nonNull(notificationResponse)) {
			val notification = new NotificacionCancelacionEntity();
			notification.setArchivoCancelacion(cancellationNotification.getCancellationFile());
			notification.setRecipient(recipient);
			notification.setSubject(cancellationNotification.getSubject());
			notification.setReason(notificationResponse.getReason());
			notification.setSendDate(LocalDateTime.now());
			iCancellationNotificationJpaRepository.save(notification);
		}
	}
}