package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JCalculation;
import org.megapractical.invoicing.webapp.wrapper.WAutomaticCalculations;
import org.megapractical.invoicing.webapp.wrapper.WConcept;

public interface InvoiceAutomaticCalculationService {
	
	JCalculation automaticCalculations(String currency, 
									   List<WConcept> wConcepts,
									   WAutomaticCalculations wAutomaticCalculations);
	
}