package org.megapractical.invoicing.webapp.exposition.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.CredencialEntity;
import org.megapractical.invoicing.dal.bean.jpa.CuentaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PerfilEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.ProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICredentialJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPreferencesJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRoleJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxRegimeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IUserJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.json.JAccount;
import org.megapractical.invoicing.webapp.json.JAccount.Account;
import org.megapractical.invoicing.webapp.json.JEmitter;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.EmitterTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.ui.Paginator;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import lombok.val;

@Controller
@Scope("session")
public class EmitterAccountController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	EmitterTools emitterTools;
	
	@Autowired
	DataStoreService dataStore;
	
	@Autowired
	SessionController sessionController;
	
	@Resource
	IUserJpaRepository iUserJpaRepository;
	
	@Resource
	IAccountJpaRepository iAccountJpaRepository;
	
	@Resource
	IPersonTypeJpaRepository iPersonTypeJpaRepository;
	
	@Resource
	ITaxpayerJpaRepository iTaxpayerJpaRepository;
	
	@Resource
	ITaxRegimeJpaRepository iTaxRegimeJpaRepository;
	
	@Resource
	ITaxpayerPersonTypeJpaRepository iTaxpayerPersonTypeJpaRepository;
	
	@Resource
	ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;
	
	@Resource
	ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository; 
	
	@Resource
	ICurrencyJpaRepository iCurrencyJpaRepository;
	
	@Resource
	IPreferencesJpaRepository iPreferencesJpaRepository;
	
	@Resource
	ICredentialJpaRepository iCredentialJpaRepository;
	
	@Resource
	IRoleJpaRepository iRoleJpaRepository;
	
	@Resource
	IProductServiceJpaRepository iProductServiceJpaRepository;
	
	@Resource
	ITaxpayerProductServiceJpaRepository iTaxpayerProductServiceJpaRepository;
	
	private static final String CURRENCY_CODE = "MXN";
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	private static final String SORT_PROPERTY = "id";
	private static final Integer INITIAL_PAGE = 0;
	private static final String ROL_CODE = "ROLE_MST";
	private static final String PRODUCT = "CFDI_PLUS";
	
	private static final String RFC_EXPRESSION = "[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?";
	private static final Integer RFC_MIN_LENGTH = 12;
	private static final Integer RFC_MAX_LENGTH = 13;
	
	private static final String PERSON_TYPE_PHYSICAL_CODE = "PF";
	private static final String PERSON_TYPE_MORAL_CODE = "PM";
	
	private String configAction;
	private String unexpectedError;
	
	
	@ModelAttribute("requiredMessage")
	public String requiredMessage() {
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	@ModelAttribute("processingRequest")
	public String processingRequest() {
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() {
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@ModelAttribute("updatingRequest")
	public String updatingRequest() {
		return UProperties.getMessage("mega.cfdi.updating", userTools.getLocale());
	}
	
	// Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() {
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	//##### Cargando Tipo contribuyente (Tipo persona)
	public List<String> personTypeSource(){
		List<String> personTypeSource = new ArrayList<>();
		try {
			
			Iterable<TipoPersonaEntity> personTypeEntitySource = iPersonTypeJpaRepository.findByEliminadoFalse();
			personTypeEntitySource.forEach(item -> personTypeSource.add(item.getValor()));	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return personTypeSource;
	}
	
	@RequestMapping(value = "/emitterAccount", method = RequestMethod.GET)
	public String emitterAccount(Model model, Integer page, String searchString) {
		try {
			
			this.unexpectedError = UProperties.genericUnexpectedError(userTools.getLocale());
			
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
			if(userTools.isMaster(taxpayer)){
				model.addAttribute("page", INITIAL_PAGE);
				model.addAttribute("personTypeSource", personTypeSource());
				
				HtmlHelper.functionalitySelected("EmitterConfig", "emitter-config-account");
				appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
				
				return "/config/EmitterAccount";
			}else{
				return "/denied/ResourceDenied";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/emitterAccount", method = RequestMethod.POST)
	public @ResponseBody String emitterAccount(HttpServletRequest request) {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JAccount account = new JAccount();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchRfc = UBase64.base64Decode(request.getParameter("searchRfc"));
			if(UValidator.isNullOrEmpty(searchRfc)){
				searchRfc = "";
			}
			
			Sort sort = Sort.by(Sort.Order.desc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Page<CuentaEntity> accountPage = null;
			Paginator<CuentaEntity> paginator = null;
			
			accountPage = iAccountJpaRepository.findByContribuyenteAndContribuyenteAsociado_RfcContainingIgnoreCaseAndEliminadoFalse(taxpayer, searchRfc, pageRequest);
			val pagination = JPagination.buildPaginaton(accountPage);
			account.setPagination(pagination);
			
			for (CuentaEntity item : accountPage) {
				Account accountObj = new Account();
				accountObj.setId(UBase64.base64Encode(item.getId().toString()));
				accountObj.setRfc(UBase64.base64Encode(item.getContribuyenteAsociado().getRfc()));
				accountObj.setNameOrBussinesName(UBase64.base64Encode(item.getContribuyenteAsociado().getNombreRazonSocial()));
				accountObj.setActive(rfcActive.equalsIgnoreCase(item.getContribuyenteAsociado().getRfc()) ? Boolean.TRUE : Boolean.FALSE);
				
				ContribuyenteTipoPersonaEntity ctp = iTaxpayerPersonTypeJpaRepository.findByContribuyente(item.getContribuyenteAsociado());
				if(!UValidator.isNullOrEmpty(ctp)){
					accountObj.setTaxpayerType(UBase64.base64Encode(ctp.getTipoPersona().getValor()));
				}
				
				ContribuyenteRegimenFiscalEntity crf = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(item.getContribuyenteAsociado());
				if(!UValidator.isNullOrEmpty(crf)){
					accountObj.setTaxRegime(UBase64.base64Encode(crf.getRegimenFiscal().getCodigo().concat(" (").concat(crf.getRegimenFiscal().getValor()).concat(")")));
				}
				
				//##### Determinando si la cuenta tiene usuario
				CredencialEntity credential = iCredentialJpaRepository.findByContribuyenteAndEliminadoFalse(item.getContribuyenteAsociado());
				if(!UValidator.isNullOrEmpty(credential)){
					accountObj.setAccountWithUser(Boolean.TRUE);
					accountObj.setUserBySystemCreated(credential.getUsuario().getGeneradoPorSistema());
				}else{
					accountObj.setAccountWithUser(Boolean.FALSE);
					accountObj.setUserBySystemCreated(Boolean.FALSE);
				}

				account.getAccounts().add(accountObj);
			}
			
			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if(!UValidator.isNullOrEmpty(searchRfc)){
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
			}
			
			if(account.getAccounts().isEmpty()){
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}
			
			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);	
			
			//##### Determinando si el contribuyente es Consignatario.
			//##### Los contribuyentes consignatarios son los únicos que pueden crear usuarios a las cuentas asociadas
			account.setConsignee(userTools.isConsigneeAvailable(taxpayer));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		account.setResponse(response);
		jsonInString = gson.toJson(account);
		return jsonInString;
	}
	
	/*@RequestMapping(value = "/emitterAccount", method = RequestMethod.GET)
	public String emitterAccount(Model model, Integer page, String searchString) {
		try {
			
			page = page != null ? page - 1 : 0;
			searchString = searchString != null && !searchString.equals("none") ? searchString : "";
			
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize());
			Page<CuentaEntity> cuentaEntityPage = iAccountJpaRepository.findByContribuyenteAndEliminadoFalse(userTools.getContribuyenteActive(), pageRequest);
			Paginator<CuentaEntity> paginator = new Paginator<CuentaEntity>(cuentaEntityPage, "/emitterAccount");
			
			List<WAccount> accountWrapperSource = new ArrayList<>();
			for (CuentaEntity item : paginator) {
				WAccount accountWrapper = new WAccount();
				accountWrapper.setRfc(item.getContribuyenteAsociado().getRfc());
				accountWrapper.setNameOrBusinessName(item.getContribuyenteAsociado().getNombreRazonSocial());
				
				ContribuyenteTipoPersonaEntity ctp = iTaxpayerPersonTypeJpaRepository.findByContribuyente(item.getContribuyenteAsociado());
				accountWrapper.setPersonType(ctp.getTipoPersona().getValor());
				
				ContribuyenteRegimenFiscalEntity crf = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(item.getContribuyenteAsociado());
				accountWrapper.setTaxRegime(crf.getRegimenFiscal().getCodigo().concat(" (").concat(crf.getRegimenFiscal().getValor()).concat(")"));
				
				CertificadoEntity certificate = iTaxpayerCertificateJpaRepository.findByContribuyente(item.getContribuyenteAsociado());
				accountWrapper.setCertificateNumber(certificate.getNumero());
				
				accountWrapperSource.add(accountWrapper);
			}
			
			//model.addAttribute("rfcAccountList", paginator);
			model.addAttribute("rfcAccountList", accountWrapperSource);
			model.addAttribute("empty", cuentaEntityPage.getTotalElements() == 0);
			model.addAttribute("page", page + 1);
			model.addAttribute("searchString", !searchString.isEmpty() ? searchString : "none");
			model.addAttribute("searchPlaceholder", searchString);
			
			return "/config/EmitterAccount";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/
	
	/*@RequestMapping(value = "/emitterAccount/addAccount/{action}", method = RequestMethod.GET)
	public String emitterAccount(Model model, @PathVariable(value="action") String action){
		try {
			
			Contribuyente taxpayer = new Contribuyente();
			taxpayerViewModel = new TaxpayerViewModel(taxpayer);
			taxpayerViewModel.setAction(action);
			
			model.addAttribute("personTypeSource", personTypeSource());
			model.addAttribute("taxpayerViewModel", taxpayerViewModel);
			
			return "/config/EmitterAccountConfig";			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/
	
	@RequestMapping(value="/associatedAccount", params={"accountDataLoad"}, method = RequestMethod.POST)
	public @ResponseBody String accountDataLoad(HttpServletRequest request) {
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		JEmitter emitter = new JEmitter();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			Boolean errorMarked = false;
			
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			CuentaEntity taxpayerAccount = iAccountJpaRepository.findByIdAndEliminadoFalse(id);
			if(!UValidator.isNullOrEmpty(taxpayerAccount)){
				ContribuyenteEntity taxpayer = iTaxpayerJpaRepository.findById(taxpayerAccount.getContribuyenteAsociado().getId()).orElse(null);
				if(UValidator.isNullOrEmpty(taxpayer)){
					errorMarked = true;
				}
				
				//##### La cuenta asociada inicialmente puede no tener Regimen fiscal
				//##### si fue asociada mediante codigo de seguridad y todavia no tenia 
				//##### la configuracion fiscal en el sistema
				ContribuyenteRegimenFiscalEntity taxpayerTaxRegime = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(taxpayer);
				/*if(UValidator.isNullOrEmpty(taxpayerTaxRegime)){
					errorMarked = true;
				}*/
				
				ContribuyenteTipoPersonaEntity taxpayerPersonType = iTaxpayerPersonTypeJpaRepository.findByContribuyente(taxpayer);
				if(UValidator.isNullOrEmpty(taxpayerPersonType)){
					errorMarked = true;
				}
				
				//##### La cuenta asociada inicialmente puede no tener Certificados configurados
				//##### si fue asociada mediante codigo de seguridad y todavia no tenia 
				//##### la configuracion fiscal en el sistema
				/*ContribuyenteCertificadoEntity taxpayerCertificate = iTaxpayerCertificateJpaRepository.findByContribuyente(taxpayer);
				if(UValidator.isNullOrEmpty(taxpayerCertificate)){
					errorMarked = true;
				}*/
				
				if(!errorMarked){
					emitter.setNameOrBusinessName(UBase64.base64Encode(taxpayer.getNombreRazonSocial()));
					emitter.setEmail(UBase64.base64Encode(taxpayerAccount.getCorreoElectronico()));
					emitter.setPersonType(UBase64.base64Encode(taxpayerPersonType.getTipoPersona().getValor()));
					emitter.setRfc(UBase64.base64Encode(taxpayer.getRfc()));
					
					if(!UValidator.isNullOrEmpty(taxpayerTaxRegime)){
						emitter.setTaxRegime(UBase64.base64Encode(taxpayerTaxRegime.getRegimenFiscal().getCodigo().concat(" (").concat(taxpayerTaxRegime.getRegimenFiscal().getValor()).concat(")")));
					}
					
					emitter.setPhone(UBase64.base64Encode(taxpayer.getTelefono()));
					
					response.setSuccess(Boolean.TRUE);
				}else{
					notification.setMessage(UProperties.getMessage("ERR0077", userTools.getLocale()));
					notification.setPageMessage(Boolean.TRUE);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setNotification(notification);
					response.setError(Boolean.TRUE);
				}
			}else{
				notification.setMessage(UProperties.getMessage("ERR0077", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0077", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}

		sessionController.sessionUpdate();
		
		emitter.setResponse(response);
		jsonInString = gson.toJson(emitter);
		return jsonInString;
	}
	
	@RequestMapping(value="/associatedAccount", params={"accountRemove"}, method = RequestMethod.POST)
    public @ResponseBody String accountRemove(HttpServletRequest request)  {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JEmitter emitter = new JEmitter();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			Boolean errorMarked = false;
			
			//ContribuyenteRegimenFiscalEntity taxpayerTaxRegime = null;
			ContribuyenteTipoPersonaEntity taxpayerPersonType = null;
			ContribuyenteCertificadoEntity taxpayerCertificate = null;
			ContribuyenteEntity taxpayerAssociated = null;
			
			CuentaEntity account = iAccountJpaRepository.findById(id).orElse(null);
			if(UValidator.isNullOrEmpty(account)){
				errorMarked = true;
			}
			
			if(!errorMarked){
				taxpayerAssociated = iTaxpayerJpaRepository.findById(account.getContribuyenteAsociado().getId()).orElse(null);
				if(UValidator.isNullOrEmpty(taxpayerAssociated)){
					errorMarked = true;
				}
			}
			
			if(!errorMarked){
				//##### La cuenta asociada inicialmente puede no tener Regimen fiscal
				//##### si fue asociada mediante codigo de seguridad y todavia no tenia 
				//##### la configuracion fiscal en el sistema
				/*taxpayerTaxRegime = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(taxpayerAssociated);
				if(UValidator.isNullOrEmpty(taxpayerTaxRegime)){
					errorMarked = true;
				}*/
			}
			
			if(!errorMarked){
				taxpayerPersonType = iTaxpayerPersonTypeJpaRepository.findByContribuyente(taxpayerAssociated);
				if(UValidator.isNullOrEmpty(taxpayerPersonType)){
					errorMarked = true;
				}
			}
			
			if(!errorMarked){
				//##### La cuenta asociada inicialmente puede no tener Certificados configurados
				//##### si fue asociada mediante codigo de seguridad y todavia no tenia 
				//##### la configuracion fiscal en el sistema
				taxpayerCertificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayerAssociated);
				/*if(UValidator.isNullOrEmpty(taxpayerCertificate)){
					errorMarked = true;
				}*/									
			}
			
			if(!errorMarked){
				account.setEliminado(Boolean.TRUE);
				persistenceDAO.update(account);
				
				if(!UValidator.isNullOrEmpty(taxpayerCertificate)){
					//##### Verificar si la cuenta asociada tiene usuario en el sistema
					//##### Si tiene, el certificado no se deshabilita
					CredencialEntity credential = iCredentialJpaRepository.findByContribuyenteAndEliminadoFalse(taxpayerAssociated);
					if(UValidator.isNullOrEmpty(credential)){
						taxpayerCertificate.setHabilitado(Boolean.FALSE);
						persistenceDAO.update(taxpayerCertificate);
					}
				}
				
				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.ELIMINAR_CUENTA_ID48);
				
				// Timeline :: Registro de accion
				timelineLog.timelineLog(EventTypeCode.ELIMINAR_CUENTA_ID23);
				
				response.setSuccess(Boolean.TRUE);
			}else{
				notification.setMessage(UProperties.getMessage("ERR0074", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0074", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();

		emitter.setResponse(response);
		jsonInString = gson.toJson(emitter);
		return jsonInString;
	}
	
	@RequestMapping(value="/associatedAccount", params={"securityCode"}, method = RequestMethod.POST)
	public @ResponseBody String accountSecurityCode(HttpServletRequest request)  {
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		JEmitter emitter = new JEmitter();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
			this.unexpectedError = UProperties.getMessage("ERR0072", userTools.getLocale());
			String accountSecurityCode = request.getParameter("accountSecurityCode");
			if(!UValidator.isNullOrEmpty(accountSecurityCode)){
				ContribuyenteEntity taxpayerAssociated = iTaxpayerJpaRepository.findByCodigoSeguridad(accountSecurityCode);
				if(!UValidator.isNullOrEmpty(taxpayerAssociated)){
					if(taxpayerAssociated.getRfc().equals(taxpayer.getRfc())){
						notification.setMessage(UProperties.getMessage("ERR0166", userTools.getLocale()));
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						
						response.setNotification(notification);
						response.setError(Boolean.TRUE);
					}else{
						CuentaEntity account = iAccountJpaRepository.findByContribuyenteAsociado_RfcIgnoreCaseAndEliminadoFalse(taxpayerAssociated.getRfc());
						if(!UValidator.isNullOrEmpty(account)){
							notification.setMessage(UProperties.getMessage("ERR0167", userTools.getLocale()));
							notification.setPageMessage(Boolean.TRUE);
							notification.setCssStyleClass(CssStyleClass.ERROR.value());
							
							response.setNotification(notification);
							response.setError(Boolean.TRUE);
						}else{
							try {
								
								//##### Determinando si la cuenta seleccionada por el codigo de seguridad tiene usuario en el sistema
								String email = null;
								CredencialEntity credentialEntity = iCredentialJpaRepository.findByContribuyenteAndEliminadoFalse(taxpayerAssociated);
								if(!UValidator.isNullOrEmpty(credentialEntity)){
									email = credentialEntity.getUsuario().getCorreoElectronico();
								}
								
								//##### Verificando si la cuenta asociada ha sido eliminada previamente
								CuentaEntity accountCheck = iAccountJpaRepository.findByContribuyenteAndContribuyenteAsociadoAndEliminadoTrue(taxpayer, taxpayerAssociated);
								if(UValidator.isNullOrEmpty(accountCheck)){
									//##### Registrando la informacion de la cuenta asociada 
									account = new CuentaEntity();
									account.setContribuyente(taxpayer);
									account.setContribuyenteAsociado(taxpayerAssociated);
									account.setCorreoElectronico(email);
									account.setEliminado(Boolean.FALSE);
									persistenceDAO.persist(account);
											
									userTools.log("[INFO] REGISTERING ASSOCICATED ACCOUNT BY SECURITY CODE...");
									
									//##### Actualizando contribuyente asociado
									taxpayerAssociated.setProductoServicioActivo("CFDI_PLUS");
									persistenceDAO.update(taxpayerAssociated);
									
									userTools.log("[INFO] UPDATING TAXPAYER ASSOCIATED INFO...");
									
									//##### Registrando preferencias del contribuyente asociado
									PreferenciasEntity preferences = iPreferencesJpaRepository.findByContribuyente(taxpayerAssociated);
									if(UValidator.isNullOrEmpty(preferences)){
										preferences = new PreferenciasEntity();
										preferences.setContribuyente(taxpayerAssociated);
										preferences.setMoneda(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(CURRENCY_CODE));
										preferences.setEmisorXmlPdf(Boolean.FALSE);
										preferences.setEmisorXml(Boolean.FALSE);
										preferences.setEmisorPdf(Boolean.FALSE);
										preferences.setReceptorXmlPdf(Boolean.FALSE);
										preferences.setReceptorXml(Boolean.FALSE);
										preferences.setReceptorPdf(Boolean.FALSE);
										preferences.setContactoXmlPdf(Boolean.FALSE);
										preferences.setContactoXml(Boolean.FALSE);
										preferences.setContactoPdf(Boolean.FALSE);
										preferences.setRegistrarCliente(Boolean.TRUE);
										preferences.setActualizarCliente(Boolean.TRUE);
										preferences.setRegistrarConcepto(Boolean.TRUE);
										preferences.setActualizarConcepto(Boolean.TRUE);
										preferences.setRegistrarRegimenFiscal(Boolean.TRUE);
										preferences.setActualizarRegimenFiscal(Boolean.TRUE);
										preferences.setReutilizarFolioCancelado(Boolean.FALSE);
										preferences.setIncrementarFolioFinal(Boolean.FALSE);
										preferences.setRegistrarActualizarLugarExpedicion(Boolean.FALSE);
										preferences.setPrefacturaPdf(Boolean.FALSE);
										persistenceDAO.persist(preferences);
										
										userTools.log("[INFO] REGISTERING TAXPAYER ASSOCIATED PREFERENCES...");
									}
									
									//##### Verificando si el contribuyente asociado tiene perfil
									//##### Si tiene, actualizarlo a Master
									PerfilEntity profile = userTools.getProfile(taxpayerAssociated);
									if(!UValidator.isNullOrEmpty(profile)){
										profile.setRol(iRoleJpaRepository.findByCodigo(ROL_CODE));
										persistenceDAO.update(profile);
										
										userTools.log("[INFO] REGISTERING TAXPAYER ASSOCIATED PROFILE...");
									}
									
									//##### Verificando si el contribuyente asociado tiene producto servicio
									ProductoServicioEntity productService = iProductServiceJpaRepository.findByCodigoAndHabilitadoTrue(PRODUCT);
									ContribuyenteProductoServicioEntity taxpayerProductService = iTaxpayerProductServiceJpaRepository.findByContribuyente(taxpayerAssociated);
									if(UValidator.isNullOrEmpty(taxpayerProductService)){
										//##### Registrando producto servicio
										taxpayerProductService = new ContribuyenteProductoServicioEntity();
										taxpayerProductService.setContribuyente(taxpayerAssociated);
										taxpayerProductService.setProductoServicio(productService);
										taxpayerProductService.setHabilitado(Boolean.TRUE);
										persistenceDAO.persist(taxpayerProductService);
										
										userTools.log("[INFO] REGISTERING TAXPAYER ASSOCIATED PRODUCT SERVICE...");
									}else{
										//##### Actualizando producto servicio
										taxpayerProductService.setProductoServicio(productService);
										taxpayerProductService.setHabilitado(Boolean.TRUE);
										persistenceDAO.update(taxpayerProductService);
										
										userTools.log("[INFO] UPDATING TAXPAYER ASSOCIATED PRODUCT SERVICE...");
									}
									
									//##### Cargando el usuario del contribuyente asociado
									UsuarioEntity user = userTools.getUserByTaxpayer(taxpayerAssociated);
									if(!UValidator.isNullOrEmpty(user)) {
										//##### Actualizando authority del usuario
										val role = iRoleJpaRepository.findByCodigo(ROL_CODE);
										user.setRol(role);
										if (!user.isActivo()) {
											user.setActivo(true);
										}
										persistenceDAO.update(user);
										
										userTools.log("[INFO] UPDATING TAXPAYER ASSOCIATED...");
									}
									
									//##### Verificando si el contribuyente tiene tipo de persona creada
									ContribuyenteTipoPersonaEntity taxpayerPersonType = iTaxpayerPersonTypeJpaRepository.findByContribuyente(taxpayerAssociated);
									if(UValidator.isNullOrEmpty(taxpayerPersonType)){
										String taxpayerAssociatedRfc = taxpayerAssociated.getRfc();
										
										TipoPersonaEntity personType = new TipoPersonaEntity();
										if(taxpayerAssociatedRfc.length() == RFC_MIN_LENGTH){
											personType = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(PERSON_TYPE_MORAL_CODE);
										}else if(taxpayerAssociatedRfc.length() == RFC_MAX_LENGTH){
											personType = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(PERSON_TYPE_PHYSICAL_CODE);
										}
										
										taxpayerPersonType = new ContribuyenteTipoPersonaEntity();
										taxpayerPersonType.setContribuyente(taxpayerAssociated);
										taxpayerPersonType.setTipoPersona(personType);
										persistenceDAO.persist(taxpayerPersonType);
										
										userTools.log("[INFO] REGISTERING TAXPAYER ASSOCIATED PERSON TYPE...");
									}
								}else{
									accountCheck.setEliminado(Boolean.FALSE);
									persistenceDAO.update(accountCheck);
								}
								
								// Bitacora :: Registro de accion en bitacora
								appLog.logOperation(OperationCode.REGISTRO_CONTRIBUYENTE_ASOCIADO_ID29);
								
								// Timeline :: Registro de accion
								timelineLog.timelineLog(EventTypeCode.CONFIGURACION_CUENTA_ASOCIADA_ID13, taxpayerAssociated);
								
								// Notificacion Toastr
								Toastr toastr = new Toastr(); 
								toastr.setTitle(UProperties.getMessage("mega.cfdi.premium.configuration.account.rfc", userTools.getLocale()));
								toastr.setMessage("Información registrada satisfactoriamente.");
								toastr.setType(Toastr.ToastrType.SUCCESS.value());
								
								// Mensaje de notificacion
								notification.setToastrNotification(Boolean.TRUE);
								notification.setToastr(toastr);
								
								// JSon response 
								response.setSuccess(Boolean.TRUE);
								response.setNotification(notification);
								
							} catch (Exception e) {
								notification.setMessage(this.unexpectedError);
								notification.setPageMessage(Boolean.TRUE);
								notification.setCssStyleClass(CssStyleClass.ERROR.value());
								
								response.setNotification(notification);
								response.setError(Boolean.TRUE);
							}
						}
					}
				}else{
					notification.setMessage(UProperties.getMessage("ERR0165", userTools.getLocale()));
					notification.setPageMessage(Boolean.TRUE);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setNotification(notification);
					response.setError(Boolean.TRUE);
				}
			}else{
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				error.setField("security-code");
				errors.add(error);
				
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(this.unexpectedError);
			
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		if(!errors.isEmpty()){
			response.setErrors(errors);
		}
		
		emitter.setResponse(response);
		jsonInString = gson.toJson(emitter);
		return jsonInString;
	}
	
	/*
	 * Para parametros que pueden ser opcionales, utilizar 
	 * @RequestParam(value="paramName") Optional<T> param
	 * o en su defecto
	 * @RequestParam(value="paramName", required = false) DataType param 
	 */	
	@RequestMapping(value = "/associatedAccount", method = RequestMethod.POST)
	public @ResponseBody String accountAddEdit(HttpServletRequest request, @RequestParam(value="certificate", required = false) Optional<MultipartFile> certificateParam,
			@RequestParam(value="privateKey", required = false) Optional<MultipartFile> privateKeyParam)  {
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		JEmitter emitter = new JEmitter();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
			this.configAction = UBase64.base64Decode(request.getParameter("operation"));
			this.unexpectedError = this.configAction.equals("config") ? UProperties.getMessage("ERR0072", userTools.getLocale()) : UProperties.getMessage("ERR0073", userTools.getLocale());
			
			String certificateEdit = request.getParameter("certificateEdit");
			Boolean isCertificateEdit = Boolean.valueOf(certificateEdit);
			
			Boolean accountDeletedFinded = false;
			Boolean errorMarked = false;
			
			if(request.getParameter("objEmitter") != null){
				String objEmitter = request.getParameter("objEmitter");
				emitter = gson.fromJson(objEmitter, JEmitter.class);
				
				//##### Id
				Long accountIndex = null;
				String index = UBase64.base64Decode(emitter.getIndex());
				if(!UValidator.isNullOrEmpty(index)){
					accountIndex = UValue.longValue(index);
				}
				
				//##### RFC
				String rfc = UBase64.base64Decode(emitter.getRfc());
				error = new JError();
				Boolean rfcError = false;

				if(UValidator.isNullOrEmpty(rfc)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else if(rfc.toString().length() < 12 || !rfc.toString().matches(RFC_EXPRESSION)){
					error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
					errorMarked = true;
				}else if(rfc.equalsIgnoreCase(taxpayer.getRfc())){
					error.setMessage(UProperties.getMessage("ERR0061", userTools.getLocale()));
					errorMarked = true;
				}else{
					String rfcUpperCase = rfc.toUpperCase();
					if(this.configAction.equals("config")){
						CuentaEntity account = iAccountJpaRepository.findByContribuyenteAsociado_RfcIgnoreCaseAndEliminadoFalse(rfcUpperCase);
						if(!UValidator.isNullOrEmpty(account)){
							error.setMessage(UProperties.getMessage("ERR0061", userTools.getLocale()));
							errorMarked = true;
						}else{
							ContribuyenteEntity taxpayerValidate = iTaxpayerJpaRepository.findByRfc(rfcUpperCase);
							if(!UValidator.isNullOrEmpty(taxpayerValidate)){
								error.setMessage(UProperties.getMessage("ERR0061", userTools.getLocale()));
								errorMarked = true;
							}else{
								account = iAccountJpaRepository.findByContribuyenteAsociado_RfcIgnoreCaseAndEliminadoTrue(rfcUpperCase);
								if(!UValidator.isNullOrEmpty(account)){
									accountIndex = account.getId();
									accountDeletedFinded = true;
								}
							}
						}
					}else if(this.configAction.equals("edit")){
						CuentaEntity account = iAccountJpaRepository.findByIdAndEliminadoFalse(accountIndex);
						String accountRfc = account.getContribuyenteAsociado().getRfc().toUpperCase(); 
						if(!accountRfc.equalsIgnoreCase(rfcUpperCase)){
							ContribuyenteEntity taxpayerValidate = iTaxpayerJpaRepository.findByRfc(rfcUpperCase);
							if(!UValidator.isNullOrEmpty(taxpayerValidate)){
								error.setMessage(UProperties.getMessage("ERR0061", userTools.getLocale()));
								errorMarked = true;
							}
						}
					}
				}
				if(errorMarked){
					error.setField("rfc");
					errors.add(error);
					rfcError = true;
				}
				
				//##### Nombre o razon social
				String nameOrBusinessName = UBase64.base64Decode(emitter.getNameOrBusinessName());
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(nameOrBusinessName)){
					error.setField("nameOrBusinessName");
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errors.add(error);
				}
				
				//##### Correo electronico
				String email = UBase64.base64Decode(emitter.getEmail());
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(email)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					if(this.configAction.equals("config")){
						CuentaEntity account = iAccountJpaRepository.findByCorreoElectronicoAndEliminadoFalse(email);
						if(!UValidator.isNullOrEmpty(account)){
							error.setMessage(UProperties.getMessage("ERR0086", userTools.getLocale()));
							errorMarked = true;
						}else{
							account = iAccountJpaRepository.findByCorreoElectronicoAndEliminadoTrue(email);
							if(!UValidator.isNullOrEmpty(account)){
								accountIndex = account.getId();
								accountDeletedFinded = true;
							}
						}
						if(!accountDeletedFinded){
							UsuarioEntity user = iUserJpaRepository.findByCorreoElectronico(email);
							if(!UValidator.isNullOrEmpty(user)){
								error.setMessage(UProperties.getMessage("ERR0042", userTools.getLocale()));
								errorMarked = true;
							}							
						}
					}else if(this.configAction.equals("edit")){
						CuentaEntity account = iAccountJpaRepository.findByIdAndEliminadoFalse(accountIndex);
						String accountEmail = account.getCorreoElectronico();
						if(!email.equalsIgnoreCase(accountEmail)){
							account = iAccountJpaRepository.findByCorreoElectronico(email);
							if(!UValidator.isNullOrEmpty(account)){
								error.setMessage(UProperties.getMessage("ERR0086", userTools.getLocale()));
								errorMarked = true;
							}else{
								UsuarioEntity user = iUserJpaRepository.findByCorreoElectronico(email);
								if(!UValidator.isNullOrEmpty(user)){
									error.setMessage(UProperties.getMessage("ERR0042", userTools.getLocale()));
									errorMarked = true;
								}
							}							
						}
					}
				}
				if(errorMarked){
					error.setField("email");
					errors.add(error);
				}
				
				//##### Telefono
				String phone = UBase64.base64Decode(emitter.getPhone());
				
				//##### Tipo contribuyente (Persona)
				String emitterType = UBase64.base64Decode(emitter.getPersonType());
				error = new JError();
				errorMarked = false;
				Boolean emitterTypeError = false;
				
				TipoPersonaEntity emitterTypeEntity = null;
				if(UValidator.isNullOrEmpty(emitterType)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					emitterTypeEntity = iPersonTypeJpaRepository.findByValorAndEliminadoFalse(emitterType);
					if(UValidator.isNullOrEmpty(emitterTypeEntity)){
						error.setMessage(UProperties.getMessage("ERR0002", userTools.getLocale()));
						errorMarked = true;
					}
				}
				if(errorMarked){
					error.setField("emitter-type");
					errors.add(error);
					emitterTypeError = true;
				}
				
				//##### Regimen fiscal
				String taxRegime = UBase64.base64Decode(emitter.getTaxRegime());
				error = new JError();
				errorMarked = false;
				
				RegimenFiscalEntity regimenFiscalEntityByCode = null;
				RegimenFiscalEntity regimenFiscalEntityByValue = null;
				if(UValidator.isNullOrEmpty(taxRegime)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					// Validando que sea un valor de catalogo
					String[] regimenFiscalCatalog = UCatalog.getCatalog(taxRegime);
					taxRegime = regimenFiscalCatalog[0];
					String regimenFiscalValue = regimenFiscalCatalog[1];
					
					regimenFiscalEntityByCode = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalse(taxRegime);
					regimenFiscalEntityByValue = iTaxRegimeJpaRepository.findByValorAndEliminadoFalse(regimenFiscalValue);
					
					if(regimenFiscalEntityByCode == null || regimenFiscalEntityByValue == null){
						String[] values = new String[]{"CFDI33130", "RegimenFiscal", "c_RegimenFiscal"};
						error.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
						errorMarked = true;
					}else{
						// Validar que el régimen fiscal corresponda al tipo de persona
						RegimenFiscalEntity regimenFiscalValidate = null;
						if(emitterType.equalsIgnoreCase("persona física")){
							regimenFiscalValidate = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalseAndPersonaFisicaTrue(taxRegime);
						}else if(emitterType.equalsIgnoreCase("persona moral")){
							regimenFiscalValidate = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalseAndPersonaMoralTrue(taxRegime);
						}
						if(regimenFiscalValidate == null){
							error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33131").value(), userTools.getLocale()));
							errorMarked = true;
						}
					}
				}
				if(errorMarked){
					error.setField("tax-regime");
					errors.add(error);
				}
				
				//##### Certificado
				File certificate = null;
				error = new JError();
				errorMarked = false;
				
				MultipartFile multipartFileCertificate;
				if(this.configAction.equals("edit") && isCertificateEdit){					
					if (UValidator.isNullOrEmpty(certificateParam.get())) {
						error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
						error.setField("certificate");
						errors.add(error);
						
						errorMarked = true;
					}
				}
				
				if(!errorMarked){
					try {
						
						multipartFileCertificate = certificateParam.get();
						if (!multipartFileCertificate.isEmpty()) {
							String certificateName = multipartFileCertificate.getOriginalFilename();
							String certificateExt = UFile.getExtension(certificateName);
							if(certificateExt == null || !certificateExt.equalsIgnoreCase("cer")){
								error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
								errorMarked = true;
							}else{
								certificate = UFile.multipartFileToFile(multipartFileCertificate);
							}
						}
						
					} catch (Exception e) {
						multipartFileCertificate = null;
					}
				}				
				
				//##### Llave privada
				File privateKey = null;
				error = new JError();
				errorMarked = false;
				
				MultipartFile multipartFilePrivateKey;
				if(this.configAction.equals("edit") && isCertificateEdit){
					if(UValidator.isNullOrEmpty(privateKeyParam.get())){
						error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
						error.setField("private-key");
						errors.add(error);
						
						errorMarked = true;
					}
				}
				
				if(!errorMarked){
					try {
						
						multipartFilePrivateKey = privateKeyParam.get();
						if (!multipartFilePrivateKey.isEmpty()) {
							String privateKeyName = multipartFilePrivateKey.getOriginalFilename();
							String privateKeyExt = UFile.getExtension(privateKeyName);
							if(privateKeyExt == null || !privateKeyExt.equalsIgnoreCase("key")){
								error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
								errorMarked = true;
							}else{
								privateKey = UFile.multipartFileToFile(multipartFilePrivateKey);
							}
						}
						
					} catch (Exception e) {
						multipartFilePrivateKey = null;
					}
				}
				
				//##### Clave de la llave privada
				String passwd = UBase64.base64Decode(emitter.getPasswd());
				if(this.configAction.equals("edit") && isCertificateEdit){
					if(UValidator.isNullOrEmpty(passwd)){
						error.setField("passwd");
						error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
						errors.add(error);
					}
				}
				
				Boolean isCertificateInfo = false;
				if(this.configAction.equals("config")){
					if(!UValidator.isNullOrEmpty(certificate) && !UValidator.isNullOrEmpty(privateKey) && !UValidator.isNullOrEmpty(passwd)){
						isCertificateInfo = true;
					}
				}
				
				Date certificateCreationDate = null;
				Date certificateExpirationDate = null;
				if(isCertificateInfo || (this.configAction.equals("edit") && isCertificateEdit)){
					String certificateValidate = UCertificateX509.certificateValidate(certificate, privateKey, passwd);
					
					error = new JError();
					errorMarked = false;
					
					if(!certificateValidate.equals("VALIDATE_SUCCESSFULY")){
						if(certificateValidate.equals("CERTIFICATE_AND_PRIVATE_KEY_DOES_NOT_CORRESPOND")){
							error.setMessage(UProperties.getMessage("ERR0005", userTools.getLocale()));
							error.setField("certificate");
							errorMarked = true;
						}else if(certificateValidate.equals("BAD_PADDING_EXCEPTION")){
							error.setMessage(UProperties.getMessage("ERR0006", userTools.getLocale()));
							error.setField("passwd");
							errorMarked = true;
						}
					}else{
						//##### Validando que el certificado no sea fiel
						if(UCertificateX509.isFiel(certificate)){
							// Es fiel
							error.setField("certificate");
							error.setMessage(UProperties.getMessage("ERR0003", userTools.getLocale()));
							errorMarked = true;
						}else{
							certificateCreationDate = UCertificateX509.certificateCreationDate(certificate);
							certificateExpirationDate = UCertificateX509.certificateExpirationDate(certificate);
							Date today = new Date();
							if(UDateTime.isBeforeLocalDate(certificateExpirationDate, today)){
								error.setField("certificate");
								error.setMessage(UProperties.getMessage("ERR0004", userTools.getLocale()));
								
							}
						}
					}
					if(errorMarked){
						errors.add(error);
					}
					
					if(!rfcError && !emitterTypeError){
						error = new JError();
						errorMarked = false;
						if(emitterTypeEntity.getCodigo().equals("PF")){
							if(!rfc.equals(UCertificateX509.getCertPersonRFC(certificate))){
								error.setMessage(UProperties.getMessage("ERR0041", userTools.getLocale()));
								errorMarked = true;
							}
						}else if(emitterTypeEntity.getCodigo().equals("PM")){
							if(!rfc.equals(UCertificateX509.getCertLegalRepresentativePersonRFC(certificate))){
								error.setMessage(UProperties.getMessage("ERR0041", userTools.getLocale()));
								errorMarked = true;
							}
						}
						if(errorMarked){
							error.setField("rfc");
							errors.add(error);
						}
					}
				}
				
				if(!errors.isEmpty()){
					response.setError(Boolean.TRUE);
					response.setErrors(errors);
				}else{
					try {
						
						if(this.configAction.equals("edit") && UValidator.isNullOrEmpty(accountIndex)){
							notification.setPageMessage(Boolean.TRUE);
							notification.setCssStyleClass(CssStyleClass.ERROR.value());
							notification.setMessage(this.unexpectedError);
							
							response.setError(Boolean.TRUE);
							response.setNotification(notification);
						}else{
							String cerPath = null;
							String privateKeyPath = null;
							String certificateNumber = null;
							
							if(isCertificateInfo || (this.configAction.equals("edit") && isCertificateEdit)){
								String emitterPath = appSettings.getPropertyValue("cfdi.cert.path");
								String filePath = emitterPath + File.separator + rfc;
								File fileFolder = new File(filePath); 
								if(!fileFolder.exists()){
									fileFolder.mkdirs();
								}
								
								// Certificado
								String cerFileName = rfc + ".cer";
								cerPath = filePath + File.separator + cerFileName;
								UFile.writeFile(UFile.fileToByte(certificate), cerPath);
								
								// Llave privada
								String privateKeyFileName = rfc + ".key";
								privateKeyPath = filePath + File.separator + privateKeyFileName;
								UFile.writeFile(UFile.fileToByte(privateKey), privateKeyPath);
								
								certificateNumber = UCertificateX509.certificateSerialNumber(UFile.fileToByte(certificate));
							}
							
							WAccount wAccount = new WAccount();
							wAccount.setTaxpayer(taxpayer);
							wAccount.setAction(this.configAction);
							wAccount.setAccountDeletedFinded(accountDeletedFinded);
							
							CuentaEntity taxpayerAccount = null;
							ContribuyenteRegimenFiscalEntity taxpayerTaxRegime = null;
							ContribuyenteTipoPersonaEntity taxpayerPersonType = null;
							ContribuyenteCertificadoEntity taxpayerCertificate = null;
							ContribuyenteEntity taxpayerAssociated = null;
							PreferenciasEntity preferences = null;
							
							errorMarked = false;
							if(this.configAction.equals("config") && !accountDeletedFinded){
								taxpayerAssociated = new ContribuyenteEntity();
								taxpayerTaxRegime = new ContribuyenteRegimenFiscalEntity();
								taxpayerPersonType = new ContribuyenteTipoPersonaEntity();
								taxpayerCertificate = new ContribuyenteCertificadoEntity();
								taxpayerAccount = new CuentaEntity();
								preferences = new PreferenciasEntity();
							}else if(this.configAction.equals("edit") || (this.configAction.equals("config") || accountDeletedFinded)){
								taxpayerAccount = iAccountJpaRepository.findById(accountIndex).orElse(null);
								if(UValidator.isNullOrEmpty(taxpayerAccount)){
									errorMarked = true;
								}
								
								if(!errorMarked){
									taxpayerAssociated = iTaxpayerJpaRepository.findById(taxpayerAccount.getContribuyenteAsociado().getId()).orElse(null);
									if(UValidator.isNullOrEmpty(taxpayerAssociated)){
										errorMarked = true;
									}
								}
								
								if(!errorMarked){
									//##### La cuenta asociada inicialmente puede no tener Regimen fiscal
									//##### si fue asociada mediante codigo de seguridad y todavia no tenia 
									//##### la configuracion fiscal en el sistema
									taxpayerTaxRegime = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(taxpayerAssociated);
									if(UValidator.isNullOrEmpty(taxpayerTaxRegime)){
										//errorMarked = true;
										taxpayerTaxRegime = new ContribuyenteRegimenFiscalEntity();
									}
								}
								
								if(!errorMarked){
									taxpayerPersonType = iTaxpayerPersonTypeJpaRepository.findByContribuyente(taxpayerAssociated);
									if(UValidator.isNullOrEmpty(taxpayerPersonType)){
										errorMarked = true;
									}
								}
								
								if(!errorMarked){
									if(accountDeletedFinded || (this.configAction.equals("edit") && isCertificateEdit)){
										//##### La cuenta asociada puede no tener informacion de Certificados
										taxpayerCertificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayerAssociated);
										if(UValidator.isNullOrEmpty(taxpayerCertificate)){
											taxpayerCertificate = new ContribuyenteCertificadoEntity();
										}
									}									
								}
								
								if(!errorMarked){
									if(accountDeletedFinded){
										preferences = iPreferencesJpaRepository.findByContribuyente(taxpayerAssociated);
										if(UValidator.isNullOrEmpty(preferences)){
											errorMarked = true;
										}
									}
								}
							}
							
							if(!errorMarked){
								//##### Contribuyente - Cuenta asociada
								taxpayerAssociated.setNombreRazonSocial(nameOrBusinessName.toUpperCase());
								taxpayerAssociated.setRfc(rfc.toUpperCase());
								taxpayerAssociated.setRfcActivo(rfc.toUpperCase());
								taxpayerAssociated.setTelefono(phone);
								taxpayerAssociated.setProductoServicioActivo("CFDI_PLUS");
								taxpayerAssociated.setCodigoSeguridad(UBase64.base64Encode(UValue.stringValueUppercase(UValue.uuid().replaceAll("-", ""))));
								
								Date today = new Date();
								taxpayerAssociated.setFechaInicioPlan(today);
								taxpayerAssociated.setFechaFinPlan(taxpayer.getFechaFinPlan());
								wAccount.setTaxpayerAsociated(taxpayerAssociated);
								
								//##### Cuenta asociada - Regimen fiscal
								taxpayerTaxRegime.setContribuyente(taxpayerAssociated);
								taxpayerTaxRegime.setRegimenFiscal(regimenFiscalEntityByCode);
								wAccount.setTaxpayerTaxRegime(taxpayerTaxRegime);
								
								//##### Cuenta asociada - Tipo persona
								taxpayerPersonType.setContribuyente(taxpayerAssociated);
								taxpayerPersonType.setTipoPersona(emitterTypeEntity);
								wAccount.setTaxpayerPersonType(taxpayerPersonType);
								
								//##### Cuenta asociada - Cer, Key y Passwd
								if((this.configAction.equals("config") || accountDeletedFinded) || (this.configAction.equals("edit") && isCertificateEdit)){
									if(UValidator.isNullOrEmpty(taxpayerCertificate.getContribuyente())){
										taxpayerCertificate = new ContribuyenteCertificadoEntity();
									}
									if(!UValidator.isNullOrEmpty(certificate) && !UValidator.isNullOrEmpty(privateKey)){
										taxpayerCertificate.setContribuyente(taxpayerAssociated);
										taxpayerCertificate.setClavePrivada(UBase64.base64Encode(passwd));
										taxpayerCertificate.setFechaCreacion(certificateCreationDate);
										taxpayerCertificate.setFechaExpiracion(certificateExpirationDate);
										taxpayerCertificate.setHabilitado(true);
										taxpayerCertificate.setNumero(certificateNumber);
										taxpayerCertificate.setCertificado(UFile.fileToByte(certificate));
										taxpayerCertificate.setRutaCertificado(cerPath);
										taxpayerCertificate.setLlavePrivada(UFile.fileToByte(privateKey));
										taxpayerCertificate.setRutaLlavePrivada(privateKeyPath);
									}else{
										taxpayerCertificate = null;
									}
								}else{
									taxpayerCertificate = null;
								}
								wAccount.setTaxpayerCertificate(taxpayerCertificate);
								
								//##### Cuenta asociada - Cuenta
								taxpayerAccount.setContribuyente(taxpayer);
								taxpayerAccount.setContribuyenteAsociado(taxpayerAssociated);
								taxpayerAccount.setCorreoElectronico(email);
								taxpayerAccount.setEliminado(Boolean.FALSE);
								wAccount.setTaxpayerAccount(taxpayerAccount);
								
								//##### Cuenta asociada - Preferencias
								if(this.configAction.equals("config") || accountDeletedFinded){
									preferences.setContribuyente(taxpayerAssociated);
									preferences.setMoneda(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(CURRENCY_CODE));
									preferences.setEmisorXmlPdf(Boolean.FALSE);
									preferences.setEmisorXml(Boolean.FALSE);
									preferences.setEmisorPdf(Boolean.FALSE);
									preferences.setReceptorXmlPdf(Boolean.FALSE);
									preferences.setReceptorXml(Boolean.FALSE);
									preferences.setReceptorPdf(Boolean.FALSE);
									preferences.setContactoXmlPdf(Boolean.FALSE);
									preferences.setContactoXml(Boolean.FALSE);
									preferences.setContactoPdf(Boolean.FALSE);
									preferences.setRegistrarCliente(Boolean.TRUE);
									preferences.setActualizarCliente(Boolean.TRUE);
									preferences.setRegistrarConcepto(Boolean.TRUE);
									preferences.setActualizarConcepto(Boolean.TRUE);
									preferences.setRegistrarRegimenFiscal(Boolean.TRUE);
									preferences.setActualizarRegimenFiscal(Boolean.TRUE);
									preferences.setReutilizarFolioCancelado(Boolean.FALSE);
									preferences.setIncrementarFolioFinal(Boolean.FALSE);
									preferences.setRegistrarActualizarLugarExpedicion(Boolean.FALSE);
									preferences.setPrefacturaPdf(Boolean.FALSE);
								}else{
									preferences = null;
								}
								wAccount.setPreferences(preferences);
								
								taxpayerAssociated = null;
								taxpayerAssociated = dataStore.taxpayerAccountStore(wAccount);
								if(!UValidator.isNullOrEmpty(taxpayerAssociated)){
									List<OperationCode> operationCodeSource = new ArrayList<>();
									EventTypeCode eventTypeCode = null;
									if(this.configAction.equals("config")){
										operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_ASOCIADO_ID29);
										operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_ASOCIADO_REGIMEN_FISCAL_ID30);
										operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_ASOCIADO_TIPO_PERSONA_ID31);
										operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_ASOCIADO_CERTIFICADO_ID32);
										operationCodeSource.add(OperationCode.REGISTRO_CUENTA_ID33);
										
										eventTypeCode = EventTypeCode.CONFIGURACION_CUENTA_ASOCIADA_ID13;
									}else if(this.configAction.equals("edit")){
										operationCodeSource.add(OperationCode.ACTUALIZACION_CONTRIBUYENTE_ASOCIADO_ID43);
										operationCodeSource.add(OperationCode.ACTUALIZACION_CONTRIBUYENTE_ASOCIADO_REGIMEN_FISCAL_ID34);
										operationCodeSource.add(OperationCode.ACTUALIZACION_CONTRIBUYENTE_ASOCIADO_TIPO_PERSONA_ID35);
										operationCodeSource.add(OperationCode.ACTUALIZACION_CONTRIBUYENTE_ASOCIADO_CERTIFICADO_ID36);
										operationCodeSource.add(OperationCode.ACTUALIZACION_CUENTA_ID44);
										
										eventTypeCode = EventTypeCode.ACTUALIZACION_CUENTA_ASOCIADA_ID14;
									}
									
									// Bitacora :: Registro de accion en bitacora
									appLog.logOperation(operationCodeSource);
									
									// Timeline :: Registro de accion
									timelineLog.timelineLog(eventTypeCode, taxpayerAssociated);
									
									String toastrMessage = "Información ".concat(this.configAction.equals("config") ? "registrada satisfactoriamente." : "actualizada satisfactoriamente."); 
									
									// Notificacion Toastr
									Toastr toastr = new Toastr(); 
									toastr.setTitle(UProperties.getMessage("mega.cfdi.premium.configuration.account.rfc", userTools.getLocale()));
									toastr.setMessage(toastrMessage);
									toastr.setType(Toastr.ToastrType.SUCCESS.value());
									
									// Mensaje de notificacion
									notification.setToastrNotification(Boolean.TRUE);
									notification.setToastr(toastr);
									
									// JSon response 
									response.setSuccess(Boolean.TRUE);
									response.setNotification(notification);
									
									// JSon informacion fiscal
									emitter.setIndex(UBase64.base64Encode(taxpayerAccount.getId().toString()));
									if(this.configAction.equals("config") || accountDeletedFinded || (this.configAction.equals("edit") && isCertificateEdit)){
										emitter.setCertifaicateCreationDate(UBase64.base64Encode(UDate.formattedDate(certificateCreationDate, DATE_FORMAT)));
										emitter.setCertificateNumber(UBase64.base64Encode(certificateNumber));
										emitter.setCertificateValidDate(UBase64.base64Encode(UDate.formattedDate(certificateExpirationDate, DATE_FORMAT)));
									}else if(this.configAction.equals("edit") && !isCertificateEdit){
										String certificateNoInfo = UProperties.getMessage("mega.cfdi.legend.no.information", userTools.getLocale());
										try {
											
											taxpayerCertificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayerAssociated);
											if(!UValidator.isNullOrEmpty(taxpayerCertificate)){
												cerPath = taxpayerCertificate.getRutaCertificado();
												privateKeyPath = taxpayerCertificate.getRutaLlavePrivada();
												
												certificate = new File(cerPath);
												certificateCreationDate = UCertificateX509.certificateCreationDate(certificate);
												certificateExpirationDate = UCertificateX509.certificateExpirationDate(certificate);
												certificateNumber = UCertificateX509.certificateSerialNumber(UFile.fileToByte(certificate));
												
												emitter.setCertifaicateCreationDate(UBase64.base64Encode(UDate.formattedDate(certificateCreationDate, DATE_FORMAT)));
												emitter.setCertificateNumber(UBase64.base64Encode(certificateNumber));
												emitter.setCertificateValidDate(UBase64.base64Encode(UDate.formattedDate(certificateExpirationDate, DATE_FORMAT)));
											}else{
												emitter.setCertifaicateCreationDate(UBase64.base64Encode(certificateNoInfo));
												emitter.setCertificateNumber(UBase64.base64Encode(certificateNoInfo));
												emitter.setCertificateValidDate(UBase64.base64Encode(certificateNoInfo));
											}
											
										} catch (Exception e) {
											emitter.setCertifaicateCreationDate(UBase64.base64Encode(certificateNoInfo));
											emitter.setCertificateNumber(UBase64.base64Encode(certificateNoInfo));
											emitter.setCertificateValidDate(UBase64.base64Encode(certificateNoInfo));
										}
									}
									
									emitter.setNameOrBusinessName(UBase64.base64Encode(nameOrBusinessName));
									emitter.setEmail(UBase64.base64Encode(email));
									emitter.setPersonType(UBase64.base64Encode(emitterTypeEntity.getValor()));
									emitter.setRfc(UBase64.base64Encode(rfc));
									emitter.setTaxRegime(UBase64.base64Encode(regimenFiscalEntityByCode.getValor()));
								}else{
									notification.setPageMessage(Boolean.TRUE);
									notification.setCssStyleClass(CssStyleClass.ERROR.value());
									notification.setMessage(this.unexpectedError);
									
									response.setError(Boolean.TRUE);
									response.setNotification(notification);
								}
							}else{
								notification.setPageMessage(Boolean.TRUE);
								notification.setCssStyleClass(CssStyleClass.ERROR.value());
								notification.setMessage(this.unexpectedError);
								
								response.setError(Boolean.TRUE);
								response.setNotification(notification);
							}
						}
						
					} catch (Exception e) {
						e.printStackTrace();
						
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(this.unexpectedError);
						
						response.setNotification(notification);
						response.setUnexpectedError(Boolean.TRUE);
					}
				}
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(this.unexpectedError);
				
				response.setError(Boolean.TRUE);
				response.setNotification(notification);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(this.unexpectedError);
			
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		emitter.setResponse(response);
		jsonInString = gson.toJson(emitter);
		return jsonInString;
	}
}