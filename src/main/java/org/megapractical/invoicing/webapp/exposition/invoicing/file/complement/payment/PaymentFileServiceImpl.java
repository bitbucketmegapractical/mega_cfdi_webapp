package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.PaymentService;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.StringTypeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.WPaymentRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentAmountValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentBankValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentCertificateValidation;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentChangeTypeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentCurrencyValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentDateValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentOriginalStringValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentPayerAccountValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentPaymWayValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentReceiverAccountValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentSealValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentSourceAccountRfcValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentStringTypeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment.PaymentTargetAccountRfcValidator;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentBaseTax;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentRelatedDocument;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentWithheld;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPaymentTotal;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment.WRelatedDocument;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPaymentTotal;
import org.megapractical.invoicing.webapp.wrapper.WTaxes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class PaymentFileServiceImpl implements PaymentFileService {
	
	@Autowired
	private PaymentDateValidator paymentDateValidator;
	
	@Autowired
	private PaymentPaymWayValidator paymentPaymWayValidator;
	
	@Autowired
	private PaymentCurrencyValidator paymentCurrencyValidator;
	
	@Autowired
	private PaymentChangeTypeValidator paymentChangeTypeValidator;
	
	@Autowired
	private PaymentAmountValidator paymentAmountValidator;
	
	@Autowired
	private PaymentSourceAccountRfcValidator paymentSourceAccountRfcValidator;
	
	@Autowired
	private PaymentBankValidator paymentBankValidator;
	
	@Autowired
	private PaymentPayerAccountValidator paymentPayerAccountValidator;
	
	@Autowired
	private PaymentTargetAccountRfcValidator paymentTargetAccountRfcValidator;
	
	@Autowired
	private PaymentReceiverAccountValidator paymentReceiverAccountValidator;
	
	@Autowired
	private PaymentStringTypeValidator paymentStringTypeValidator;
	
	@Autowired
	private PaymentCertificateValidation paymentCertificateValidation;
	
	@Autowired
	private PaymentOriginalStringValidator paymentOriginalStringValidator;
	
	@Autowired
	private PaymentSealValidator paymentSealValidator;
	
	@Autowired
	private PaymentService paymentService;
	
	@Autowired
	private RelatedDocumentFileService relatedDocumentFileService;
	
	@Override
	public void populatePayment(String paymentId, ComplementPayment cmpPayment, Map<String, WPayment> paymentsMap) {
		// Feha y hora del pago
		val paymentDate = getPaymentDate(cmpPayment);
		// Forma de pago
		val cmpPaymentWay = getPaymentWay(cmpPayment);
		// Moneda
		val cmpPaymentCurrency = getCurrency(cmpPayment);
		// Tipo cambio
		val cmpChangeType = getChangeType(cmpPayment, cmpPaymentCurrency);
		// Monto (Importe del pago)
		val amount = getAmount(cmpPayment, cmpPaymentCurrency);
		// Numero de operacion
		val operationNumber = cmpPayment.getOperationNumber();
		// RFC emisor cuenta origen
		val sourceAccountRfc = getSourceAccountRfc(cmpPayment, cmpPaymentWay);
		// Nombre de banco
		val bankName = getBankName(cmpPayment, cmpPaymentWay, sourceAccountRfc);
		// Cuenta ordenante
		val payerAccount = getPayerAccount(cmpPayment, cmpPaymentWay);
		// RFC emisor cuenta beneficiaria
		val targetAccountRfc = getTargetAccountRfc(cmpPayment, cmpPaymentWay);
		// Cuenta beneficiaria
		val receiverAccount = getReceiverAccount(cmpPayment, cmpPaymentWay);
		// Tipo cadena de pago
		val stringTypeResponse = getStringType(cmpPayment, cmpPaymentWay);
		val stringType = stringTypeResponse.getStringType();
		val stringTypeCode = stringTypeResponse.getStringTypeCode();
		// Certificado de pago
		val paymentCertificate = getCertificate(cmpPayment, stringType);
		// Cadena de pago
		val originalString = getOriginalString(cmpPayment, stringType);
		// Sello de pago
		val paymentSeal = getPaymentSeal(cmpPayment, stringType);
		
		val wPaymentRequest = WPaymentRequest
								.builder()
								.paymentDate(paymentDate)
								.cmpPaymentWay(cmpPaymentWay)
								.cmpPaymentCurrency(cmpPaymentCurrency)
								.cmpChangeType(cmpChangeType)
								.amount(amount)
								.operationNumber(operationNumber)
								.sourceAccountRfc(sourceAccountRfc)
								.bankName(bankName)
								.payerAccount(payerAccount)
								.targetAccountRfc(targetAccountRfc)
								.receiverAccount(receiverAccount)
								.stringType(stringType)
								.stringTypeCode(stringTypeCode)
								.paymentCertificate(paymentCertificate)
								.originalString(originalString)
								.paymentSeal(paymentSeal)
								.transferreds(cmpPayment.getTransferreds())
								.withhelds(cmpPayment.getWithhelds())
								.build();
		
		val payment = paymentService.populatePayment(wPaymentRequest);
		paymentsMap.put(paymentId, payment);
	}
	
	@Override
	public void populatePaymentTransferreds(String paymentId, ComplementPaymentBaseTax itax,
			Map<String, List<WTaxes>> paymentTransferreds) {
		var transferreds = paymentTransferreds.get(paymentId);
		if (transferreds == null) {
			transferreds = new ArrayList<>();
		}
		
		val transferred = getWTax(itax);
		transferreds.add(transferred);
		
		paymentTransferreds.put(paymentId, transferreds);
	}
	
	@Override
	public void populatePaymentWithhelds(String paymentId, ComplementPaymentWithheld itax,
			Map<String, List<WTaxes>> paymentWithhelds) {
		var withhelds = paymentWithhelds.get(paymentId);
		if (withhelds == null) {
			withhelds = new ArrayList<>();
		}
		
		val withheld = WTaxes
						.builder()
						.tax(itax.getTax())
						.amount(UValue.bigDecimalStrict(itax.getAmount()))
						.build();
		withhelds.add(withheld);
		
		paymentWithhelds.put(paymentId, withhelds);
	}
	
	@Override
	public void populateRelatedDocument(String paymentId, ComplementPaymentRelatedDocument data,
			Map<String, List<WRelatedDocument>> relatedDocuments) {
		var rdocs = relatedDocuments.get(paymentId);
		if (rdocs == null) {
			rdocs = new ArrayList<>();
		}
		
		val relatedDcoument = relatedDocumentFileService.populateRelatedDocument(data);
		rdocs.add(relatedDcoument);
		
		relatedDocuments.put(paymentId, rdocs);
	}
	
	@Override
	public void populateRelDocTransferreds(String docId, ComplementPaymentBaseTax itax,
			Map<String, List<WTaxes>> relDocTransferreds) {
		var transferreds = relDocTransferreds.get(docId);
		if (transferreds == null) {
			transferreds = new ArrayList<>();
		}
		
		val transferred = getWTax(itax);
		transferreds.add(transferred);
		
		relDocTransferreds.put(docId, transferreds);
	}

	@Override
	public void populateRelDocWithhelds(String docId, ComplementPaymentBaseTax itax,
			Map<String, List<WTaxes>> relDocWithhelds) {
		var withhelds = relDocWithhelds.get(docId);
		if (withhelds == null) {
			withhelds = new ArrayList<>();
		}
		
		val withheld = getWTax(itax);
		withhelds.add(withheld);
		
		relDocWithhelds.put(docId, withhelds);
	}
	
	@Override
	public WPaymentTotal paymentTotal(ComplementPaymentTotal paymentTotal) {
		if (paymentTotal != null) {
			val totalAmountPayments = paymentTotal.getTotalAmountPayments();
			if (!UValidator.isNullOrEmpty(totalAmountPayments)) {
				val totalWithheldIva = UValue.bigDecimalStrict(paymentTotal.getTotalWithheldIva());
				val totalWithheldIsr = UValue.bigDecimalStrict(paymentTotal.getTotalWithheldIsr());
				val totalWithheldIeps = UValue.bigDecimalStrict(paymentTotal.getTotalWithheldIeps());
				val totalTransferredBaseIva16 = UValue.bigDecimalStrict(paymentTotal.getTotalTransferredBaseIva16());
				val totalTransferredTaxIva16 = UValue.bigDecimalStrict(paymentTotal.getTotalTransferredTaxIva16());
				val totalTransferredBaseIva8 = UValue.bigDecimalStrict(paymentTotal.getTotalTransferredBaseIva8());
				val totalTransferredTaxIva8 = UValue.bigDecimalStrict(paymentTotal.getTotalTransferredTaxIva8());
				val totalTransferredBaseIva0 = UValue.bigDecimalStrict(paymentTotal.getTotalTransferredBaseIva0());
				val totalTransferredTaxIva0 = UValue.bigDecimalStrict(paymentTotal.getTotalTransferredTaxIva0());
				val totalTransferredBaseIvaExempt = UValue.bigDecimalStrict(paymentTotal.getTotalTransferredBaseIvaExempt());
				val totalAmountPay = UValue.bigDecimalStrict(totalAmountPayments);
				
				return WPaymentTotal
						.builder()
						.totalWithheldIva(totalWithheldIva)
						.totalWithheldIsr(totalWithheldIsr)
						.totalWithheldIeps(totalWithheldIeps)
						.totalTransferredBaseIva16(totalTransferredBaseIva16)
						.totalTransferredTaxIva16(totalTransferredTaxIva16)
						.totalTransferredBaseIva8(totalTransferredBaseIva8)
						.totalTransferredTaxIva8(totalTransferredTaxIva8)
						.totalTransferredBaseIva0(totalTransferredBaseIva0)
						.totalTransferredTaxIva0(totalTransferredTaxIva0)
						.totalTransferredBaseIvaExempt(totalTransferredBaseIvaExempt)
						.totalAmountPayments(totalAmountPay)
						.build();
			}
		}
		return null;
	}
	
	private String getPaymentDate(ComplementPayment payment) {
		return paymentDateValidator.validate(payment.getPaymentDate());
	}

	private FormaPagoEntity getPaymentWay(ComplementPayment payment) {
		return paymentPaymWayValidator.getPaymentWay(payment.getPaymentWay());
	}

	private MonedaEntity getCurrency(ComplementPayment payment) {
		return paymentCurrencyValidator.getCurrency(payment.getCurrency());
	}

	private BigDecimal getChangeType(ComplementPayment payment, MonedaEntity currency) {
		return paymentChangeTypeValidator.getChangeType(payment.getChangeType(), currency);
	}

	private BigDecimal getAmount(ComplementPayment payment, MonedaEntity currency) {
		return paymentAmountValidator.getAmount(payment.getAmount(), currency);
	}

	private String getSourceAccountRfc(ComplementPayment payment, FormaPagoEntity paymentWay) {
		return paymentSourceAccountRfcValidator.getSourceAccountRfc(payment.getSourceAccountRfc(), paymentWay);
	}

	private String getBankName(ComplementPayment payment, FormaPagoEntity paymentWay, String sourceAccountRfc) {
		return paymentBankValidator.getBankName(payment.getBank(), paymentWay, sourceAccountRfc);
	}

	private String getPayerAccount(ComplementPayment payment, FormaPagoEntity paymentWay) {
		return paymentPayerAccountValidator.getPayerAccount(payment.getPayerAccount(), paymentWay);
	}

	private String getTargetAccountRfc(ComplementPayment payment, FormaPagoEntity paymentWay) {
		return paymentTargetAccountRfcValidator.getTargetAccountRfc(payment.getTargetAccountRfc(), paymentWay);
	}

	private String getReceiverAccount(ComplementPayment payment, FormaPagoEntity paymentWay) {
		return paymentReceiverAccountValidator.getReceiverAccount(payment.getReceiverAccount(), paymentWay);
	}

	private StringTypeResponse getStringType(ComplementPayment payment, FormaPagoEntity paymentWay) {
		return paymentStringTypeValidator.getStringType(payment.getStringType(), paymentWay);
	}
	
	private String getCertificate(ComplementPayment payment, String stringType) {
		return paymentCertificateValidation.getCertificate(payment.getCertificate(), stringType);
	}
	
	private String getOriginalString(ComplementPayment payment, String stringType) {
		return paymentOriginalStringValidator.getOriginalString(payment.getOriginalString(), stringType);
	}
	
	private String getPaymentSeal(ComplementPayment payment, String stringType) {
		return paymentSealValidator.getPaymentSeal(payment.getSeal(), stringType);
	}
	
	private WTaxes getWTax(ComplementPaymentBaseTax itax) {
		return WTaxes
				.builder()
				.base(UValue.bigDecimalStrict(itax.getBase()))
				.tax(itax.getTax())
				.factorType(UCatalog.factorType(itax.getFactorType()))
				.rateOrFee(UValue.bigDecimalStrict(itax.getRateOrFee()))
				.amount(UValue.bigDecimalStrict(itax.getAmount()))
				.build();
	}

}