package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import org.megapractical.invoicing.webapp.json.JResponse;

public interface VoucherCurrencyVoucherTypeValidator {
	
	JResponse validateCurrencyVoucherType(String currency, String voucherType);
	
}