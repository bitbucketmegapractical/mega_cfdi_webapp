package org.megapractical.invoicing.webapp.exposition.invoicing.validator.emitter;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterCertificateRequest;
import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterResponse;
import org.megapractical.invoicing.webapp.json.JError;

public interface EmitterValidator {
	
	JEmitterResponse getEmitter(String emitterData, JEmitterCertificateRequest emitterCertificateRequest, List<JError> errors);
	
}