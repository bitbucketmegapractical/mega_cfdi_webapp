package org.megapractical.invoicing.webapp.exposition.configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.json.JEmitterLogo;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.notification.Toastr.ToastrType;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

@Controller
@Scope("session")
public class EmitterLogoController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	SessionController sessionController;
	
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	@ModelAttribute("processingRequest")
	public String processingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@ModelAttribute("updatingRequest")
	public String updatingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.updating", userTools.getLocale());
	}
	
	// Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() throws IOException{
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	private ContribuyenteEntity taxpayer;
	
	@RequestMapping(value = "/emitterLogo", method = RequestMethod.GET)
	public String emitterLogo(Model model) {
		try {
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			this.taxpayer = userTools.getContribuyenteActive(rfcActive);
						
			HtmlHelper.functionalitySelected("EmitterConfig", "emitter-config-logo");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			return "/config/EmitterLogo";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/emitterLogo", method = RequestMethod.POST)
	public @ResponseBody String emitterLogo(HttpServletRequest request) throws IOException {
		JNotification notification = new JNotification();
		JEmitterLogo emitterLogo = new JEmitterLogo();
		JResponse response = new JResponse();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String externalLocation = "/external-resources/LOGO/";
			
			if(!UValidator.isNullOrEmpty(this.taxpayer)){
				emitterLogo.setLogoDefined(!UValidator.isNullOrEmpty(this.taxpayer.getLogo()));
				emitterLogo.setLogoUrl(externalLocation + this.taxpayer.getLogo());
				
				response.setSuccess(Boolean.TRUE);
			}else{
				notification.setMessage(UProperties.getMessage("ERR0172", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			notification.setMessage(UProperties.getMessage("ERR0172", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		emitterLogo.setResponse(response);
		jsonInString = gson.toJson(emitterLogo);
		return jsonInString;
	}
	
	@Transactional
	@RequestMapping(value = "/emitterLogo", params={"uploadFile"})
    public @ResponseBody String uploadFile(Model model, HttpServletRequest request, @RequestParam(value="logoFile") MultipartFile multipartPayrollFile) throws IOException{
		JNotification notification = new JNotification();
		JEmitterLogo emitterLogo = new JEmitterLogo();
		JResponse response = new JResponse();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		Boolean errorMarked = false;
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			//##### Logo
			File logoFileUpload = null;
			
			String originalFilename = null;
			String logoFileName = null;
			String logoFileExt = null;
			
			if (multipartPayrollFile.isEmpty()) {
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				error.setField("logo-file");
				errorMarked = true;
			}else{
				originalFilename = multipartPayrollFile.getOriginalFilename();
				logoFileExt = UFile.getExtension(originalFilename);
				
				if(UValidator.isNullOrEmpty(logoFileExt) || (!logoFileExt.equalsIgnoreCase("png") && !logoFileExt.equalsIgnoreCase("jpg"))){
					error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
					error.setField("logo-file");
					errorMarked = true;
				}else{
					logoFileName = UValue.uuid().toUpperCase().replaceAll("-", "") + "." + logoFileExt;
					logoFileUpload = UFile.multipartFileToFile(multipartPayrollFile);
					
					//##### Ruta del logo
					String logoPath = appSettings.getPropertyValue("cfdi.logo.path");
					
					String externalLocation = "/external-resources/LOGO/";
					
					//##### Directorio para el logo
					String folderPath = this.taxpayer.getRfcActivo();
					
					//##### Verificar si el usuario tiene logo y eliminarlo
					if(!UValidator.isNullOrEmpty(this.taxpayer.getLogo())){
						String logoToDelete = logoPath + File.separator + this.taxpayer.getLogo();
						try {
							
							File fileToDelete = new File(logoToDelete);
							fileToDelete.delete();
							
						} catch (Exception e) {}
					}
					
					//##### Directorio para la db
					String logoDbPath = folderPath + File.separator + logoFileName;
					
					String folderAbsolutePath = logoPath + File.separator + folderPath;
					File logoAbsolutePath = new File(folderAbsolutePath); 
					if(!logoAbsolutePath.exists()){
						logoAbsolutePath.mkdirs();
					}
					
					//##### Subiendo el archivo al servidor
					String fileUploadPath = folderAbsolutePath + File.separator + logoFileName;
					UFile.writeFile(UFile.fileToByte(logoFileUpload), fileUploadPath);
					
					this.taxpayer.setLogo(logoDbPath);
					persistenceDAO.update(this.taxpayer);
					
					emitterLogo.setLogoDefined(!UValidator.isNullOrEmpty(this.taxpayer.getLogo()));
					emitterLogo.setLogoUrl(externalLocation + this.taxpayer.getLogo());
					
					// Bitacora :: Registro de accion en bitacora
					appLog.logOperation(OperationCode.CARGA_LOGO_ID73);
					
					// Timeline :: Registro de accion
					timelineLog.timelineLog(EventTypeCode.CARGA_LOGO_ID41);
					
					// Notificacion Toastr
					Toastr toastr = new Toastr();
					toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.emitter.logo.upload.title", userTools.getLocale()));
					toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.emitter.logo.upload.message", userTools.getLocale()));
					toastr.setType(ToastrType.SUCCESS.value());
					
					notification.setToastrNotification(Boolean.TRUE);
					notification.setToastr(toastr);
					
					response.setSuccess(Boolean.TRUE);
					response.setNotification(notification);
				}
			}
			if(errorMarked){
				errors.add(error);
				response.setError(Boolean.TRUE);
				response.setErrors(errors);
			}
			
		} catch (Exception e) {
			notification.setMessage(UProperties.getMessage("ERR0162", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
			response.setErrors(errors);
		}
		
		sessionController.sessionUpdate();
		
		emitterLogo.setResponse(response);
		jsonInString = gson.toJson(emitterLogo);
		return jsonInString;
	}
	
	@RequestMapping(value = "/emitterLogo", params={"logoDelete"})
	public @ResponseBody String logoDelete(HttpServletRequest request) throws IOException {
		JNotification notification = new JNotification();
		JEmitterLogo emitterLogo = new JEmitterLogo();
		JResponse response = new JResponse();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			if(!UValidator.isNullOrEmpty(this.taxpayer)){
				if(!UValidator.isNullOrEmpty(this.taxpayer.getLogo())){
					//##### Ruta del logo
					String logoPath = appSettings.getPropertyValue("cfdi.logo.path");
					
					//##### Eliminando logo fisicamente
					String logoToDelete = logoPath + File.separator + this.taxpayer.getLogo();
					try {
						
						File fileToDelete = new File(logoToDelete);
						fileToDelete.delete();
						
					} catch (Exception e) {}
					
					this.taxpayer.setLogo(null);
					persistenceDAO.update(this.taxpayer);
					
					// Bitacora :: Registro de accion en bitacora
					appLog.logOperation(OperationCode.ELIMINAR_LOGO_ID74);
					
					// Timeline :: Registro de accion
					timelineLog.timelineLog(EventTypeCode.ELIMINAR_LOGO_ID42);
					
					response.setSuccess(Boolean.TRUE);
				}
			}else{
				notification.setMessage(UProperties.getMessage("ERR0171", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0171", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		emitterLogo.setResponse(response);
		jsonInString = gson.toJson(emitterLogo);
		return jsonInString;
	}
}
