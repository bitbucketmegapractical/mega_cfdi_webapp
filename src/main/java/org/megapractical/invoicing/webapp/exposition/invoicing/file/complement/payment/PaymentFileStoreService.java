package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;

public interface PaymentFileStoreService {
	
	boolean store(ContribuyenteEntity taxpayer, ApiCfdi apiCfdi);
	
}