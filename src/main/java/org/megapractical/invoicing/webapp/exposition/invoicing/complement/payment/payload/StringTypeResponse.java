package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StringTypeResponse {
	private String stringType;
	private String stringTypeCode;
}