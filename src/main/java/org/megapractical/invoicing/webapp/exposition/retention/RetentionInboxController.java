package org.megapractical.invoicing.webapp.exposition.retention;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionEntity;
import org.megapractical.invoicing.dal.data.repository.IRetentionJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JRetention;
import org.megapractical.invoicing.webapp.json.JRetention.Retention;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionNotification;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.notification.Toastr.ToastrType;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WCfdiNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import lombok.val;

@Controller
@SessionScope
@RequestMapping("/retention")
public class RetentionInboxController {

	@Autowired
	AppSettings appSettings;
	
	@Autowired
	private AppLog appLog;
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private SessionController sessionController;
	
	@Autowired
	private MailService mailService;
	
	@Resource
	private IRetentionJpaRepository iRetentionJpaRepository;
	
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	private static final String SORT_PROPERTY = "id";
	private static final Integer INITIAL_PAGE = 0;
	
	//##### Contribuyente
	private ContribuyenteEntity taxpayer;
	
	//##### Mensaje procesando...
	@ModelAttribute("processingRequest")
	public String processingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	//##### Mensaje cargando...
	@ModelAttribute("loadingRequest")
	public String loadingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@GetMapping(params = "inbox")
	public String retentionList(final Model model) {
		try {
			
			model.addAttribute("page", INITIAL_PAGE);
			
			HtmlHelper.functionalitySelected("Retention", "retention-inbox");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			//##### Cargando el contribuyente por el rfc activo
			this.taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
			return "/retention/RetentionList";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}

	@PostMapping(params = "inbox")
	public @ResponseBody String retentionList(final HttpServletRequest request) {
		
		JRetention retentionJson = new JRetention();
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String determineQuery = null;
			
			Integer paginateSize = appSettings.getPaginationSize();
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchValue = UBase64.base64Decode(request.getParameter("searchValue"));
			if(UValidator.isNullOrEmpty(searchValue)) {
				searchValue = "";
			}
			
			String searchDateStart = UBase64.base64Decode(request.getParameter("searchDateStart"));
			Date startDate = null;
			
			String searchDateEnd = UBase64.base64Decode(request.getParameter("searchDateEnd"));
			Date endDate = null;
			
			Boolean error = false;
			if(!UValidator.isNullOrEmpty(searchDateStart) && !UValidator.isNullOrEmpty(searchDateEnd)) {
				startDate = searchDateStart != null ? UDate.formattedDate(searchDateStart, DATE_FORMAT) : null;
				endDate = searchDateEnd != null ? UDate.formattedDate(searchDateEnd, DATE_FORMAT) : null;
				if(UValidator.isNullOrEmpty(startDate) || UValidator.isNullOrEmpty(endDate)) {
					error = true;
				} else {
					if(UDateTime.isAfterLocalDate(startDate, endDate)) {
						error = true;
					} else {
						determineQuery = determineQuery != null ?  determineQuery.concat("_between") : "between";
					}
				}
			} else if(!UValidator.isNullOrEmpty(searchDateStart)) {
				startDate = searchDateStart != null ? UDate.formattedDate(searchDateStart, DATE_FORMAT) : null;
				if(UValidator.isNullOrEmpty(startDate)) {
					error = true;
				} else {
					determineQuery = determineQuery != null ?  determineQuery.concat("_after") : "after";
				}
			} else if(!UValidator.isNullOrEmpty(searchDateEnd)) {
				endDate = searchDateEnd != null ? UDate.formattedDate(searchDateEnd, DATE_FORMAT) : null;
				if(UValidator.isNullOrEmpty(endDate)) {
					error = true;
				} else {
					determineQuery = determineQuery != null ?  determineQuery.concat("_before") : "before";
				}
			}
			determineQuery = determineQuery == null ? "default" : determineQuery;
			
			String paginate = UBase64.base64Decode(request.getParameter("paginateSize"));
			if(!UValidator.isNullOrEmpty(paginate)) {
				paginateSize = UValue.integerValue(paginate);
			}
			
			if(error) {
				notification.setMessage(UProperties.getMessage("ERR0056", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			} else {
				Sort sort = Sort.by(Sort.Order.desc(SORT_PROPERTY));
				PageRequest pageRequest = PageRequest.of(page, paginateSize, sort);
				
				Page<RetencionEntity> retentionPage = null;
				
				switch (determineQuery) {
					case "between":
						retentionPage = iRetentionJpaRepository.findBetween(this.taxpayer, searchValue, startDate, endDate, pageRequest);
						break;
					case "after":
						retentionPage = iRetentionJpaRepository.findAfter(this.taxpayer, searchValue, startDate, pageRequest);
						break;
					case "before":
						retentionPage = iRetentionJpaRepository.findBefore(this.taxpayer, searchValue, endDate, pageRequest);
						break;
					default:
						retentionPage = iRetentionJpaRepository.findAll(this.taxpayer, searchValue, pageRequest);
						break;
				}
				
				val pagination = JPagination.buildPaginaton(retentionPage);
				retentionJson.setPagination(pagination);
				
				retentionPage.forEach(item -> {
					Retention objRet = new Retention();
					objRet.setUuid(UBase64.base64Encode(item.getUuid()));
					objRet.setReceiverRfc(UBase64.base64Encode(item.getReceptorRfc()));
					objRet.setReceiverCurp(UBase64.base64Encode(item.getReceptorCurp()));
					objRet.setReceiverBusinessName(UBase64.base64Encode(item.getReceptorRazonSocial()));
					objRet.setReceiverRegNumber(UBase64.base64Encode(item.getReceptorNumeroIdentificacion()));
					objRet.setFolio(UBase64.base64Encode(item.getFolio()));
					objRet.setRetetionKey(UBase64.base64Encode(item.getClaveRetencion()));
					objRet.setPeriodInitialMonth(UBase64.base64Encode(String.valueOf(item.getPeriodoMesInicial())));
					objRet.setPeriodEndMonth(UBase64.base64Encode(String.valueOf(item.getPeriodoMesFinal())));
					objRet.setPeriodYear(UBase64.base64Encode(String.valueOf(item.getPeriodoEjercicio())));
					objRet.setExpeditionDate(UBase64.base64Encode(UDate.formattedDate(item.getFechaExpedicion(), DATE_FORMAT)));
					retentionJson.getRetentions().add(objRet);
				});
				
				String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
				if(!UValidator.isNullOrEmpty(searchValue) || !UValidator.isNullOrEmpty(startDate) || !UValidator.isNullOrEmpty(endDate)) {
					notification.setSearch(Boolean.TRUE);
					searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
				}
				
				if(retentionJson.getRetentions().isEmpty()) {
					notification.setSearchResultMessage(searchResultEmpty);
					notification.setPanelMessage(Boolean.TRUE);
					notification.setDismiss(Boolean.FALSE);
					notification.setCssStyleClass(CssStyleClass.INFO.value());
				}
				
				response.setSuccess(Boolean.TRUE);
				response.setNotification(notification);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		retentionJson.setResponse(response);
		jsonInString = gson.toJson(retentionJson);
		return jsonInString;
	}
	
	@GetMapping("/download/{uuid}/{fileType}")
	@ResponseBody
	public ModelAndView retentionDownload(@PathVariable(value = "uuid") String uuid,
			@PathVariable(value = "fileType") String fileType, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			
			String repositoryPath = appSettings.getPropertyValue("cfdi.retention.path");
			RetencionEntity retentionEntity = iRetentionJpaRepository.findByUuidAndCanceladoFalse(uuid);
			
			if(retentionEntity != null){
				String absolutePath = repositoryPath + File.separator + (fileType.equalsIgnoreCase("xml") ? retentionEntity.getRutaXml() : retentionEntity.getRutaPdf());
				
				File file = new File(absolutePath);
				byte[] fileByte = UFile.fileToByte(file);
				
				String contentType = "application/xml";
				if(fileType.equalsIgnoreCase("pdf")) {
					contentType = "application/pdf";
				}
				
				response.setContentType(contentType);
		        response.setContentLength(fileByte.length);
		        response.setHeader("Content-Disposition","attachment; filename=\"" + file.getName() +"\"");
		 
		        FileCopyUtils.copy(fileByte, response.getOutputStream());
			}
	 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		return null;
    }
	
	@PostMapping(params = "mail")
	@ResponseBody
	public String retentionMailSend(final HttpServletRequest request) throws IOException {
		
		JRetention retentionJson = new JRetention();
		
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		Boolean sendedError = false; 
		
		try {
			
			String retentionNotificationRequest = request.getParameter("retentionNotificationRequest");
			retentionJson = gson.fromJson(retentionNotificationRequest, JRetention.class);
			
			if(!UValidator.isNullOrEmpty(retentionJson)) {
				WCfdiNotification wCfdiNotification = new WCfdiNotification();
				List<String> recipients = new ArrayList<>();
				
				RetentionNotification retentionNotification = retentionJson.getRetentionNotification();
				
				String uuid = UBase64.base64Decode(retentionNotification.getUuid());
				wCfdiNotification.setUuid(uuid);
				
				String recipientTo = UBase64.base64Decode(retentionNotification.getRecipientTo());
				recipients.add(recipientTo);
				
				if(!UValidator.isNullOrEmpty(retentionNotification.getRecipients())) {
					for (String item : retentionNotification.getRecipients()) {
						String email = UBase64.base64Decode(item);
						if(!email.equalsIgnoreCase(recipientTo)) {
							recipients.add(email);
						}
					}
				}
				wCfdiNotification.setRecipients(recipients);
				
				String repositoryPath = appSettings.getPropertyValue("cfdi.retention.path");
				
				RetencionEntity retentionEntity = iRetentionJpaRepository.findByUuidAndCanceladoFalse(uuid);
				if(!UValidator.isNullOrEmpty(retentionEntity.getRutaXml())) {
					String xmlPath = repositoryPath + File.separator + retentionEntity.getRutaXml();
					File xmlFile = new File(xmlPath);
					InputStream xmlInputStream = new FileInputStream(xmlFile);
					wCfdiNotification.setXmlInputStream(xmlInputStream);
					//IOUtils.closeQuietly(xmlInputStream);
				}
					
				if(!UValidator.isNullOrEmpty(retentionEntity.getRutaPdf())) {
					String pdfPath = repositoryPath + File.separator + retentionEntity.getRutaPdf();
					File pdfFile = new File(pdfPath);
					InputStream pdfInputStream = new FileInputStream(pdfFile);
					wCfdiNotification.setPdfInputStream(pdfInputStream);
					//IOUtils.closeQuietly(pdfInputStream);
				}
				
				wCfdiNotification.setEmitterRfc(retentionEntity.getContribuyente().getRfc());
				wCfdiNotification.setEmitterName(retentionEntity.getContribuyente().getNombreRazonSocial());
				wCfdiNotification.setReceiverName(retentionEntity.getReceptorRazonSocial());
				String rfc = !UValidator.isNullOrEmpty(retentionEntity.getReceptorRfc()) ? retentionEntity.getReceptorRfc() : retentionEntity.getReceptorNumeroIdentificacion();
				wCfdiNotification.setReceiverRfc(rfc);
				
				if(mailService.sendRetentionToRecipients(wCfdiNotification)) {
					//##### Bitacora :: Registro de accion en bitácora
					OperationCode operationCode = OperationCode.ENVIO_CORREO_ELECTRONICO_ID82;
					appLog.logOperation(operationCode, uuid);
					
					//##### Notificacion Toastr
					Toastr toastr = new Toastr();
					toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.email.send.title", userTools.getLocale()));
					toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.email.send.message", userTools.getLocale()));
					toastr.setType(ToastrType.SUCCESS.value());
					
					notification.setToastrNotification(Boolean.TRUE);
					notification.setToastr(toastr);
					
					response.setSuccess(Boolean.TRUE);
					response.setNotification(notification);
				} else {
					sendedError = true;
				}
				
			} else {
				sendedError = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			sendedError = true;
		}
		
		if(sendedError) {
			notification.setMessage(UProperties.getMessage("ERR0183", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		retentionJson.setResponse(response);
		jsonInString = gson.toJson(retentionJson);
		return jsonInString;
	}
	
}