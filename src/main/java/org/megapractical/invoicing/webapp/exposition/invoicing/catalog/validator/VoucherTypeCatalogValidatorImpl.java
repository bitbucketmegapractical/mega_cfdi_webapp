package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.megapractical.invoicing.dal.data.repository.IVoucherTypeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.VoucherTypeValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherTypeCatalogValidatorImpl implements VoucherTypeCatalogValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private IVoucherTypeJpaRepository iVoucherTypeJpaRepository;
	
	@Override
	public VoucherTypeValidatorResponse validate(String voucherType) {
		TipoComprobanteEntity voucherTypeEntity = null;
		var hasError = false;
		if (UValidator.isNullOrEmpty(voucherType)) {
			hasError = true;
		} else {
			voucherTypeEntity = iVoucherTypeJpaRepository.findByCodigoAndEliminadoFalse(voucherType);
			if (voucherTypeEntity == null) {
				hasError = true;
			}
		}
		
		return new VoucherTypeValidatorResponse(voucherTypeEntity, hasError);
	}
	
	@Override
	public VoucherTypeValidatorResponse validate(String voucherType, String field) {
		TipoComprobanteEntity voucherTypeEntity = null;
		JError error = null;
		
		if (UValidator.isNullOrEmpty(voucherType)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			// Validar que sea un valor de catalogo
			val voucherTypeCatalog = UCatalog.getCatalog(voucherType);
			voucherType = voucherTypeCatalog[0];
			val voucherTypeValue = voucherTypeCatalog[1];
			
			voucherTypeEntity = iVoucherTypeJpaRepository.findByCodigoAndEliminadoFalse(voucherType);
			val voucherTypeEntityByValue = iVoucherTypeJpaRepository.findByValorAndEliminadoFalse(voucherTypeValue);
			if (voucherTypeEntity == null || voucherTypeEntityByValue == null) {
				val values = new String[]{"CFDI33120", "TipoDeComprobante", "c_TipoDeComprobante"};
				error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
			}
		}
		
		return new VoucherTypeValidatorResponse(voucherTypeEntity, error);
	}

}