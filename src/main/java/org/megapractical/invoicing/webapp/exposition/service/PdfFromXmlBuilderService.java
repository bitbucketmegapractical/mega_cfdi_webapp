package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.wrapper.ApiCfdiVoucher;
import org.megapractical.invoicing.api.wrapper.ApiPayrollVoucher;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface PdfFromXmlBuilderService {
	
	ApiCfdiVoucher buildCfdi(String xmlPath);
	
	ApiPayrollVoucher buildPayroll(String xmlPath);
	
}