package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import org.megapractical.invoicing.api.wrapper.ApiConcept;

public interface PaymentConceptService {
	
	ApiConcept paymentConcept();
	
}