package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.wrapper.ApiPaymentStamp;

public interface PaymentProcessingService {
	
	void paymentProcessing(ApiPaymentStamp paymentStamp);
	
}