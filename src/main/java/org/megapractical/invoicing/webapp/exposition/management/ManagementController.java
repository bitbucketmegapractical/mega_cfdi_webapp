package org.megapractical.invoicing.webapp.exposition.management;

import java.io.IOException;

import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.support.Layout;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Scope("session")
public class ManagementController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	UserTools userTools;
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@Layout(value = "layouts/admon")
	@RequestMapping(value = "/managementDashboard")
	public String adminAccess(){
		HtmlHelper.functionalitySelected("MngGeneral", null);
		appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
		return "management/Dashboard";
	}
	
	@Layout(value = "layouts/admon")
	@RequestMapping("/management")
	public String home(Model model) {
		return "management/Dashboard";
	}
}