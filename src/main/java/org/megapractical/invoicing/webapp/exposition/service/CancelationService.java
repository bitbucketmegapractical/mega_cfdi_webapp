package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.client.CancelationClient;
import org.megapractical.invoicing.api.client.CancelationProperties;
//import org.megapractical.invoicing.api.client.CancelationResponse;
//import org.megapractical.invoicing.api.client.CancelationResponse.CancelationResult;
//import org.megapractical.invoicing.api.client.CancelationResponse.CancelationResult.StatusByUuid;
import org.megapractical.invoicing.api.smarterweb.SWCancellationClient;
import org.megapractical.invoicing.api.util.UCancelation;
import org.megapractical.invoicing.api.util.UNetwork;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.CfdiCancelacionLogEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiCancelacionLogErrorEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiSolicitudCancelacionEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Scope("session")
public class CancelationService {

	@Autowired
	AppSettings appSettings;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Resource
	ICfdiJpaRepository iCfdiJpaRepository;
	
	@Async
	public Future<Boolean> cancelationAsync(CancelationProperties properties){
		try {
			
			cancelation(properties);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new AsyncResult<Boolean>(Boolean.TRUE);
	}
	
	@Async
	public Future<Boolean> cancelationAsync(List<CancelationProperties> properties){
		try {
			
			cancelation(properties);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new AsyncResult<Boolean>(Boolean.TRUE);
	}
	
	@Transactional
	private void cancelation(List<CancelationProperties> properties){
		try {
			
			for (CancelationProperties item : properties) {
				cancelation(item);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	private void cancelation(CancelationProperties properties){
		/*try {
			
			CancelationResponse response = new CancelationResponse();
			try {
				
				if(UNetwork.isAvailable()){
					try {
						
						if(properties.getServices().equals("mega")) {
							new CancelationClient();
							response = CancelationClient.cancelationRequest(properties);
						} else if(properties.getServices().equals("sw")) {
							new SWCancellationClient();
							response = SWCancellationClient.cancelationRequest(properties);
						}
						
						if(!UValidator.isNullOrEmpty(response.getCancelationResult())) {
							String repositoryPath = appSettings.getPropertyValue("cfdi.repository");
							
							CancelationResult cancelationResult = response.getCancelationResult();
							
							List<StatusByUuid> statusByUuids = cancelationResult.getStatusByUuids();
							if(!statusByUuids.isEmpty()) {
								for (StatusByUuid status : statusByUuids) {
									String uuid = status.getUuid();
									CfdiEntity cfdiEntity = iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoTrueAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull(uuid);
									if(status.getStatus().equals("201") || status.getStatus().equals("202")) {
										if(status.getResponseStatus().equalsIgnoreCase("Cancelado")) {
											if(cfdiEntity != null){
												String acuseXml = status.getResponseAcuseXml();
												
												String pdfOriginalPath = cfdiEntity.getRutaPdf();
												String absolutePath = repositoryPath + File.separator + pdfOriginalPath;
												
												String[] splitted = absolutePath.split(Pattern.quote(".")); 
												String pdfCanceled = splitted[0].concat("_CANCELADO.").concat(splitted[1]);
												OutputStream outputStream = new FileOutputStream(pdfCanceled);
												
												// Marca de cancelado al PDF
												UCancelation.estampaPDF(absolutePath, outputStream);
												
												splitted = pdfOriginalPath.split(Pattern.quote("."));
												pdfCanceled = splitted[0].concat("_CANCELADO.").concat(splitted[1]);
												
												cfdiEntity.setRutaPdf(pdfCanceled);
												cfdiEntity.setAcuseCancelacion(acuseXml);
												cfdiEntity.setCancelado(Boolean.TRUE);
												cfdiEntity.setFechaCancelado(new Date());
												cfdiEntity.setHoraCancelado(new Date());
												cfdiEntity.setCancelacionEnProceso(Boolean.FALSE);													
												persistenceDAO.update(cfdiEntity);
											}
										} else {
											cfdiEntity.setCancelacionEnProceso(Boolean.FALSE);
											cfdiEntity.setSolicitudCancelacion(Boolean.TRUE);
											persistenceDAO.update(cfdiEntity);
											
											CfdiSolicitudCancelacionEntity cancelRequest = new CfdiSolicitudCancelacionEntity();
											cancelRequest.setUuid(uuid);
											cancelRequest.setCodigoEstatus(status.getResponseStatusCode());
											cancelRequest.setEsCancelable(status.getResponseCancelableText());
											cancelRequest.setEstado(status.getResponseStatus());
											cancelRequest.setEstatusCancelacion(status.getResponseCancelableStatus());
											cancelRequest.setFechaSolicitud(new Date());
											cancelRequest.setHoraSolicitud(new Date());
											cancelRequest.setSolicitudActiva(Boolean.TRUE);
											persistenceDAO.persist(cancelRequest);
										}		
										
										CfdiCancelacionLogEntity log = new CfdiCancelacionLogEntity();
										log.setUuid(uuid);
										log.setCodigoEstatus(status.getResponseStatusCode());
										log.setEsCancelable(status.getResponseCancelableText());
										log.setEstado(status.getResponseStatus());
										log.setEstatusCancelacion(status.getResponseCancelableStatus());
										log.setFecha(new Date());
										log.setHora(new Date());
										log.setTareaProgramada(Boolean.FALSE);
										persistenceDAO.persist(log);
									} else {
										String faultMessage = null; 
										switch (status.getStatus()) {
										case "203":
											faultMessage = "CFDI203";
											break;
										case "205":
											faultMessage = "CFDI205";
											break;
										case "301":
											faultMessage = "CFDI301";
											break;
										case "302":
											faultMessage = "CFDI302";
											break;
										case "303":
											faultMessage = "CFDI303";
											break;
										case "304":
											faultMessage = "CFDI304";
											break;
										default:
											faultMessage = "ERR0058";
											break;
										}
										
										faultMessage += ": " + UProperties.getMessage(faultMessage, "es_MX");
										
										//##### Registrando log
										CfdiCancelacionLogErrorEntity log = new CfdiCancelacionLogErrorEntity();
										log.setUuid(uuid);
										log.setTrazaError(faultMessage);
										log.setFecha(new Date());
										log.setHora(new Date());
										log.setTareaProgramada(Boolean.FALSE);
										persistenceDAO.persist(log);
										
										cfdiEntity.setCancelacionEnProceso(Boolean.FALSE);													
										persistenceDAO.update(cfdiEntity);
									}
								}
							}
						}else {
							
							List<String> errorUuids = response.getUuidsError();
							if(!errorUuids.isEmpty()) {
								for (String uuid : errorUuids) {
									CfdiEntity cfdi = iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoTrueAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull(uuid);
									if(!UValidator.isNullOrEmpty(cfdi)) {
										cfdi.setCancelacionEnProceso(Boolean.FALSE);													
										persistenceDAO.update(cfdi);
									}
								}
							}
							
							//##### Registrando log
							CfdiCancelacionLogErrorEntity log = new CfdiCancelacionLogErrorEntity();
							log.setUuid(response.getUuidError());
							log.setWsdlUrl(response.getWsdlUrl());
							log.setWsdlEndpoint(response.getWsdlEndpoint());
							log.setWsdlOperacion(response.getWsdlOperation());
							log.setTrazaError(response.getWsdlErrorTrace());
							log.setFecha(new Date());
							log.setHora(new Date());
							log.setTareaProgramada(Boolean.FALSE);
							persistenceDAO.persist(log);
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					CfdiCancelacionLogErrorEntity log = new CfdiCancelacionLogErrorEntity();
					log.setTrazaError("Error de red. No hay conexión a internet");
					log.setFecha(new Date());
					log.setHora(new Date());
					log.setTareaProgramada(Boolean.FALSE);
					persistenceDAO.persist(log);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}	
}