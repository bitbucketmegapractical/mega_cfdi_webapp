package org.megapractical.invoicing.webapp.exposition.retention.helper;

import java.util.List;
import java.util.Map;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.api.wrapper.ApiRetention.ApiRetImpRetenido;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.IRetentionYearRangeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.retention.payload.RetentionDataResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRetention.RetInfo;
import org.megapractical.invoicing.webapp.json.JRetention.RetReceiver;
import org.megapractical.invoicing.webapp.json.JRetention.RetTotal;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import lombok.val;
import lombok.var;

@Component
public class RetentionHelper {
	
	private final UserTools userTools;
	private final ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;
	private final IRetentionYearRangeJpaRepository iRetentionYearRangeJpaRepository;
	
	public static final String RFC_EXPRESSION = "[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?";
	public static final String CURP_EXPRESSION = "[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]";
	public static final Integer RFC_MIN_LENGTH = 12;
	public static final Integer RFC_MAX_LENGTH = 13;
	
	private static final String ERR1001 = "ERR1001";
	private static final String ERR1004 = "ERR1004";
	
	public RetentionHelper(UserTools userTools, ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository,
			IRetentionYearRangeJpaRepository iRetentionYearRangeJpaRepository) {
		this.userTools = userTools;
		this.iTaxpayerTaxRegimeJpaRepository = iTaxpayerTaxRegimeJpaRepository;
		this.iRetentionYearRangeJpaRepository = iRetentionYearRangeJpaRepository;
	}

	public void populateEmitterData(ContribuyenteEntity taxpayer, String certificateNumber, ApiRetention apiRetention) {
		apiRetention.setEmitterRfc(taxpayer.getRfcActivo());
		apiRetention.setEmitterBusinessName(taxpayer.getNombreRazonSocial());
		apiRetention.setCertificateEmitterNumber(certificateNumber);
		
		val taxRegime = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(taxpayer);
		if (taxRegime != null) {
			apiRetention.setEmitterTaxRegime(taxRegime.getRegimenFiscal().getCodigo());
		}
	}
	
	public boolean populateReceiverData(RetentionData retentionData, final ApiRetention apiRetention, final List<JError> errors) {
		val retReceiver = retentionData.getRetReceiver();
		if (!UValidator.isNullOrEmpty(retReceiver)) {
			// Nacialidad
			val receiverNationality = UBase64.base64Decode(retReceiver.getReceiverNationality());
			val errorMarked = populateReceiverNationality(receiverNationality, apiRetention, errors);
			if (!errorMarked) {
				if (receiverNationality.equalsIgnoreCase("nacional")) {
					populateNationalReceiver(retReceiver, apiRetention, errors);
				} else if (receiverNationality.equalsIgnoreCase("extranjero")) {
					populateForeignReceiver(retReceiver, apiRetention, errors);
				}
			}
			return false;
		} else {
			return true;
		}
	}
	
	public RetentionDataResponse populateRetention(RetentionData retentionData, final ApiRetention apiRetention, final List<JError> errors) {
		String retentionKey = null;
		var sectionError = false;
		
		val retInfo = retentionData.getRetInfo();
		if (!UValidator.isNullOrEmpty(retInfo)) {
			// Clave retencion
			retentionKey = populateRetentionKey(retInfo, apiRetention, errors);
			// Lugar de expedicion
			populateExpeditionPlace(retInfo, apiRetention, errors);
			// Mes inicial
			populateInitialMonth(retInfo, apiRetention, errors);
			// Mes final
			populateEndMonth(retInfo, apiRetention, errors);
			// Ejercicio fiscal
			populateYear(retInfo, apiRetention, errors);
			// Folio
			val folio = UBase64.base64Decode(retInfo.getFolio());
			apiRetention.setFolio(folio);
			// Descripcion
			val description = UBase64.base64Decode(retInfo.getDescription());
			apiRetention.setDescription(description);
		} else {
			sectionError = true;
		}
		return RetentionDataResponse.builder().retentionKey(retentionKey).sectionError(sectionError).build();
	}
	
	public void populateRelatedCfdi(RetentionData retentionData, final ApiRetention apiRetention, Map<String, String> sourceMap) {
		val relatedCfdi = retentionData.getRetRelatedCfdi();
		if (relatedCfdi != null) {
			val relationType = UBase64.base64Decode(relatedCfdi.getType());
			val uuid = UBase64.base64Decode(relatedCfdi.getUuid());
			if (!UValidator.isNullOrEmpty(relationType) && !UValidator.isNullOrEmpty(uuid)) {
				val relationTypeValue = sourceMap.get(relationType);
				apiRetention.setRelatedDocumentType(relationType);
				apiRetention.setRelatedDocumentPlainValue(UCatalog.plainValue(relationTypeValue));
				apiRetention.setRelatedDocumentUuid(uuid);
			}
		}
	}
	
	public boolean populateRetentionTotals(RetentionData retentionData, List<ApiRetImpRetenido> withheldTaxs, final ApiRetention apiRetention, final List<JError> errors) {
		val retTotal = retentionData.getRetTotal();
		if (!UValidator.isNullOrEmpty(retTotal)) {
			// Total monto Operación
			poplateTotalOperationAmount(retTotal, apiRetention, errors);
			// Total monto gravado
			populateTotalAmountTaxed(retTotal, apiRetention, errors);
			// Total monto exento
			populateTotalAmountExempt(retTotal, apiRetention, errors);
			// Total monto retenciones
			populateTotalAmountWithheld(retTotal, apiRetention, errors);
			// Utilidad bimestral
			populateQuarterlyProfit(retTotal, apiRetention, errors);
			// ISR correspondiente
			populateCorrespondingISR(retTotal, apiRetention, errors);
			// Impuesto retenidos
			apiRetention.setApiRetImpRetenidos(withheldTaxs);
			
			return false;
		} else {
			return true;
		}
	}
	
	private boolean populateReceiverNationality(String receiverNationality, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		if (!UValidator.isNullOrEmpty(receiverNationality)) {
			if (receiverNationality.equalsIgnoreCase("nacional") || receiverNationality.equalsIgnoreCase("extranjero")) {
				apiRetention.setReceiverNationality(receiverNationality);
			} else {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("resident");
			errors.add(error);
			return true;
		}
		return false;
	}
	
	private void populateNationalReceiver(RetReceiver retReceiver, final ApiRetention apiRetention, final List<JError> errors) {
		// Receptor nacional :: RFC
		var error = new JError();
		var errorMarked = false;
		
		val receiverNatRfc = UBase64.base64Decode(retReceiver.getReceiverNatRfc());
		if (!UValidator.isNullOrEmpty(receiverNatRfc)) {
			if (receiverNatRfc.length() < RFC_MIN_LENGTH || receiverNatRfc.length() > RFC_MAX_LENGTH || !receiverNatRfc.matches(RFC_EXPRESSION)) {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			} else {
				apiRetention.setReceiverNatRfc(receiverNatRfc);
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("rfc");
			errors.add(error);
		}
		
		// Receptor nacional :: Razón social
		val receiverNatBusinessName = UBase64.base64Decode(retReceiver.getReceiverNatBusinessName());
		if (!UValidator.isNullOrEmpty(receiverNatRfc)) {
			apiRetention.setReceiverNatBusinessName(receiverNatBusinessName);
		} else {
			val nameError = JError.required("name-business-name", userTools.getLocale());
			errors.add(nameError);
		}
		
		// Receptor nacional :: Domicilio Fiscal
		val receiverNatPostalCode = UBase64.base64Decode(retReceiver.getReceiverNatPostalCode());
		if (!UValidator.isNullOrEmpty(receiverNatPostalCode)) {
			apiRetention.setReceiverNatPostalCode(receiverNatPostalCode);
		} else {
			val nameError = JError.required("postal-code", userTools.getLocale());
			errors.add(nameError);
		}
		
		// Receptor nacional :: CURP
		val receiverNatCurp = UBase64.base64Decode(retReceiver.getReceiverNatCurp());
		if (!UValidator.isNullOrEmpty(receiverNatCurp)) {
			if (receiverNatCurp.length() == 18 && receiverNatCurp.matches(CURP_EXPRESSION)) {
				apiRetention.setReceiverNatCurp(receiverNatCurp);
			} else {
				val curpError = JError.invalidValue("curp", userTools.getLocale());
				errors.add(curpError);
			}
		}
	}
	
	private void populateForeignReceiver(RetReceiver retReceiver, final ApiRetention apiRetention, final List<JError> errors) {
		// Receptor extranjero :: Razón social
		val receiverExtBusinessName = UBase64.base64Decode(retReceiver.getReceiverExtBusinessName()); 
		if (!UValidator.isNullOrEmpty(receiverExtBusinessName)) {
			apiRetention.setReceiverExtBusinessName(receiverExtBusinessName);
		} else {
			val error = JError.required("ext-business-name", userTools.getLocale());
			errors.add(error);
		}
				
		// Receptor extranjero :: Número registro identidad fiscal
		val receiverExtNumber = UBase64.base64Decode(retReceiver.getReceiverExtNumber());
		if (!UValidator.isNullOrEmpty(receiverExtNumber)) {
			apiRetention.setReceiverExtNumber(receiverExtNumber);
		} 
	}
	
	private String populateRetentionKey(RetInfo retInfo, final ApiRetention apiRetention, final List<JError> errors) {
		val retentionKey = UBase64.base64Decode(retInfo.getRetentionKey());
		if (!UValidator.isNullOrEmpty(retentionKey)) {
			apiRetention.setRetentionKey(retentionKey);
		} else {
			val error = JError.required("retention-key", userTools.getLocale());
			errors.add(error);
		}
		return retentionKey;
	}
	
	private void populateExpeditionPlace(RetInfo retInfo, final ApiRetention apiRetention, final List<JError> errors) {
		val expeditionPlace = UBase64.base64Decode(retInfo.getExpeditionPlace());
		if (!UValidator.isNullOrEmpty(expeditionPlace)) {
			apiRetention.setExpeditionPlace(expeditionPlace);
		} else {
			val error = JError.required("expedition-place", userTools.getLocale());
			errors.add(error);
		}
	}
	
	private void populateInitialMonth(RetInfo retInfo, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val initialMonth = UBase64.base64Decode(retInfo.getInitialMonth());
		if (!UValidator.isNullOrEmpty(initialMonth)) {
			val validateMonth = UValue.integerValue(initialMonth);
			if (!UValidator.isNullOrEmpty(validateMonth) && validateMonth >= 1 && validateMonth <= 12) {
				apiRetention.setInitialMonth(initialMonth);
			} else {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("initial-month");
			errors.add(error);
		}
	}
	
	private void populateEndMonth(RetInfo retInfo, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val endMonth = UBase64.base64Decode(retInfo.getEndMonth());
		if (!UValidator.isNullOrEmpty(endMonth)) {
			val validateMonth = UValue.integerValue(endMonth);
			if (!UValidator.isNullOrEmpty(validateMonth) && validateMonth >= 1 && validateMonth <= 12) {
				apiRetention.setEndMonth(endMonth);
			} else {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("end-month");
			errors.add(error);
		}
	}
	
	private void populateYear(RetInfo retInfo, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val year = UBase64.base64Decode(retInfo.getYear());
		if (!UValidator.isNullOrEmpty(year)) {
			val validateYear = UValue.integerValue(year);
			if (!UValidator.isNullOrEmpty(validateYear) && year.length() == 4) {
				val range = iRetentionYearRangeJpaRepository.findByEliminadoFalse();
				if (!UValidator.isNullOrEmpty(range)) {
					if (validateYear >= range.getEjercicioInicial() && validateYear <= range.getEjercicioFinal()) {
						apiRetention.setYear(year);
					} else {
						val rangeError = "El Ejercicio fiscal debe estar entre el rango de " + range.getEjercicioInicial() + " y " + range.getEjercicioFinal();
						error.setMessage(rangeError);
						errorMarked = true;
					}
				} else {
					error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
					errorMarked = true;
				}
			} else {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("year");
			errors.add(error);
		}
	}
	
	private void poplateTotalOperationAmount(RetTotal retTotal, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val totalOperationAmount = UBase64.base64Decode(retTotal.getTotalOperationAmount());
		if (!UValidator.isNullOrEmpty(totalOperationAmount)) {
			val bgValidate = UValue.bigDecimalStrict(totalOperationAmount);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setTotalOperationAmount(totalOperationAmount);
			} else {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("total-operation-amount");
			errors.add(error);
		}
	}
	
	private void populateTotalAmountTaxed(RetTotal retTotal, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val totalAmountTaxed = UBase64.base64Decode(retTotal.getTotalAmountTaxed());
		if (!UValidator.isNullOrEmpty(totalAmountTaxed)) {
			val bgValidate = UValue.bigDecimalStrict(totalAmountTaxed);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setTotalAmountTaxed(totalAmountTaxed);
			} else {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("total-amount-taxed");
			errors.add(error);
		}
	}
	
	private void populateTotalAmountExempt(RetTotal retTotal, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val totalAmountExempt = UBase64.base64Decode(retTotal.getTotalAmountExempt());
		if (!UValidator.isNullOrEmpty(totalAmountExempt)) {
			val bgValidate = UValue.bigDecimalStrict(totalAmountExempt);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setTotalAmountExempt(totalAmountExempt);
			} else {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("total-amount-exempt");
			errors.add(error);
		}
	}
	
	private void populateTotalAmountWithheld(RetTotal retTotal, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;
		
		val totalAmountWithheld = UBase64.base64Decode(retTotal.getTotalAmountWithheld());
		if (!UValidator.isNullOrEmpty(totalAmountWithheld)) {
			val bgValidate = UValue.bigDecimalStrict(totalAmountWithheld);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setTotalAmountWithheld(totalAmountWithheld);
			} else {
				error.setMessage(UProperties.getMessage(ERR1004, userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage(ERR1001, userTools.getLocale()));
			errorMarked = true;
		}
		
		if (errorMarked) {
			error.setField("total-amount-withheld");
			errors.add(error);
		}
	}
	
	private void populateQuarterlyProfit(RetTotal retTotal, final ApiRetention apiRetention, final List<JError> errors) {
		val quarterlyProfit = UBase64.base64Decode(retTotal.getQuarterlyProfit());
		if (!UValidator.isNullOrEmpty(quarterlyProfit)) {
			val bgValidate = UValue.bigDecimalStrict(quarterlyProfit);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setQuarterlyProfit(quarterlyProfit);
			} else {
				val error = JError.invalidValue("quarterly-profit", userTools.getLocale());
				errors.add(error);
			}
		}
	}
	
	private void populateCorrespondingISR(RetTotal retTotal, final ApiRetention apiRetention, final List<JError> errors) {
		val correspondingISR = UBase64.base64Decode(retTotal.getCorrespondingISR());
		if (!UValidator.isNullOrEmpty(correspondingISR)) {
			val bgValidate = UValue.bigDecimalStrict(correspondingISR);
			if (!UValidator.isNullOrEmpty(bgValidate)) {
				apiRetention.setCorrespondingISR(correspondingISR);
			} else {
				val error = JError.invalidValue("corresponding-isr", userTools.getLocale());
				errors.add(error);
			}
		}
	}
	
}