package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PreInvoiceNotificationRequest {
	private String emitterRfc;
	private String receiverRfc;
	private String receiverName;
	private String receiverEmail;
	private ContribuyenteEntity taxpayer;
	private String fileName;
	private String pdfPath;
	private PrefacturaEntity preInvoiceStored;
	private PreferenciasEntity preferences;
}