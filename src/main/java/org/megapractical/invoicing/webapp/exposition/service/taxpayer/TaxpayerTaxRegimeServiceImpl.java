package org.megapractical.invoicing.webapp.exposition.service.taxpayer;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.springframework.stereotype.Service;

@Service
public class TaxpayerTaxRegimeServiceImpl implements TaxpayerTaxRegimeService {

	private final ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;

	public TaxpayerTaxRegimeServiceImpl(ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository) {
		this.iTaxpayerTaxRegimeJpaRepository = iTaxpayerTaxRegimeJpaRepository;
	}

	@Override
	public ContribuyenteRegimenFiscalEntity taxRegime(ContribuyenteEntity taxpayer) {
		return iTaxpayerTaxRegimeJpaRepository.findByContribuyente(taxpayer);
	}

}