package org.megapractical.invoicing.webapp.exposition.service;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
public class VoucherBuilderService {
	
	@Autowired
	AppSettings appSettings;
	
	public void populateEmitter(ApiCfdi apiCfdi, final Comprobante voucher) {
		val emitter = new Comprobante.Emisor();
		emitter.setNombre(UValue.singleReplace(apiCfdi.getEmitter().getNombreRazonSocial()));
		emitter.setRegimenFiscal(UValue.stringValueUppercase(apiCfdi.getEmitter().getRegimenFiscal().getCodigo()));
		emitter.setRfc(UValue.stringValueUppercase(apiCfdi.getEmitter().getRfc()));
		voucher.setEmisor(emitter);
	}
	
	public void populateReceiver(ApiCfdi apiCfdi, final Comprobante voucher) {
		val receiver = new Comprobante.Receptor();
		receiver.setRfc(UValue.stringValueUppercase(apiCfdi.getReceiver().getRfc()));
		receiver.setUsoCFDI(UCatalog.usoCFDI(apiCfdi.getReceiver().getUsoCFDI().getCodigo()));
		if (!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getNombreRazonSocial())) {
			val cleanReceiverName = UValue.fixQuotes(apiCfdi.getReceiver().getNombreRazonSocial());
			apiCfdi.getReceiver().setNombreRazonSocial(cleanReceiverName);
			receiver.setNombre(apiCfdi.getReceiver().getNombreRazonSocial());
		}
		if (!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getResidenciaFiscal())
				&& !apiCfdi.getReceiver().getResidenciaFiscal().getCodigo().equalsIgnoreCase("mex")) {
			receiver.setResidenciaFiscal(UCatalog.pais(apiCfdi.getReceiver().getResidenciaFiscal().getCodigo()));
		}
		if (!UValidator.isNullOrEmpty(apiCfdi.getReceiver().getNumRegIdTrib())) {
			receiver.setNumRegIdTrib(UValue.stringValue(apiCfdi.getReceiver().getNumRegIdTrib()));
		}
		receiver.setDomicilioFiscalReceptor(apiCfdi.getReceiver().getDomicilioFiscal());
		receiver.setRegimenFiscalReceptor(apiCfdi.getReceiver().getTaxRegime());
		voucher.setReceptor(receiver);
	}
	
	public void populateVoucher(ApiCfdi apiCfdi, final Comprobante voucher, boolean useDateByPostalCode) {
		try {
			voucher.setVersion(VoucherUtils.VOUCHER_VERSION_DEFAULT);
			setSello(apiCfdi, voucher);
			voucher.setNoCertificado(UCertificateX509.certificateSerialNumber(apiCfdi.getEmitter().getCertificado()));
			voucher.setCertificado(UCertificateX509.certificateBase64(apiCfdi.getEmitter().getCertificado()));
			
			var voucherDate = UDateTime.format(LocalDateTime.now().minusHours(1));
			if (!appSettings.isDev()) {
				if (useDateByPostalCode) {
					voucherDate = UDate.getDateByZipCode(apiCfdi.getVoucher().getPostalCode());
				} else {
					voucherDate = apiCfdi.getVoucher().getDateTimeExpedition();
				}
			}
			voucher.setFecha(voucherDate);
			
			voucher.setSubTotal(apiCfdi.getVoucher().getSubTotal());
			voucher.setTotal(apiCfdi.getVoucher().getTotal());
			voucher.setTipoDeComprobante(apiCfdi.getVoucher().getVoucherType());
			voucher.setMoneda(apiCfdi.getVoucher().getCurrency());
			voucher.setExportacion(apiCfdi.getVoucher().getExportation());
			voucher.setLugarExpedicion(UValue.stringValue(apiCfdi.getVoucher().getPostalCode()));
			voucher.setSerie(UValue.stringValue(apiCfdi.getVoucher().getSerie()));
			voucher.setFolio(UValue.stringValue(apiCfdi.getVoucher().getFolio()));
			voucher.setCondicionesDePago(UValue.stringValueSingleReplace(apiCfdi.getVoucher().getPaymentConditions()));
			voucher.setDescuento(UValue.getBigDecimal(apiCfdi.getVoucher().getDiscount()));
			voucher.setTipoCambio(UValue.getBigDecimal(apiCfdi.getVoucher().getChangeType()));
			voucher.setConfirmacion(UValue.stringValue(apiCfdi.getVoucher().getConfirmation()));

			if (!UValidator.isNullOrEmpty(apiCfdi.getVoucher().getPaymentWay())) {
				voucher.setFormaPago(apiCfdi.getVoucher().getPaymentWay());
			}

			if (!UValidator.isNullOrEmpty(apiCfdi.getVoucher().getPaymentMethod())) {
				voucher.setMetodoPago(apiCfdi.getVoucher().getPaymentMethod());
			}
		} catch (IOException | CertificateException e) {
			Logger.getLogger(VoucherBuilderService.class.getName()).log(Level.SEVERE, (String) null, e);
		}
	}

	private void setSello(ApiCfdi apiCfdi, final Comprobante voucher) {
		val certificate = apiCfdi.getEmitter().getCertificado();
		val privateKey = apiCfdi.getEmitter().getLlavePrivada();
		val keyPasswd = apiCfdi.getEmitter().getPasswd();
		voucher.setSello(UCertificateX509.certificateSeal(certificate, privateKey, keyPasswd));
	}
	
}