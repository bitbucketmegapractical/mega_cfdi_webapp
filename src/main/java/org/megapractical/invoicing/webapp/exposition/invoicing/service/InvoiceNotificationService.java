package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.InvoiceNotificationRequest;

public interface InvoiceNotificationService {
	
	void sendNotification(InvoiceNotificationRequest notificationRequest);
	
}