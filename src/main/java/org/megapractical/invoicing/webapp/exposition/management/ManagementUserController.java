package org.megapractical.invoicing.webapp.exposition.management;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCalculation;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioContratoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.CredencialEntity;
import org.megapractical.invoicing.dal.bean.jpa.CuentaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PerfilEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.ProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreCompraEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbrePaqueteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreTransferenciaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IConsigneeContractJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICredentialJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPreferencesJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRoleJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampBuyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampPackageJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IUserJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.json.JConsignee;
import org.megapractical.invoicing.webapp.json.JConsignee.Consignee;
import org.megapractical.invoicing.webapp.json.JConsignee.ConsigneeContract;
import org.megapractical.invoicing.webapp.json.JCredential;
import org.megapractical.invoicing.webapp.json.JCredential.Credential;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JStamp;
import org.megapractical.invoicing.webapp.json.JStampBuyTransfer;
import org.megapractical.invoicing.webapp.json.JStampBuyTransfer.StampTransferOptions;
import org.megapractical.invoicing.webapp.json.JStampPackage;
import org.megapractical.invoicing.webapp.json.JStampPackage.StampPackage;
import org.megapractical.invoicing.webapp.json.JStampPackage.StampPackageOptions;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.EmitterTools;
import org.megapractical.invoicing.webapp.support.Layout;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.ui.Paginator;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import lombok.val;

@Controller
@Scope("session")
public class ManagementUserController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	EmitterTools emitterTools;
	
	@Autowired
	StampControlService stampControlService;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Resource
	ICredentialJpaRepository iCredentialJpaRepository;
	
	@Resource
	IUserJpaRepository iUserJpaRepository;
	
	@Resource
	ITaxpayerJpaRepository iTaxpayerJpaRepository; 
	
	@Resource
	IStampPackageJpaRepository iStampPackageJpaRepository;
	
	@Resource
	IStampJpaRepository iStampJpaRepository;
	
	@Resource
	IPreferencesJpaRepository iPreferencesJpaRepository;
	
	@Resource
	IStampBuyJpaRepository iStampBuyJpaRepository;
	
	@Resource
	IRoleJpaRepository iRoleJpaRepository;
	
	@Resource
	ICurrencyJpaRepository iCurrencyJpaRepository;
	
	@Resource
	IPersonTypeJpaRepository iPersonTypeJpaRepository;
	
	@Resource
	IProductServiceJpaRepository iProductServiceJpaRepository;
	
	@Resource
	IAccountJpaRepository iAccountJpaRepository;
	
	@Resource
	IConsigneeContractJpaRepository iConsigneeContractJpaRepository;
	
	private static final Integer RFC_MIN_LENGTH = 12;
	private static final Integer RFC_MAX_LENGTH = 13;
	
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	private static final String SORT_PROPERTY = "usuario.correoElectronico";
	
	private static final String ROL_CODE = "ROLE_MST";
	private static final String PRODUCT = "CFDI_PLUS";
	private static final String CURRENCY_CODE = "MXN";
	
	private static final String PERSON_TYPE_PHYSICAL_CODE = "PF";
	private static final String PERSON_TYPE_MORAL_CODE = "PM";
	
	private static final Integer INITIAL_PAGE = 0;
	
	//##### Contribuyente
	private ContribuyenteEntity taxpayer;
	
	//##### Cuenta
	private List<CuentaEntity> accounts;
	
	//##### Timbre
	private TimbreEntity stamp;
	private TimbrePaqueteEntity stampPackage;
	private List<TimbrePaqueteEntity> stampPackages;
	
	//##### Usuario
	private UsuarioEntity user;
	
	//##### Credencial
	private CredencialEntity credential;
	private JCredential credentialJson;
	
	//##### Mensaje campo requerido
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	//##### Mensaje procesando...
	@ModelAttribute("processingRequest")
	public String processingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	//##### Mensaje cargando...
	@ModelAttribute("loadingRequest")
	public String loadingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	//##### Mensaje formato incorrecto
	@ModelAttribute("invalidDataType")
	public String invalidDataTypeMessage() throws IOException{
		return UProperties.getMessage("ERR1003", userTools.getLocale());
	}
	
	//##### Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() throws IOException{
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	//##### Valores condicionales para la activacion de timbres
	@ModelAttribute("incompleteStampReference")
	public String incompleteStampReference() throws IOException{
		return UProperties.getMessage("ERR0124", userTools.getLocale());
	}
	
	//##### Css class danger
	@ModelAttribute("cssAlertDanger")
	public String cssAlertDanger() throws IOException{
		return CssStyleClass.ERROR.value();
	}
	
	@Layout(value = "layouts/admon")
	@RequestMapping(value = "/managementUser", method = RequestMethod.GET)
	public String init(Model model) {
		try {
			
			if(userTools.isAdmon() || userTools.isRoot()){				
				
				model.addAttribute("page", INITIAL_PAGE);
				
				List<TimbrePaqueteEntity> stampPackageSource = iStampPackageJpaRepository.findByEliminadoFalse();
				List<String> stampPackages = new ArrayList<>();  
				for (TimbrePaqueteEntity item : stampPackageSource) {
					stampPackages.add(item.getCantidad().toString());
				}
				model.addAttribute("stampPackSource", stampPackages);
				
				HtmlHelper.functionalitySelected("MngGeneral", "management-user");
				appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
				
				return "/management/ManagementUser";
				
			}else{
				return "/denied/ResourceDenied";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@Layout(value = "layouts/admon")
	@RequestMapping(value = "/managementUser", method = RequestMethod.POST)
	public @ResponseBody String managementUser(HttpServletRequest request) {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		JCredential credential = new JCredential();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchEmail = UBase64.base64Decode(request.getParameter("searchEmail"));
			if(UValidator.isNullOrEmpty(searchEmail)){
				searchEmail = "";
			}
			
			String searchRfc = UBase64.base64Decode(request.getParameter("searchRfc"));
			if(UValidator.isNullOrEmpty(searchRfc)){
				searchRfc = "";
			}
			
			String searchName = UBase64.base64Decode(request.getParameter("searchName"));
			if(UValidator.isNullOrEmpty(searchName)){
				searchName = "";
			}
			
			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);
			
			Page<CredencialEntity> credentialPage = null;
			Paginator<CredencialEntity> paginator = null;
			
			credentialPage = iCredentialJpaRepository.findByEliminadoFalseAndUsuario_CorreoElectronicoContainingIgnoreCaseAndContribuyente_RfcContainingIgnoreCaseAndContribuyente_NombreRazonSocialContainingIgnoreCase(searchEmail, searchRfc, searchName, pageRequest);
			val pagination = JPagination.buildPaginaton(credentialPage);
			credential.setPagination(pagination);
			
			for (CredencialEntity item : credentialPage) {
				Credential credentialObj = new Credential();
				credentialObj.setId(UBase64.base64Encode(item.getId().toString()));
				credentialObj.setLanguage(UBase64.base64Encode(item.getLenguaje().getValor()));
				
				UsuarioEntity user = new UsuarioEntity();
				user = item.getUsuario();
				credentialObj.setUserId(UBase64.base64Encode(user.getId().toString()));
				credentialObj.setEmail(UBase64.base64Encode(user.getCorreoElectronico()));
				
				ContribuyenteEntity taxpayer = new ContribuyenteEntity();
				taxpayer = item.getContribuyente();
				credentialObj.setTaxpayerId(UBase64.base64Encode(taxpayer.getId().toString()));
				credentialObj.setName(UBase64.base64Encode(taxpayer.getNombreRazonSocial()));
				credentialObj.setRfc(UBase64.base64Encode(taxpayer.getRfc()));
				credentialObj.setRfcActive(UBase64.base64Encode(taxpayer.getRfcActivo()));
				credentialObj.setPhone(UBase64.base64Encode(taxpayer.getTelefono()));
				credentialObj.setActiveProduct(UBase64.base64Encode(taxpayer.getProductoServicioActivo()));
				
				credential.getCredentials().add(credentialObj);
			}
			
			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if(!UValidator.isNullOrEmpty(searchName)){
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
			}
			
			if(credential.getCredentials().isEmpty()){
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}
			
			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		credential.setResponse(response);
		jsonInString = gson.toJson(credential);
		return jsonInString;
	}
	
	public void clear(){
		this.credentialJson = null;
		
		this.credential = null;
		this.user = null;
		this.taxpayer = null;
		
		
		this.stampPackages = new ArrayList<>();
	}
	
	public void populate(String value){
		try {
			
			clear();
			if(!UValidator.isNullOrEmpty(value)){
				String[] valueSplit = value.split(Pattern.quote("_"));
				
				try {
					
					Long userId = UValue.longValue(valueSplit[1]);
					this.user = iUserJpaRepository.findById(userId).orElse(null);
					
					Long taxpayerId = UValue.longValue(valueSplit[2]);
					this.taxpayer = iTaxpayerJpaRepository.findById(taxpayerId).orElse(null);
					
					Long credentialId = UValue.longValue(valueSplit[0]);
					this.credential = iCredentialJpaRepository.findByUsuarioAndContribuyenteAndEliminadoFalse(this.user, this.taxpayer);
					
					if(!UValidator.isNullOrEmpty(this.credential)){
						if(!UValue.longString(this.credential.getId()).equals(UValue.longString(credentialId))){
							clear();
						}else{
							Credential credential = new Credential();
							credential.setId(UBase64.base64Encode(this.credential.getId().toString()));
							credential.setLanguage(UBase64.base64Encode(this.credential.getLenguaje().getValor()));
							
							credential.setUserId(UBase64.base64Encode(this.user.getId().toString()));
							credential.setEmail(UBase64.base64Encode(this.user.getCorreoElectronico()));
							
							credential.setTaxpayerId(UBase64.base64Encode(this.taxpayer.getId().toString()));
							credential.setName(UBase64.base64Encode(this.taxpayer.getNombreRazonSocial()));
							credential.setRfc(UBase64.base64Encode(this.taxpayer.getRfc()));
							credential.setRfcActive(UBase64.base64Encode(this.taxpayer.getRfcActivo()));
							credential.setPhone(UBase64.base64Encode(this.taxpayer.getTelefono()));
							credential.setActiveProduct(UBase64.base64Encode(this.taxpayer.getProductoServicioActivo()));
							
							this.credentialJson = new JCredential();
							this.credentialJson.setCredential(credential);
						}
					}
					
				} catch (Exception e) {
					clear();
				}
				
			}else{
				clear();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			clear();
		}
	}
	
	//###############################################################################################
	//##### Activar timbres #########################################################################
	//###############################################################################################
	@Layout(value = "layouts/admon")
	@RequestMapping(value = "/managementUser", params={"stampActivate"})
	public @ResponseBody String stampActivate(HttpServletRequest request) throws IOException {
		
		JStampPackage stampPackageJson = new JStampPackage();
		StampPackage stampPackage = new StampPackage(); 
			
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		Gson gson = new Gson();
		String jsonInString = null;
		try {
			
			String options = "<option value=\"-\">&laquo;Seleccione&raquo;</option>";
			List<StampPackageOptions> stampPackageOptions = new ArrayList<>();
			
			String credential = UBase64.base64Decode(request.getParameter("credential"));
			if(!UValidator.isNullOrEmpty(credential)){
				this.populate(credential);
				if(!UValidator.isNullOrEmpty(this.credential) && !UValidator.isNullOrEmpty(this.user) && !UValidator.isNullOrEmpty(this.taxpayer)){
					// Cargando la informacion de los timbres de la cuenta RFC seleccionada
					this.stamp = iStampJpaRepository.findByContribuyente(this.taxpayer);
					
					this.stampPackages.addAll(iStampPackageJpaRepository.findByEliminadoFalse());
					for (TimbrePaqueteEntity item : this.stampPackages) {
						String option = "<option value="+UBase64.base64Encode(item.getId().toString())+">"+item.getCantidad()+"</option>";
						
						if(!UValidator.isNullOrEmpty(options)){
							options += option;
						}else{
							options = option;
						}
						
						StampPackageOptions spo = new StampPackageOptions();
						spo.setId(UBase64.base64Encode(item.getId().toString()));
						spo.setValue(UBase64.base64Encode(item.getCantidad().toString()));
						stampPackageOptions.add(spo);
					}
					
					stampPackage.setOptions(UBase64.base64Encode(options));
					stampPackageJson.setStampPackage(stampPackage);
					stampPackageJson.getStampPackageOptions().addAll(stampPackageOptions);
					this.credentialJson.setStampPackage(stampPackageJson);
					
					response.setSuccess(Boolean.TRUE);
				}else{
					notification.setPageMessage(Boolean.TRUE);
					notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setError(Boolean.TRUE);
					response.setNotification(notification);
				}
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setError(Boolean.TRUE);
				response.setNotification(notification);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setError(Boolean.TRUE);
			response.setNotification(notification);
		}
		
		sessionController.sessionUpdate();
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		return jsonInString;
	}
	
	@RequestMapping(value="/managementUser", params={"loadStampPackageInfo"})
	public @ResponseBody String stampPackageInfo(@RequestParam("amount") String amount, HttpServletRequest request) throws IOException{
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		JStampPackage stampPackageJson = new JStampPackage();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			Integer quantity = Integer.valueOf(amount);
			
			
			this.stampPackage = new TimbrePaqueteEntity();
			this.stampPackage = iStampPackageJpaRepository.findByCantidad(quantity);
			if(!UValidator.isNullOrEmpty(stampPackage)){
				
				StampPackage stampPackageObj = new StampPackage();
				stampPackageObj.setQuantity(quantity);
				stampPackageObj.setUnitPrice(UValue.doubleString(this.stampPackage.getPrecioUnitario()));
				stampPackageObj.setTaxPercent(UValue.doubleString(this.stampPackage.getIvaPorciento()));
				stampPackageObj.setTaxImport(UValue.doubleString(this.stampPackage.getImpuesto()));
				stampPackageObj.setSubtotal(UValue.doubleString(this.stampPackage.getPrecioUnitario()));
				stampPackageObj.setTotal(UValue.doubleString(this.stampPackage.getPrecioVenta()));
				stampPackageObj.setDescription(this.stampPackage.getDescripcion());
				stampPackageJson.setStampPackage(stampPackageObj);
				
				response.setSuccess(Boolean.TRUE);
				
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setMessage(UProperties.getMessage("ERR0111", userTools.getLocale()));
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setError(Boolean.TRUE);
				response.setNotification(notification);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setMessage(UProperties.getMessage("ERR0111", userTools.getLocale()));
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setError(Boolean.TRUE);
			response.setNotification(notification);
		}
		
		sessionController.sessionUpdate();
		
		stampPackageJson.setResponse(response);
		jsonInString = gson.toJson(stampPackageJson);
		return jsonInString;
	}
	
	@RequestMapping(value = "/stampIndicateCalculation", method = RequestMethod.POST)
	public @ResponseBody String stampIndicateCalculation(HttpServletRequest request){
		
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		String quantityStr = null;
		String unitPriceStr = null;
		
		String taxStr = null;
		String salePriceStr = null;
		
		try {
			
			String objStampBuyTransfer = request.getParameter("objStampBuyTransfer");
			stampBuyTransfer = gson.fromJson(objStampBuyTransfer, JStampBuyTransfer.class);
			
			quantityStr = UBase64.base64Decode(stampBuyTransfer.getQuantity());
			BigDecimal quantity = null;
			if(!UValidator.isNullOrEmpty(quantityStr)){
				if(UValidator.isBigDecimal(quantityStr)){
					quantity = UValue.bigDecimal(quantityStr);
					quantityStr = UBase64.base64Encode(UValue.bigDecimalString(quantity));
				}
			}
			
			unitPriceStr = UBase64.base64Decode(stampBuyTransfer.getUnitPrice());
			BigDecimal unitPrice = null;
			if(!UValidator.isNullOrEmpty(unitPriceStr)){
				if(UValidator.isBigDecimal(unitPriceStr)){
					unitPrice = UValue.bigDecimal(unitPriceStr);
					unitPriceStr = UBase64.base64Encode(UValue.bigDecimalString(unitPrice));
				}
			}
			
			BigDecimal tax = null;
			
			String valueNull = null;
			BigDecimal salePrice = UValue.bigDecimal(valueNull);
			BigDecimal percentage = new BigDecimal(16);
			
			if(!UValidator.isNullOrEmpty(quantity) && !UValidator.isNullOrEmpty(unitPrice)){
				tax = UValue.bigDecimal(UValue.bigDecimalString(UCalculation.percentage(unitPrice, percentage)));
				taxStr = UBase64.base64Encode(UValue.bigDecimalString(tax));
				
				salePrice = unitPrice.add(tax);
				salePriceStr = UBase64.base64Encode(UValue.bigDecimalString(salePrice));
			}
			
		} catch (Exception e) {
			taxStr = null;
			salePriceStr = null;
		}
		
		stampBuyTransfer.setTax(taxStr);
		stampBuyTransfer.setSalePrice(salePriceStr);
		
		jsonInString = gson.toJson(stampBuyTransfer);
		return jsonInString;
	}
	//*****************************************************************//
	/*@RequestMapping(value="/managementUSer", params={"datePeriodActivate"}) 
	public @ResponseBody String datePeriodActivate(HttpServletRequest request, @RequestParam(value="document") Optional<MultipartFile> documentParam) throws IOException{
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		JError error = new JError();
		List<JError> errors = new ArrayList<>();			
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		Boolean sessionDestroyed = false;

		try {
			
			File document = null;
			String documentExtension = null;
			
			Boolean errorMarked = false;
			
			if(!UValidator.isNullOrEmpty(this.taxpayer)){
				
				
				
				
				if(!stampBuyTransfer.getStampIndicate() && UValidator.isNullOrEmpty(this.stampPackage)){
					notification.setPageMessage(Boolean.TRUE);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					notification.setMessage(UProperties.getMessage("ERR0070", userTools.getLocale()));
					response.setNotification(notification);
					response.setUnexpectedError(Boolean.TRUE);
					errorMarked = true;
				}else if(stampBuyTransfer.getStampIndicate()){
					if(UValidator.isNullOrEmpty(stampBuyTransfer.getQuantity())){
						errorMarked = true;
					}
					if(!stampBuyTransfer.getStampIndicateChangeSystem()){
						if(UValidator.isNullOrEmpty(stampBuyTransfer.getUnitPrice())){
							errorMarked = true;
						}
						if(UValidator.isNullOrEmpty(stampBuyTransfer.getSalePrice())){
							errorMarked = true;
						}
						if(UValidator.isNullOrEmpty(stampBuyTransfer.getTax())){
							errorMarked = true;
						}
					}					
				}
				if(errorMarked){
					if(!stampBuyTransfer.getStampIndicate()){
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(UProperties.getMessage("ERR0070", userTools.getLocale()));
						response.setNotification(notification);
						response.setUnexpectedError(Boolean.TRUE);
					}else{
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(UProperties.getMessage("ERR0120", userTools.getLocale()));
						response.setNotification(notification);
						response.setUnexpectedError(Boolean.TRUE);
					}
				}else{
					try {
						MultipartFile multipartFileDocument = documentParam.get();
						if (!multipartFileDocument.isEmpty()) {
							String documentName = multipartFileDocument.getOriginalFilename();
							documentExtension = UFile.getExtension(documentName);
							if(UValidator.isNullOrEmpty(documentExtension) || (!documentExtension.equalsIgnoreCase("jpg") && !documentExtension.equalsIgnoreCase("png") && !documentExtension.equalsIgnoreCase("pdf") && !documentExtension.equalsIgnoreCase("msg"))){
								String errorMessage = UProperties.getMessage("ERR1005", userTools.getLocale()).concat(" ").concat(UProperties.getMessage("ERR0125", userTools.getLocale()));
								error.setMessage(errorMessage);
								error.setField("stamp-reference-document");
								errors.add(error);
								errorMarked = true;
							}else{
								document = UFile.MultipartFileToFile(multipartFileDocument);
							}
						}
					} catch (Exception e) {}
					
					String transactionId = UBase64.base64Decode(stampBuyTransfer.getTransactionId());
					if(!UValidator.isNullOrEmpty(transactionId)){
						error = new JError();
						TimbreCompraEntity stampByEntity = iStampBuyJpaRepository.findByIdTransaccion(transactionId);
						if(!UValidator.isNullOrEmpty(stampByEntity)){
							error.setMessage(UProperties.getMessage("ERR0126", userTools.getLocale()));
							error.setField("stamp-reference-transaction");
							errors.add(error);
							errorMarked = true;
						}
					}
					
					String reference = UBase64.base64Decode(stampBuyTransfer.getReference());
					if(!UValidator.isNullOrEmpty(reference)){
						if(!stampBuyTransfer.getStampIndicateChangeSystem()){
							error = new JError();
							TimbreCompraEntity stampByEntity = iStampBuyJpaRepository.findByReferencia(reference);
							if(!UValidator.isNullOrEmpty(stampByEntity)){
								error.setMessage(UProperties.getMessage("ERR0127", userTools.getLocale()));
								error.setField("stamp-reference-reference");
								errors.add(error);
								errorMarked = true;
							}
						}						
					}
					
					if(!errorMarked){
						System.out.println("-----------------------------------------------------------------------------------------------------------");
						if(!stampBuyTransfer.getStampIndicate()){
							System.out.println(userTools.log() + "[INFO] ACTIVATING STAMP PACKAGE...");
						}else{
							System.out.println(userTools.log() + "[INFO] ACTIVATING STAMP INDICATE...");
						}
						
						if(!UValidator.isNullOrEmpty(this.stamp)){
							Integer quantity = null;
							if(!stampBuyTransfer.getStampIndicate()){
								quantity = this.stampPackage.getCantidad();
								stampBuyTransfer.setQuantity(UBase64.base64Encode(quantity.toString()));
							}else{
								String quantityStr = UBase64.base64Decode(stampBuyTransfer.getQuantity());
								quantity = Integer.valueOf(quantityStr);
							}
							this.stamp.setTotal(this.stamp.getTotal() + quantity);
							this.stamp.setTimbresDisponibles(this.stamp.getTimbresDisponibles() + quantity);
							this.stamp.setHabilitado(Boolean.TRUE);
							persistenceDAO.update(this.stamp);
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(userTools.log() + "[INFO] USER'S STAMP UPDATED");
						}else{
							Integer quantity = null;
							if(!stampBuyTransfer.getStampIndicate()){
								quantity = this.stampPackage.getCantidad();
								stampBuyTransfer.setQuantity(UBase64.base64Encode(quantity.toString()));
							}else{
								String quantityStr = UBase64.base64Decode(stampBuyTransfer.getQuantity());
								quantity = Integer.valueOf(quantityStr);
							}
							this.stamp = new TimbreEntity();
							this.stamp.setContribuyente(this.taxpayer);
							this.stamp.setHabilitado(Boolean.TRUE);
							this.stamp.setTimbresConsumidos(0);
							this.stamp.setTimbresDisponibles(quantity);
							this.stamp.setTimbresTransferidos(0);
							this.stamp.setTotal(quantity);
							persistenceDAO.persist(this.stamp);
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(userTools.log() + "[INFO] USER'S STAMP CREATED");
						}
						
						JStamp stampJson = new JStamp();
						stampJson.setStampAvailable(UBase64.base64Encode(this.stamp.getTimbresDisponibles().toString()));
						stampJson.setStampSpent(UBase64.base64Encode(this.stamp.getTimbresConsumidos().toString()));
						stampJson.setEnabled(this.stamp.getHabilitado());
						
						Integer stampTransfered = !UValidator.isNullOrEmpty(this.stamp.getTimbresTransferidos()) ? this.stamp.getTimbresTransferidos() : 0; 
						Integer stampTotal = this.stamp.getTotal();
						
						stampJson.setStampTransfered(UBase64.base64Encode(stampTransfered.toString()));
						stampJson.setStampTotal(UBase64.base64Encode(stampTotal.toString()));

						this.credentialJson.setStamp(stampJson);
						
						OperationCode operationCode = null;
						EventTypeCode eventTypeCode = null;
						
						// Registrando la informacion de la compra
						TimbreCompraEntity stampBuy = new TimbreCompraEntity();
						stampBuy.setContribuyente(this.taxpayer);
						stampBuy.setFecha(new Date());
						stampBuy.setHora(new Date());
						
						if(!UValidator.isNullOrEmpty(transactionId)){
							stampBuy.setIdTransaccion(transactionId);
						}
						
						if(!UValidator.isNullOrEmpty(reference)){
							stampBuy.setReferencia(reference);
						}
						
						if(!UValidator.isNullOrEmpty(document)){
							String stampReferencePath = appSettings.getPropertyValue("cfdi.stamp.reference");
							String stampReferenceFolder = UDateTime.getLocalDateToday().toString().replaceAll("-", "");
							String directory = stampReferencePath + File.separator + stampReferenceFolder;
							
							String fileName = UValue.uuid().replaceAll("-", "").toLowerCase().concat(".").concat(documentExtension);
							String filePath = directory + File.separator + fileName;
							String dbPath = stampReferenceFolder + File.separator + fileName;
									
							try {
								
								File fileFolder = new File(directory); 
								if(!fileFolder.exists()){
									fileFolder.mkdirs();
								}
								
								UFile.writeFile(UFile.FileToByte(document), filePath);
								
								stampBuy.setDocumento(dbPath);
								
								stampBuyTransfer.setDocument(UBase64.base64Encode(fileName));
								stampBuyTransfer.setDocumentExtension(UBase64.base64Encode(documentExtension));
								
								String dbPathReplaced = dbPath.replaceAll(Pattern.quote("\\"), "_").replaceAll(Pattern.quote("/"), "_");
								stampBuyTransfer.setDocumentPath(UBase64.base64Encode(dbPathReplaced));
								
							} catch (Exception e) {}
						}
						
						String description = UBase64.base64Decode(stampBuyTransfer.getDescription());
						if(!UValidator.isNullOrEmpty(description)){
							stampBuy.setDescripcion(description);
						}
						
						if(!stampBuyTransfer.getStampIndicate()){
							stampBuy.setTimbrePaquete(this.stampPackage);
							
							operationCode = OperationCode.ACTIVACION_PAQUETE_TIMBRE_ID12;
							eventTypeCode = EventTypeCode.ACTIVACION_PAQUETE_TIMBRE_ID1;
							
						}else{
							String quantityStr = UBase64.base64Decode(stampBuyTransfer.getQuantity());
							Integer quantity = Integer.valueOf(quantityStr);
							stampBuy.setCantidad(quantity);
							
							if(!stampBuyTransfer.getStampIndicateChangeSystem()){
								String unitPriceStr = UBase64.base64Decode(stampBuyTransfer.getUnitPrice());
								BigDecimal unitPrice = UValue.bigDecimal(unitPriceStr);
								stampBuy.setPrecioUnitario(UValue.doubleValue(unitPrice));
								
								String salePriceStr = UBase64.base64Decode(stampBuyTransfer.getSalePrice());
								BigDecimal salePrice = UValue.bigDecimal(salePriceStr);
								stampBuy.setPrecioVenta(UValue.doubleValue(salePrice));
								
								String taxStr = UBase64.base64Decode(stampBuyTransfer.getTax());
								BigDecimal tax = UValue.bigDecimal(taxStr);
								stampBuy.setImpuesto(UValue.doubleValue(tax));
							}
							
							operationCode = OperationCode.ACTIVACION_TIMBRE_ID59;
							eventTypeCode = EventTypeCode.ACTIVACION_TIMBRE_ID34;
						}
						
						persistenceDAO.persist(stampBuy);
						
						System.out.println("-----------------------------------------------------------------------------------------------------------");
						System.out.println(userTools.log() + "[INFO] USER'S STAMP BUY REGISTERED");
						
						// Verificando si el usuario es Master
						if(!userTools.isMaster(this.user.getCorreoElectronico())){
							//##### Actualizando el perfil del usuario a Master
							PerfilEntity profile = userTools.getProfile(this.taxpayer);
							profile.setRol(iRoleJpaRepository.findByCodigo(ROL_CODE));
							persistenceDAO.update(profile);
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(userTools.log() + "[INFO] USER'S PROFILE UPDATED TO MASTER");
							
							//##### Actualizando el producto activo 
							this.taxpayer.setProductoServicioActivo(PRODUCT);
							persistenceDAO.update(this.taxpayer);
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(userTools.log() + "[INFO] USER'S ACTIVATED PRODUCT HAS CHANGED FROM 'MEGA-CFDI FREE' TO 'MEGA-CFDI PLUS'");
							
							//##### Registrando producto servicio
							ProductoServicioEntity productService = iProductServiceJpaRepository.findByCodigoAndHabilitadoTrue(PRODUCT);
							ContribuyenteProductoServicioEntity taxpayerProductService = new ContribuyenteProductoServicioEntity();
							taxpayerProductService.setContribuyente(this.taxpayer);
							taxpayerProductService.setProductoServicio(productService);
							taxpayerProductService.setHabilitado(Boolean.TRUE);
							persistenceDAO.persist(taxpayerProductService);
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(userTools.log() + "[INFO] USER'S PRODUCT SERVICE CREATED");
							
							//##### Registrando preferencias
							PreferenciasEntity preferences = new PreferenciasEntity();
							preferences.setContribuyente(this.taxpayer);
							preferences.setMoneda(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(CURRENCY_CODE));
							preferences.setEmisorXmlPdf(Boolean.FALSE);
							preferences.setEmisorXml(Boolean.FALSE);
							preferences.setEmisorPdf(Boolean.FALSE);
							preferences.setReceptorXmlPdf(Boolean.FALSE);
							preferences.setReceptorXml(Boolean.FALSE);
							preferences.setReceptorPdf(Boolean.FALSE);
							preferences.setContactoXmlPdf(Boolean.FALSE);
							preferences.setContactoXml(Boolean.FALSE);
							preferences.setContactoPdf(Boolean.FALSE);
							preferences.setRegistrarCliente(Boolean.TRUE);
							preferences.setActualizarCliente(Boolean.TRUE);
							preferences.setRegistrarConcepto(Boolean.TRUE);
							preferences.setActualizarConcepto(Boolean.TRUE);
							preferences.setRegistrarRegimenFiscal(Boolean.TRUE);
							preferences.setActualizarRegimenFiscal(Boolean.TRUE);
							preferences.setReutilizarFolioCancelado(Boolean.FALSE);
							preferences.setIncrementarFolioFinal(Boolean.FALSE);
							preferences.setRegistrarActualizarLugarExpedicion(Boolean.FALSE);
							preferences.setPrefacturaPdf(Boolean.FALSE);
							persistenceDAO.persist(preferences);
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(userTools.log() + "[INFO] USER'S PREFERENCES CREATED");
							
							//##### Registrando tipo de persona
							Integer rfcLength = this.taxpayer.getRfc().length();
							String emitterType = rfcLength == RFC_MIN_LENGTH ? PERSON_TYPE_MORAL_CODE : (rfcLength == RFC_MAX_LENGTH ? PERSON_TYPE_PHYSICAL_CODE : null);
							TipoPersonaEntity emitterTypeEntity = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(emitterType);
							if(UValidator.isNullOrEmpty(emitterTypeEntity)){
								ContribuyenteTipoPersonaEntity taxpayerPersonType = emitterTools.getPersonType();
								if(UValidator.isNullOrEmpty(taxpayerPersonType)){
									taxpayerPersonType = new ContribuyenteTipoPersonaEntity();
									taxpayerPersonType.setContribuyente(this.taxpayer);
									taxpayerPersonType.setTipoPersona(emitterTypeEntity);
									persistenceDAO.persist(taxpayerPersonType);
									
									System.out.println("-----------------------------------------------------------------------------------------------------------");
									System.out.println(userTools.log() + "[INFO] USER'S PERSON TYTPE CREATED");
								}
							}
							
							//##### Actualizando authority en spring security
							AuthoritiesEntity authority = iAuthoritiesJpaRepository.findByUsers_Username(this.user.getCorreoElectronico());
							iAuthoritiesJpaRepository.delete(authority);
							authority.setAuthority(ROL_CODE);
							persistenceDAO.update(authority);
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(userTools.log() + "[INFO] USER'S SPRING AUTHORITIES UPDATED");
							
							//##### Verificando si esta habilitada la cuenta del usuario en spring security
							UsersEntity userSpring = iUsersJpaRepository.findByUsernameAndEnabledFalse(this.user.getCorreoElectronico());
							if(!UValidator.isNullOrEmpty(userSpring)){
								userSpring.setEnabled(Boolean.TRUE);
								persistenceDAO.update(userSpring);
								
								System.out.println("-----------------------------------------------------------------------------------------------------------");
								System.out.println(userTools.log() + "[INFO] USER'S SPRING ENABLED");
							}
							
							// Bitacora :: Registro de accion en bitacora
							List<OperationCode> operationCodeSource = new ArrayList<>();						
							operationCodeSource.add(OperationCode.ACTUALIZACION_PERFIL_USUARIO_A_MASTER_ID22);
							operationCodeSource.add(OperationCode.ACTUALIZACION_PRODUCTO_ACTIVO_ID61);
							operationCodeSource.add(OperationCode.REGISTRO_PRODUCTO_ACTIVO_ID62);
							operationCodeSource.add(OperationCode.REGISTRO_PREFERENCIAS_ID63);
							operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_MASTER_TIPO_PERSONA_ID24);
							appLog.logOperation(operationCodeSource);
							
							// Eliminando la session activa del usuario
							sessionController.sessionDestroy(this.user);
							sessionDestroyed = true;
							
							this.credentialJson.getCredential().setActiveProduct(UBase64.base64Encode(this.taxpayer.getProductoServicioActivo()));
						}
						
						stampBuyTransfer.setNameBuy(UBase64.base64Encode(this.taxpayer.getNombreRazonSocial()));
						stampBuyTransfer.setRfcBuy(UBase64.base64Encode(this.taxpayer.getRfcActivo()));
						stampBuyTransfer.setDate(UDate.formattedSingleDate(stampBuy.getFecha()));
						stampBuyTransfer.setTime(UDate.formattedTime(stampBuy.getFecha()));
						this.credentialJson.setStampBuyTransfer(stampBuyTransfer);
						
						// Bitacora :: Registro de accion en bitacora
						appLog.logOperation(operationCode);
						
						// Timeline :: Registro de accion
						timelineLog.timelineLog(eventTypeCode);
						
						Toastr toastr = new Toastr();
						toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.buy.title", userTools.getLocale()));
						toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.buy.message", userTools.getLocale()));
						toastr.setType(Toastr.ToastrType.SUCCESS.value());
						
						notification.setToastrNotification(Boolean.TRUE);
						notification.setToastr(toastr);
						
						response.setNotification(notification);
						response.setSuccess(Boolean.TRUE);
					}else{
						response.setErrors(errors);
						response.setError(Boolean.TRUE);
					}
				}
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0121", userTools.getLocale()));
				response.setNotification(notification);
				response.setUnexpectedError(Boolean.TRUE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			
			if(!stampBuyTransfer.getStampIndicate()){
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0070", userTools.getLocale()));
				response.setNotification(notification);
				response.setUnexpectedError(Boolean.TRUE);
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0120", userTools.getLocale()));
				response.setNotification(notification);
				response.setUnexpectedError(Boolean.TRUE);
			}
			
		}
		
		if(!sessionDestroyed){
			sessionController.sessionUpdate();
		}
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		
		
		return jsonInString;
	}*/
	
	
	@RequestMapping(value="/managementUser", params={"stampPackageActivate"})
	public @ResponseBody String stampPackageActivate(HttpServletRequest request, @RequestParam(value="document") Optional<MultipartFile> documentParam) throws IOException{
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		JError error = new JError();
		List<JError> errors = new ArrayList<>();
		
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer(); 
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		Boolean sessionDestroyed = false;

		String objStampBuyTransfer = request.getParameter("objStampBuyTransfer");
		stampBuyTransfer = gson.fromJson(objStampBuyTransfer, JStampBuyTransfer.class);
		
		try {
			
			File document = null;
			String documentExtension = null;
			
			Boolean errorMarked = false;
			
			if(!UValidator.isNullOrEmpty(this.taxpayer)){
				
				if(!stampBuyTransfer.getStampIndicate() && UValidator.isNullOrEmpty(this.stampPackage)){
					notification.setPageMessage(Boolean.TRUE);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					notification.setMessage(UProperties.getMessage("ERR0070", userTools.getLocale()));
					response.setNotification(notification);
					response.setUnexpectedError(Boolean.TRUE);
					errorMarked = true;
				}else if(stampBuyTransfer.getStampIndicate()){
					if(UValidator.isNullOrEmpty(stampBuyTransfer.getQuantity())){
						errorMarked = true;
					}
					if(!stampBuyTransfer.getStampIndicateChangeSystem()){
						if(UValidator.isNullOrEmpty(stampBuyTransfer.getUnitPrice())){
							errorMarked = true;
						}
						if(UValidator.isNullOrEmpty(stampBuyTransfer.getSalePrice())){
							errorMarked = true;
						}
						if(UValidator.isNullOrEmpty(stampBuyTransfer.getTax())){
							errorMarked = true;
						}
					}					
				}
				if(errorMarked){
					if(!stampBuyTransfer.getStampIndicate()){
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(UProperties.getMessage("ERR0070", userTools.getLocale()));
						response.setNotification(notification);
						response.setUnexpectedError(Boolean.TRUE);
					}else{
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(UProperties.getMessage("ERR0120", userTools.getLocale()));
						response.setNotification(notification);
						response.setUnexpectedError(Boolean.TRUE);
					}
				}else{
					try {
						MultipartFile multipartFileDocument = documentParam.get();
						if (!multipartFileDocument.isEmpty()) {
							String documentName = multipartFileDocument.getOriginalFilename();
							documentExtension = UFile.getExtension(documentName);
							if(UValidator.isNullOrEmpty(documentExtension) || (!documentExtension.equalsIgnoreCase("jpg") && !documentExtension.equalsIgnoreCase("png") && !documentExtension.equalsIgnoreCase("pdf") && !documentExtension.equalsIgnoreCase("msg"))){
								String errorMessage = UProperties.getMessage("ERR1005", userTools.getLocale()).concat(" ").concat(UProperties.getMessage("ERR0125", userTools.getLocale()));
								error.setMessage(errorMessage);
								error.setField("stamp-reference-document");
								errors.add(error);
								errorMarked = true;
							}else{
								document = UFile.multipartFileToFile(multipartFileDocument);
							}
						}
					} catch (Exception e) {}
					
					String transactionId = UBase64.base64Decode(stampBuyTransfer.getTransactionId());
					if(!UValidator.isNullOrEmpty(transactionId)){
						error = new JError();
						TimbreCompraEntity stampByEntity = iStampBuyJpaRepository.findByIdTransaccion(transactionId);
						if(!UValidator.isNullOrEmpty(stampByEntity)){
							error.setMessage(UProperties.getMessage("ERR0126", userTools.getLocale()));
							error.setField("stamp-reference-transaction");
							errors.add(error);
							errorMarked = true;
						}
					}
					
					String reference = UBase64.base64Decode(stampBuyTransfer.getReference());
					if(!UValidator.isNullOrEmpty(reference)){
						if(!stampBuyTransfer.getStampIndicateChangeSystem()){
							error = new JError();
							TimbreCompraEntity stampByEntity = iStampBuyJpaRepository.findByReferencia(reference);
							if(!UValidator.isNullOrEmpty(stampByEntity)){
								error.setMessage(UProperties.getMessage("ERR0127", userTools.getLocale()));
								error.setField("stamp-reference-reference");
								errors.add(error);
								errorMarked = true;
							}
						}						
					}
					
					if(!errorMarked){
						System.out.println("-----------------------------------------------------------------------------------------------------------");
						if(!stampBuyTransfer.getStampIndicate()){
							userTools.log("[INFO] ACTIVATING STAMP PACKAGE...");
						}else{
							userTools.log("[INFO] ACTIVATING STAMP INDICATE...");
						}
						
						if(!UValidator.isNullOrEmpty(this.stamp)){
							Integer quantity = null;
							if(!stampBuyTransfer.getStampIndicate()){
								quantity = this.stampPackage.getCantidad();
								stampBuyTransfer.setQuantity(UBase64.base64Encode(quantity.toString()));
							}else{
								String quantityStr = UBase64.base64Decode(stampBuyTransfer.getQuantity());
								quantity = Integer.valueOf(quantityStr);
							}
							this.stamp.setTotal(this.stamp.getTotal() + quantity);
							this.stamp.setTimbresDisponibles(this.stamp.getTimbresDisponibles() + quantity);
							this.stamp.setHabilitado(Boolean.TRUE);
							persistenceDAO.update(this.stamp);
							
							userTools.log("[INFO] USER'S STAMP UPDATED");
						}else{
							Integer quantity = null;
							if(!stampBuyTransfer.getStampIndicate()){
								quantity = this.stampPackage.getCantidad();
								stampBuyTransfer.setQuantity(UBase64.base64Encode(quantity.toString()));
							}else{
								String quantityStr = UBase64.base64Decode(stampBuyTransfer.getQuantity());
								quantity = Integer.valueOf(quantityStr);
							}
							this.stamp = new TimbreEntity();
							this.stamp.setContribuyente(this.taxpayer);
							this.stamp.setHabilitado(Boolean.TRUE);
							this.stamp.setTimbresConsumidos(0);
							this.stamp.setTimbresDisponibles(quantity);
							this.stamp.setTimbresTransferidos(0);
							this.stamp.setTotal(quantity);
							persistenceDAO.persist(this.stamp);
							
							userTools.log("[INFO] USER'S STAMP CREATED");
						}
						
						JStamp stampJson = new JStamp();
						stampJson.setStampAvailable(UBase64.base64Encode(this.stamp.getTimbresDisponibles().toString()));
						stampJson.setStampSpent(UBase64.base64Encode(this.stamp.getTimbresConsumidos().toString()));
						stampJson.setEnabled(this.stamp.getHabilitado());
						
						Integer stampTransfered = !UValidator.isNullOrEmpty(this.stamp.getTimbresTransferidos()) ? this.stamp.getTimbresTransferidos() : 0; 
						Integer stampTotal = this.stamp.getTotal();
						
						stampJson.setStampTransfered(UBase64.base64Encode(stampTransfered.toString()));
						stampJson.setStampTotal(UBase64.base64Encode(stampTotal.toString()));

						this.credentialJson.setStamp(stampJson);
						
						OperationCode operationCode = null;
						EventTypeCode eventTypeCode = null;
						
						// Registrando la informacion de la compra
						TimbreCompraEntity stampBuy = new TimbreCompraEntity();
						stampBuy.setContribuyente(this.taxpayer);
						stampBuy.setFecha(new Date());
						stampBuy.setHora(new Date());
						
						if(!UValidator.isNullOrEmpty(transactionId)){
							stampBuy.setIdTransaccion(transactionId);
						}
						
						if(!UValidator.isNullOrEmpty(reference)){
							stampBuy.setReferencia(reference);
						}
						
						if(!UValidator.isNullOrEmpty(document)){
							String stampReferencePath = appSettings.getPropertyValue("cfdi.stamp.reference");
							String stampReferenceFolder = UDateTime.getLocalDateToday().toString().replaceAll("-", "");
							String directory = stampReferencePath + File.separator + stampReferenceFolder;
							
							String fileName = UValue.uuid().replaceAll("-", "").toLowerCase().concat(".").concat(documentExtension);
							String filePath = directory + File.separator + fileName;
							String dbPath = stampReferenceFolder + File.separator + fileName;
									
							try {
								
								File fileFolder = new File(directory); 
								if(!fileFolder.exists()){
									fileFolder.mkdirs();
								}
								
								UFile.writeFile(UFile.fileToByte(document), filePath);
								
								stampBuy.setDocumento(dbPath);
								
								stampBuyTransfer.setDocument(UBase64.base64Encode(fileName));
								stampBuyTransfer.setDocumentExtension(UBase64.base64Encode(documentExtension));
								
								String dbPathReplaced = dbPath.replaceAll(Pattern.quote("\\"), "_").replaceAll(Pattern.quote("/"), "_");
								stampBuyTransfer.setDocumentPath(UBase64.base64Encode(dbPathReplaced));
								
							} catch (Exception e) {}
						}
						
						String description = UBase64.base64Decode(stampBuyTransfer.getDescription());
						if(!UValidator.isNullOrEmpty(description)){
							stampBuy.setDescripcion(description);
						}
						
						if(!stampBuyTransfer.getStampIndicate()){
							stampBuy.setTimbrePaquete(this.stampPackage);
							
							operationCode = OperationCode.ACTIVACION_PAQUETE_TIMBRE_ID12;
							eventTypeCode = EventTypeCode.ACTIVACION_PAQUETE_TIMBRE_ID1;
							
						}else{
							String quantityStr = UBase64.base64Decode(stampBuyTransfer.getQuantity());
							Integer quantity = Integer.valueOf(quantityStr);
							stampBuy.setCantidad(quantity);
							
							if(!stampBuyTransfer.getStampIndicateChangeSystem()){
								String unitPriceStr = UBase64.base64Decode(stampBuyTransfer.getUnitPrice());
								BigDecimal unitPrice = UValue.bigDecimal(unitPriceStr);
								stampBuy.setPrecioUnitario(UValue.doubleValue(unitPrice));
								
								String salePriceStr = UBase64.base64Decode(stampBuyTransfer.getSalePrice());
								BigDecimal salePrice = UValue.bigDecimal(salePriceStr);
								stampBuy.setPrecioVenta(UValue.doubleValue(salePrice));
								
								String taxStr = UBase64.base64Decode(stampBuyTransfer.getTax());
								BigDecimal tax = UValue.bigDecimal(taxStr);
								stampBuy.setImpuesto(UValue.doubleValue(tax));
							}
							
							operationCode = OperationCode.ACTIVACION_TIMBRE_ID59;
							eventTypeCode = EventTypeCode.ACTIVACION_TIMBRE_ID34;
						}
						
						persistenceDAO.persist(stampBuy);
						
						userTools.log("[INFO] USER'S STAMP BUY REGISTERED");
						
						// Verificando si el usuario es Master
						if(!userTools.isMaster(this.user.getCorreoElectronico())){
							//##### Actualizando el perfil del usuario a Master
							PerfilEntity profile = userTools.getProfile(this.taxpayer);
							profile.setRol(iRoleJpaRepository.findByCodigo(ROL_CODE));
							persistenceDAO.update(profile);
							
							userTools.log("[INFO] USER'S PROFILE UPDATED TO MASTER");
							
							//##### Actualizando el producto activo 
							this.taxpayer.setProductoServicioActivo(PRODUCT);
							persistenceDAO.update(this.taxpayer);
							
							userTools.log("[INFO] USER'S ACTIVATED PRODUCT HAS CHANGED FROM 'MEGA-CFDI FREE' TO 'MEGA-CFDI PLUS'");
							
							//##### Registrando producto servicio
							ProductoServicioEntity productService = iProductServiceJpaRepository.findByCodigoAndHabilitadoTrue(PRODUCT);
							ContribuyenteProductoServicioEntity taxpayerProductService = new ContribuyenteProductoServicioEntity();
							taxpayerProductService.setContribuyente(this.taxpayer);
							taxpayerProductService.setProductoServicio(productService);
							taxpayerProductService.setHabilitado(Boolean.TRUE);
							persistenceDAO.persist(taxpayerProductService);
							
							userTools.log("[INFO] USER'S PRODUCT SERVICE CREATED");
							
							//##### Registrando preferencias
							PreferenciasEntity preferencias = iPreferencesJpaRepository.findByContribuyente(taxpayer);
							if(UValidator.isNullOrEmpty(preferencias)) {
								PreferenciasEntity preferences = new PreferenciasEntity();
								preferences.setContribuyente(this.taxpayer);
								preferences.setMoneda(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(CURRENCY_CODE));
								preferences.setEmisorXmlPdf(Boolean.FALSE);
								preferences.setEmisorXml(Boolean.FALSE);
								preferences.setEmisorPdf(Boolean.FALSE);
								preferences.setReceptorXmlPdf(Boolean.FALSE);
								preferences.setReceptorXml(Boolean.FALSE);
								preferences.setReceptorPdf(Boolean.FALSE);
								preferences.setContactoXmlPdf(Boolean.FALSE);
								preferences.setContactoXml(Boolean.FALSE);
								preferences.setContactoPdf(Boolean.FALSE);
								preferences.setRegistrarCliente(Boolean.TRUE);
								preferences.setActualizarCliente(Boolean.TRUE);
								preferences.setRegistrarConcepto(Boolean.TRUE);
								preferences.setActualizarConcepto(Boolean.TRUE);
								preferences.setRegistrarRegimenFiscal(Boolean.TRUE);
								preferences.setActualizarRegimenFiscal(Boolean.TRUE);
								preferences.setReutilizarFolioCancelado(Boolean.FALSE);
								preferences.setIncrementarFolioFinal(Boolean.FALSE);
								preferences.setRegistrarActualizarLugarExpedicion(Boolean.FALSE);
								preferences.setPrefacturaPdf(Boolean.FALSE);
								persistenceDAO.persist(preferences);
							
							
								userTools.log("[INFO] USER'S PREFERENCES CREATED");
							}
							
							//##### Registrando tipo de persona
							Integer rfcLength = this.taxpayer.getRfc().length();
							String emitterType = rfcLength == RFC_MIN_LENGTH ? PERSON_TYPE_MORAL_CODE : (rfcLength == RFC_MAX_LENGTH ? PERSON_TYPE_PHYSICAL_CODE : null);
							TipoPersonaEntity emitterTypeEntity = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(emitterType);
							if(UValidator.isNullOrEmpty(emitterTypeEntity)){
								ContribuyenteTipoPersonaEntity taxpayerPersonType = emitterTools.getPersonType();
								if(UValidator.isNullOrEmpty(taxpayerPersonType)){
									taxpayerPersonType = new ContribuyenteTipoPersonaEntity();
									taxpayerPersonType.setContribuyente(this.taxpayer);
									taxpayerPersonType.setTipoPersona(emitterTypeEntity);
									persistenceDAO.persist(taxpayerPersonType);
									
									userTools.log("[INFO] USER'S PERSON TYTPE CREATED");
								}
							}
							
							//##### Actualizando authority del usuario
							val role = iRoleJpaRepository.findByCodigo(ROL_CODE);
							user.setRol(role);
							if (!user.isActivo()) {
								user.setActivo(true);
							}
							persistenceDAO.update(user);
							
							userTools.log("[INFO] USER'S AUTHORITIES UPDATED");
							
							// Bitacora :: Registro de accion en bitacora
							List<OperationCode> operationCodeSource = new ArrayList<>();						
							operationCodeSource.add(OperationCode.ACTUALIZACION_PERFIL_USUARIO_A_MASTER_ID22);
							operationCodeSource.add(OperationCode.ACTUALIZACION_PRODUCTO_ACTIVO_ID61);
							operationCodeSource.add(OperationCode.REGISTRO_PRODUCTO_ACTIVO_ID62);
							operationCodeSource.add(OperationCode.REGISTRO_PREFERENCIAS_ID63);
							operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_MASTER_TIPO_PERSONA_ID24);
							appLog.logOperation(operationCodeSource);
							
							// Eliminando la session activa del usuario
							sessionController.sessionDestroy(this.user);
							sessionDestroyed = true;
							
							this.credentialJson.getCredential().setActiveProduct(UBase64.base64Encode(this.taxpayer.getProductoServicioActivo()));
						}
						
						stampBuyTransfer.setNameBuy(UBase64.base64Encode(this.taxpayer.getNombreRazonSocial()));
						stampBuyTransfer.setRfcBuy(UBase64.base64Encode(this.taxpayer.getRfcActivo()));
						stampBuyTransfer.setDate(UDate.formattedSingleDate(stampBuy.getFecha()));
						stampBuyTransfer.setTime(UDate.formattedTime(stampBuy.getFecha()));
						this.credentialJson.setStampBuyTransfer(stampBuyTransfer);
						
						// Bitacora :: Registro de accion en bitacora
						appLog.logOperation(operationCode);
						
						// Timeline :: Registro de accion
						timelineLog.timelineLog(eventTypeCode);
						
						Toastr toastr = new Toastr();
						toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.buy.title", userTools.getLocale()));
						toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.buy.message", userTools.getLocale()));
						toastr.setType(Toastr.ToastrType.SUCCESS.value());
						
						notification.setToastrNotification(Boolean.TRUE);
						notification.setToastr(toastr);
						
						response.setNotification(notification);
						response.setSuccess(Boolean.TRUE);
					}else{
						response.setErrors(errors);
						response.setError(Boolean.TRUE);
					}
				}
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0121", userTools.getLocale()));
				response.setNotification(notification);
				response.setUnexpectedError(Boolean.TRUE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			
			if(!stampBuyTransfer.getStampIndicate()){
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0070", userTools.getLocale()));
				response.setNotification(notification);
				response.setUnexpectedError(Boolean.TRUE);
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0120", userTools.getLocale()));
				response.setNotification(notification);
				response.setUnexpectedError(Boolean.TRUE);
			}
			
		}
		
		if(!sessionDestroyed){
			sessionController.sessionUpdate();
		}
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		return jsonInString;
	}
	
	@ResponseBody
	@RequestMapping(value="/stampReferenceDownload/{document}", method=RequestMethod.GET)
	public ModelAndView stampReferenceDownload(HttpServletRequest request, HttpServletResponse response, @PathVariable(value="document") String document) throws Exception {
		try {
			
			String repositoryPath = appSettings.getPropertyValue("cfdi.stamp.reference");
			
			String filePath = UBase64.base64Decode(document);
			filePath = filePath.replaceAll("_", "\\\\");
					
			TimbreCompraEntity stampBuyEntity = iStampBuyJpaRepository.findByDocumento(filePath);
			if(stampBuyEntity != null){
				String absolutePath = repositoryPath + File.separator + stampBuyEntity.getDocumento();
				
				File file = new File(absolutePath);
				byte[] fileByte = UFile.fileToByte(file);
				
				response.setContentType("application/xml");
		        response.setContentLength(fileByte.length);
		        response.setHeader("Content-Disposition","attachment; filename=\"" + file.getName() +"\"");
		 
		        FileCopyUtils.copy(fileByte, response.getOutputStream());
			}
	 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		return null;
    }
	
	//###############################################################################################
	//##### Transferir timbres ######################################################################
	//###############################################################################################
	@Layout(value = "layouts/admon")
	@RequestMapping(value = "/managementUser", params={"stampTransferInit"})
	public @ResponseBody String stampTransferInit(HttpServletRequest request) throws IOException {
		
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer();
		JStamp stamp = new JStamp();
			
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			String options = "<option value=\"-\">&laquo;Seleccione&raquo;</option>";
			List<StampTransferOptions> stampTransferOptions = new ArrayList<>();
			
			Boolean forceNotification = false;
			String forceNotificationMessage = null;
			
			String credential = UBase64.base64Decode(request.getParameter("credential"));
			if(!UValidator.isNullOrEmpty(credential)){
				this.populate(credential);
				if(!UValidator.isNullOrEmpty(this.credential) && !UValidator.isNullOrEmpty(this.user) && !UValidator.isNullOrEmpty(this.taxpayer)){
					// Cargando la informacion de los timbres de la cuenta RFC seleccionada
					this.stamp = iStampJpaRepository.findByContribuyente(this.taxpayer);
					
					if(!UValidator.isNullOrEmpty(this.stamp)){
						stamp.setStampAvailable(UBase64.base64Encode(this.stamp.getTimbresDisponibles().toString()));
					}else{
						stamp = null;
						forceNotification = true;
						forceNotificationMessage = UProperties.getMessage("ERR0122", userTools.getLocale());
					}
					
					// Cargando las cuentas asociadas del usuario
					this.accounts = new ArrayList<>();
					this.accounts.addAll(iAccountJpaRepository.findByContribuyenteAndEliminadoFalse(this.taxpayer));
					if(!this.accounts.isEmpty()){
						for (CuentaEntity item : this.accounts) {
							String option = "<option value="+UBase64.base64Encode(item.getContribuyenteAsociado().getId().toString())+">"+item.getContribuyenteAsociado().getRfc()+"</option>";
							
							if(!UValidator.isNullOrEmpty(options)){
								options += option;
							}else{
								options = option;
							}
							
							StampTransferOptions sto = new StampTransferOptions();
							sto.setId(UBase64.base64Encode(item.getId().toString()));
							sto.setValue(UBase64.base64Encode(item.getContribuyenteAsociado().getRfc()));
							stampTransferOptions.add(sto);
						}
					}else{
						if(!forceNotification){
							stamp = null;
							forceNotification = true;
							forceNotificationMessage = UProperties.getMessage("ERR0123", userTools.getLocale());
						}
					}
					
					if(forceNotification){
						notification.setMessage(forceNotificationMessage);
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.WARNING.value());
						response.setNotification(notification);
					}
					
					stampBuyTransfer.setRfcMatserAccount(UBase64.base64Encode(this.taxpayer.getRfc()));
					stampBuyTransfer.setOptions(UBase64.base64Encode(options));
					stampBuyTransfer.getStampTransferOptions().addAll(stampTransferOptions);
					stampBuyTransfer.setStamp(stamp);
					this.credentialJson.setStampBuyTransfer(stampBuyTransfer);
					
					response.setSuccess(Boolean.TRUE);
				}else{
					notification.setPageMessage(Boolean.TRUE);
					notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setError(Boolean.TRUE);
					response.setNotification(notification);
				}
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setError(Boolean.TRUE);
				response.setNotification(notification);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setError(Boolean.TRUE);
			response.setNotification(notification);
		}
		
		sessionController.sessionUpdate();
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		return jsonInString;
	}
	
	@RequestMapping(value="/managementUser", params={"loadStampTransfer"})
	public @ResponseBody String loadStampTransfer(HttpServletRequest request) throws IOException{
		
		JResponse response = new JResponse();
		
		JStamp stamp = new JStamp();
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			Long id = null;
			CuentaEntity account = null;
			String idStr = UBase64.base64Decode(request.getParameter("accountIndex"));
			
			if(!UValidator.isNullOrEmpty(idStr)){
				id = UValue.longValue(idStr);
				account = iAccountJpaRepository.findByContribuyenteAsociado_IdAndEliminadoFalse(id);
				if(!UValidator.isNullOrEmpty(account)){
					ContribuyenteEntity taxpayerAssociated = iTaxpayerJpaRepository.findById(id).orElse(null);
					if(!UValidator.isNullOrEmpty(taxpayerAssociated)){
						TimbreEntity stampEntity = iStampJpaRepository.findByContribuyente(taxpayerAssociated);
						if(!UValidator.isNullOrEmpty(stampEntity)){
							stamp.setStampAvailable(UBase64.base64Encode(stampEntity.getTimbresDisponibles().toString()));
						}else{
							stamp.setStampAvailable(UBase64.base64Encode("0"));
						}						
						stampBuyTransfer.setStamp(stamp);
						
						response.setSuccess(Boolean.TRUE);
					}else{
						error.setField("stamp-transfer-account");
						error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
						errors.add(error);
					}
				}else{
					error.setField("stamp-transfer-account");
					error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
					errors.add(error);
				}
			}else{
				error.setField("stamp-transfer-account");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			error.setField("stamp-transfer-account");
			error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
			errors.add(error);
		}
		
		if(!errors.isEmpty()){
			response.setError(Boolean.TRUE);
			response.setErrors(errors);
		}
		stampBuyTransfer.setResponse(response);
		
		sessionController.sessionUpdate();
		
		jsonInString = gson.toJson(stampBuyTransfer);
		return jsonInString;
	}
	
	@RequestMapping(value="/managementUser", params={"stampTransfer"})
	public @ResponseBody String stampTransfer(HttpServletRequest request) throws IOException{
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		JStamp stamp = new JStamp();
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			TimbreEntity masterStampEntity = null;
			TimbreEntity associatedStampEntity = null;
			
			String stampBuyTransferObj = request.getParameter("stampBuyTransfer");
			stampBuyTransfer = gson.fromJson(stampBuyTransferObj, JStampBuyTransfer.class);
			
			String masterRfc = UBase64.base64Decode(stampBuyTransfer.getRfcMatserAccount());
			if(UValidator.isNullOrEmpty(masterRfc)){
				error.setField("stamp-transfer-master");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}else if(!masterRfc.equalsIgnoreCase(this.taxpayer.getRfc())){
				error.setField("stamp-transfer-master");
				error.setMessage(UProperties.getMessage("ERR0001", userTools.getLocale()));
				errors.add(error);
			}
			
			Long associatedId = null;
			CuentaEntity account = null;
			ContribuyenteEntity taxpayerAssociated = null;
			
			String associatedIdStr = UBase64.base64Decode(stampBuyTransfer.getAssociatedAccountId());
			error = new JError();
			
			if(!UValidator.isNullOrEmpty(associatedIdStr)){
				associatedId = UValue.longValue(associatedIdStr);
				account = iAccountJpaRepository.findByContribuyenteAsociado_IdAndEliminadoFalse(associatedId);
				if(!UValidator.isNullOrEmpty(account)){
					taxpayerAssociated = iTaxpayerJpaRepository.findById(associatedId).orElse(null);
					if(!UValidator.isNullOrEmpty(taxpayerAssociated)){
						Boolean finded = false;
						for (CuentaEntity item : this.accounts) {
							if(item.getContribuyenteAsociado().getRfc().equalsIgnoreCase(account.getContribuyenteAsociado().getRfc())){
								finded = Boolean.TRUE;
								break;
							}
						}
						if(!finded){
							error.setField("stamp-transfer-account");
							error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
							errors.add(error);
						}
					}else{
						error.setField("stamp-transfer-account");
						error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
						errors.add(error);
					}
				}else{
					error.setField("stamp-transfer-account");
					error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
					errors.add(error);
				}
			}else{
				error.setField("stamp-transfer-account");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}
			
			stamp = stampBuyTransfer.getStamp();
			
			String stampAvailableStr = UBase64.base64Decode(stamp.getStampAvailable());
			Integer stampAvailable = null;
			error = new JError();
			
			if(UValidator.isNullOrEmpty(stampAvailableStr)){
				error.setField("stamp-transfer-available");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}else{
				stampAvailable = Integer.valueOf(stampAvailableStr);
			}
			
			String transferAmountStr = UBase64.base64Decode(stampBuyTransfer.getTransferAmount());
			Integer transferAmount = null;
			error = new JError();
			
			if(UValidator.isNullOrEmpty(transferAmountStr)){
				error.setField("stamp-transfer-amount");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}else{
				transferAmount = Integer.valueOf(transferAmountStr);
			}
			
			Boolean transferMasterToAccount = stampBuyTransfer.getTransferMasterToAccount();
			Boolean transferAccountToMaster = stampBuyTransfer.getTransferAccountToMaster();
			
			Boolean errorMarked = false;
			
			if(errors.isEmpty()){
				if(transferMasterToAccount){
					masterStampEntity = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
					Integer masterStampAvailable = masterStampEntity.getTimbresDisponibles();
					error = new JError();
					
					if(masterStampAvailable == 0){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0115", userTools.getLocale()));
						errorMarked = true;
					}else if(!masterStampAvailable.toString().equals(stampAvailable.toString())){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0116", userTools.getLocale()));
						errorMarked = true;
					}else if(transferAmount > stampAvailable){
						error.setField("stamp-transfer-amount");
						error.setMessage(UProperties.getMessage("ERR0117", userTools.getLocale()));
						errorMarked = true;
					}else if(stampAvailable - transferAmount < 0){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0118", userTools.getLocale()));
						errorMarked = true;
					}
				}else if(transferAccountToMaster){
					associatedStampEntity = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayerAssociated);
					Integer associatedStampAvailable = associatedStampEntity.getTimbresDisponibles();
					error = new JError();
					
					if(associatedStampAvailable == 0){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0115", userTools.getLocale()));
						errorMarked = true;
					}else if(!associatedStampAvailable.toString().equals(stampAvailable.toString())){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0116", userTools.getLocale()));
						errorMarked = true;
					}else if(transferAmount > stampAvailable){
						error.setField("stamp-transfer-amount");
						error.setMessage(UProperties.getMessage("ERR0117", userTools.getLocale()));
						errorMarked = true;
					}else if(stampAvailable - transferAmount < 0){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0119", userTools.getLocale()));
						errorMarked = true;
					}
				}
				if(errorMarked){
					errors.add(error);
					response.setError(Boolean.TRUE);
					response.setErrors(errors);
				}else {
					OperationCode operationCode = null;
					EventTypeCode eventTypeCode = null;
					
					String transferType = null;
					
					stampAvailable = stampAvailable - transferAmount;
					
					if(transferMasterToAccount){
						masterStampEntity.setTimbresDisponibles(stampAvailable);
						if(!UValidator.isNullOrEmpty(masterStampEntity.getTimbresTransferidos())){
							masterStampEntity.setTimbresTransferidos(masterStampEntity.getTimbresTransferidos() + transferAmount);
						}else{
							masterStampEntity.setTimbresTransferidos(transferAmount);
						}
						//masterStampEntity.setTotal(masterStampEntity.getTotal() - transferAmount);
						masterStampEntity.setHabilitado(stampAvailable > 0);
						persistenceDAO.update(masterStampEntity);
						
						associatedStampEntity = iStampJpaRepository.findByContribuyente(taxpayerAssociated);
						if(!UValidator.isNullOrEmpty(associatedStampEntity)){
							Integer associatedStampAvailable = associatedStampEntity.getTimbresDisponibles() + transferAmount;
							associatedStampEntity.setTimbresDisponibles(associatedStampAvailable);
							associatedStampEntity.setTotal(associatedStampEntity.getTotal() + transferAmount );
							associatedStampEntity.setHabilitado(Boolean.TRUE);
							persistenceDAO.update(associatedStampEntity);
						}else{
							associatedStampEntity = new TimbreEntity();
							associatedStampEntity.setContribuyente(taxpayerAssociated);
							associatedStampEntity.setHabilitado(Boolean.TRUE);
							associatedStampEntity.setTimbresConsumidos(0);
							associatedStampEntity.setTimbresDisponibles(transferAmount);
							associatedStampEntity.setTimbresTransferidos(0);
							associatedStampEntity.setTotal(transferAmount);
							persistenceDAO.persist(associatedStampEntity);
						}
						transferType = UBase64.base64Encode(UProperties.getMessage("mega.cfdi.information.stamp.transfer.master.to.account", userTools.getLocale()));
						
						operationCode = OperationCode.TRANSFERENCIA_TIMBRE_ID13;
						eventTypeCode = EventTypeCode.TRANSFERENCIA_TIMBRE_ID2;
						
					}else if(transferAccountToMaster){
						associatedStampEntity.setTimbresDisponibles(stampAvailable);
						if(!UValidator.isNullOrEmpty(associatedStampEntity.getTimbresTransferidos())){
							associatedStampEntity.setTimbresTransferidos(associatedStampEntity.getTimbresTransferidos() + transferAmount);
						}else{
							associatedStampEntity.setTimbresTransferidos(transferAmount);
						}
						//associatedStampEntity.setTotal(associatedStampEntity.getTotal() - transferAmount);
						associatedStampEntity.setHabilitado(stampAvailable > 0);
						persistenceDAO.update(associatedStampEntity);
						
						masterStampEntity = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
						Integer masterStampAvailable = masterStampEntity.getTimbresDisponibles() + transferAmount;
						masterStampEntity.setTimbresDisponibles(masterStampAvailable);
						masterStampEntity.setTotal(masterStampEntity.getTotal() + transferAmount );
						masterStampEntity.setHabilitado(Boolean.TRUE);
						persistenceDAO.update(masterStampEntity);
						
						transferType = UBase64.base64Encode(UProperties.getMessage("mega.cfdi.information.stamp.transfer.account.to.master", userTools.getLocale()));
						
						operationCode = OperationCode.TRANSFERENCIA_TIMBRE_CUENTA_MASTER_ID60;
						eventTypeCode = EventTypeCode.TRANSFERENCIA_TIMBRE_CUENTA_MASTER_ID35;
					}
					
					TimbreTransferenciaEntity stampTransferEntity = new TimbreTransferenciaEntity();
					stampTransferEntity.setContribuyente(this.taxpayer);
					stampTransferEntity.setCuenta(account);
					stampTransferEntity.setCantidad(transferAmount);
					stampTransferEntity.setFecha(new Date());
					stampTransferEntity.setHora(new Date());
					stampTransferEntity.setTransferenciaCuentaMaster(transferAccountToMaster);
					persistenceDAO.persist(stampTransferEntity);
					
					//##### Cargando informacion de timbres
					this.stamp = iStampJpaRepository.findByContribuyente(this.taxpayer);
					
					Integer stampTransfered = !UValidator.isNullOrEmpty(this.stamp.getTimbresTransferidos()) ? this.stamp.getTimbresTransferidos() : 0; 
					Integer stampTotal = this.stamp.getTotal();
					
					stamp.setStampAvailable(UBase64.base64Encode(this.stamp.getTimbresDisponibles().toString()));
					stamp.setStampSpent(UBase64.base64Encode(this.stamp.getTimbresConsumidos().toString()));
					stamp.setEnabled(this.stamp.getHabilitado());
					stamp.setStampTransfered(UBase64.base64Encode(stampTransfered.toString()));
					stamp.setStampTotal(UBase64.base64Encode(stampTotal.toString()));
					stampBuyTransfer.setStamp(stamp);
					
					stampBuyTransfer.setTransferType(transferType);
					stampBuyTransfer.setRfcAssociatedAccount(UBase64.base64Encode(account.getContribuyenteAsociado().getRfc().toUpperCase()));
					this.credentialJson.setStampBuyTransfer(stampBuyTransfer);
					
					// Bitacora :: Registro de accion en bitacora
					appLog.logOperation(operationCode);
					
					// Timeline :: Registro de accion
					timelineLog.timelineLog(eventTypeCode);
					
					Toastr toastr = new Toastr();
					toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.transfer.title", userTools.getLocale()));
					toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.transfer.message", userTools.getLocale()));
					toastr.setType(Toastr.ToastrType.SUCCESS.value());
					
					notification.setToastrNotification(Boolean.TRUE);
					notification.setToastr(toastr);
					
					response.setNotification(notification);
					response.setSuccess(Boolean.TRUE);
				}
			}else{
				errors.add(error);
				response.setError(Boolean.TRUE);
				response.setErrors(errors);
			}

		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(UProperties.getMessage("ERR0114", userTools.getLocale()));
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		return jsonInString;
	}
	
	//###############################################################################################
	//##### Consignatarios ##########################################################################
	//###############################################################################################
	@Layout(value = "layouts/admon")
	@RequestMapping(value = "/managementUser", params={"consignee"})
	public @ResponseBody String consignee(HttpServletRequest request) throws IOException {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		JConsignee consigneeJson = new JConsignee();
		
		try {
			
			String credential = UBase64.base64Decode(request.getParameter("credential"));
			if(!UValidator.isNullOrEmpty(credential)){
				this.populate(credential);
				if(!UValidator.isNullOrEmpty(this.credential) && !UValidator.isNullOrEmpty(this.user) && !UValidator.isNullOrEmpty(this.taxpayer)){
					ConsignatarioEntity consignee = userTools.consigneeByContribuyente(this.taxpayer);
					if(!UValidator.isNullOrEmpty(consignee)){
						//##### Cargando informacion de consignatario
						Consignee consigneeObj = new Consignee();
						consigneeObj.setIdTaxpayer(UBase64.base64Encode(consignee.getId().toString()));
						consigneeObj.setIdTaxpayer(UBase64.base64Encode(consignee.getContribuyente().getId().toString()));
						consigneeObj.setContractStartDate(UBase64.base64Encode(UDate.formattedDate(consignee.getFechaInicioContrato(), DATE_FORMAT)));
						if(!UValidator.isNullOrEmpty(consignee.getFechaRenovacionContrato())){
							consigneeObj.setContractRenewalDate(UBase64.base64Encode(UDate.formattedDate(consignee.getFechaRenovacionContrato(), DATE_FORMAT)));
						}
						consigneeObj.setContractEndDate(UBase64.base64Encode(UDate.formattedDate(consignee.getFechaFinContrato(), DATE_FORMAT)));
						consigneeObj.setActive(consignee.getActivo());
						consigneeObj.setContractPeriodEnded(consignee.getContratoFinalizado());
						
						String consigneeContractActiveName = null;
						//##### Cargando informacion de los contratos del consignatario
						Sort sort = Sort.by(Sort.Order.desc("id"));
						Iterable<ConsignatarioContratoEntity> consigneeContracts = iConsigneeContractJpaRepository.findAll(sort);
						for (ConsignatarioContratoEntity item : consigneeContracts) {
							ConsigneeContract contract = new ConsigneeContract();
							contract.setId(UBase64.base64Encode(item.getId().toString()));
							contract.setIdConsignee(UBase64.base64Encode(item.getConsignatario().getId().toString()));
							contract.setContractStartDate(UBase64.base64Encode(UDate.formattedDate(item.getFechaInicioContrato(), DATE_FORMAT)));
							contract.setContractEndDate(UBase64.base64Encode(UDate.formattedDate(item.getFechaFinContrato(), DATE_FORMAT)));
							contract.setDocument(UBase64.base64Encode(item.getNombreContrato()));
							contract.setDocumentExtension(UBase64.base64Encode(item.getExtensionContrato()));
							contract.setActive(item.getActivo());
							String dbPathReplaced = item.getRutaContrato().replaceAll(Pattern.quote("\\"), "_").replaceAll(Pattern.quote("/"), "_");
							contract.setContractPath(UBase64.base64Encode(dbPathReplaced));
							consigneeObj.getConsigneeContracts().add(contract);
							
							if(item.getActivo()){
								consigneeContractActiveName = UBase64.base64Encode(item.getNombreContrato());
							}
						}
						consigneeObj.setContractName(consigneeContractActiveName);
						
						consigneeJson.setConsignee(consigneeObj);
						this.credentialJson.setConsignee(consigneeJson);
					}
					response.setSuccess(Boolean.TRUE);
				}else{
					notification.setPageMessage(Boolean.TRUE);
					notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setError(Boolean.TRUE);
					response.setNotification(notification);
				}
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setError(Boolean.TRUE);
				response.setNotification(notification);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setMessage(UProperties.getMessage("ERR1010", userTools.getLocale()));
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setError(Boolean.TRUE);
			response.setNotification(notification);
		}

		sessionController.sessionUpdate();
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		return jsonInString;
	}
	
	@Transactional
	@RequestMapping(value="/managementUser", params={"consigneeCreateEdit"})	
	public @ResponseBody String consigneeCreateEdit(HttpServletRequest request, @RequestParam(value="document") Optional<MultipartFile> documentParam) throws IOException{
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		JError error = new JError();
		List<JError> errors = new ArrayList<>();
		
		JConsignee consigneeJson = new JConsignee();
		Consignee consigneeObj = new Consignee();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		String action = null;
		String errorMessage = null;
		String toastrTitle = null;
		String toastrMessage = null;
		
		try {
			
			String consigneeRequest = request.getParameter("consigneeRequest");
			consigneeObj = gson.fromJson(consigneeRequest, Consignee.class);
			
			action = UBase64.base64Decode(request.getParameter("action"));
			errorMessage = action.equals("register") ? UProperties.getMessage("ERR0175", userTools.getLocale()) : UProperties.getMessage("ERR0176", userTools.getLocale());
			
			File document = null;
			String documentExtension = null;
			
			String folderPath = null;
			String consigneePath = null;
			String directory = null;
			
			String fileName = null;
			String filePath = null;
			String dbPath = null;
			
			Boolean errorMarked = false;
			
			if(!UValidator.isNullOrEmpty(this.taxpayer)){
				ConsignatarioEntity consigneeEntity = null;
				if(action.equals("register")){
					consigneeEntity = userTools.consigneeByContribuyente(this.taxpayer);
					if(!UValidator.isNullOrEmpty(consigneeEntity)){
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(UProperties.getMessage("ERR0177", userTools.getLocale()));
						response.setNotification(notification);
						response.setUnexpectedError(Boolean.TRUE);
						errorMarked = true;
					}
				}else if(action.equals("renewal") || action.equals("edit")){
					consigneeEntity = userTools.consigneeByContribuyente(this.taxpayer);
					if(UValidator.isNullOrEmpty(consigneeEntity)){
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(UProperties.getMessage("ERR0174", userTools.getLocale()));
						response.setNotification(notification);
						response.setUnexpectedError(Boolean.TRUE);
						errorMarked = true;
					}
				}
				
				if(!errorMarked){
					Boolean active = consigneeObj.isActive();
					
					Date contractStartDate = null;
					String startDate = UBase64.base64Decode(consigneeObj.getContractStartDate());
					if(action.equals("register")){
						contractStartDate = UDate.formattedDate(startDate, DATE_FORMAT);
					}
					
					Date contractRenewalDate = null;
					String renewalDate = UBase64.base64Decode(consigneeObj.getContractRenewalDate());
					if(action.equals("renewal")){
						contractRenewalDate = UDate.formattedDate(renewalDate, DATE_FORMAT);
					}
					
					String endDate = UBase64.base64Decode(consigneeObj.getContractEndDate());
					Date contractEndDate = UDate.formattedDate(endDate, DATE_FORMAT);
					
					if(action.equals("register")){
						if(!endDate.equals(consigneeContractPeriod(startDate))){
							errorMarked = true;
						}
					}else if(action.equals("renewal")){
						if(!endDate.equals(consigneeContractPeriod(renewalDate))){
							errorMarked = true;
						}
					}
					
					if(errorMarked){
						error.setMessage(UProperties.getMessage("ERR0181", userTools.getLocale()));
						error.setField("consignee-contract-end-date");
						errors.add(error);
						
						response.setErrors(errors);
						response.setError(Boolean.TRUE);
					}else{
						if(action.equals("register") || action.equals("renewal")){
							try {
								
								MultipartFile multipartFileDocument = documentParam.get();
								if (!multipartFileDocument.isEmpty()) {
									String documentName = multipartFileDocument.getOriginalFilename();
									documentExtension = UFile.getExtension(documentName);
									if(UValidator.isNullOrEmpty(documentExtension) || (!documentExtension.equalsIgnoreCase("jpg") && !documentExtension.equalsIgnoreCase("png") && !documentExtension.equalsIgnoreCase("pdf"))){
										errorMessage = UProperties.getMessage("ERR1005", userTools.getLocale()).concat(" ").concat(UProperties.getMessage("ERR0178", userTools.getLocale()));
										error.setMessage(errorMessage);
										error.setField("consignee-contract-document");
										errors.add(error);
										errorMarked = true;
									}else{
										document = UFile.multipartFileToFile(multipartFileDocument);
									}
								}else{
									notification.setPageMessage(Boolean.TRUE);
									notification.setCssStyleClass(CssStyleClass.ERROR.value());
									notification.setMessage(UProperties.getMessage("ERR0179", userTools.getLocale()));
									response.setNotification(notification);
									response.setUnexpectedError(Boolean.TRUE);
									
									errorMarked = true;
								}
								
							} catch (Exception e) {
								notification.setPageMessage(Boolean.TRUE);
								notification.setCssStyleClass(CssStyleClass.ERROR.value());
								notification.setMessage(UProperties.getMessage("ERR0179", userTools.getLocale()));
								response.setNotification(notification);
								response.setUnexpectedError(Boolean.TRUE);
								
								errorMarked = true;
							}
							
							if(!errorMarked){
								try {
									
									folderPath = appSettings.getPropertyValue("cfdi.consignee.contract");
									consigneePath = this.taxpayer.getRfc();
									directory = folderPath + File.separator + consigneePath;
									
									fileName = UValue.uuid().replaceAll("-", "").toLowerCase().concat(".").concat(documentExtension);
									filePath = directory + File.separator + fileName;
									dbPath = consigneePath + File.separator + fileName;
									
									File fileFolder = new File(directory); 
									if(!fileFolder.exists()){
										fileFolder.mkdirs();
									}
									
									UFile.writeFile(UFile.fileToByte(document), filePath);
									
									try {
										
										if(action.equals("register")){
											errorMessage = UProperties.getMessage("ERR0175", userTools.getLocale());
											toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.consignee.register.title", userTools.getLocale());
											toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.consignee.register.message", userTools.getLocale());
											
											//##### Registrando consignatario
											consigneeEntity = new ConsignatarioEntity();
											consigneeEntity.setContribuyente(this.taxpayer);
											consigneeEntity.setFechaInicioContrato(contractStartDate);
											consigneeEntity.setFechaFinContrato(contractEndDate);
											consigneeEntity.setActivo(active);
											consigneeEntity.setContratoFinalizado(Boolean.FALSE);
											persistenceDAO.persist(consigneeEntity);
											
											//##### Registrando contrato
											ConsignatarioContratoEntity consigneeContractEntity = new ConsignatarioContratoEntity();
											consigneeContractEntity.setConsignatario(consigneeEntity);
											consigneeContractEntity.setFechaInicioContrato(contractStartDate);
											consigneeContractEntity.setFechaFinContrato(contractEndDate);
											consigneeContractEntity.setNombreContrato(fileName);
											consigneeContractEntity.setExtensionContrato(documentExtension);
											consigneeContractEntity.setActivo(Boolean.TRUE);
											consigneeContractEntity.setRutaContrato(dbPath);
											persistenceDAO.persist(consigneeContractEntity);
											
											// Bitacora :: Registro de accion en bitacora
											appLog.logOperation(OperationCode.REGISTRO_CONSIGNATARIO_ID79);
										}else if(action.equals("renewal")){
											errorMessage = UProperties.getMessage("ERR0176", userTools.getLocale());
											toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.consignee.renewal.title", userTools.getLocale());
											toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.consignee.renewal.message", userTools.getLocale());
											
											consigneeEntity.setFechaRenovacionContrato(contractRenewalDate);
											consigneeEntity.setFechaFinContrato(contractEndDate);
											consigneeEntity.setActivo(active);
											consigneeEntity.setContratoFinalizado(Boolean.FALSE);
											persistenceDAO.update(consigneeEntity);
											
											//##### Registrando contrato
											ConsignatarioContratoEntity consigneeContractEntity = new ConsignatarioContratoEntity();
											consigneeContractEntity.setConsignatario(consigneeEntity);
											consigneeContractEntity.setFechaInicioContrato(contractRenewalDate);
											consigneeContractEntity.setFechaFinContrato(contractEndDate);
											consigneeContractEntity.setNombreContrato(fileName);
											consigneeContractEntity.setExtensionContrato(documentExtension);
											consigneeContractEntity.setActivo(Boolean.TRUE);
											consigneeContractEntity.setRutaContrato(dbPath);
											persistenceDAO.persist(consigneeContractEntity);
											
											// Bitacora :: Registro de accion en bitacora
											appLog.logOperation(OperationCode.ACTUALIZACION_CONSIGNATARIO_ID80);
											
											consigneeObj.setContractStartDate(UBase64.base64Encode(UDate.formattedDate(consigneeEntity.getFechaInicioContrato(), DATE_FORMAT)));
										}
										
										//##### Cargando informacion de los contratos del consignatario
										Sort sort = Sort.by(Sort.Order.asc("id"));
										Iterable<ConsignatarioContratoEntity> consigneeContracts = iConsigneeContractJpaRepository.findAll(sort);
										for (ConsignatarioContratoEntity item : consigneeContracts) {
											ConsigneeContract contract = new ConsigneeContract();
											contract.setId(UBase64.base64Encode(item.getId().toString()));
											contract.setIdConsignee(UBase64.base64Encode(item.getConsignatario().getId().toString()));
											contract.setContractStartDate(UBase64.base64Encode(UDate.formattedDate(item.getFechaInicioContrato(), DATE_FORMAT)));
											contract.setContractEndDate(UBase64.base64Encode(UDate.formattedDate(item.getFechaFinContrato(), DATE_FORMAT)));
											contract.setDocument(UBase64.base64Encode(item.getNombreContrato()));
											contract.setDocumentExtension(UBase64.base64Encode(item.getExtensionContrato()));
											contract.setActive(item.getActivo());
											String dbPathReplaced = item.getRutaContrato().replaceAll(Pattern.quote("\\"), "_").replaceAll(Pattern.quote("/"), "_");
											contract.setContractPath(UBase64.base64Encode(dbPathReplaced));
											
											consigneeObj.getConsigneeContracts().add(contract);
										}																				
										
										consigneeJson.setConsignee(consigneeObj);
										
										this.credentialJson.setConsignee(consigneeJson);
										
										Toastr toastr = new Toastr();
										toastr.setTitle(toastrTitle);
										toastr.setMessage(toastrMessage);
										toastr.setType(Toastr.ToastrType.SUCCESS.value());
										
										notification.setToastrNotification(Boolean.TRUE);
										notification.setToastr(toastr);

										response.setNotification(notification);
										response.setSuccess(Boolean.TRUE);
										
									} catch (Exception e) {
										e.printStackTrace();
										
										notification.setPageMessage(Boolean.TRUE);
										notification.setCssStyleClass(CssStyleClass.ERROR.value());
										notification.setMessage(errorMessage);
										response.setNotification(notification);
										response.setUnexpectedError(Boolean.TRUE);
									}
									
								} catch (Exception e) {
									e.printStackTrace();
									
									notification.setPageMessage(Boolean.TRUE);
									notification.setCssStyleClass(CssStyleClass.ERROR.value());
									notification.setMessage(UProperties.getMessage("ERR0180", userTools.getLocale()));
									response.setNotification(notification);
									response.setUnexpectedError(Boolean.TRUE);
								}
							}else{
								response.setErrors(errors);
								response.setError(Boolean.TRUE);
							}
						}else if(action.equals("edit")){
							try {
								toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.consignee.edit.title", userTools.getLocale());
								toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.consignee.edit.message", userTools.getLocale());
								
								consigneeEntity.setActivo(active);
								persistenceDAO.update(consigneeEntity);
								
								// Bitacora :: Registro de accion en bitacora
								appLog.logOperation(OperationCode.ACTUALIZACION_CONSIGNATARIO_ID80);
								
								//##### Cargando informacion de los contratos del consignatario
								Sort sort = Sort.by(Sort.Order.desc("id"));
								Iterable<ConsignatarioContratoEntity> consigneeContracts = iConsigneeContractJpaRepository.findAll(sort);
								for (ConsignatarioContratoEntity item : consigneeContracts) {
									ConsigneeContract contract = new ConsigneeContract();
									contract.setId(UBase64.base64Encode(item.getId().toString()));
									contract.setIdConsignee(UBase64.base64Encode(item.getConsignatario().getId().toString()));
									contract.setContractStartDate(UBase64.base64Encode(UDate.formattedDate(item.getFechaInicioContrato(), DATE_FORMAT)));
									contract.setContractEndDate(UBase64.base64Encode(UDate.formattedDate(item.getFechaFinContrato(), DATE_FORMAT)));
									contract.setDocument(UBase64.base64Encode(item.getNombreContrato()));
									contract.setDocumentExtension(UBase64.base64Encode(item.getExtensionContrato()));
									contract.setActive(item.getActivo());
									String dbPathReplaced = item.getRutaContrato().replaceAll(Pattern.quote("\\"), "_").replaceAll(Pattern.quote("/"), "_");
									contract.setContractPath(UBase64.base64Encode(dbPathReplaced));
									
									consigneeObj.getConsigneeContracts().add(contract);
								}
								
								consigneeObj.setContractStartDate(UBase64.base64Encode(UDate.formattedDate(consigneeEntity.getFechaInicioContrato(), DATE_FORMAT)));
								if(!UValidator.isNullOrEmpty(consigneeEntity.getFechaRenovacionContrato())){
									consigneeObj.setContractRenewalDate(UBase64.base64Encode(UDate.formattedDate(consigneeEntity.getFechaRenovacionContrato(), DATE_FORMAT)));
								}
								
								consigneeJson.setConsignee(consigneeObj);
								
								this.credentialJson.setConsignee(consigneeJson);
								
								Toastr toastr = new Toastr();
								toastr.setTitle(toastrTitle);
								toastr.setMessage(toastrMessage);
								toastr.setType(Toastr.ToastrType.SUCCESS.value());
								
								notification.setToastrNotification(Boolean.TRUE);
								notification.setToastr(toastr);

								response.setNotification(notification);
								response.setSuccess(Boolean.TRUE);
								
							} catch (Exception e) {
								e.printStackTrace();
								
								notification.setPageMessage(Boolean.TRUE);
								notification.setCssStyleClass(CssStyleClass.ERROR.value());
								notification.setMessage(UProperties.getMessage("ERR0176", userTools.getLocale()));
								response.setNotification(notification);
								response.setUnexpectedError(Boolean.TRUE);
							}
						}
					}
				}
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR0121", userTools.getLocale()));
				response.setNotification(notification);
				response.setUnexpectedError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(errorMessage);
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		return jsonInString;
	}
	
	@RequestMapping(value="/managementUser", params={"consigneeContractPeriod"})
	public @ResponseBody String consigneeContractPeriod(HttpServletRequest request){
		
		JConsignee consigneeJson = new JConsignee();
		Consignee consignee = new Consignee();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		String startDate = null;
		String renewalDate = null;
		
		try {
			
			String consigneeRequest = request.getParameter("consigneeRequest");
			consignee = gson.fromJson(consigneeRequest, Consignee.class);
			
			startDate = UBase64.base64Decode(consignee.getContractStartDate());
			renewalDate = UBase64.base64Decode(consignee.getContractRenewalDate());
			
			String resultDate = UValidator.isNullOrEmpty(renewalDate) ? consigneeContractPeriod(startDate) : consigneeContractPeriod(renewalDate);
			
			if(!UValidator.isNullOrEmpty(resultDate)){
				String contractEndDate = UBase64.base64Encode(resultDate);
				consignee.setContractEndDate(contractEndDate);
			}else{
				consignee.setContractEndDate(null);
			}
			
		} catch (Exception e) {
			consignee.setContractEndDate(null);
		}
		
		consigneeJson.setConsignee(consignee);
		
		jsonInString = gson.toJson(consigneeJson);
		return jsonInString;
	}
	
	private String consigneeContractPeriod(String date){
		try {
			
			String contractPeriodValue = appSettings.getPropertyValue("cfdi.consignee.contract.period.value");
			String contractPeriodString = appSettings.getPropertyValue("cfdi.consignee.contract.period");
			
			Integer contractPeriod = Integer.valueOf(contractPeriodString);
						
			LocalDate ld = UDateTime.localDateFromString(date, DATE_FORMAT);
			LocalDate ldEnd = null;
			
			if(contractPeriodValue.equals("days")){
				ldEnd = ld.plusDays(contractPeriod);
			}else if(contractPeriodValue.equals("months")){
				ldEnd = ld.plusMonths(contractPeriod);
			}else if(contractPeriodValue.equals("years")){
				ldEnd = ld.plusYears(contractPeriod);
			}
			
			return UDateTime.formattedStringDateFromLocalDate(ldEnd);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value="/consigneeContractDownload/{document}", method=RequestMethod.GET)
	public ModelAndView consigneeContractDownload(HttpServletRequest request, HttpServletResponse response, @PathVariable(value="document") String document) throws Exception {
		try {
			
			String repositoryPath = appSettings.getPropertyValue("cfdi.consignee.contract");
			
			String filePath = UBase64.base64Decode(document);
			filePath = filePath.replaceAll("_", "\\\\");
					
			ConsignatarioContratoEntity contract = iConsigneeContractJpaRepository.findByRutaContrato(filePath);
			if(contract != null){
				String absolutePath = repositoryPath + File.separator + contract.getRutaContrato();
				
				File file = new File(absolutePath);
				byte[] fileByte = UFile.fileToByte(file);
				
				response.setContentType("application/xml");
		        response.setContentLength(fileByte.length);
		        response.setHeader("Content-Disposition","attachment; filename=\"" + file.getName() +"\"");
		 
		        FileCopyUtils.copy(fileByte, response.getOutputStream());
			}
	 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		return null;
    }
}
