package org.megapractical.invoicing.webapp.exposition.invoicing.validator.relatedcfdi;

import java.util.List;
import java.util.stream.Collectors;

import org.megapractical.invoicing.api.common.ApiRelatedCfdi;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.CfdiTipoRelacionEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiRelationshipTypeJpaRepository;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRelatedCfdi.JRelatedCfdiRequest;
import org.megapractical.invoicing.webapp.json.JRelatedCfdi.JRelatedCfdiResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class RelatedCfdiValidatorImpl implements RelatedCfdiValidator {

	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ICfdiRelationshipTypeJpaRepository iCfdiRelationshipTypeJpaRepository;
	
	@Override
	public JRelatedCfdiResponse getRelatedCfdi(JRelatedCfdiRequest relatedCfdiRequest, List<JError> errors) {
		ApiRelatedCfdi apiRelatedCfdi = null;
		JError error = null;
		val field = "relationship-type";
		
		val relatedCfdiCheck = relatedCfdiRequest.getRelatedCfdiCheck();
		if (relatedCfdiCheck.equals("on")) {
			val relatedCfdi = relatedCfdiRequest.getRelatedCfdi();
			val uuids = relatedCfdi.getUuids().stream()
											  .map(UBase64::base64Decode)
											  .map(RelatedCfdiUuid::new)
											  .collect(Collectors.toList());
			
			CfdiTipoRelacionEntity relationshipTypeEntity = null;
			var relationshipType = UBase64.base64Decode(relatedCfdi.getRelationshipType());
			if (UValidator.isNullOrEmpty(relationshipType)) {
				error = JError.required(field, userTools.getLocale());
			} else {
				// Validar que sea un valor de catalogo
				val relationshipTypeCatalog = UCatalog.getCatalog(relationshipType);
				relationshipType = relationshipTypeCatalog[0];
				val relationshipTypeValue = relationshipTypeCatalog[1];
				
				relationshipTypeEntity = iCfdiRelationshipTypeJpaRepository.findByCodigoAndEliminadoFalse(relationshipType);
				val relationshipTypeEntityByValue = iCfdiRelationshipTypeJpaRepository.findByValorAndEliminadoFalse(relationshipTypeValue);
				if (relationshipTypeEntity == null || relationshipTypeEntityByValue == null) {
					val values = new String[]{"CFDI33129", "TipoRelacion", "c_TipoRelacion"};
					val errorMessage = UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values);
					error = JError.error(field, errorMessage);
				}
			}
			
			if (relationshipTypeEntity != null) {
				apiRelatedCfdi = ApiRelatedCfdi
									.builder()
									.relationshipTypeCode(relationshipTypeEntity.getCodigo())
									.relationshipTypeValue(relationshipTypeEntity.getValor())
									.uuids(uuids).build();
			}
		}
		
		var relatedCfdiError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			relatedCfdiError = true;
		}
		
		return new JRelatedCfdiResponse(apiRelatedCfdi, relatedCfdiError);
	}

}