package org.megapractical.invoicing.webapp.exposition.sftp.file;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
public class SftpFileNotFoundException extends RuntimeException {

	public SftpFileNotFoundException(Long id) {
		super(String.format("File with id %s not found", id));
	}

}