package org.megapractical.invoicing.webapp.exposition.retention;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiEmitter;
import org.megapractical.invoicing.api.wrapper.ApiRetentionFileConfiguration;
import org.megapractical.invoicing.api.wrapper.ApiRetentionStamp;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoArchivoEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IFileStatusJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IFileTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionFileConfigurationJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionFileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.exposition.service.RetentionStampService;
import org.megapractical.invoicing.webapp.exposition.service.SessionService;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JRetentionFile;
import org.megapractical.invoicing.webapp.json.JRetentionFile.RetentionFile;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */

@Controller
@Scope("session")
@RequestMapping("/retention")
public class RetentionFileController {

	@Autowired
	private AppLog appLog;
	
	@Autowired
	private AppSettings appSettings;
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private PropertiesTools propertiesTools; 
	
	@Autowired
	private DataStoreService dataStore;
	
	@Autowired
	private StampControlService stampControl;
	
	@Autowired
	private SessionController sessionController;
	
	@Autowired
	private SessionService sessionService;
	
	@Autowired
	private IPersistenceDAO persistenceDAO;
	
	@Autowired
	private RetentionStampService retentionStampService;
	
	@Resource
	private ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	
	@Resource
	private IRetentionFileConfigurationJpaRepository iRetentionFileConfigurationJpaRepository;
	
	@Resource
	private IFileStatusJpaRepository iFileStatusJpaRepository;
	
	@Resource
	private IRetentionFileJpaRepository iRetentionFileJpaRepository;
	
	@Resource
	private IFileTypeJpaRepository iFileTypeJpaRepository;
	
	@Resource
	private ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;
	
	private static final String SORT_PROPERTY = "id";
	private static final Integer INITIAL_PAGE = 0;
	
	//##### Estado de archivo: En cola
	private static final String FILE_STATUS_QUEUED = "C";
	
	//##### Estado de archivo: Procesando
	private static final String FILE_STATUS_PROCESSING = "P";
	
	//##### Estado de archivo: Generado
	private static final String FILE_STATUS_GENERATED = "G";
	
	//##### Tipo Archivo TXT
	private static final String FILE_TYPE_TXT = "R10TXT";
	
	//##### Contribuyente
	private ContribuyenteEntity taxpayer;
	
	//##### Usuario
	private UsuarioEntity user;
	
	//##### Certificado del emisor
	private ContribuyenteCertificadoEntity certificate;
	private byte[] certificateByte;
	private byte[] privateKeyByte;
	private String keyPasswd;
	private String certificateNumber;
	
	//##### Informacion de timbres del emisor
	private TimbreEntity stampDetail;
	
	//##### Informacion para el timbrado
	private StampProperties stampProperties;
	
	//##### Confirguracion del archivo de retenciones
	private ArchivoRetencionConfiguracionEntity retentionFileConfiguration;
	
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	@ModelAttribute("processingRequest")
	public String processingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@ModelAttribute("emptyInformation")
	public String emptyInformation() throws IOException{
		return UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
	}
	
	@ModelAttribute("availableStampError")
	public String availableStampError() throws IOException{
		return UProperties.getMessage("ERR0163", userTools.getLocale());
	}
	
	//##### Inicializando informacion
	@GetMapping(params = {"load"})
	public String init(Model model) {
		try {
			
			//##### Cargando el contribuyente por el rfc activo
			this.taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
			//##### Cargando el usuario
			this.user = userTools.getCurrentUser();
			
			//##### Cargando certificado del contribuyente
			this.certificate = new ContribuyenteCertificadoEntity();
			this.certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
			
			//##### Verificando vigencia del certificado
			if(UValidator.isNullOrEmpty(this.certificate) || UDateTime.isBeforeLocalDate(this.certificate.getFechaExpiracion(), new Date())){
				return "/denied/ResourceDeniedByCertificateExpiration";
			}
			
			//##### Cargando informacion inicial
			initial(model);
			
			HtmlHelper.functionalitySelected("Retention", "retention-file-inbox");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			//return "/retention/Retention"; <- Bandeja de archivos (Ocultada)
			return "/retention/RetentionList";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	private void initial(Model model) {
		try {
			
			initialContent();
			model.addAttribute("page", INITIAL_PAGE);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initialContent() {
		try {
			
			//##### Cargando informacion de timbres del emisor
			this.stampDetail = stampControl.stampDetails();
			
			//##### Cargando informacion del certificado
			this.certificateByte = this.certificate.getCertificado();
			this.privateKeyByte = this.certificate.getLlavePrivada();
			this.keyPasswd = UBase64.base64Decode(this.certificate.getClavePrivada());
			this.certificateNumber = this.certificate.getNumero();
			
			//##### Cargando informacion para el timbrado
			this.stampProperties = new StampProperties();
			this.stampProperties.setEmitterId(propertiesTools.getEmitterId());
			this.stampProperties.setSessionId(propertiesTools.getSessionId());
			this.stampProperties.setSourceSystem(propertiesTools.getSourceSystem());
			this.stampProperties.setCorrelationId(propertiesTools.getCorrelationId());
			this.stampProperties.setCertificate(this.certificateByte);
			this.stampProperties.setPrivateKey(this.privateKeyByte);
			this.stampProperties.setPasswd(this.keyPasswd);
			this.stampProperties.setCertificateNumber(this.certificateNumber);
			this.stampProperties.setEnvironment(propertiesTools.getEnvironment());
			
			//##### Cargando configuracion del archivo de retenciones del contribuyente
			this.retentionFileConfiguration = iRetentionFileConfigurationJpaRepository.findByContribuyente(this.taxpayer);
			if(UValidator.isNullOrEmpty(this.retentionFileConfiguration)){
				configRetentionFileConfiguration();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void configRetentionFileConfiguration() {
		try {
			
			ArchivoRetencionConfiguracionEntity entity = new ArchivoRetencionConfiguracionEntity();
			entity.setContribuyente(this.taxpayer);
			entity.setEnvioXmlPdf(Boolean.FALSE);
			entity.setEnvioXml(Boolean.FALSE);
			entity.setEnvioPdf(Boolean.FALSE);
			entity.setCompactado(Boolean.TRUE);
			
			ApiRetentionFileConfiguration apiRetentionFileConfiguration = new ApiRetentionFileConfiguration();
			apiRetentionFileConfiguration.setRetentionFileConfiguration(entity);
			apiRetentionFileConfiguration.setAction("config");
			
			this.retentionFileConfiguration = dataStore.retentionFileConfigurationStore(apiRetentionFileConfiguration);
			
			//##### Bitacora
			appLog.logOperation(OperationCode.REGISTRO_ARCHIVO_RETENCION_CONFIGURACION_SISTEMA_ID85);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@PostMapping(params = {"fileTray"})
    public @ResponseBody String retentionFilelList(HttpServletRequest request) {
		
		JRetentionFile retentionFile = new JRetentionFile();
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		String action = null;
		
		try {
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchName = UBase64.base64Decode(request.getParameter("searchName"));
			if(UValidator.isNullOrEmpty(searchName)){
				searchName = "";
			}
			
			//##### Determinando la accion
			action = UBase64.base64Decode(request.getParameter("action"));
			
			EstadoArchivoEntity fileStatus = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);
			
			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Page<ArchivoRetencionEntity> retentionFilePage = null;
			
			//##### Listado de archivos con estado generado (con paginado)
			retentionFilePage = iRetentionFileJpaRepository.findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivoAndEliminadoFalseOrderByIdDesc(taxpayer, searchName, fileStatus, pageRequest);
			val pagination = JPagination.buildPaginaton(retentionFilePage);
			retentionFile.setPagination(pagination);
			
			for (ArchivoRetencionEntity item : retentionFilePage) {
				RetentionFile rfObj = new RetentionFile();
				rfObj.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
				rfObj.setFileType(UBase64.base64Encode(item.getTipoArchivo().getValor()));
				rfObj.setFileStatus(UBase64.base64Encode(item.getEstadoArchivo().getValor()));
				rfObj.setName(UBase64.base64Encode(item.getNombre()));
				rfObj.setPath(UBase64.base64Encode(item.getRuta()));
				rfObj.setUploadDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaCarga())));
				rfObj.setCompactedPath(UBase64.base64Encode(item.getRutaCompactado()));
				rfObj.setErrorPath(UBase64.base64Encode(item.getRutaError()));
				
				String period = null;
				if(!UValidator.isNullOrEmpty(item.getPeriodoMesInicial()) && !UValidator.isNullOrEmpty(item.getPeriodoMesFinal())) {
					period = UBase64.base64Encode(item.getPeriodoMesInicial() + " - " + item.getPeriodoMesFinal());
				}
				rfObj.setPeriod(period);
				
				String year = null;
				if(!UValidator.isNullOrEmpty(item.getPeriodoEjercicio())) {
					year = UBase64.base64Encode(item.getPeriodoEjercicio());
				}
				rfObj.setYear(year);
				
				retentionFile.getGeneratedRetentionFiles().add(rfObj);
			}
			
			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if(!UValidator.isNullOrEmpty(searchName)){
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
			}
			
			if(retentionFile.getGeneratedRetentionFiles().isEmpty()) {
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}
			
			//##### Listado de archivos con estado procesando
			List<ArchivoRetencionEntity> processingRetentionFiles = iRetentionFileJpaRepository.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(taxpayer, FILE_STATUS_PROCESSING);
			for (ArchivoRetencionEntity item : processingRetentionFiles) {
				RetentionFile rfObj = new RetentionFile();
				rfObj.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
				rfObj.setName(UBase64.base64Encode(item.getNombre()));
			}
			
			//##### Listado de archivos con estado en cola
			List<ArchivoRetencionEntity> queuedRetentionFiles = iRetentionFileJpaRepository.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(taxpayer, FILE_STATUS_QUEUED);
			for (ArchivoRetencionEntity item : queuedRetentionFiles) {
				RetentionFile rfObj = new RetentionFile();
				rfObj.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
				rfObj.setName(UBase64.base64Encode(item.getNombre()));
				rfObj.setUploadDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaCarga())));
				if(!UValidator.isNullOrEmpty(item.getTimbresDisponibles())){
					rfObj.setStampAvailable(item.getTimbresDisponibles());
				}
				retentionFile.getQueuedRetentionFiles().add(rfObj);
			}
			
			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(!action.equalsIgnoreCase("async")){
			sessionController.sessionUpdate();
		}
		
		retentionFile.setResponse(response);
		jsonInString = gson.toJson(retentionFile);
		return jsonInString;
	}
	
	@PostMapping(params={"retentionFileGeneratedList"})
    public @ResponseBody String retentionFilelGeneratedList(HttpServletRequest request){
		
		JRetentionFile retentionFile = new JRetentionFile();
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
					
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			//##### Listado de archivos con estado generado (con paginado)
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchName = UBase64.base64Decode(request.getParameter("searchName"));
			if(UValidator.isNullOrEmpty(searchName)){
				searchName = "";
			}
			
			String searchYear = UBase64.base64Decode(request.getParameter("searchYear"));
			if(UValidator.isNullOrEmpty(searchYear)){
				searchYear = "";
			}
			
			EstadoArchivoEntity fileStatus = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);
			
			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Page<ArchivoRetencionEntity> retentionFilePage = null;
			
			if(!UValidator.isNullOrEmpty(searchYear)) {
				retentionFilePage = iRetentionFileJpaRepository.findByContribuyenteAndNombreContainingIgnoreCaseAndPeriodoEjercicioAndEstadoArchivoAndEliminadoFalseOrderByIdDesc(taxpayer, searchName, searchYear, fileStatus, pageRequest);
			} else {
				retentionFilePage = iRetentionFileJpaRepository.findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivoAndEliminadoFalseOrderByIdDesc(taxpayer, searchName, fileStatus, pageRequest);
			}
			
			val pagination = JPagination.buildPaginaton(retentionFilePage);
			retentionFile.setPagination(pagination);
			
			for (ArchivoRetencionEntity item : retentionFilePage) {
				RetentionFile rfObj = new RetentionFile();
				rfObj.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
				rfObj.setFileType(UBase64.base64Encode(item.getTipoArchivo().getValor()));
				rfObj.setFileStatus(UBase64.base64Encode(item.getEstadoArchivo().getValor()));
				rfObj.setName(UBase64.base64Encode(item.getNombre()));
				rfObj.setPath(UBase64.base64Encode(item.getRuta()));
				rfObj.setUploadDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaCarga())));
				rfObj.setStampAvailable(item.getTimbresDisponibles());
				rfObj.setCompactedPath(UBase64.base64Encode(item.getRutaCompactado()));
				rfObj.setErrorPath(UBase64.base64Encode(item.getRutaError()));
				
				String period = null;
				if(!UValidator.isNullOrEmpty(item.getPeriodoMesInicial()) && !UValidator.isNullOrEmpty(item.getPeriodoMesFinal())) {
					period = UBase64.base64Encode(item.getPeriodoMesInicial() + " - " + item.getPeriodoMesFinal());
				}
				rfObj.setPeriod(period);
				
				String year = null;
				if(!UValidator.isNullOrEmpty(item.getPeriodoEjercicio())) {
					year = UBase64.base64Encode(item.getPeriodoEjercicio());
				}
				rfObj.setYear(year);
				
				retentionFile.getGeneratedRetentionFiles().add(rfObj);
			}
			
			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if(!UValidator.isNullOrEmpty(searchName)){
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
			}
			
			if(retentionFile.getGeneratedRetentionFiles().isEmpty()){
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}
			
			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		retentionFile.setResponse(response);
		jsonInString = gson.toJson(retentionFile);
		return jsonInString;
	}
	
	@Transactional
	@PostMapping(params={"uploadFile"})
    public @ResponseBody String uploadFile(Model model, HttpServletRequest request, @RequestParam(value="retentionFile") MultipartFile multipartRetentionFile) throws IOException {
		
		JRetentionFile retentionFile = new JRetentionFile();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		Boolean errorMarked = false;
		Boolean retentionUploadError = false;
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			if(stampControl.stampStatus(this.taxpayer)) {
				//##### Archivo retenciones
				File retentionFileUpload = null;
				
				String originalFilename = null;
				String retentionFileName = null;
				String retentionFileExt = null;
				
				error = new JError();
				
				if (multipartRetentionFile.isEmpty()) {
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				} else {
					originalFilename = multipartRetentionFile.getOriginalFilename();
					retentionFileExt = UFile.getExtension(originalFilename);
					
					if(UValidator.isNullOrEmpty(retentionFileExt) || !retentionFileExt.equalsIgnoreCase("txt")) {
						error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
						errorMarked = true;
					} else {
						retentionFileName = UFile.getName(originalFilename).toUpperCase();
						ArchivoRetencionEntity retentionFileEntity = iRetentionFileJpaRepository.findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(this.taxpayer, retentionFileName);
						if (!UValidator.isNullOrEmpty(retentionFileEntity)) {
							error.setMessage(UProperties.getMessage("ERR0188", userTools.getLocale()));
							errorMarked = true;
						} else {
							retentionFileUpload = UFile.multipartFileToFile(multipartRetentionFile);
						}
					}
				}
				
				if(errorMarked){
					error.setField("retention-file");
					errors.add(error);
					retentionUploadError = true;
				}
				
				if(!retentionUploadError) {
					//##### Ruta de las retenciones
					String retentionPath = appSettings.getPropertyValue("cfdi.retention.path");
					
					//##### Directorio para el archivo actual
					String folderPath = this.taxpayer.getRfcActivo() + File.separator + UDate.date().replaceAll("-", "") + File.separator + retentionFileName;
					String folderAbsolutePath = retentionPath + File.separator + folderPath; 
					File retentionAbsolutePath = new File(folderAbsolutePath); 
					if(!retentionAbsolutePath.exists()){
						retentionAbsolutePath.mkdirs();
					}
					
					//##### Cargando las configuraciones de archivo del usuario
					this.retentionFileConfiguration = iRetentionFileConfigurationJpaRepository.findByContribuyente(this.taxpayer);				
					
					//##### Subiendo el archivo al servidor
					String retentionUploadPath = folderAbsolutePath + File.separator + retentionFileName + "." + retentionFileExt;
					UFile.writeFile(UFile.fileToByte(retentionFileUpload), retentionUploadPath);								
					
					//##### Generando informacion del archivo
					ArchivoRetencionEntity retentionFileEntity = new ArchivoRetencionEntity();
					retentionFileEntity.setContribuyente(this.taxpayer);
					
					TipoArchivoEntity fileType = iFileTypeJpaRepository.findByCodigoAndEliminadoFalse(FILE_TYPE_TXT);
					retentionFileEntity.setTipoArchivo(fileType);
					
					EstadoArchivoEntity fileStatus = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_QUEUED);
					retentionFileEntity.setEstadoArchivo(fileStatus);
					
					retentionFileEntity.setNombre(retentionFileName);
					
					String retentionDbPath = folderPath + File.separator + retentionFileName + "." + retentionFileExt;
					retentionFileEntity.setRuta(retentionDbPath);
					
					retentionFileEntity.setFechaCarga(new Date());
					retentionFileEntity.setHoraCarga(new Date());
					retentionFileEntity.setEliminado(Boolean.FALSE);
					
					//##### Verificar si hay algun otro archivo procesandose
					Boolean anotherRetentionFileProcessing = false;
					List<ArchivoRetencionEntity> retentionFileList = iRetentionFileJpaRepository.findByContribuyenteAndEliminadoFalse(this.taxpayer);
					for (ArchivoRetencionEntity item : retentionFileList) {
						if(item.getEstadoArchivo().getCodigo().equalsIgnoreCase("P")){
							anotherRetentionFileProcessing = true;
							break;
						}
					}
					
					if (!anotherRetentionFileProcessing) {
						//##### Cargando informacion del emisor para la generacion de los comprobantes y el timbrado
						ApiEmitter apiEmitter = new ApiEmitter();
						apiEmitter.setRfc(UValue.stringValueUppercase(userTools.getContribuyenteActive().getRfcActivo()));
						apiEmitter.setNombreRazonSocial(userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo()).getNombreRazonSocial());
						apiEmitter.setCorreoElectronico(UValue.stringValue(userTools.getCurrentUser().getCorreoElectronico()));
						
						ContribuyenteRegimenFiscalEntity taxpayerTaxRegime = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(this.taxpayer);
						apiEmitter.setRegimenFiscal(taxpayerTaxRegime.getRegimenFiscal());
						
						apiEmitter.setCertificado(this.certificateByte);
						apiEmitter.setLlavePrivada(this.privateKeyByte);
						apiEmitter.setPasswd(this.keyPasswd);
						
						//##### Cargando informacion para procesar el fichero
						ApiRetentionStamp retentionStamp = new ApiRetentionStamp();
						retentionStamp.setTaxpayer(this.taxpayer);
						retentionStamp.setUser(user);
						retentionStamp.setIp(sessionService.getIp());
						retentionStamp.setUrlAcceso("retention/Retention");
						retentionStamp.setApiEmitter(apiEmitter);
						retentionStamp.setRetentionFileConfiguration(this.retentionFileConfiguration);
						retentionStamp.setRetentionFile(retentionFileEntity);
						retentionStamp.setStampProperties(this.stampProperties);
						retentionStamp.setStampDetail(this.stampDetail);
						retentionStamp.setAbsolutePath(folderAbsolutePath);
						retentionStamp.setUploadPath(retentionUploadPath);
						retentionStamp.setDbPath(folderPath);
						
						if(stampControl.stampStatus(taxpayer)) {
							//##### Procesar el fichero
							retentionStampService.retentionProcessingAsync(retentionStamp);
							
							//##### Notificacion Toastr
							Toastr toastr = new Toastr();
							toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.retention.file.upload.title", userTools.getLocale()));
							toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.retention.file.upload.message", userTools.getLocale()));
							toastr.setType(Toastr.ToastrType.SUCCESS.value());
							
							//##### Mensaje de notificacion
							notification = new JNotification();
							notification.setToastrNotification(Boolean.TRUE);
							notification.setToastr(toastr);
							
							response.setNotification(notification);
							response.setSuccess(Boolean.TRUE);
						} else {
							notification.setMessage(UProperties.getMessage("ERR0184", userTools.getLocale()));
							notification.setModalPanelMessage(Boolean.TRUE);
							notification.setCssStyleClass(CssStyleClass.ERROR.value());
							
							response.setUploadLimit(true);
							response.setNotification(notification);
							response.setError(Boolean.TRUE);
							response.setErrors(errors);															
						}
					} else {
						if(stampControl.stampStatus(taxpayer)) {
							//##### Registrando archivo en DB
							persistenceDAO.persist(retentionFileEntity);	
							
							//##### Notificacion Toastr													
							Toastr toastr = new Toastr();
							toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.generate.cfdi.title", userTools.getLocale()));
							toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.generate.cfdi.message", userTools.getLocale()));
							toastr.setType(Toastr.ToastrType.SUCCESS.value());
							
							//##### Mensaje de notificacion
							notification = new JNotification();
							notification.setToastrNotification(Boolean.TRUE);
							notification.setToastr(toastr);
							
							response.setNotification(notification);
							response.setSuccess(true);
						} else {
							notification.setMessage(UProperties.getMessage("ERR0184", userTools.getLocale()));
							notification.setModalPanelMessage(Boolean.TRUE);
							notification.setCssStyleClass(CssStyleClass.ERROR.value());
							
							response.setUploadLimit(true);
							response.setNotification(notification);
							response.setError(Boolean.TRUE);
							response.setErrors(errors);
						}
					}
				} else {
					response.setError(Boolean.TRUE);
					response.setErrors(errors);					
				}
			} else {
				//##### Notificacion Toastr																					
				notification.setMessage(UProperties.getMessage("ERR0163", userTools.getLocale()));
				notification.setModalPanelMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setSuccess(false);				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
				response.setErrors(errors);
			}
			
		} catch (Exception e) {
			notification.setMessage(UProperties.getMessage("ERR0162", userTools.getLocale()));
			notification.setModalPanelMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
			response.setErrors(errors);
		}
		
		sessionController.sessionUpdate();
		
		retentionFile.setResponse(response);
		
		jsonInString = gson.toJson(retentionFile);
		return jsonInString;
	}
	
	@ResponseBody
	@RequestMapping(value="/file/download/{name}/{type}", method=RequestMethod.GET)
	public ModelAndView download(@PathVariable(value="name") String name, @PathVariable(value="type") String type, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			
			String repositoryPath = appSettings.getPropertyValue("cfdi.retention.path");
			ArchivoRetencionEntity retentionFile = iRetentionFileJpaRepository.findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(this.taxpayer, name);
			if(!UValidator.isNullOrEmpty(retentionFile)){				
				String absolutePath = repositoryPath + File.separator;
				if(type.equalsIgnoreCase("compacted")) {
					absolutePath = absolutePath + retentionFile.getRutaCompactado();
				} else if(type.equalsIgnoreCase("error")) {
					absolutePath = absolutePath + retentionFile.getRutaError();
				}
				
				File file = new File(absolutePath);
				byte[] fileByte = UFile.fileToByte(file);
				
				response.setContentType("application/xml");
		        response.setContentLength(fileByte.length);
		        response.setHeader("Content-Disposition","attachment; filename=\"" + file.getName() +"\"");
		 
		        FileCopyUtils.copy(fileByte, response.getOutputStream());
			}
	 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		return null;
    }
		
}