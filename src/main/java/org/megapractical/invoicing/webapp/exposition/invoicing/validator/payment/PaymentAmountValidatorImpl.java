package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.math.BigDecimal;
import java.util.List;

import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentAmountValidatorImpl implements PaymentAmountValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Override
	public BigDecimal getAmount(String amountStr, MonedaEntity currency) {
		if (currency != null) {
			BigDecimal amount = null;
			
			if (UValidator.isNullOrEmpty(amountStr) || !UValidator.isDouble(amountStr)) {
				return null;
			}
			
			amount = UValue.bigDecimalStrict(amountStr, UTools.decimalValue(amountStr));
			if (!UValidator.isValidDecimalPart(amount, currency.getDecimales())) {
				return null;
			}
			return amount;
		}
		return null;
	}

	@Override
	public BigDecimal getAmount(String amountStr, String field, MonedaEntity currency, List<JError> errors) {
		if (currency != null) {
			BigDecimal amount = null;
			JError error = null;
			
			if (UValidator.isNullOrEmpty(amountStr)) {
				error = JError.required(field, userTools.getLocale());
			} else if (!UValidator.isDouble(amountStr)) {
				error = JError.error(field, "ERR1003", userTools.getLocale());
			} else {
				amount = UValue.bigDecimalStrict(amountStr, UTools.decimalValue(amountStr));
				if (!UValidator.isValidDecimalPart(amount, currency.getDecimales())) {
					error = JError.error(field, "ERR0129", userTools.getLocale());
				}
			}
			
			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
			}
			return amount;
		}
		return null;
	}

}