package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.TipoFactorEntity;
import org.megapractical.invoicing.dal.data.repository.IFactorTypeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CatalogValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class FactorTypeValidatorImpl implements FactorTypeValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private IFactorTypeJpaRepository iFactorTypeJpaRepository;
	
	@Override
	public CatalogValidatorResponse validate(String factorType, String rateOrFee, boolean isTransferred) {
		JError error = null;
		val field = "tax-transferred-withheld-factor-type";
		
		if (UValidator.isNullOrEmpty(factorType)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			TipoFactorEntity factorTypeEntity = iFactorTypeJpaRepository.findByValorAndEliminadoFalse(factorType);
			if (factorTypeEntity == null) {
				val errorCode = isTransferred ? "CFDI33156" : "CFDI33165";
				val values = new String[]{errorCode, "Impuesto", "c_Impuesto"};
				error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
			}
		}
		
		return new CatalogValidatorResponse(error);
	}

}