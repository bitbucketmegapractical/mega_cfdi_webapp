package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ExportacionEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.ExportationValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service.ExportationService;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class ExportationValidatorImpl implements ExportationValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ExportationService exportationService;
	
	@Override
	public ExportationValidatorResponse validate(String exportation) {
		ExportacionEntity exportationEntity = null;
		var hasError = false;

		if (UValidator.isNullOrEmpty(exportation)) {
			hasError = true;
		} else {
			exportationEntity = exportationService.findByCode(exportation);
			if (exportationEntity == null) {
				hasError = true;
			}
		}
		return new ExportationValidatorResponse(exportationEntity, hasError);
	}
	
	@Override
	public ExportationValidatorResponse validate(String exportation, String field) {
		ExportacionEntity exportationEntity = null;
		JError error = null;

		if (UValidator.isNullOrEmpty(exportation)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			exportationEntity = getExportation(exportation);
			if (exportationEntity == null) {
				val values = new String[] { "CFDI40123", "Exportacion", "c_Exportacion" };
				error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
			}
		}
		return new ExportationValidatorResponse(exportationEntity, error);
	}
	
	private ExportacionEntity getExportation(String exportation) {
		// Validar que sea un valor de catalogo
		val exportationCatalog = UCatalog.getCatalog(exportation);
		exportation = exportationCatalog[0];
		val exportationValue = exportationCatalog[1];

		val exportationEntity = exportationService.findByCode(exportation);
		val exportationEntityByValue = exportationService.findByValue(exportationValue);
		if (exportationEntity == null || exportationEntityByValue == null) {
			return null;
		}
		return exportationEntity;
	}

}