package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;

public interface PaymentTryService {
	
	ContribuyenteEntity getCurrentTaxpayer();
	
	UsuarioEntity getCurrentUser();
	
	ContribuyenteCertificadoEntity getCertificate(ContribuyenteEntity taxpayer);
	
	TimbreEntity getStampDetails();
	
	StampProperties getStampProperties(ContribuyenteCertificadoEntity certificate);
	
	ArchivoFacturaConfiguracionEntity getFileConfiguration(ContribuyenteEntity taxpayer);
	
}