package org.megapractical.invoicing.webapp.exposition.retention.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RetentionDataResponse {
	private String retentionKey;
	private boolean sectionError;
}