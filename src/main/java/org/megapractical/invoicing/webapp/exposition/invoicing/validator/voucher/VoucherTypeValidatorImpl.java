package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.VoucherTypeCatalogValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherTypeResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherTypeValidatorImpl implements VoucherTypeValidator {
	
	@Autowired
	private VoucherTypeCatalogValidator voucherTypeCatalogValidator;
	
	@Override
	public VoucherTypeResponse getVoucherType(String voucherType) {
		val voucherTypeValidatorResponse = voucherTypeCatalogValidator.validate(voucherType);
		val voucherTypeEntity = voucherTypeValidatorResponse.getVoucherType();
		val voucherTypeError = voucherTypeValidatorResponse.getError() != null;
		return new VoucherTypeResponse(voucherTypeEntity, voucherTypeError);
	}
	
	@Override
	public VoucherTypeResponse getVoucherType(JVoucher voucher, List<JError> errors) {
		var voucherType = UBase64.base64Decode(voucher.getVoucherType());
		val voucherTypeValidatorResponse = voucherTypeCatalogValidator.validate(voucherType, "voucher-type");
		val voucherTypeEntity = voucherTypeValidatorResponse.getVoucherType();
		val error = voucherTypeValidatorResponse.getError();
		
		var voucherTypeError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			voucherTypeError = true;
		}
		
		return new VoucherTypeResponse(voucherTypeEntity, voucherTypeError);
	}

}