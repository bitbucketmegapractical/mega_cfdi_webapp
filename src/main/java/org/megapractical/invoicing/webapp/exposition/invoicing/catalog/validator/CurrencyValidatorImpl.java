package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CurrencyValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service.CurrencyService;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class CurrencyValidatorImpl implements CurrencyValidator {

	@Autowired
	private UserTools userTools;

	@Autowired
	private CurrencyService currencyService;

	@Override
	public CurrencyValidatorResponse validate(String currency) {
		MonedaEntity currencyEntity = null;
		var hasError = false;

		if (UValidator.isNullOrEmpty(currency)) {
			hasError = true;
		} else {
			currencyEntity = currencyService.findByCode(currency);
			if (currencyEntity == null) {
				hasError = true;
			}
		}

		return new CurrencyValidatorResponse(currencyEntity, hasError);
	}
	
	@Override
	public CurrencyValidatorResponse validate(String currency, String field) {
		MonedaEntity currencyEntity = null;
		JError error = null;

		if (UValidator.isNullOrEmpty(currency)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			currencyEntity = getCurrency(currency);
			if (currencyEntity == null) {
				val values = new String[] { "CFDI33112", "Moneda", "c_Moneda" };
				error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
			}
		}

		return new CurrencyValidatorResponse(currencyEntity, error);
	}
	
	private MonedaEntity getCurrency(String currency) {
		// Validar que sea un valor de catalogo
		val currencyCatalog = UCatalog.getCatalog(currency);
		currency = currencyCatalog[0];
		val currencyValue = currencyCatalog[1];

		val currencyEntity = currencyService.findByCode(currency);
		val currencyEntityByValue = currencyService.findByValue(currencyValue);
		if (currencyEntity == null || currencyEntityByValue == null) {
			return null;
		}
		return currencyEntity;
	}
	
}