package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.util.Date;

import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.CertificateService;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

@Service
@Scope("session")
@RequiredArgsConstructor
class CancellationTryService {

	private final UserTools userTools;
	private final CertificateService certificateService;
	
	public ContribuyenteEntity getCurrentTaxpayer() {
		val activeRfc = userTools.getContribuyenteActive().getRfcActivo();
		return userTools.getContribuyenteActive(activeRfc);
	}

	public UsuarioEntity getCurrentUser() {
		return userTools.getCurrentUser();
	}

	public ContribuyenteCertificadoEntity getCertificate(ContribuyenteEntity taxpayer) {
		val certificate = certificateService.findByTaxpayer(taxpayer);
		// Verifying validity
		if (certificate == null || UDateTime.isBeforeLocalDate(certificate.getFechaExpiracion(), new Date())) {
			return null;
		}
		return certificate;
	}

}