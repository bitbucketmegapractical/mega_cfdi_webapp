package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.math.BigDecimal;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCalculation;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JConcept;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class InvoiceConceptAmountCalcServiceImpl implements InvoiceConceptAmountCalcService {
	
	public JConcept conceptAmountCalc(String objConcept, MonedaEntity currency) {
		val concept = GsonClient.deserialize(objConcept, JConcept.class);
		
		val quantity = getQuantity(concept, currency);
		val discountStr = getDiscount(concept, currency);
		val unitValue = getUnitValue(concept, currency);
		val amountStr = getAmount(quantity, unitValue, currency);
		
		concept.setQuantity(UBase64.base64Encode(UValue.bigDecimalString(quantity)));
		concept.setUnitValue(UBase64.base64Encode(UValue.bigDecimalString(unitValue)));
		concept.setDiscount(discountStr);
		concept.setAmount(amountStr);
		
		return concept;
	}
	
	private static BigDecimal getQuantity(JConcept concept, MonedaEntity currency) {
		var quantityStr = UBase64.base64Decode(concept.getQuantity());
		BigDecimal quantity = null;
		if (!UValidator.isNullOrEmpty(quantityStr) && UValidator.isBigDecimal(quantityStr)) {
			var decimalValue = UTools.decimalValue(quantityStr);
			decimalValue = (!UValidator.isNullOrEmpty(decimalValue) && decimalValue > 0) ? decimalValue : currency.getDecimales(); 
			quantity = UValue.bigDecimal(quantityStr, decimalValue);
		}
		return quantity;
	}
	
	private static String getDiscount(JConcept concept, MonedaEntity currency) {
		var discountStr = UBase64.base64Decode(concept.getDiscount());
		BigDecimal discount = null;
		if (!UValidator.isNullOrEmpty(discountStr) && UValidator.isBigDecimal(discountStr)) {
			var decimalValue = UTools.decimalValue(discountStr);
			decimalValue = (!UValidator.isNullOrEmpty(decimalValue) && decimalValue > 0) ? decimalValue : currency.getDecimales();
			
			discount = UValue.bigDecimal(discountStr, decimalValue);
			discountStr = UBase64.base64Encode(UValue.bigDecimalString(discount));
		}
		return discountStr;
	}
	
	private static BigDecimal getUnitValue(JConcept concept, MonedaEntity currency) {
		var unitValueStr = UBase64.base64Decode(concept.getUnitValue());
		BigDecimal unitValue = null;
		if (!UValidator.isNullOrEmpty(unitValueStr) && UValidator.isBigDecimal(unitValueStr)) {
			var decimalValue = UTools.decimalValue(unitValueStr);
			decimalValue = (!UValidator.isNullOrEmpty(decimalValue) && decimalValue > 0) ? decimalValue : currency.getDecimales();
			unitValue = UValue.bigDecimal(unitValueStr, decimalValue);
		}
		return unitValue;
	}
	
	private static String getAmount(BigDecimal quantity, BigDecimal unitValue, MonedaEntity currency) {
		String amountStr = null;
		if (!UValidator.isNullOrEmpty(quantity) && !UValidator.isNullOrEmpty(unitValue)) {
			val amount = UValue.bigDecimal(UValue.bigDecimalString(UCalculation.amount(quantity, unitValue)), currency.getDecimales());					
			amountStr = UBase64.base64Encode(UValue.bigDecimalString(amount));
		}
		return amountStr;
	}
	
}