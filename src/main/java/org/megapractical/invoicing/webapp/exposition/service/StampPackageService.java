package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.webapp.json.JStampPackage;

public interface StampPackageService {
	
	JStampPackage stampPackageInfo();
	
}