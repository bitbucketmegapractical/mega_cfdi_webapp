package org.megapractical.invoicing.webapp.exposition.payroll.file.service;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEntity;
import org.megapractical.invoicing.dal.data.repository.IPayrollFileJpaRepository;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class PayrollFileServiceImpl implements PayrollFileService {
	
	private final IPayrollFileJpaRepository iPayrollFileJpaRepository;
	
	@Override
	public ArchivoNominaEntity findOne(Long id, FileStatus status) {
		return iPayrollFileJpaRepository.findByIdAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalse(id, status.value())
				.orElse(null);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		if (id != null) {
			iPayrollFileJpaRepository.deleteById(id);
		}
	}

}