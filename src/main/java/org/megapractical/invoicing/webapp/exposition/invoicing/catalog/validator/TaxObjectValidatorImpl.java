package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ObjetoImpuestoEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.TaxObjectValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service.TaxObjectService;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class TaxObjectValidatorImpl implements TaxObjectValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private TaxObjectService taxObjectService;
	
	@Override
	public TaxObjectValidatorResponse validate(String taxObject) {
		ObjetoImpuestoEntity taxObjectEntity = null;
		var hasError = false;

		if (UValidator.isNullOrEmpty(taxObject)) {
			hasError = true;
		} else {
			taxObjectEntity = taxObjectService.findByCode(taxObject);
			if (taxObjectEntity == null) {
				hasError = true;
			}
		}
		return new TaxObjectValidatorResponse(taxObjectEntity, hasError);
	}
	
	@Override
	public TaxObjectValidatorResponse validate(String taxObject, String field) {
		ObjetoImpuestoEntity taxObjectEntity = null;
		JError error = null;

		if (UValidator.isNullOrEmpty(taxObject)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			taxObjectEntity = getTaxObject(taxObject);
			if (taxObjectEntity == null) {
				val values = new String[] { "CFDI40123", "ObjetoImp", "c_ObjetoImp" };
				error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
			}
		}
		return new TaxObjectValidatorResponse(taxObjectEntity, error);
	}
	
	private ObjetoImpuestoEntity getTaxObject(String taxObject) {
		// Validar que sea un valor de catalogo
		val taxObjectCatalog = UCatalog.getCatalog(taxObject);
		taxObject = taxObjectCatalog[0];
		val taxObjectValue = taxObjectCatalog[1];

		val taxObjectEntity = taxObjectService.findByCode(taxObject);
		val taxObjectEntityByValue = taxObjectService.findByValue(taxObjectValue);
		if (taxObjectEntity == null || taxObjectEntityByValue == null) {
			return null;
		}
		return taxObjectEntity;
	}

}