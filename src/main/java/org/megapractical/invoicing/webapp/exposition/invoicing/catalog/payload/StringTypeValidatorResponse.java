package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StringTypeValidatorResponse {
	private String stringType;
	private String stringTypeCode;
	private JError error;
	private boolean hasError;
	
	public StringTypeValidatorResponse(String stringType, String stringTypeCode) {
		this.stringType = stringType;
		this.stringTypeCode = stringTypeCode;
		this.hasError = Boolean.FALSE;
	}
	
	public StringTypeValidatorResponse(String stringType, String stringTypeCode, boolean hasError) {
		this.stringType = stringType;
		this.stringTypeCode = stringTypeCode;
		this.hasError = hasError;
	}
	
	public StringTypeValidatorResponse(String stringType, String stringTypeCode, JError error) {
		this.stringType = stringType;
		this.stringTypeCode = stringTypeCode;
		this.hasError = error != null;
	}
	
}