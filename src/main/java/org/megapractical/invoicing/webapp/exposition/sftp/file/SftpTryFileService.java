package org.megapractical.invoicing.webapp.exposition.sftp.file;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.megapractical.common.datetime.DateTimeFormat;
import org.megapractical.common.datetime.LocalDateUtils;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.exposition.sftp.file.JSftpFile.SftpFile;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class SftpTryFileService {
	
	private final UserTools userTools;
	private final SftpFileService sftpFileService;
	
	private static final Integer INITIAL_PAGE = 0;
	private static final String SORT_PROPERTY = "id";
	
	public JSftpFile loadSftpFiles(ContribuyenteEntity taxpayer, String pageStr, int paginateSize, String searchBy, String date, String type) {
		var page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
		page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
		
		val sort = Sort.by(Sort.Order.desc(SORT_PROPERTY));
		val pageRequest = PageRequest.of(page, paginateSize, sort);
		
		val uploadDate = LocalDateUtils.parse(date, DateTimeFormat.CUSTOM_FORMAT);

		Page<ArchivoStfpEntity> sftpFilePage = Page.empty();
		if ("cfdi".equals(type)) {
			sftpFilePage = sftpFileService.getCfdiFiles(taxpayer, searchBy, uploadDate, pageRequest);
		} else if ("payroll".equals(type)) {
			sftpFilePage = sftpFileService.getPayrollFiles(taxpayer, searchBy, uploadDate, pageRequest);
		}
		
		val pagination = JPagination.buildPaginaton(sftpFilePage);
		val sftpFiles = getSftpFiles(sftpFilePage);
		
		val notification = new JNotification();
		var searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
		if (!UValidator.isNullOrEmpty(searchBy)) {
			notification.setSearch(Boolean.TRUE);
			searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
		}

		if (sftpFiles.isEmpty()) {
			notification.setSearchResultMessage(searchResultEmpty);
			notification.setPanelMessage(Boolean.TRUE);
			notification.setDismiss(Boolean.FALSE);
			notification.setCssStyleClass(CssStyleClass.INFO.value());
		}
		
		val response = new JResponse();
		response.setSuccess(Boolean.TRUE);
		response.setNotification(notification);
		
		return JSftpFile.builder().pagination(pagination).sftpFiles(sftpFiles).response(response).build();
	}
	
	private List<SftpFile> getSftpFiles(Page<ArchivoStfpEntity> sftpFilePage) {
		if (sftpFilePage != null) {
			return sftpFilePage.stream().map(SftpFileConverter::convert).filter(Objects::nonNull).collect(toList());
		}
		return Collections.emptyList();
	}
	
}