package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface CfdiFakeStampService {
	
	StampResponse cfdiFakeStamp(ApiCfdi apiCfdi);
	
	StampResponse cfdiFakeStamp(ApiCfdi apiCfdi, boolean withStampError);
	
	StampResponse cfdiFakeStamp(Comprobante voucher);
	
	StampResponse paymentFakeStamp(ApiCfdi apiCfdi);
	
}