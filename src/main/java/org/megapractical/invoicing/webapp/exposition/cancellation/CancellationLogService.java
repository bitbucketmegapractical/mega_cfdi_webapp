package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.time.LocalDateTime;

import org.megapractical.invoicing.api.smarterweb.SWCancellationResponse;
import org.megapractical.invoicing.dal.bean.datatype.CancellationType;
import org.megapractical.invoicing.dal.bean.jpa.BitacoraCancelacionEntity;
import org.megapractical.invoicing.dal.data.repository.ICancellationLogJpaRepository;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
class CancellationLogService {
	
	private final ICancellationLogJpaRepository iCancellationLogJpaRepository;
	
	public BitacoraCancelacionEntity cancellationLog(SWCancellationResponse cancellationResponse,
													 CancellationFileData cancellationFileData, 
													 CancellationType type) {
		val cancellationLog = new BitacoraCancelacionEntity();
		cancellationLog.setArchivoCancelacion(cancellationFileData.getCancellationFile());
		cancellationLog.setTipo(type);
		cancellationLog.setEmisorRfc(cancellationResponse.getEmitterRfc());
		cancellationLog.setReceptorRfc(cancellationResponse.getReceiverRfc());
		cancellationLog.setUuid(cancellationResponse.getUuid());
		cancellationLog.setTotal(cancellationResponse.getVoucherTotal());
		cancellationLog.setMotivo(cancellationResponse.getReason());
		cancellationLog.setUuidSustitucion(cancellationResponse.getSubstitutionUuid());
		cancellationLog.setCancelado(cancellationResponse.isSuccess());
		cancellationLog.setCancelacionEnProceso(false);
		cancellationLog.setSolicitudCancelacion(false);
		cancellationLog.setAcuseCancelacion(cancellationResponse.getAcuseXml());
		cancellationLog.setFechaCancelacion(LocalDateTime.now());
		return iCancellationLogJpaRepository.save(cancellationLog);
	}
	
}