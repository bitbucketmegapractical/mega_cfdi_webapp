package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentCurrencyValidator {
	
	MonedaEntity getCurrency(String currency);
	
	MonedaEntity getCurrency(String currency, String field, List<JError> errors);
	
}