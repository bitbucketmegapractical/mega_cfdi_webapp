package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.InvoiceStoreRequest;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface InvoiceStoreService {

	boolean store(ContribuyenteEntity taxpayer, PreferenciasEntity preferences, ApiCfdi apiCfdi);

	boolean store(InvoiceStoreRequest storeRequest);
	
	void generateAndStorePdf(ApiCfdi apiCfdi, String pdfPath, String complementCode);

}