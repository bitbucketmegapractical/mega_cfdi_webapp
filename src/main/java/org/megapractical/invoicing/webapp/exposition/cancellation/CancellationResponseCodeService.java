package org.megapractical.invoicing.webapp.exposition.cancellation;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.smarterweb.SWCancellationResponse;
import org.megapractical.invoicing.dal.bean.jpa.CancelacionCodigoRespuestaEntity;
import org.megapractical.invoicing.dal.data.repository.ICancellationResponseCodeJpaRepository;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
class CancellationResponseCodeService {
	
	private final ICancellationResponseCodeJpaRepository iCancellationResponseCodeJpaRepository;
	
	public CancelacionCodigoRespuestaEntity findByCode(String code) {
		return iCancellationResponseCodeJpaRepository.findByCodigoAndEliminadoFalse(code).orElse(null);
	}
	
	public String details(SWCancellationResponse cancellationResponse) {
		var details = "Código de cancelación no reconocido";
		val cancellationResponseCode = findByCode(cancellationResponse.getResponseUuidStatusCode());
		if (AssertUtils.nonNull(cancellationResponseCode)) {
			details = cancellationResponse.getUuid() + " - " + cancellationResponseCode.getValor();
		}
		return details;
	}
	
}