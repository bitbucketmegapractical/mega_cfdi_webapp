package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.data.repository.ITaxTypeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CatalogValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class TaxTypeValidatorImpl implements TaxTypeValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ITaxTypeJpaRepository iTaxTypeJpaRepository;
	
	@Override
	public CatalogValidatorResponse validate(String taxType, String field, boolean isTransferred) {
		JError error = null;
		
		if (UValidator.isNullOrEmpty(taxType)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			// Validar que sea un valor de catalogo
			val taxTypeCatalog = UCatalog.getCatalog(taxType);
			val taxTypeCode = taxTypeCatalog[0];
			val taxTypeValue = taxTypeCatalog[1];
			
			val taxTypeEntityByCode = iTaxTypeJpaRepository.findByCodigo(taxTypeCode);
			val taxTypeEntityByValue = iTaxTypeJpaRepository.findByValor(taxTypeValue);
			
			if (taxTypeEntityByCode == null || taxTypeEntityByValue == null) {
				val errorCode = isTransferred ? "CFDI33155" : "CFDI33164";
				val values = new String[]{errorCode, "Impuesto", "c_Impuesto"};
				error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
			}
		}
		
		return new CatalogValidatorResponse(error);
	}

}