package org.megapractical.invoicing.webapp.exposition.service.customer;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ClienteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.json.JCustomer.Customer;

public interface CustomerService {
	
	List<ClienteEntity> findAllByTaxpayer(ContribuyenteEntity taxpayer);
	
	List<Customer> getMappedCustomers(List<ClienteEntity> sourceList);
	
	String getPostalCodeEncoded(ClienteEntity customer);
	
	String getTaxRegimeEncoded(ClienteEntity customer);
	
}