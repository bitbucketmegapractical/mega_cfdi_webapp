package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.api.common.ApiVoucherTaxes;
import org.megapractical.invoicing.api.util.UCalculation;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiConcept.ConceptCustomsInformation;
import org.megapractical.invoicing.api.wrapper.ApiConcept.ConceptPropertyAccount;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.data.repository.IKeyProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IMeasurementUnitJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IObjetoImpuestoJpaRepository;
import org.megapractical.invoicing.sat.complement.concept.iedu.InstEducativas;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.CuentaPredial;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.InformacionAduanera;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherConceptRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherConceptResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WConcept;
import org.megapractical.invoicing.webapp.wrapper.WTaxes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherConceptValidatorImpl implements VoucherConceptValidator {

	@Autowired
	private UserTools userTools;
	
	@Autowired
	private IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository;
	
	@Autowired
	private IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository;
	
	@Autowired
	private IObjetoImpuestoJpaRepository iObjetoImpuestoJpaRepository;
	
	/**
	 * Version complemento entidades educativas privadas
	 */
	private static final String COMPLEMENT_IEDU_VERSION = "1.0";

	@Override
	public VoucherConceptResponse getConcepts(VoucherConceptRequest voucherConceptRequest, List<JError> errors) {
		val apiConcepts = new ArrayList<ApiConcept>();
		JResponse response = null;
		JNotification notification = null;
		JError error = null;
		
		val currency = voucherConceptRequest.getCurrency();
		var voucherSubtotal = UValue.bigDecimal(currency.getDecimales());
		val conceptPropertyAccounts = new ArrayList<ConceptPropertyAccount>();
		val conceptCustomsInformations = new ArrayList<ConceptCustomsInformation>();
		ApiVoucherTaxes conceptsTax = null;
		var discountByConcept = false;
		
		val wConcepts = voucherConceptRequest.getWConcepts();
		if (wConcepts.isEmpty()) {
			error = JError.message("ERR0008", userTools.getLocale());
			notification = JNotification.pageMessageError(error.getMessage());
		} else {
			BigDecimal conceptTaxTransferred = null;
			BigDecimal conceptTaxWithheld = null;
			var conceptNumber = 1;
			
			for (val item : wConcepts) {
				val amount = UValue.bigDecimal(item.getAmount(), UTools.decimals(item.getAmount()));
				val unitValue = UValue.bigDecimal(item.getUnitValue(), UTools.decimals(item.getUnitValue()));
				val quantity = UValue.bigDecimal(item.getQuantity(), UTools.decimals(item.getQuantity()));
				var discount = UValue.bigDecimal(item.getDiscount(), UTools.decimals(item.getDiscount()));
				
				// Verificando importe
				val amountValid = UValue.bigDecimal(UCalculation.amount(quantity, unitValue).toString(), currency.getDecimales()) ;
				if (!amount.equals(amountValid)) {
					var message = UProperties.getMessage("ERR0023", userTools.getLocale());
					val messageSplitted = message.split(Pattern.quote("{0}"));
					message = messageSplitted[0].concat(item.getDescription()).concat(messageSplitted[1]);
					
					error = JError.builder().message(message).build();
					notification = JNotification.pageMessageError(error.getMessage());
					break;
				}
				
				voucherSubtotal = voucherSubtotal.add(amount);
				
				val keyProductServiceCode = UCatalog.getCode(item.getKeyProductService());
				val keyProductService = iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(keyProductServiceCode);
				
				val measurementUnitCode = UCatalog.getCode(item.getMeasurementUnit());
				val measurementUnit = iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(measurementUnitCode);
				
				val objImpCode = UCatalog.getCode(item.getObjImp());
				val objImp = iObjetoImpuestoJpaRepository.findByCodigoAndEliminadoFalse(objImpCode);
				
				Long conceptSelected = null;
				if (!UValidator.isNullOrEmpty(item.getConceptSelected())) {
					conceptSelected = UValue.longValue(item.getConceptSelected());
				}
				
				val apiConcept = new ApiConcept();
				apiConcept.setConceptSelected(conceptSelected);
				apiConcept.setAmount(amount);
				apiConcept.setUnitValue(unitValue);
				apiConcept.setQuantity(quantity);
				apiConcept.setIdentificationNumber(item.getIdentificationNumber());
				apiConcept.setDescription(item.getDescription());
				apiConcept.setKeyProductServiceEntity(keyProductService);
				apiConcept.setKeyProductService(keyProductService.getValor());
				apiConcept.setKeyProductServiceCode(keyProductService.getCodigo());
				apiConcept.setMeasurementUnitEntity(measurementUnit);
				apiConcept.setMeasurementUnit(measurementUnit.getValor());
				apiConcept.setMeasurementUnitCode(measurementUnit.getCodigo());
				apiConcept.setUnit(item.getUnit());
				apiConcept.setObjImp(objImp.getCodigo());
				apiConcept.setConceptNumber(UValue.integerString(conceptNumber));
				
				if (item.getDiscount() != null) {
					discount = UValue.bigDecimalStrict(UValue.bigDecimalString(UCalculation.discount(quantity, unitValue, discount)), currency.getDecimales());
					apiConcept.setDiscount(discount);
					discountByConcept = true;
				}
				
				// Impuestos trasladados
				conceptTaxTransferred = getTaxesTransferred(currency, conceptTaxTransferred, conceptNumber, item, apiConcept);
				// Impuestos retenidos
				conceptTaxWithheld = getTaxesWithheld(currency, conceptTaxWithheld, conceptNumber, item, apiConcept);
				// Informacion aduanera
				setCustomsInformation(conceptCustomsInformations, conceptNumber, item, apiConcept);
				// Cuenta predial
				setPropertyAccount(conceptPropertyAccounts, conceptNumber, item, apiConcept);
				// Complemento Concepto de Instituciones Educativas Privadas
				setIeduComplement(item, apiConcept);
				
				apiConcepts.add(apiConcept);
				conceptNumber ++;
			}
			
			conceptsTax = getConceptTax(conceptTaxTransferred, conceptTaxWithheld);
		}

		var conceptError = false;
		if (error != null) {
			errors.add(error);
			response = JResponse.error(notification, errors);
			conceptError = true;
		}

		return VoucherConceptResponse
				.builder()
				.apiConcepts(apiConcepts)
				.conceptPropertyAccounts(conceptPropertyAccounts)
				.conceptCustomsInformations(conceptCustomsInformations)
				.voucherSubtotal(voucherSubtotal)
				.conceptsTax(conceptsTax)
				.discountByConcept(discountByConcept)
				.response(response)
				.error(conceptError)
				.build();
	}

	private BigDecimal getTaxesTransferred(final MonedaEntity currency,
										   BigDecimal conceptTaxTransferred,
										   Integer conceptNumber, 
										   final WConcept item,
										   final ApiConcept apiConcept) {
		if (item.getTaxesTransferred() != null && !item.getTaxesTransferred().isEmpty()) {
			val transferredList = new ArrayList<ApiConceptTaxes>();
			for (val itemTransferred : item.getTaxesTransferred()) {
				val transferred = new ApiConceptTaxes();  
				transferred.setBase(UValue.bigDecimalString(itemTransferred.getBase()));
				transferred.setBaseBigDecimal(itemTransferred.getBase());
				transferred.setTax(itemTransferred.getTax());											
				transferred.setRateOrFee(UValue.bigDecimalString(itemTransferred.getRateOrFee()));
				transferred.setRateOrFeeBigDecimal(itemTransferred.getRateOrFee());
				transferred.setFactor(itemTransferred.getFactorType().value());
				transferred.setFactorType(itemTransferred.getFactorType());
				transferred.setAmount(UValue.bigDecimalString(itemTransferred.getAmount()));
				transferred.setAmountBigDecimal(itemTransferred.getAmount());
				transferred.setConceptNumber(UValue.integerString(conceptNumber));
				transferredList.add(transferred);
				
				conceptTaxTransferred = getConceptTaxTransferred(currency, conceptTaxTransferred, itemTransferred);
			}
			apiConcept.getTaxesTransferred().addAll(transferredList);
		}
		return conceptTaxTransferred;
	}

	private BigDecimal getConceptTaxTransferred(final MonedaEntity currency, 
												BigDecimal conceptTaxTransferred,
												final WTaxes itemTransferred) {
		if (!itemTransferred.getFactorType().value().equalsIgnoreCase("exento")) {
			if (!UValidator.isNullOrEmpty(itemTransferred.getAmount())) {
				if (UValidator.isNullOrEmpty(conceptTaxTransferred)) {
					conceptTaxTransferred = UValue.bigDecimal(currency.getDecimales());
				}
				conceptTaxTransferred = conceptTaxTransferred.add(itemTransferred.getAmount());
			}
		} else {
			if (UValidator.isNullOrEmpty(conceptTaxTransferred)) {
				conceptTaxTransferred = UValue.bigDecimal(currency.getDecimales());
			} else {
				conceptTaxTransferred = conceptTaxTransferred.add(UValue.bigDecimal(currency.getDecimales()));
			}
		}
		return conceptTaxTransferred;
	}
	
	private BigDecimal getTaxesWithheld(final MonedaEntity currency,
										BigDecimal conceptTaxWithheld, 
										Integer conceptNumber,
										final WConcept item,
										final ApiConcept apiConcept) {
		if (item.getTaxesWithheld() != null && !item.getTaxesWithheld().isEmpty()) {
			val withheldList = new ArrayList<ApiConceptTaxes>();
			for (val itemWithheld : item.getTaxesWithheld()) {
				val withheld = new ApiConceptTaxes();  
				withheld.setBase(UValue.bigDecimalString(itemWithheld.getBase()));
				withheld.setBaseBigDecimal(itemWithheld.getBase());
				withheld.setTax(itemWithheld.getTax());											
				withheld.setRateOrFee(UValue.bigDecimalString(itemWithheld.getRateOrFee()));
				withheld.setRateOrFeeBigDecimal(itemWithheld.getRateOrFee());
				withheld.setFactor(itemWithheld.getFactorType().value());
				withheld.setFactorType(itemWithheld.getFactorType());
				withheld.setAmount(UValue.bigDecimalString(itemWithheld.getAmount()));
				withheld.setAmountBigDecimal(itemWithheld.getAmount());
				withheld.setConceptNumber(UValue.integerString(conceptNumber));
				withheldList.add(withheld);
				
				if (!UValidator.isNullOrEmpty(itemWithheld.getAmount())) {
					if (UValidator.isNullOrEmpty(conceptTaxWithheld)) {
						conceptTaxWithheld = UValue.bigDecimal(currency.getDecimales());
					}
					conceptTaxWithheld = conceptTaxWithheld.add(itemWithheld.getAmount());
				}
			}
			apiConcept.getTaxesWithheld().addAll(withheldList);
		}
		return conceptTaxWithheld;
	}
	
	private void setCustomsInformation(final List<ConceptCustomsInformation> conceptCustomsInformations,
									   Integer conceptNumber, 
									   final WConcept item,
									   final ApiConcept apiConcept) {
		if (item.getCustomsInformation() != null && !item.getCustomsInformation().isEmpty()) {
			for (val itemCustomsInformation : item.getCustomsInformation()) {
				val customsInformation = new InformacionAduanera();
				customsInformation.setNumeroPedimento(itemCustomsInformation.getRequirementNumber());
				apiConcept.getCustomsInformation().add(customsInformation);
				
				val conceptCustomsInformation = new ConceptCustomsInformation();
				conceptCustomsInformation.setConceptNumber(UValue.integerString(conceptNumber));
				conceptCustomsInformation.setNumber(itemCustomsInformation.getRequirementNumber());
				conceptCustomsInformations.add(conceptCustomsInformation);
			}
		}
	}
	
	private void setPropertyAccount(final List<ConceptPropertyAccount> conceptPropertyAccounts,
									Integer conceptNumber, 
									final WConcept item,
									final ApiConcept apiConcept) {
		if (item.getPropertyAccount() != null) {
			val propertyAccount = new CuentaPredial();
			propertyAccount.setNumero(item.getPropertyAccount().getNumber());
			apiConcept.setPropertyAccount(propertyAccount);
			
			val conceptPropertyAccount = new ConceptPropertyAccount();
			conceptPropertyAccount.setConceptNumber(UValue.integerString(conceptNumber));
			conceptPropertyAccount.setNumber(item.getPropertyAccount().getNumber());
			conceptPropertyAccounts.add(conceptPropertyAccount);
		}
	}
	
	private void setIeduComplement(final WConcept item, final ApiConcept apiConcept) {
		val wIedu = item.getIedu();
		if (item.getIedu() != null) {
			val iedu = new InstEducativas();
			iedu.setVersion(COMPLEMENT_IEDU_VERSION);
			iedu.setNombreAlumno(wIedu.getStudentName());
			iedu.setCURP(wIedu.getCurp());
			iedu.setNivelEducativo(wIedu.getEducationLevel());
			iedu.setAutRVOE(wIedu.getAutRVOE());
			iedu.setRfcPago(!UValidator.isNullOrEmpty(wIedu.getRfc()) ? wIedu.getRfc() : null);
			apiConcept.setIedu(iedu);
		}
	}
	
	private ApiVoucherTaxes getConceptTax(BigDecimal conceptTaxTransferred, BigDecimal conceptTaxWithheld) {
		val conceptsTax = new ApiVoucherTaxes();
		
		if (!UValidator.isNullOrEmpty(conceptTaxTransferred)) {
			conceptsTax.setTotalTaxTransferred(conceptTaxTransferred);
		}
		
		if (!UValidator.isNullOrEmpty(conceptTaxWithheld)) {
			conceptsTax.setTotalTaxWithheld(conceptTaxWithheld);
		}
		
		return conceptsTax;
	}

}