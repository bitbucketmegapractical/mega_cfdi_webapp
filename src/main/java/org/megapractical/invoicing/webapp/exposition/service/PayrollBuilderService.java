package org.megapractical.invoicing.webapp.exposition.service;

import static org.megapractical.invoicing.api.util.MoneyUtils.formatStrict;

import org.megapractical.common.datetime.DateUtils;
import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.sat.complement.payroll.Nomina;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Complemento;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.AllowStamp;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class PayrollBuilderService {

	private final VoucherBuilderService voucherBuilderService;
	private final PayrollService payrollService;

	public AllowStamp build(ApiPayroll apiPayroll, boolean useDateByPostalCode) {
		try {
			val curp = apiPayroll.getPayrollReceiver().getCurp();
			val serie = UValue.stringValue(apiPayroll.getApiCfdi().getVoucher().getSerie());
			val folio = UValue.stringValue(apiPayroll.getApiCfdi().getVoucher().getFolio());
			val allowStamp = payrollService.verifyPayroll(curp, serie, folio);
			if (allowStamp.isAllowedToStamp()) {
				val voucher = new Comprobante();
				val payroll = new Nomina();
				val apiCfdi = apiPayroll.getApiCfdi();

				voucherBuilderService.populateVoucher(apiCfdi, voucher, useDateByPostalCode);
				voucherBuilderService.populateEmitter(apiCfdi, voucher);
				voucherBuilderService.populateReceiver(apiCfdi, voucher);
				populateConcepts(apiCfdi, voucher);
				populatePayroll(apiPayroll, payroll);
				populatePayrollEmitter(apiPayroll, payroll);
				populatePayrollReceiver(apiPayroll, payroll);
				populatePayrollPerceptions(apiPayroll, payroll);
				populatePensionRetirement(apiPayroll, payroll);
				populatePayrollDeductions(apiPayroll, payroll);
				populatePayrollOtherPayments(apiPayroll, payroll);
				
				val complemento = new Complemento();
		        complemento.getAnies().add(payroll);
		        voucher.getComplementos().add(complemento);
		        
		        allowStamp.setVoucher(voucher);
				return allowStamp;
			}
			return allowStamp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AllowStamp.notAllowed();
	}

	private void populateConcepts(ApiCfdi apiCfdi, final Comprobante voucher) {
		val concepts = new Comprobante.Conceptos();
		for (val item : apiCfdi.getConcepts()) {
			val concepto = new Comprobante.Conceptos.Concepto();
			concepto.setClaveProdServ(UValue.stringValue(item.getKeyProductServiceCode()));
			concepto.setCantidad(item.getQuantity());
			concepto.setClaveUnidad(UValue.stringValue(item.getMeasurementUnitCode()));
			concepto.setDescripcion(UValue.stringValue(UValue.singleReplace(item.getDescription())));
			concepto.setValorUnitario(item.getUnitValue());
			concepto.setImporte(item.getAmount());
			concepto.setObjetoImp(item.getObjImp());
			concepto.setNoIdentificacion(UValue.stringValue(item.getIdentificationNumber()));
			concepto.setUnidad(UValue.stringValue(item.getUnit()));
			concepto.setDescuento(UValue.getBigDecimal(item.getDiscount()));
			concepts.getConceptos().add(concepto);
		}
		voucher.setConceptos(concepts);
	}

	private void populatePayroll(ApiPayroll apiPayroll, final Nomina payroll) {
		payroll.setVersion(ApiPayroll.VERSION);
		payroll.setTipoNomina(UCatalog.payrollType(apiPayroll.getPayrollTypeCode()));
		payroll.setFechaPago(DateUtils.format(apiPayroll.getPaymentDate()));
		payroll.setFechaInicialPago(DateUtils.format(apiPayroll.getPaymentIntialDate()));
		payroll.setFechaFinalPago(DateUtils.format(apiPayroll.getPaymentEndDate()));
		payroll.setNumDiasPagados(formatStrict(apiPayroll.getNumberDaysPaid()));
		payroll.setTotalPercepciones(formatStrict(apiPayroll.getTotalPerceptions()));
		payroll.setTotalDeducciones(formatStrict(apiPayroll.getTotalDeductions()));
		payroll.setTotalOtrosPagos(formatStrict(apiPayroll.getTotalOtherPayments()));
	}

	private void populatePayrollEmitter(ApiPayroll apiPayroll, final Nomina payroll) {
		val apiPayrollEmitter = apiPayroll.getPayrollEmitter();
		val payrollEmitter = new Nomina.Emisor();
		
		if (AssertUtils.nonNull(apiPayrollEmitter.getEmployerRegistration())) {
			payrollEmitter.setRegistroPatronal(apiPayrollEmitter.getEmployerRegistration());
		}
		
		if (AssertUtils.nonNull(apiPayrollEmitter.getRfcPatronOrigin())) {
			payrollEmitter.setRfcPatronOrigen(apiPayrollEmitter.getRfcPatronOrigin());
		}
		payroll.setEmisor(payrollEmitter);
	}

	private void populatePayrollReceiver(ApiPayroll apiPayroll, final Nomina payroll) {
		val apiPayrollReceiver = apiPayroll.getPayrollReceiver();

		val payrollReceiver = new Nomina.Receptor();
		payrollReceiver.setCurp(apiPayrollReceiver.getCurp());
		payrollReceiver.setNumSeguridadSocial(apiPayrollReceiver.getSocialSecurityNumber());
		payrollReceiver
				.setFechaInicioRelLaboral(DateUtils.format(apiPayrollReceiver.getStartDateEmploymentRelationship()));
		payrollReceiver.setAntigüedad(apiPayrollReceiver.getAge());
		payrollReceiver.setTipoContrato(apiPayrollReceiver.getContractTypeCode());
		payrollReceiver.setSindicalizado(apiPayrollReceiver.getUnionized());
		payrollReceiver.setTipoJornada(apiPayrollReceiver.getDayTypeCode());
		payrollReceiver.setTipoRegimen(apiPayrollReceiver.getRegimeTypeCode());
		payrollReceiver.setNumEmpleado(apiPayrollReceiver.getEmployeeNumber());
		payrollReceiver.setDepartamento(apiPayrollReceiver.getDepartment());
		payrollReceiver.setPuesto(apiPayrollReceiver.getJobTitle());
		payrollReceiver.setRiesgoPuesto(apiPayrollReceiver.getJobRiskCode());
		payrollReceiver.setPeriodicidadPago(apiPayrollReceiver.getPaymentFrequencyCode());
		payrollReceiver.setBanco(apiPayrollReceiver.getBankCode());
		payrollReceiver.setCuentaBancaria(apiPayrollReceiver.getBankAccount());
		payrollReceiver.setSalarioBaseCotApor(formatStrict(apiPayrollReceiver.getShareContributionBaseSalary()));
		payrollReceiver.setSalarioDiarioIntegrado(formatStrict(apiPayrollReceiver.getIntegratedDailySalary()));
		payrollReceiver.setClaveEntFed(UCatalog.estate(apiPayrollReceiver.getFederalEntityCode()));

		payroll.setReceptor(payrollReceiver);
	}

	private void populatePayrollPerceptions(ApiPayroll apiPayroll, final Nomina payroll) {
		val perception = apiPayroll.getPayrollPerception();
		if (AssertUtils.nonNull(perception)) {
			val payrollPerceptions = new Nomina.Percepciones();
			payrollPerceptions.setTotalSueldos(formatStrict(perception.getTotalSalaries()));
			payrollPerceptions.setTotalSeparacionIndemnizacion(formatStrict(perception.getTotalSeparationCompensation()));
			payrollPerceptions.setTotalJubilacionPensionRetiro(formatStrict(perception.getTotalPensionRetirement()));
			payrollPerceptions.setTotalGravado(formatStrict(perception.getTotalTaxed()));
			payrollPerceptions.setTotalExento(formatStrict(perception.getTotalExempt()));
			
			val perceptions = apiPayroll.getPayrollPerception().getPayrollPerceptions();
			perceptions.forEach(item -> {
				val payrollPerception = new Nomina.Percepciones.Percepcion();
				payrollPerception.setTipoPercepcion(item.getPerceptionTypeCode());
				payrollPerception.setClave(item.getKey());
				payrollPerception.setConcepto(item.getConcept());
				payrollPerception.setImporteGravado(formatStrict(item.getTaxableAmount()));
				payrollPerception.setImporteExento(formatStrict(item.getExemptAmount()));
				
				payrollPerceptions.getPercepcions().add(payrollPerception);
			});
			
			payroll.setPercepciones(payrollPerceptions);
		}
	}
	
	private void populatePensionRetirement(ApiPayroll apiPayroll, final Nomina payroll) {
		val pensionRetirement = apiPayroll.getPayrollPerception().getPayrollPensionRetirement();
		if (AssertUtils.nonNull(pensionRetirement)) {
			val payrollPensionRetirement = new Nomina.Percepciones.JubilacionPensionRetiro();
			if (AssertUtils.nonNull(pensionRetirement.getTotalExhibition())) {
				payrollPensionRetirement.setTotalUnaExhibicion(formatStrict(pensionRetirement.getTotalExhibition()));
			}
			
			if (AssertUtils.nonNull(pensionRetirement.getTotalPartiality())) {
				payrollPensionRetirement.setTotalParcialidad(formatStrict(pensionRetirement.getTotalPartiality()));
			}
			
			if (AssertUtils.nonNull(pensionRetirement.getDailyAmount())) {
				payrollPensionRetirement.setMontoDiario(formatStrict(pensionRetirement.getDailyAmount()));
			}
			
			if (AssertUtils.nonNull(pensionRetirement.getIncomeAccumulate())) {
				payrollPensionRetirement.setIngresoAcumulable(formatStrict(pensionRetirement.getIncomeAccumulate()));
			}
			
			if (AssertUtils.nonNull(pensionRetirement.getIncomeNoAccumulate())) {
				payrollPensionRetirement.setIngresoNoAcumulable(formatStrict(pensionRetirement.getIncomeNoAccumulate()));
			}
			payroll.getPercepciones().setJubilacionPensionRetiro(payrollPensionRetirement);
		}
	}
	
	private void populatePayrollDeductions(ApiPayroll apiPayroll, final Nomina payroll) {
		val deduction = apiPayroll.getPayrollDeduction();
		if (AssertUtils.nonNull(deduction)) {
			val payrollDeductions = new Nomina.Deducciones();
			payrollDeductions.setTotalOtrasDeducciones(formatStrict(deduction.getTotalOtherDeductions()));
			payrollDeductions.setTotalImpuestosRetenidos(formatStrict(deduction.getTotalTaxesWithheld()));
			
			val deductions = apiPayroll.getPayrollDeduction().getPayrollDeductions();
			deductions.forEach(item -> {
				val payrollDeduction = new Nomina.Deducciones.Deduccion();
				payrollDeduction.setTipoDeduccion(item.getDeductionTypeCode());
				payrollDeduction.setClave(item.getKey());
				payrollDeduction.setConcepto(item.getConcept());
				payrollDeduction.setImporte(formatStrict(item.getAmount()));
				
				payrollDeductions.getDeduccions().add(payrollDeduction);
			});
			
			payroll.setDeducciones(payrollDeductions);
		}
	}
	
	private void populatePayrollOtherPayments(ApiPayroll apiPayroll, final Nomina payroll) {
		if (AssertUtils.nonNull(apiPayroll.getPayrollOtherPayments())) {
			val otherPayments = new Nomina.OtrosPagos();
			apiPayroll.getPayrollOtherPayments().forEach(op -> {
				val payrollOtherPayment = new Nomina.OtrosPagos.OtroPago();
				payrollOtherPayment.setTipoOtroPago(op.getOtherPaymentTypeCode());
				payrollOtherPayment.setClave(op.getKey());
				payrollOtherPayment.setConcepto(op.getConcept());
				payrollOtherPayment.setImporte(formatStrict(op.getAmount()));
				
				if (op.getEmploymentSubsidy() != null) {
					val employmentSubsidy = new Nomina.OtrosPagos.OtroPago.SubsidioAlEmpleo();
					employmentSubsidy.setSubsidioCausado(formatStrict(op.getEmploymentSubsidy().getSubsidyCaused()));
					payrollOtherPayment.setSubsidioAlEmpleo(employmentSubsidy);
				}
				
				otherPayments.getOtroPagos().add(payrollOtherPayment);
			});
			payroll.setOtrosPagos(otherPayments);
		}
	}

}