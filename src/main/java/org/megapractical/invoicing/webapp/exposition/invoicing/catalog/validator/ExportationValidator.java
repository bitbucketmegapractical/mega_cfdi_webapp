package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.ExportationValidatorResponse;

public interface ExportationValidator {
	
	ExportationValidatorResponse validate(String exportation);
	
	ExportationValidatorResponse validate(String exportation, String field);
	
}