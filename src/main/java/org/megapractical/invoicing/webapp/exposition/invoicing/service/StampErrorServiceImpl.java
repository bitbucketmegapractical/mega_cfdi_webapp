package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.util.ArrayList;
import java.util.Arrays;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.webapp.json.JCfdiStampError;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class StampErrorServiceImpl implements StampErrorService {
	
	@Autowired
	private UserTools userTools;
	
	private static final String CONFIRMATION_ERROR_CODE = "CFDI33119";
	
	@Override
	public JResponse unexpectedError() {
		val error = JError.message("ERR0037", userTools.getLocale());
		val errors = Arrays.asList(error);
		val notification = JNotification.pageMessageError(error.getMessage());
		
		return JResponse.error(notification, errors);
	}
	
	@Override
	public JResponse stampError(StampResponse stampResponse) {
		val validator = Validator.validate(stampResponse);
		JNotification notification = null;
		val errors = new ArrayList<JError>();
		
		val error = JError.message(validator.getErrorDescription(), userTools.getLocale());
		if (UValidator.isNullOrEmpty(error.getMessage())) {
			if (!validator.getErrorMessage().equalsIgnoreCase("Error No Definido")) {
				error.setCode(validator.getErrorCode());
				error.setMessage(validator.getErrorMessage());
			} else {
				error.setMessage(UProperties.getMessage("ERR0038", userTools.getLocale()));
			}
		}
		
		val errorCode = validator.getErrorCode();
		if (errorCode.equals("CFDI33117") || errorCode.equals(CONFIRMATION_ERROR_CODE)) {
			error.setCode(errorCode);
			
			val stampError = determineError(errorCode);
			if (stampError != null) {
				error.setField(stampError.getField());
			}
		} else {
			notification = JNotification.pageMessageError(error.getMessage());
		}
		errors.add(error);
		
		return JResponse.tabError(validator.getActivateTab(), notification, errors);
	}
	
	@Override
	public JResponse swStampError(StampResponse stampResponse) {
		val validator = Validator.validate(stampResponse);
		var errorMessage = validator.getErrorMessage();
		if (!errorMessage.startsWith("Error")) {
			errorMessage = "Error " + errorMessage;
		}
		return JResponse.tabError(validator.getActivateTab(),
				JNotification.pageMessageError(errorMessage));
	}
	
	private JCfdiStampError determineError(String errorCode) {
		if (!UValidator.isNullOrEmpty(errorCode) && (errorCode.equals("CFDI33117") || errorCode.equals(CONFIRMATION_ERROR_CODE))) {
			return JCfdiStampError
					.builder()
					.code(errorCode)
					.message(UProperties.getMessage(CONFIRMATION_ERROR_CODE, userTools.getLocale()))
					.field("confirmation-code")
					.tab("tab-voucher")
					.build();
		}
		return null;
	}

}