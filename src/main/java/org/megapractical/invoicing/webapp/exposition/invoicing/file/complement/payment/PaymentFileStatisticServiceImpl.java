package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import java.util.Date;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiPaymentParser;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;
import org.megapractical.invoicing.dal.data.repository.IInvoiceFileStatisticsJpaRepository;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.val;
import lombok.var;

@Service
@Transactional
public class PaymentFileStatisticServiceImpl implements PaymentFileStatisticService {

	private final IInvoiceFileStatisticsJpaRepository iInvoiceFileStatisticsJpaRepository;
	private final IPersistenceDAO persistenceDAO;

	public PaymentFileStatisticServiceImpl(IInvoiceFileStatisticsJpaRepository iInvoiceFileStatisticsJpaRepository,
			IPersistenceDAO persistenceDAO) {
		this.iInvoiceFileStatisticsJpaRepository = iInvoiceFileStatisticsJpaRepository;
		this.persistenceDAO = persistenceDAO;
	}

	@Override
	public ArchivoFacturaEstadisticaEntity managePaymentFileStatistics(ApiPaymentParser paymentParser) {
		// ##### Verificando si el archivo tiene estadisticas generadas
		val invoiceFile = paymentParser.getPaymentStamp().getInvoiceFile();
		var paymentFileStatistics = iInvoiceFileStatisticsJpaRepository.findByArchivoFactura(invoiceFile);
		if (UValidator.isNullOrEmpty(paymentFileStatistics)) {
			// Registrando estadisticas del archivo
			paymentFileStatistics = paymentParser.getStatistics().getInvoiceFileStatistics();
			paymentFileStatistics.setArchivoFactura(paymentParser.getPaymentStamp().getInvoiceFile());
			paymentFileStatistics.setFechaInicioTimbrado(new Date());
			paymentFileStatistics.setHoraInicioTimbrado(new Date());
			return (ArchivoFacturaEstadisticaEntity) persistenceDAO.persist(paymentFileStatistics);
		} else {
			// Actualizando estadisticas del archivo
			val statistics = paymentParser.getStatistics().getInvoiceFileStatistics();
			paymentFileStatistics.setFacturas(statistics.getFacturas());
			paymentFileStatistics.setFacturasTimbradas(statistics.getFacturasTimbradas());
			paymentFileStatistics.setFacturasError(statistics.getFacturasError());
			paymentFileStatistics.setFechaInicioAnalisis(statistics.getFechaInicioAnalisis());
			paymentFileStatistics.setHoraInicioAnalisis(statistics.getHoraInicioAnalisis());
			paymentFileStatistics.setFechaFinAnalisis(statistics.getFechaFinAnalisis());
			paymentFileStatistics.setHoraFinAnalisis(statistics.getHoraFinAnalisis());
			paymentFileStatistics.setFechaInicioGeneracionComprobante(statistics.getFechaInicioGeneracionComprobante());
			paymentFileStatistics.setHoraInicioGeneracionComprobante(statistics.getHoraInicioGeneracionComprobante());
			paymentFileStatistics.setFechaFinGeneracionComprobante(statistics.getFechaFinGeneracionComprobante());
			paymentFileStatistics.setHoraFinGeneracionComprobante(statistics.getHoraFinGeneracionComprobante());
			paymentFileStatistics.setFechaInicioTimbrado(new Date());
			paymentFileStatistics.setHoraInicioTimbrado(new Date());
			return (ArchivoFacturaEstadisticaEntity) persistenceDAO.update(paymentFileStatistics);
		}
	}

}