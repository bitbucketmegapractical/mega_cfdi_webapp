package org.megapractical.invoicing.webapp.exposition.invoicing;


import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.client.ConfirmationCode;
import org.megapractical.invoicing.api.client.ConfirmationCodeProperties;
import org.megapractical.invoicing.api.client.ConfirmationCodeResponse;
import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCalculation;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiCmp;
import org.megapractical.invoicing.dal.bean.jpa.ClaveProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ClienteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteConceptoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyentePlantillaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.bean.jpa.NivelEducativoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.TasaCuotaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UnidadMedidaEntity;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiRelationshipTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiUseJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IComplementJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IContactJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICountryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICredentialJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICustomerJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IEducationLevelJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IFactorTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IKeyProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IMeasurementUnitJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IObjetoImpuestoJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentMethodJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentWayJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPostalCodeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPreferencesJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRateOrFeeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStringTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxRegimeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerConceptJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerPostalCode;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerSerieSheetJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTemplateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IVoucherTypeJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.PaymentRelatedDocumentService;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.PaymentService;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherConceptRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherTotalsRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.InvoiceAutomaticCalculationService;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.InvoiceConceptAmountCalcService;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.InvoiceService;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.InvoiceStoreService;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.RateOrFeeService;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.StampErrorService;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.TaxService;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.emitter.EmitterValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.receiver.ReceiverValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.TaxRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher.VoucherConceptValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher.VoucherTotalsValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher.VoucherValidator;
import org.megapractical.invoicing.webapp.exposition.service.CfdiBuilderService;
import org.megapractical.invoicing.webapp.exposition.service.CfdiFakeStampService;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.exposition.service.customer.CustomerService;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JCalculation;
import org.megapractical.invoicing.webapp.json.JComplement;
import org.megapractical.invoicing.webapp.json.JComplement.Complement;
import org.megapractical.invoicing.webapp.json.JComplementPayment;
import org.megapractical.invoicing.webapp.json.JConcept;
import org.megapractical.invoicing.webapp.json.JConcept.Iedu;
import org.megapractical.invoicing.webapp.json.JConfirmationCode;
import org.megapractical.invoicing.webapp.json.JCustomer;
import org.megapractical.invoicing.webapp.json.JEmitter.JEmitterCertificateRequest;
import org.megapractical.invoicing.webapp.json.JEmitterConcept;
import org.megapractical.invoicing.webapp.json.JEmitterConcept.Concept;
import org.megapractical.invoicing.webapp.json.JEmitterProperties;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JRateOrFee;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JSerieSheet;
import org.megapractical.invoicing.webapp.json.JSerieSheet.SerieSheet;
import org.megapractical.invoicing.webapp.json.JSerieSheet.SerieSheetOptions;
import org.megapractical.invoicing.webapp.json.JStamp;
import org.megapractical.invoicing.webapp.json.JTaxes.JTax;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.EmitterTools;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.megapractical.invoicing.webapp.wrapper.WAutomaticCalculations;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPaymentTotal;
import org.megapractical.invoicing.webapp.wrapper.WConcept;
import org.megapractical.invoicing.webapp.wrapper.WCustomsInformation;
import org.megapractical.invoicing.webapp.wrapper.WIedu;
import org.megapractical.invoicing.webapp.wrapper.WPropertyAccount;
import org.megapractical.invoicing.webapp.wrapper.WTaxes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import lombok.val;
import lombok.var;

@Controller
@Scope("session")
public class InvoicingController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	EmitterTools emitterTools;
	
	@Autowired
	PropertiesTools propertiesTools; 
	
	@Autowired
	CfdiBuilderService cfdiBuilder; 
	
	@Autowired
	ReportManager reportManager;
	
	@Autowired
	DataStoreService dataStore;
	
	@Autowired
	StampControlService stampControl;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private EmitterValidator emitterValidator;
	
	@Autowired
	private ReceiverValidator receiverValidator;
	
	@Autowired
	private VoucherValidator voucherValidator;
	
	@Autowired
	private VoucherConceptValidator voucherConceptValidator;
	
	@Autowired
	private VoucherTotalsValidator voucherTotalsValidator;
	
	@Autowired
	private PaymentService paymentService;
	
	@Autowired
	private PaymentRelatedDocumentService paymentRelatedDocumentService;
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private CfdiFakeStampService cfdiFakeStampService;
	
	@Autowired
	private StampErrorService stampErrorService;
	
	@Autowired
	private InvoiceStoreService invoiceStoreService;
	
	@Autowired
	private InvoiceConceptAmountCalcService invoiceConceptAmountCalcService;
	
	@Autowired
	private TaxService taxService;
	
	@Autowired
	private RateOrFeeService rateOrFeeService;
	
	@Autowired
	private InvoiceAutomaticCalculationService invoiceAutomaticCalculationService;
	
	@Resource
	private ITaxRegimeJpaRepository iTaxRegimeJpaRepository;
	
	@Resource
	private IVoucherTypeJpaRepository iVoucherTypeJpaRepository; 
	
	@Resource
	private IPaymentWayJpaRepository iPaymentWayJpaRepository;
	
	@Resource
	private IPaymentMethodJpaRepository iPaymentMethodJpaRepository;
	
	@Resource
	private ICurrencyJpaRepository iCurrencyJpaRepository;
	
	@Resource
	private ICfdiUseJpaRepository iCfdiUseJpaRepository;
	
	@Resource
	private IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository; 
	
	@Resource
	private IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository;
	
	@Resource
	private ICountryJpaRepository iCountryJpaRepository;
	
	@Resource
	private IPostalCodeJpaRepository iPostalCodeJpaRepository;
	
	@Resource
	private IRateOrFeeJpaRepository iRateOrFeeJpaRepository;
	
	@Resource
	ITaxTypeJpaRepository iTaxTypeJpaRepository;
	
	@Resource
	IFactorTypeJpaRepository iFactorTypeJpaRepository;
	
	@Resource
	ICfdiRelationshipTypeJpaRepository iCfdiRelationshipType;
	
	@Resource
	IPersonTypeJpaRepository iPersonTypeJpaRepository;
	
	@Resource
	ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	
	@Resource
	ICustomerJpaRepository iCustomerJpaRepository;
	
	@Resource
	ITaxpayerConceptJpaRepository iTaxpayerConceptJpaRepository;
	
	@Resource
	IPreferencesJpaRepository iPreferencesJpaRepository;
	
	@Resource
	ITaxpayerPersonTypeJpaRepository iTaxpayerPersonTypeJpaRepository;
	
	@Resource
	ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;
	
	@Resource
	IContactJpaRepository iContactJpaRepository;
	
	@Resource
	ICredentialJpaRepository iCredentialJpaRepository; 
	
	@Resource
	IStampJpaRepository iStampJpaRepository;
	
	@Resource
	ITaxpayerSerieSheetJpaRepository iTaxpayerSerieSheetJpaRepository;
	
	@Resource
	ICfdiJpaRepository iCfdiJpaRepository;
	
	@Resource
	IComplementJpaRepository iComplementJpaRepository;
	
	@Resource
	IStringTypeJpaRepository iStringTypeJpaRepository;
	
	@Resource
	IAccountJpaRepository iAccountJpaRepository;
	
	@Resource
	ITaxpayerPostalCode iTaxpayerPostalCode; 
	
	@Resource
	ITaxpayerTemplateJpaRepository iTaxpayerTemplateJpaRepository;
	
	@Resource
	IEducationLevelJpaRepository iEducationLevelJpaRepository;
	
	@Autowired
	private IObjetoImpuestoJpaRepository iObjetoImpuestoJpaRepository;
	
	//##### Formato de fecha
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	
	//##### Expresion regular para validar RFC
	private static final String RFC_EXPRESSION = "[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?";
	private static final Integer RFC_MIN_LENGTH = 12;
	private static final Integer RFC_MAX_LENGTH = 13;
	
	//##### Expresion regular para validar el CURP
	private static final String CURP_EXPRESSION = "[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]";
	private static final Integer CURP_LENGTH = 12;
	
	//##### Listado de residente
	private static final List<String> RESIDENT = Arrays.asList("Nacional", "Extranjero");
	
	//##### Listado de tipo de factor
	private static final List<String> FACTOR_TYPE = Arrays.asList("Tasa", "Cuota", "Exento");
	
	//##### Redondeo
	private static final RoundingMode roundingMode = RoundingMode.HALF_EVEN;
	
	//##### Version complemento entidades educativas privadas
	private static final String COMPLEMENT_IEDU_VERSION = "1.0";
	
	private static final String CODE_VALUE_FORMAT = "%s (%s)";
	
	//##### Propiedades del emisor
	private JEmitterProperties emitterProperties;
	
	//##### Wrapper de conceptos 
	private List<WConcept> wConcepts = new ArrayList<>();
	
	//##### Tasa o cuota
	private JRateOrFee rateOrFee;
	
	//##### Contribuyente
	private ContribuyenteEntity taxpayer;
	
	//##### Certificado del emisor
	private ContribuyenteCertificadoEntity certificate;
	
	//##### Listado de clientes del emisor
	private List<ClienteEntity> customers;
	private List<String> customersByRfc;
	private List<String> customersByNameOrBusinessName;
	
	//##### Listado de conceptos del emisor
	private List<ContribuyenteConceptoEntity> concepts;
	private List<String> conceptsByDescription;
	private Integer conceptIndex;
	
	//##### Preferencias del usuario
	private PreferenciasEntity preferences;
	
	//##### Tipo de moneda del emisor
	private MonedaEntity currency;
	private String currencyValue;
	
	//##### Tipo de contribuyente (Tipo persona)
	private TipoPersonaEntity personType;
	private String personTypeValue;
	
	//##### Regimen fiscal del emisor
	private ContribuyenteRegimenFiscalEntity taxRegime;
	private String taxRegimeValue;
	
	//##### Calculos automaticos
	private WAutomaticCalculations wAutomaticCalculations; 
	
	//##### Complemento pago
	private WPaymentTotal wPaymentTotal;
	private List<WPayment> payments = new ArrayList<>();
	private Integer paymentIndex;
	
	private boolean useFakeStampService;
	
	//##### Mensaje campo requerido
	@ModelAttribute("requiredMessage")
	public String requiredMessage() {
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	//##### Mensaje procesando...
	@ModelAttribute("processingRequest")
	public String processingMessage() {
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	//##### Mensaje cargando...
	@ModelAttribute("loadingRequest")
	public String loadingMessage() {
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	//##### Mensaje formato incorrecto
	@ModelAttribute("invalidDataType")
	public String invalidDataTypeMessage() {
		return UProperties.getMessage("ERR1003", userTools.getLocale());
	}
	
	//##### Cargando residencia
	@ModelAttribute("residenteSource")
	public List<String> residenteSource() {
		return RESIDENT;
	}
	
	//##### Cargando tipo factor
	@ModelAttribute("factorTypeSource")
	public List<String> factorTypeSource() {
		return FACTOR_TYPE;
	}
	
	@ModelAttribute("educationLevelSource")
	public List<String> educationLevelSource() {
		val levelSource = iEducationLevelJpaRepository.findByEliminadoFalse();
		return levelSource.stream().map(NivelEducativoEntity::getValor).collect(toList());
	}
	
	// ##### Cargando objeto impuesto
	@ModelAttribute("objTaxSource")
	public List<String> objTaxSource() {
		val objImpSource = iObjetoImpuestoJpaRepository.findByEliminadoFalse();
		return objImpSource.stream()
						   .map(item -> String.format(CODE_VALUE_FORMAT, item.getCodigo(), item.getValor()))
						   .collect(toList());
	}
	
	// ##### Cargando tipo de impuesto
	public List<String> taxTypeSourceRequest() {
		val taxTypeSource = iTaxTypeJpaRepository.findByEliminadoFalse();
		return taxTypeSource.stream()
							.map(item -> String.format(CODE_VALUE_FORMAT, item.getCodigo(), item.getValor()))
							.collect(toList());
	}
	
	//##### Cargando catalogo impuestos
	public List<TasaCuotaEntity> rateOrFeeSourceRequest() {
		return iRateOrFeeJpaRepository.findByEliminadoFalse();			
	}
	
	// ##### Cargando tipo persona
	public List<String> personTypeSourceRequest() {
		val personTypeSource = iPersonTypeJpaRepository.findByEliminadoFalse();
		return personTypeSource.stream().map(TipoPersonaEntity::getValor).collect(toList());
	}
	
	//##### Cargando cfdi tipo relacion
	public List<String> cfdiRelationshipTypeSourceRequest() {
		val cfdiRelationshipTypeSource = iCfdiRelationshipType.findByEliminadoFalse();
		return cfdiRelationshipTypeSource.stream()
										 .map(item -> String.format(CODE_VALUE_FORMAT, item.getCodigo(), item.getValor()))
										 .collect(toList());
	}
	
	//##### Actualizando moneda del usuario
	@RequestMapping(value = "/currencyUpdate", method = RequestMethod.POST)
	public @ResponseBody String currencyUpdate(HttpServletRequest request) {
		try {
			
			// Moneda
			String currency = UBase64.base64Decode(request.getParameter("currency"));
			MonedaEntity currencyEntityByCode = null;
			
			if (!UValidator.isNullOrEmpty(currency)) {
				// Validar que sea un valor de catalogo
				String[] currencyCatalog = UCatalog.getCatalog(currency);
				currency = currencyCatalog[0];
				String currencyValue = currencyCatalog[1];
				
				currencyEntityByCode = iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(currency);
				MonedaEntity currencyEntityByValue = iCurrencyJpaRepository.findByValorAndEliminadoFalse(currencyValue);
				
				if (UValidator.isNullOrEmpty(currencyEntityByCode)  || UValidator.isNullOrEmpty(currencyEntityByValue)) {
					this.currency = null;
					this.currencyValue = null;
				} else {
					this.currency = new MonedaEntity();
					this.currency = currencyEntityByCode;
					this.currencyValue = this.currency.getCodigo().concat(" (").concat(this.currency.getValor()).concat(")");
				}
			} else {
				this.currency = null;
				this.currencyValue = null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		return !UValidator.isNullOrEmpty(this.currency) ? UBase64.base64Encode(this.currencyValue) : UBase64.base64Encode("INVALID_CURRENCY");
	}
	
	//##### Cargando clientes
	@PostMapping("/customersByEmitter")
	@ResponseBody
	public String customersByEmitter() {
		val mappedCustomers = customerService.getMappedCustomers(this.customers);
		val customer = new JCustomer(mappedCustomers);
		return GsonClient.response(customer);
	}
	
	//##### Cargando rfc de clientes (Autocomplete)
	@PostMapping("/customersByRfc")
	@ResponseBody
	public List<String> customersByRfc() {
		val source = customerService.findAllByTaxpayer(this.taxpayer);
		this.customersByRfc = source.stream()
									.map(ClienteEntity::getRfc)
									.map(UBase64::base64Encode)
									.collect(toList());
		return this.customersByRfc;
	}
	
	//##### Cargando razon social de clientes (Autocomplete)
	@PostMapping("/customersByNameOrBusinessName")
	@ResponseBody
	public List<String> customersByNameOrBusinessName() {
		val source = customerService.findAllByTaxpayer(this.taxpayer);
		this.customersByNameOrBusinessName = source.stream()
												   .map(ClienteEntity::getNombreRazonSocial)
												   .map(UBase64::base64Encode)
												   .collect(toList());
		return this.customersByNameOrBusinessName;
	}
	
	//##### Cargando conceptos
	@RequestMapping(value = "/conceptsByEmitter", method = RequestMethod.POST)
	public @ResponseBody String conceptsByEmitter() {
		JEmitterConcept concept = new JEmitterConcept();
		List<Concept> concepts = new ArrayList<>();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			if (!UValidator.isNullOrEmpty(this.concepts)) {
				for (ContribuyenteConceptoEntity item : this.concepts) {
					Concept conceptObj = new Concept();
					conceptObj.setId(UBase64.base64Encode(item.getId().toString()));
					conceptObj.setDescription(UBase64.base64Encode(item.getDescripcion().toUpperCase()));
					conceptObj.setKeyProductService(UBase64.base64Encode(item.getClaveProductoServicio().getCodigo().concat(" (").concat(item.getClaveProductoServicio().getValor()).concat(")")));
					conceptObj.setMeasurementUnit(UBase64.base64Encode(item.getUnidadMedida().getCodigo().concat(" (").concat(item.getUnidadMedida().getValor()).concat(")")));					
					conceptObj.setIdentificationNumber(UBase64.base64Encode(item.getNoIdentificacion()));
					conceptObj.setUnit(UBase64.base64Encode(item.getUnidad()));
					conceptObj.setQuantity(UBase64.base64Encode(UValue.doubleString(item.getCantidad())));
					conceptObj.setUnitValue(UBase64.base64Encode(UValue.doubleString(item.getValorUnitario())));
					conceptObj.setDiscount(UBase64.base64Encode(UValue.doubleString(item.getDescuento())));
					
					concepts.add(conceptObj);
				}
				concept.getConcepts().addAll(concepts);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jsonInString = gson.toJson(concept);
		return jsonInString;
	}
	
	//##### Cargando conceptos por descripcion (Autocomplete)
	@RequestMapping(value = "/conceptsByDescription", method = RequestMethod.POST)
	public @ResponseBody List<String> conceptsByDescription() {
		this.conceptsByDescription = new ArrayList<>();
		try {
			
			if (!UValidator.isNullOrEmpty(this.taxpayer)) {
				Iterable<ContribuyenteConceptoEntity> concepts = iTaxpayerConceptJpaRepository.findByContribuyenteAndEliminadoFalse(taxpayer);
				concepts.forEach(item -> this.conceptsByDescription.add(UBase64.base64Encode(item.getDescripcion())));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.conceptsByDescription;
	}
	
	//##### Cargando listado de series
	public List<String> seriesSourceRequest() {
		List<String> serieSheetSource = new ArrayList<>();
		try {
			
			Iterable<ContribuyenteSerieFolioEntity> serieSheetEntitySource = iTaxpayerSerieSheetJpaRepository.findByContribuyenteAndEliminadoFalse(this.taxpayer);
			for (ContribuyenteSerieFolioEntity item : serieSheetEntitySource) {
				serieSheetSource.add(item.getSerie());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serieSheetSource;
	}
	
	public List<String> seriesIdSourceRequest() {
		List<String> serieSheetSource = new ArrayList<>();
		try {
			
			Iterable<ContribuyenteSerieFolioEntity> serieSheetEntitySource = iTaxpayerSerieSheetJpaRepository.findByContribuyenteAndEliminadoFalse(this.taxpayer);
			for (ContribuyenteSerieFolioEntity item : serieSheetEntitySource) {
				serieSheetSource.add(UBase64.base64Encode(item.getId().toString()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serieSheetSource;
	}
	
	//##### Cargando listado de series
	@RequestMapping(value = "/serieSourceRequest", method = RequestMethod.POST)
	public @ResponseBody String serieSourceRequest() {
		JSerieSheet serieSheetJson = new JSerieSheet();
		SerieSheet serieSheet = new SerieSheet();
		
		Gson gson = new Gson();
		String jsonInString = null;
		try {
			
			String options = "<option value=\"-\">&laquo;Seleccione&raquo;</option>";
			List<SerieSheetOptions> serieSheetOptions = new ArrayList<>();
			
			List<ContribuyenteSerieFolioEntity> serieSheetEntitySource = iTaxpayerSerieSheetJpaRepository.findByContribuyenteAndEliminadoFalse(this.taxpayer);
			
			if (serieSheetEntitySource.size() == 1) {
				options = null;
			}
			
			for (ContribuyenteSerieFolioEntity item : serieSheetEntitySource) {
				String option = "<option value="+UBase64.base64Encode(item.getId().toString())+">"+item.getSerie()+"</option>";
				
				if (!UValidator.isNullOrEmpty(options)) {
					options += option;
				} else {
					options = option;
				}
				
				Integer actualSheet = item.getFolioActual();
				Integer endSheet = item.getFolioFinal();
				
				PreferenciasEntity preferences = iPreferencesJpaRepository.findByContribuyente(this.taxpayer);
				Boolean increaseEndSheet = preferences.getIncrementarFolioFinal();
				
				if (actualSheet < endSheet) {
					actualSheet ++;
				} else if (actualSheet == endSheet) {
					if (increaseEndSheet) {
						actualSheet ++;
					}
				} else if (actualSheet > endSheet) {
					if (increaseEndSheet) {
						actualSheet ++;
					}
				}
				
				SerieSheetOptions sso = new SerieSheetOptions();
				sso.setId(UBase64.base64Encode(item.getId().toString()));
				sso.setValue(UBase64.base64Encode(item.getSerie()));
				sso.setActualSheet(UBase64.base64Encode(actualSheet.toString()));
				serieSheetOptions.add(sso);
			}
			serieSheet.setOptions(UBase64.base64Encode(options));
			serieSheet.getSerieSheetOptions().addAll(serieSheetOptions);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		serieSheetJson.setSerieSheet(serieSheet);
		jsonInString = gson.toJson(serieSheetJson);
		return jsonInString;
	}
	
	//##### Cargando folio actual segun serie
	@RequestMapping(value = "/sheetBySerieRequest", method = RequestMethod.POST)
	public @ResponseBody String sheetBySerieRequest(HttpServletRequest request) {
		
		JSerieSheet serieSheetJson = new JSerieSheet();
		SerieSheet serieSheet = new SerieSheet();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			String serie = UBase64.base64Decode(request.getParameter("serie"));
			if (!UValidator.isNullOrEmpty(serie)) {
				ContribuyenteSerieFolioEntity serieSheetEntity = iTaxpayerSerieSheetJpaRepository.findByContribuyenteAndSerieIgnoreCaseAndEliminadoFalse(this.taxpayer, serie);
				
				Integer actualSheet = serieSheetEntity.getFolioActual();
				Integer endSheet = serieSheetEntity.getFolioFinal();
				
				PreferenciasEntity preferences = iPreferencesJpaRepository.findByContribuyente(this.taxpayer);
				Boolean increaseEndSheet = preferences.getIncrementarFolioFinal();
				
				if (actualSheet < endSheet) {
					actualSheet ++;
				} else if (actualSheet == endSheet) {
					if (increaseEndSheet) {
						actualSheet ++;
					}
				} else if (actualSheet > endSheet) {
					if (increaseEndSheet) {
						actualSheet ++;
					}
				}
				
				serieSheet.setId(UBase64.base64Encode(serieSheetEntity.getId().toString()));
				serieSheet.setSerie(UBase64.base64Encode(serieSheetEntity.getSerie()));
				serieSheet.setInitialSheet(UBase64.base64Encode(serieSheetEntity.getFolioFinal().toString()));
				serieSheet.setActualSheet(UBase64.base64Encode(actualSheet.toString()));
				serieSheet.setEndSheet(UBase64.base64Encode(endSheet.toString()));				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		serieSheetJson.setSerieSheet(serieSheet);
		jsonInString = gson.toJson(serieSheetJson);
		return jsonInString;
	}
	
	//##### Cargando listado de complementos
	@RequestMapping(value = "/complementSourceRequest", method = RequestMethod.POST)
	public @ResponseBody String complementSourceRequest() {
		
		JComplement complement = new JComplement();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			List<ComplementoEntity> sourceList = iComplementJpaRepository.findByHabilitadoTrue();
			
			for (ComplementoEntity item : sourceList) {
				
				Complement obj = new Complement();
				
				obj.setCode(UBase64.base64Encode(item.getCodigo()));
				obj.setName(UBase64.base64Encode(item.getNombre()));
				obj.setVersion(UBase64.base64Encode(item.getVersion()));
				obj.setLastActualization(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaUltimaActualizacionSat())));
				
				complement.getComplements().add(obj);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jsonInString = gson.toJson(complement);
		return jsonInString;
	}
	
	//##### Decimales por moneda
	@PostMapping(value = "/currencyDecimalsSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Integer> currencyDecimalsSourceRequest(HttpServletRequest request) {
		var decimals = 0;
		
		var code = request.getParameter("code");
		if (!UValidator.isNullOrEmpty(code)) {
			code = code.substring(0, 3);
			val moneda = iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(code);
			if ( !UValidator.isNullOrEmpty(moneda) ) {
				decimals = moneda.getDecimales();
			}										
		}
		
		return ResponseEntity.ok(decimals);
	}
	
	@PostConstruct
	public void init() {
		this.useFakeStampService = appSettings.getBooleanValue("cfdi.use.fake.stamp.service");
	}
	
	//##### Inicializando informacion
	@RequestMapping(value = "/cfdiStamp", method = RequestMethod.GET)
	public String init(Model model) {
		try {
			
			//##### Cargando el contribuyente por el rfc activo
			this.taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
			//##### Verificando que el contribuyente sea usuario master
			//if (userTools.isMaster(this.taxpayer)) {
				if (stampControl.stampStatus(this.taxpayer) || userTools.haveDaysLeft()) {
				//if (userTools.hasDaysLeft()) {
					//##### Cargando informacion inicial
					initial(model);
					
					HtmlHelper.functionalitySelected("MyCFDIs", "cfdi-stamp");
					appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
				} else {
					//return "/denied/ResourceDeniedByStampAvailability";
					return "/home/Dashboard";
				}
			/*
			} else {
				return "/denied/ResourceDenied";
			}*/
			return "/invoicing/CfdiMaster";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	private void initial(Model model) {
		try {
			
			initialContent();
			
			//##### Cargando valores en el model
			model.addAttribute("activetab", "tab-emisor");
			model.addAttribute("currency", this.currencyValue);
			model.addAttribute("taxTypeSource", taxTypeSourceRequest());
			model.addAttribute("cfdiRelationshipTypeSource", cfdiRelationshipTypeSourceRequest());
			model.addAttribute("certificate", this.certificate);
			model.addAttribute("isCertificateConfigured", !UValidator.isNullOrEmpty(this.certificate));			
			model.addAttribute("taxpayer", this.taxpayer);
			model.addAttribute("taxpayerActiveRfc", this.taxpayer.getRfcActivo());
			model.addAttribute("serieSheetSource", seriesSourceRequest());
			model.addAttribute("serieEmpty", seriesSourceRequest().isEmpty());
			model.addAttribute("serieDefaultValue", seriesIdSourceRequest().size() == 1 ? seriesIdSourceRequest().get(0) : "-");
			model.addAttribute("reuseSheetCanceled", this.preferences.getReutilizarFolioCancelado());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initialContent() {
		try {
			
			//##### Cargando certificado del contribuyente
			this.certificate = new ContribuyenteCertificadoEntity();
			this.certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
			
			//##### Cargando listado de clientes del emisor
			this.customers = customerService.findAllByTaxpayer(this.taxpayer);
			
			//##### Cargando listado de conceptos del emisor
			this.concepts = new ArrayList<>();
			Iterable<ContribuyenteConceptoEntity> concepts = iTaxpayerConceptJpaRepository.findByContribuyenteAndEliminadoFalse(this.taxpayer);
			concepts.forEach(item -> this.concepts.add(item));
			
			//##### Cargando preferencias
			this.preferences = new PreferenciasEntity();
			this.preferences = iPreferencesJpaRepository.findByContribuyente(this.taxpayer);
			
			//##### Cargando tipo de moneda
			this.currency = new MonedaEntity();
			this.currency = this.preferences.getMoneda();
			this.currencyValue = String.format(CODE_VALUE_FORMAT, this.currency.getCodigo(), this.currency.getValor());
			
			//##### Cargando tipo de contribuyente (Tipo persona)
			ContribuyenteTipoPersonaEntity taxpayerPersonType = iTaxpayerPersonTypeJpaRepository.findByContribuyente(this.taxpayer);
			this.personType = new TipoPersonaEntity();
			if (!UValidator.isNullOrEmpty(taxpayerPersonType)) {
				this.personType = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(taxpayerPersonType.getTipoPersona().getCodigo());
				this.personTypeValue = this.personType.getValor();
			}
			
			//##### Cargando regimen fiscal
			this.taxRegime = new ContribuyenteRegimenFiscalEntity();
			this.taxRegime = iTaxpayerTaxRegimeJpaRepository.findByContribuyente(this.taxpayer);
			if (!UValidator.isNullOrEmpty(this.taxRegime)) {
				this.taxRegimeValue = this.taxRegime.getRegimenFiscal().getCodigo().concat(" (").concat(this.taxRegime.getRegimenFiscal().getValor()).concat(")");
			}
			
			//##### Cargando tasa o cuota
			rateOrFee = new JRateOrFee();
			rateOrFee = rateOrFeeLoad();
			
			//##### Inicializando los calculos automaticos
			this.wAutomaticCalculations = new WAutomaticCalculations();
			
			//##### Inicializando el wrapper de conceptos
			this.wConcepts = new ArrayList<>();
			
			//##### Inicializando propiedades
			this.emitterProperties = new JEmitterProperties();
			
			//##### Inicializando el wrapper de complemento de pago
			this.wPaymentTotal = new WPaymentTotal();
			this.payments = new ArrayList<>();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/rateOrFeeRequest", method = RequestMethod.POST)
	public @ResponseBody String loadRateOrFee() {
		Gson gson = new Gson();
		String jsonInString = null;
		
		JRateOrFee source = this.rateOrFeeLoad();
		
		jsonInString = gson.toJson(source);
		return jsonInString;		
	}
	
	@PostMapping("/initialContent")
	@ResponseBody
	public String loadInitialContent() {
		reloadContent();
		return GsonClient.response(this.emitterProperties);
	}
	
	public void reloadContent() {
		try {
			
			this.emitterProperties.setPersonType(UBase64.base64Encode(this.personTypeValue));
			this.emitterProperties.setTaxRegime(UBase64.base64Encode(this.taxRegimeValue));
			this.emitterProperties.setDisplayCustomerCatalog(!this.customers.isEmpty());
			this.emitterProperties.setDisplayConceptCatalog(!this.concepts.isEmpty());
			this.emitterProperties.setCertificateConfigured(!UValidator.isNullOrEmpty(this.certificate));
			if (!UValidator.isNullOrEmpty(this.certificate)) {
				this.emitterProperties.setCertificateCreationDate(UBase64.base64Encode(UDate.formattedDate(UCertificateX509.certificateCreationDate(new File(this.certificate.getRutaCertificado())), DATE_FORMAT)));
				this.emitterProperties.setCertificateValidDate(UBase64.base64Encode(UDate.formattedDate(UCertificateX509.certificateExpirationDate(new File(this.certificate.getRutaCertificado())), DATE_FORMAT)));
				this.emitterProperties.setCertificateNumber(UBase64.base64Encode(UCertificateX509.certificateSerialNumber(UFile.fileToByte(new File(this.certificate.getRutaCertificado())))));
			}
			this.emitterProperties.setCurrency(UBase64.base64Encode(this.currencyValue));
			
			val stampEntity = iStampJpaRepository.findByContribuyente(this.taxpayer);						
			
			val stamp = new JStamp();
			stamp.setStampAvailable(UBase64.base64Encode(stampEntity.getTimbresDisponibles().toString()));
			stamp.setStampSpent(UBase64.base64Encode(stampEntity.getTimbresConsumidos().toString()));
			stamp.setStampTotal(UBase64.base64Encode(stampEntity.getTotal().toString()));
			
			BigDecimal percentage;
			if (!UValidator.isNullOrEmpty(stampEntity.getTotal()) && stampEntity.getTotal() > 0) {
				percentage = new BigDecimal(stampEntity.getTimbresDisponibles()).multiply(new BigDecimal(100)).divide(new BigDecimal(stampEntity.getTotal()), 0, roundingMode);
			} else {
				percentage = new BigDecimal(0);
			}
			stamp.setPercentage(UBase64.base64Encode(UValue.bigDecimalString(percentage)));
			
			val taxpayerPostalCode = iTaxpayerPostalCode.findByContribuyenteAndEliminadoFalse(this.taxpayer);
			if (this.preferences != null && !UValidator.isNullOrEmpty(taxpayerPostalCode)
					&& this.preferences.getRegistrarActualizarLugarExpedicion()) {
				val postalCode = taxpayerPostalCode.getCodigoPostal().getValor();
				this.emitterProperties.setPostalCode(UBase64.base64Encode(postalCode));
			}
			/*
			if (!UValidator.isNullOrEmpty(taxpayerPostalCode)) {
				String postalCode = taxpayerPostalCode.getCodigoPostal().getValor();
				this.emitterProperties.setPostalCode(UBase64.base64Encode(postalCode));
			}*/
			
			this.emitterProperties.setStamp(stamp);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/reportPreferences", method = RequestMethod.POST)
	public @ResponseBody String reportPreferences() {
		Gson gson = new Gson();
		String jsonInString = null;
		
		ContribuyentePlantillaEntity response = new ContribuyentePlantillaEntity();
		
		try {
			
			ContribuyentePlantillaEntity taxpayerTemplate = iTaxpayerTemplateJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayer);						
			if (!UValidator.isNullOrEmpty(taxpayerTemplate)) {
				response.setObservaciones(taxpayerTemplate.getObservaciones());
			} else {
				response.setObservaciones(false);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jsonInString = gson.toJson(response);
		return jsonInString;		
	}
	
	@PostMapping("/automaticCalculations")
	@ResponseBody
	public String automaticCalculations(HttpServletRequest request) {
		val currencyStr = UBase64.base64Decode(request.getParameter("currency"));
		this.wAutomaticCalculations = new WAutomaticCalculations();
		
		val calculation = invoiceAutomaticCalculationService.automaticCalculations(currencyStr, wConcepts, wAutomaticCalculations);
		
		sessionController.sessionUpdate();
		return GsonClient.response(calculation);
	}
	
	@RequestMapping(value="/cfdiStamp", params={"complementSelected"})
	public @ResponseBody String complementSelected(HttpServletRequest request) throws IOException {
		
		JComplement complement = new JComplement();
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String complementCode = UBase64.base64Decode(request.getParameter("complementCode"));
			if (!UValidator.isNullOrEmpty(complementCode)) {
				ComplementoEntity complementEntity = iComplementJpaRepository.findByCodigoAndHabilitadoTrue(complementCode);
				if (!UValidator.isNullOrEmpty(complementEntity)) {
					//##### Complemento de pago
					if (complementCode.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.payments")) {
						// Creando el concepto para el comprobante
						val wConcept = new WConcept();
						wConcept.setDescription("Pago");
						wConcept.setKeyProductService("84111506 (Servicios de facturación)");
						wConcept.setMeasurementUnit("ACT (Actividad)");
						wConcept.setIdentificationNumber(null);
						wConcept.setUnit(null);
						wConcept.setQuantity("1");
						wConcept.setUnitValue("0");
						wConcept.setDiscount(null);
						wConcept.setAmount("0");
						wConcept.setObjImp("01 (No objeto de impuesto)");
						this.wConcepts.add(wConcept);
					}
					response.setSuccess(Boolean.TRUE);
				} else {
					notification.setMessage(UProperties.getMessage("ERR0128", userTools.getLocale()));
					notification.setPageMessage(Boolean.TRUE);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setNotification(notification);
					response.setError(Boolean.TRUE);
				}
			} else {
				notification.setMessage(UProperties.getMessage("ERR0128", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		complement.setResponse(response);
		jsonInString = gson.toJson(complement);
		return jsonInString;
	}
	
	@RequestMapping(value="/cfdiStamp", params={"conceptDataLoad"})
	public @ResponseBody String conceptDataLoad(HttpServletRequest request) throws IOException{
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JConcept concept = new JConcept();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String index = UBase64.base64Decode(request.getParameter("index"));			
			concept.setIndex(index);
			concept.setEdit(Boolean.TRUE);
						
			this.conceptIndex = Integer.valueOf(UBase64.base64Decode(request.getParameter("index")));
			
			WConcept wConcept = new WConcept();
			wConcept = this.wConcepts.get(this.conceptIndex);
			
			concept.setDescription(UBase64.base64Encode(wConcept.getDescription()));
			concept.setMeasurementUnit(UBase64.base64Encode(wConcept.getMeasurementUnit()));
			concept.setKeyProductService(UBase64.base64Encode(wConcept.getKeyProductService()));
			concept.setIdentificationNumber(UBase64.base64Encode(wConcept.getIdentificationNumber()));
			concept.setUnit(UBase64.base64Encode(wConcept.getUnit()));
			concept.setQuantity(UBase64.base64Encode(UValue.doubleString(wConcept.getQuantity())));
			concept.setUnitValue(UBase64.base64Encode(UValue.doubleString(wConcept.getUnitValue())));
			concept.setDiscount(UBase64.base64Encode(UValue.doubleString(wConcept.getDiscount())));
			concept.setAmount(UBase64.base64Encode(UValue.doubleString(wConcept.getAmount())));
			concept.setObjImp(UBase64.base64Encode(wConcept.getObjImp()));
			
			// Impuestos trasladados
			concept.setTaxesTransferred(new ArrayList<>());
			if (wConcept.getTaxesTransferred() != null) {
				for (WTaxes item : wConcept.getTaxesTransferred()) {
					JTax tax = new JTax();
					if (UValidator.isNullOrEmpty(wConcept.getDiscount())) {
						tax.setBase(UBase64.base64Encode(item.getBase().toString()));
					} else {
						Double baseWithoutDiscount = Double.parseDouble(wConcept.getQuantity()) * Double.parseDouble(wConcept.getUnitValue());
						tax.setBase(UBase64.base64Encode(baseWithoutDiscount.toString() ));
					}
					tax.setTax(UBase64.base64Encode(item.getTax()));
					tax.setFactorType(UBase64.base64Encode(item.getFactorType().value()));
					
					if (!UValidator.isNullOrEmpty(item.getRateOrFee())) {
						tax.setRateOrFee(UBase64.base64Encode(item.getRateOrFee().toString()));
					}
					
					if (!UValidator.isNullOrEmpty(item.getAmount())) {
						tax.setAmount(UBase64.base64Encode(item.getAmount().toString()));
					}
					
					concept.getTaxesTransferred().add(tax);
				}
			}
			
			// Impuestos retenidos
			concept.setTaxesWithheld(new ArrayList<>());
			if (wConcept.getTaxesWithheld() != null) {
				for (WTaxes item : wConcept.getTaxesWithheld()) {
					JTax tax = new JTax();
					
					tax.setBase(UBase64.base64Encode(item.getBase().toString()));
					
					tax.setTax(UBase64.base64Encode(item.getTax()));
					tax.setFactorType(UBase64.base64Encode(item.getFactorType().value()));
					if (!UValidator.isNullOrEmpty(item.getRateOrFee())) {
						tax.setRateOrFee(UBase64.base64Encode(item.getRateOrFee().toString()));
					}
					
					if (!UValidator.isNullOrEmpty(item.getAmount())) {
						tax.setAmount(UBase64.base64Encode(item.getAmount().toString()));
					}
					concept.getTaxesWithheld().add(tax);
				}
			}
			
			// Informacion aduanera
			concept.setCustomsInformationNumbers(new ArrayList<>());
			if (wConcept.getCustomsInformation() != null) {
				wConcept.getCustomsInformation().forEach(item -> concept.getCustomsInformationNumbers().add(UBase64.base64Encode(item.getRequirementNumber())));
			}
			
			// Cuenta predial
			if (wConcept.getPropertyAccount() != null) {
				concept.setPropertyAccountNumber(UBase64.base64Encode(wConcept.getPropertyAccount().getNumber()));
			}
			
			// Complemento Concepto de Instituciones Educativas Privadas
			if (wConcept.getIedu() != null) {
				Iedu iedu = new Iedu();
				iedu.setStudentName(UBase64.base64Encode(wConcept.getIedu().getStudentName()));
				iedu.setStudentCurp(UBase64.base64Encode(wConcept.getIedu().getCurp()));
				iedu.setEducationLevel(UBase64.base64Encode(wConcept.getIedu().getEducationLevel()));
				iedu.setSchoolKey(UBase64.base64Encode(wConcept.getIedu().getAutRVOE()));
				iedu.setStudentRfc(UBase64.base64Encode(wConcept.getIedu().getRfc()));
				concept.setIedu(iedu);
			}

			response.setSuccess(Boolean.TRUE);
			concept.setResponse(response);

		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0049", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
			response.setTabError("tab-concept");
			concept.setResponse(response);
		}
		
		sessionController.sessionUpdate();
		
		jsonInString = gson.toJson(concept);
		return jsonInString;        
    }
	
	@RequestMapping(value="/cfdiStamp", params={"conceptRemove"})
    public @ResponseBody String conceptRemove(HttpServletRequest request) throws IOException {
		
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		JConcept concept = new JConcept();
		JVoucher voucher = new JVoucher();
		JCalculation calculation = new JCalculation();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
        	
        	String index = UBase64.base64Decode(request.getParameter("index"));
        	int indexInteger = Integer.valueOf(index);
            
        	this.conceptIndex = null;
        	
        	boolean indexOutOfBoundsException = false;
        	try {
        		
        		this.wConcepts.remove(indexInteger);
        		
			} catch (IndexOutOfBoundsException e) {
				indexOutOfBoundsException = true;
				
				notification.setMessage(UProperties.getMessage("ERR0050", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
				response.setTabError("tab-concept");
				concept.setResponse(response);
			}
        	
        	if (!indexOutOfBoundsException) {
        		String currency = UBase64.base64Decode(request.getParameter("currency"));
    			MonedaEntity currencyEntity = null;
    					
    			if (!UValidator.isNullOrEmpty(currency)) {
    				// Validar que sea un valor de catalogo
    				String[] currencyCatalog = UCatalog.getCatalog(currency);
    				currency = currencyCatalog[0];
    				String currencyValue = currencyCatalog[1];
    				
    				currencyEntity = iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(currency);
    				MonedaEntity currencyEntityByValue = iCurrencyJpaRepository.findByValorAndEliminadoFalse(currencyValue);
    				
    				if (!UValidator.isNullOrEmpty(currencyEntity) && !UValidator.isNullOrEmpty(currencyEntityByValue)) {
    					//##### Actualizando calculos automaticos
    					String valueNull = null;
    					
    					BigDecimal voucherSubtotal = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
    					BigDecimal voucherDiscount = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
    					BigDecimal voucherTaxTransferred = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
    					BigDecimal voucherTaxWithheld = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
    					BigDecimal voucherTotal = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
    					
    					for (WConcept item : wConcepts) {
    						// Subtotal
    						voucherSubtotal = voucherSubtotal.add(UValue.bigDecimalStrict(item.getAmount(), currencyEntity.getDecimales()));
    						
    						// Descuento
    						if (!UValidator.isNullOrEmpty(item.getDiscount())) {
    							voucherDiscount = voucherDiscount.add(UValue.bigDecimalStrict(item.getDiscount(), currencyEntity.getDecimales()));
    						}
    						
    						// Total de impuestos trasladados
    						for (WTaxes transferred : item.getTaxesTransferred()) {
    							voucherTaxTransferred = voucherTaxTransferred.add(transferred.getAmount());
    						}
    						
    						// Total de impuestos retenidos
    						for (WTaxes withheld : item.getTaxesWithheld()) {
    							voucherTaxWithheld = voucherTaxWithheld.add(withheld.getAmount());
    						}
    					}
    					voucherTotal = UCalculation.total(voucherSubtotal, voucherDiscount, voucherTaxTransferred, voucherTaxWithheld, currencyEntity.getDecimales());
    					
    					this.wAutomaticCalculations = new WAutomaticCalculations();
    					this.wAutomaticCalculations.setVoucherSubtotal(voucherSubtotal);
    					this.wAutomaticCalculations.setVoucherDiscount(voucherDiscount);
    					this.wAutomaticCalculations.setVoucherTaxTransferred(voucherTaxTransferred);
    					this.wAutomaticCalculations.setVoucherTaxWithheld(voucherTaxWithheld);
    					this.wAutomaticCalculations.setVoucherTotal(voucherTotal);
    					
    					voucher = new JVoucher();
    					voucher.setSubTotal(UBase64.base64Encode(UValue.bigDecimalString(voucherSubtotal)));
    					voucher.setDiscount(UBase64.base64Encode(UValue.bigDecimalString(voucherDiscount)));
    					voucher.setTotalTaxesTransferred(UBase64.base64Encode(UValue.bigDecimalString(voucherTaxTransferred)));
    					voucher.setTotalTaxesWithheld(UBase64.base64Encode(UValue.bigDecimalString(voucherTaxWithheld)));
    					voucher.setTotal(UBase64.base64Encode(UValue.bigDecimalString(voucherTotal)));
    				}
    			}        	
                
                response.setSuccess(Boolean.TRUE);
                concept.setIndex(UBase64.base64Encode(index));
                concept.setResponse(response);
        	}
        	
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0050", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
			response.setTabError("tab-concept");
			concept.setResponse(response);
		}	
		
		sessionController.sessionUpdate();
		
		calculation.setConcept(concept);
		calculation.setVoucher(voucher);
		
		jsonInString = gson.toJson(calculation);
		return jsonInString; 
    }
	
	@RequestMapping(value="/cfdiStamp", params={"conceptAddEdit"})
    public @ResponseBody String conceptAddEdit(HttpServletRequest request) throws IOException {
		JConcept concept = new JConcept();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		JVoucher voucher = new JVoucher();
		JCalculation calculation = new JCalculation();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		boolean isEdit = false;
		
		try {
			
			String operation = UBase64.base64Decode(request.getParameter("operation"));
			
			Integer index = null;
			if (!operation.equals("add") && this.conceptIndex != null) {
				isEdit = true;
				index = this.conceptIndex;
			}		
			
			Boolean errorMarked = false;
			
			if (request.getParameter("objConcept") != null) {
				String objConcept = request.getParameter("objConcept");
				concept = gson.fromJson(objConcept, JConcept.class);
				
				WIedu wIedu = null;
				
				// Moneda
				String currency = UBase64.base64Decode(request.getParameter("currency"));
				boolean currencyError = false;
				MonedaEntity currencyEntityByCode = null;
				
				if (UValidator.isNullOrEmpty(currency)) {
					error.setMessage(requiredMessage());
					currencyError = true;
				} else {
					// Validar que sea un valor de catalogo
					String[] currencyCatalog = UCatalog.getCatalog(currency);
					currency = currencyCatalog[0];
					String currencyValue = currencyCatalog[1];
					
					currencyEntityByCode = iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(currency);
					MonedaEntity currencyEntityByValue = iCurrencyJpaRepository.findByValorAndEliminadoFalse(currencyValue);
					
					if (UValidator.isNullOrEmpty(currencyEntityByCode) || UValidator.isNullOrEmpty(currencyEntityByValue)) {
						String[] values = new String[]{"CFDI33112", "Moneda", "c_Moneda"};
						notification.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
						currencyError = true;
					}
				}
				
				if (currencyError) {
					error.setField("currency");
					errors.add(error);
					
					
					notification.setModalPanelMessage(Boolean.TRUE);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setNotification(notification);
					response.setError(Boolean.TRUE);
				} else {
					// Concepto seleccionado desde catalogo o por descripcion
					String conceptSelected = UBase64.base64Decode(concept.getConceptSelected());
					ContribuyenteConceptoEntity taxpayerConcept = null;
					if (!UValidator.isNullOrEmpty(conceptSelected)) {
						Long id = UValue.longValue(conceptSelected);
						taxpayerConcept = iTaxpayerConceptJpaRepository.findByIdAndEliminadoFalse(id);
					}
					
					// Descripcion
					String description = UBase64.base64Decode(concept.getDescription());
					error = new JError();
					
					if (UValidator.isNullOrEmpty(description)) {
						error.setMessage(requiredMessage());
						errorMarked = true;
					} else {
						boolean validateEditDescription = false;
						if (isEdit) {
							String conceptDescripcion = this.wConcepts.get(index).getDescription();
							if (!conceptDescripcion.equalsIgnoreCase(description)) {
								validateEditDescription = true;
							}
						}
						if (!isEdit || (isEdit && validateEditDescription)) {
							boolean descriptionExist = false;
							for (WConcept item : this.wConcepts) {
								if (item.getDescription().equalsIgnoreCase(description)) {
									descriptionExist = true;
									break;
								}
							}
							if (descriptionExist) {
								error.setMessage(UProperties.getMessage("ERR0010", userTools.getLocale()));
								errorMarked = true;
							}
						}
					}
					if (errorMarked) {
						error.setField("description");
						errors.add(error);
					}
					
					// Unidad de medida
					String measurementUnit = UBase64.base64Decode(concept.getMeasurementUnit());
					error = new JError();
					errorMarked = false;
					
					if (UValidator.isNullOrEmpty(measurementUnit)) {
						error.setMessage(requiredMessage());
						errorMarked = true;
					} else {
						String[] measurementUnitCatalog = UCatalog.getCatalog(measurementUnit);
						String measurementUnitCode = measurementUnitCatalog[0];
						String measurementUnitValue = measurementUnitCatalog[1];
						UnidadMedidaEntity measurementUnitEntityByCode = iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(measurementUnitCode);
						UnidadMedidaEntity measurementUnitEntityByValue = iMeasurementUnitJpaRepository.findByValorAndEliminadoFalse(measurementUnitValue);
						
						if (measurementUnitEntityByCode == null || measurementUnitEntityByValue == null) {
							String[] values = new String[]{"CFDI33145", "UnidadMedida", "c_ClaveUnidad"};
							error.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
							errorMarked = true;
						}
					}
					if (errorMarked) {
						error.setField("measurement-unit");
						errors.add(error);
					}
					
					// Clave producto servicio
					String keyProductService = UBase64.base64Decode(concept.getKeyProductService());
					error = new JError();
					errorMarked = false;
					
					if (UValidator.isNullOrEmpty(keyProductService)) {
						error.setMessage(requiredMessage());
						errorMarked = true;
					} else {
						String[] keyProductServiceCatalog = UCatalog.getCatalog(keyProductService);
						
						String keyProductServiceCode = keyProductServiceCatalog[0];
						ClaveProductoServicioEntity keyProductServiceEntityByCode = iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(keyProductServiceCode);
						
						String keyProductServiceValue = keyProductServiceCatalog[1];
						ClaveProductoServicioEntity keyProductServiceEntityByValue = iKeyProductServiceJpaRepository.findByValorAndEliminadoFalse(keyProductServiceValue);
						
						if (keyProductServiceEntityByCode == null || keyProductServiceEntityByValue == null) {
							String[] values = new String[]{"CFDI33142", "ClaveProdServ", "c_ClaveProdServ"};
							error.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
							errorMarked = true;
						} else {
							ClaveProductoServicioEntity keyProductServiceIvaIncluded = iKeyProductServiceJpaRepository.findByCodigoAndIncluirIvaAndEliminadoFalse(keyProductServiceCode, "Sí");
							if (keyProductServiceIvaIncluded != null) {
								if (concept.getTaxesWithheld() == null) {
									error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33144").value(), userTools.getLocale()));
									errorMarked = true;
								} else {
									boolean iva = false;
									for (JTax item : concept.getTaxesWithheld()) {
										String tax = UBase64.base64Decode(item.getTax());
										if (tax.equalsIgnoreCase("002 (iva)")) {
											iva = true;
											break;
										}
									}
									if (iva) {
										error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33144").value(), userTools.getLocale()));
										errorMarked = true;
									}
								}
							}
							if (!errorMarked) {
								ClaveProductoServicioEntity keyProductServiceIepsTransferred = iKeyProductServiceJpaRepository.findByCodigoAndIncluirIepsTrasladadoAndEliminadoFalse(keyProductServiceCode, "No");
								if (keyProductServiceIepsTransferred != null) {
									if (concept.getTaxesTransferred() != null) {
										boolean ieps = false;
										for (JTax item : concept.getTaxesTransferred()) {
											String tax = UBase64.base64Decode(item.getTax());
											if (tax.equalsIgnoreCase( "003 (ieps)")) {
												ieps = true;
												break;
											}
										}
										if (ieps) {
											error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33144").value(), userTools.getLocale()));
											errorMarked = true;
										}
									}
								}
							}
						}
					}
					if (errorMarked) {
						error.setField("key-product-service");
						errors.add(error);
					}
					
					// Numero de identificacion
					String identificationNumber = UBase64.base64Decode(concept.getIdentificationNumber());
					
					// Unidad
					String unit = UBase64.base64Decode(concept.getUnit());
					
					// Cantidad
					String quantity = UBase64.base64Decode(concept.getQuantity());
					BigDecimal quantityBigDecimal = UValue.bigDecimalStrict(quantity);
					error = new JError();
					errorMarked = false;
					
					if (UValidator.isNullOrEmpty(quantity)) {
						error.setMessage(requiredMessage());
						errorMarked = true;
					} else if (!UValidator.isDouble(quantity)) {
						error.setMessage(invalidDataTypeMessage());
						errorMarked = true;
					}	
					if (errorMarked) {
						error.setField("quantity");
						errors.add(error);
					}
					
					// Tipo de comprobante
					String voucherType = UBase64.base64Decode(request.getParameter("voucherType"));
					boolean voucherTypeError = false;
					
					// Validar que sea un valor de catalogo
					String[] voucherTypeCatalog = UCatalog.getCatalog(voucherType);
					voucherType = voucherTypeCatalog[0];
					String voucherTypeValue = voucherTypeCatalog[1];
					
					TipoComprobanteEntity voucherTypeEntityByCode = iVoucherTypeJpaRepository.findByCodigoAndEliminadoFalse(voucherType);
					TipoComprobanteEntity voucherTypeEntityByValue = iVoucherTypeJpaRepository.findByValorAndEliminadoFalse(voucherTypeValue);
					
					if (voucherTypeEntityByCode == null || voucherTypeEntityByValue == null) {
						String[] values = new String[]{"CFDI33120", "TipoDeComprobante", "c_TipoDeComprobante"};
						error.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
						voucherTypeError = true;
					}
					if (voucherTypeError) {
						error.setField("voucher-type");
						errors.add(error);
					}
					
					// Valor unitario
					String unitValue = UBase64.base64Decode(concept.getUnitValue());
					BigDecimal unitValueBigDecimal = UValue.bigDecimalStrict(unitValue);
					
					String valueNull = null;
					BigDecimal zeroDecimal = UValue.bigDecimal(valueNull); 
					
					error = new JError();
					errorMarked = false;
					
					if (UValidator.isNullOrEmpty(unitValue)) {
						error.setMessage(requiredMessage());
						errorMarked = true;
					} else {
						if (!UValidator.isDouble(unitValue)) {
							error.setMessage(invalidDataTypeMessage());
							errorMarked = true;
						} else {
							if (!voucherTypeError && (voucherType.equals(VoucherUtils.VOUCHER_TYPE_CODE_I)
									|| voucherType.equals(VoucherUtils.VOUCHER_TYPE_CODE_E)
									|| voucherType.equals(VoucherUtils.VOUCHER_TYPE_CODE_N))) {
								val zeroInteger = unitValueBigDecimal.toBigInteger().intValueExact();
								if (zeroInteger <= 0 && unitValueBigDecimal.compareTo(zeroDecimal) == 0) {
									error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33147").value(), userTools.getLocale()));
									errorMarked = true;
								}
							}
						}
					} 
					if (errorMarked) {
						error.setField("unit-value");
						errors.add(error);
					}
					
					// Descuento
					String discount = UBase64.base64Decode(concept.getDiscount());
					BigDecimal discountBigDecimal = null;
					BigDecimal discountValid = null;
					error = new JError();
					boolean discountError = false;
					
					if (!UValidator.isNullOrEmpty(discount)) {
						if (!UValidator.isDouble(discount)) {
							error.setMessage(invalidDataTypeMessage());
							discountError = true;
						} else {
							if (!currencyError) {
								discountValid = UValue.bigDecimalStrict(discount, UTools.decimalValue(discount));
								if (!UValidator.isValidDecimalPart(discountValid, currencyEntityByCode.getDecimales())) {
									error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33150").value(), userTools.getLocale()));
									errors.add(error);
									discountError = true;
								} else {
									discountBigDecimal = UValue.bigDecimalStrict(discount);
								}
							}
						}
					}/*else{
						String valueNull = null;
						discountBigDecimal = UValue.bigDecimal(valueNull);
					}*/
					if (discountError) {
						error.setField("discount");
						errors.add(error);
					}

					// Importe
					String amount = UBase64.base64Decode(concept.getAmount());
					BigDecimal amountBigDecimal = UValue.bigDecimalStrict(amount);
					error = new JError();
					errorMarked = false;
					
					if (UValidator.isNullOrEmpty(amount)) {
						error.setMessage(requiredMessage());
						errorMarked = true;
					} else {
						if (!UValidator.isDouble(amount)) {
							error.setMessage(invalidDataTypeMessage());
							errorMarked = true;
						} else {
							if (!discountError && !currencyError) {
								// Verificando importe
								BigDecimal amountValid = UValue.bigDecimal(UCalculation.amount(quantityBigDecimal, unitValueBigDecimal).toString(), currencyEntityByCode.getDecimales()) ;
								if (amountBigDecimal.compareTo(amountValid) != 0) {
									error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33149").value(), userTools.getLocale()));
									errorMarked = true;
								}
							}
						}
					}
					if (errorMarked) {
						error.setField("amount");
						errors.add(error);
					}
					
					// Objeto Impuesto
					val objImp = UBase64.base64Decode(concept.getObjImp());
					error = new JError();
					errorMarked = false;
					
					if (UValidator.isNullOrEmpty(objImp)) {
						error.setMessage(requiredMessage());
						errorMarked = true;
					} else {
						val objImpCatalog = UCatalog.getCatalog(objImp);
						val objImpCode = objImpCatalog[0];
						val objImpValue = objImpCatalog[1];
						val objImpByCode = iObjetoImpuestoJpaRepository.findByCodigoAndEliminadoFalse(objImpCode);
						val objImpByValue = iObjetoImpuestoJpaRepository.findByValorAndEliminadoFalse(objImpValue);
						
						if (objImpByCode == null || objImpByValue == null) {
							val values = new String[]{"CFDI40170", "ObjetoImp", "c_ObjetoImp"};
							error.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
							errorMarked = true;
						}
					}
					if (errorMarked) {
						error.setField("obj-tax");
						errors.add(error);
					}
					
					if (!errorMarked) {
						error = new JError();
						if (discountBigDecimal != null) {
							//discountValid = UValue.bigDecimalStrict(UCalculation.discount(quantityBigDecimal, unitValueBigDecimal, discountValid).toString(), UTools.decimalValue(discount.toString()));
							BigDecimal discountCalc = UCalculation.discount(quantityBigDecimal, unitValueBigDecimal, discountValid);
							String discountCalcString = UValue.bigDecimalString(discountCalc); 
							discountValid = UValue.bigDecimalStrict(discountCalcString, UTools.decimalValue(discountCalcString));
							if (discountValid.compareTo(amountBigDecimal) > 0) {
								error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33151").value(), userTools.getLocale()));
								error.setField("discount");
								errors.add(error);
							}
						}
					}
					
					String taxes = UBase64.base64Decode(request.getParameter("taxes"));
					error = new JError();
					boolean taxesError = false;
					if (taxes.equalsIgnoreCase("on")) {
						if (concept.getTaxesTransferred() == null && concept.getTaxesWithheld() == null) {
							error.setMessage(UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33152").value(), userTools.getLocale()));						
							errors.add(error);
							notification.setMessage(error.getMessage());
							notification.setModalPanelMessage(true);
							notification.setCssStyleClass(CssStyleClass.ERROR.value());
							taxesError = true;
						}
					}
					
					Iedu iedu = concept.getIedu();
					boolean ieduError = false;
					if (iedu != null) {
						//##### Nombre estudiante
						String studentName = UBase64.base64Decode(iedu.getStudentName());
						error = new JError();
						
						if (UValidator.isNullOrEmpty(studentName)) {
							error.setMessage(requiredMessage());
							error.setField("student-name");
							errors.add(error);
							ieduError = true;
						}
						
						//##### CURP
						String studentCurp = UBase64.base64Decode(iedu.getStudentCurp());
						error = new JError();
						errorMarked = false;
						
						if (UValidator.isNullOrEmpty(studentCurp)) {
							error.setMessage(requiredMessage());
							errorMarked = true;
						} else {
							if (studentCurp.toString().length() < CURP_LENGTH || !studentCurp.matches(CURP_EXPRESSION)) {
								error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
								errorMarked = true;
							}
						}
						if (errorMarked) {
							error.setField("student-curp");
							errors.add(error);
							ieduError = true;
						}
						
						//##### Nivel educativo
						String educationLevel = UBase64.base64Decode(iedu.getEducationLevel());
						error = new JError();
						errorMarked = false;
						
						if (UValidator.isNullOrEmpty(educationLevel)) {
							error.setMessage(requiredMessage());
							errorMarked = true;
						} else {
							NivelEducativoEntity educationLevelEntity = iEducationLevelJpaRepository.findByValorAndEliminadoFalse(educationLevel);
							if (UValidator.isNullOrEmpty(educationLevelEntity)) {
								error.setMessage(UProperties.getMessage("ERR0104", userTools.getLocale()));
								errorMarked = true;
							}
						}
						if (errorMarked) {
							error.setField("education-level");
							errors.add(error);
							ieduError = true;
						}
						
						//##### Clave centro trabajo
						String schoolKey = UBase64.base64Decode(iedu.getSchoolKey());
						error = new JError();
						
						if (UValidator.isNullOrEmpty(schoolKey)) {
							error.setMessage(requiredMessage());
							error.setField("school-key");
							errors.add(error);
							ieduError = true;
						}
						
						//#### Rfc
						String rfcPago = UBase64.base64Decode(iedu.getStudentRfc());
						error = new JError();
						errorMarked = false;
						
						if (!UValidator.isNullOrEmpty(rfcPago)) {
							if (rfcPago.toString().length() < RFC_MIN_LENGTH || rfcPago.toString().length() > RFC_MAX_LENGTH || !rfcPago.toString().matches(RFC_EXPRESSION)) {
								error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
								errorMarked = true;
							}
							
							if (errorMarked) {
								error.setField("student-rfc");
								errors.add(error);
								ieduError = true;
							}
						}
						
						if (!ieduError) {
							wIedu = new WIedu();
							wIedu.setVersion(COMPLEMENT_IEDU_VERSION);
							wIedu.setStudentName(studentName);
							wIedu.setCurp(studentCurp);
							wIedu.setEducationLevel(educationLevel);
							wIedu.setAutRVOE(schoolKey);
							wIedu.setRfc(rfcPago);
						}
					}
					
					if (!errors.isEmpty()) {
						response.setError(Boolean.TRUE);
						response.setErrors(errors);
						
						if (taxesError) {
							response.setNotification(notification);
						}
					} else {
						amountBigDecimal = UValue.bigDecimalStrict(amount, currencyEntityByCode.getDecimales());
						error = new JError();
						taxesError = false;
						
						// Informacion del concepto
						WConcept wConcept = new WConcept();
						wConcept.setDescription(description);
						wConcept.setKeyProductService(keyProductService);
						wConcept.setMeasurementUnit(measurementUnit);
						wConcept.setIdentificationNumber(identificationNumber);
						wConcept.setUnit(unit);
						wConcept.setQuantity(quantity);
						wConcept.setUnitValue(unitValue);
						wConcept.setObjImp(objImp);
						
						if (!UValidator.isNullOrEmpty(discount)) {
							wConcept.setDiscount(discount);
						}
												
						wConcept.setAmount(amount);
						wConcept.setTaxpayerConcept(taxpayerConcept);
						wConcept.setConceptSelected(conceptSelected);
						
						if (!isEdit) {
							// Impuestos
							if (concept.getTaxesWithheld() != null) {
								wConcept.setTaxesWithheld(new ArrayList<>());
								for (JTax item : concept.getTaxesWithheld()) {
									BigDecimal baseBigDecimal = UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase()), currencyEntityByCode.getDecimales());																																		
									if (amountBigDecimal.equals(baseBigDecimal)) {
									//if (amountWithDiscount.equals(baseBigDecimal)) {
										WTaxes withheld = new WTaxes();
										//withheld.setBase(UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase())));
										withheld.setBase(UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase()), currencyEntityByCode.getDecimales()));
										withheld.setTax(UBase64.base64Decode(item.getTax()));							
										withheld.setFactorType(UCatalog.factorType(UBase64.base64Decode(item.getFactorType())));
										withheld.setRateOrFee(UValue.bigDecimalStrict(UBase64.base64Decode(item.getRateOrFee())));
										//withheld.setAmount(UValue.bigDecimalStrict(UBase64.base64Decode(item.getAmount())));
										withheld.setAmount(UValue.bigDecimalStrict(UBase64.base64Decode(item.getAmount()), currencyEntityByCode.getDecimales()));										
										wConcept.getTaxesWithheld().add(withheld);										
									} else {
										error.setMessage(UProperties.getMessage("ERR0069", userTools.getLocale()));
										errors.add(error);
										notification.setMessage(error.getMessage());
										notification.setModalPanelMessage(true);
										notification.setCssStyleClass(CssStyleClass.ERROR.value());
										taxesError = true;
										break;
									}									
								}
							}
							
							// Impuestos trasladados
							if (concept.getTaxesTransferred() != null) {
								wConcept.setTaxesTransferred(new ArrayList<>());
								for (JTax item : concept.getTaxesTransferred()) {
									BigDecimal baseBigDecimal = UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase()), currencyEntityByCode.getDecimales());
									if (UValidator.isNullOrEmpty(discountBigDecimal)) {
										discountBigDecimal = BigDecimal.ZERO;
									}
									BigDecimal amountTimesDiscount = amountBigDecimal.multiply(discountBigDecimal);
									BigDecimal amountDivide = amountTimesDiscount.divide(BigDecimal.valueOf(100));
									BigDecimal amountWithDiscount = amountBigDecimal.subtract(amountDivide);
									//BigDecimal amountWithDiscount = amountBigDecimal.subtract(amountBigDecimal.multiply(discountBigDecimal).divide(BigDecimal.valueOf(100)));									
									amountWithDiscount = amountWithDiscount.setScale(currencyEntityByCode.getDecimales(), RoundingMode.DOWN);
									//if (amountBigDecimal.equals(baseBigDecimal)) {
									if (amountWithDiscount.equals(baseBigDecimal)) {
										WTaxes transferred = new WTaxes();
										//transferred.setBase(UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase())));
										transferred.setBase(UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase()), currencyEntityByCode.getDecimales()));
										transferred.setTax(UBase64.base64Decode(item.getTax()));							
										transferred.setFactorType(UCatalog.factorType(UBase64.base64Decode(item.getFactorType())));
										transferred.setRateOrFee(UValue.bigDecimalStrict(UBase64.base64Decode(item.getRateOrFee())));
										//transferred.setAmount(UValue.bigDecimalStrict(UBase64.base64Decode(item.getAmount())));
										transferred.setAmount(UValue.bigDecimalStrict(UBase64.base64Decode(item.getAmount()), currencyEntityByCode.getDecimales()));
										wConcept.getTaxesTransferred().add(transferred);										
									} else {
										error.setMessage(UProperties.getMessage("ERR0069", userTools.getLocale()));				
										errors.add(error);
										notification.setMessage(error.getMessage());
										notification.setModalPanelMessage(true);
										notification.setCssStyleClass(CssStyleClass.ERROR.value());
										taxesError = true;
										break;
									}								
								}
							}
							
							// Informacion aduanera
							if (concept.getCustomsInformationNumbers() != null) {
								wConcept.setCustomsInformation(new ArrayList<>());
								for (String item : concept.getCustomsInformationNumbers()) {
									WCustomsInformation wCustomsInformation = new WCustomsInformation();
									wCustomsInformation.setRequirementNumber(UBase64.base64Decode(item));
									wConcept.getCustomsInformation().add(wCustomsInformation);
								}
							}
							
							// Cuenta predial
							if (concept.getPropertyAccountNumber() != null) {
								WPropertyAccount wPropertyAccount = new WPropertyAccount();
								wPropertyAccount.setNumber(UBase64.base64Decode(concept.getPropertyAccountNumber()));
								wConcept.setPropertyAccount(wPropertyAccount);
							}
							
							// IEDU
							if (wIedu != null) {
								wConcept.setIedu(wIedu);
							}
						} else {
							// Actualizando concepto
							this.wConcepts.get(index).setDescription(wConcept.getDescription());
							this.wConcepts.get(index).setKeyProductService(wConcept.getKeyProductService());
							this.wConcepts.get(index).setMeasurementUnit(wConcept.getMeasurementUnit());
							this.wConcepts.get(index).setIdentificationNumber(wConcept.getIdentificationNumber());
							this.wConcepts.get(index).setUnit(wConcept.getUnit());
							this.wConcepts.get(index).setQuantity(wConcept.getQuantity());
							this.wConcepts.get(index).setUnitValue(wConcept.getUnitValue());
							this.wConcepts.get(index).setDiscount(wConcept.getDiscount());
							this.wConcepts.get(index).setAmount(wConcept.getAmount());
							this.wConcepts.get(index).setObjImp(wConcept.getObjImp());
							
							// Actualizando impuestos retenidos
							this.wConcepts.get(index).setTaxesWithheld(new ArrayList<>());
							if (concept.getTaxesWithheld() != null) {
								for (JTax item : concept.getTaxesWithheld()) {
									BigDecimal baseBigDecimal = UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase()), currencyEntityByCode.getDecimales());
									if (amountBigDecimal.equals(baseBigDecimal)) {
										WTaxes withheld = new WTaxes();
										//withheld.setBase(UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase())));
										withheld.setBase(UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase()), currencyEntityByCode.getDecimales()));
										withheld.setTax(UBase64.base64Decode(item.getTax()));							
										withheld.setFactorType(UCatalog.factorType(UBase64.base64Decode(item.getFactorType())));
										withheld.setRateOrFee(UValue.bigDecimalStrict(UBase64.base64Decode(item.getRateOrFee())));
										//withheld.setAmount(UValue.bigDecimalStrict(UBase64.base64Decode(item.getAmount())));
										withheld.setAmount(UValue.bigDecimalStrict(UBase64.base64Decode(item.getAmount()), currencyEntityByCode.getDecimales()));
										this.wConcepts.get(index).getTaxesWithheld().add(withheld);
									} else {
										error.setMessage(UProperties.getMessage("ERR0069", userTools.getLocale()));
										errors.add(error);
										notification.setMessage(error.getMessage());
										notification.setModalPanelMessage(true);
										notification.setCssStyleClass(CssStyleClass.ERROR.value());
										taxesError = true;
										break;
									}
								}
							}
							
							// Actualizando impuestos trasladados
							this.wConcepts.get(index).setTaxesTransferred(new ArrayList<>());
							if (concept.getTaxesTransferred() != null) {
								for (JTax item : concept.getTaxesTransferred()) {
									BigDecimal baseBigDecimal = UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase()), currencyEntityByCode.getDecimales());
									if (amountBigDecimal.equals(baseBigDecimal)) {
										WTaxes transferred = new WTaxes();
										//transferred.setBase(UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase())));
										transferred.setBase(UValue.bigDecimalStrict(UBase64.base64Decode(item.getBase()), currencyEntityByCode.getDecimales()));
										transferred.setTax(UBase64.base64Decode(item.getTax()));							
										transferred.setFactorType(UCatalog.factorType(UBase64.base64Decode(item.getFactorType())));
										transferred.setRateOrFee(UValue.bigDecimalStrict(UBase64.base64Decode(item.getRateOrFee())));
										//transferred.setAmount(UValue.bigDecimalStrict(UBase64.base64Decode(item.getAmount())));
										transferred.setAmount(UValue.bigDecimalStrict(UBase64.base64Decode(item.getAmount()), currencyEntityByCode.getDecimales()));
										this.wConcepts.get(index).getTaxesTransferred().add(transferred);
									} else {
										error.setMessage(UProperties.getMessage("ERR0069", userTools.getLocale()));
										errors.add(error);
										notification.setMessage(error.getMessage());
										notification.setModalPanelMessage(true);
										notification.setCssStyleClass(CssStyleClass.ERROR.value());
										taxesError = true;
										break;
									}
								}
							}
							
							// Informacion aduanera
							this.wConcepts.get(index).setCustomsInformation(new ArrayList<>());
							if (concept.getCustomsInformationNumbers() != null) {
								wConcept.setCustomsInformation(new ArrayList<>());
								for (String item : concept.getCustomsInformationNumbers()) {
									WCustomsInformation wCustomsInformation = new WCustomsInformation();
									wCustomsInformation.setRequirementNumber(UBase64.base64Decode(item));
									this.wConcepts.get(index).getCustomsInformation().add(wCustomsInformation);
								}
							}
							
							// Cuenta predial
							WPropertyAccount wPropertyAccount = null;
							if (concept.getPropertyAccountNumber() != null) {
								wPropertyAccount = new WPropertyAccount();
								wPropertyAccount.setNumber(UBase64.base64Decode(concept.getPropertyAccountNumber()));
							}
							this.wConcepts.get(index).setPropertyAccount(wPropertyAccount);
							
							// IEDU
							this.wConcepts.get(index).setIedu(wIedu);
						}
						
						if (!taxesError) {
							if (!isEdit) {
								// Adicionando el nuevo concepto a la lista de conceptos
								this.wConcepts.add(wConcept);
							} else {
								concept.setIndex(UBase64.base64Encode(index.toString()));
								concept.setEdit(Boolean.TRUE);
							}
							this.conceptIndex = null;
							
							//##### Actualizando calculos automaticos
							BigDecimal voucherSubtotal = UValue.bigDecimal(valueNull);
							BigDecimal voucherDiscount = UValue.bigDecimal(valueNull);
							BigDecimal voucherTaxTransferred = null;
							BigDecimal voucherTaxWithheld = null;
							BigDecimal voucherTotal = UValue.bigDecimal(valueNull);
							
							for (WConcept item : wConcepts) {
								// Subtotal
								voucherSubtotal = voucherSubtotal.add(UValue.bigDecimalStrict(item.getAmount(), this.currency.getDecimales()));
								
								// Descuento
								if (!UValidator.isNullOrEmpty(item.getDiscount())) {
									
									BigDecimal conceptQuantity = UValue.bigDecimalStrict(item.getQuantity(), this.currency.getDecimales());
									BigDecimal conceptUnitValue = UValue.bigDecimalStrict(item.getUnitValue(), this.currency.getDecimales());
									BigDecimal conceptDiscount = UValue.bigDecimalStrict(item.getDiscount(), this.currency.getDecimales());
									
									//conceptDiscount = UValue.bigDecimalStrict(UValue.bigDecimalString(UCalculation.discount(conceptQuantity, conceptUnitValue, conceptDiscount)), this.currency.getDecimales());
									
									BigDecimal discountCalc = UCalculation.discount(conceptQuantity, conceptUnitValue, conceptDiscount);
									String discountCalcString = UValue.bigDecimalString(discountCalc); 
									conceptDiscount = UValue.bigDecimalStrict(discountCalcString, this.currency.getDecimales());
									
									
									voucherDiscount = voucherDiscount.add(conceptDiscount);
										
								}
								
								// Total de impuestos trasladados
								for (WTaxes transferred : item.getTaxesTransferred()) {
									if (!transferred.getFactorType().value().equalsIgnoreCase("exento")) {
										if (!UValidator.isNullOrEmpty(transferred.getAmount())) {
											if (UValidator.isNullOrEmpty(voucherTaxTransferred)) {
												voucherTaxTransferred = UValue.bigDecimal(valueNull);
											}
											voucherTaxTransferred = voucherTaxTransferred.add(transferred.getAmount());
										}
									} else {
										if (UValidator.isNullOrEmpty(voucherTaxTransferred)) {
											voucherTaxTransferred = UValue.bigDecimal(valueNull);
										} else {
											voucherTaxTransferred = voucherTaxTransferred.add(UValue.bigDecimal(valueNull));
										}
									}									
								}
								
								// Total de impuestos retenidos
								for (WTaxes withheld : item.getTaxesWithheld()) {
									if (!UValidator.isNullOrEmpty(withheld.getAmount())) {
										if (UValidator.isNullOrEmpty(voucherTaxWithheld)) {
											voucherTaxWithheld = UValue.bigDecimal(valueNull);
										}
										voucherTaxWithheld = voucherTaxWithheld.add(withheld.getAmount());
									}
								}
							}
							voucherTotal = UCalculation.total(voucherSubtotal, voucherDiscount, voucherTaxTransferred, voucherTaxWithheld, currencyEntityByCode.getDecimales());
							
							this.wAutomaticCalculations = new WAutomaticCalculations();
							this.wAutomaticCalculations.setVoucherSubtotal(voucherSubtotal);
							this.wAutomaticCalculations.setVoucherDiscount(voucherDiscount);
							this.wAutomaticCalculations.setVoucherTaxTransferred(voucherTaxTransferred);
							this.wAutomaticCalculations.setVoucherTaxWithheld(voucherTaxWithheld);
							this.wAutomaticCalculations.setVoucherTotal(voucherTotal);
							
							voucher = new JVoucher();
							voucher.setSubTotal(UBase64.base64Encode(UValue.bigDecimalString(voucherSubtotal)));
							voucher.setDiscount(UBase64.base64Encode(UValue.bigDecimalString(voucherDiscount)));
							voucher.setTotalTaxesTransferred(UBase64.base64Encode(UValue.bigDecimalString(voucherTaxTransferred)));
							voucher.setTotalTaxesWithheld(UBase64.base64Encode(UValue.bigDecimalString(voucherTaxWithheld)));
							voucher.setTotal(UBase64.base64Encode(UValue.bigDecimalString(voucherTotal)));
							
							// JSon response 
							response.setSuccess(Boolean.TRUE);
						} else {
							response.setError(Boolean.TRUE);
							response.setErrors(errors);
							response.setNotification(notification);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			String errorCode = !isEdit ? "ERR0051" : "ERR0052";
			notification.setMessage(UProperties.getMessage(errorCode, userTools.getLocale()));
			notification.setModalPanelMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
			response.setTabError("tab-concept");
			response.setErrors(errors);
		}
		
		sessionController.sessionUpdate();
		
		concept.setResponse(response);
		
		calculation.setConcept(concept);
		calculation.setVoucher(voucher);
		
		jsonInString = gson.toJson(calculation);
		return jsonInString;
    }
	
	//##### Adicionando impuestos al concepto
	@RequestMapping(value="/cfdiStamp", params={"addTaxes"})
	@ResponseBody
	public String taxTransferredWithheld(HttpServletRequest request) {
		val fields = new HashMap<String, String>();
		fields.put("baseField", "tax-transferred-withheld-base");
		fields.put("taxTypeField", "tax-transferred-withheld-tax-type");
		fields.put("factorTypeField", "tax-transferred-withheld-factor-type");
		fields.put("rateFeeField", "tax-transferred-withheld-rate-fee");
		fields.put("amountField", "tax-transferred-withheld-amount");
		
		val conceptTaxRequest = populateTaxRequest(request, fields);
		val taxes = taxService.validateTax(conceptTaxRequest);
		
		sessionController.sessionUpdate();
		
		return GsonClient.response(taxes);
	}
	
	private TaxRequest populateTaxRequest(HttpServletRequest request, Map<String, String> fields) {
		return TaxRequest
				.builder()
				.requestData(request.getParameter("objTaxes"))
				.selectedTax(UBase64.base64Decode(request.getParameter("taxSelected")))
				.action(UBase64.base64Decode(request.getParameter("taxAction")))
				.currency(UBase64.base64Decode(request.getParameter("currency")))
				.rateOrFeeJson(this.rateOrFee)
				.baseField(fields.get("baseField"))
				.taxTypeField(fields.get("taxTypeField"))
				.factorTypeField(fields.get("factorTypeField"))
				.rateOrFeeField(fields.get("rateFeeField"))
				.amountField(fields.get("amountField"))
				.build();
	}
	
	//##### Calculando importe de concepto
	@PostMapping("/conceptAmountCalc")
	public @ResponseBody String conceptAmountCalc(HttpServletRequest request) {
		val objConcept = request.getParameter("objConcept");
		val concept = invoiceConceptAmountCalcService.conceptAmountCalc(objConcept, currency);
		
		sessionController.sessionUpdate();
		return GsonClient.response(concept);
	}
	
	public JRateOrFee rateOrFeeLoad() {
		return rateOrFeeService.load();
	}
	
	//##### Complemento de pago :: Adicionar documento relacionado
	@RequestMapping(value = "/cfdiStamp", params = { "complementPaymentDocumentRelated" })
	@ResponseBody
	public String complementPaymentDocumentRelated(HttpServletRequest request) throws IOException {
		val relatedDocumentData = request.getParameter("cmpPaymentDocumentRelatedObj");
		val paymentCurrency = UBase64.base64Decode(request.getParameter("cmpPaymentCurrency"));
		val amountStr = UBase64.base64Decode(request.getParameter("cmpPaymentAmount"));
		
		val response = paymentRelatedDocumentService.validate(relatedDocumentData, paymentCurrency, amountStr);
		val complementPaymentJson = JComplementPayment.builder().response(response).build();
		
		sessionController.sessionUpdate();
		
		return GsonClient.response(complementPaymentJson);
	}
	
	// ##### Complemento de pago :: Cargar pago
	@RequestMapping(value = "/cfdiStamp", params = { "complementPaymentDataLoad" })
	@ResponseBody
	public String complementPaymentDataLoad(HttpServletRequest request) {
		this.paymentIndex = Integer.valueOf(UBase64.base64Decode(request.getParameter("index")));
		val complementPaymentJson = paymentService.loadPaymentData(this.payments, this.paymentIndex);

		sessionController.sessionUpdate();

		return GsonClient.response(complementPaymentJson);
	}

	//##### Complemento de pago :: Eliminar pago
	@RequestMapping(value = "/cfdiStamp", params = { "complementPaymentRemove" })
	@ResponseBody
	public String complementPaymentRemove(HttpServletRequest request) {
		JResponse response = null;
		try {
			
			val index = UBase64.base64Decode(request.getParameter("index"));
        	val indexInteger = Integer.parseInt(index);
        	
        	this.paymentIndex = null;
        	this.payments.remove(indexInteger);
        	
        	response = JResponse.success();
			
		} catch (NumberFormatException | IndexOutOfBoundsException e) {
			e.printStackTrace();
			
			val notification = JNotification.pageMessageError(UProperties.getMessage("ERR0148", userTools.getLocale()));
			response = JResponse.error(notification);
		}
		
		sessionController.sessionUpdate();
		
		val complementPaymentJson = new JComplementPayment(response);
		return GsonClient.response(complementPaymentJson); 
	}
	
	/**
	 * Complemento de pago :: Adicionar/Editar
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/cfdiStamp", params = { "complementPayment" })
	public @ResponseBody String complementPayment(HttpServletRequest request) {
		val complementCode = UBase64.base64Decode(request.getParameter("complementCode"));
		val paymentData = request.getParameter("cmpPaymentObj");

		Integer index = null;
		boolean isEdit = false;
		
		val operation = UBase64.base64Decode(request.getParameter("operation"));
		if (!operation.equals("add") && this.paymentIndex != null) {
			isEdit = true;
			index = this.paymentIndex;
		}

		val complementPaymentJson = paymentService.addOrEditPayment(complementCode, paymentData, isEdit,
				index, this.payments);
		if (complementPaymentJson.getResponse().isSuccess()) {
			this.paymentIndex = null;
		}

		sessionController.sessionUpdate();

		return GsonClient.response(complementPaymentJson);
	}
	
	@Transactional
	@PostMapping(value = "/cfdiStamp")
    public @ResponseBody String stamp(@RequestParam(value="certificate") Optional<MultipartFile> certificateParam,
									  @RequestParam(value="privateKey") Optional<MultipartFile> privateKeyParam, 
									  HttpServletRequest request, 
									  Model model) throws IOException{
		ApiCfdi apiCfdi = new ApiCfdi();
		JVoucher voucher = new JVoucher();
		
		JResponse response = new JResponse();
		JNotification notification = null; 
		JError error = null;
		val errors = new ArrayList<JError>();
		
		try {
			
			userTools.log("[INFO] STAMP STATUS VALIDATING...");
			if (stampControl.stampStatus(this.taxpayer) || userTools.haveDaysLeft()) {
				userTools.log("[INFO] CFDI INFORMATION VALIDATING...");
				
				// ============================================
				// ===== Validando informacion del emisor =====
				// ============================================
				val emitterCertificateRequest = new JEmitterCertificateRequest(this.certificate, certificateParam, privateKeyParam);
				val emitterResponse = emitterValidator.getEmitter(request.getParameter("objEmitter"), emitterCertificateRequest, errors);
				val emitterErrorMarked = emitterResponse.isError();
				
				// ===============================================
				// ===== Validando informacion del receptor ======
				// ===============================================
				val receiverResponse = receiverValidator.getReceiver(request.getParameter("objReceiver"), errors);
				val receiverErrorMarked = receiverResponse.isError();
				
				// =================================================
				// ===== Validando informacion del comprobante =====
				// =================================================
				val voucherData = request.getParameter("objVoucher");
				val relatedCfdiCheck = UBase64.base64Decode(request.getParameter("cfdiRelated"));
				val voucherResponse = voucherValidator.getVoucher(voucherData, relatedCfdiCheck, taxpayer, errors);
				val apiVoucher = voucherResponse.getApiVoucher();
				val voucherErrorMarked = voucherResponse.isError();
				
				// ================================
				// ===== Complemento de pagos =====
				// ================================
				val complementCode = UBase64.base64Decode(request.getParameter("complementCode"));
				var complementErrorMarked = false;
				if (!UValidator.isNullOrEmpty(complementCode) && paymentService.isPayment(complementCode)) {
					val paymentTotalsData = request.getParameter("objPaymentTotals");
					val paymentTotalsResponse = paymentService.populatePaymentTotals(paymentTotalsData, errors);
					this.wPaymentTotal = paymentTotalsResponse.getPaymentTotal();
					complementErrorMarked = paymentTotalsResponse.isError();
				}
				
				if (!emitterErrorMarked && !receiverErrorMarked && !voucherErrorMarked && !complementErrorMarked) {
					// ==================================================
					// ===== Validando informacion de los conceptos =====
					// ==================================================
					val voucherConceptRequest = VoucherConceptRequest
												.builder()
												.currency(voucherResponse.getCurrency())
												.wConcepts(this.wConcepts)
												.build();
					val voucherConceptResponse = voucherConceptValidator.getConcepts(voucherConceptRequest, errors);
					val conceptsTax = voucherConceptResponse.getConceptsTax();
					val conceptErrorMarked = voucherConceptResponse.isError();
					
					// ================================================
					// ===== Validando informacion de los totales =====
					// ================================================
					val voucherTotalsRequest = VoucherTotalsRequest
												.builder()
												.voucherSubtotal(voucherConceptResponse.getVoucherSubtotal())
												.voucherTotalsSubtotal(apiVoucher.getSubTotal())
												.voucherTotalsDiscount(apiVoucher.getDiscount())
												.voucherTotalsTotal(apiVoucher.getTotal())
												.discount(apiVoucher.getDiscount())
												.conceptsTax(conceptsTax)
												.currency(voucherResponse.getCurrency())
												.voucherType(voucherResponse.getVoucherType().getCodigo())
												.discountByConcept(voucherConceptResponse.isDiscountByConcept())
												.build();
					val voucherTotalsErrorMarked = voucherTotalsValidator.validateTotals(voucherTotalsRequest, errors);
					
					if (!conceptErrorMarked && !voucherTotalsErrorMarked) {
						val confirmationCodeRequired = getConfirmationCode(apiVoucher);
						if (!confirmationCodeRequired) {
							// ========================
							// ===== Complementos =====
							// ========================
							val apiComplement = getApiComplement(complementCode);
							
							val apiEmitter = emitterResponse.getApiEmitter();
							val cert = apiEmitter.getCertificado();
							val privateKey = apiEmitter.getLlavePrivada();
							val keyPasswd = apiEmitter.getPasswd();
							
							apiCfdi.setEmitter(apiEmitter);
							apiCfdi.setCertificate(this.certificate);
							apiCfdi.setReceiver(receiverResponse.getApiReceiver());
							apiCfdi.setSerieSheet(voucherResponse.getSerieSheet());
							apiCfdi.setVoucherType(voucherResponse.getVoucherType());
							apiCfdi.setPostalCode(voucherResponse.getExpeditionPlace());
							apiCfdi.setPaymentWay(voucherResponse.getPaymentWay());
							apiCfdi.setPaymentMethod(voucherResponse.getPaymentMethod());
							apiCfdi.setCurrency(voucherResponse.getCurrency());
							apiCfdi.setLegend(UBase64.base64Decode(voucher.getLegend()));
							apiCfdi.setVoucher(apiVoucher);
							apiCfdi.getCustomsInformations().addAll(voucherConceptResponse.getConceptCustomsInformations());
							apiCfdi.getPropertyAccounts().addAll(voucherConceptResponse.getConceptPropertyAccounts());
							apiCfdi.getConcepts().addAll(voucherConceptResponse.getApiConcepts());
							apiCfdi.setComplement(apiComplement);
							
							// Cargando preferencias nuevamente por si hizo algún cambio estando en la vista de facturación
							this.preferences = iPreferencesJpaRepository.findByContribuyente(this.taxpayer);
							apiCfdi.setPreferences(this.preferences);
							
							StampResponse stampResponse;
							if (!useFakeStampService) {
								stampResponse = invoiceService.cfdiStamp(apiCfdi, cert, privateKey, keyPasswd);
							} else {
								stampResponse = cfdiFakeStampService.cfdiFakeStamp(apiCfdi, false);
							}
							
							if (stampResponse.isStamped()) {
								apiCfdi.setStampResponse(stampResponse);
								invoiceStoreService.store(this.taxpayer, this.preferences, apiCfdi);
								
								// Reset concept wrapper
								this.wConcepts = new ArrayList<>();
								
								// Reloading initial content
								initialContent();
								reloadContent();
								
								response.setSuccess(true);
								this.emitterProperties.getStamp().setResponse(response);
								
								userTools.log("[INFO] CFDI WAS GENERATED AND RELATED INFORMATION WAS STORED SUCCESSFULY");
							} else {
								response = stampErrorService.swStampError(stampResponse);
							}
						} else {
							error = JError.error("confirmation-code", "CFDI33119", userTools.getLocale());
							errors.add(error);
							response.setTabError("tab-voucher");
						}
					} else {
						response.setTabError(determineTab(conceptErrorMarked, voucherTotalsErrorMarked));
					}
				} else {
					response.setTabError(determineTab(emitterErrorMarked, receiverErrorMarked, voucherErrorMarked));
				}
			} else {				
				userTools.log("[WARNING] THE USER HAS NOT STAMPS AVAILABLE");
				
				error = JError.message("ERR0163", userTools.getLocale());
				errors.add(error);
				notification = JNotification.pageMessageError(error.getMessage());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			error = JError.message("ERR0164", userTools.getLocale());
			errors.add(error);
			notification = JNotification.pageMessageError(error.getMessage());
		}
		
		if (!errors.isEmpty()) {
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
			response.setErrors(errors);
		}
		
		this.emitterProperties.getStamp().setResponse(response);
		
		// Response
		return stampRespone();
	}

	private ApiCmp getApiComplement(final String complementCode) {
		if (!UValidator.isNullOrEmpty(complementCode) && paymentService.isPayment(complementCode)) {
			val apiCmpPayment = paymentService.getPayment(this.wPaymentTotal, this.payments);
			return new ApiCmp(complementCode, apiCmpPayment);
		}
		return null;
	}
	
	private boolean getConfirmationCode(final ApiVoucher apiVoucher) {
		val amountAlert = "999999999999999999.999999";
		val amountAlertBigDecimal = UValue.bigDecimal(amountAlert);
		return apiVoucher.getTotal().compareTo(amountAlertBigDecimal) > 0 && UValidator.isNullOrEmpty(apiVoucher.getConfirmation());
	}
	
	private String determineTab(boolean emitterTab, boolean receiverTab, boolean voucherTab) {
		String tab = null;
		if (emitterTab) {
			tab = "tab-emitter";
		} else if (receiverTab) {
			tab = "tab-receiver";
		} else if (voucherTab) {
			tab = "tab-voucher";
		}
		return tab;
	}
	
	private String determineTab(boolean conceptTab, boolean voucherTab) {
		String tab = null;
		if (conceptTab) {
			tab = "tab-concept";
		} else if (voucherTab) {
			tab = "tab-voucher";
		}
		return tab;
	}
	
	private final String stampRespone() {
		val response = this.emitterProperties.getStamp().getResponse();		
		if (!response.isError()) {
			//##### Notificacion Toastr
			var toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.generate.cfdi.title", userTools.getLocale());
			var toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.generate.cfdi.message", userTools.getLocale());
			val toastr = Toastr.success(toastrTitle, toastrMessage);
			val notification = JNotification.toastrNotification(toastr);
			response.setNotification(notification);
		}

		sessionController.sessionUpdate();
		this.emitterProperties.getStamp().setResponse(response);
		return GsonClient.response(this.emitterProperties);
	}
	
	@PostMapping(value = "/voucherConfirmationCode")
	public @ResponseBody String confirmationCode() {
		
		val error = new JError();
		List<JError> errors = new ArrayList<>();
		val notification = new JNotification();
		val response = new JResponse();
		var confirmationCode = new JConfirmationCode();
		
		try {

			String environment = appSettings.getPropertyValue("cfdi.environment");
			String emitterRfcActive = userTools.getContribuyenteActive().getRfcActivo();
			
			val properties = ConfirmationCodeProperties
								.builder()
								.emitterId(propertiesTools.getEmitterId())
								.sessionId(propertiesTools.getSessionId())
								.sourceSystem(propertiesTools.getSourceSystem())
								.correlationId(propertiesTools.getCorrelationId())
								.environment(Integer.valueOf(environment))
								.rfc(emitterRfcActive)
								.build();
			
			val confirmationCodeResponse = ConfirmationCode.confirmationCode(properties);
			if (confirmationCodeResponse.isSuccess()) {
				confirmationCode = confirmationCodeSuccess(confirmationCodeResponse);
			} else {
				confirmationCode = confirmationCodeError(confirmationCodeResponse);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			error.setMessage(UProperties.getMessage("ERR0045", userTools.getLocale()));
			errors.add(error);
			
			notification.setPageMessage(true);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(error.getMessage());
			
			response.setTabError("tab-voucher");
			response.setErrors(errors);
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
			confirmationCode.setResponse(response);
		}
		
		sessionController.sessionUpdate();
		return GsonClient.response(confirmationCode);
	}

	private JConfirmationCode confirmationCodeSuccess(ConfirmationCodeResponse confirmationCodeResponse) {
		val message = UBase64.base64Encode(UProperties.getMessage("mega.cfdi.information.confirmation.code.success", userTools.getLocale()));
		val confirmationCode = UBase64.base64Encode(confirmationCodeResponse.getConfirmationCode());
		
		return JConfirmationCode
				.builder()
				.message(message)
				.confirmationCode(confirmationCode)
				.response(JResponse.success())
				.build();
	}
	
	private JConfirmationCode confirmationCodeError(ConfirmationCodeResponse confirmationCodeResponse) {
		JNotification notification = null; 
		if (confirmationCodeResponse.getError() != null) {
			val notificationMessage = UProperties.getMessage(confirmationCodeResponse.getErrorCode(), userTools.getLocale());
			notification = JNotification.pageMessageError(notificationMessage);
		}
		
		val errorMessage = UProperties.getMessage(Validator.ErrorSource.fromValue("CFDI33119").value(), userTools.getLocale());
		val error = JError.builder().message(errorMessage).field("confirmation-code").build();
		val errors = Arrays.asList(error);
		
		val response = JResponse
						.builder()
						.notification(notification)
						.errors(errors)
						.error(Boolean.TRUE)
						.tabError("tab-voucher")
						.build();
		
		return JConfirmationCode.builder().response(response).build();
	}
	
}