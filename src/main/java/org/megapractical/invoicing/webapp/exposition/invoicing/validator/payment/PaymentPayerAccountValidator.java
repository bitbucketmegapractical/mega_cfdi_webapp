package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentPayerAccountValidator {

	String getPayerAccount(String payerAccount, FormaPagoEntity paymentWay);
	
	String getPayerAccount(String payerAccount, String field, FormaPagoEntity paymentWay, List<JError> errors);

}