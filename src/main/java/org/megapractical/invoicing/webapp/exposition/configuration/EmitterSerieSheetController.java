package org.megapractical.invoicing.webapp.exposition.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerSerieSheetJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JSerieSheet;
import org.megapractical.invoicing.webapp.json.JSerieSheet.SerieSheet;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.notification.Toastr.ToastrType;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.ui.Paginator;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WSerieSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import lombok.val;

@Controller
@Scope("session")
public class EmitterSerieSheetController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	DataStoreService dataStore;
	
	@Resource
	ICfdiJpaRepository iCfdiJpaRepository;
	
	@Resource
	ITaxpayerSerieSheetJpaRepository iTaxpayerSerieSheetJpaRepository;
	
	private static final String SORT_PROPERTY = "serie";
	private static final Integer INITIAL_PAGE = 0;
	
	private String configAction;
	private String unexpectedError;
	
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	@ModelAttribute("processingRequest")
	public String processingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@ModelAttribute("updatingRequest")
	public String updatingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.updating", userTools.getLocale());
	}
	
	// Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() throws IOException{
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	@RequestMapping(value = "/emitterSerieSheet", method = RequestMethod.GET)
	public String emitterSerieSheet(Model model, Integer page, String searchString) {
		try {
			
			this.unexpectedError = UProperties.genericUnexpectedError(userTools.getLocale());
			
			model.addAttribute("page", INITIAL_PAGE);
			
			HtmlHelper.functionalitySelected("EmitterConfig", "emitter-config-serie-sheet");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			return "/config/EmitterSerieSheet";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/emitterSerieSheet", method = RequestMethod.POST)
	public @ResponseBody String emitterSerieSheet(HttpServletRequest request) {
		
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JSerieSheet serieSheet = new JSerieSheet();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchSerie = UBase64.base64Decode(request.getParameter("searchSerie"));
			if(UValidator.isNullOrEmpty(searchSerie)){
				searchSerie = "";
			}
			
			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Page<ContribuyenteSerieFolioEntity> serieSheetPage = null;
			Paginator<ContribuyenteSerieFolioEntity> paginator = null;
			
			serieSheetPage = iTaxpayerSerieSheetJpaRepository.findByContribuyenteAndSerieContainingIgnoreCaseAndEliminadoFalse(taxpayer, searchSerie, pageRequest);
			val pagination = JPagination.buildPaginaton(serieSheetPage);
			serieSheet.setPagination(pagination);
			
			for (ContribuyenteSerieFolioEntity item : serieSheetPage) {
				SerieSheet serieSheetObj = new SerieSheet(); 
				serieSheetObj.setId(UBase64.base64Encode(item.getId().toString()));
				serieSheetObj.setSerie(UBase64.base64Encode(item.getSerie()));
				serieSheetObj.setInitialSheet(UBase64.base64Encode(item.getFolioInicial().toString()));
				serieSheetObj.setEndSheet(UBase64.base64Encode(item.getFolioFinal().toString()));
				
				Integer actualSheet = item.getFolioActual() + 1;
				serieSheetObj.setActualSheet(UBase64.base64Encode(actualSheet.toString()));
				
				serieSheetObj.setCreationDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaCreacion())));
				if(!UValidator.isNullOrEmpty(item.getFechaUltimaModificacion())){
					serieSheetObj.setLastUpdatedDate(UBase64.base64Encode(UDate.formattedSingleDate(item.getFechaUltimaModificacion())));
				}
				serieSheet.getSerieSheets().add(serieSheetObj);
			}
			
			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if(!UValidator.isNullOrEmpty(searchSerie)){
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
			}
			
			if(serieSheet.getSerieSheets().isEmpty()){
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}
			
			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		sessionController.sessionUpdate();
		
		serieSheet.setResponse(response);
		jsonInString = gson.toJson(serieSheet);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterSerieSheet", params={"serieSheetDataLoad"})
	public @ResponseBody String emitterSerieSheetDataLoad(HttpServletRequest request) throws IOException{
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JSerieSheet serieSheet = new JSerieSheet();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			ContribuyenteSerieFolioEntity serieSheetEntity = iTaxpayerSerieSheetJpaRepository.findByIdAndEliminadoFalse(id);
			if(!UValidator.isNullOrEmpty(serieSheetEntity)){
				SerieSheet serieSheetObj = new SerieSheet(); 
				serieSheetObj.setSerie(UBase64.base64Encode(serieSheetEntity.getSerie()));
				serieSheetObj.setInitialSheet(UBase64.base64Encode(serieSheetEntity.getFolioInicial().toString()));
				serieSheetObj.setEndSheet(UBase64.base64Encode(serieSheetEntity.getFolioFinal().toString()));
				
				Integer actualSheet = serieSheetEntity.getFolioActual() + 1;
				serieSheetObj.setActualSheet(UBase64.base64Encode(actualSheet.toString()));
				
				serieSheetObj.setCreationDate(UBase64.base64Encode(UDate.formattedSingleDate(serieSheetEntity.getFechaCreacion())));
				if(!UValidator.isNullOrEmpty(serieSheetEntity.getFechaUltimaModificacion())){
					serieSheetObj.setLastUpdatedDate(UBase64.base64Encode(UDate.formattedSingleDate(serieSheetEntity.getFechaUltimaModificacion())));
				}
				
				Boolean enableEdit = true;
				List<CfdiEntity> cfdis = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndSerieIgnoreCase(serieSheetEntity.getContribuyente(), serieSheetEntity.getSerie());
				if(!cfdis.isEmpty()){
					enableEdit = false;
				}
				serieSheetObj.setEnableEdit(enableEdit);
				
				serieSheet.setSerieSheet(serieSheetObj);
				
				response.setSuccess(Boolean.TRUE);				
			}else{
				notification.setMessage(UProperties.getMessage("ERR0087", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0087", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		serieSheet.setResponse(response);
		jsonInString = gson.toJson(serieSheet);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterSerieSheet", params={"serieSheetRemove"})
    public @ResponseBody String emitterSerieSheetRemove(HttpServletRequest request) throws IOException {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JSerieSheet serieSheet = new JSerieSheet();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
        	
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			ContribuyenteSerieFolioEntity serieSheetEntity = iTaxpayerSerieSheetJpaRepository.findByIdAndEliminadoFalse(id);
			if(!UValidator.isNullOrEmpty(serieSheetEntity)){
				
				//##### NOTA: No se puede eliminar una serie si hay al menos una factura (CFDI) vigente con la misma
				List<CfdiEntity> cfdis = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndSerieIgnoreCase(serieSheetEntity.getContribuyente(), serieSheetEntity.getSerie());
				if(cfdis.isEmpty()){
					serieSheetEntity.setEliminado(Boolean.TRUE);
					persistenceDAO.update(serieSheetEntity);
					
					// Bitacora :: Registro de accion en bitacora
					appLog.logOperation(OperationCode.ELIMINAR_SERIE_FOLIO_ID55);
					
					// Timeline :: Registro de accion
					timelineLog.timelineLog(EventTypeCode.ELIMINAR_SERIE_FOLIO_ID30);
					
					response.setSuccess(Boolean.TRUE);
				}else{
					notification.setMessage(UProperties.getMessage("ERR0092", userTools.getLocale()));
					notification.setPageMessage(Boolean.TRUE);
					notification.setCssStyleClass(CssStyleClass.WARNING.value());
					
					response.setNotification(notification);
					response.setError(Boolean.TRUE);
				}
			}else{
				notification.setMessage(UProperties.getMessage("ERR0090", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}			
        	
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0090", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}		
		
		sessionController.sessionUpdate();
		
		serieSheet.setResponse(response);
		jsonInString = gson.toJson(serieSheet);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterSerieSheet", params={"serieSheetAddEdit"})
    public @ResponseBody String emitterCustomerAddEdit(HttpServletRequest request) throws IOException {
		JSerieSheet serieSheetJson = new JSerieSheet();
		SerieSheet serieSheet = new SerieSheet();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			this.configAction = UBase64.base64Decode(request.getParameter("operation"));
			this.unexpectedError = this.configAction.equals("config") ? UProperties.getMessage("ERR0088", userTools.getLocale()) : UProperties.getMessage("ERR0089", userTools.getLocale());
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Boolean serieSheetDeletedFinded = false;
			Boolean errorMarked = false;
			
			ContribuyenteSerieFolioEntity serieSheetEntity = new ContribuyenteSerieFolioEntity();
			
			if(request.getParameter("objSerieSheet") != null){
				String objSerieSheet = request.getParameter("objSerieSheet");
				serieSheet = gson.fromJson(objSerieSheet, SerieSheet.class);
				
				//##### Id
				Long serieSheetId = null;
				String index = UBase64.base64Decode(serieSheet.getId());
				if(!UValidator.isNullOrEmpty(index)){
					serieSheetId = UValue.longValue(index);
				}
				
				//##### Indicar si se puede editar la serie y el folio inicial
				String enableEditStr = request.getParameter("enableEdit");
				Boolean enableEdit = true;
				if(!UValidator.isNullOrEmpty(enableEditStr)){
					enableEdit = Boolean.valueOf(enableEditStr);
				}
				
				//##### Serie
				String serie = UBase64.base64Decode(serieSheet.getSerie());
				error = new JError();
				
				if(UValidator.isNullOrEmpty(serie)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else if(this.configAction.equals("config")){
					ContribuyenteSerieFolioEntity serieSheetObj = iTaxpayerSerieSheetJpaRepository.findByContribuyenteAndSerieIgnoreCaseAndEliminadoFalse(taxpayer, serie);
					if(!UValidator.isNullOrEmpty(serieSheetObj)){
						error.setMessage(UProperties.getMessage("ERR0091", userTools.getLocale()));
						errorMarked = true;
					}else{
						serieSheetObj = iTaxpayerSerieSheetJpaRepository.findByContribuyenteAndSerieIgnoreCaseAndEliminadoTrue(taxpayer, serie);
						if(!UValidator.isNullOrEmpty(serieSheetObj)){
							serieSheetEntity = serieSheetObj;
							serieSheetDeletedFinded = true;
						}else{
							serieSheetEntity = new ContribuyenteSerieFolioEntity();
						}							
					}
				}else if(this.configAction.equals("edit")){
					serieSheetEntity = iTaxpayerSerieSheetJpaRepository.findByIdAndEliminadoFalse(serieSheetId);
					String serieSheetSerie = serieSheetEntity.getSerie();
					if(!enableEdit && !serie.equalsIgnoreCase(serieSheetSerie)){
						error.setMessage(UProperties.getMessage("ERR0098", userTools.getLocale()));
						errorMarked = true;
					}else if(!serie.equalsIgnoreCase(serieSheetSerie)){
						ContribuyenteSerieFolioEntity serieSheetObj = iTaxpayerSerieSheetJpaRepository.findByContribuyenteAndSerieIgnoreCaseAndEliminadoFalse(taxpayer, serie);
						if(!UValidator.isNullOrEmpty(serieSheetObj)){
							error.setMessage(UProperties.getMessage("ERR0091", userTools.getLocale()));
							errorMarked = true;
						}
					}
				}
				if(errorMarked){
					error.setField("serie");
					errors.add(error);
				}else{
					serie = serie.toUpperCase();
				}
				
				//##### Folio inicial
				String initialSheetStr = UValue.stringValue(UBase64.base64Decode(serieSheet.getInitialSheet()));
				Integer initialSheet = 0;
				error = new JError();
				errorMarked = false;
				Boolean initialSheetErrorMarked = false;
				
				if(UValidator.isNullOrEmpty(initialSheetStr)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					initialSheet = Integer.valueOf(initialSheetStr);
					if(initialSheet < 1){
						error.setMessage(UProperties.getMessage("ERR0093", userTools.getLocale()));
						errorMarked = true;
						initialSheetErrorMarked = true;
					}else{
						if(this.configAction.equals("edit")){
							if(!enableEdit && initialSheet != serieSheetEntity.getFolioInicial()){
								error.setMessage(UProperties.getMessage("ERR0099", userTools.getLocale()));
								errorMarked = true;
								initialSheetErrorMarked = true;
							}
						}
					}
				}
				if(errorMarked){
					error.setField("initial-sheet");
					errors.add(error);
				}
				
				//##### Folio actual
				String actualSheetStr = UValue.stringValue(UBase64.base64Decode(serieSheet.getActualSheet()));
				Integer actualSheet = 1;
				if(this.configAction.equals("edit")){
					actualSheet = Integer.valueOf(actualSheetStr);
				}
				
				//##### Folio inicial
				String endSheetStr = UValue.stringValue(UBase64.base64Decode(serieSheet.getEndSheet()));
				Integer endSheet = 0;
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(endSheetStr)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					endSheet = Integer.valueOf(endSheetStr);
					if(endSheet < 1){
						error.setMessage(UProperties.getMessage("ERR0094", userTools.getLocale()));
						errorMarked = true;
					}else{
						if(!initialSheetErrorMarked){
							if(initialSheet > endSheet){
								error.setMessage(UProperties.getMessage("ERR0095", userTools.getLocale()));
								errorMarked = true;
							}else{
								if(this.configAction.equals("edit")){
									if(actualSheet > endSheet){
										error.setMessage(UProperties.getMessage("ERR0096", userTools.getLocale()));
										errorMarked = true;
									}else{
										//##### El folio final no puede ser menor que algun folio de alguna factura (CFDI) vigente que tenga la misma serie 
										List<CfdiEntity> cfdis = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndSerieIgnoreCase(taxpayer, serie);
										if(!cfdis.isEmpty()){
											Boolean failed = false;
											for (CfdiEntity item : cfdis) {
												Integer cfdiSheet = Integer.valueOf(item.getFolio());
												if(cfdiSheet > endSheet){
													failed = true;
													break;
												}
											}
											if(failed){
												error.setMessage(UProperties.getMessage("ERR0097", userTools.getLocale()));
												errorMarked = true;
											}
										}
									}
								}
							}
						}
					}
				}
				if(errorMarked){
					error.setField("end-sheet");
					errors.add(error);
				}
				
				if(!errors.isEmpty()){
					response.setError(Boolean.TRUE);
					response.setErrors(errors);
				}else{
					serieSheetEntity.setContribuyente(taxpayer);
					
					if(this.configAction.equals("config") || (this.configAction.equals("edit") && enableEdit)){
						serieSheetEntity.setSerie(serie);
						serieSheetEntity.setFolioInicial(initialSheet);
						serieSheetEntity.setFolioActual(initialSheet - 1);
						
						if(this.configAction.equals("edit")){
							serieSheetEntity.setFechaUltimaModificacion(new Date());
						}
					}
					
					serieSheetEntity.setFolioFinal(endSheet);
					serieSheetEntity.setFechaCreacion(new Date());
					serieSheetEntity.setEliminado(Boolean.FALSE);
					
					WSerieSheet wSerieSheet = new WSerieSheet();
					wSerieSheet.setSerieSheet(serieSheetEntity);
					wSerieSheet.setAction(this.configAction);
					wSerieSheet.setSerieSheetDeletedFinded(serieSheetDeletedFinded);
					
					serieSheetEntity = null;
					serieSheetEntity = dataStore.serieSheetStore(wSerieSheet);
					
					serieSheet.setSerie(UBase64.base64Encode(serieSheetEntity.getSerie()));
					serieSheet.setInitialSheet(UBase64.base64Encode(serieSheetEntity.getFolioInicial().toString()));
					serieSheet.setEndSheet(UBase64.base64Encode(serieSheetEntity.getFolioFinal().toString()));
					
					actualSheet = serieSheetEntity.getFolioActual() + 1;
					serieSheet.setActualSheet(UBase64.base64Encode(actualSheet.toString()));
					
					serieSheet.setCreationDate(UBase64.base64Encode(UDate.formattedSingleDate(serieSheetEntity.getFechaCreacion())));
					if(!UValidator.isNullOrEmpty(serieSheetEntity.getFechaUltimaModificacion())){
						serieSheet.setLastUpdatedDate(UBase64.base64Encode(UDate.formattedSingleDate(serieSheetEntity.getFechaUltimaModificacion())));
					}
					
					if(!UValidator.isNullOrEmpty(serieSheetEntity)){
						OperationCode operationCode = null;
						EventTypeCode eventTypeCode = null;
						String toastrTitle = null;
						String toastrMessage = null;
						
						if(this.configAction.equals("edit")){
							operationCode = OperationCode.ACTUALIZACION_SERIE_FOLIO_ID54;
							eventTypeCode = EventTypeCode.ACTUALIZACION_SERIE_FOLIO_ID29;
							toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.serie.sheet.edit.title", userTools.getLocale());
							toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.serie.sheet.edit.message", userTools.getLocale());
						}else{						
							operationCode = OperationCode.REGISTRO_SERIE_FOLIO_ID53;
							eventTypeCode = EventTypeCode.REGISTRO_SERIE_FOLIO_ID28;
							toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.serie.sheet.config.title", userTools.getLocale());
							toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.serie.sheet.config.message", userTools.getLocale());
						}
						serieSheet.setId(UBase64.base64Encode(serieSheetEntity.getId().toString()));
						serieSheetJson.setSerieSheet(serieSheet);
						
						// Bitacora :: Registro de accion en bitacora
						appLog.logOperation(operationCode);
						
						// Timeline :: Registro de accion
						timelineLog.timelineLog(eventTypeCode);
						
						// Notificacion Toastr
						Toastr toastr = new Toastr();
						toastr.setTitle(toastrTitle);
						toastr.setMessage(toastrMessage);
						toastr.setType(ToastrType.SUCCESS.value());
						
						notification.setToastrNotification(Boolean.TRUE);
						notification.setToastr(toastr);
						
						response.setSuccess(Boolean.TRUE);
						response.setNotification(notification);
					}else{
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(this.unexpectedError);
						response.setNotification(notification);
						response.setError(Boolean.TRUE);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(this.unexpectedError);
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		serieSheetJson.setResponse(response);
		jsonInString = gson.toJson(serieSheetJson);
		
		return jsonInString;
	}	
}