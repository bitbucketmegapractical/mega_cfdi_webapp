package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiConsolidate;
import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.ApiFileErrorWritter;
import org.megapractical.invoicing.api.util.UConsolidate;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UZipper;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPathDetail;
import org.megapractical.invoicing.api.wrapper.ApiPaymentError;
import org.megapractical.invoicing.api.wrapper.ApiPaymentParser;
import org.megapractical.invoicing.api.wrapper.ApiPaymentStamp;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.PaymentFileStatisticService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.PaymentFileStoreService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.service.InvoiceFileService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.service.InvoiceFileStatsService;
import org.megapractical.invoicing.webapp.exposition.invoicing.service.InvoiceService;
import org.megapractical.invoicing.webapp.exposition.service.nomenclator.FileStatusService;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.FilePathUtils;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class PaymentProcessingServiceImpl implements PaymentProcessingService {

	@Autowired
	private UserTools userTools;
	
	@Autowired
	private AppSettings appSettings;
	
	@Autowired
	private PropertiesTools propertiesTools;

	@Autowired
	private FileStatusService fileStatusService;

	@Autowired
	private PaymentParserService paymentParserService;

	@Autowired
	private PaymentBuilderService paymentBuilderService;

	@Autowired
	private PaymentFileStatisticService paymentFileStatisticService;

	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private CfdiFakeStampService cfdiFakeStampService;
	
	@Autowired
	private CfdiBuilderService cfdiBuilderService;

	@Autowired
	private StampControlService stampControlService;

	@Autowired
	private PaymentFileStoreService paymentFileStoreService;
	
	@Autowired
	private InvoiceFileService invoiceFileService;
	
	@Autowired
	private InvoiceFileStatsService invoiceFileStatsService;

	@Override
	@Transactional
	public void paymentProcessing(ApiPaymentStamp paymentStamp) {
		try {
			// Cambiando estado del archivo a Procesando
			val fileStatus = fileStatusService.findByCode(FileStatus.PROCESSING);
			paymentStamp.getInvoiceFile().setEstadoArchivo(fileStatus);

			// Actualizando archivo
			invoiceFileService.save(paymentStamp.getInvoiceFile());

			// Procesando el fichero
			val paymentParser = paymentParserService.paymentParser(paymentStamp);
			if (paymentParser != null) {
				// Listado de errores
				var errors = paymentParser.getErrors();
				if (errors == null) {
					errors = new ArrayList<>();
				}
				
				// Determinando la cantidad de facturas a timbrar
				val paymentToStamp = paymentParser.getStatistics().getSingleEntrie();
				
				// Obteniendo la cantidad de timbres disponibles del emisor
				val emitterAvailableStamp = paymentParser.getPaymentStamp().getStampDetail().getTimbresDisponibles();
				
				// Listado auxialiar de comprobantes no timbrados
				// En caso que el codigo de error no sea reconocido, intentar retimbrar
				val auxiliaryVouchers = new ArrayList<ApiCfdi>();
				if (!UValidator.isNullOrEmpty(paymentToStamp)) {
					if (paymentToStamp <= emitterAvailableStamp) {
						// Indicando que el emisor tiene suficientes timbres para timbrar todas
						// las facturas del archivo
						paymentParser.getPaymentStamp().getInvoiceFile().setTimbresDisponibles(Boolean.TRUE);
						// Actualizando archivo
						invoiceFileService.save(paymentParser.getPaymentStamp().getInvoiceFile());
						// Estadisticas generadas del archivo
						val paymentFileStatistics = paymentFileStatisticService.managePaymentFileStatistics(paymentParser);
						// Cargando las estadisticas del archivo
						paymentStamp.setStatistics(paymentFileStatistics);
						// Generando comprobantes
						val vouchers = paymentBuilderService.build(paymentParser);
						if (!vouchers.isEmpty()) {
							// Obteniendo la ruta absoluta del archivo
							val folderAbsolutePath = paymentStamp.getAbsolutePath();
							// Obteniendo la ruta en db
							val dbPath = paymentStamp.getDbPath();
							// Generando rutas segun la configuracion del usuario
							val pathDetail = FilePathUtils.configurePaths(folderAbsolutePath,
									paymentStamp.getInvoiceFile().getNombre(), dbPath, false);
							val pdfFolder = pathDetail.getPdfPath();
							// Contador de facturas timbradas
							val paymentStamped = paymentStamp(paymentStamp, vouchers, auxiliaryVouchers, pathDetail, errors);
							// Verificando si quedo alguna factura sin timbrar
							updateFailureStats(paymentStamp, errors, auxiliaryVouchers);
							// Verificando si hay errores
							verifyErrors(paymentStamp, paymentParser, errors, folderAbsolutePath, dbPath);
							// Cargando las configuraciones de archivo del usuario
							val paymentFileConfig = paymentStamp.getPaymentFileConfig();
							
							if (paymentStamped >= 1) {
								// Verificar si se genera compactado
								verifyZipper(paymentStamp, paymentParser, dbPath, paymentFileConfig);
								verifyConsolidate(paymentStamp, paymentParser, folderAbsolutePath, dbPath, pdfFolder,
										paymentFileConfig);
							}
							
							// Cambiando estado del archivo a Generado
							val fileStatusGenerated = fileStatusService.findByCode(FileStatus.GENERATED);
							paymentParser.getPaymentStamp().getInvoiceFile().setEstadoArchivo(fileStatusGenerated);
							// Actualizando archivo
							invoiceFileService.save(paymentParser.getPaymentStamp().getInvoiceFile());
							// Actualizando estadisticas del archivo
							paymentStamp.getStatistics().setFechaFinTimbrado(new Date());
							paymentStamp.getStatistics().setHoraFinTimbrado(new Date());
							invoiceFileStatsService.save(paymentStamp.getStatistics());
							// Verificar si hay algun otro archivo en cola
							val paymentFileList = invoiceFileService.findQueuedFiles(paymentStamp.getTaxpayer());
							processingQueuedFiles(paymentStamp, paymentFileList);
						} else {
							// Verificando si hay errores
							verifyErrors(paymentStamp, paymentParser, errors);
							
							val fileStatusGenerated = fileStatusService.findByCode(FileStatus.GENERATED);
							paymentParser.getPaymentStamp().getInvoiceFile().setEstadoArchivo(fileStatusGenerated);
							
							// Actualizando archivo
							invoiceFileService.save(paymentParser.getPaymentStamp().getInvoiceFile());
							
							// Estadisticas generadas del archivo
							paymentFileStatisticService.managePaymentFileStatistics(paymentParser);
							
							UPrint.logWithLine(UProperties.env(), "[ERROR] PAYMENT PROCESSING SERVICE > NO VOUCHERS WERE GENERATED");
						}
					} else {
						// Indicando que el emisor no tiene suficientes timbres para timbrar todas
						// las facturas del archivo
						paymentParser.getPaymentStamp().getInvoiceFile().setTimbresDisponibles(Boolean.FALSE);
						
						// Cambiando estado del archivo a Cola nuevamente
						EstadoArchivoEntity fileStatusQueued = fileStatusService.findByCode(FileStatus.QUEUED);
						paymentParser.getPaymentStamp().getInvoiceFile().setEstadoArchivo(fileStatusQueued);
						
						// Actualizando archivo
						invoiceFileService.save(paymentParser.getPaymentStamp().getInvoiceFile());
						
						// Estadisticas generadas del archivo
						paymentFileStatisticService.managePaymentFileStatistics(paymentParser);
						
						UPrint.logWithLine(UProperties.env(), "[ERROR] PAYMENT PROCESSING SERVICE > THE EMITTER HASN'T SUFFICIENT STAMPS");
					}
				} else {
					// Obteniendo la ruta absoluta del archivo
					val folderAbsolutePath = paymentStamp.getAbsolutePath();
					// Obteniendo la ruta en db
					val dbPath = paymentStamp.getDbPath();
					
					// Verificando si hay errores
					if (!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()) {
						// Generando ruta de almacenamiento del error
						val errorPath = folderAbsolutePath + File.separator + "ERROR";
						val folderError = new File(errorPath);
						if (!folderError.exists()) {
							folderError.mkdirs();
						}
						
						// Fichero de error a generar
						val fileErrorPath = errorPath + File.separator + paymentStamp.getInvoiceFile().getNombre()
								+ "_ERROR.txt";
						
						// Generando fichero de error
						ApiFileErrorWritter.writeError(fileErrorPath, errors);
						
						// Actualizando la ruta de error en el archivo
						val errorDbPath = dbPath + File.separator + "ERROR" + File.separator
								+ paymentStamp.getInvoiceFile().getNombre() + "_ERROR.txt";
						paymentParser.getPaymentStamp().getInvoiceFile().setRutaError(errorDbPath);
					}
					
					// Cambiando estado del archivo a Generado
					val fileStatusGenerated = fileStatusService.findByCode(FileStatus.GENERATED);
					paymentParser.getPaymentStamp().getInvoiceFile().setEstadoArchivo(fileStatusGenerated);
					
					// Actualizando archivo
					invoiceFileService.save(paymentParser.getPaymentStamp().getInvoiceFile());
					
					// Actualizando estadisticas del archivo
					if (!UValidator.isNullOrEmpty(paymentStamp.getStatistics())) {
						paymentStamp.getStatistics().setFechaFinTimbrado(new Date());
						paymentStamp.getStatistics().setHoraFinTimbrado(new Date());
						invoiceFileStatsService.save(paymentStamp.getStatistics());
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void verifyErrors(ApiPaymentStamp paymentStamp, final ApiPaymentParser paymentParser,
			List<ApiPaymentError> errors) {
		if (!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()) {
			// Obteniendo la ruta absoluta del archivo
			val folderAbsolutePath = paymentStamp.getAbsolutePath();

			// Obteniendo la ruta en db
			val dbPath = paymentStamp.getDbPath();

			// Generando ruta de almacenamiento del error
			val errorPath = folderAbsolutePath + File.separator + "ERROR";
			val folderError = new File(errorPath);
			if (!folderError.exists()) {
				folderError.mkdirs();
			}

			// Fichero de error a generar
			val fileErrorPath = errorPath + File.separator
					+ paymentStamp.getInvoiceFile().getNombre() + "_ERROR.txt";

			// Generando fichero de error
			ApiFileErrorWritter.writeError(fileErrorPath, errors);

			// Actualizando la ruta de error en el archivo
			val errorDbPath = dbPath + File.separator + "ERROR" + File.separator
					+ paymentStamp.getInvoiceFile().getNombre() + "_ERROR.txt";
			paymentParser.getPaymentStamp().getInvoiceFile().setRutaError(errorDbPath);
		}
	}

	private void processingQueuedFiles(ApiPaymentStamp paymentStamp, List<ArchivoFacturaEntity> paymentFileList) {
		String folderAbsolutePath;
		var finded = false;
		// Verificando si el emisor tiene timbres disponibles
		if (stampControlService.stampStatus(paymentStamp.getTaxpayer())) {
			for (val item : paymentFileList) {
				if (item.getEstadoArchivo().getCodigo().equalsIgnoreCase(FileStatus.QUEUED.value())) {
					item.setTimbresDisponibles(Boolean.TRUE);
					// Cargando el archivo
					paymentStamp.setInvoiceFile(item);
					// Ruta de las facturas
					val paymentPath = appSettings.getPropertyValue("cfdi.repository");
					// Ruta db del archivo
					val folderPath = userTools.getContribuyenteActive().getRfcActivo() + File.separator
							+ UDate.date().replace("-", "") + File.separator + item.getNombre();
					paymentStamp.setDbPath(folderPath);
					// Ruta absoluta del archivo
					folderAbsolutePath = paymentPath + File.separator + folderPath;
					paymentStamp.setAbsolutePath(folderAbsolutePath);
					// Ruta fisica del archivo
					val paymentUploadPath = paymentPath + File.separator + item.getRuta();
					paymentStamp.setUploadPath(paymentUploadPath);
					// Actualizando informacion de timbres
					paymentStamp.setStampDetail(stampControlService.stampDetails(paymentStamp.getTaxpayer()));

					finded = true;
					break;
				}
			}

			// Procesando el archivo
			if (finded) {
				paymentProcessing(paymentStamp);
			}
		}
	}

	private int paymentStamp(final ApiPaymentStamp paymentStamp, final List<ApiCfdi> vouchers, final List<ApiCfdi> auxiliaryVouchers,
			ApiPathDetail pathDetail, final List<ApiPaymentError> errors) {
		val taxpayer = paymentStamp.getTaxpayer();
		val user = paymentStamp.getUser();
		val cert = paymentStamp.getApiEmitter().getCertificado();
		val privateKey = paymentStamp.getApiEmitter().getLlavePrivada();
		val keyPasswd = paymentStamp.getApiEmitter().getPasswd();
		
		val statistics = paymentStamp.getStatistics();
		var paymentStamped = statistics.getFacturasTimbradas() == null ? 0 : statistics.getFacturasTimbradas();
		// Timbrando comprobantes
		for (val item : vouchers) {
			// Verificando si el emisor tiene timbres disponibles
			if (stampControlService.stampStatus(taxpayer)) {
				var isStored = false;
				val stampProperties = getStampProperties(taxpayer, user, item, cert, privateKey, keyPasswd);
				val stampResponse = invoiceService.cfdiStamp(stampProperties);
				//val stampResponse = cfdiFakeStampService.paymentFakeStamp(item); // Only for testing
				if (stampResponse.isStamped()) {
					item.setStampResponse(stampResponse);
					determinePath(stampResponse, pathDetail, item);
					isStored = paymentFileStoreService.store(taxpayer, item);
				} else {
					auxiliaryVouchers.add(item);

					val error = ApiPaymentError.stampError(item.getReceiver(), stampResponse.getErrorMessage());
					errors.add(error);
				}

				if (isStored) {
					paymentStamped++;
					statistics.setFacturasTimbradas(paymentStamped);
					invoiceFileStatsService.save(statistics);
					stampControlService.stampControlAsync(taxpayer, user, paymentStamp.getUrlAcceso(), paymentStamp.getIp());
				}
			} else {
				val stampErrorMessage = UProperties.getMessage("ERR0163", userTools.getLocale(user));
				val error = ApiPaymentError.stampStatusError(item.getReceiver(), stampErrorMessage);
				errors.add(error);
				break;
			}
		}
		paymentStamp.setStatistics(statistics);
		return paymentStamped;
	}

	private void determinePath(StampResponse stampResponse, ApiPathDetail pathDetail, final ApiCfdi item) {
		val stampedDate = UDateTime.clean(stampResponse.getDateStamp());
		
		// Xml
		val xmlFileName = item.getReceiver().getRfc() + "_" + stampedDate + ".xml";
		val xmlPath = pathDetail.getXmlPath() + File.separator + xmlFileName;
		val xmlDbPath = pathDetail.getXmlDb() + File.separator + xmlFileName;
		
		// Pdf
		val pdfFileName = xmlFileName.replace(".xml", ".pdf");
		val pdfPath = pathDetail.getPdfPath() + File.separator + pdfFileName;
		val pdfDbPath = pathDetail.getPdfDb() + File.separator + pdfFileName;
		
		item.setXmlPath(xmlPath);
		item.setXmlDbPath(xmlDbPath);
		item.setPdfPath(pdfPath);
		item.setPdfDbPath(pdfDbPath);
	}

	private StampProperties getStampProperties(ContribuyenteEntity taxpayer, UsuarioEntity user, ApiCfdi apiCfdi, byte[] cert, byte[] privateKey, String keyPasswd) {
		val allowStamp = cfdiBuilderService.voucherBuild(apiCfdi, true);
		
		val stampProperties = new StampProperties();
		stampProperties.setEmitterId(propertiesTools.getEmitterId(taxpayer));
		stampProperties.setSessionId(propertiesTools.getSessionId(user));
		stampProperties.setSourceSystem(propertiesTools.getSourceSystem(taxpayer));
		stampProperties.setCorrelationId(propertiesTools.getCorrelationId());
		stampProperties.setVoucher(allowStamp.getVoucher());
		stampProperties.setCertificate(cert);
		stampProperties.setPrivateKey(privateKey);
		stampProperties.setPasswd(keyPasswd);
		stampProperties.setEnvironment(propertiesTools.getEnvironment());
		stampProperties.setServices(propertiesTools.getServices());
		return stampProperties;
	}
	
	private void verifyErrors(ApiPaymentStamp paymentStamp, final ApiPaymentParser paymentParser,
			List<ApiPaymentError> errors, final String folderAbsolutePath, final String dbPath) {
		if (!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()) {
			// Generando ruta de almacenamiento del error
			val errorPath = folderAbsolutePath + File.separator + "ERROR";
			val folderError = new File(errorPath);
			if (!folderError.exists()) {
				folderError.mkdirs();
			}

			// Fichero de error a generar
			val fileErrorPath = errorPath + File.separator + paymentStamp.getInvoiceFile().getNombre() + "_ERROR.txt";

			// Generando fichero de error
			ApiFileErrorWritter.writeError(fileErrorPath, errors);

			// Actualizando la ruta de error en el archivo
			val errorDbPath = dbPath + File.separator + "ERROR" + File.separator
					+ paymentStamp.getInvoiceFile().getNombre() + "_ERROR.txt";
			paymentParser.getPaymentStamp().getInvoiceFile().setRutaError(errorDbPath);
		}
	}

	private void verifyConsolidate(ApiPaymentStamp paymentStamp, final ApiPaymentParser paymentParser,
			final String folderAbsolutePath, final String dbPath, final String pdfFolder,
			final ArchivoFacturaConfiguracionEntity paymentFileConfig) {
		// Verificar si se genera consolidado
		val consolidate = new ApiConsolidate();
		consolidate.setAbsolutePath(folderAbsolutePath);
		consolidate.setSourcePath(pdfFolder);
		consolidate.setConsolidateDbPath(dbPath);
		consolidate.setFileName(paymentStamp.getInvoiceFile().getNombre());

		ApiConsolidate consolidateResult = null;
		if (Boolean.TRUE.equals(paymentFileConfig.getConsolidado1x())) {
			consolidateResult = UConsolidate.consolidateSingle(consolidate);
		} else if (Boolean.TRUE.equals(paymentFileConfig.getConsolidado2x())) {
			consolidateResult = UConsolidate.consolidateDual(consolidate);
		}

		if (consolidateResult != null) {
			paymentParser.getPaymentStamp().getInvoiceFile()
					.setRutaConsolidado(consolidateResult.getConsolidateDbPath());
		}
	}

	private void verifyZipper(ApiPaymentStamp paymentStamp, final ApiPaymentParser paymentParser, final String dbPath,
			final ArchivoFacturaConfiguracionEntity paymentFileConfig) {
		if (Boolean.TRUE.equals(paymentFileConfig.getCompactado())) {
			val paymentFileName = paymentStamp.getInvoiceFile().getNombre();
			val zipDirectory = paymentStamp.getAbsolutePath() + File.separator + paymentFileName;
			val zipFile = zipDirectory + ".zip";
			val zipDbPath = dbPath + File.separator + paymentFileName + ".zip";

			try {
				UPrint.logWithLine(UProperties.env(),
						"[INFO] PAYMENT PROCESSING SERVICE > ZIPPING DIRECTORY: " + zipDirectory);

				UZipper.zipping(new File(zipDirectory), zipFile);
				// Actualizando ruta de compactado
				paymentParser.getPaymentStamp().getInvoiceFile().setRutaCompactado(zipDbPath);

				UPrint.logWithLine(UProperties.env(), "[INFO] PAYMENT PROCESSING SERVICE > ZIPPING DIRECTORY FINISHED");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void updateFailureStats(ApiPaymentStamp paymentStamp, List<ApiPaymentError> errors,
			final ArrayList<ApiCfdi> auxiliaryVouchers) {
		if (!auxiliaryVouchers.isEmpty()) {
			for (val item : auxiliaryVouchers) {
				val stampErrorMessage = UProperties.getMessage("ERR0037", userTools.getLocale());
				val error = ApiPaymentError.stampError(item.getReceiver(), stampErrorMessage);
				errors.add(error);

				// Actualizando estadisticas del archivo
				var paymentStampedError = UValidator.isNullOrEmpty(paymentStamp.getStatistics().getFacturasError()) ? 0
						: paymentStamp.getStatistics().getFacturasError();
				paymentStampedError++;

				paymentStamp.getStatistics().setFacturasError(paymentStampedError);
				invoiceFileStatsService.save(paymentStamp.getStatistics());
			}
		}
	}

}