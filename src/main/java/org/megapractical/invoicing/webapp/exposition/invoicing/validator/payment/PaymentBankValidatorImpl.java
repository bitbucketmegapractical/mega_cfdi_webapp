package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentBankValidatorImpl implements PaymentBankValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Override
	public String getBankName(String bankName, FormaPagoEntity paymentWay, String sourceAccountRfc) {
		if (paymentWay != null) {
			val bankValidate = paymentWay.getBanco();
			if (UValidator.isNullOrEmpty(bankName)) {
				if (bankValidate.equalsIgnoreCase("si") && sourceAccountRfc != null
						&& sourceAccountRfc.equalsIgnoreCase("XEXX010101000")) {
					return null;
				}
			} else if (!UValidator.isNullOrEmpty(bankName) && bankValidate.equalsIgnoreCase("no")) {
				return null;
			}
			return bankName;
		}
		return null;
	}

	@Override
	public String getBankName(String bankName, String field, FormaPagoEntity paymentWay, String sourceAccountRfc,
			List<JError> errors) {
		if (paymentWay != null) {
			JError error = null;
			
			val bankValidate = paymentWay.getBanco();
			if (UValidator.isNullOrEmpty(bankName)) {
				if (bankValidate.equalsIgnoreCase("si") && sourceAccountRfc != null
						&& sourceAccountRfc.equalsIgnoreCase("XEXX010101000")) {
					error = JError.required(field, userTools.getLocale());
				}
			} else if (!UValidator.isNullOrEmpty(bankName) && bankValidate.equalsIgnoreCase("no")) {
				error = JError.error(field, "ERR1006", userTools.getLocale());
			}
			
			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
			}
			return bankName;
		}
		return null;
	}

}