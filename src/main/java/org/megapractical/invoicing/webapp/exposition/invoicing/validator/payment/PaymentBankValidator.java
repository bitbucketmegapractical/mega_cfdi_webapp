package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentBankValidator {

	String getBankName(String bankName, FormaPagoEntity paymentWay, String sourceAccountRfc);

	String getBankName(String bankName, String field, FormaPagoEntity paymentWay, String sourceAccountRfc,
			List<JError> errors);

}