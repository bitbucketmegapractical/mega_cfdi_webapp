package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload;

import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPaymentTotal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentTotalsResponse {
	private WPaymentTotal paymentTotal;
	private boolean error;
	
	public static PaymentTotalsResponse empty() {
		return PaymentTotalsResponse.builder().error(Boolean.FALSE).build();
	}
}