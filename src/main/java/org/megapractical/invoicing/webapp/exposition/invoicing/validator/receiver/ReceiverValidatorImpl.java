package org.megapractical.invoicing.webapp.exposition.invoicing.validator.receiver;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiReceiver;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.PostalCodeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.TaxRegimeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverCfdiUseResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverResidencyResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverTaxRegimeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ValidationResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.cfdiuse.CfdiUseValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.residency.ResidencyValidator;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JReceiver;
import org.megapractical.invoicing.webapp.json.JReceiver.JReceiverResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.RfcUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class ReceiverValidatorImpl implements ReceiverValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private TaxRegimeValidator taxRegimeValidator;
	
	@Autowired
	private PostalCodeValidator postalCodeValidator;
	
	@Autowired
	private CfdiUseValidator cfdiUseValidator;
	
	@Autowired
	private ResidencyValidator residencyValidator;
	
	private static final String ERR1001 = "ERR1001";
	
	@Override
	public JReceiverResponse getReceiver(String receiverData, List<JError> errors) {
		val receiver = GsonClient.deserialize(receiverData, JReceiver.class);
		
		val rfcResponse = getRfc(receiver, errors);
		val rfcError = rfcResponse.isError();
		
		val cfdiUseResponse = getCfdiUse(receiver, errors);
		val cfdiUseError = cfdiUseResponse.isError();
		
		val residencyResponse = getResidency(receiver, errors);
		val residencyError = residencyResponse.isError();
		
		val idRegNumberResponse = getIdentityRegistrationNumber(receiver, errors);
		val idRegNumberError = idRegNumberResponse.isError();
		
		val taxRegimeResponse = getTaxRegime(receiver, errors);
		val taxRegimeError = taxRegimeResponse.isError();
		
		val fiscalResidenceResponse = getFiscalResidence(receiver, errors);
		val fiscalResidenceError = fiscalResidenceResponse.isError();
		
		if (!rfcError && !cfdiUseError && !residencyError && !idRegNumberError 
					  && !taxRegimeError && !fiscalResidenceError) {
			val customerSelectedStr = UBase64.base64Decode(receiver.getCustomerSelected());
			Long customerSelected = null;
			if (!UValidator.isNullOrEmpty(customerSelectedStr)) {
				customerSelected = UValue.longValue(customerSelectedStr);
			}
			
			val receiverNameOrBusinessName = UBase64.base64Decode(receiver.getNameOrBusinessName());
			val receiverEmail = UBase64.base64Decode(receiver.getEmail());
			val receiverCurp = UBase64.base64Decode(receiver.getCurp());
			
			val apiReceiver = ApiReceiver
								.builder()
								.customerSelected(customerSelected)
								.rfc(UValue.stringValueUppercase(rfcResponse.getData()))
								.usoCFDI(cfdiUseResponse.getCfdiUse())
								.nombreRazonSocial(receiverNameOrBusinessName)
								.residenciaFiscal(residencyResponse.getResidency())
								.residente(residencyResponse.getResident())
								.numRegIdTrib(idRegNumberResponse.getData())
								.taxRegimeEntity(taxRegimeResponse.getTaxRegimeEntity())
								.taxRegime(taxRegimeResponse.getTaxRegimeEntity().getCodigo())
								.domicilioFiscal(fiscalResidenceResponse.getData())
								.correoElectronico(receiverEmail)
								.curp(receiverCurp)
								.build();
			
			return new JReceiverResponse(apiReceiver);
		}
		return JReceiverResponse.empty();
	}
	
	private final ValidationResponse getRfc(JReceiver receiver, List<JError> errors) {
		String errorCode = null;
		
		val rfc = UBase64.base64Decode(receiver.getRfc());
		if (UValidator.isNullOrEmpty(rfc)) {
			errorCode = ERR1001;
		} else {
			if (rfc.length() < RfcUtils.RFC_MIN_LENGTH || rfc.length() > RfcUtils.RFC_MAX_LENGTH || !rfc.matches(RfcUtils.RFC_EXPRESSION)) {
				errorCode = RfcUtils.RFC_ERROR_CODE;
			}
		}
		
		var rfcError = false;
		if (!UValidator.isNullOrEmpty(errorCode)) {
			val error = JError.error("rfc", errorCode, userTools.getLocale());
			errors.add(error);
			rfcError = true;
		}
		
		return new ValidationResponse(rfc, rfcError);
	}
	
	private final ReceiverCfdiUseResponse getCfdiUse(JReceiver receiver, List<JError> errors) {
		var cfdiUse = UBase64.base64Decode(receiver.getCfdiUse());
		val field = "cfdi-use";
		return cfdiUseValidator.validate(cfdiUse, field, receiver.getRfc(), errors);
	}
	
	private final ReceiverResidencyResponse getResidency(JReceiver receiver, List<JError> errors) {
		val resident = UBase64.base64Decode(receiver.getResident());
		val field = "residency";
		return residencyValidator.validate(resident, receiver.getResidency(), field, errors);
	}
	
	private final ValidationResponse getIdentityRegistrationNumber(JReceiver receiver, List<JError> errors) {
		val identityRegistrationNumber = UBase64.base64Decode(receiver.getIdentityRegistrationNumber());
		
		val resident = UBase64.base64Decode(receiver.getResident());
		if (resident.equalsIgnoreCase("extranjero")) {
			if (UValidator.isNullOrEmpty(identityRegistrationNumber)) {
				val error = JError.required("identity-registration-number", userTools.getLocale());
				errors.add(error);
				return ValidationResponse.error();
			}
			return new ValidationResponse(identityRegistrationNumber, false);
		}
		return ValidationResponse.empty();
	}
	
	private final ReceiverTaxRegimeResponse getTaxRegime(JReceiver receiver, List<JError> errors) {
		var taxRegime = UBase64.base64Decode(receiver.getTaxRegime());
		val field = "receiver-tax-regime";
		val validatorResponse = taxRegimeValidator.validate(taxRegime, field);
		var taxRegimeEntity = validatorResponse.getTaxRegime();
		var error = validatorResponse.getError();
		
		var taxRegimeError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			taxRegimeError = true;
		}
		
		return new ReceiverTaxRegimeResponse(taxRegimeEntity, taxRegimeError);
	}
	
	private ValidationResponse getFiscalResidence(JReceiver receiver, List<JError> errors) {
		var postalCode = UBase64.base64Decode(receiver.getPostalCode());
		val validatorResponse = postalCodeValidator.validate(postalCode, "receiver-postal-code");
		val postalCodeEntity = validatorResponse.getPostalCode();
		val error = validatorResponse.getError();
		
		String postalCodeVal = null;
		if (postalCodeEntity != null) {
			postalCodeVal = postalCodeEntity.getCodigo();
		}
		
		var postalCodeError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			postalCodeError = true;
		}
		
		return new ValidationResponse(postalCodeVal, postalCodeError);
	}

}