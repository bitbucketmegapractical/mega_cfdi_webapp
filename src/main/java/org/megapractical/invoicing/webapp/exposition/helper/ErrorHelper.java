package org.megapractical.invoicing.webapp.exposition.helper;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.stereotype.Component;

import lombok.val;

@Component
public class ErrorHelper {
	
	private final String locale;

	public ErrorHelper(UserTools userTools) {
		this.locale = userTools.getLocale();
	}
	
	public void evaluateError(String field, String errorCode, final List<JError> errors) {
		if (errorCode != null) {
			val error = JError.error(field, errorCode, locale);
			errors.add(error);
		}
	}
	
	public void requiredFieldError(String field, final List<JError> errors) {
		val error = JError.required(field, locale);
		errors.add(error);
	}
}