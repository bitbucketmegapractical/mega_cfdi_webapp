package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CurrencyValidatorResponse;

public interface CurrencyValidator {
	
	CurrencyValidatorResponse validate(String currency);
	
	CurrencyValidatorResponse validate(String currency, String field);
	
}