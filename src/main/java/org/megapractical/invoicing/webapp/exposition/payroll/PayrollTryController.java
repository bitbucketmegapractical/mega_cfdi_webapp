package org.megapractical.invoicing.webapp.exposition.payroll;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.PayrollDownloadService;
import org.megapractical.invoicing.webapp.exposition.service.PayrollService;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Controller
@RequestMapping("/payrolltry")
@Scope("session")
@RequiredArgsConstructor
public class PayrollTryController {
	
	private final AppSettings appSettings;
	private final AppLog appLog;
	private final UserTools userTools;
	private final PayrollService payrollService;
	private final PayrollDownloadService payrollDownloadService;
	private final SessionController sessionController;
	
	// Contribuyente
	private String activeRfc;
	
	@ModelAttribute("processingRequest")
	public String processingRequest() {
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}

	@ModelAttribute("loadingRequest")
	public String loadingRequest() {
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}

	@ModelAttribute("emptyInformation")
	public String emptyInformation() {
		return UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
	}
	
	@PostConstruct
	private void init() {
		this.activeRfc = userTools.getContribuyenteActive().getRfcActivo();
	}
	
	@GetMapping
	public String payrollTryView(@ModelAttribute("downloadErr") String downloadErr, Model model) {
		HtmlHelper.functionalitySelected("Payroll", "payroll-inbox");
		appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
		
		if (!UValidator.isNullOrEmpty(downloadErr)) {
			model.addAttribute("downloadErrMsg", downloadErr);
		}
		
		return "/payroll/PayrollTry";
	}
	
	@PostMapping
	@ResponseBody
	public String payrollTryList(HttpServletRequest request) {
		// Numero de página
		val pageStr = UBase64.base64Decode(request.getParameter("page"));
		
		// Registros por página
		var paginateSize = appSettings.getPaginationSize();
		val paginate = UBase64.base64Decode(request.getParameter("paginateSize"));
		if (!UValidator.isNullOrEmpty(paginate)) {
			paginateSize = UValue.integerValue(paginate);
		}
		
		// Determinando la accion
		val action = UBase64.base64Decode(request.getParameter("action"));
		
		// Search by
		var searchBy = UBase64.base64Decode(request.getParameter("searchBy"));
		if (UValidator.isNullOrEmpty(searchBy)) {
			searchBy = "";
		}
		
		// Fecha timbrado
		var searchDate = UBase64.base64Decode(request.getParameter("searchDate"));
		if (UValidator.isNullOrEmpty(searchDate)) {
			searchDate = "";
		}

		val payrollData = payrollService.payrollList(this.activeRfc, pageStr, paginateSize, searchBy, searchDate);
		if (!"async".equalsIgnoreCase(action)) {
			sessionController.sessionUpdate();
		}
		return GsonClient.response(payrollData);
	}
	
	@ResponseBody
	@GetMapping("/download/{uuid}/{fileType}")
	public ModelAndView payrollDownload(@PathVariable(value = "uuid") String uuid,
									 	@PathVariable(value = "fileType") String fileType, 
									 	HttpServletResponse response) {
		try {
			val fileDownload = payrollDownloadService.download(uuid, fileType);
			if (fileDownload != null) {
				val header = fileDownload.getHeader();
				
				response.setContentType(fileDownload.getContentType());
				response.setContentLength(fileDownload.getFileByte().length);
				response.setHeader("Content-Disposition", header);
				
				FileCopyUtils.copy(fileDownload.getFileByte(), response.getOutputStream());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		sessionController.sessionUpdate();
		return null;
    }
	
}