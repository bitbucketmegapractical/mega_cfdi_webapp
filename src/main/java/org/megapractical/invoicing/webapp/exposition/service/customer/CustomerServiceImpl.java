package org.megapractical.invoicing.webapp.exposition.service.customer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ClienteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.ICustomerJpaRepository;
import org.megapractical.invoicing.webapp.json.JCustomer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private ICustomerJpaRepository iCustomerJpaRepository;
	
	@Override
	public List<ClienteEntity> findAllByTaxpayer(ContribuyenteEntity taxpayer) {
		if (!UValidator.isNullOrEmpty(taxpayer)) {
			return iCustomerJpaRepository.findByContribuyenteAndEliminadoFalse(taxpayer);
		}
		return Collections.emptyList();
	}

	@Override
	public List<Customer> getMappedCustomers(List<ClienteEntity> sourceList) {
		val customers = new ArrayList<Customer>();
		if (!UValidator.isNullOrEmpty(sourceList)) {
			sourceList.forEach(item -> {
				val customerObj = new Customer();
				customerObj.setId(UBase64.base64Encode(item.getId().toString()));
				customerObj.setRfc(UBase64.base64Encode(item.getRfc()));
				customerObj.setNameOrBusinessName(UBase64.base64Encode(item.getNombreRazonSocial()));
				customerObj.setEmail(UBase64.base64Encode(item.getCorreoElectronico()));
				
				if (!UValidator.isNullOrEmpty(item.getCurp())) {
					customerObj.setCurp(UBase64.base64Encode(item.getCurp()));
				}
				
				customerObj.setResident(UBase64.base64Encode(item.getResidente()));
				if (!UValidator.isNullOrEmpty(item.getPais())) {
					customerObj.setResidency(UBase64.base64Encode(item.getPais().getCodigo().concat(" (").concat(item.getPais().getValor()).concat(")")));
				}
				
				if (!UValidator.isNullOrEmpty(item.getNumeroRegistroIdentidadFiscal())) {
					customerObj.setIdentityRegistrationNumber(UBase64.base64Encode(item.getNumeroRegistroIdentidadFiscal()));
				}
				
				val postalCode = getPostalCodeEncoded(item);
				customerObj.setPostalCode(postalCode);
				
				val taxRegime = getTaxRegimeEncoded(item);
				customerObj.setTaxRegime(taxRegime);
				
				customers.add(customerObj);
			});
		}
		return customers;
	}
	
	@Override
	public String getPostalCodeEncoded(final ClienteEntity customer) {
		String postalCode = null;
		if (customer.getCodigoPostal() != null) {
			postalCode = UBase64.base64Encode(customer.getCodigoPostal().getCodigo());
		}
		return postalCode;
	}
	
	@Override
	public String getTaxRegimeEncoded(final ClienteEntity customer) {
		String taxRegime = null;
		if (customer.getRegimenFiscal() != null) {
			val taxRegimeCode = customer.getRegimenFiscal().getCodigo();
			val taxRegimeValue = customer.getRegimenFiscal().getValor();
			taxRegime = UBase64.base64Encode(String.format("%s (%s)", taxRegimeCode, taxRegimeValue));
		}
		return taxRegime;
	}

}