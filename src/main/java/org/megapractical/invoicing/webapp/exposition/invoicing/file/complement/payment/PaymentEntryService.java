package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoTramaEntity;

public interface PaymentEntryService {
	
	ComplementoPagoTramaEntity findByName(String entryName);
	
	boolean isPrincipal(String entryName);
	
}