package org.megapractical.invoicing.webapp.exposition.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiPayrollError;
import org.megapractical.invoicing.dal.data.repository.IPayrollEntryJpaRepository;
import org.megapractical.invoicing.webapp.payload.PayrollParserRequest;
import org.megapractical.invoicing.webapp.payload.PayrollParserResponse;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
@Scope("session")
public class PayrollParserTxtService {
	
	@Autowired
	private PayrollParserValidationService payrollParserValidationService;
	
	@Resource
	IPayrollEntryJpaRepository iPayrollEntryJpaRepository;
	
	public PayrollParserResponse parse(PayrollParserRequest request) {
		// Current environment
		val env = UProperties.env();
					
		//##### Contenido final despues de parsear
    	List<String[]> entriesFinally = new ArrayList<>();
    	
    	//##### Indica la línea del fichero
    	Integer line = 0;
    	
    	//##### Indica la cantidad de nominas del fichero
    	Integer singleEntriesTotal = 0;
    	
    	boolean errorSet = false;
    	
    	List<String[]> entriesAux = new ArrayList<>();
		
    	val payrollErrors = new ArrayList<ApiPayrollError>();
    	
    	val payrollParser = request.getPayrollParser();
    	val statistics = request.getStatistics();
    	val entries = request.getEntries();
    	
    	for (int i = 0; i < entries.size(); i ++) {
    		//##### Informacion de la línea actual
    		val item = entries.get(i);
    		
    		//##### Incrementenado línea
			line ++;
			
			errorSet = false;
			val payrollError = new ApiPayrollError();
			
			UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > TXT FILE > READING FILE LINE > " + line);
			
			if (i == 0 && !item[0].equalsIgnoreCase("CNOM")) {
				//##### Agregando el error a la lista de errores
				payrollError.setLine(line);
				payrollError.setDescription("Trama CNOM no presente o existe un error en la codificación del archivo.");
				payrollErrors.add(payrollError);
				
				break;
			}
			
			//##### Obteniendo trama
			val payrollEntry = iPayrollEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(item[0]);	
			if (!UValidator.isNullOrEmpty(payrollEntry)) {
				if (Boolean.TRUE.equals(payrollEntry.isPrincipal())) {
					//##### Aumentando el valor de las tramas principales
					//##### para contabilizar el total de comprobantes del fichero
        			singleEntriesTotal ++;
				}
				
				val isValidEntry = payrollParserValidationService.entriesValidate(item);
				if (!payrollEntry.isPrincipal() && !payrollEntry.getNombre().equalsIgnoreCase("CEND") && !isValidEntry) {
					//##### Indicando que ha ocurrido un error
					errorSet = true;
					
					//##### Agregando el error a la lista de errores
					payrollError.setLine(line);
					payrollError.setEntry(payrollEntry);
					payrollError.setEntryLength(payrollEntry.getLongitud());
					payrollError.setEntryLengthOnFile(item.length);
					payrollError.setDescription("Longitud de trama no válida.");
					payrollErrors.add(payrollError);
					
					//##### Decrementando el total de nominas parseadas
					singleEntriesTotal --;
				} else {
					if (Boolean.TRUE.equals(payrollEntry.isPrincipal())) {
						entriesAux.add(item);
					} else {
						val errors = payrollParserValidationService.entryValidate(item);
						val errorWrapperListAux = new ArrayList<ApiPayrollError>();
						errorWrapperListAux.addAll(errors);
						
						if (!errorWrapperListAux.isEmpty()) {
							//##### Indicando que ha ocurrido un error
							errorSet = true;
							
							//##### Agregando el error a la lista de errores
							errorWrapperListAux.forEach(error -> payrollErrors.add(error));
							
							//##### Decrementando el total de nominas parseadas
							singleEntriesTotal --;
						}
					}
				}
				
				if (!errorSet) {
					if (i >= 1 && !payrollEntry.isPrincipal() && !payrollParserValidationService.isEntryEmpty(item)) {
						entriesAux.add(item);
					}
					
					String[] nextEntry = null;
					try {
						nextEntry = entries.get(i + 1);
					} catch (IndexOutOfBoundsException e) {
						if (item[0].equals("CEND") && singleEntriesTotal >= 1) {
							entriesFinally.addAll(entriesAux);
							statistics.setEntries(entriesFinally);
						} else {
							statistics.setEntries(new ArrayList<>());
						}
						statistics.setSingleEntrie(singleEntriesTotal);
						break;
					}
					
					val nextPayrollEntry = iPayrollEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(nextEntry[0]);
					if (i > 1 && !UValidator.isNullOrEmpty(nextPayrollEntry) && (nextPayrollEntry.isPrincipal() || nextPayrollEntry.getNombre().equalsIgnoreCase("CEND"))) {
						entriesFinally.addAll(entriesAux);
						entriesAux = new ArrayList<>();
						
						if (nextPayrollEntry.getNombre().equalsIgnoreCase("CEND")) {
							statistics.setEntries(entriesFinally);
							statistics.setSingleEntrie(singleEntriesTotal);
							break;
						}
					}
				}

				if (errorSet) {
            		//##### Elimino las tramas cargadas en entriesAux
            		entriesAux = new ArrayList<>();
            		
            		//##### Aumento la posicion de i e incremento la línea del fichero mientras la trama siguiente no sea CNOM o CEND
            		i ++;
        			line ++;
        			
        			String[] itemErr = entries.get(i);
        			while (!itemErr[0].equalsIgnoreCase("CNOM") && !itemErr[0].equalsIgnoreCase("CEND") && line < entries.size()) {
            			i ++;
            			line ++;
            			
            			try {
            				itemErr = entries.get(i + 1);
						} catch (Exception e) {
							line ++;
							e.printStackTrace();
						}
            		}
				}
			}
    	}
    	
    	statistics.getPayrollFileStatistics().setNominas(singleEntriesTotal);
    	statistics.getPayrollFileStatistics().setFechaFinAnalisis(new Date());
    	statistics.getPayrollFileStatistics().setHoraFinAnalisis(new Date());
    	
    	payrollParser.setPayrollDataFile(payrollParserValidationService.getPayrollDataFile());
    	payrollParser.setErrors(payrollErrors);
    	payrollParser.setStatistics(statistics);
    	
    	UPrint.logWithLine(env, "[INFO] PAYROLL PARSER > PARSER FILE FINISHED...");
		
		val payrollParserResponse = new PayrollParserResponse();
		payrollParserResponse.setPayrollParser(payrollParser);
		
    	return payrollParserResponse;
	}
	
}