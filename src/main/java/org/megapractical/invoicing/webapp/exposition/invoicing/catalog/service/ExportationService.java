package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service;

import org.megapractical.invoicing.dal.bean.jpa.ExportacionEntity;

public interface ExportationService {
	
	ExportacionEntity findByCode(String code);
	
	ExportacionEntity findByValue(String value);
	
	String description(ExportacionEntity entity);
	
}