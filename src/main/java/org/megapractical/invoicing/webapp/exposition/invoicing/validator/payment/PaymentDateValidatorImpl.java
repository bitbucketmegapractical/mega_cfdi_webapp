package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentDateValidatorImpl implements PaymentDateValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Override
	public String validate(String paymentDate) {
		if (paymentDate == null || paymentDate.length() != 19) {
			return null;
		}
		
		val paymentDateSplitted = paymentDate.split("T");
		val dateStr = formattedDate(paymentDateSplitted[0]);
		val isValidTime = isValidTime(paymentDateSplitted[1]);
		
		if (UValidator.isNullOrEmpty(dateStr) || !isValidTime) {
			return null;
		}
		return paymentDate;
	}

	@Override
	public String validate(String paymentDate, String field, List<JError> errors) {
		JError error = null;
		
		if (UValidator.isNullOrEmpty(paymentDate)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			if (paymentDate.length() != 19) {
				error = JError.error(field, "ERR1003", userTools.getLocale());
			} else {
				val paymentDateSplitted = paymentDate.split("T");
				val dateStr = formattedDate(paymentDateSplitted[0]);
				val isValidTime = isValidTime(paymentDateSplitted[1]);
				
				if (UValidator.isNullOrEmpty(dateStr) || !isValidTime) {
					error = JError.error(field, "ERR1004", userTools.getLocale());
				}
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return paymentDate;
	}

	private String formattedDate(String dateStr) {
		return UDate.formattedDate(UDate.date(dateStr), "yyyy-MM-dd");
	}
	
	private boolean isValidTime(String timeStr) {
		return UValidator.isValidTime(timeStr);
	}

}