package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.PaymentMethodValidatorResponse;

public interface PaymentMethodValidator {
	
	PaymentMethodValidatorResponse validate(String paymentMethod, String field);
	
}