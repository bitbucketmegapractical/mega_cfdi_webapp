package org.megapractical.invoicing.webapp.exposition.sftp.file;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.tenant.bancomext.sftp.SftpConstants;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Controller
@RequestMapping("/sftp/files")
@Scope("session")
@RequiredArgsConstructor
public class SftpFileController {
	
	private final AppSettings appSettings;
	private final AppLog appLog;
	private final UserTools userTools;
	private final SftpTryFileService sftpTryFileService;
	private final SftpFileDownloadService sftpFileDownloadService;
	private final SessionController sessionController;
	
	// Contribuyente
	private ContribuyenteEntity taxpayer;
	
	@ModelAttribute("processingRequest")
	public String processingRequest() {
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}

	@ModelAttribute("loadingRequest")
	public String loadingRequest() {
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}

	@ModelAttribute("emptyInformation")
	public String emptyInformation() {
		return UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
	}
	
	@PostConstruct
	private void init() {
		val activeRfc = userTools.getContribuyenteActive().getRfcActivo();
		this.taxpayer = userTools.getContribuyenteActive(activeRfc);
	}
	
	@GetMapping("/cfdi")
	public String initSftpCfdiFiles(@ModelAttribute("downloadErr") String downloadErr, Model model) {
		HtmlHelper.functionalitySelected("MyCFDIs", "sftp-file-cfdi-inbox");
		appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
		
		if (!UValidator.isNullOrEmpty(downloadErr)) {
			model.addAttribute("downloadErrMsg", downloadErr);
		}
		
		return "/sftp/SftpCfdiTry";
	}
	
	@GetMapping("/payroll")
	public String initSftpPayrollFiles(@ModelAttribute("downloadErr") String downloadErr, Model model) {
		HtmlHelper.functionalitySelected("Payroll", "sftp-file-payroll-inbox");
		appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
		
		if (!UValidator.isNullOrEmpty(downloadErr)) {
			model.addAttribute("downloadErrMsg", downloadErr);
		}
		
		return "/sftp/SftpPayrollTry";
	}
	
	@PostMapping("/{type}")
	@ResponseBody
	public String sftpFileList(@PathVariable String type, HttpServletRequest request) {
		// Numero de página
		val pageStr = UBase64.base64Decode(request.getParameter("page"));
		
		// Registros por página
		var paginateSize = appSettings.getPaginationSize();
		val paginate = UBase64.base64Decode(request.getParameter("paginateSize"));
		if (!UValidator.isNullOrEmpty(paginate)) {
			paginateSize = UValue.integerValue(paginate);
		}
		
		// Determinando la accion
		val action = UBase64.base64Decode(request.getParameter("action"));
		
		// Search by
		var searchName = UBase64.base64Decode(request.getParameter("searchName"));
		if (UValidator.isNullOrEmpty(searchName)) {
			searchName = "";
		}
		
		// Fecha carga
		var date = UBase64.base64Decode(request.getParameter("searchDate"));

		val sftpFile = sftpTryFileService.loadSftpFiles(this.taxpayer, pageStr, paginateSize, searchName, date, type);
		if (!"async".equalsIgnoreCase(action)) {
			sessionController.sessionUpdate();
		}
		return GsonClient.response(sftpFile);
	}
	
	@GetMapping("/download/{filetype}/{id}/{type}")
	public String fileDownload(@PathVariable(value = "id") Long id,
							   @PathVariable(value = "filetype") String fileType,
							   @PathVariable(value = "type") String type,
							   HttpServletResponse response,
							   RedirectAttributes ra) {
		try {
			val sftp = "cfdi".equals(fileType) ? SftpConstants.CFDI_SFTP : SftpConstants.PAYROLL_SFTP;
			val fileDownload = sftpFileDownloadService.download(id, type, sftp);
			SftpFileDownloadHelper.download(fileDownload, id, response);
		} catch (SftpFileNotFoundException | SftpFileDownloadException e) {
			System.out.println(e.getMessage());
			
			val downloadErrMsg = "No se puede descargar el contenido relacionado al archivo";
			ra.addFlashAttribute("downloadErr", downloadErrMsg);
			return "redirect:/sftp/files/" + fileType;
		}
		return null;
    }
	
}