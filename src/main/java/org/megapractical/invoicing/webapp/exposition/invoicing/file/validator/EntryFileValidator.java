package org.megapractical.invoicing.webapp.exposition.invoicing.file.validator;

import org.megapractical.invoicing.dal.bean.common.EntryBase;
import org.megapractical.invoicing.dal.bean.common.FieldBase;

public interface EntryFileValidator {
	
	boolean validateContent(EntryBase entry, FieldBase field, String value);
	
}