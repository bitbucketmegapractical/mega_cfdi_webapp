package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiCancellationRequest;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class CancellationFileParser {
	@Getter(AccessLevel.NONE)
	private List<ApiCancellationRequest> cancellationRequests;
	@Getter(AccessLevel.NONE)
	private List<String> parserErrors;
	@Getter(AccessLevel.NONE)
	private List<String> recipients;
	
	public List<ApiCancellationRequest> getCancellationRequests() {
		if (cancellationRequests == null) {
			cancellationRequests = new ArrayList<>();
		}
		return cancellationRequests;
	}
	
	public List<String> getParserErrors() {
		if (parserErrors == null) {
			parserErrors = new ArrayList<>();
		}
		return parserErrors;
	}

	public List<String> getRecipients() {
		if (recipients == null) {
			recipients = new ArrayList<>();
		}
		return recipients;
	}
	
}