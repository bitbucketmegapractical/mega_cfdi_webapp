package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.math.BigDecimal;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentChangeTypeValidator {
	
	BigDecimal getChangeType(String changeType, MonedaEntity currency);
	
	BigDecimal getChangeType(String changeType, String field, MonedaEntity currency, List<JError> errors);
	
}