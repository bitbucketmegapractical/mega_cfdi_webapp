package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentPayerAccountValidatorImpl implements PaymentPayerAccountValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Override
	public String getPayerAccount(String payerAccount, FormaPagoEntity paymentWay) {
		if (payerAccount != null && paymentWay != null) {
			val sourceAccountValid = paymentWay.getCuentaOrdenante();
			if (sourceAccountValid.equalsIgnoreCase("no")) {
				return null;
			}
		}
		return payerAccount;
	}

	@Override
	public String getPayerAccount(String payerAccount, String field, FormaPagoEntity paymentWay, List<JError> errors) {
		JError error = null;
		if (payerAccount != null && paymentWay != null) {
			val sourceAccountValid = paymentWay.getCuentaOrdenante();
			if (sourceAccountValid.equalsIgnoreCase("no")) {
				error = JError.error(field, "ERR1006", userTools.getLocale());
			}
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return payerAccount;
	}

}