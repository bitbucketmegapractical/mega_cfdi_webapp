package org.megapractical.invoicing.webapp.exposition.sat.catalog;

import org.megapractical.invoicing.dal.bean.jpa.TipoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UnidadMedidaEntity;
import org.megapractical.invoicing.dal.data.repository.IBankJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiRelationshipTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiUseJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IContractJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IDayTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IDeductionTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IExportationJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IFederalEntityJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IJobRiskJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IKeyProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IMeasurementUnitJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IOtherPaymentTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentFrequencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentMethodJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentWayJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPerceptionTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRegimeTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxRegimeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IVoucherTypeJpaRepository;
import org.megapractical.invoicing.webapp.json.JOption;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Component
@RequiredArgsConstructor
public class SatCatalog {
	
	private final IPaymentMethodJpaRepository iPaymentMethodJpaRepository;
	private final IPaymentWayJpaRepository iPaymentWayJpaRepository;
	private final ICurrencyJpaRepository iCurrencyJpaRepository;
	private final IVoucherTypeJpaRepository iVoucherTypeJpaRepository;
	private final IExportationJpaRepository iExportationJpaRepository;
	private final ITaxRegimeJpaRepository iTaxRegimeJpaRepository;
	private final ITaxTypeJpaRepository iTaxTypeJpaRepository;
	private final ICfdiUseJpaRepository iCfdiUseJpaRepository;
	private final IMeasurementUnitJpaRepository iMeasurementUnitJpaRepository;
	private final IKeyProductServiceJpaRepository iKeyProductServiceJpaRepository;
	private final ICfdiRelationshipTypeJpaRepository iCfdiRelationshipTypeJpaRepository;
	private final IPayrollTypeJpaRepository iPayrollTypeJpaRepository;
	private final IContractJpaRepository iContractJpaRepository;
	private final IDayTypeJpaRepository iDayTypeJpaRepository;
	private final IRegimeTypeJpaRepository iRegimeTypeJpaRepository;
	private final IPaymentFrequencyJpaRepository iPaymentFrequencyJpaRepository;
	private final IJobRiskJpaRepository iJobRiskJpaRepository;
	private final IBankJpaRepository iBankJpaRepository;
	private final IFederalEntityJpaRepository iFederalEntityJpaRepository;
	private final IPerceptionTypeJpaRepository iPerceptionTypeJpaRepository;
	private final IDeductionTypeJpaRepository iDeductionTypeJpaRepository;
	private final IOtherPaymentTypeJpaRepository iOtherPaymentTypeJpaRepository;
	
	private static final CustomSeparator DEFAULT_SEPARATOR = CustomSeparator.SPACE; 
	
	public String getPaymentMethodPrint(String code) {
		return getPaymentMethodPrint(code, DEFAULT_SEPARATOR);
	}

	public String getPaymentMethodPrint(String code, CustomSeparator customSeparator) {
		val paymentMethodData = getPaymentMethod(code, customSeparator);
		if (paymentMethodData != null) {
			return paymentMethodData.getCustomValue();
		}
		return null;
	}

	public JOption getPaymentMethod(String code) {
		return getPaymentMethod(code, DEFAULT_SEPARATOR);
	}

	public JOption getPaymentMethod(String code, CustomSeparator customSeparator) {
		val paymentMethod = iPaymentMethodJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (paymentMethod != null) {
			val customValue = code + getCustomValue(paymentMethod.getValor(), customSeparator);
			return JOption.builder().code(code).value(paymentMethod.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getPaymentWayPrint(String code) {
		return getPaymentWayPrint(code, DEFAULT_SEPARATOR);
	}

	public String getPaymentWayPrint(String code, CustomSeparator customSeparator) {
		val paymentWayData = getPaymentWay(code, customSeparator);
		if (paymentWayData != null) {
			return paymentWayData.getCustomValue();
		}
		return null;
	}

	public JOption getPaymentWay(String code) {
		return getPaymentWay(code, DEFAULT_SEPARATOR);
	}

	public JOption getPaymentWay(String code, CustomSeparator customSeparator) {
		val paymentWay = iPaymentWayJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (paymentWay != null) {
			val customValue = code + getCustomValue(paymentWay.getValor(), customSeparator);
			return JOption.builder().code(code).value(paymentWay.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getCurrencyPrint(String code) {
		return getCurrencyPrint(code, DEFAULT_SEPARATOR);
	}

	public String getCurrencyPrint(String code, CustomSeparator customSeparator) {
		val currencyData = getCurrency(code, customSeparator);
		if (currencyData != null) {
			return currencyData.getCustomValue();
		}
		return null;
	}

	public JOption getCurrency(String code) {
		return getCurrency(code, DEFAULT_SEPARATOR);
	}

	public JOption getCurrency(String code, CustomSeparator customSeparator) {
		val currency = iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (currency != null) {
			val customValue = code + getCustomValue(currency.getValor(), customSeparator);
			return JOption.builder().code(code).value(currency.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getVoucherTypePrint(String code) {
		return getVoucherTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getVoucherTypePrint(String code, CustomSeparator customSeparator) {
		val voucherTypeData = getVoucherType(code, customSeparator);
		if (voucherTypeData != null) {
			return voucherTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getVoucherType(String code) {
		return getVoucherType(code, DEFAULT_SEPARATOR);
	}

	public JOption getVoucherType(String code, CustomSeparator customSeparator) {
		val voucherType = iVoucherTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (voucherType != null) {
			val customValue = code + getCustomValue(voucherType.getValor(), customSeparator);
			return JOption.builder().code(code).value(voucherType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getExportationPrint(String code) {
		return getExportationPrint(code, DEFAULT_SEPARATOR);
	}

	public String getExportationPrint(String code, CustomSeparator customSeparator) {
		val exportationData = getExportation(code, customSeparator);
		if (exportationData != null) {
			return exportationData.getCustomValue();
		}
		return null;
	}

	public JOption getExportation(String code) {
		return getExportation(code, DEFAULT_SEPARATOR);
	}

	public JOption getExportation(String code, CustomSeparator customSeparator) {
		val exportation = iExportationJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (exportation != null) {
			val customValue = code + getCustomValue(exportation.getValor(), customSeparator);
			return JOption.builder().code(code).value(exportation.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getTaxRegimePrint(String code) {
		return getTaxRegimePrint(code, DEFAULT_SEPARATOR);
	}
	
	public String getTaxRegimePrint(String code, CustomSeparator customSeparator) {
		val taxRegimeData = getTaxRegime(code, customSeparator);
		if (taxRegimeData != null) {
			return taxRegimeData.getCustomValue();
		}
		return null;
	}
	
	public JOption getTaxRegime(String code) {
		return getTaxRegime(code, DEFAULT_SEPARATOR);
	}
	
	public JOption getTaxRegime(String code, CustomSeparator customSeparator) {
		val taxRegime = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (taxRegime != null) {
			val customValue = code + getCustomValue(taxRegime.getValor(), customSeparator);
			return JOption.builder().code(code).value(taxRegime.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getTaxTypePrint(String code) {
		return getTaxTypePrint(code, DEFAULT_SEPARATOR);
	}
	
	public String getTaxTypePrint(String code, CustomSeparator customSeparator) {
		val taxTypeData = getTaxType(code, customSeparator);
		if (taxTypeData != null) {
			return taxTypeData.getCustomValue();
		}
		return null;
	}
	
	public JOption getTaxType(String code) {
		return getTaxType(code, DEFAULT_SEPARATOR);
	}
	
	public JOption getTaxType(String code, CustomSeparator customSeparator) {
		val taxType = iTaxTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (taxType != null) {
			val customValue = code + getCustomValue(taxType.getValor(), customSeparator);
			return JOption.builder().code(code).value(taxType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getCfdiUsePrint(String code) {
		return getCfdiUsePrint(code, DEFAULT_SEPARATOR);
	}

	public String getCfdiUsePrint(String code, CustomSeparator customSeparator) {
		val cfdiUseData = getCfdiUse(code, customSeparator);
		if (cfdiUseData != null) {
			return cfdiUseData.getCustomValue();
		}
		return null;
	}

	public JOption getCfdiUse(String code) {
		return getCfdiUse(code, DEFAULT_SEPARATOR);
	}

	public JOption getCfdiUse(String code, CustomSeparator customSeparator) {
		val cfdiUse = iCfdiUseJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (cfdiUse != null) {
			val customValue = code + getCustomValue(cfdiUse.getValor(), customSeparator);
			return JOption.builder().code(code).value(cfdiUse.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getMeasurementUnitValue(String code) {
		val measurementUnit = getMeasurementUnitEntity(code);
		if (measurementUnit != null) {
			return measurementUnit.getValor();
		}
		return null;
	}
	
	public String getMeasurementUnitPrint(String code) {
		return getMeasurementUnitPrint(code, DEFAULT_SEPARATOR);
	}

	public String getMeasurementUnitPrint(String code, CustomSeparator customSeparator) {
		val measurementUnitData = getMeasurementUnit(code, customSeparator);
		if (measurementUnitData != null) {
			return measurementUnitData.getCustomValue();
		}
		return null;
	}

	public JOption getMeasurementUnit(String code) {
		return getMeasurementUnit(code, DEFAULT_SEPARATOR);
	}

	public JOption getMeasurementUnit(String code, CustomSeparator customSeparator) {
		val measurementUnit = getMeasurementUnitEntity(code);
		if (measurementUnit != null) {
			val customValue = code + getCustomValue(measurementUnit.getValor(), customSeparator);
			return JOption.builder().code(code).value(measurementUnit.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	private UnidadMedidaEntity getMeasurementUnitEntity(String code) {
		return iMeasurementUnitJpaRepository.findByCodigoAndEliminadoFalse(code);
	}
	
	public String getKeyProductServicePrint(String code) {
		return getKeyProductServicePrint(code, DEFAULT_SEPARATOR);
	}

	public String getKeyProductServicePrint(String code, CustomSeparator customSeparator) {
		val keyProductServiceData = getKeyProductService(code, customSeparator);
		if (keyProductServiceData != null) {
			return keyProductServiceData.getCustomValue();
		}
		return null;
	}

	public JOption getKeyProductService(String code) {
		return getKeyProductService(code, DEFAULT_SEPARATOR);
	}

	public JOption getKeyProductService(String code, CustomSeparator customSeparator) {
		val keyProductService = iKeyProductServiceJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (keyProductService != null) {
			val customValue = code + getCustomValue(keyProductService.getValor(), customSeparator);
			return JOption.builder().code(code).value(keyProductService.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getRelationshipTypePrint(String code) {
		return getRelationshipTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getRelationshipTypePrint(String code, CustomSeparator customSeparator) {
		val relationshipTypeData = getRelationshipType(code, customSeparator);
		if (relationshipTypeData != null) {
			return relationshipTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getRelationshipType(String code) {
		return getRelationshipType(code, DEFAULT_SEPARATOR);
	}

	public JOption getRelationshipType(String code, CustomSeparator customSeparator) {
		val relationshipType = iCfdiRelationshipTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (relationshipType != null) {
			val customValue = code + getCustomValue(relationshipType.getValor(), customSeparator);
			return JOption.builder().code(code).value(relationshipType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getPayrollTypePrint(String code) {
		return getPayrollTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getPayrollTypePrint(String code, CustomSeparator customSeparator) {
		val payrollTypeData = getPayrollType(code, customSeparator);
		if (payrollTypeData != null) {
			return payrollTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getPayrollType(String code) {
		return getPayrollType(code, DEFAULT_SEPARATOR);
	}

	public JOption getPayrollType(String code, CustomSeparator customSeparator) {
		val payrollType = iPayrollTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (payrollType != null) {
			val customValue = code + getCustomValue(payrollType.getValor(), customSeparator);
			return JOption.builder().code(code).value(payrollType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public TipoNominaEntity getPayrollTypeEntity(String code) {
		return iPayrollTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
	}
	
	public String getContractTypePrint(String code) {
		return getContractTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getContractTypePrint(String code, CustomSeparator customSeparator) {
		val contractTypeData = getContractType(code, customSeparator);
		if (contractTypeData != null) {
			return contractTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getContractType(String code) {
		return getContractType(code, DEFAULT_SEPARATOR);
	}

	public JOption getContractType(String code, CustomSeparator customSeparator) {
		val contractType = iContractJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (contractType != null) {
			val customValue = code + getCustomValue(contractType.getValor(), customSeparator);
			return JOption.builder().code(code).value(contractType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getDayTypePrint(String code) {
		return getDayTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getDayTypePrint(String code, CustomSeparator customSeparator) {
		val dayTypeData = getDayType(code, customSeparator);
		if (dayTypeData != null) {
			return dayTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getDayType(String code) {
		return getDayType(code, DEFAULT_SEPARATOR);
	}

	public JOption getDayType(String code, CustomSeparator customSeparator) {
		val dayType = iDayTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (dayType != null) {
			val customValue = code + getCustomValue(dayType.getValor(), customSeparator);
			return JOption.builder().code(code).value(dayType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getRegimeTypePrint(String code) {
		return getRegimeTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getRegimeTypePrint(String code, CustomSeparator customSeparator) {
		val regimeTypeData = getRegimeType(code, customSeparator);
		if (regimeTypeData != null) {
			return regimeTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getRegimeType(String code) {
		return getRegimeType(code, DEFAULT_SEPARATOR);
	}

	public JOption getRegimeType(String code, CustomSeparator customSeparator) {
		val regimeType = iRegimeTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (regimeType != null) {
			val customValue = code + getCustomValue(regimeType.getValor(), customSeparator);
			return JOption.builder().code(code).value(regimeType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getPaymentFrequencyPrint(String code) {
		return getPaymentFrequencyPrint(code, DEFAULT_SEPARATOR);
	}

	public String getPaymentFrequencyPrint(String code, CustomSeparator customSeparator) {
		val paymentFrequencyData = getPaymentFrequency(code, customSeparator);
		if (paymentFrequencyData != null) {
			return paymentFrequencyData.getCustomValue();
		}
		return null;
	}

	public JOption getPaymentFrequency(String code) {
		return getPaymentFrequency(code, DEFAULT_SEPARATOR);
	}

	public JOption getPaymentFrequency(String code, CustomSeparator customSeparator) {
		val paymentFrequency = iPaymentFrequencyJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (paymentFrequency != null) {
			val customValue = code + getCustomValue(paymentFrequency.getValor(), customSeparator);
			return JOption.builder().code(code).value(paymentFrequency.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getJobRiskPrint(String code) {
		return getJobRiskPrint(code, DEFAULT_SEPARATOR);
	}

	public String getJobRiskPrint(String code, CustomSeparator customSeparator) {
		val jobRiskData = getJobRisk(code, customSeparator);
		if (jobRiskData != null) {
			return jobRiskData.getCustomValue();
		}
		return null;
	}

	public JOption getJobRisk(String code) {
		return getJobRisk(code, DEFAULT_SEPARATOR);
	}

	public JOption getJobRisk(String code, CustomSeparator customSeparator) {
		val jobRisk = iJobRiskJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (jobRisk != null) {
			val customValue = code + getCustomValue(jobRisk.getValor(), customSeparator);
			return JOption.builder().code(code).value(jobRisk.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getBankPrint(String code) {
		return getBankPrint(code, DEFAULT_SEPARATOR);
	}

	public String getBankPrint(String code, CustomSeparator customSeparator) {
		val bankData = getBank(code, customSeparator);
		if (bankData != null) {
			return bankData.getCustomValue();
		}
		return null;
	}

	public JOption getBank(String code) {
		return getBank(code, DEFAULT_SEPARATOR);
	}

	public JOption getBank(String code, CustomSeparator customSeparator) {
		val bank = iBankJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (bank != null) {
			val customValue = code + getCustomValue(bank.getValor(), customSeparator);
			return JOption.builder().code(code).value(bank.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getFederalEntityPrint(String code) {
		return getFederalEntityPrint(code, DEFAULT_SEPARATOR);
	}

	public String getFederalEntityPrint(String code, CustomSeparator customSeparator) {
		val federalEntityData = getFederalEntity(code, customSeparator);
		if (federalEntityData != null) {
			return federalEntityData.getCustomValue();
		}
		return null;
	}

	public JOption getFederalEntity(String code) {
		return getFederalEntity(code, DEFAULT_SEPARATOR);
	}

	public JOption getFederalEntity(String code, CustomSeparator customSeparator) {
		val federalEntity = iFederalEntityJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (federalEntity != null) {
			val customValue = code + getCustomValue(federalEntity.getValor(), customSeparator);
			return JOption.builder().code(code).value(federalEntity.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getPerceptionTypePrint(String code) {
		return getPerceptionTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getPerceptionTypePrint(String code, CustomSeparator customSeparator) {
		val perceptionTypeData = getPerceptionType(code, customSeparator);
		if (perceptionTypeData != null) {
			return perceptionTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getPerceptionType(String code) {
		return getPerceptionType(code, DEFAULT_SEPARATOR);
	}

	public JOption getPerceptionType(String code, CustomSeparator customSeparator) {
		val perceptionType = iPerceptionTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (perceptionType != null) {
			val customValue = code + getCustomValue(perceptionType.getValor(), customSeparator);
			return JOption.builder().code(code).value(perceptionType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getDeductionTypePrint(String code) {
		return getDeductionTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getDeductionTypePrint(String code, CustomSeparator customSeparator) {
		val deductionTypeData = getDeductionType(code, customSeparator);
		if (deductionTypeData != null) {
			return deductionTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getDeductionType(String code) {
		return getDeductionType(code, DEFAULT_SEPARATOR);
	}

	public JOption getDeductionType(String code, CustomSeparator customSeparator) {
		val deductionType = iDeductionTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (deductionType != null) {
			val customValue = code + getCustomValue(deductionType.getValor(), customSeparator);
			return JOption.builder().code(code).value(deductionType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	public String getOtherPaymentTypePrint(String code) {
		return getOtherPaymentTypePrint(code, DEFAULT_SEPARATOR);
	}

	public String getOtherPaymentTypePrint(String code, CustomSeparator customSeparator) {
		val otherPaymentTypeData = getOtherPaymentType(code, customSeparator);
		if (otherPaymentTypeData != null) {
			return otherPaymentTypeData.getCustomValue();
		}
		return null;
	}

	public JOption getOtherPaymentType(String code) {
		return getOtherPaymentType(code, DEFAULT_SEPARATOR);
	}

	public JOption getOtherPaymentType(String code, CustomSeparator customSeparator) {
		val otherPaymentType = iOtherPaymentTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (otherPaymentType != null) {
			val customValue = code + getCustomValue(otherPaymentType.getValor(), customSeparator);
			return JOption.builder().code(code).value(otherPaymentType.getValor()).customValue(customValue).build();
		}
		return null;
	}
	
	private String getCustomValue(String value, CustomSeparator customSeparator) {
		if (CustomSeparator.DASH.equals(customSeparator)) {
			return " - " + value;
		} else if (CustomSeparator.PARENTHESES.equals(customSeparator)) {
			return " (" + value + ")";
		}
		return " " + value;
	}
	
	public enum CustomSeparator {
		SPACE, DASH, PARENTHESES
	}
	
}