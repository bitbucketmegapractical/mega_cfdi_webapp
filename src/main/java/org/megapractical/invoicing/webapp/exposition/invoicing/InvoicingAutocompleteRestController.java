package org.megapractical.invoicing.webapp.exposition.invoicing;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.service.InvoiceAutocompleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.val;

@RestController
@Scope("session")
public class InvoicingAutocompleteRestController {

	@Autowired
	private InvoiceAutocompleteService invoiceAutocompleteService;

	/**
	 * Cargando regimen fiscal
	 * 
	 * @return
	 */
	@PostMapping(value = "/taxRegimeSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> taxRegimeSourceRequest() {
		val source = invoiceAutocompleteService.taxRegimeSource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando uso cfdi
	 * 
	 * @return
	 */
	@PostMapping(value = "/cfdiUseSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> cfdiUseSourceRequest() {
		val source = invoiceAutocompleteService.cfdiUseSource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando residencia fiscal (Pais)
	 * 
	 * @return
	 */
	@PostMapping(value = "/residencySourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> residencySourceRequest() {
		val source = invoiceAutocompleteService.residencySource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando unidad medida
	 * 
	 * @return
	 */
	@PostMapping(value = "/measurementUnitSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> measurementUnitSourceRequest() {
		val source = invoiceAutocompleteService.measurementUnitSource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando clave producto servicio
	 * 
	 * @return
	 */
	@PostMapping(value = "/keyProductServiceSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> keyProductServiceSourceRequest() {
		val source = invoiceAutocompleteService.keyProductServiceSource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando Tipo Comprobante
	 * 
	 * @return
	 */
	@PostMapping(value = "/voucherTypeSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> voucherTypeSourceRequest() {
		val source = invoiceAutocompleteService.voucherTypeSource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando lugar expedicion (codigo postal)
	 * 
	 * @return
	 */
	@PostMapping(value = "/postalCodeSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> postalCodeSourceRequest() {
		val source = invoiceAutocompleteService.expeditionPlaceSource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando forma de pago
	 * 
	 * @return
	 */
	@PostMapping(value = "/paymentWaySourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> paymentWaySourceRequest() {
		val source = invoiceAutocompleteService.paymentWaySource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando metodo de pago
	 * 
	 * @return
	 */
	@PostMapping(value = "/paymentMethodSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> paymentMethodSourceRequest() {
		val source = invoiceAutocompleteService.paymentMethodSource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando moneda
	 * 
	 * @return
	 */
	@PostMapping(value = "/currencySourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> currencySourceRequest() {
		val source = invoiceAutocompleteService.currencySource();
		return ResponseEntity.ok(source);
	}

	/**
	 * Cargando tipo cadena pago (Autocomplete)
	 * 
	 * @return
	 */
	@PostMapping(value = "/stringTypeSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> stringTypeSourceRequest() {
		val source = invoiceAutocompleteService.stringTypeSource();
		return ResponseEntity.ok(source);
	}
	
	/**
	 * Cargando exportacion (Autocomplete)
	 * 
	 * @return
	 */
	@PostMapping(value = "/exportationSourceRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> exportationSourceRequest() {
		val source = invoiceAutocompleteService.exportationSource();
		return ResponseEntity.ok(source);
	}

}