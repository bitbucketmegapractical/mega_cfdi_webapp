package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyServiceImpl implements CurrencyService {
	
	@Autowired
	private ICurrencyJpaRepository iCurrencyJpaRepository;
	
	@Override
	public MonedaEntity findByCode(String code) {
		return iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(code);
	}

	@Override
	public MonedaEntity findByValue(String value) {
		return iCurrencyJpaRepository.findByValorAndEliminadoFalse(value);
	}

}