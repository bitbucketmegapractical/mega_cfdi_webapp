package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentWayValidatorResponse {
	private FormaPagoEntity paymentWay;
	private JError error;
	private boolean hasError;
	
	public PaymentWayValidatorResponse(FormaPagoEntity paymentWay) {
		this.paymentWay = paymentWay;
		this.hasError = Boolean.FALSE;
	}
	
	public PaymentWayValidatorResponse(FormaPagoEntity paymentWay, JError error) {
		this.paymentWay = paymentWay;
		this.error = error;
		this.hasError = error != null;
	}
	
	public PaymentWayValidatorResponse(FormaPagoEntity paymentWay, boolean hasError) {
		this.paymentWay = paymentWay;
		this.hasError = hasError;
	}
	
}