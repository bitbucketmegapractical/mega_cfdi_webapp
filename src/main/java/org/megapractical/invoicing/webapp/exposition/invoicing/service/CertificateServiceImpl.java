package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.springframework.stereotype.Service;

@Service
public class CertificateServiceImpl implements CertificateService {

	private final ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;

	public CertificateServiceImpl(ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository) {
		this.iTaxpayerCertificateJpaRepository = iTaxpayerCertificateJpaRepository;
	}

	@Override
	public ContribuyenteCertificadoEntity findByTaxpayer(ContribuyenteEntity taxpayer) {
		return iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayer);
	}

}