package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentReceiverAccountValidatorImpl implements PaymentReceiverAccountValidator {

	@Autowired
	private UserTools userTools;
	
	@Override
	public String getReceiverAccount(String receiverAccount, FormaPagoEntity paymentWay) {
		if (receiverAccount != null && paymentWay != null) {
			val targetAccountValid = paymentWay.getCuentaBeneficiario();
			if (targetAccountValid.equalsIgnoreCase("no")) {
				return null;
			}
		}
		return receiverAccount;
	}

	@Override
	public String getReceiverAccount(String receiverAccount, String field, FormaPagoEntity paymentWay,
			List<JError> errors) {
		JError error = null;
		if (receiverAccount != null && paymentWay != null) {
			val targetAccountValid = paymentWay.getCuentaBeneficiario();
			if (targetAccountValid.equalsIgnoreCase("no")) {
				error = JError.error(field, "ERR1006", userTools.getLocale());
			}
		}

		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return receiverAccount;
	}

}