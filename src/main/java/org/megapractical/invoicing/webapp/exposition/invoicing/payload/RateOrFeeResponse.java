package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RateOrFeeResponse {
	private String value;
	private boolean change;
}