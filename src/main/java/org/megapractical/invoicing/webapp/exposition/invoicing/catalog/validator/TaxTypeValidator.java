package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CatalogValidatorResponse;

public interface TaxTypeValidator {
	
	CatalogValidatorResponse validate(String taxType, String field, boolean isTransferred);
	
}