package org.megapractical.invoicing.webapp.exposition.invoicing.file.validator;

import org.megapractical.invoicing.api.wrapper.ApiReceiver;
import org.megapractical.invoicing.webapp.json.JReceiver;

public interface ReceiverFileValidator {
	
	ApiReceiver getReceiver(JReceiver receiverData);
	
}