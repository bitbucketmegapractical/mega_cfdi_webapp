package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherExportationResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;

public interface VoucherExportationValidator {
	
	VoucherExportationResponse getExportation(String exportation);
	
	VoucherExportationResponse getExportation(String exportation, String field);
	
	VoucherExportationResponse getExportation(JVoucher voucher, String field, List<JError> errors);
	
}