package org.megapractical.invoicing.webapp.exposition.service.api;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.annotation.PostConstruct;

import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.dal.bean.jpa.ApiToken;
import org.megapractical.invoicing.dal.data.repository.IApiTokenJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

@Service
@RequiredArgsConstructor
public class ApiTokenService {
	
	private final AppSettings appSettings;
	private final RestTemplate restTemplate;
	private final IApiTokenJpaRepository iApiTokenJpaRepository;
	
	private String authServiceUrl;
	private String username;
	private String apiKey;
	
	@PostConstruct
	public void init() {
		this.authServiceUrl = appSettings.getPropertyValue("mega.cfdi.auth.service.url");
		this.username = appSettings.getPropertyValue("mega.cfdi.auth.service.username");
		this.apiKey = appSettings.getPropertyValue("mega.cfdi.auth.service.apikey");
	}
	
	public String getToken() {
		var apiToken = iApiTokenJpaRepository.findFirstByOrderByIdDesc();
		if (apiToken != null) {
			return apiToken.getToken();
		}
		return null;
	}
	
	public void updateToken() {
		val newApiToken = requestToken();
		if (newApiToken != null) {
			var apiToken = iApiTokenJpaRepository.findFirstByOrderByIdDesc();
			if (apiToken == null) {
				apiToken = new ApiToken();
			}
			
			apiToken.setToken(newApiToken.getToken());
			apiToken.setExpireAt(newApiToken.getExpireAt());
			apiToken.setRequestDate(LocalDateTime.now());
			save(apiToken);
		}
	}
	
	private ApiToken save(ApiToken apiToken) {
		if (apiToken != null) {
			return iApiTokenJpaRepository.save(apiToken);
		}
		return null;
	}
	
    private ApiToken requestToken() {
    	try {
    		UPrint.logWithLine(UProperties.env(), "[INFO] API TOKEN SERVICE > REQUEST FOR " + authServiceUrl);
        	
            // Create the request body
    		val requestBody = "{\"username\":\"" + username + "\",\"apiKey\":\"" + apiKey + "\"}";

            // Set the headers for the request
            val headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            // Create the HTTP entity with the request body and headers
            val entity = new HttpEntity<>(requestBody, headers);

            // Make the request to the API
            val response = restTemplate.postForEntity(authServiceUrl, entity, TokenResponse.class);
            return convertToApiToken(response.getBody());
		} catch (RestClientException e) {
			UPrint.logWithLine(UProperties.env(), "[INFO] API TOKEN SERVICE > ERROR: UNABLE TO ESTABLISH CONNECTION");
		}
    	return null;
    }
    
    private ApiToken convertToApiToken(TokenResponse tokenResponse) {
    	if (tokenResponse != null) {
    		UPrint.logWithLine(UProperties.env(), "[INFO] API TOKEN SERVICE > TOKEN OBTAINED SUCCESSFULLY");
    		
    		val expireAt = LocalDateTime.ofInstant(tokenResponse.getExpireAt(), ZoneId.systemDefault());
    		return new ApiToken(tokenResponse.getAccessToken(), expireAt);
    	}
    	UPrint.logWithLine(UProperties.env(), "[INFO] API TOKEN SERVICE > ERROR: UNABLE TO OBTAIN TOKEN");
    	return null;
    }
    
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    private static class TokenResponse  {
    	private String accessToken;
    	private Instant expireAt;
    }
	
}