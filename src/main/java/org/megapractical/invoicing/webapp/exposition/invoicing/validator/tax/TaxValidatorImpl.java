package org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Pattern;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.FactorTypeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.TaxTypeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.AmountValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.BaseValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.RateOrFeeValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.TaxValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JRateOrFee;
import org.megapractical.invoicing.webapp.json.JRateOrFee.RateOrFee;
import org.megapractical.invoicing.webapp.json.JTaxes;
import org.megapractical.invoicing.webapp.json.JTaxes.JTax;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class TaxValidatorImpl implements TaxValidator {

	@Autowired
	private UserTools userTools;

	@Autowired
	private TaxTypeValidator taxTypeValidator;

	@Autowired
	private FactorTypeValidator factorTypeValidator;

	private static final String INVALID_FORMAT_ERROR_CODE = "ERR1003";
	private static final String EXEMPT = "exento";
	private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;

	@Override
	public BaseValidatorResponse validateBase(String base, String field, MonedaEntity currency, boolean isTransferred) {
		JError error = null;

		val baseBigDecimal = UValue.bigDecimalStrict(base);
		if (UValidator.isNullOrEmpty(baseBigDecimal)) {
			error = JError.required(field, userTools.getLocale());
		} else if (!UValidator.isDouble(baseBigDecimal)) {
			error = JError.error(field, INVALID_FORMAT_ERROR_CODE, userTools.getLocale());
		} else {
			// [CFDI33154, CFDI33163]
			String zeroValue = null;
			val zeroBd = UValue.bigDecimal(zeroValue, currency.getDecimales());
			if (baseBigDecimal.compareTo(zeroBd) != 1) {
				val errorCode = isTransferred ? "CFDI33154" : "CFDI33163";
				error = JError.errorSource(field, errorCode, userTools.getLocale());
			}
		}

		return new BaseValidatorResponse(baseBigDecimal, error);
	}

	@Override
	public JError validateTaxType(String taxType, String field, boolean isTransferred) {
		val validateCatalogResponse = taxTypeValidator.validate(taxType, field, isTransferred);
		return validateCatalogResponse.getError();
	}

	@Override
	public JError validateFactorType(String factorType, 
									 String rateOrFee, 
									 String amount, 
									 String field,
									 boolean isAutomaticCalculation,
									 boolean isTransferred) {
		val validateCatalogResponse = factorTypeValidator.validate(factorType, rateOrFee, isTransferred);
		var error = validateCatalogResponse.getError();
		if (error == null) {
			// [CFDI33157, CFDI33158, CFDI33166]
			if (factorType.equalsIgnoreCase(EXEMPT)) {
				error = validateFactorTypeExempt(rateOrFee, amount, isAutomaticCalculation, isTransferred, field);
			} else {
				error = validateFactorType(rateOrFee, amount, isAutomaticCalculation, isTransferred, field);
			}
		}
		return error;
	}

	@Override
	public RateOrFeeValidatorResponse validateRateOrFee(JRateOrFee rateOrFeeJson, 
														String rateOrFee, 
														String factorType,
														String taxType, 
														String field, 
														boolean isTransferred) {
		JError error = null;

		val rateOrFeeBigDecimal = UValue.bigDecimalStrict(rateOrFee);
		val rateOrFeeBigDecimalWithSixDecimals = UValue.bigDecimalWithSixDecimalsStrict(rateOrFee);

		if (!factorType.equalsIgnoreCase(EXEMPT)) {
			if (!UValidator.isDouble(rateOrFeeBigDecimal) || !UValidator.isDouble(rateOrFeeBigDecimalWithSixDecimals)) {
				error = JError.error(field, INVALID_FORMAT_ERROR_CODE, userTools.getLocale());
			} else if (!rateOrFeeBigDecimal.equals(rateOrFeeBigDecimalWithSixDecimals)) {
				error = JError.error(field, INVALID_FORMAT_ERROR_CODE, userTools.getLocale());
			} else {
				error = validateRateOrFee(rateOrFeeJson, rateOrFee, factorType, taxType, isTransferred,
						rateOrFeeBigDecimalWithSixDecimals);
			}
		}
		return new RateOrFeeValidatorResponse(rateOrFeeBigDecimal, error);
	}

	@Override
	public AmountValidatorResponse validateAmount(BigDecimal base, 
												  BigDecimal rateOrFee, 
												  String factorType,
												  String amount, 
												  String field, 
												  boolean isTransferred, 
												  boolean isAutomaticCalculation, 
												  MonedaEntity currency) {
		JTax tax = null;
		JError error = null;

		if (!isAutomaticCalculation) {
			val amountBigDecimal = UValue.bigDecimalStrict(amount);
			if (!factorType.equalsIgnoreCase(EXEMPT)) {
				error = validateAmount(base, rateOrFee, amount, isTransferred, currency, field, amountBigDecimal);
			}
		} else {
			if (!factorType.equalsIgnoreCase(EXEMPT)) {
				var roundedValueCalculated = base.multiply(rateOrFee);
				roundedValueCalculated = roundedValueCalculated.setScale(currency.getDecimales(), ROUNDING_MODE);

				val amountStr = UValue.bigDecimalString(roundedValueCalculated);
				val amountSplitted = amountStr.split(Pattern.quote("."));
				val amountInteger = Integer.valueOf(amountSplitted[0]);

				if (amountInteger < 0) {
					val errorCode = isTransferred ? "CFDI33161" : "CFDI33169";
					error = JError.errorSource(field, errorCode, userTools.getLocale());
				} else {
					tax = new JTax(UBase64.base64Encode(amountStr));
				}
			}
		}

		return new AmountValidatorResponse(tax, error);
	}

	@Override
	public TaxValidatorResponse validateTax(JTaxes taxes, String factorType, String taxType, BigDecimal rateOrFee,
			boolean isTransferred) {
		JNotification notification = null;
		JError error = null;
		for (val item : taxes.getTaxes()) {
			val itemFactor = UBase64.base64Decode(item.getFactorType());
			val itemTax = UBase64.base64Decode(item.getTax());
			if (itemFactor.equals(factorType) && itemTax.equals(taxType)
					&& (UValue.bigDecimalStrict(UBase64.base64Decode(item.getRateOrFee())).compareTo(rateOrFee) == 0)) {
				var message = UProperties.getMessage("ERR0047", userTools.getLocale());
				val value = isTransferred ? "un traslado" : "una retención";
				val messageSplitted = message.split(Pattern.quote("{0}"));
				message = messageSplitted[0].concat(value).concat(messageSplitted[1]);
				error = JError.builder().message(message).build();
				notification = JNotification.modalPanelMessageError(error.getMessage());
				break;
			}
		}

		return new TaxValidatorResponse(notification, error);
	}

	private JError validateFactorTypeExempt(String rateOrFee, String amount, boolean isAutomaticCalculation,
			boolean isTransferred, final String field) {
		JError error = null;
		if (isTransferred) {
			if (!isAutomaticCalculation) {
				if (!UValidator.isNullOrEmpty(rateOrFee) || !UValidator.isNullOrEmpty(amount)) {
					error = JError.errorSource(field, "CFDI33157", userTools.getLocale());
				}
			} else {
				if (!UValidator.isNullOrEmpty(rateOrFee)) {
					error = JError.errorSource(field, "CFDI33157", userTools.getLocale());
				}
			}
		} else {
			error = JError.errorSource(field, "CFDI33166", userTools.getLocale());
		}
		return error;
	}

	private JError validateFactorType(String rateOrFee, String amount, boolean isAutomaticCalculation,
			boolean isTransferred, final String field) {
		JError error = null;
		if (isTransferred) {
			if (!isAutomaticCalculation) {
				if (UValidator.isNullOrEmpty(rateOrFee) || UValidator.isNullOrEmpty(amount)) {
					error = JError.errorSource(field, "CFDI33158", userTools.getLocale());
				}
			} else {
				if (UValidator.isNullOrEmpty(rateOrFee)) {
					error = JError.errorSource(field, "CFDI33158", userTools.getLocale());
				}
			}
		}
		return error;
	}

	private JError validateRateOrFee(JRateOrFee rateOrFeeJson, String rateOrFee, String factorType, String taxType,
			boolean isTransferred, final BigDecimal rateOrFeeBigDecimalWithSixDecimals) {
		JError error = null;
		val field = "tax-transferred-withheld-rate-fee";

		// [CFDI33159, CFDI33167]
		var finded = false;
		for (val item : rateOrFeeJson.getRateOrFees()) {
			if (item.getTaxType().equals(taxType) && item.getFactorType().equals(factorType)
					&& (isTransferred ? item.isTransferred() : item.isWithheld())) {
				if (item.getRangeType().equalsIgnoreCase("fijo")) {
					finded = getRangeType(rateOrFee, item);
				} else if (item.getRangeType().equalsIgnoreCase("rango")) {
					finded = getRangeType(item, rateOrFeeBigDecimalWithSixDecimals);
				}
			}

			if (finded) {
				break;
			}
		}

		if (!finded) {
			val errorCode = isTransferred ? "CFDI33159" : "CFDI33167";
			error = JError.errorSource(field, errorCode, userTools.getLocale());
		}
		return error;
	}

	private boolean getRangeType(String rateOrFee, RateOrFee item) {
		val expectedValue = item.getExpectedValues().stream().filter(value -> value.equals(rateOrFee)).findFirst()
				.orElse(null);
		return expectedValue != null;
	}

	private boolean getRangeType(RateOrFee item, BigDecimal rateOrFeeBigDecimalWithSixDecimals) {
		val minValue = UValue.bigDecimalWithSixDecimalsStrict(item.getMinValue());
		val maxValue = UValue.bigDecimalWithSixDecimalsStrict(item.getMaxValue());
		return rateOrFeeBigDecimalWithSixDecimals.compareTo(minValue) != -1
				&& rateOrFeeBigDecimalWithSixDecimals.compareTo(maxValue) != 1;
	}

	private JError validateAmount(BigDecimal base, BigDecimal rateOrFee, String amount, boolean isTransferred,
			MonedaEntity currency, final String field, final BigDecimal amountBigDecimal) {
		JError error = null;
		if (UValidator.isNullOrEmpty(amount)) {
			error = JError.required(field, userTools.getLocale());
		} else if (!UValidator.isDouble(amountBigDecimal)) {
			error = JError.error(field, INVALID_FORMAT_ERROR_CODE, userTools.getLocale());
		} else {
			// [CFDI33161, CFDI33169]
			val amountSplitted = amount.split(Pattern.quote("."));
			val amountInteger = Integer.valueOf(amountSplitted[0]);

			var roundedValue = UValue.bigDecimalStrict(amount, currency.getDecimales());
			roundedValue = roundedValue.setScale(currency.getDecimales(), ROUNDING_MODE);

			var roundedValueCalculated = base.multiply(rateOrFee);
			roundedValueCalculated = roundedValueCalculated.setScale(currency.getDecimales(), ROUNDING_MODE);

			if (amountInteger < 0 || !roundedValue.equals(roundedValueCalculated)) {
				val errorCode = isTransferred ? "CFDI33161" : "CFDI33169";
				error = JError.errorSource(field, errorCode, userTools.getLocale());
			}
		}
		return error;
	}

}