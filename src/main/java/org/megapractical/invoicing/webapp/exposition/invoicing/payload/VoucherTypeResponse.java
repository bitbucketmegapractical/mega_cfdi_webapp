package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoucherTypeResponse {
	private TipoComprobanteEntity voucherType;
	private boolean error;
}