package org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload;

import java.math.BigDecimal;

import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RateOrFeeValidatorResponse {
	private BigDecimal rateOrFee;
	private JError error;
}