package org.megapractical.invoicing.webapp.exposition.sftp.file;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JSftpFile {
	private List<SftpFile> sftpFiles;
	private JResponse response;
	private JPagination<?> pagination;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class SftpFile {
		private String id;
		private String name;
		private String area;
		private String uploadDate;
		private String fileStatus;
		private String sftpPath;
		private String errorValidationPath;
		private String errorContentPath;
		private String errorS3Path;
	}
	
}