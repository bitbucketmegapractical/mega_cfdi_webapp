package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.dal.bean.jpa.MetodoPagoEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentMethodValidatorResponse {
	private MetodoPagoEntity paymentMethod;
	private JError error;
	private boolean hasError;
	
	public PaymentMethodValidatorResponse(MetodoPagoEntity paymentMethod) {
		this.paymentMethod = paymentMethod;
		this.hasError = Boolean.FALSE;
	}
	
	public PaymentMethodValidatorResponse(MetodoPagoEntity paymentMethod, JError error) {
		this.paymentMethod = paymentMethod;
		this.error = error;
		this.hasError = error != null;
	}
	
	public PaymentMethodValidatorResponse(MetodoPagoEntity paymentMethod, boolean hasError) {
		this.paymentMethod = paymentMethod;
		this.hasError = hasError;
	}
	
}