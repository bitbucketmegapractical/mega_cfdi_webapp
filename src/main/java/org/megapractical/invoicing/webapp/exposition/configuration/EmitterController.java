package org.megapractical.invoicing.webapp.exposition.configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.Contribuyente;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.megapractical.invoicing.dal.business.service.mapping.ContribuyenteServiceMapper;
import org.megapractical.invoicing.dal.data.repository.IPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxRegimeJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.json.JEmitter;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.model.TaxpayerViewModel;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.EmitterTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

@Controller
@Scope("session")
public class EmitterController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	EmitterTools emitterTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Resource
	private ITaxRegimeJpaRepository iTaxRegimeJpaRepository;
	
	@Resource
	private IPersonTypeJpaRepository iPersonTypeJpaRepository;
	
	@Resource
	private ContribuyenteServiceMapper contribuyenteServiceMapper;
	
	@ModelAttribute("processingRequest")
	public String processingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}	
	
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	
	// Cargando Tipo contribuyente (Tipo persona)
	public List<TipoPersonaEntity> personTypeSource(){
		try {
			
			return iPersonTypeJpaRepository.findByEliminadoFalse();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private TaxpayerViewModel taxpayerViewModel;
	
	@RequestMapping(value = "/emitterTaxRegimeConfiguration/{action}", method = RequestMethod.GET)
	public String init(Model model, @PathVariable(value="action") String action) {
		try {
			
			ContribuyenteEntity contribuyenteEntity = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			Contribuyente taxpayer = contribuyenteServiceMapper.mapContribuyenteEntityToContribuyente(contribuyenteEntity);
			taxpayerViewModel = new TaxpayerViewModel(taxpayer);
			taxpayerViewModel.setAction(action);
			
			Boolean isTaxInformationAvailable = action.equals("config") ? emitterTools.isTaxInformationAvailable() : Boolean.FALSE;
			if(isTaxInformationAvailable || action.equals("update")){
				RegimenFiscalEntity taxRegime = emitterTools.getTaxRegime().getRegimenFiscal();
				taxpayerViewModel.setTaxRegime(taxRegime.getCodigo().concat(" (").concat(taxRegime.getValor()).concat(")"));
				
				TipoPersonaEntity personType = emitterTools.getPersonType().getTipoPersona();
				taxpayerViewModel.setEmitterType(personType.getValor());
				model.addAttribute("emitterType", personType.getCodigo());
				
				ContribuyenteCertificadoEntity certificate = emitterTools.getCertificate();
				if(!UValidator.isNullOrEmpty(certificate)){
					File certificateFile = new File(certificate.getRutaCertificado());
					taxpayerViewModel.setCertificateNumber(UCertificateX509.certificateSerialNumber(UFile.fileToByte(certificateFile)));
					taxpayerViewModel.setCertificateCreateDate(UDate.formattedDate(UCertificateX509.certificateCreationDate(certificateFile), DATE_FORMAT));
					taxpayerViewModel.setCertificateExpiredDate(UDate.formattedDate(UCertificateX509.certificateExpirationDate(certificateFile), DATE_FORMAT));
				}
			}
			
			model.addAttribute("isTaxInformationAvailable", isTaxInformationAvailable);
			model.addAttribute("personTypeSource", personTypeSource());
			model.addAttribute("taxpayerViewModel", taxpayerViewModel);
			
			HtmlHelper.functionalitySelected("EmitterConfig", "emitter-config-taxregime");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			return "/config/EmitterTaxRegime";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/emitterTaxInformation", method = RequestMethod.POST)
	public @ResponseBody String emitterTaxRegimeConfiguration(HttpServletRequest request, @RequestParam(value="certificate") MultipartFile multipartFileCertificate,
			@RequestParam(value="privateKey") MultipartFile multipartFilePrivateKey){
		
		JEmitter emitter = new JEmitter();  
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		List<JError> errors = new ArrayList<>();
		Toastr toastr = new Toastr();
		
		try {
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			
			Decoder decoder = Base64.getDecoder();
			Encoder encoder = Base64.getEncoder();
			
			String rfc = request.getParameter("rfc");
			byte[] rfcDecode = decoder.decode(rfc);
			rfc = new String(rfcDecode);
			JError jsonErrorRfc = new JError();
			if(UValidator.isNullOrEmpty(rfc)){
				jsonErrorRfc.setField("rfcActivo");
				jsonErrorRfc.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(jsonErrorRfc);
			}else if(!rfc.equals(rfcActive)){
				jsonErrorRfc.setField("rfcActivo");
				jsonErrorRfc.setMessage(UProperties.getMessage("ERR0001", userTools.getLocale()));
				errors.add(jsonErrorRfc);
			}
			
			String emitterType = request.getParameter("emitterType");
			byte[] emitterTypeDecode = decoder.decode(emitterType);
			emitterType = new String(emitterTypeDecode);
			TipoPersonaEntity emitterTypeEntity = null;
			JError jsonErrorEmitterType = new JError();
			if(UValidator.isNullOrEmpty(emitterType)){
				jsonErrorEmitterType.setField("emitterType");
				jsonErrorEmitterType.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(jsonErrorEmitterType);
			}else{
				emitterTypeEntity = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(emitterType);
				if(emitterTypeEntity == null){
					jsonErrorEmitterType.setField("emitterType");
					jsonErrorEmitterType.setMessage(UProperties.getMessage("ERR0002", userTools.getLocale()));
					errors.add(jsonErrorEmitterType);
				}
			}
			
			String taxRegime = request.getParameter("taxRegime");
			byte[] taxRegimeDecode = decoder.decode(taxRegime);
			taxRegime = new String(taxRegimeDecode);
			RegimenFiscalEntity regimenFiscalEntityByCode = null;
			RegimenFiscalEntity regimenFiscalEntityByValue = null;
			JError jsonErrorTaxRegime = new JError();
			if(UValidator.isNullOrEmpty(taxRegime)){
				jsonErrorTaxRegime.setField("taxRegime");
				jsonErrorTaxRegime.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(jsonErrorTaxRegime);
			}else{
				//##### Regimen fiscal :: Validando que sea un valor de catalogo
				String[] regimenFiscalCatalog = UCatalog.getCatalog(taxRegime);
				taxRegime = regimenFiscalCatalog[0];
				String regimenFiscalValue = regimenFiscalCatalog[1];
				
				regimenFiscalEntityByCode = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalse(taxRegime);
				regimenFiscalEntityByValue = iTaxRegimeJpaRepository.findByValorAndEliminadoFalse(regimenFiscalValue);
				
				if(regimenFiscalEntityByCode == null || regimenFiscalEntityByValue == null){
					String[] values = new String[]{"CFDI33130", "RegimenFiscal", "c_RegimenFiscal"};
					jsonErrorTaxRegime.setField("taxRegime");
					jsonErrorTaxRegime.setMessage(UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values));
					errors.add(jsonErrorTaxRegime);
				}
			}
			
			//##### Certificado
			File certificate = null;
			JError jsonErrorCertificate = new JError();
			if (multipartFileCertificate.isEmpty()) {
				jsonErrorCertificate.setField("certificate");
				jsonErrorCertificate.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(jsonErrorCertificate);
			}else{
				String certificateName = multipartFileCertificate.getOriginalFilename();
				String certificateExt = UFile.getExtension(certificateName);
				if(certificateExt == null || !certificateExt.equalsIgnoreCase("cer")){
					jsonErrorCertificate.setField("certificate");
					jsonErrorCertificate.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
					errors.add(jsonErrorCertificate);
				}else{
					certificate = UFile.multipartFileToFile(multipartFileCertificate);
				}
				//certificate.getBytes();
			}
			
			//##### Llave privada
			File privateKey = null;
			JError jsonErrorPrivateKey = new JError();
			if (multipartFilePrivateKey.isEmpty()) {
				jsonErrorPrivateKey.setField("privateKey");
				jsonErrorPrivateKey.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(jsonErrorPrivateKey);
			}else{
				String privateKeyName = multipartFilePrivateKey.getOriginalFilename();
				String privateKeyExt = UFile.getExtension(privateKeyName);
				if(privateKeyExt == null || !privateKeyExt.equalsIgnoreCase("key")){
					jsonErrorPrivateKey.setField("privateKey");
					jsonErrorPrivateKey.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
					errors.add(jsonErrorPrivateKey);
				}else{
					privateKey = UFile.multipartFileToFile(multipartFilePrivateKey);
				}
				//privateKey.getBytes();
			}
			
			//##### Contraseña de la clave privada
			String passwd = request.getParameter("passwd");
			byte[] passwdDecode = decoder.decode(passwd);
			passwd = new String(passwdDecode);
			JError jsonErrorPasswd = new JError();
			if(UValidator.isNullOrEmpty(passwd)){
				jsonErrorPasswd.setField("passwd");
				jsonErrorPasswd.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(jsonErrorPasswd);
			}
			
			JError jsonErrorX509 = new JError();
			String certificateValidate = UCertificateX509.certificateValidate(certificate, privateKey, passwd);
			Date certificateCreationDate = null;
			Date certificateExpirationDate = null;
			if(!certificateValidate.equals("VALIDATE_SUCCESSFULY")){
				String[] keyPropertie = new String[]{""};
				if(certificateValidate.equals("CERTIFICATE_AND_PRIVATE_KEY_DOES_NOT_CORRESPOND")){
					keyPropertie = new String[]{"certificate", "ERR0005"};
				}else if(certificateValidate.equals("BAD_PADDING_EXCEPTION")){
					keyPropertie = new String[]{"passwd", "ERR0006"};
				}else if(certificateValidate.equals("GENERAL_SECURITY_EXCEPTION")){
					keyPropertie = new String[]{"certificate", "ERR0003"};
				}
				if(!keyPropertie[0].isEmpty()){
					jsonErrorX509.setField(keyPropertie[0]);
					jsonErrorX509.setMessage(UProperties.getMessage(keyPropertie[1], userTools.getLocale()));
					errors.add(jsonErrorX509);
				}else{
					/*String messageClassErr = "alert alert-danger alert-dismissable";
					model.addAttribute("message", UProperties.getMessage("ERR0007", userTools.getLocale()));
					model.addAttribute("messageClass", messageClassErr);
					model.addAttribute("billingFreeViewModel", billingFreeViewModel);
					return "/invoicing/CfdiFreeEmitter";*/
				}
			}else{
				//##### Validando que el certificado no sea fiel
				if(UCertificateX509.isFiel(certificate)){
					// Es fiel
					jsonErrorX509.setField("certificate");
					jsonErrorX509.setMessage(UProperties.getMessage("ERR0003", userTools.getLocale()));
					errors.add(jsonErrorX509);
				}else{
					certificateCreationDate = UCertificateX509.certificateCreationDate(certificate);
					certificateExpirationDate = UCertificateX509.certificateExpirationDate(certificate);
					Date today = new Date();
					if(UDateTime.isBeforeLocalDate(certificateExpirationDate, today)){
						jsonErrorX509.setField("certificate");
						jsonErrorX509.setMessage(UProperties.getMessage("ERR0004", userTools.getLocale()));
						errors.add(jsonErrorX509);
					}
				}
			}
			
			Gson gson = new Gson();
			String jsonInString = null;
			
			if(!errors.isEmpty()){
				response.setError(Boolean.TRUE);
				response.setErrors(errors);
			}else{
				try {
					
					String emitterPath = appSettings.getPropertyValue("cfdi.cert.path");
					String filePath = emitterPath + File.separator + rfc;
					File fileFolder = new File(filePath); 
					if(!fileFolder.exists()){
						fileFolder.mkdirs();
					}
					
					String uuid = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
					
					// Certificado
					String cerFileName = uuid + ".cer";
					String cerPath = filePath + File.separator + cerFileName;
					UFile.writeFile(UFile.fileToByte(certificate), cerPath);
					
					// Llave privada
					String privateKeyFileName = uuid + ".key";
					String privateKeyPath = filePath + File.separator + privateKeyFileName;
					UFile.writeFile(UFile.fileToByte(privateKey), privateKeyPath);
					
					byte[] passwdEncode = encoder.encode(passwd.getBytes());
					passwd = new String(passwdEncode);
					
					ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfc);
					
					String certificateNumber = UCertificateX509.certificateSerialNumber(UFile.fileToByte(certificate));
					
					List<OperationCode> operationCodeSource = new ArrayList<>();
					EventTypeCode eventTypeCode = null;
					
					if(taxpayerViewModel.getAction().equals("config")){
						//##### Registrando informacion del cer, key y passwd 
						ContribuyenteCertificadoEntity certificadoEntity = new ContribuyenteCertificadoEntity();
						certificadoEntity.setContribuyente(taxpayer);
						certificadoEntity.setClavePrivada(passwd);
						certificadoEntity.setFechaCreacion(certificateCreationDate);
						certificadoEntity.setFechaExpiracion(certificateExpirationDate);
						certificadoEntity.setHabilitado(true);
						certificadoEntity.setNumero(certificateNumber);						
						certificadoEntity.setCertificado(UFile.fileToByte(certificate));
						certificadoEntity.setRutaCertificado(cerPath);
						certificadoEntity.setLlavePrivada(UFile.fileToByte(privateKey));
						certificadoEntity.setRutaLlavePrivada(privateKeyPath);
						persistenceDAO.persist(certificadoEntity);
						
						//##### Registrando Contribuyente - Regimen Fiscal
						ContribuyenteRegimenFiscalEntity contribuyenteRegimenFiscalEntity = new ContribuyenteRegimenFiscalEntity();
						contribuyenteRegimenFiscalEntity.setContribuyente(taxpayer);
						contribuyenteRegimenFiscalEntity.setRegimenFiscal(regimenFiscalEntityByCode);
						persistenceDAO.persist(contribuyenteRegimenFiscalEntity);
						
						//##### Registrando Contribuyente - Tipo persona
						Boolean personTypeUpdate = false;
						ContribuyenteTipoPersonaEntity contribuyenteTipoPersonaEntity = emitterTools.getPersonType();
						if(UValidator.isNullOrEmpty(contribuyenteTipoPersonaEntity)){
							contribuyenteTipoPersonaEntity = new ContribuyenteTipoPersonaEntity();
						}else{
							personTypeUpdate = true;
						}
							
						contribuyenteTipoPersonaEntity.setContribuyente(taxpayer);
						contribuyenteTipoPersonaEntity.setTipoPersona(emitterTypeEntity);
						
						if(!personTypeUpdate){
							persistenceDAO.persist(contribuyenteTipoPersonaEntity);
							operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_MASTER_TIPO_PERSONA_ID24);
						}else{
							persistenceDAO.update(contribuyenteTipoPersonaEntity);
							operationCodeSource.add(OperationCode.ACTUALIZACION_CONTRIBUYENTE_MASTER_TIPO_PERSONA_ID27);
						}
						
						//##### Bitacora
						operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_MASTER_REGIMEN_FISCAL_ID23);
						operationCodeSource.add(OperationCode.REGISTRO_CONTRIBUYENTE_MASTER_CERTIFICADO_ID25);
						
						//##### Bitacora de suceso
						eventTypeCode = EventTypeCode.CONFIGURACION_INFORMACION_FISCAL_ID11;
					}else if(taxpayerViewModel.getAction().equals("update")){
						ContribuyenteCertificadoEntity certificadoEntity = emitterTools.getCertificate();
						
						if(!UValidator.isNullOrEmpty(certificadoEntity)){
							certificadoEntity.setHabilitado(Boolean.FALSE);
							persistenceDAO.update(certificadoEntity);
						}
						
						certificadoEntity = new ContribuyenteCertificadoEntity();
						certificadoEntity.setContribuyente(taxpayer);
						certificadoEntity.setClavePrivada(passwd);
						certificadoEntity.setFechaCreacion(certificateCreationDate);
						certificadoEntity.setFechaExpiracion(certificateExpirationDate);
						certificadoEntity.setHabilitado(true);
						certificadoEntity.setNumero(certificateNumber);
						certificadoEntity.setCertificado(UFile.fileToByte(certificate));
						certificadoEntity.setRutaCertificado(cerPath);
						certificadoEntity.setLlavePrivada(UFile.fileToByte(privateKey));
						certificadoEntity.setRutaLlavePrivada(privateKeyPath);
						persistenceDAO.persist(certificadoEntity);
						
						//##### Actualizando Contribuyente - Regimen Fiscal
						ContribuyenteRegimenFiscalEntity contribuyenteRegimenFiscalEntity = emitterTools.getTaxRegime();
						contribuyenteRegimenFiscalEntity.setContribuyente(taxpayer);
						contribuyenteRegimenFiscalEntity.setRegimenFiscal(regimenFiscalEntityByCode);
						persistenceDAO.update(contribuyenteRegimenFiscalEntity);
						
						//##### Actualizando Contribuyente - Tipo persona
						ContribuyenteTipoPersonaEntity contribuyenteTipoPersonaEntity = emitterTools.getPersonType();
						contribuyenteTipoPersonaEntity.setContribuyente(taxpayer);
						contribuyenteTipoPersonaEntity.setTipoPersona(emitterTypeEntity);
						persistenceDAO.update(contribuyenteTipoPersonaEntity);
						
						//##### Bitacora
						operationCodeSource.add(OperationCode.ACTUALIZACION_CONTRIBUYENTE_MASTER_REGIMEN_FISCAL_ID26);
						operationCodeSource.add(OperationCode.ACTUALIZACION_CONTRIBUYENTE_MASTER_TIPO_PERSONA_ID27);
						operationCodeSource.add(OperationCode.ACTUALIZACION_CONTRIBUYENTE_MASTER_CERTIFICADO_ID28);
						
						//##### Bitacora de suceso
						eventTypeCode = EventTypeCode.ACTUALIZACION_INFORMACION_FISCAL_ID12;
					}
					
					//##### Registrando bitacora
					appLog.logOperation(operationCodeSource);
					
					//##### Registrando bitacora de suceso
					timelineLog.timelineLog(eventTypeCode);
					
					String toastrMessage = "Información ".concat(taxpayerViewModel.getAction().equals("config") ? "registrada satisfactoriamente." : "actualizada satisfactoriamente."); 
					
					// Notificacion Toastr
					toastr.setTitle("Información fiscal");
					toastr.setMessage(toastrMessage);
					toastr.setType(Toastr.ToastrType.SUCCESS.value());
					
					// Mensaje de notificacion
					notification.setToastrNotification(Boolean.TRUE);
					notification.setToastr(toastr);
					
					// JSon response 
					response.setSuccess(Boolean.TRUE);
					response.setNotification(notification);
					
					// JSon informacion fiscal
					emitter.setCertifaicateCreationDate(UDate.formattedDate(certificateCreationDate, DATE_FORMAT));
					emitter.setCertificateNumber(certificateNumber);
					emitter.setCertificateValidDate(UDate.formattedDate(certificateExpirationDate, DATE_FORMAT));
					emitter.setNameOrBusinessName(null);
					emitter.setPersonType(emitterTypeEntity.getValor());
					emitter.setRfc(rfc);
					emitter.setTaxRegime(regimenFiscalEntityByCode.getValor());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			emitter.setResponse(response);
			jsonInString = gson.toJson(emitter);
			return jsonInString;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/emitterTaxInformationLoad", method = RequestMethod.POST)
	public @ResponseBody String emitterTaxInformationLoad(HttpServletRequest request){
		try {
			
			JEmitter emitter = new JEmitter();
			JResponse response = new JResponse();
			List<JError> errors = new ArrayList<>();
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			Decoder decoder = Base64.getDecoder();
			
			String rfc = request.getParameter("rfc");
			byte[] rfcDecode = decoder.decode(rfc);
			rfc = new String(rfcDecode);
			JError error = new JError();
			if(UValidator.isNullOrEmpty(rfc)){
				error.setField("rfcActivo");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}else if(!rfc.equals(rfcActive)){
				error.setField("rfcActivo");
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errors.add(error);
			}
			
			Gson gson = new Gson();
			String jsonInString = null;
			
			if(!errors.isEmpty()){
				response.setError(Boolean.TRUE);
				response.setErrors(errors);
			}else{
				
				// JSon response 
				response.setSuccess(Boolean.TRUE);
				
				emitter.setRfc(rfc);
				emitter.setPersonType(emitterTools.getPersonType().getTipoPersona().getValor());
				emitter.setTaxRegime(emitterTools.getTaxRegime().getRegimenFiscal().getValor());
			}
			
			emitter.setResponse(response);
			jsonInString = gson.toJson(emitter);
			return jsonInString;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
