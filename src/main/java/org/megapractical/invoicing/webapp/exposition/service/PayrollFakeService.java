package org.megapractical.invoicing.webapp.exposition.service;

import java.io.ByteArrayInputStream;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.sat.integration.v40.CFDv4;

import lombok.val;

public final class PayrollFakeService {
	
	private PayrollFakeService() {		
	}
	
	public static final StampResponse payrollFakeStamp() {
		try {
			
			val cfdi = getCfdi();
			val xml = cfdi.getBytes();
			val cfd = xml != null ? new CFDv4(new ByteArrayInputStream(xml)) : null;
			val voucher = cfd != null ? cfd.doGetComprobante() : null;
			val expeditionDate = voucher != null ? voucher.getFecha() : null;
			
			return StampResponse
					.builder()
					.stamped(true)
					//.xmlBeforeStamp(xmlBeforeStamp)
					.stampedXml(cfdi)
					.xml(xml)
					.uuid("358072e0-2c5a-486a-b9b4-f005c8708453")
					.emitterSeal(getEmitterSeal())
					.satSeal(getSatSeal())
					.certificateEmitterNumber("30001000000400002434")
					.certificateSatNumber("30001000000400002495")
					.originalString(getOriginalString())
					.qrCode(getQrCode())
					.cfdv4(cfd)
					.voucher(voucher)
					.dateStamp("2022-12-26T14:19:53")
					.dateExpedition(expeditionDate)
					.build();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static String getCfdi() {
		return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<cfdi:Comprobante xmlns:cfdi=\"http://www.sat.gob.mx/cfd/4\" xmlns:nomina12=\"http://www.sat.gob.mx/nomina12\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Certificado=\"MIIFuzCCA6OgAwIBAgIUMzAwMDEwMDAwMDA0MDAwMDI0MzQwDQYJKoZIhvcNAQELBQAwggErMQ8wDQYDVQQDDAZBQyBVQVQxLjAsBgNVBAoMJVNFUlZJQ0lPIERFIEFETUlOSVNUUkFDSU9OIFRSSUJVVEFSSUExGjAYBgNVBAsMEVNBVC1JRVMgQXV0aG9yaXR5MSgwJgYJKoZIhvcNAQkBFhlvc2Nhci5tYXJ0aW5lekBzYXQuZ29iLm14MR0wGwYDVQQJDBQzcmEgY2VycmFkYSBkZSBjYWRpejEOMAwGA1UEEQwFMDYzNzAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBDSVVEQUQgREUgTUVYSUNPMREwDwYDVQQHDAhDT1lPQUNBTjERMA8GA1UELRMIMi41LjQuNDUxJTAjBgkqhkiG9w0BCQITFnJlc3BvbnNhYmxlOiBBQ0RNQS1TQVQwHhcNMTkwNjE3MTk0NDE0WhcNMjMwNjE3MTk0NDE0WjCB4jEnMCUGA1UEAxMeRVNDVUVMQSBLRU1QRVIgVVJHQVRFIFNBIERFIENWMScwJQYDVQQpEx5FU0NVRUxBIEtFTVBFUiBVUkdBVEUgU0EgREUgQ1YxJzAlBgNVBAoTHkVTQ1VFTEEgS0VNUEVSIFVSR0FURSBTQSBERSBDVjElMCMGA1UELRMcRUtVOTAwMzE3M0M5IC8gWElRQjg5MTExNlFFNDEeMBwGA1UEBRMVIC8gWElRQjg5MTExNk1HUk1aUjA1MR4wHAYDVQQLExVFc2N1ZWxhIEtlbXBlciBVcmdhdGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCN0peKpgfOL75iYRv1fqq+oVYsLPVUR/GibYmGKc9InHFy5lYF6OTYjnIIvmkOdRobbGlCUxORX/tLsl8Ya9gm6Yo7hHnODRBIDup3GISFzB/96R9K/MzYQOcscMIoBDARaycnLvy7FlMvO7/rlVnsSARxZRO8Kz8Zkksj2zpeYpjZIya/369+oGqQk1cTRkHo59JvJ4Tfbk/3iIyf4H/Ini9nBe9cYWo0MnKob7DDt/vsdi5tA8mMtA953LapNyCZIDCRQQlUGNgDqY9/8F5mUvVgkcczsIgGdvf9vMQPSf3jjCiKj7j6ucxl1+FwJWmbvgNmiaUR/0q4m2rm78lFAgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQBcpj1TjT4jiinIujIdAlFzE6kRwYJCnDG08zSp4kSnShjxADGEXH2chehKMV0FY7c4njA5eDGdA/G2OCTPvF5rpeCZP5Dw504RZkYDl2suRz+wa1sNBVpbnBJEK0fQcN3IftBwsgNFdFhUtCyw3lus1SSJbPxjLHS6FcZZ51YSeIfcNXOAuTqdimusaXq15GrSrCOkM6n2jfj2sMJYM2HXaXJ6rGTEgYmhYdwxWtil6RfZB+fGQ/H9I9WLnl4KTZUS6C9+NLHh4FPDhSk19fpS2S/56aqgFoGAkXAYt9Fy5ECaPcULIfJ1DEbsXKyRdCv3JY89+0MNkOdaDnsemS2o5Gl08zI4iYtt3L40gAZ60NPh31kVLnYNsmvfNxYyKp+AeJtDHyW9w7ftM0Hoi+BuRmcAQSKFV3pk8j51la+jrRBrAUv8blbRcQ5BiZUwJzHFEKIwTsRGoRyEx96sNnB03n6GTwjIGz92SmLdNl95r9rkvp+2m4S6q1lPuXaFg7DGBrXWC8iyqeWE2iobdwIIuXPTMVqQb12m1dAkJVRO5NdHnP/MpqOvOgLqoZBNHGyBg4Gqm4sCJHCxA1c8Elfa2RQTCk0tAzllL4vOnI1GHkGJn65xokGsaU4B4D36xh7eWrfj4/pgWHmtoDAYa8wzSwo2GVCZOs+mtEgOQB91/g==\" Descuento=\"2171.37\" Exportacion=\"01\" Fecha=\"2022-12-26T14:15:49\" Folio=\"16UAGRO2022200P000025NP1\" LugarExpedicion=\"39070\" MetodoPago=\"PUE\" Moneda=\"MXN\" NoCertificado=\"30001000000400002434\" Sello=\"WjUJkaYnPtq7GGUaeA7G7JXTrHmHe2Bu4OM9ZgVEBM5XzrrY621Qbpoy+gKrhBQJ9txQpnzZsrUcKPZhELYpggDllUjMKJcCwTNe4vxzLpuDblzERJjGTVkTHgOwfgkgekc803CHNQv1vw8f6PkDM/nYOC4i7g87P7HmEXnQCuRQx1c6+BrJwwE6sXVfqg+CfvhJmWEwmUNVVemmwOTNv8VOzd0l9ZDDQQ29kUVzJT7GgEW7GWimyZwNV08WOwBKvz86eEDVci4lW0wQpgkmS1ndNKtftPozDhyAeij2cnbTWfYgh4v4yklYs/CWJ122B79g5A5MWaSH0MdCFaG5GA==\" Serie=\"16UAGRO2022200\" SubTotal=\"10088.61\" TipoDeComprobante=\"N\" Total=\"7917.24\" Version=\"4.0\" xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/nomina12 http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd\">\r\n"
				+ "	<cfdi:Emisor Nombre=\"ESCUELA KEMPER URGATE\" RegimenFiscal=\"601\" Rfc=\"EKU9003173C9\"/>\r\n"
				+ "	<cfdi:Receptor DomicilioFiscalReceptor=\"39070\" Nombre=\"BENJAMIN DE LA CRUZ SERRANO\" RegimenFiscalReceptor=\"605\" Rfc=\"CUSB600707679\" UsoCFDI=\"CN01\"/>\r\n"
				+ "	<cfdi:Conceptos>\r\n"
				+ "		<cfdi:Concepto Cantidad=\"1\" ClaveProdServ=\"84111505\" ClaveUnidad=\"ACT\" Descripcion=\"Pago de nómina\" Descuento=\"2171.37\" Importe=\"10088.61\" ObjetoImp=\"01\" ValorUnitario=\"10088.61\"/>\r\n"
				+ "	</cfdi:Conceptos>\r\n"
				+ "	<cfdi:Complemento>\r\n"
				+ "		<nomina12:Nomina FechaFinalPago=\"2022-10-30\" FechaInicialPago=\"2022-10-16\" FechaPago=\"2022-10-28\" NumDiasPagados=\"15\" TipoNomina=\"O\" TotalDeducciones=\"2171.37\" TotalOtrosPagos=\"0.00\" TotalPercepciones=\"10088.61\" Version=\"1.2\">\r\n"
				+ "			<nomina12:Emisor RegistroPatronal=\"1272309901\"/>\r\n"
				+ "			<nomina12:Receptor Antigüedad=\"P2222W\" ClaveEntFed=\"GRO\" Curp=\"CUSB600707HGRRRN07\" Departamento=\"ESCUELA PREPARATORIA 1 CHILPANCINGO\" FechaInicioRelLaboral=\"1980-03-14\" NumEmpleado=\"25\" NumSeguridadSocial=\"80876038037\" PeriodicidadPago=\"04\" Puesto=\"CAPT OP PROF A\" RiesgoPuesto=\"99\" SalarioDiarioIntegrado=\"672.57\" Sindicalizado=\"Sí\" TipoContrato=\"01\" TipoJornada=\"01\" TipoRegimen=\"02\">\r\n"
				+ "				<nomina12:SubContratacion PorcentajeTiempo=\"100.00\" RfcLabora=\"UAG630904NU6\"/>\r\n"
				+ "			</nomina12:Receptor>\r\n"
				+ "			<nomina12:Percepciones TotalExento=\"0.00\" TotalGravado=\"10088.61\" TotalSeparacionIndemnizacion=\"0.00\" TotalSueldos=\"10088.61\">\r\n"
				+ "				<nomina12:Percepcion Clave=\"001\" Concepto=\"SUELDO BASE\" ImporteExento=\"0.00\" ImporteGravado=\"5441.70\" TipoPercepcion=\"001\"/>\r\n"
				+ "				<nomina12:Percepcion Clave=\"002\" Concepto=\"5,5% MATERIAL DE TRABAJO\" ImporteExento=\"0.00\" ImporteGravado=\"299.29\" TipoPercepcion=\"038\"/>\r\n"
				+ "				<nomina12:Percepcion Clave=\"004\" Concepto=\"PRIMA DE ANTIGUEDAD\" ImporteExento=\"0.00\" ImporteGravado=\"4026.86\" TipoPercepcion=\"038\"/>\r\n"
				+ "				<nomina12:Percepcion Clave=\"005\" Concepto=\"EST, A LA PRODUCTIVIDAD\" ImporteExento=\"0.00\" ImporteGravado=\"103.39\" TipoPercepcion=\"038\"/>\r\n"
				+ "				<nomina12:Percepcion Clave=\"040\" Concepto=\"DESPENSA FAMILIAR\" ImporteExento=\"0.00\" ImporteGravado=\"191.42\" TipoPercepcion=\"029\"/>\r\n"
				+ "				<nomina12:Percepcion Clave=\"041\" Concepto=\"AYUDA  PARA TRANSPORTE\" ImporteExento=\"0.00\" ImporteGravado=\"13.56\" TipoPercepcion=\"036\"/>\r\n"
				+ "				<nomina12:Percepcion Clave=\"042\" Concepto=\"AYUDA PARA RENTA\" ImporteExento=\"0.00\" ImporteGravado=\"12.39\" TipoPercepcion=\"033\"/>\r\n"
				+ "			</nomina12:Percepciones>\r\n"
				+ "			<nomina12:Deducciones TotalImpuestosRetenidos=\"1434.35\" TotalOtrasDeducciones=\"737.02\">\r\n"
				+ "				<nomina12:Deduccion Clave=\"500\" Concepto=\"ISR\" Importe=\"1434.35\" TipoDeduccion=\"002\"/>\r\n"
				+ "				<nomina12:Deduccion Clave=\"530\" Concepto=\"3,375% DESC SEGURO DE SALUD\" Importe=\"183.66\" TipoDeduccion=\"001\"/>\r\n"
				+ "				<nomina12:Deduccion Clave=\"531\" Concepto=\"6,125% DESC CESANTIA Y VEJEZ\" Importe=\"333.30\" TipoDeduccion=\"003\"/>\r\n"
				+ "				<nomina12:Deduccion Clave=\"532\" Concepto=\"0,625% DESC SEGURO DE INVALIDEZ\" Importe=\"34.01\" TipoDeduccion=\"004\"/>\r\n"
				+ "				<nomina12:Deduccion Clave=\"533\" Concepto=\"0,5% DESC SERV SOC Y CULTURALES\" Importe=\"27.21\" TipoDeduccion=\"004\"/>\r\n"
				+ "				<nomina12:Deduccion Clave=\"560\" Concepto=\"1% FONDO DE AH, STTAISUAG\" Importe=\"54.42\" TipoDeduccion=\"004\"/>\r\n"
				+ "				<nomina12:Deduccion Clave=\"562\" Concepto=\"CAJA DE AHORRO STTAISUAG\" Importe=\"50.00\" TipoDeduccion=\"004\"/>\r\n"
				+ "				<nomina12:Deduccion Clave=\"564\" Concepto=\"1% CUOTA STTAISUAG\" Importe=\"54.42\" TipoDeduccion=\"019\"/>\r\n"
				+ "			</nomina12:Deducciones>\r\n"
				+ "			<nomina12:OtrosPagos>\r\n"
				+ "				<nomina12:OtroPago Clave=\"055\" Concepto=\"SUBSIDIO AL EMPLEO\" Importe=\"0.00\" TipoOtroPago=\"002\">\r\n"
				+ "					<nomina12:SubsidioAlEmpleo SubsidioCausado=\"0.00\"/>\r\n"
				+ "				</nomina12:OtroPago>\r\n"
				+ "			</nomina12:OtrosPagos>\r\n"
				+ "		</nomina12:Nomina>\r\n"
				+ "		<tfd:TimbreFiscalDigital xsi:schemaLocation=\"http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd\" Version=\"1.1\" UUID=\"358072e0-2c5a-486a-b9b4-f005c8708453\" FechaTimbrado=\"2022-12-26T14:19:53\" RfcProvCertif=\"SPR190613I52\" SelloCFD=\"WjUJkaYnPtq7GGUaeA7G7JXTrHmHe2Bu4OM9ZgVEBM5XzrrY621Qbpoy+gKrhBQJ9txQpnzZsrUcKPZhELYpggDllUjMKJcCwTNe4vxzLpuDblzERJjGTVkTHgOwfgkgekc803CHNQv1vw8f6PkDM/nYOC4i7g87P7HmEXnQCuRQx1c6+BrJwwE6sXVfqg+CfvhJmWEwmUNVVemmwOTNv8VOzd0l9ZDDQQ29kUVzJT7GgEW7GWimyZwNV08WOwBKvz86eEDVci4lW0wQpgkmS1ndNKtftPozDhyAeij2cnbTWfYgh4v4yklYs/CWJ122B79g5A5MWaSH0MdCFaG5GA==\" NoCertificadoSAT=\"30001000000400002495\" SelloSAT=\"JiQRvESltJIWlQvitPFHs+77X4V7+yehNeZXYMzPXbXKhEc1yiCsUybPjAmtsc5mhupbok51BW8nsrVtPGb2Qn5KkniBDLt5/NvfzWeZXT+A4bAxXGw1MuGEheT5iRVI03pkD4IWkmwHMj1Xthrfd3pe7LO27JPm1OsQ9thbJlZJ9PPx7fmPAXRUVwcqold7oQOxIgOrBpjQvKVbq/1nIoWdoBJDGCFfvW5cCb+om0X2ymXTa90ERHn0Nbp+9rbjx9Q95MxJG6im7MJ6AOuU/XGIuuj4Lu6QDyWa4bHrRrMkZV3kX3bCTeX8AQZBIM7K0T4ZpBFvicQBAmfHoedhbQ==\" xmlns:tfd=\"http://www.sat.gob.mx/TimbreFiscalDigital\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>\r\n"
				+ "	</cfdi:Complemento>\r\n"
				+ "</cfdi:Comprobante>";
	}
	
	private static String getOriginalString() {
		return "||1.1|358072e0-2c5a-486a-b9b4-f005c8708453|2022-12-26T14:19:53|SPR190613I52|WjUJkaYnPtq7GGUaeA7G7JXTrHmHe2Bu4OM9ZgVEBM5XzrrY621Qbpoy+gKrhBQJ9txQpnzZsrUcKPZhELYpggDllUjMKJcCwTNe4vxzLpuDblzERJjGTVkTHgOwfgkgekc803CHNQv1vw8f6PkDM/nYOC4i7g87P7HmEXnQCuRQx1c6+BrJwwE6sXVfqg+CfvhJmWEwmUNVVemmwOTNv8VOzd0l9ZDDQQ29kUVzJT7GgEW7GWimyZwNV08WOwBKvz86eEDVci4lW0wQpgkmS1ndNKtftPozDhyAeij2cnbTWfYgh4v4yklYs/CWJ122B79g5A5MWaSH0MdCFaG5GA==|30001000000400002495||";
	}
	
	private static String getQrCode() {
		return "iVBORw0KGgoAAAANSUhEUgAAAIwAAACMCAYAAACuwEE+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAxYSURBVHhe7ZLRqiQxDkPn/396dhtaIA5ROa642WHJAT1IVnzdl/rz93JpcD+YS4v7wVxa3A/m0uJ+MJcW94O5tLgfzKXF/WAuLe4Hc2lxP5hLi/vBXFrcD+bS4n4wlxb3g7m0uB/MpcX9YC4t7gdzaTHywfz58+eVEpxXXijnfNczF5xTFbs9UfU172qCkS2r43aU4LzyQjnnu5654Jyq2O2Jqq95VxOMbOkeNPkDPnDf7n71Kgl64m86IrvzXbr9J0a2/C9/wAfu292vXiVBT/xNR2R3vku3/8TIFh4kT4m3PuXi1CfU25WgF297grk8JehPGNmSDqTEW59yceoT6u1K0Iu3PcFcnhL0J4xsSQdSIvmUJ6o+PUl95oJzqeJtX+x6StCfMLIlHUiJ5FOeqPr0JPWZC86lird9sespQX/CyJZ0ICWSr5RIvSoX3jlRIvWYUwnO/Y1L0J8wsiUdSInkKyVSr8qFd06USD3mVIJzf+MS9CeMbOkexL48JZLfzUU3F9WcsF95oTzNRTUn3f4TI1tOf4A8JZLfzUU3F9WcsF95oTzNRTUn3f4TI1t0UFfi+ne+qwlGtqyO25G4/p3vaoKZLYfwB/mPdCW688oTzSlBL7zrc+ZJ/yL/xFX8B/k/zZXozitPNKcEvfCuz5kn/YuMXMUf6D/6SaTKOWcuidXsI7LqPEnseua7pPfMk37ByFYe6Ec/iVQ558wlsZp9RFadJ4ldz3yX9J550i8Y2bo69iORcsJ58rt5gn2KME9+NxecU4T5ae8NI1t0ECVSTjhPfjdPsE8R5snv5oJzijA/7b1hZktB9QPkd3PBuSRSnmDvrU8S9InUYy7P/Bf8dvsX/pDkd3PBuSRSnmDvrU8S9InUYy7P/BeMbOfBVGLVdYmUC+befZKocuGdJwn6hL91EeZdP8HINh2WlFh1XSLlgrl3nySqXHjnSYI+4W9dhHnXTzC6LR1c5Ulk1flIVJ6kuXLOU0449zeekzRnvuupCWa2fOFhfuxTnkRWnY9E5UmaK+c85YRzf+M5SXPmu56aYGQLD6p8Qj2qgr23vptP43/LVVH1U/6GkS08qPIJ9agK9t76bj6N/y1XRdVP+Rvmf/V/4YHJJwl64V0XqfLunJ6kOfPKi9SrcmqS2W1feGjySYJeeNdFqrw7pydpzrzyIvWqnJpkZFs6kJ74m1Vvd56o5kK91Oe80i7pXcpFNRfV/A0j2/x4P5Ce+JtVb3eeqOZCvdTnvNIu6V3KRTUX1fwNs9u+8NDkkxKrrksk381F5UXqpZx41+f0xN+sNMnsti88NPmkxKrrEsl3c1F5kXopJ971OT3xNytNMrLt9MC37/mOSlQ9zpNIygXn8t1ceMcl6CcY2ebHvjnw7Xu+oxJVj/MkknLBuXw3F95xCfoJRrb5sR2J1ewjceqJ5lWP7PZ995N2YT+9T/kkI9t1aFdiNftInHqiedUju33f/aRd2E/vUz7JT7bzcHnmCfb8bUcV3b5g33e4SJXvKlHNJ/jJdh4uzzzBnr/tqKLbF+z7Dhep8l0lqvkEI9v9x+wcnHrMk095wt94L3lKVJ5oXkkkT5GUi2reYWSLDto9LPWYJ5/yhL/xXvKUqDzRvJJIniIpF9W8w8yWLzys8iL1Koldz1ww9+5J/pa0j16kXFTzDjNbvvCwyovUqyR2PXPB3Lsn+VvSPnqRclHNO8xs+aLDkgS9qPI0r0jvK09Sf1diNXOJKhf0IuUnjG7TgUmCXlR5mlek95Unqb8rsZq5RJULepHyE0a36UBK0Avv+pw+Ub2TpwR9xe77lAvN2WNOCXpSzd8wuk0HUoJeeNfn9InqnTwl6Ct236dcaM4ec0rQk2r+htFtOvCtBD1Jc+VUYtXdkaBP+NuVyKpzoklGt62O7UjQkzRXTiVW3R0J+oS/XYmsOieaZGQbD6s8eTtXTonkU55IfeaJ076USL2UnzCyhQdVnrydK6dE8ilPpD7zxGlfSqReyk8Y2cKD/EiXqHzF7vuUE/aSZ07Yo8Rq5iLMK/9LRv5K+gGUqHzF7vuUE/aSZ07Yo8Rq5iLMK/9LfvpX9EOqH8S5v5lQRdXjfNdXEqvZR4Ke+BvvpfyEmS2B3YM59zcTqqh6nO/6SmI1+0jQE3/jvZSfMLIlHca8K0Ff4TtWEqvZk0TKCXtJYjfflaA/YWSLH+mHMe9K0Ff4jpXEavYkkXLCXpLYzXcl6E8Y2ZIOTDl5m3NeeaH8dC7e9pJnLph713NRzd8wsoUH+ZGrnLzNOa+8UH46F297yTMXzL3ruajmbxjZkg6r8oS/eZJYzZ5E3s6TT7nwzkqkmovd3gkjW9OhVZ7wN08Sq9mTyNt58ikX3lmJVHOx2zthZGs6lD6Ret2csCfPXDDf9VRi1V1J0Avv+jx55ieMbEmH0SdSr5sT9uSZC+a7nkqsuisJeuFdnyfP/ISZLV94oCRWs48I89Qjb9+J9D6JrDorJdLc365EqvkJo9t4qCRWs48I89Qjb9+J9D6JrDorJdLc365EqvkJI9vSYX60i1R5UoJzf/OkiqrP3LsusZqtJFYzV6KadxjZkg5STpEqT0pw7m+eVFH1mXvXJVazlcRq5kpU8w4zW77wsHSo8u6ceSWRcpJ6lSfd+W4/9Zh71/MJRrfxwHSw8u6ceSWRcpJ6lSfd+W4/9Zh71/MJZrcBHi6JrhfKu3P6ivS+0i6rtx+J5Lv5JLPbgP8Il+h6obw7p69I7yvtsnr7kUi+m08yss2PdYnV7KNd2Pcdroqql+bKKcLcu6uceNfnVU68u5qfMLKNB0piNftoF/Z9h6ui6qW5coow9+4qJ971eZUT767mJ4xu46FUoppX+N94kljNPhL0wrtPc+Fdzyu679ir/AkzW77osKRENa/wv/EksZp9JOiFd5/mwrueV3TfsVf5E2a2AB1IieSTTuGet555gv2uuqx2uCaZ3fZldfRHIvmkU7jnrWeeYL+rLqsdrklGtq2O/EjQJ6qe5kmCXnj3aS7e9uiFcs6TZy6Ye9fzXzCynQdLgj5R9TRPEvTCu09z8bZHL5Rznjxzwdy7nv+Cke3p4CoXla847cunnFR5kqjyRNWn/wUj23UoD65yUfmK0758ykmVJ4kqT1R9+l8wsl2HpoPTnDklunmCvfROeaWKbo8Sq9lHgp5U8w4jW3RQOizNmVOimyfYS++UV6ro9iixmn0k6Ek17zCyRQfxMOa7Eru+mydSnxLJV7mUqObCd636KT9hZJsf7Qcy35XY9d08kfqUSL7KpUQ1F75r1U/5CaPb/PidQ6s+c++uJJJnPgX3+t/yPMGev33KKZLyE0a3+fE7h1Z95t5dSSTPfAru9b/leYI9f/uUUyTlJ8xuA9UPqea7sJ88c8F5kqhy4l2f04vUO9UEM1sC6VD/EU/zXdhPnrngPElUOfGuz+lF6p1qgpkth6Qf5D92JbGauQjz1BNVnz6ReszlqUTqpfyEmS2HpB/kP3YlsZq5CPPUE1WfPpF6zOWpROql/ISRLTxsVwnO/c1Kgl5492kukk85qXryKRfeWeWC/peM/BUd3FWCc3+zkqAX3n2ai+RTTqqefMqFd1a5oP8lI3+lezD78szJbo+kvu9azQV7kqAn/mbVY+5dV6LqpfwNI1u6B7Evz5zs9kjq+67VXLAnCXrib1Y95t51Japeyt8wsoUHyVMi+ZQL73guOJcEvfDujhKrroukXPjbNxL0J4xsSQdSIvmUC+94LjiXBL3w7o4Sq66LpFz42zcS9CeMbEkHUmLXUxXs+VvPRcqFv/UevUg9SiRfSdAndns7jGzhQfKU2PVUBXv+1nORcuFvvUcvUo8SyVcS9Ind3g4jW3iQPCWSp0jKhb9d9Zh790mCPuFvV/2UV1T7OE/5CSNbeJAf6RLJUyTlwt+uesy9+yRBn/C3q37KK6p9nKf8hJEt3YOqvuZJgl54tzNPflck5cLfeo9epB5zkfITRrZ1D6v6micJeuHdzjz5XZGUC3/rPXqResxFyk8Y2eZHdyRWs49Eyrvwve9c5V18l79PnvkufC8J+klGtvrRHYnV7COR8i587ztXeRff5e+TZ74L30uCfpLfbL3833I/mEuL+8FcWtwP5tLifjCXFveDubS4H8ylxf1gLi3uB3NpcT+YS4v7wVxa3A/m0uJ+MJcW94O5tLgfzKXF/WAuLe4Hc2nw9+9/AKA9VruIQ8fuAAAAAElFTkSuQmCC";
	}
	
	private static String getSatSeal() {
		return "JiQRvESltJIWlQvitPFHs+77X4V7+yehNeZXYMzPXbXKhEc1yiCsUybPjAmtsc5mhupbok51BW8nsrVtPGb2Qn5KkniBDLt5/NvfzWeZXT+A4bAxXGw1MuGEheT5iRVI03pkD4IWkmwHMj1Xthrfd3pe7LO27JPm1OsQ9thbJlZJ9PPx7fmPAXRUVwcqold7oQOxIgOrBpjQvKVbq/1nIoWdoBJDGCFfvW5cCb+om0X2ymXTa90ERHn0Nbp+9rbjx9Q95MxJG6im7MJ6AOuU/XGIuuj4Lu6QDyWa4bHrRrMkZV3kX3bCTeX8AQZBIM7K0T4ZpBFvicQBAmfHoedhbQ==";
	}
	
	private static String getEmitterSeal() {
		return "WjUJkaYnPtq7GGUaeA7G7JXTrHmHe2Bu4OM9ZgVEBM5XzrrY621Qbpoy+gKrhBQJ9txQpnzZsrUcKPZhELYpggDllUjMKJcCwTNe4vxzLpuDblzERJjGTVkTHgOwfgkgekc803CHNQv1vw8f6PkDM/nYOC4i7g87P7HmEXnQCuRQx1c6+BrJwwE6sXVfqg+CfvhJmWEwmUNVVemmwOTNv8VOzd0l9ZDDQQ29kUVzJT7GgEW7GWimyZwNV08WOwBKvz86eEDVci4lW0wQpgkmS1ndNKtftPozDhyAeij2cnbTWfYgh4v4yklYs/CWJ122B79g5A5MWaSH0MdCFaG5GA==";
	}
	
}