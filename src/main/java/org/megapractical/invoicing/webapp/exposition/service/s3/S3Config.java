package org.megapractical.invoicing.webapp.exposition.service.s3;

import org.megapractical.invoicing.webapp.config.AppSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;
import lombok.val;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Configuration
@RequiredArgsConstructor
public class S3Config {

	private final AppSettings appSettings;
	
	@Bean
	public S3Client s3Client() {
		val accessKey = appSettings.getPropertyValue("aws.access.key");
		val secretAccess = appSettings.getPropertyValue("aws.secret.access");
		val region = appSettings.getPropertyValue("aws.region");
		
		val credentials = AwsBasicCredentials.create(accessKey, secretAccess);
		val credentialsProvider = StaticCredentialsProvider.create(credentials);
		return S3Client.builder().region(Region.of(region)).credentialsProvider(credentialsProvider).build();
	}

}