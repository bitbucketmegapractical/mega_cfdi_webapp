package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.json.JVoucher.JVoucherResponse;

public interface VoucherPaymentFileService {
	
	JVoucherResponse populateVoucher(JVoucher voucher);
	
}