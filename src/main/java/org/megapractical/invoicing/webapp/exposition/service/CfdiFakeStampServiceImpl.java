package org.megapractical.invoicing.webapp.exposition.service;

import java.io.ByteArrayInputStream;

import org.megapractical.invoicing.api.service.CfdiXmlService;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.sat.integration.v40.CFDv4;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.webapp.util.FakeStampUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
public class CfdiFakeStampServiceImpl implements CfdiFakeStampService {
	
	@Autowired
	private CfdiBuilderService cfdiBuilderService;
	
	@Override
	public StampResponse cfdiFakeStamp(Comprobante voucher) {
		val xmlBeforeStamp = getXmlBeforeStamp(voucher);
		return fakeStamp(xmlBeforeStamp);
	}
	
	@Override
	public StampResponse cfdiFakeStamp(ApiCfdi apiCfdi) {
		return cfdiFakeStamp(apiCfdi, false);
	}
	
	@Override
	public StampResponse cfdiFakeStamp(ApiCfdi apiCfdi, boolean withStampError) {
		if (!withStampError) {
			val xmlBeforeStamp = getXmlBeforeStamp(apiCfdi);
			return fakeStamp(xmlBeforeStamp);
		}
		return FakeStampUtils.cfdiFakeStampError();
	}
	
	@Override
	public StampResponse paymentFakeStamp(ApiCfdi apiCfdi) {
		try {
			
			val xmlBeforeStamp = getXmlBeforeStamp(apiCfdi);
			val cfdi = getPaymentXml();
			val xml = cfdi.getBytes();
			val cfd = xml != null ? new CFDv4(new ByteArrayInputStream(xml)) : null;
			val voucher = cfd != null ? cfd.doGetComprobante() : null;
			val expeditionDate = voucher != null ? voucher.getFecha() : null;
			
			return StampResponse
					.builder()
					.stamped(true)
					.xmlBeforeStamp(xmlBeforeStamp)
					.stampedXml(cfdi)
					.xml(xml)
					.uuid("d0bdabe5-043c-42e7-952b-298b4e0196f5")
					.emitterSeal(getPaymentEmitterSeal())
					.satSeal(getPaymentSatSeal())
					.certificateEmitterNumber("30001000000400002434")
					.certificateSatNumber("30001000000400002495")
					.originalString(getPaymentOriginalString())
					.qrCode(getPaymentQrCode())
					.cfdv4(cfd)
					.voucher(voucher)
					.dateStamp("2023-01-08T20:45:37")
					.dateExpedition(expeditionDate)
					.build();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private StampResponse fakeStamp(byte[] xmlBeforeStamp) {
		try {
			
			val cfdi = getCfdi();
			val xml = cfdi.getBytes();
			val cfd = xml != null ? new CFDv4(new ByteArrayInputStream(xml)) : null;
			val voucher = cfd != null ? cfd.doGetComprobante() : null;
			val expeditionDate = voucher != null ? voucher.getFecha() : null;
			
			return StampResponse
					.builder()
					.stamped(true)
					.xmlBeforeStamp(xmlBeforeStamp)
					.stampedXml(cfdi)
					.xml(xml)
					.uuid("2185599b-d3a1-44c6-a9fa-8fd6e79bf1af")
					.emitterSeal(getEmitterSeal())
					.satSeal(getSatSeal())
					.certificateEmitterNumber("30001000000400002434")
					.certificateSatNumber("30001000000400002495")
					.originalString(getOriginalString())
					.qrCode(getQrCode())
					.cfdv4(cfd)
					.voucher(voucher)
					.dateStamp("2022-12-02T17:07:32")
					.dateExpedition(expeditionDate)
					.build();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private byte[] getXmlBeforeStamp(Comprobante voucher) {
		return CfdiXmlService.generateXml(voucher);
	}
	
	private byte[] getXmlBeforeStamp(ApiCfdi apiCfdi) {
		val allowStamp = cfdiBuilderService.voucherBuild(apiCfdi, false);
		val voucher = allowStamp.getVoucher();
		return CfdiXmlService.generateXml(voucher);
	}
	
	private String getCfdi() {
		return "<?xml version=\"1.0\" encoding=\"utf-8\"?><cfdi:Comprobante xmlns:cfdi=\"http://www.sat.gob.mx/cfd/4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Certificado=\"MIIFuzCCA6OgAwIBAgIUMzAwMDEwMDAwMDA0MDAwMDI0MzQwDQYJKoZIhvcNAQELBQAwggErMQ8wDQYDVQQDDAZBQyBVQVQxLjAsBgNVBAoMJVNFUlZJQ0lPIERFIEFETUlOSVNUUkFDSU9OIFRSSUJVVEFSSUExGjAYBgNVBAsMEVNBVC1JRVMgQXV0aG9yaXR5MSgwJgYJKoZIhvcNAQkBFhlvc2Nhci5tYXJ0aW5lekBzYXQuZ29iLm14MR0wGwYDVQQJDBQzcmEgY2VycmFkYSBkZSBjYWRpejEOMAwGA1UEEQwFMDYzNzAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBDSVVEQUQgREUgTUVYSUNPMREwDwYDVQQHDAhDT1lPQUNBTjERMA8GA1UELRMIMi41LjQuNDUxJTAjBgkqhkiG9w0BCQITFnJlc3BvbnNhYmxlOiBBQ0RNQS1TQVQwHhcNMTkwNjE3MTk0NDE0WhcNMjMwNjE3MTk0NDE0WjCB4jEnMCUGA1UEAxMeRVNDVUVMQSBLRU1QRVIgVVJHQVRFIFNBIERFIENWMScwJQYDVQQpEx5FU0NVRUxBIEtFTVBFUiBVUkdBVEUgU0EgREUgQ1YxJzAlBgNVBAoTHkVTQ1VFTEEgS0VNUEVSIFVSR0FURSBTQSBERSBDVjElMCMGA1UELRMcRUtVOTAwMzE3M0M5IC8gWElRQjg5MTExNlFFNDEeMBwGA1UEBRMVIC8gWElRQjg5MTExNk1HUk1aUjA1MR4wHAYDVQQLExVFc2N1ZWxhIEtlbXBlciBVcmdhdGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCN0peKpgfOL75iYRv1fqq+oVYsLPVUR/GibYmGKc9InHFy5lYF6OTYjnIIvmkOdRobbGlCUxORX/tLsl8Ya9gm6Yo7hHnODRBIDup3GISFzB/96R9K/MzYQOcscMIoBDARaycnLvy7FlMvO7/rlVnsSARxZRO8Kz8Zkksj2zpeYpjZIya/369+oGqQk1cTRkHo59JvJ4Tfbk/3iIyf4H/Ini9nBe9cYWo0MnKob7DDt/vsdi5tA8mMtA953LapNyCZIDCRQQlUGNgDqY9/8F5mUvVgkcczsIgGdvf9vMQPSf3jjCiKj7j6ucxl1+FwJWmbvgNmiaUR/0q4m2rm78lFAgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQBcpj1TjT4jiinIujIdAlFzE6kRwYJCnDG08zSp4kSnShjxADGEXH2chehKMV0FY7c4njA5eDGdA/G2OCTPvF5rpeCZP5Dw504RZkYDl2suRz+wa1sNBVpbnBJEK0fQcN3IftBwsgNFdFhUtCyw3lus1SSJbPxjLHS6FcZZ51YSeIfcNXOAuTqdimusaXq15GrSrCOkM6n2jfj2sMJYM2HXaXJ6rGTEgYmhYdwxWtil6RfZB+fGQ/H9I9WLnl4KTZUS6C9+NLHh4FPDhSk19fpS2S/56aqgFoGAkXAYt9Fy5ECaPcULIfJ1DEbsXKyRdCv3JY89+0MNkOdaDnsemS2o5Gl08zI4iYtt3L40gAZ60NPh31kVLnYNsmvfNxYyKp+AeJtDHyW9w7ftM0Hoi+BuRmcAQSKFV3pk8j51la+jrRBrAUv8blbRcQ5BiZUwJzHFEKIwTsRGoRyEx96sNnB03n6GTwjIGz92SmLdNl95r9rkvp+2m4S6q1lPuXaFg7DGBrXWC8iyqeWE2iobdwIIuXPTMVqQb12m1dAkJVRO5NdHnP/MpqOvOgLqoZBNHGyBg4Gqm4sCJHCxA1c8Elfa2RQTCk0tAzllL4vOnI1GHkGJn65xokGsaU4B4D36xh7eWrfj4/pgWHmtoDAYa8wzSwo2GVCZOs+mtEgOQB91/g==\" Exportacion=\"01\" Fecha=\"2022-12-02T17:03:39\" FormaPago=\"03\" LugarExpedicion=\"50220\" MetodoPago=\"PUE\" Moneda=\"MXN\" NoCertificado=\"30001000000400002434\" Sello=\"KU2kWX4xxHrzAL+MXjXi2KeMaEIo9g3+XvI2H9EVbxphEmEwtxXyX2cTBBYasumUggsmMsDCqz0K8TANrJmr5XmvQ4zIZE6+fuU3Jny3Gkk0YJ4VbL4FSlBkLVLrUo7sXF/F+R6kaYriQn7N0L7HHzET2ueQQqHRzT//A3xGNWJ6B3dxkehObHOpanJHlhGj7KVvnSNqIg7pdDmIU5FEoV+i3yawszVwfa38J505b8lNsCOK79GoePAQ2aYSSxZUVTZCqdWWMpRCn4WbA6+9vrwN0kCtEuWqoEKm5NqQhIY8YhaGfAGdcB1J0gB8FFTzyC+H7EAAtRTnDXR7+kWxAg==\" SubTotal=\"15000.00\" TipoDeComprobante=\"I\" Total=\"17400.00\" Version=\"4.0\" xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd\"><cfdi:Emisor Nombre=\"ESCUELA KEMPER URGATE\" RegimenFiscal=\"601\" Rfc=\"EKU9003173C9\" /><cfdi:Receptor DomicilioFiscalReceptor=\"39070\" Nombre=\"BENJAMIN DE LA CRUZ SERRANO\" RegimenFiscalReceptor=\"605\" Rfc=\"CUSB600707679\" UsoCFDI=\"D10\" /><cfdi:Conceptos><cfdi:Concepto Cantidad=\"1.00\" ClaveProdServ=\"01010101\" ClaveUnidad=\"H87\" Descripcion=\"CONCEPTO DE PRUEBA 15000\" Importe=\"15000.00\" ObjetoImp=\"02\" ValorUnitario=\"15000.00\"><cfdi:Impuestos><cfdi:Traslados><cfdi:Traslado Base=\"15000.00\" Importe=\"2400.00\" Impuesto=\"002\" TasaOCuota=\"0.160000\" TipoFactor=\"Tasa\" /></cfdi:Traslados></cfdi:Impuestos></cfdi:Concepto></cfdi:Conceptos><cfdi:Impuestos TotalImpuestosTrasladados=\"2400.00\"><cfdi:Traslados><cfdi:Traslado Base=\"15000.00\" Importe=\"2400.00\" Impuesto=\"002\" TasaOCuota=\"0.160000\" TipoFactor=\"Tasa\" /></cfdi:Traslados></cfdi:Impuestos><cfdi:Complemento><tfd:TimbreFiscalDigital xsi:schemaLocation=\"http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd\" Version=\"1.1\" UUID=\"2185599b-d3a1-44c6-a9fa-8fd6e79bf1af\" FechaTimbrado=\"2022-12-02T17:07:32\" RfcProvCertif=\"SPR190613I52\" SelloCFD=\"KU2kWX4xxHrzAL+MXjXi2KeMaEIo9g3+XvI2H9EVbxphEmEwtxXyX2cTBBYasumUggsmMsDCqz0K8TANrJmr5XmvQ4zIZE6+fuU3Jny3Gkk0YJ4VbL4FSlBkLVLrUo7sXF/F+R6kaYriQn7N0L7HHzET2ueQQqHRzT//A3xGNWJ6B3dxkehObHOpanJHlhGj7KVvnSNqIg7pdDmIU5FEoV+i3yawszVwfa38J505b8lNsCOK79GoePAQ2aYSSxZUVTZCqdWWMpRCn4WbA6+9vrwN0kCtEuWqoEKm5NqQhIY8YhaGfAGdcB1J0gB8FFTzyC+H7EAAtRTnDXR7+kWxAg==\" NoCertificadoSAT=\"30001000000400002495\" SelloSAT=\"YRnKrNkFY+b8zEfhyLTXb7cD3QSwZ9jYGfdQGBETJnUxUrKuI4HK62Z0Yb7cxK8jymF/8Y7hGnsoYKeMvlLpCaeurIxYfb93H4Zu1pdC1YbbOWAMAoTHDPrBNaksZE11t8vLQiDOWKwSGrQ8CTR19PIVw0799/Gk7JvFwXBha8qXqfAIT6J3db5BfomdsuC7NavivpLz/8B8ba8Hm6Wqxc1VSPWEB6WOqNrcsA+w4+eYdTwT1tRK5RjxTQi4OwhoMTQKg2hxVQ4f/hOFceErA+PLrcidwnURjaH6T+YMzYEy02+H/NCuJASXooygqw8K+DAmSkAyDJXo0CSAyLA/9A==\" xmlns:tfd=\"http://www.sat.gob.mx/TimbreFiscalDigital\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" /></cfdi:Complemento></cfdi:Comprobante>";
	}
	
	private String getPaymentXml() {
		return "<?xml version=\"1.0\" encoding=\"utf-8\"?><cfdi:Comprobante xmlns:cfdi=\"http://www.sat.gob.mx/cfd/4\" xmlns:pago20=\"http://www.sat.gob.mx/Pagos20\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Certificado=\"MIIFuzCCA6OgAwIBAgIUMzAwMDEwMDAwMDA0MDAwMDI0MzQwDQYJKoZIhvcNAQELBQAwggErMQ8wDQYDVQQDDAZBQyBVQVQxLjAsBgNVBAoMJVNFUlZJQ0lPIERFIEFETUlOSVNUUkFDSU9OIFRSSUJVVEFSSUExGjAYBgNVBAsMEVNBVC1JRVMgQXV0aG9yaXR5MSgwJgYJKoZIhvcNAQkBFhlvc2Nhci5tYXJ0aW5lekBzYXQuZ29iLm14MR0wGwYDVQQJDBQzcmEgY2VycmFkYSBkZSBjYWRpejEOMAwGA1UEEQwFMDYzNzAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBDSVVEQUQgREUgTUVYSUNPMREwDwYDVQQHDAhDT1lPQUNBTjERMA8GA1UELRMIMi41LjQuNDUxJTAjBgkqhkiG9w0BCQITFnJlc3BvbnNhYmxlOiBBQ0RNQS1TQVQwHhcNMTkwNjE3MTk0NDE0WhcNMjMwNjE3MTk0NDE0WjCB4jEnMCUGA1UEAxMeRVNDVUVMQSBLRU1QRVIgVVJHQVRFIFNBIERFIENWMScwJQYDVQQpEx5FU0NVRUxBIEtFTVBFUiBVUkdBVEUgU0EgREUgQ1YxJzAlBgNVBAoTHkVTQ1VFTEEgS0VNUEVSIFVSR0FURSBTQSBERSBDVjElMCMGA1UELRMcRUtVOTAwMzE3M0M5IC8gWElRQjg5MTExNlFFNDEeMBwGA1UEBRMVIC8gWElRQjg5MTExNk1HUk1aUjA1MR4wHAYDVQQLExVFc2N1ZWxhIEtlbXBlciBVcmdhdGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCN0peKpgfOL75iYRv1fqq+oVYsLPVUR/GibYmGKc9InHFy5lYF6OTYjnIIvmkOdRobbGlCUxORX/tLsl8Ya9gm6Yo7hHnODRBIDup3GISFzB/96R9K/MzYQOcscMIoBDARaycnLvy7FlMvO7/rlVnsSARxZRO8Kz8Zkksj2zpeYpjZIya/369+oGqQk1cTRkHo59JvJ4Tfbk/3iIyf4H/Ini9nBe9cYWo0MnKob7DDt/vsdi5tA8mMtA953LapNyCZIDCRQQlUGNgDqY9/8F5mUvVgkcczsIgGdvf9vMQPSf3jjCiKj7j6ucxl1+FwJWmbvgNmiaUR/0q4m2rm78lFAgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQBcpj1TjT4jiinIujIdAlFzE6kRwYJCnDG08zSp4kSnShjxADGEXH2chehKMV0FY7c4njA5eDGdA/G2OCTPvF5rpeCZP5Dw504RZkYDl2suRz+wa1sNBVpbnBJEK0fQcN3IftBwsgNFdFhUtCyw3lus1SSJbPxjLHS6FcZZ51YSeIfcNXOAuTqdimusaXq15GrSrCOkM6n2jfj2sMJYM2HXaXJ6rGTEgYmhYdwxWtil6RfZB+fGQ/H9I9WLnl4KTZUS6C9+NLHh4FPDhSk19fpS2S/56aqgFoGAkXAYt9Fy5ECaPcULIfJ1DEbsXKyRdCv3JY89+0MNkOdaDnsemS2o5Gl08zI4iYtt3L40gAZ60NPh31kVLnYNsmvfNxYyKp+AeJtDHyW9w7ftM0Hoi+BuRmcAQSKFV3pk8j51la+jrRBrAUv8blbRcQ5BiZUwJzHFEKIwTsRGoRyEx96sNnB03n6GTwjIGz92SmLdNl95r9rkvp+2m4S6q1lPuXaFg7DGBrXWC8iyqeWE2iobdwIIuXPTMVqQb12m1dAkJVRO5NdHnP/MpqOvOgLqoZBNHGyBg4Gqm4sCJHCxA1c8Elfa2RQTCk0tAzllL4vOnI1GHkGJn65xokGsaU4B4D36xh7eWrfj4/pgWHmtoDAYa8wzSwo2GVCZOs+mtEgOQB91/g==\" Exportacion=\"01\" Fecha=\"2023-01-08T20:43:28\" LugarExpedicion=\"50220\" Moneda=\"XXX\" NoCertificado=\"30001000000400002434\" Sello=\"hWlCGI7ZL3rW9RIKcO8HY3cx552lCcnluxqq6V80BziB0D4oFkrUsLZ+rkno8ORSIjIROkuZPUXH/itMz6hyCD5k1hvnDlpAUOuwrmiqCe2susa54JgblN6TsueAvk3+4X38EJaI5sB/bcaMBy/2iYo8P/L0hSVl3WG9M+NvDaIgbXFGHF8gIIrhBpfXVd/2s07GhU8wx4YKyWC/2cr1AIUvEPmyokqUyTxuyVaG/XqiSThMO+aXmQDItfYNuk7gAN6ZjJNytAdsny30foaakpV5lZVwo3f2L+/WVbRXzCR8yw+xNpE1yatGH25RVMGZLxZB8rhsDEdbIQ1ZduYo9A==\" SubTotal=\"0\" TipoDeComprobante=\"P\" Total=\"0\" Version=\"4.0\" xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd\"><cfdi:Emisor Nombre=\"ESCUELA KEMPER URGATE\" RegimenFiscal=\"601\" Rfc=\"EKU9003173C9\" /><cfdi:Receptor DomicilioFiscalReceptor=\"39070\" Nombre=\"BENJAMIN DE LA CRUZ SERRANO\" RegimenFiscalReceptor=\"605\" Rfc=\"CUSB600707679\" UsoCFDI=\"CP01\" /><cfdi:Conceptos><cfdi:Concepto Cantidad=\"1\" ClaveProdServ=\"84111506\" ClaveUnidad=\"ACT\" Descripcion=\"Pago\" Importe=\"0\" ObjetoImp=\"01\" ValorUnitario=\"0\" /></cfdi:Conceptos><cfdi:Complemento><pago20:Pagos Version=\"2.0\"><pago20:Totales MontoTotalPagos=\"100.00\" TotalRetencionesIEPS=\"0.00\" TotalRetencionesISR=\"0.00\" TotalRetencionesIVA=\"0.00\" TotalTrasladosBaseIVA0=\"0.00\" TotalTrasladosBaseIVA16=\"1.00\" TotalTrasladosBaseIVA8=\"0.00\" TotalTrasladosBaseIVAExento=\"1.00\" TotalTrasladosImpuestoIVA0=\"0.00\" TotalTrasladosImpuestoIVA16=\"0.16\" TotalTrasladosImpuestoIVA8=\"0.00\" /><pago20:Pago FechaPago=\"2023-01-07T09:00:00\" FormaDePagoP=\"01\" MonedaP=\"MXN\" Monto=\"100.00\" TipoCambioP=\"1\"><pago20:DoctoRelacionado EquivalenciaDR=\"1\" IdDocumento=\"2185599b-d3a1-44c6-a9fa-8fd6e79bf1af\" ImpPagado=\"100.00\" ImpSaldoAnt=\"100.00\" ImpSaldoInsoluto=\"0.00\" MonedaDR=\"MXN\" NumParcialidad=\"1\" ObjetoImpDR=\"02\"><pago20:ImpuestosDR><pago20:TrasladosDR><pago20:TrasladoDR BaseDR=\"1.00\" ImpuestoDR=\"002\" TipoFactorDR=\"Exento\" /><pago20:TrasladoDR BaseDR=\"1.00\" ImporteDR=\"0.16\" ImpuestoDR=\"002\" TasaOCuotaDR=\"0.160000\" TipoFactorDR=\"Tasa\" /><pago20:TrasladoDR BaseDR=\"1.00\" ImpuestoDR=\"003\" TipoFactorDR=\"Exento\" /><pago20:TrasladoDR BaseDR=\"1.00\" ImporteDR=\"0.08\" ImpuestoDR=\"003\" TasaOCuotaDR=\"0.080000\" TipoFactorDR=\"Tasa\" /></pago20:TrasladosDR></pago20:ImpuestosDR></pago20:DoctoRelacionado><pago20:ImpuestosP><pago20:TrasladosP><pago20:TrasladoP BaseP=\"1.00\" ImpuestoP=\"002\" TipoFactorP=\"Exento\" /><pago20:TrasladoP BaseP=\"1.00\" ImporteP=\"0.16\" ImpuestoP=\"002\" TasaOCuotaP=\"0.160000\" TipoFactorP=\"Tasa\" /><pago20:TrasladoP BaseP=\"1.00\" ImpuestoP=\"003\" TipoFactorP=\"Exento\" /><pago20:TrasladoP BaseP=\"1.00\" ImporteP=\"0.08\" ImpuestoP=\"003\" TasaOCuotaP=\"0.080000\" TipoFactorP=\"Tasa\" /></pago20:TrasladosP></pago20:ImpuestosP></pago20:Pago></pago20:Pagos><tfd:TimbreFiscalDigital xsi:schemaLocation=\"http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd\" Version=\"1.1\" UUID=\"d0bdabe5-043c-42e7-952b-298b4e0196f5\" FechaTimbrado=\"2023-01-08T20:45:37\" RfcProvCertif=\"SPR190613I52\" SelloCFD=\"hWlCGI7ZL3rW9RIKcO8HY3cx552lCcnluxqq6V80BziB0D4oFkrUsLZ+rkno8ORSIjIROkuZPUXH/itMz6hyCD5k1hvnDlpAUOuwrmiqCe2susa54JgblN6TsueAvk3+4X38EJaI5sB/bcaMBy/2iYo8P/L0hSVl3WG9M+NvDaIgbXFGHF8gIIrhBpfXVd/2s07GhU8wx4YKyWC/2cr1AIUvEPmyokqUyTxuyVaG/XqiSThMO+aXmQDItfYNuk7gAN6ZjJNytAdsny30foaakpV5lZVwo3f2L+/WVbRXzCR8yw+xNpE1yatGH25RVMGZLxZB8rhsDEdbIQ1ZduYo9A==\" NoCertificadoSAT=\"30001000000400002495\" SelloSAT=\"dGLLqs1Vopy+dIHtz5PkBA/g7sPNNAzTg7TaqHuB6OnoNb8OjXUM/O/Io/+oNHpOcnlkrFdTM+QO87NjqzoTvpQLx4GMs5sx3Wc3eL7ZxyygNw0BLwEdvh+z+Ir1VcBShYE0WTL94HrDHtb7JW/zfSnFPTrt7IiEHYqSj0rWd7HIn9zJJMo70u2Qpw42xQy6U5XbxSksh0o5tIrQHDK9J74mSz7bZfsdYJf+3FYQvje5IwQKnEiwZcCez1Y9Z8wbY8iRQCEV0laNUaoza62LYZq/JE7HQNmyKPFkkOXLCQjE3FvbCiuK8g+iswH8jh2h3DiNJFTrqZM544fjC1rRwQ==\" xmlns:tfd=\"http://www.sat.gob.mx/TimbreFiscalDigital\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" /></cfdi:Complemento></cfdi:Comprobante>";
	}
	
	private String getOriginalString() {
		return "||1.1|2185599b-d3a1-44c6-a9fa-8fd6e79bf1af|2022-12-02T17:07:32|SPR190613I52|KU2kWX4xxHrzAL+MXjXi2KeMaEIo9g3+XvI2H9EVbxphEmEwtxXyX2cTBBYasumUggsmMsDCqz0K8TANrJmr5XmvQ4zIZE6+fuU3Jny3Gkk0YJ4VbL4FSlBkLVLrUo7sXF/F+R6kaYriQn7N0L7HHzET2ueQQqHRzT//A3xGNWJ6B3dxkehObHOpanJHlhGj7KVvnSNqIg7pdDmIU5FEoV+i3yawszVwfa38J505b8lNsCOK79GoePAQ2aYSSxZUVTZCqdWWMpRCn4WbA6+9vrwN0kCtEuWqoEKm5NqQhIY8YhaGfAGdcB1J0gB8FFTzyC+H7EAAtRTnDXR7+kWxAg==|30001000000400002495||";
	}
	
	private String getPaymentOriginalString() {
		return "||1.1|d0bdabe5-043c-42e7-952b-298b4e0196f5|2023-01-08T20:45:37|SPR190613I52|hWlCGI7ZL3rW9RIKcO8HY3cx552lCcnluxqq6V80BziB0D4oFkrUsLZ+rkno8ORSIjIROkuZPUXH/itMz6hyCD5k1hvnDlpAUOuwrmiqCe2susa54JgblN6TsueAvk3+4X38EJaI5sB/bcaMBy/2iYo8P/L0hSVl3WG9M+NvDaIgbXFGHF8gIIrhBpfXVd/2s07GhU8wx4YKyWC/2cr1AIUvEPmyokqUyTxuyVaG/XqiSThMO+aXmQDItfYNuk7gAN6ZjJNytAdsny30foaakpV5lZVwo3f2L+/WVbRXzCR8yw+xNpE1yatGH25RVMGZLxZB8rhsDEdbIQ1ZduYo9A==|30001000000400002495||";
	}
	
	private String getQrCode() {
		return "iVBORw0KGgoAAAANSUhEUgAAAIwAAACMCAYAAACuwEE+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAx/SURBVHhe7ZLRigQ7rgTP///03m3oABE4R3aVejlcHJAPmUpr1EP985/L5YD7wVyOuB/M5Yj7wVyOuB/M5Yj7wVyOuB/M5Yj7wVyOuB/M5Yj7wVyOuB/M5Yj7wVyOuB/M5Yj7wVyOuB/M5Yj7wVyOuB/M5YiRD+aff/55pITn9c0qh9pZ5ZC8c/Dc6ki90xyYn2qCkS2r43aU8Ly+WeVQO6sckncOnlsdqXeaA/NTTTCy5fSgrn+6z+y+p9cJ7E19cyKzO9/ltP8XI1umf8DpPrP7nl4nsDf1zYnM7nyX0/5fjGzxQXgLkncOaW5vun7ngXxXYA9Pe+Acb4H9G0a2pAMtSN45pLm96fqdB/JdgT087YFzvAX2bxjZkg60IPmUJ7p5h9/jnSdOe6d92PUW2L9hZEs60ILkU57o5h1+j3eeOO2d9mHXW2D/hpEt6UALOm9O50/7b5VIPedWwvP6pgrs3zCyJR1oQefN6fxp/60SqefcSnhe31SB/RtGtpwe5D7e6nCvvq05nObQzY37nQfyNIdubk77fzGy5e0PwFsd7tW3NYfTHLq5cb/zQJ7m0M3Naf8vRrZw0Kng+mf+VBOMbFkdtyO4/pk/1QQzW16y+nEfmTS3h9RLeaK+qYKnOdRO1b+Rf8VVq3/WRybN7SH1Up6ob6rgaQ61U/VvZOQq/8D6o/+SSfNd79ykeX27I7A39c2ql3Kob2vPedIvGNnqA+vRf8mk+a53btK8vt0R2Jv6ZtVLOdS3tec86Rf8ZusXH568Bcnv5uC8dleCLofa2cmNe8g4f9t7wsyWQHc43oLkd3NwXrsrQZdD7ezkxj1knL/tPWFmy5d0mHN8yqF2VjJpbp/oep4nnwQpN2nuHO/8F4xuTwc7x6ccamclk+b2ia7nefJJkHKT5s7xzn/Bb7d/ST/IuQWr2UeJNK9v63w331UizevbKuP81E8wuy3A4f4Bzi1YzT5KpHl9W+e7+a4SaV7fVhnnp36CkW3pMPKngtXso8RuL5Hep9y4h8AeUi/lkLw1wciWdFA99olgNfsosdtLpPcpN+4hsIfUSzkkb00wssUHPT2Qd0kw5ZOgy6epf+tkf/cu5U8Y2eKDnh7IuySY8knQ5dPUv3Wyv3uX8ifM/+r/0h2eBKvZR7CafWS6/HRub1I/5WAPqXeaTzK77Us6lDwJVrOPYDX7yHT56dzepH7KwR5S7zSfZGRbPbYeaG9SP+WJ1Hee6Pqed9olvbM39c1Ob5KRbfX4eqC9Sf2UJ1LfeaLre95pl/TO3tQ3O71JZrd9qT+mCp76XcGuTzl0HlIv5aZ269ze1DcrTTK77cvq6I/gqd8V7PqUQ+ch9VJuarfO7U19s9IkI9vSgfamm0PqkZ/OuxxqZyWTcvAcv5sb9xDYTzCyrR5bD7Q33RxSj/x03uVQOyuZlIPn+N3cuIfAfoKRbd1hzC1YzarAHnZ7wDz1dvOu12mX1duPTMonGdneHcrcgtWsCuxhtwfMU28373qddlm9/cikfJLfbhe7P4heEqxmVR2nfXC/7qjaZfX2LyW6+QS/3S52fxC9JFjNqjpO++B+3VG1y+rtX0p08wl+u13UH736Yc7f9oC5e8lb0HnDPPWcJ2+ZlEM3P2FmyyYcnn6A87c9YO5e8hZ03jBPPefJWybl0M1PmNnyxYftHpreWbu4X3fUHJx3Pc9T3pH6dVed2+/y9N2KmS1ffNjuoemdtYv7dUfNwXnX8zzlHalfd9W5/S5P362Y2fKFw5LA3qR+yjvq29rvvEn9XYE9pF7KwR5S/obRbRyYBPYm9VPeUd/WfudN6u8K7CH1Ug72kPI3jG7jQAuSdw4pT3R7LLA39U0V2EPKgbl7zi2w/18w+tfqj6qC5J1DyhPdHgvsTX1TBfaQcmDunnML7P8XjP619APIdwX2sJvjnYPnT9WxelNlVp0n+gWjW9Oh9UfsCOxhN8c7B8+fqmP1psqsOk/0C0a2pgPr8as5dPNE3f2XYDWrSnhe39Q88baPEqmX8jeMbEkH1WNXc+jmibr7L8FqVpXwvL6peeJtHyVSL+VvGNnig+qRVbDrU2663IIuh9qpuUm95JOM887/kpG/kn6ABbs+5abLLehyqJ2am9RLPsk47/wvGf0r6Yc4N+5NaZeu7/mufyuwN/VN7aX8DTNbvviwemzNjXtT2qXre77r3wrsTX1Teyl/w8yWLz6sHvtEYN+R3nd5J0i5cc8yae58V2D/hpktX9KhTwX2Hel9l3eClBv3LJPmzncF9m+Y2RI4PZy5lfA89VPPOXiOIPmUw653Ds5rt+bQzZ8wsyXgQ7vDmVsJz1M/9ZyD5wiSTznseufgvHZrDt38CSNbfBiC5C1YzT7aJb2zh66HT7npevbQ5WkOu703jGz1oQiSt2A1+2iX9M4euh4+5abr2UOXpzns9t4wstUH1qNrfkr3vv6N2jvNE+4lbyVW3ZXAHmq3zpN3/oaRLT6oHlnzU7r39W/U3mmecC95K7HqrgT2ULt1nrzzN8xs+eIDEaxmVbCafZRYdas6Ut+5ZVadlRJpXt+uZLr5G0a3+VAEq1kVrGYfJVbdqo7Ud26ZVWelRJrXtyuZbv6GkW3pMOedN8zde5tbp6R3zvEWrGYrwWpWBZ1/w8iWdJDzzhvm7r3NrVPSO+d4C1azlWA1q4LOv2Fmy5fdQ8mTYDWrgl3v3Lhn7dL1Pe/6kHrknttPMLpt92DyJFjNqmDXOzfuWbt0fc+7PqQeuef2E8xuE90PwHc5gtXsI+N8t5eg537KO/wOwWpWBfaQ8jfMbhM+OPkuR7CafWSc7/YS9NxPeYffIVjNqsAeUv6GkW0c5gPtIeXAPPU8R2bV+SixO7cg+ZSb2q3zLofaqZpkZFs60B5SDsxTz3NkVp2PErtzC5JPuandOu9yqJ2qSUa3rY79COzhNDfu4S1YzT6C1WwlOM07Tt+51/k3zGz5wmEW2MNpbtzDW7CafQSr2UpwmnecvnOv828Y2cJBp4LV7KMpvO+pT7mp3Sc6ZbWjapKRbasjdwSr2UdTeN9Tn3JTu090ympH1SQj29Jhpwd3fc/xKYfaqTLOn/bsgdzz5J2D89qt+S8Y2Z4OPf0BXd9zfMqhdqqM86c9eyD3PHnn4Lx2a/4LRrang3dzBPZQu1Wn+F3dtcpNlydBl+/y9v0TRrZzqA/ezRHYQ+1WneJ3ddcqN12eBF2+y9v3T/jt9i/8kPSDPEfQ5bvsvifv1HHasyDlkHLo5ifMbGng4HS45wi6fJfd9+SdOk57FqQcUg7d/ISRLRzkw5xbZtX5COxNfVMF9h11RxUk3+UokebO8c4h5W8Y2VaPrgc6t8yq8xHYm/qmCuw76o4qSL7LUSLNneOdQ8rfMLqtHn9yaOo7x1twmpvduXvJO0+4V9/WHDxHJuVvGN1Wjz85NPWd4y04zc3u3L3knSfcq29rDp4jk/I3zG4T3Q/x3H6Xumv1/nTeCToP5J4n3+VPNcHMlkA6tP6IOrffpe5avT+dd4LOA7nnyXf5U00ws+Ul3Q/zHMHTHOxN17c3zFPPee1WJVIv5W+Y2fKS7od5juBpDvam69sb5qnnvHarEqmX8jeMbPFhu+pI/S43tVvnXQ61s8pN18OnHGpnlYP9Lxn5Kxx8qo7U73JTu3Xe5VA7q9x0PXzKoXZWOdj/kpG/cnqw+/gkk3LT9Zif9hCsZh9BysF57VYlul7KnzCy5fQg9/FJJuWm6zE/7SFYzT6ClIPz2q1KdL2UP2Fkiw/CW5B8ysEeyC3T5btKeF7f1BxSDvXtE4H9G0a2pAMtSD7lYA/klunyXSU8r29qDimH+vaJwP4NI1vSgRbseiuRevamvqmCLu+ob6sg+U6nPH23YmSLD8JbsOutROrZm/qmCrq8o76tguQ7nfL03YqRLT4Ib0HyVod7px5SL+WJ+qbKpLzD7/AWpPwNI1t8UD2yCpK3Otw79ZB6KU/UN1Um5R1+h7cg5W8Y2XJ6UNdnbkGXm9RLOdROzWF3nqhva88eUs85pPwNI9tOD+v6zC3ocpN6KYfaqTnszhP1be3ZQ+o5h5S/YWRbPfpEsJqtZFKecB+fcpNyYO5el5/SvXu6d4eRrRx4KljNVjIpT7iPT7lJOTB3r8tP6d493bvDb7Ze/t9yP5jLEfeDuRxxP5jLEfeDuRxxP5jLEfeDuRxxP5jLEfeDuRxxP5jLEfeDuRxxP5jLEfeDuRxxP5jLEfeDuRxxP5jLEfeDuRzwn//8Hx+mFgrnAHbgAAAAAElFTkSuQmCC";
	}
	
	private String getPaymentQrCode() {
		return "iVBORw0KGgoAAAANSUhEUgAAAIwAAACMCAYAAACuwEE+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAyISURBVHhe7ZLRiu3IDkPP///03AlsgVhYcVXiDIdLLdCDZJXbu8mffw6HDc4Hc9jifDCHLc4Hc9jifDCHLc4Hc9jifDCHLc4Hc9jifDCHLc4Hc9jifDCHLc4Hc9jifDCHLc4Hc9jifDCHLc4Hc9jifDCHLUY+mD9//jxSgvPOC+WcT/mkjtTbzYXmu5pgZEt13IoSnHdeKOd8yid1pN5uLjTf1QQjW3YP6vpv962+V48SnSea74qszlfZ7d8xsmX6B7zdt/pePUp0nmi+K7I6X2W3f8fIFh4kT4nkmYs0f5qLzovUSxL04mlPMJenBP0bRrakAymRPHOR5k9z0XmRekmCXjztCebylKB/w8iWdCAlOk/SnHnnSZp378Rub7cvVj0l6N8wsiUdSInOkzRn3nmS5t07sdvb7YtVTwn6N4xsSQdSYtd37L5P/U6iml1KpB5zKsG5v3EJ+jeMbEkHUmLXd+y+T/1OoppdSqQecyrBub9xCfo3jGzZPYh9eeaE8+SZd3R933nXE+x1XihPc9HNyW7/jpEtb3+APHPCefLMO7q+77zrCfY6L5SnuejmZLd/x8gWHbQrcfwzv6sJRrZUx61IHP/M72qCmS0vqX6cS9CLlAvNUy/lwt+6xNNceMf1N/JXXFX9s1yCXqRcaJ56KRf+1iWe5sI7rr+Rkav4A/1H3ynB+Wqfvc4L5asSySeRlAt/6z3mSV8wspUH+tF3SnC+2mev80L5qkTySSTlwt96j3nSF4xsfXpo9465d+9EmHu3EmGe/Gqe6PrM3/aeMLJFB+0e1r1j7t07EeberUSYJ7+aJ7o+87e9J8xsATpwVSJ55qTrpZyw99QnCfpE6jGXZ/4Fn2znD+gkkmdOul7KCXtPfZKgT6Qec3nmXzCyPR2ccsE5JeiJv6l6nEsi5YS9pESa+1sXYb7rJxjZpsN4YMoF55SgJ/6m6nEuiZQT9pISae5vXYT5rp9gdFs6uMuTRDVzkW4uUm83J+wlkdV81VMTzGz5wcP82Ls8SVQzF+nmIvV2c8JeElnNVz01wcgWHtT5hHpdn70kkXyS6PJp/G+5Orre6p4VRrbwoM4n1Ov67CWJ5JNEl0/jf8vV0fVW96ww/6v/hQcmT4lqdkmknKT56jv26EmaM++8UJ4kunyS2W0/usPlKVHNLomUkzRffccePUlz5p0XypNEl08yss2P9QPpib+5U0fq0xN/4yJV55JIeUd6R0/8jfc6P8HINh3GA+mJv7lTR+rTE3/jIlXnkkh5R3pHT/yN9zo/wey2H93h8kmimt1J0IvUS7lIPuXCO1VOdnPBuTzzCWa3/eChySeJanYnQS9SL+Ui+ZQL71Q52c0F5/LMJxjZxgOlXaodLlJ1Lgl64d2VeRJJueBcfjcX3nEJ+glGtvmxrl2qHS5SdS4JeuHdlXkSSbngXH43F95xCfoJZrcF/Me4SJozl0TyzEWa05M0Zy7faRX20/uUT/Lt9h/6IRRJc+aSSJ65SHN6kubM5Tutwn56n/JJRrbzUPmUd/hbl6hmd0qs9gj7vsO1SvX2ToluPsHIdh4qn/IOf+sS1exOidUeYd93uFap3t4p0c0n+Hb7D/+xLsK88yLlQnP2kqdE54nmqcc8eYqkXHTzHWa2NOhgijDvvEi50Jy95CnReaJ56jFPniIpF918h5EtOoiHMZcIc+/eKcG5v/FcMO96nKe8I/V9l8+TZy66+RNGtqTDmEuEuXfvlODc33gumHc9zlPekfq+y+fJMxfd/AkzW37wQImkXHAuv5qT1Os8Sf1ViS4X3rnLpf+C0b9S/YhLJOWCc/nVnKRe50nqr0p0ufDOXS79F4z+lepHXBLJd0pw7m/ucknQJ/yt9+lFyoXm7DGnBD3p5k8Y3aYDKZF8pwTn/uYulwR9wt96n16kXGjOHnNK0JNu/oTRbTxQ/qkS3Vz4rqrPeSeR8gT7FKk6T/QFo1t5qB//RIluLnxX1ee8k0h5gn2KVJ0n+oKRrd2Bq/OuR/guSXR5wt9U6njblxKpl/I3jGzpDlqddz3Cd0miyxP+plLH276USL2Uv2Fmy490GPNdL5RzTk/8TdVj3vmEeuwnn0SYd/5LRv9KOpz5rhfKOacn/qbqMe98Qj32k08izDv/JZ/8Ff0AKlF1L3VUb1wi5SLlgvNV/1aCnvgb76X8DTNbAA+VElX3Ukf1xiVSLlIuOF/1byXoib/xXsrfMLPlRzqQeSdSdSqJanaJVJ07iZQT9iiS5sxXJejfMLPlhx/rBzLvRKpOJVHNLpGqcyeRcsIeRdKc+aoE/RtmtvzwY12CPuFvvc9cEvTCu5VIygXn8ikXyScR5t71XHTzJ8xs+cEDJUGf8LfeZy4JeuHdSiTlgnP5lIvkkwhz73ouuvkTRrbwsCRRzS6JanZJVDNXIs39rUt0Oel68ikn3q3mYrX3hpGtPDRJVLNLoppdEtXMlUhzf+sSXU66nnzKiXeruVjtvWFkKw+VVkl95vJUouq6SNW5JJKnRPKdBL3wrs+TZ/6GkS08TFol9ZnLU4mq6yJV55JInhLJdxL0wrs+T575G2a2/OCBElmdC++u5CLlq/juSiTNU07S3N9WIt38DaPbeKhEVufCuyu5SPkqvrsSSfOUkzT3t5VIN3/D7Dbw9mD/0b7nqd8VWZ0L73ouOE8S1cyV6OY7zGwJvD1U77nnqd8VWZ0L73ouOE8S1cyV6OY7zGz5wcPkO5E0Z06J5JkLziVB3+E77iToib+pesy96/kEo9t4oB99J5LmzCmRPHPBuSToO3zHnQQ98TdVj7l3PZ9gdhtYPdh/nCvBefIpF/Qk9TutUr29JJLfzSeZ3QZWD1aPSnCefMoFPUn9TqtUby+J5HfzSUa2+bErB6ae73CRqlNJpJxwnjxzwdy7VU68ezcXXS/N3zCybffA1PMdLlJ1KomUE86TZy6Ye7fKiXfv5qLrpfkbRrelQ1MudnOiXidRzS6J1VwSyTPvYL973/Xp3zCz5YcO44EpF7s5Ua+TqGaXxGouieSZd7Dfve/69G8Y2aKDVg9jz9+6SMo7+O6pZy6Ye/eJdql2uCYZ2bZ7IHv+1kVS3sF3Tz1zwdy7T7RLtcM1yey2H08PTn3m8pTYzQXzpz16oZzz5JkL5t71/As+2f70B6Q+c3lK7OaC+dMevVDOefLMBXPvev4Fn25PP2TXi908wb58ykmXJ4kuT3R9+i/4dLt+AH/Irhe7eYJ9+ZSTLk8SXZ7o+vRfMLo9Hayc8+Tf5gn20jvlnTq6Hufyq7no8jR/wsyWH+kwP9rnyb/NE+yld8o7dXQ9zuVXc9Hlaf6EkS3pMOaU6HJBL1bz1BOpT4nku5wiq7k88y8Z+SvpcOaU6HJBL1bz1BOpT4nku5wiq7k88y8Z/Su7P4D9JJFy0eVpTlLPd/g8eeYJ9vyt54JziXTzJ8xs+bF7IPtJIuWiy9OcpJ7v8HnyzBPs+VvPBecS6eZPmNkSSIf6j3gyJ+x3IlVnR4JeeNfn9CL1nmqS2W0gHew/5smcsN+JVJ0dCXrhXZ/Ti9R7qklmtz2EP8x/7J1E8pToPOn69ETz1GPuXVci9VL+hpktL+EP8h95J5E8JTpPuj490Tz1mHvXlUi9lL9hZAsPWxWpOpXEqqcI8+RTTrqefMqFd6pc0H/JyF/RwbsiVaeSWPUUYZ58yknXk0+58E6VC/ovGfkruwezL59EUp7o9qS5YE8SKRerc+FdV6LrpfwJI1t2D2JfPomkPNHtSXPBniRSLlbnwruuRNdL+RNGtvAgeUp0XqRel3ciVedOiarrIikX/vaJBP0bRrakAynReZF6Xd6JVJ07Jaqui6Rc+NsnEvRvGNmSDqTEqn+qVaq3l0SXk9SjRPKdBH1itbfCyBYeJE+JVf9Uq1RvL4kuJ6lHieQ7CfrEam+FkS08SJ4SyVMJzv3NijpSnz7hb6t+yju6fZyn/A0jW3iQH+kSyVMJzv3NijpSnz7hb6t+yju6fZyn/A0jW3YP6vqaUyTNUy7SnF54906kyylBL1KPuUj5G0a27R7W9TWnSJqnXKQ5vfDunUiXU4JepB5zkfI3jGzzo3ckqplLVLNKCc79TZUn0lw5512+i+9yCfpJRrb60TsS1cwlqlmlBOf+psoTaa6c8y7fxXe5BP0k32w9/N9yPpjDFueDOWxxPpjDFueDOWxxPpjDFueDOWxxPpjDFueDOWxxPpjDFueDOWxxPpjDFueDOWxxPpjDFueDOWxxPpjDFueDOWzwzz//Azd+N8zq/5N2AAAAAElFTkSuQmCC";
	}
	
	private String getSatSeal() {
		return "YRnKrNkFY+b8zEfhyLTXb7cD3QSwZ9jYGfdQGBETJnUxUrKuI4HK62Z0Yb7cxK8jymF/8Y7hGnsoYKeMvlLpCaeurIxYfb93H4Zu1pdC1YbbOWAMAoTHDPrBNaksZE11t8vLQiDOWKwSGrQ8CTR19PIVw0799/Gk7JvFwXBha8qXqfAIT6J3db5BfomdsuC7NavivpLz/8B8ba8Hm6Wqxc1VSPWEB6WOqNrcsA+w4+eYdTwT1tRK5RjxTQi4OwhoMTQKg2hxVQ4f/hOFceErA+PLrcidwnURjaH6T+YMzYEy02+H/NCuJASXooygqw8K+DAmSkAyDJXo0CSAyLA/9A==";
	}
	
	private String getPaymentSatSeal() {
		return "dGLLqs1Vopy+dIHtz5PkBA/g7sPNNAzTg7TaqHuB6OnoNb8OjXUM/O/Io/+oNHpOcnlkrFdTM+QO87NjqzoTvpQLx4GMs5sx3Wc3eL7ZxyygNw0BLwEdvh+z+Ir1VcBShYE0WTL94HrDHtb7JW/zfSnFPTrt7IiEHYqSj0rWd7HIn9zJJMo70u2Qpw42xQy6U5XbxSksh0o5tIrQHDK9J74mSz7bZfsdYJf+3FYQvje5IwQKnEiwZcCez1Y9Z8wbY8iRQCEV0laNUaoza62LYZq/JE7HQNmyKPFkkOXLCQjE3FvbCiuK8g+iswH8jh2h3DiNJFTrqZM544fjC1rRwQ==";
	}
	
	private String getEmitterSeal() {
		return "KU2kWX4xxHrzAL+MXjXi2KeMaEIo9g3+XvI2H9EVbxphEmEwtxXyX2cTBBYasumUggsmMsDCqz0K8TANrJmr5XmvQ4zIZE6+fuU3Jny3Gkk0YJ4VbL4FSlBkLVLrUo7sXF/F+R6kaYriQn7N0L7HHzET2ueQQqHRzT//A3xGNWJ6B3dxkehObHOpanJHlhGj7KVvnSNqIg7pdDmIU5FEoV+i3yawszVwfa38J505b8lNsCOK79GoePAQ2aYSSxZUVTZCqdWWMpRCn4WbA6+9vrwN0kCtEuWqoEKm5NqQhIY8YhaGfAGdcB1J0gB8FFTzyC+H7EAAtRTnDXR7+kWxAg==";
	}
	
	private String getPaymentEmitterSeal() {
		return "hWlCGI7ZL3rW9RIKcO8HY3cx552lCcnluxqq6V80BziB0D4oFkrUsLZ+rkno8ORSIjIROkuZPUXH/itMz6hyCD5k1hvnDlpAUOuwrmiqCe2susa54JgblN6TsueAvk3+4X38EJaI5sB/bcaMBy/2iYo8P/L0hSVl3WG9M+NvDaIgbXFGHF8gIIrhBpfXVd/2s07GhU8wx4YKyWC/2cr1AIUvEPmyokqUyTxuyVaG/XqiSThMO+aXmQDItfYNuk7gAN6ZjJNytAdsny30foaakpV5lZVwo3f2L+/WVbRXzCR8yw+xNpE1yatGH25RVMGZLxZB8rhsDEdbIQ1ZduYo9A==";
	}
	
}