package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherTypeResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;

public interface VoucherTypeValidator {
	
	VoucherTypeResponse getVoucherType(String voucherType);
	
	VoucherTypeResponse getVoucherType(JVoucher voucher, List<JError> errors);
	
}