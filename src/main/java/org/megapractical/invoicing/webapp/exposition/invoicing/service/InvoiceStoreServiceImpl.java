package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.io.File;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.InvoiceNotificationRequest;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.InvoiceStoreRequest;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@Scope("session")
public class InvoiceStoreServiceImpl implements InvoiceStoreService {
	
	@Autowired
	private AppSettings appSettings;
	
	@Autowired
	private DataStoreService dataStoreService;
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ReportManager reportManager;
	
	@Autowired
	private StampControlService stampControlService;
	
	@Autowired
	private InvoiceNotificationService invoiceNotificationService;
	
	@Autowired
	private AppLog appLog;
	
	@Autowired
	private TimelineLog timelineLog;
	
	@Override
	public boolean store(ContribuyenteEntity taxpayer, PreferenciasEntity preferences, ApiCfdi apiCfdi) {
		val complementCode = apiCfdi.getComplement() != null ? apiCfdi.getComplement().getPackageClass() : null;
		InvoiceStoreRequest storeRequest = InvoiceStoreRequest
											.builder()
											.taxpayer(taxpayer)
											.emitterRfc(userTools.getContribuyenteActive().getRfcActivo())
											.receiverRfc(apiCfdi.getReceiver().getRfc())
											.receiverName(apiCfdi.getReceiver().getNombreRazonSocial())
											.receiverEmail(apiCfdi.getReceiver().getCorreoElectronico())
											.uuid(apiCfdi.getVoucher().getTaxSheet())
											.apiCfdi(apiCfdi)
											.complementCode(complementCode)
											.preferences(preferences)
											.build();
		return store(storeRequest);
	}
	
	@Override
	public boolean store(InvoiceStoreRequest storeRequest) {
		val emitterRfc = storeRequest.getEmitterRfc();
		val receiverRfc = storeRequest.getReceiverRfc();
		val apiCfdi = storeRequest.getApiCfdi();
		val complementCode = storeRequest.getComplementCode();
		
		// Repository path
		val repositoryPath = appSettings.getPropertyValue("cfdi.repository");
		
		// Folder type
		val target = "emited";
		
		// XML/PDF name
		val fileName = storeRequest.getUuid();
		
		// Database path storage
		val dbPath = emitterRfc + File.separator + target + File.separator + receiverRfc;
		
		// Xml database path storage
		val xmlDbPath = dbPath + File.separator + fileName + ".xml";
		
		// Pdf database path storage
		val pdfDbPath = dbPath + File.separator + fileName + ".pdf";
		
		apiCfdi.setXmlDbPath(xmlDbPath);
		apiCfdi.setPdfDbPath(pdfDbPath);
		
		val cfdiStored = dataStoreService.cfdiStore(apiCfdi);
		if (!UValidator.isNullOrEmpty(cfdiStored)) {
			val filePath = repositoryPath + File.separator + emitterRfc + File.separator + target + File.separator + receiverRfc;
			val fileFolder = new File(filePath); 
			if (!fileFolder.exists()) {
				userTools.log("[INFO] CREATING STORE PATH...");
				fileFolder.mkdirs();
			}
			
			userTools.log("[INFO] STORING XML...");
			
			// Storing XML
			val xmlPath = filePath + File.separator + fileName + ".xml";
			UFile.writeFile(apiCfdi.getStampResponse().getXml(), xmlPath);
			
			// Generating and storing PDF
			val pdfPath = filePath + File.separator + fileName + ".pdf";
			generateAndStorePdf(apiCfdi, pdfPath, complementCode);
			
			// Stamp control
			stampControlService.stampControl(storeRequest.getTaxpayer());
			
			logAndTimeline(complementCode);
			
			val notificationRequest = InvoiceNotificationRequest
										.builder()
										.emitterRfc(emitterRfc)
										.receiverRfc(receiverRfc)
										.receiverName(storeRequest.getReceiverName())
										.receiverEmail(storeRequest.getReceiverEmail())
										.taxpayer(storeRequest.getTaxpayer())
										.uuid(storeRequest.getUuid())
										.xmlPath(xmlPath)
										.pdfPath(pdfPath)
										.cfdiStored(cfdiStored)
										.preferences(storeRequest.getPreferences())
										.build();
			invoiceNotificationService.sendNotification(notificationRequest);
			
			return true;
		}
		return false;
	}

	@Override
	public void generateAndStorePdf(ApiCfdi apiCfdi, String pdfPath, String complementCode) {
		if (UValidator.isNullOrEmpty(complementCode)) {
			reportManager.cfdiV4GeneratePdf(apiCfdi, pdfPath);
		} else {
			reportManager.cfdiV4PaymentV2GeneratePdf(apiCfdi, pdfPath);
		}
	}
	
	private void logAndTimeline(final String complementCode) {
		// Bitacora :: Registro de accion en bitacora
		val operationCode = UValidator.isNullOrEmpty(complementCode)
				? OperationCode.GENERACION_CFDIV33_SIN_COMPLEMENTO_MASTER_ID15
				: OperationCode.GENERACION_CFDIV33_CON_COMPLEMENTO_MASTER_ID16; 
		appLog.logOperation(operationCode);
		
		// Timeline :: Registro de accion
		val eventTypeCode = UValidator.isNullOrEmpty(complementCode)
				? EventTypeCode.GENERACION_CFDIV33_SIN_COMPLEMENTO_MASTER_ID4
				: EventTypeCode.GENERACION_CFDIV33_CON_COMPLEMENTO_MASTER_ID5;
		timelineLog.timelineLog(eventTypeCode);
	}

}