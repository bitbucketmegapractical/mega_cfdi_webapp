package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;
import org.megapractical.invoicing.dal.data.repository.ITaxRegimeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.TaxRegimeValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class TaxRegimeValidatorImpl implements TaxRegimeValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ITaxRegimeJpaRepository iTaxRegimeJpaRepository;
	
	@Override
	public TaxRegimeValidatorResponse validate(String taxRegime) {
		RegimenFiscalEntity taxRegimeEntity = null;
		var hasError = false;
		
		if (UValidator.isNullOrEmpty(taxRegime)) {
			hasError = true;
		} else {
			taxRegimeEntity = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalse(taxRegime);
			if (taxRegimeEntity == null) {
				hasError = true;
			}
		}
		
		return new TaxRegimeValidatorResponse(taxRegimeEntity, hasError);
	}
	
	@Override
	public TaxRegimeValidatorResponse validate(String taxRegime, String field) {
		RegimenFiscalEntity taxRegimeEntity = null;
		JError error = null;
		
		if (UValidator.isNullOrEmpty(taxRegime)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			// Validando que sea un valor de catalogo
			val taxRegimeCatalog = UCatalog.getCatalog(taxRegime);
			taxRegime = taxRegimeCatalog[0];
			val taxRegimeValue = taxRegimeCatalog[1];
			
			taxRegimeEntity = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalse(taxRegime);
			val taxRegimeByValue = iTaxRegimeJpaRepository.findByValorAndEliminadoFalse(taxRegimeValue);
			
			if (taxRegimeEntity == null || taxRegimeByValue == null) {
				val values = new String[]{"CFDI33130", "RegimenFiscal", "c_RegimenFiscal"};
				error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
			}
		}
		
		return new TaxRegimeValidatorResponse(taxRegimeEntity, error);
	}

	@Override
	public TaxRegimeValidatorResponse validate(String personType, String taxRegime, String field) {
		RegimenFiscalEntity taxRegimeEntity = null;
		JError error = null;
		
		// Validar que el régimen fiscal corresponda al tipo de persona
		if (personType.equalsIgnoreCase("persona física")) {
			taxRegimeEntity = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalseAndPersonaFisicaTrue(taxRegime);
		} else if (personType.equalsIgnoreCase("persona moral")) {
			taxRegimeEntity = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalseAndPersonaMoralTrue(taxRegime);
		}
		
		if (taxRegimeEntity == null) {
			error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33131").value(), userTools.getLocale());
		}
		
		return new TaxRegimeValidatorResponse(taxRegimeEntity, error);
	}

}