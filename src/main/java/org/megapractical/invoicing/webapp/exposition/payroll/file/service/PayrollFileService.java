package org.megapractical.invoicing.webapp.exposition.payroll.file.service;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEntity;
import org.megapractical.invoicing.webapp.util.FileStatus;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface PayrollFileService {
	
	ArchivoNominaEntity findOne(Long id, FileStatus status);
	
	void delete(Long id);
	
}