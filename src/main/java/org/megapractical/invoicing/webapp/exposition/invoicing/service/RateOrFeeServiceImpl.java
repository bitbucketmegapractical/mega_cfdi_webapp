package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.megapractical.invoicing.dal.bean.jpa.TasaCuotaEntity;
import org.megapractical.invoicing.dal.data.repository.IRateOrFeeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.RateOrFeeResponse;
import org.megapractical.invoicing.webapp.json.JRateOrFee;
import org.megapractical.invoicing.webapp.json.JRateOrFee.RateOrFee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class RateOrFeeServiceImpl implements RateOrFeeService {

	@Autowired
	private IRateOrFeeJpaRepository iRateOrFeeJpaRepository;

	@Override
	public JRateOrFee load() {
		val rateOrFee = new JRateOrFee();
		String tax = null;
		String factorType = null;
		String rangeType = null;

		val source = iRateOrFeeJpaRepository.findByEliminadoFalse();
		for (val item : source) {
			val taxResponse = getTax(item, tax);
			tax = taxResponse.getValue();
			
			val factorTypeResponse = getFactorType(item, factorType, taxResponse.isChange());
			factorType = factorTypeResponse.getValue();

			val rangeTypeResponse = getRangeType(item, rangeType, factorTypeResponse.isChange());
			rangeType = rangeTypeResponse.getValue();

			val change = taxResponse.isChange() && factorTypeResponse.isChange() && rangeTypeResponse.isChange();
			if (change) {
				val rateOrFeeList = iRateOrFeeJpaRepository
						.findByTipoImpuestoAndTipoFactorAndEliminadoFalseOrderByValorMaximo(item.getTipoImpuesto(),
								item.getTipoFactor());
				if (!rateOrFeeList.isEmpty()) {
					val obj = RateOrFee
								.builder()
								.taxType(tax)
								.factorType(factorType)
								.rangeType(rangeType)
								.transferred(item.getTraslado())
								.withheld(item.getRetencion())
								.build();
					setExpectedValues(item, obj, rangeType, rateOrFeeList);
					setRateOrFee(rateOrFee, obj);
				}
			}
		}
		return rateOrFee;
	}

	private RateOrFeeResponse getTax(TasaCuotaEntity item, String tax) {
		var change = false;

		val itemTax = String.format("%s (%s)", item.getTipoImpuesto().getCodigo(), item.getTipoImpuesto().getValor());
		if (tax == null || !tax.equals(itemTax)) {
			tax = itemTax;
			change = true;
		}

		return RateOrFeeResponse.builder().value(tax).change(change).build();
	}

	private RateOrFeeResponse getFactorType(TasaCuotaEntity item, String factorType, boolean change) {
		val itemFactorType = item.getTipoFactor().getValor();
		if (factorType == null || !factorType.equals(itemFactorType)) {
			factorType = itemFactorType;
			change = true;
		}

		return RateOrFeeResponse.builder().value(factorType).change(change).build();
	}

	private RateOrFeeResponse getRangeType(TasaCuotaEntity item, String rangeType, boolean change) {
		val itemRangeType = item.getTipoRango().getValor();
		if (rangeType == null || !rangeType.equals(itemRangeType)) {
			rangeType = itemRangeType;
			change = true;
		}

		return RateOrFeeResponse.builder().value(rangeType).change(change).build();
	}
	
	private void setExpectedValues(TasaCuotaEntity item, RateOrFee obj, String rangeType, List<TasaCuotaEntity> rateOrFeeList) {
		if (rangeType.equalsIgnoreCase("fijo")) {
			val expectedValues = rateOrFeeList.stream()
											  .map(TasaCuotaEntity::getValorMaximo)
											  .distinct()
											  .collect(Collectors.toList());
			obj.getExpectedValues().addAll(expectedValues);
		} else {
			obj.setMinValue(item.getValorMinimo());
			obj.setMaxValue(item.getValorMaximo());
		}
	}
	
	private void setRateOrFee(JRateOrFee rateOrFee, RateOrFee obj) {
		if (rateOrFee.getRateOrFees().isEmpty()) {
			rateOrFee.getRateOrFees().add(obj);
		} else {
			val add = rateOrFee.getRateOrFees().stream().filter(rateOrFeeFilter(obj)).findFirst().orElse(null);
			if (add != null) {
				rateOrFee.getRateOrFees().add(obj);
			}
		}
	}
	
	private static Predicate<RateOrFee> rateOrFeeFilter(RateOrFee obj) {
		return p -> !p.getFactorType().equals(obj.getFactorType()) || !p.getTaxType().equals(obj.getTaxType());
	}

}