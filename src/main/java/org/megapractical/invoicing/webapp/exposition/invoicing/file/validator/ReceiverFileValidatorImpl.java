package org.megapractical.invoicing.webapp.exposition.invoicing.file.validator;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiReceiver;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.PostalCodeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.TaxRegimeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverCfdiUseResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverResidencyResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverTaxRegimeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ValidationResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.cfdiuse.CfdiUseValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.residency.ResidencyValidator;
import org.megapractical.invoicing.webapp.json.JReceiver;
import org.megapractical.invoicing.webapp.util.RfcUtils;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class ReceiverFileValidatorImpl implements ReceiverFileValidator {
	
	private final TaxRegimeValidator taxRegimeValidator;
	private final PostalCodeValidator postalCodeValidator;
	private final CfdiUseValidator cfdiUseValidator;
	private final ResidencyValidator residencyValidator;
	
	public ReceiverFileValidatorImpl(TaxRegimeValidator taxRegimeValidator, 
									 PostalCodeValidator postalCodeValidator,
									 CfdiUseValidator cfdiUseValidator, 
									 ResidencyValidator residencyValidator) {
		this.taxRegimeValidator = taxRegimeValidator;
		this.postalCodeValidator = postalCodeValidator;
		this.cfdiUseValidator = cfdiUseValidator;
		this.residencyValidator = residencyValidator;
	}

	@Override
	public ApiReceiver getReceiver(JReceiver receiverData) {
		val rfcResponse = getRfc(receiverData);
		val rfcError = rfcResponse.isError();
		
		val cfdiUseResponse = getCfdiUse(receiverData);
		val cfdiUseError = cfdiUseResponse.isError();
		
		val residencyResponse = getResidency(receiverData);
		val residencyError = residencyResponse.isError();
		
		val idRegNumberResponse = getIdentityRegistrationNumber(receiverData);
		val idRegNumberError = idRegNumberResponse.isError();
		
		val taxRegimeResponse = getTaxRegime(receiverData);
		val taxRegimeError = taxRegimeResponse.isError();
		
		val fiscalResidenceResponse = getFiscalResidence(receiverData);
		val fiscalResidenceError = fiscalResidenceResponse.isError();
		
		if (!rfcError && !cfdiUseError && !residencyError && !idRegNumberError 
					  && !taxRegimeError && !fiscalResidenceError) {
			val receiverNameOrBusinessName = receiverData.getNameOrBusinessName();
			val receiverEmail = receiverData.getEmail();
			val receiverCurp = receiverData.getCurp();
			
			return ApiReceiver
					.builder()
					.rfc(UValue.stringValueUppercase(rfcResponse.getData()))
					.usoCFDI(cfdiUseResponse.getCfdiUse())
					.nombreRazonSocial(receiverNameOrBusinessName)
					.residenciaFiscal(residencyResponse.getResidency())
					.residente(residencyResponse.getResident())
					.numRegIdTrib(idRegNumberResponse.getData())
					.taxRegimeEntity(taxRegimeResponse.getTaxRegimeEntity())
					.taxRegime(taxRegimeResponse.getTaxRegimeEntity().getCodigo())
					.domicilioFiscal(fiscalResidenceResponse.getData())
					.correoElectronico(receiverEmail)
					.curp(receiverCurp)
					.build();
		}
		return null;
	}
	
	private final ValidationResponse getRfc(JReceiver receiver) {
		var rfcError = false;
		
		val rfc = receiver.getRfc();
		if (UValidator.isNullOrEmpty(rfc)) {
			rfcError = true;
		} else {
			if (rfc.length() < RfcUtils.RFC_MIN_LENGTH || rfc.length() > RfcUtils.RFC_MAX_LENGTH || !rfc.matches(RfcUtils.RFC_EXPRESSION)) {
				rfcError = true;
			}
		}
		return new ValidationResponse(rfc, rfcError);
	}
	
	private final ReceiverCfdiUseResponse getCfdiUse(JReceiver receiver) {
		var cfdiUse = receiver.getCfdiUse();
		return cfdiUseValidator.validate(cfdiUse, receiver.getRfc());
	}
	
	private final ReceiverResidencyResponse getResidency(JReceiver receiver) {
		return residencyValidator.validate(receiver.getResident(), receiver.getResidency());
	}
	
	private final ValidationResponse getIdentityRegistrationNumber(JReceiver receiver) {
		val identityRegistrationNumber = receiver.getIdentityRegistrationNumber();
		
		val resident = receiver.getResident();
		if (resident.equalsIgnoreCase("extranjero")) {
			if (UValidator.isNullOrEmpty(identityRegistrationNumber)) {
				return ValidationResponse.error();
			}
			return new ValidationResponse(identityRegistrationNumber, false);
		}
		return ValidationResponse.empty();
	}
	
	private final ReceiverTaxRegimeResponse getTaxRegime(JReceiver receiver) {
		val validatorResponse = taxRegimeValidator.validate(receiver.getTaxRegime());
		val taxRegimeEntity = validatorResponse.getTaxRegime();
		val taxRegimeError = validatorResponse.isHasError();
		
		return new ReceiverTaxRegimeResponse(taxRegimeEntity, taxRegimeError);
	}
	
	private ValidationResponse getFiscalResidence(JReceiver receiver) {
		var postalCode = receiver.getPostalCode();
		val validatorResponse = postalCodeValidator.validate(postalCode);
		val postalCodeEntity = validatorResponse.getPostalCode();
		var postalCodeError = validatorResponse.isHasError();
		
		String postalCodeVal = null;
		if (postalCodeEntity != null) {
			postalCodeVal = postalCodeEntity.getCodigo();
		}
		
		return new ValidationResponse(postalCodeVal, postalCodeError);
	}

}