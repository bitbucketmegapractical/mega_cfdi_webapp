package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import java.util.concurrent.Future;

import org.megapractical.invoicing.api.wrapper.ApiPaymentStamp;
import org.megapractical.invoicing.webapp.exposition.service.PaymentProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@EnableAsync
@Service
@Scope("session")
public class PaymentFileAsyncServiceImpl implements PaymentFileAsyncService {
	
	@Autowired
	private PaymentProcessingService paymentProcessingService;
	
	@Override
	@Async
	public Future<Boolean> processFileAsync(ApiPaymentStamp paymentStamp) {
		paymentProcessingService.paymentProcessing(paymentStamp);
		return new AsyncResult<>(Boolean.TRUE);
	}

}