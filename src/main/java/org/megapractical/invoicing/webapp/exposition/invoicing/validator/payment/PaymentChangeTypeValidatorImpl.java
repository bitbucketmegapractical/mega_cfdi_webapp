package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.math.BigDecimal;
import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentChangeTypeValidatorImpl implements PaymentChangeTypeValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Override
	public BigDecimal getChangeType(String changeType, MonedaEntity currency) {
		if (currency != null) {
			BigDecimal cmpChangeType = null;
			
			if (UValidator.isNullOrEmpty(changeType) || !UValidator.isDouble(changeType)) {
				return null;
			}
			
			cmpChangeType = UValue.bigDecimalStrict(changeType);
			if (!cmpChangeType.toString().matches(VoucherUtils.CHANGE_TYPE_EXPRESSION)) {
				return null;
			}
			
			return cmpChangeType;
		}
		return null;
	}

	@Override
	public BigDecimal getChangeType(String changeType, String field, MonedaEntity currency, List<JError> errors) {
		if (currency != null) {
			BigDecimal cmpChangeType = null;
			JError error = null;
			
			if (UValidator.isNullOrEmpty(changeType)) {
				error = JError.required(field, userTools.getLocale());
			} else if (!UValidator.isDouble(changeType)) {
				error = JError.error(field, "ERR1003", userTools.getLocale());
			} else {
				cmpChangeType = UValue.bigDecimalStrict(changeType);
				if (!cmpChangeType.toString().matches(VoucherUtils.CHANGE_TYPE_EXPRESSION)) {
					error = JError.errorSource(field, "CFDI33116", userTools.getLocale());
				}
			}
			
			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
			}
			return cmpChangeType;
		}
		return null;
	}

}