package org.megapractical.invoicing.webapp.exposition.service.nomenclator;

import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.megapractical.invoicing.dal.data.repository.IFileStatusJpaRepository;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class FileStatusServiceImpl implements FileStatusService {
	
	private final IFileStatusJpaRepository iFileStatusJpaRepository;
	
	public FileStatusServiceImpl(IFileStatusJpaRepository iFileStatusJpaRepository) {
		this.iFileStatusJpaRepository = iFileStatusJpaRepository;
	}

	@Override
	public EstadoArchivoEntity findByCode(FileStatus code) {
		return iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(code.value());
	}

}
