package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JError;

public interface PaymentOriginalStringValidator {
	
	String getOriginalString(String originalString, String stringType);
	
	String getOriginalString(String originalString, String field, String stringType, List<JError> errors);
	
}