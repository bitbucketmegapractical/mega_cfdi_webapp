package org.megapractical.invoicing.webapp.exposition.cancellation;

/**
 * @author Maikel Guerra Ferrer
 *
 */
final class CancellationConstants {
	
	private CancellationConstants() {		
	}
	
	public static final String CANCELLATION_PATH = "cfdi.cancellation.path";
	
	public static final String FILE_TXT_CODE = "CANTXT";
	
	public static final String NOTIF_SUBJECT = "Notificación de cancelación de comprobantes concluida";
	
}