package org.megapractical.invoicing.webapp.exposition.stamp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.CuentaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PerfilEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.ProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreCompraEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbrePaqueteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IProductServiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRoleJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampPackageJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JStampBuyTransfer;
import org.megapractical.invoicing.webapp.json.JStampPackage;
import org.megapractical.invoicing.webapp.json.JStampPackage.StampPackage;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.model.BillingPayActivateModel;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import lombok.val;

@Controller
@Scope("session")
public class StampManageController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	JdbcUserDetailsManager userDetailsManager;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Resource
	private IAccountJpaRepository iAccountJpaRepository;
	
	@Resource
	private IStampJpaRepository iStampJpaRepository;
	
	@Resource
	private IStampPackageJpaRepository iStampPackageJpaRepository;
	
	@Resource
	private ITaxpayerJpaRepository iTaxpayerJpaRepository;
	
	@Resource
	private IRoleJpaRepository iRoleJpaRepository;
	
	@Resource
	private ICurrencyJpaRepository iCurrencyJpaRepository;
	
	@Resource
	private IPersonTypeJpaRepository iPersonTypeJpaRepository;
	
	@Resource
	private IProductServiceJpaRepository iProductServiceJpaRepository;
	
	private BillingPayActivateModel billingPayActivateModel;
	
	private ContribuyenteEntity taxpayer;
	private TimbreEntity stamp;
	private TimbrePaqueteEntity stampPackage;
	private JStampPackage jStampPackage;
	
	private static final String ROL_CODE = "ROLE_MST";
	private static final String PRODUCT = "CFDI_PLUS";
	private static final String CURRENCY_CODE = "MXN";
	
	// Mensaje campo requerido
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	// Mensaje procesando...
	@ModelAttribute("processingRequest")
	public String processingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	// Mensaje cargando...
	@ModelAttribute("loadingRequest")
	public String loadingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@RequestMapping(value = "/cfdiMasterActivate", method = RequestMethod.GET)
	public String init(Model model) {
		try {
			
			initial(model);
			return "/invoicing/CfdiMasterActivate";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional
	private void initial(Model model){
		try {
			
			billingPayActivateModel = new BillingPayActivateModel();
			billingPayActivateModel.setRfcSource(new ArrayList<String>());
			billingPayActivateModel.setStampPackSource(new ArrayList<String>());
			
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive();
			billingPayActivateModel.setTaxpayerRfc(taxpayer.getRfc()); 
			
			List<CuentaEntity> taxpayerAccountSource = iAccountJpaRepository.findByContribuyenteAndEliminadoFalse(taxpayer);
			if(!taxpayerAccountSource.isEmpty()){
				for (CuentaEntity item : taxpayerAccountSource) {
					billingPayActivateModel.getRfcSource().add(item.getContribuyenteAsociado().getRfc());
				}
			}
			
			List<TimbrePaqueteEntity> stampPackageSource = iStampPackageJpaRepository.findAll();
			for (TimbrePaqueteEntity item : stampPackageSource) {
				billingPayActivateModel.getStampPackSource().add(item.getCantidad().toString());
			}
			
			model.addAttribute("activetab", "tab-stamp-buy");
			model.addAttribute("stampPackSource", billingPayActivateModel.getStampPackSource());
			model.addAttribute("rfcSource", billingPayActivateModel.getRfcSource());
			model.addAttribute("transferDisplay", userTools.isMaster());
			
			populateModel(model);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void populateModel(Model model){
		try {
			
			model.addAttribute("billingPayActivateModel", billingPayActivateModel);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/cfdiMasterActivate", params={"loadStampPackageInfo"})
	public @ResponseBody String stampPackageInfo(@RequestParam("amount") String amount, HttpServletRequest request){
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			Integer quantity = Integer.valueOf(amount);
			
			jStampPackage = new JStampPackage();
			stampPackage = new TimbrePaqueteEntity();
			stampPackage = iStampPackageJpaRepository.findByCantidad(quantity);
			
			if(stampPackage != null){
				StampPackage stampPackageObj = new StampPackage();
				stampPackageObj.setQuantity(quantity);
				stampPackageObj.setUnitPrice(UValue.doubleString(stampPackage.getPrecioUnitario()));
				stampPackageObj.setTaxPercent(UValue.doubleString(stampPackage.getIvaPorciento()));
				stampPackageObj.setTaxImport(UValue.doubleString(stampPackage.getImpuesto()));
				stampPackageObj.setSubtotal(UValue.doubleString(stampPackage.getPrecioUnitario()));
				stampPackageObj.setTotal(UValue.doubleString(stampPackage.getPrecioVenta()));
				stampPackageObj.setDescription(stampPackage.getDescripcion());
				jStampPackage.setStampPackage(stampPackageObj);
				response.setSuccess(Boolean.TRUE);
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setMessage(UProperties.getMessage("ERR0111", userTools.getLocale()));
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setError(Boolean.TRUE);
				response.setNotification(notification);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jStampPackage.setResponse(response);
		jsonInString = gson.toJson(jStampPackage);
		return jsonInString;
	}
	
	@RequestMapping(value="/cfdiMasterActivate", params={"stampPackageValidate"})
	public @ResponseBody String stampPackageValidate(@RequestParam("rfc") String rfc, HttpServletRequest request){
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			if(!rfc.equals(rfcActive)){
				error.setMessage(UProperties.getMessage("ERR0001", userTools.getLocale()));
				error.setField("rfc");
				errors.add(error);
			}else{
				if(jStampPackage != null){
					this.taxpayer = iTaxpayerJpaRepository.findByRfc(rfc);
					if(this.taxpayer != null){
						StampPackage stampPackageObj = new StampPackage();
						stampPackageObj.setRfcBuy(rfc);
						stampPackageObj.setNameBuy(this.taxpayer.getNombreRazonSocial());
						jStampPackage.setStampPackage(stampPackageObj);
						response.setSuccess(Boolean.TRUE);
					}
				}else{
					notification.setPageMessage(Boolean.TRUE);
					notification.setMessage(UProperties.getMessage("ERR0111", userTools.getLocale()));
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setError(Boolean.TRUE);
					response.setNotification(notification);
				}
			}
			
			if(!errors.isEmpty()){
				response.setError(Boolean.TRUE);
				response.setErrors(errors);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jStampPackage.setResponse(response);
		jsonInString = gson.toJson(jStampPackage);
        return jsonInString;
	}
	
	@RequestMapping(value="/cfdiMasterActivate", params={"stampPackageBuy"})
	public @ResponseBody String stampPackageBuy(HttpServletRequest request) throws IOException{
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer(); 
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			// NOTA IMPORTANTE! Aqui va la integracion con PayU
			
			if(stampPackage != null && this.taxpayer != null){
				userTools.log("[INFO] ACTIVATING STAMP PACKAGE...");
				
				// Cargando la informacion de los timbres de la cuenta RFC seleccionada
				stamp = iStampJpaRepository.findByContribuyente(this.taxpayer);
				if(stamp != null){
					stamp.setTotal(stamp.getTotal() + stampPackage.getCantidad());
					stamp.setTimbresDisponibles(stamp.getTimbresDisponibles() + stampPackage.getCantidad());
					persistenceDAO.update(stamp);
					
					userTools.log("[INFO] USER'S STAMP UPDATED");
				}else{
					stamp = new TimbreEntity();
					stamp.setContribuyente(this.taxpayer);
					stamp.setHabilitado(Boolean.TRUE);
					stamp.setTimbresConsumidos(0);
					stamp.setTimbresDisponibles(stampPackage.getCantidad());
					stamp.setTotal(stampPackage.getCantidad());
					persistenceDAO.persist(stamp);
					
					userTools.log("[INFO] USER'S STAMP CREATED");
				}
				
				// Registrando la informacion de la compra
				TimbreCompraEntity stampBuy = new TimbreCompraEntity();
				stampBuy.setContribuyente(this.taxpayer);
				stampBuy.setTimbrePaquete(stampPackage);
				stampBuy.setFecha(new Date());
				stampBuy.setHora(new Date());
				persistenceDAO.persist(stampBuy);
				
				userTools.log("[INFO] USER'S STAMP BUY REGISTERED");
				
				// Verificando si el usuario es Master
				val currentUser = userTools.getCurrentUser();
				String username = currentUser.getCorreoElectronico();
				if(!userTools.isMaster(username)){
					//##### Actualizando el producto activo 
					this.taxpayer.setProductoServicioActivo(PRODUCT);
					persistenceDAO.update(this.taxpayer);
					
					userTools.log("[INFO] USER'S ACTIVATED PRODUCT HAS CHANGED FROM 'MEGA-CFDI FREE' TO 'MEGA-CFDI PLUS'");
					
					//##### Actualizando el perfil del usuario a Master
					PerfilEntity profile = userTools.getProfile(this.taxpayer);
					profile.setRol(iRoleJpaRepository.findByCodigo(ROL_CODE));
					persistenceDAO.update(profile);
					
					userTools.log("[INFO] USER'S PROFILE UPDATED");
					
					//##### Registrando producto servicio
					ProductoServicioEntity productService = iProductServiceJpaRepository.findByCodigoAndHabilitadoTrue(PRODUCT);
					ContribuyenteProductoServicioEntity taxpayerProductService = new ContribuyenteProductoServicioEntity();
					taxpayerProductService.setContribuyente(this.taxpayer);
					taxpayerProductService.setProductoServicio(productService);
					taxpayerProductService.setHabilitado(Boolean.TRUE);
					persistenceDAO.persist(taxpayerProductService);
					
					userTools.log("[INFO] USER'S PRODUCT SERVICE CREATED");
					
					//##### Registrando preferencias
					PreferenciasEntity preferences = new PreferenciasEntity();
					preferences.setContribuyente(this.taxpayer);
					preferences.setMoneda(iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(CURRENCY_CODE));
					preferences.setEmisorXmlPdf(Boolean.FALSE);
					preferences.setEmisorXml(Boolean.FALSE);
					preferences.setEmisorPdf(Boolean.FALSE);
					preferences.setReceptorXmlPdf(Boolean.FALSE);
					preferences.setReceptorXml(Boolean.FALSE);
					preferences.setReceptorPdf(Boolean.FALSE);
					preferences.setContactoXmlPdf(Boolean.FALSE);
					preferences.setContactoXml(Boolean.FALSE);
					preferences.setContactoPdf(Boolean.FALSE);
					preferences.setRegistrarCliente(Boolean.TRUE);
					preferences.setActualizarCliente(Boolean.TRUE);
					preferences.setRegistrarConcepto(Boolean.TRUE);
					preferences.setActualizarConcepto(Boolean.TRUE);
					preferences.setRegistrarRegimenFiscal(Boolean.TRUE);
					preferences.setActualizarRegimenFiscal(Boolean.TRUE);
					preferences.setReutilizarFolioCancelado(Boolean.FALSE);
					preferences.setIncrementarFolioFinal(Boolean.FALSE);
					preferences.setRegistrarActualizarLugarExpedicion(Boolean.FALSE);
					preferences.setPrefacturaPdf(Boolean.FALSE);
					persistenceDAO.persist(preferences);
					
					userTools.log("[INFO] USER'S PREFERENCES CREATED");
					
					//##### Registrando tipo de persona
					Integer rfcLength = this.taxpayer.getRfc().length();
					String emitterType = rfcLength == 12 ? "PM" : (rfcLength == 13 ? "PF" : null); 
					TipoPersonaEntity emitterTypeEntity = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(emitterType);
					if(!UValidator.isNullOrEmpty(emitterTypeEntity)){
						ContribuyenteTipoPersonaEntity taxpayerPersonType = new ContribuyenteTipoPersonaEntity();
						taxpayerPersonType.setContribuyente(this.taxpayer);
						taxpayerPersonType.setTipoPersona(emitterTypeEntity);
						persistenceDAO.persist(taxpayerPersonType);
						
						userTools.log("[INFO] USER'S PERSON TYTPE CREATED");
					}
					
					// Actualizando authority del usuario
					val role = iRoleJpaRepository.findByCodigo(ROL_CODE);
					currentUser.setRol(role);
					persistenceDAO.update(currentUser);
					
					userTools.log("[INFO] USER'S SPRING AUTHORITIES UPDATED");
					
					// Recargando authority del usuario
					Authentication auth = SecurityContextHolder.getContext().getAuthentication();
					val authorities = new ArrayList<GrantedAuthority>();
					authorities.add(new SimpleGrantedAuthority(ROL_CODE));
					Authentication authReload = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), authorities);
					SecurityContextHolder.getContext().setAuthentication(authReload);
				}
				
				stampBuyTransfer.setNameBuy(this.taxpayer.getNombreRazonSocial());
				stampBuyTransfer.setRfcBuy(this.taxpayer.getRfcActivo());
				stampBuyTransfer.setDate(UDate.formattedSingleDate(stampBuy.getFecha()));
				stampBuyTransfer.setTime(UDate.formattedTime(stampBuy.getFecha()));
				
			}

			Toastr toastr = new Toastr();
			toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.buy.title", userTools.getLocale()));
			toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.buy.message", userTools.getLocale()));
			toastr.setType(Toastr.ToastrType.SUCCESS.value());
			
			notification.setToastrNotification(Boolean.TRUE);
			notification.setToastr(toastr);
			
			response.setNotification(notification);
			response.setSuccess(Boolean.TRUE);
			
		} catch (Exception e) {
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(UProperties.getMessage("ERR0070", userTools.getLocale()));
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
			
			e.printStackTrace();
		}
		
		jStampPackage.setResponse(response);
		jsonInString = gson.toJson(jStampPackage);
		return jsonInString;
	}
}