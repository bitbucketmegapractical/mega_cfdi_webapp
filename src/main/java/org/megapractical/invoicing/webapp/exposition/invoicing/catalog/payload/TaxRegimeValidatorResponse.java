package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaxRegimeValidatorResponse {
	private RegimenFiscalEntity taxRegime;
	private JError error;
	private boolean hasError;
	
	public TaxRegimeValidatorResponse(RegimenFiscalEntity taxRegime) {
		this.taxRegime = taxRegime;
		this.hasError = Boolean.FALSE;
	}
	
	public TaxRegimeValidatorResponse(RegimenFiscalEntity taxRegime, JError error) {
		this.taxRegime = taxRegime;
		this.error = error;
		this.hasError = error != null;
	}
	
	public TaxRegimeValidatorResponse(RegimenFiscalEntity taxRegime, boolean hasError) {
		this.taxRegime = taxRegime;
		this.hasError = hasError;
	}
	
}