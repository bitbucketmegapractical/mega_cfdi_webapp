package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.CurrencyValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.PostalCodeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.PostalCodeResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher.VoucherExportationValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher.VoucherTypeValidator;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.json.JVoucher.JVoucherResponse;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class VoucherPaymentFileServiceImpl implements VoucherPaymentFileService {
	
	private final PostalCodeValidator postalCodeValidator;
	private final VoucherExportationValidator voucherExportationValidator;
	private final VoucherTypeValidator voucherTypeValidator;
	private final CurrencyValidator currencyValidator;
	
	public VoucherPaymentFileServiceImpl(PostalCodeValidator postalCodeValidator,
										 VoucherExportationValidator voucherExportationValidator, 
										 VoucherTypeValidator voucherTypeValidator, 
										 CurrencyValidator currencyValidator) {
		this.postalCodeValidator = postalCodeValidator;
		this.voucherExportationValidator = voucherExportationValidator;
		this.voucherTypeValidator = voucherTypeValidator;
		this.currencyValidator = currencyValidator;
	}

	private static final String VOUCHER_CURRENCY = "XXX";
	
	@Override
	public JVoucherResponse populateVoucher(JVoucher voucher) {
		val voucherTypeResponse = voucherTypeValidator.getVoucherType(VoucherUtils.VOUCHER_TYPE_CODE_P);
		val voucherType = voucherTypeResponse.getVoucherType();
		val voucherTypeError = voucherTypeResponse.isError();
		
		val currencyResponse = currencyValidator.validate(VOUCHER_CURRENCY);
		val currency = currencyResponse.getCurrency();
		val currencyError = currencyResponse.isHasError();
		
		val expeditionPlaceResponse = getExpeditionPlace(voucher);
		val expeditionPlace = expeditionPlaceResponse.getPostalCode();
		val expeditionPlaceError = expeditionPlaceResponse.isError();
		
		val exportationResponse = voucherExportationValidator.getExportation(voucher.getExportation());
		val exportation = exportationResponse.getExportation();
		val exportationError = exportationResponse.isError();
		
		if (voucherTypeError || currencyError || expeditionPlaceError || exportationError) {
			return null;
		}
		
		val apiVoucher = new ApiVoucher();
		apiVoucher.setVoucherType(UCatalog.tipoDeComprobante(VoucherUtils.VOUCHER_TYPE_CODE_P));
		apiVoucher.setVersion(VoucherUtils.VOUCHER_VERSION_DEFAULT);
		apiVoucher.setSerie(voucher.getSerie());
		apiVoucher.setFolio(voucher.getFolio());
		apiVoucher.setDateTimeExpedition(UDate.formattedDate());
		apiVoucher.setCurrency(UCatalog.moneda(VOUCHER_CURRENCY));
		apiVoucher.setSubTotal(UValue.moneyFormat("0"));
		apiVoucher.setTotal(UValue.moneyFormat("0"));
		apiVoucher.setExportation(exportation.getCodigo());
		apiVoucher.setPostalCode(UValue.stringValue(expeditionPlace.getCodigo()));
		
		return JVoucherResponse
				.builder()
				.apiVoucher(apiVoucher)
				.voucherType(voucherType)
				.expeditionPlace(expeditionPlace)
				.currency(currency)
				.build();
	}
	
	private PostalCodeResponse getExpeditionPlace(JVoucher voucher) {
		val validatorResponse = postalCodeValidator.validate(voucher.getPostalCode());
		val postalCodeEntity = validatorResponse.getPostalCode();
		val hasError = validatorResponse.isHasError();
		
		return new PostalCodeResponse(postalCodeEntity, hasError);
	}

}