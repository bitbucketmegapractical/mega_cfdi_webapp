package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.webapp.exposition.cfdi.payload.FileDownload;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.FileDownload.FileDownloadData;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class CfdiDownloadService {

	private final CfdiService cfdiService;
	private final FileDownloadService fileDownloadService;
	
	public FileDownload download(String uuid, String fileType) {
		try {
			val cfdiEntity = cfdiService.findByUuid(uuid);
			if (cfdiEntity != null) {
				val downloadData = FileDownloadData
									.builder()
									.fileType(fileType)
									.storageType(cfdiEntity.getStorage())
									.xmlPath(cfdiEntity.getRutaXml())
									.pdfPath(cfdiEntity.getRutaPdf())
									.xmlFileNameFromS3(cfdiEntity.getXmlFileNameFromS3())
									.pdfFileNameFromS3(cfdiEntity.getPdfFileNameFromS3())
									.build();
				return fileDownloadService.download(downloadData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}