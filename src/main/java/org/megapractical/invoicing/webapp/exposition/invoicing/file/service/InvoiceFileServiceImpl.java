package org.megapractical.invoicing.webapp.exposition.invoicing.file.service;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.IInvoiceFileJpaRepository;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class InvoiceFileServiceImpl implements InvoiceFileService {

	private final IInvoiceFileJpaRepository iInvoiceFileJpaRepository;

	public InvoiceFileServiceImpl(IInvoiceFileJpaRepository iInvoiceFileJpaRepository) {
		this.iInvoiceFileJpaRepository = iInvoiceFileJpaRepository;
	}

	@Override
	public List<ArchivoFacturaEntity> findQueuedFiles(ContribuyenteEntity taxpayer) {
		return iInvoiceFileJpaRepository
				.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdAsc(taxpayer,
						FileStatus.QUEUED.value());
	}

	@Override
	public List<ArchivoFacturaEntity> findByTaxpayerAndStatus(ContribuyenteEntity taxpayer, FileStatus status) {
		return iInvoiceFileJpaRepository
				.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(taxpayer,
						status.value());
	}

	@Override
	public Page<ArchivoFacturaEntity> getFiles(ContribuyenteEntity taxpayer, String name, FileStatus status,
			String fileType, Pageable pageable) {
		return iInvoiceFileJpaRepository
				.findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivo_CodigoAndTipoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(
						taxpayer, name, status.value(), fileType, pageable);
	}

	@Override
	public List<ArchivoFacturaEntity> getFiles(ContribuyenteEntity taxpayer, FileStatus status, String fileType) {
		return iInvoiceFileJpaRepository
				.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndTipoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(
						taxpayer, status.value(), fileType);
	}

	@Override
	public ArchivoFacturaEntity findByTaxpayerAndName(ContribuyenteEntity taxpayer, String fileName) {
		return iInvoiceFileJpaRepository.findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(taxpayer, fileName);
	}

	@Transactional
	@Override
	public ArchivoFacturaEntity save(ArchivoFacturaEntity entity) {
		if (entity != null) {
			return iInvoiceFileJpaRepository.save(entity);
		}
		return null;
	}

	@Override
	public ArchivoFacturaEntity findOne(Long id, FileStatus status) {
		return iInvoiceFileJpaRepository.findByIdAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalse(id, status.value())
										.orElse(null);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		if (id != null) {
			iInvoiceFileJpaRepository.deleteById(id);
		}
	}

}