package org.megapractical.invoicing.webapp.exposition.account;

import java.io.IOException;
import java.math.BigInteger;
import java.security.Principal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.Contribuyente;
import org.megapractical.invoicing.dal.bean.Usuario;
import org.megapractical.invoicing.dal.bean.jpa.CodigoActivacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.CredencialEntity;
import org.megapractical.invoicing.dal.bean.jpa.CuentaEntity;
import org.megapractical.invoicing.dal.bean.jpa.LenguajeEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PerfilEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.RolEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.business.service.mapping.ContribuyenteServiceMapper;
import org.megapractical.invoicing.dal.business.service.mapping.UsuarioServiceMapper;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IActivationCodeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPreferencesJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRoleJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IUserJpaRepository;
import org.megapractical.invoicing.webapp.json.JAccount;
import org.megapractical.invoicing.webapp.json.JAccount.Account;
import org.megapractical.invoicing.webapp.json.JAccountRegister;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JUser;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.model.AccountViewModel;
import org.megapractical.invoicing.webapp.model.TaxpayerViewModel;
import org.megapractical.invoicing.webapp.model.UserViewModel;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.Layout;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.google.gson.Gson;

import lombok.val;

@Controller
public class AccountController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog; 
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	SpringTemplateEngine thymeleaf;

	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	SessionController sessionController; 
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Resource
	private IUserJpaRepository iUserJpaRepository;
	
	@Resource
	private ITaxpayerJpaRepository iTaxpayerJpaRepository;
	
	@Resource
	private IRoleJpaRepository iRoleJpaRepository;
	
	@Resource
	private ITaxpayerTypeJpaRepository iTaxpayerTypeJpaRepository;
	
	@Resource
	private IActivationCodeJpaRepository iActivationCodeJpaRepository;
	
	@Resource
	private UsuarioServiceMapper userServiceMapper;
	
	@Resource
	private ContribuyenteServiceMapper taxpayerServiceMapper;
	
	@Resource
	private IPersonTypeJpaRepository iPersonTypeJpaRepository;
	
	@Resource
	private IAccountJpaRepository iAccountJpaRepository;
	
	@Resource
	private IPreferencesJpaRepository iPreferencesTypeJpaRepository;
	
	@Resource
	private ICurrencyJpaRepository iCurrencyTypeJpaRepository;
	
	private static final String ROLE_CODE_DEFAULT = "ROLE_USR";
	private static final String ROLE_CODE_MASTER = "ROLE_MST";
	
	private static final String TAXPAYER_TYPE_DEFAULT = "EMI";
	private static final String SERVICE_PRODUCT_DEFAULT = "CFDI_FREE";
	
	private static final String RFC_EXPRESSION = "^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$";
	private static final Integer RFC_MIN_LENGTH = 12;
	private static final Integer RFC_MAX_LENGTH = 13;
	
	private static final String PERSON_TYPE_PHYSICAL_CODE = "PF";
	private static final String PERSON_TYPE_MORAL_CODE = "PM";
	
	private static final int FREE_PLAN_DAYS = 10;
	
	// Mensaje campo requerido
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	// Mensaje procesando...
	@ModelAttribute("processingRequest")
	public String processingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	// Las claves no coinciden
	@ModelAttribute("passwdNotMatch")
	public String passwdNotMatch() throws IOException{
		return UProperties.getMessage("ERR0059", userTools.getLocale());
	}
	
	// La contraseña no cumple con el formato establecido
	@ModelAttribute("passwdInvalidRegex")
	public String passwdInvalidRegex() throws IOException{
		return UProperties.getMessage("ERR0060", userTools.getLocale());
	}
	
	// Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() throws IOException{
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	// Error inesperado
	@ModelAttribute("unexpectedError")
	public String unexpectedError() throws IOException{
		return UProperties.getMessage("ERR0068", userTools.getLocale());
	}
	
	
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/registerAccount", method = RequestMethod.GET)
	public String register(Model model) {
		return "/register/RegisterAccount";
	}
	
	@Transactional
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/registerAccount", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
	public @ResponseBody String processRegistration(Model model, HttpServletRequest request) {
		
		String jsonInString = null;
		Gson gson = new Gson();
		JAccountRegister accountRegister = new JAccountRegister();
		JResponse response = new JResponse();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		Boolean errorMarked = false;
		
		try {
			
			String objAccountRegister = request.getParameter("objAccountRegister");
			accountRegister = gson.fromJson(objAccountRegister, JAccountRegister.class);
			
			//##### Nombre o razon social
			String nameOrBusinessName = UBase64.base64Decode(accountRegister.getNameOrBusinessName());
			
			//##### RFC
			String rfc = UBase64.base64Decode(accountRegister.getRfc());
			
			if(UValidator.isNullOrEmpty(rfc)){
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errorMarked = true;
			}else{
				if(rfc.toString().length() < RFC_MIN_LENGTH || rfc.toString().length() > RFC_MAX_LENGTH || !rfc.toString().matches(RFC_EXPRESSION)){
					error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
					errorMarked = true;
				}else{
					String rfcUpperCase = rfc.toUpperCase();
					ContribuyenteEntity contribuyenteByRfc = iTaxpayerJpaRepository.findByRfc(rfcUpperCase);
					if(contribuyenteByRfc != null){
						error.setMessage(UProperties.getMessage("ERR0061", userTools.getLocale()));
						errorMarked = true;
					}
				}
			}			
			
			
			if(errorMarked){
				error.setField("rfc");
				errors.add(error);
			}
			
			//##### Correo
			String email = UBase64.base64Decode(accountRegister.getEmail());
			error = new JError();
			errorMarked = false;
			
			if(UValidator.isNullOrEmpty(email)){
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errorMarked = true;
			}else{
				UsuarioEntity userByEmail = iUserJpaRepository.findByCorreoElectronico(email);
				if (userByEmail != null) {
					error.setMessage(UProperties.getMessage("ERR0062", userTools.getLocale()));
					errorMarked = true;
				}
			}
			if(errorMarked){
				error.setField("email");
				errors.add(error);
			}
			
			//##### Telefono
			String phone = UBase64.base64Decode(accountRegister.getPhone());
			error = new JError();
			errorMarked = false;
			if(UValidator.isNullOrEmpty(phone)){
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errorMarked = true;
			}
			if(errorMarked){
				error.setField("phone");
				errors.add(error);
			}
			
			//##### Contraseña/Repetir contraseña
			String passwd = UBase64.base64Decode(accountRegister.getPasswd());
			String passwdRepeat = UBase64.base64Decode(accountRegister.getPasswdRepeat());
			error = new JError();
			errorMarked = false;
			
			if(UValidator.isNullOrEmpty(passwd)){
				error.setField("passwd-repeat");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errorMarked = true;
			}
			
			if(UValidator.isNullOrEmpty(passwdRepeat)){
				error.setField("passwd-repeat");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errorMarked = true;
			}
			
			if(errorMarked){
				errors.add(error);
			}else{
				if (!passwd.equals(passwdRepeat)) {
					error.setField("passwd-repeat");
					error.setMessage(UProperties.getMessage("ERR0059", userTools.getLocale()));
					errors.add(error);
				}
			}
			
			if(!errors.isEmpty()){
				response.setErrors(errors);
				response.setError(Boolean.TRUE);
			}else{				
				userTools.log("[INFO] CREATING NEW ACCOUNT...");
				
				val role = iRoleJpaRepository.findByCodigo(ROLE_CODE_DEFAULT);
				val passwordCrypt = new BCryptPasswordEncoder(12).encode(passwd);
				//##### Password hash
				String hash = BCrypt.hashpw(passwd, BCrypt.gensalt());
				
				//##### Usuario :: Registro
				UsuarioEntity user = new UsuarioEntity();
				user.setCorreoElectronico(email);
				user.setClave(passwordCrypt);
				user.setHash(hash);
				user.setRol(role);
				user.setGeneradoPorSistema(Boolean.TRUE);
				user.setActivo(false);
				user = (UsuarioEntity) persistenceDAO.persist(user);
				
				userTools.log("[INFO] REGISTERING USER...");
				
				//##### Contribuyente :: Registro
				String businessNameUpperCase = nameOrBusinessName.toUpperCase();
				String rfcUpperCase = rfc.toUpperCase();
				ContribuyenteEntity taxpayer = new ContribuyenteEntity();
				Date today = Calendar.getInstance().getTime();
				Calendar finalDay = Calendar.getInstance();
				finalDay.setTime(today);
				finalDay.add(Calendar.DATE, FREE_PLAN_DAYS);
				taxpayer.setNombreRazonSocial(businessNameUpperCase);
				taxpayer.setRfc(rfcUpperCase);
				taxpayer.setRfcActivo(rfcUpperCase);
				taxpayer.setTelefono(phone);
				taxpayer.setProductoServicioActivo(SERVICE_PRODUCT_DEFAULT);
				taxpayer.setCodigoSeguridad(UBase64.base64Encode(UValue.stringValueUppercase(UValue.uuid().replaceAll("-", ""))));
				taxpayer.setFechaInicioPlan(today);
				taxpayer.setFechaFinPlan(finalDay.getTime());
				taxpayer = (ContribuyenteEntity) persistenceDAO.persist(taxpayer);
				
				userTools.log("[INFO] REGISTERING TAXPAYER...");
				
				//##### Tipo persona :: Registro
				TipoPersonaEntity personType = null;
				if(rfcUpperCase.length() == RFC_MIN_LENGTH){
					personType = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(PERSON_TYPE_MORAL_CODE);
				}else if(rfcUpperCase.length() == RFC_MAX_LENGTH){
					personType = iPersonTypeJpaRepository.findByCodigoAndEliminadoFalse(PERSON_TYPE_PHYSICAL_CODE);
				}
				ContribuyenteTipoPersonaEntity taxpayerPersonType = new ContribuyenteTipoPersonaEntity();
				taxpayerPersonType.setContribuyente(taxpayer);
				taxpayerPersonType.setTipoPersona(personType);
				persistenceDAO.persist(taxpayerPersonType);
				
				userTools.log("[INFO] REGISTERING PERSON TYPE...");
				
				//##### Perfil :: Registro
				TipoContribuyenteEntity taxpayerType = iTaxpayerTypeJpaRepository.findByCodigo(TAXPAYER_TYPE_DEFAULT);
				PerfilEntity profile = new PerfilEntity();
				profile.setContribuyente(taxpayer);
				profile.setRol(role);
				profile.setTipoContribuyente(taxpayerType);
				persistenceDAO.persist(profile);
				
				userTools.log("[INFO] CREATING PROFILE...");
				
				//##### Credencial :: Registro
				LenguajeEntity language = userTools.getLocaleDefault();
				CredencialEntity credential = new CredencialEntity();
				credential.setContribuyente(taxpayer);
				credential.setUsuario(user);
				credential.setLenguaje(language);
				credential.setEliminado(Boolean.FALSE);
				persistenceDAO.persist(credential);							
				
				userTools.log("[INFO] CREATING CREDENTIAL...");
				
				//TODO: Solucion temporal cambiar al registrar por fechas
				//##### Preferencias :: Registro
				TimbreEntity timbre = new TimbreEntity();								
				
				timbre.setContribuyente(taxpayer);
				timbre.setTimbresConsumidos(0);
				timbre.setTimbresDisponibles(0);
				timbre.setTotal(0);				
				timbre.setHabilitado(true);
				timbre.setTimbresTransferidos(0);
				persistenceDAO.persist(timbre);
				
				userTools.log("[INFO] REGISTERING STAMPS...");
				
				//##### Preferencias :: Registro
				PreferenciasEntity preferences = new PreferenciasEntity();				
				MonedaEntity moneda = iCurrencyTypeJpaRepository.findByCodigoAndEliminadoFalse("MXN");
				preferences.setEmisorXml(false);
				preferences.setEmisorXmlPdf(false);
				preferences.setEmisorPdf(false);
				preferences.setReceptorXml(false);
				preferences.setReceptorXmlPdf(false);
				preferences.setReceptorPdf(false);
				preferences.setContactoXmlPdf(false);
				preferences.setContactoXml(false);
				preferences.setContactoPdf(false);
				preferences.setRegistrarCliente(true);
				preferences.setActualizarCliente(true);				
				preferences.setRegistrarConcepto(true);
				preferences.setActualizarConcepto(true);
				preferences.setRegistrarRegimenFiscal(true);
				preferences.setActualizarRegimenFiscal(true);
				preferences.setReutilizarFolioCancelado(false);
				preferences.setIncrementarFolioFinal(false);
				preferences.setRegistrarActualizarLugarExpedicion(false);
				preferences.setPrefacturaPdf(false);
				preferences.setContribuyente(taxpayer);
				preferences.setMoneda(moneda);
				
				persistenceDAO.persist(preferences);
				
				userTools.log("[INFO] REGISTERING PREFERENCES...");
				
				//##### Codigo de activacion :: Generando codigo de activacion para la cuenta del usuario
				val activationCode = this.generarCodigoActivacion(user);
				String codigoActivacion = null;
				if (activationCode != null) {
					activationCode.getCodigo();
				}
				
				//##### Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.REGISTRO_DE_CUENTA_DE_USUARIO_ID1, user);
				
				userTools.log("[INFO] REGISTERING LOG OPERATION...");
				
				//##### Email :: Enviando correo de confirmacion
				String to = nameOrBusinessName.toUpperCase();
				boolean emailSended;
				try {
					
					userTools.log("[INFO] PREPARING TO SEND EMAIL...");
					
					emailSended = mailService.sendAccountCreationConfirmationMail(email, to, codigoActivacion);
					
				} catch (Exception e) {
					emailSended = Boolean.FALSE;
					
					userTools.log("[ERROR] CREATING NEW ACCOUNT > EMAIL WASN'T SENDED");
				}
				
				//##### Actualizando codigo de activacion
				activationCode.setEnviado(emailSended);
				entityManager.merge(activationCode);
				entityManager.flush();
				
				response.setSuccess(Boolean.TRUE);
			}
			
			accountRegister.setResponse(response);
			
			userTools.log("[INFO] ACCOUNT CREATED SUCCESSFULY");
			
		} catch (Exception e) {
			userTools.log("[ERROR] UNEXPECTED ERROR ATTEMPTING TO CREATE A NEW ACCOUNT");
			e.printStackTrace();
		}
		
		jsonInString = gson.toJson(accountRegister);
		return jsonInString;
	}
	
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/registerSuccessfully", method = RequestMethod.POST)
	public String registerSuccessfully(HttpServletRequest request) {
		return "/register/RegisterSuccessfully";
	}
	
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/registerSuccessfully", method = RequestMethod.GET)
	public String registerSuccessfully(Model model, HttpServletRequest request) {
		return "/login/login";
	}

	@Transactional
	private CodigoActivacionEntity generarCodigoActivacion(UsuarioEntity usuarioEntity) {
		try {
			
			userTools.log("[INFO] CREATING ACTIVATION CODE...");
			
			SecureRandom random = new SecureRandom();
			String code = new BigInteger(130, random).toString(32);
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DATE, 8);

			CodigoActivacionEntity codigo = new CodigoActivacionEntity();
			codigo.setCodigo(code);
			codigo.setFechaGeneracion(new Date());
			codigo.setFechaExpiracion(calendar.getTime());
			codigo.setUsuario(usuarioEntity);
			codigo.setActivo(Boolean.TRUE);
			codigo.setCodigoActivado(Boolean.FALSE);

			entityManager.persist(codigo);

			// Bitacora :: Registro de accion en bitacora
			appLog.logOperation(OperationCode.GENERACION_CODIGO_ACTIVACION_ID64, usuarioEntity);
			
			return codigo;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/confirmAccount", method = RequestMethod.GET)
	public String confirm(String code, Model model) throws IOException {
		try {
			
			Boolean error = false;
			CodigoActivacionEntity codigo = iActivationCodeJpaRepository.findByCodigo(code);
			
			if(UValidator.isNullOrEmpty(code)){
				return "/login/login";
			}else if (UValidator.isNullOrEmpty(codigo)) {
				model.addAttribute("message", UProperties.getMessage("ERR0063", userTools.getLocale()));
				error = true;
			} else if (codigo.getFechaExpiracion().before(new Date())) {
				codigo.setActivo(Boolean.FALSE);
				codigo.setCodigoActivado(Boolean.FALSE);
				
				entityManager.merge(codigo);
				entityManager.flush();
				
				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.DESHABILITAR_CODIGO_ACTIVACION_ID65, codigo.getUsuario());
				
				model.addAttribute("message", UProperties.getMessage("ERR0064", userTools.getLocale()));
				error = true;
			} else {
				UsuarioEntity usuarioEntity = codigo.getUsuario();
				if(usuarioEntity.isActivo()){
					model.addAttribute("message", UProperties.getMessage("ERR0065", userTools.getLocale()));
					error = true;
				}else{
					userTools.log("[INFO] ACTIVATING ACCOUNT...");
					
					usuarioEntity.setActivo(true);
					entityManager.persist(usuarioEntity);
					
					codigo.setActivo(Boolean.FALSE);
					codigo.setCodigoActivado(Boolean.TRUE);
					codigo.setFechaActivacion(new Date());
					entityManager.persist(codigo);
					
					entityManager.flush();
					
					// Bitacora :: Registro de accion en bitacora
					appLog.logOperation(OperationCode.ACTIVACION_CUENTA_USUARIO_ID66, usuarioEntity);
					
					userTools.log("[INFO] ACCOUNT ACTIVATED SUCCESSFULY");
				}
			}
			
			if(!error){
				model.addAttribute("message", UProperties.getMessage("mega.cfdi.notification.account.activated", userTools.getLocale()));
			}
			
		} catch (Exception e) {
			model.addAttribute("message", UProperties.getMessage("ERR0066", userTools.getLocale()));
		}
		
		return "/register/AccountConfirmation";
	}
	
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
	public String forgotPassword(Model model) {
		return "/recovery/RecoveryPassword";
	}
	
	@Transactional
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
	public @ResponseBody String recoveryPassword(Model model, HttpServletRequest request) {
		
		String jsonInString = null;
		Gson gson = new Gson();
		JUser jUser = new JUser();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		boolean errorMarked = false;
		
		try {
			
			String objUser = request.getParameter("objUser");
			jUser = gson.fromJson(objUser, JUser.class);
			
			// RFC
			String rfc = UBase64.base64Decode(jUser.getRfc());
			ContribuyenteEntity taxpayer = new ContribuyenteEntity();
			
			if(UValidator.isNullOrEmpty(rfc)){
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				error.setField("rfc");
				errors.add(error);
			}else{
				taxpayer = userTools.getContribuyenteActive(rfc);
				if(UValidator.isNullOrEmpty(taxpayer)){
					errorMarked = true;
				}
			}
			
			// Correo
			String email = UBase64.base64Decode(jUser.getEmail());
			UsuarioEntity user = new UsuarioEntity();
			error = new JError();
			
			if(UValidator.isNullOrEmpty(email)){
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				error.setField("email");
				errors.add(error);
			}else{
				user = userTools.getUserByEmail(email);
				if(UValidator.isNullOrEmpty(user)){
					errorMarked = true;
				}
			}
			
			// Credencial
			val credential = userTools.getCredentialByUserByTaxpayer(user, taxpayer);
			if(UValidator.isNullOrEmpty(credential)){
				errorMarked = true;
			}
			
			if(!errors.isEmpty()){
				response.setErrors(errors);
				response.setError(Boolean.TRUE);
			}else if(errorMarked){
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				notification.setMessage(UProperties.getMessage("ERR1007", userTools.getLocale()));
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}else{
				userTools.log("[INFO] GENERATING NEW PASSWORD...");
				
				//String passwdOld = user.getHash();
				val passwdNew = UUID.randomUUID().toString().toLowerCase().replace("-", "").replace("a", "@")
						.replace("i", "$").replace("o", "*").replace("3", "#");
				String passwdCrypt = new BCryptPasswordEncoder(12).encode(passwdNew);
				// Password hash
				String hash = BCrypt.hashpw(passwdNew, BCrypt.gensalt());
				
				user.setClave(passwdCrypt);
				user.setHash(hash);
				user = entityManager.merge(user);
				entityManager.persist(user);
				entityManager.flush();
				
				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.SOLICITUD_CAMBIO_CLAVE_ID37, user);
				
				// Timeline :: Registro de accion
				timelineLog.timelineLog(EventTypeCode.SOLICITUD_CAMBIO_CLAVE_ID15);
				
				userTools.log("[INFO] PREPARING TO SEND EMAIL...");
				
				// Email :: Enviando correo de confirmacion
				String to = taxpayer.getNombreRazonSocial().toUpperCase();
				mailService.sendRecoveryPassword(email, to, passwdNew);
				
				response.setSuccess(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			response.setUnexpectedError(Boolean.TRUE);
			e.printStackTrace();
		}

		jUser.setResponse(response);
		jsonInString = gson.toJson(jUser);
		return jsonInString;
	}
	
	@RequestMapping(value = "/accountInfo", method = RequestMethod.GET)
	public String userInfo(Principal principal, Model model) {
		try {
			
			UsuarioEntity usuarioEntity = userTools.getCurrentUser();
			Usuario user = userServiceMapper.mapUsuarioEntityToUsuario(usuarioEntity);
			UserViewModel usuarioViewModel = new UserViewModel(user);
			
			ContribuyenteEntity contribuyenteEntity = userTools.getContribuyenteActive(usuarioEntity);
			Contribuyente contribuyente = taxpayerServiceMapper.mapContribuyenteEntityToContribuyente(contribuyenteEntity);
			TaxpayerViewModel contribuyenteViewModel = new TaxpayerViewModel(contribuyente);
			
			AccountViewModel accountViewModel = new AccountViewModel();
			accountViewModel.setUsuarioViewModel(usuarioViewModel);
			accountViewModel.setContribuyenteViewModel(contribuyenteViewModel);
			
			model.addAttribute("accountViewModel", accountViewModel);
			model.addAttribute("securityCode", UBase64.base64Decode(contribuyenteEntity.getCodigoSeguridad()));
			
			HtmlHelper.functionalitySelected();
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			return "/account/AccountInfo";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value = "/accountChangePassword", method = RequestMethod.GET)
	String changePassword(Principal principal, Model model) {
		
		HtmlHelper.functionalitySelected();
		appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
		
		return "/account/PasswordChange";
	}
	
	@Transactional
	@RequestMapping(value = "/accountChangePassword", method = RequestMethod.POST)
	public @ResponseBody String changePassword(HttpServletRequest request) throws IOException {
		
		String jsonInString = null;
		Gson gson = new Gson();
		JUser jUser = new JUser();
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		try {
			
			String objUser = request.getParameter("objUser");
			jUser = gson.fromJson(objUser, JUser.class);
			
			// Clave
			String passwd = UBase64.base64Decode(jUser.getPasswd());
			String passwdRepeat = UBase64.base64Decode(jUser.getPasswdRepeat());
			
			if(UValidator.isNullOrEmpty(passwd)){
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				error.setField("passwd");
				errors.add(error);
			}else if(UValidator.isNullOrEmpty(passwdRepeat)){
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				error.setField("passwd-repeat");
				errors.add(error);
			}else{
				if(!passwd.equals(passwdRepeat)){
					error.setMessage(UProperties.getMessage("ERR0059", userTools.getLocale()));
					error.setField("passwd-repeat");
					errors.add(error);
				}else{
					String passwdHashStored = userTools.getCurrentUser().getHash();
					if (BCrypt.checkpw(passwd, passwdHashStored)){
						error.setMessage(UProperties.getMessage("ERR0067", userTools.getLocale()));
						error.setField("passwd");
						errors.add(error);
					}
				}
			}
			
			if(!errors.isEmpty()){
				response.setErrors(errors);
				response.setError(Boolean.TRUE);
			}else{
				String passwdCrypt = new BCryptPasswordEncoder(12).encode(passwd);			
				// Password hash
				String hash = BCrypt.hashpw(passwd, BCrypt.gensalt());
				
				UsuarioEntity usuarioEntity = userTools.getCurrentUser();
				usuarioEntity.setClave(passwdCrypt);
				usuarioEntity.setHash(hash);
				
				usuarioEntity = entityManager.merge(usuarioEntity);
				entityManager.persist(usuarioEntity);
				entityManager.flush();
				
				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.ACTUALIZACION_CLAVE_ID38);
				
				// Timeline :: Registro de accion
				timelineLog.timelineLog(EventTypeCode.ACTUALIZACION_CLAVE_ID16);
				
				Toastr toastr = new Toastr();
				toastr.setTitle("Cambiar contraseña");
				toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.passwd.changed", userTools.getLocale()));
				notification.setToastrNotification(Boolean.TRUE);
				toastr.setType(Toastr.ToastrType.SUCCESS.value());
				
				notification.setToastrNotification(Boolean.TRUE);
				notification.setToastr(toastr);
				
				response.setNotification(notification);
				response.setSuccess(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(UProperties.getMessage("ERR0068", userTools.getLocale()));
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();

		jUser.setResponse(response);
		jsonInString = gson.toJson(jUser);
		return jsonInString;
	}
	
	@Transactional
	@RequestMapping(value = "/userByAssociatedAccount", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
	public @ResponseBody String userByAssociatedAccount(Model model, HttpServletRequest request) {
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		JAccount accountJson = new JAccount();
		Account account = new Account();
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		Boolean errorMarked = false;
		
		try {
			
			String objAccount = request.getParameter("objAccount");
			account = gson.fromJson(objAccount, Account.class);
						
			CuentaEntity associatedAccount = iAccountJpaRepository.findByIdAndEliminadoFalse(UValue.longValue(UBase64.base64Decode(account.getId())));
			if(!UValidator.isNullOrEmpty(associatedAccount)){
				ContribuyenteEntity taxpayer = associatedAccount.getContribuyenteAsociado();
				
				//##### Correo
				String email = associatedAccount.getCorreoElectronico();
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(email)){
					error.setMessage(UProperties.getMessage("ERR0168", userTools.getLocale()));
					errorMarked = true;
				}else{
					UsuarioEntity userByEmail = iUserJpaRepository.findByCorreoElectronico(email);
					if (userByEmail != null) {
						error.setMessage(UProperties.getMessage("ERR0062", userTools.getLocale()));
						errorMarked = true;
					}
				}
				if(errorMarked){
					errors.add(error);
					
					notification.setMessage(error.getMessage());
					notification.setPageMessage(Boolean.TRUE);
					notification.setCssStyleClass(CssStyleClass.ERROR.value());
					
					response.setNotification(notification);
					response.setError(Boolean.TRUE);					
				}
				
				if(!errors.isEmpty()){
					response.setErrors(errors);
					response.setError(Boolean.TRUE);
				}else{				
					userTools.log("[INFO] CREATING USER FROM ASSOCIATED ACCOUNT...");
					
					//##### Initial password
					String passwd = UValue.stringValueUppercase(taxpayer.getRfc());
					String passwdCrypt = new BCryptPasswordEncoder(12).encode(passwd);
					//##### Password hash
					String hash = BCrypt.hashpw(passwd, BCrypt.gensalt());
					
					//##### Usuario :: Registro
					UsuarioEntity user = new UsuarioEntity();
					user.setCorreoElectronico(email);
					user.setClave(passwdCrypt);
					user.setHash(hash);
					user.setGeneradoPorSistema(Boolean.FALSE);
					user.setActivo(false);
					user = (UsuarioEntity) persistenceDAO.persist(user);
					
					userTools.log("[INFO] REGISTERING USER...");
					
					//##### Perfil :: Registro
					RolEntity role = iRoleJpaRepository.findByCodigo(ROLE_CODE_MASTER);
					TipoContribuyenteEntity taxpayerType = iTaxpayerTypeJpaRepository.findByCodigo(TAXPAYER_TYPE_DEFAULT);
					PerfilEntity profile = new PerfilEntity();
					profile.setContribuyente(taxpayer);
					profile.setRol(role);
					profile.setTipoContribuyente(taxpayerType);
					persistenceDAO.persist(profile);
					
					userTools.log("[INFO] CREATING PROFILE...");
					
					//##### Credencial :: Registro
					LenguajeEntity language = userTools.getLocaleDefault();
					CredencialEntity credential = new CredencialEntity();
					credential.setContribuyente(taxpayer);
					credential.setUsuario(user);
					credential.setLenguaje(language);
					credential.setEliminado(Boolean.FALSE);
					persistenceDAO.persist(credential);
					
					userTools.log("[INFO] CREATING CREDENTIAL...");
					
					//##### Codigo de activacion :: Generando codigo de activacion para la cuenta del usuario
					CodigoActivacionEntity activationCode = this.generarCodigoActivacion(user);
					String activationCodeToSend = activationCode.getCodigo();
					
					//##### Bitacora :: Registro de accion en bitacora
					appLog.logOperation(OperationCode.GENERACION_CREDENCIALES_ACCESO_CUENTA_ASOCIADA_ID72);
					timelineLog.timelineLog(EventTypeCode.GENERACION_CREDENCIALES_ACCESO_CUENTA_ASOCIADA_ID40, taxpayer);
					
					userTools.log("[INFO] REGISTERING LOG OPERATION...");
					
					//##### Email :: Enviando correo de confirmacion
					String to = taxpayer.getNombreRazonSocial().toUpperCase();
					boolean emailSended;
					try {
						
						userTools.log("[INFO] PREPARING TO SEND EMAIL...");
						
						emailSended = mailService.sendAssociatedAccountUserMail(email, to, email, passwd, activationCodeToSend);
						
					} catch (Exception e) {
						emailSended = false;
						userTools.log("[ERROR] CREATING USER FROM ASSOCIATED ACCOUNT > EMAIL WASN'T SENDED");
					}
					
					//##### Actualizando codigo de activacion
					activationCode.setEnviado(emailSended);
					entityManager.merge(activationCode);
					entityManager.flush();
					
					//##### Notificacion Toastr
					Toastr toastr = new Toastr(); 
					toastr.setTitle(UProperties.getMessage("mega.cfdi.premium.configuration.account.rfc", userTools.getLocale()));
					toastr.setMessage("Credenciales de acceso generadas satisfactoriamente.");
					toastr.setType(Toastr.ToastrType.SUCCESS.value());
					
					//##### Mensaje de notificacion
					notification.setToastrNotification(Boolean.TRUE);
					notification.setToastr(toastr);
					
					//##### JSon response 
					response.setSuccess(Boolean.TRUE);
					response.setNotification(notification);
				}
				
				userTools.log("[INFO] USER FROM ASSOCIATED ACCOUNT CREATED SUCCESSFULY");
			}else{
				notification.setMessage(UProperties.getMessage("ERR0077", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			userTools.log("[ERROR] UNEXPECTED ERROR ATTEMPTING TO CREATE USER FROM ASSOCIATED ACCOUNT");
			
			e.printStackTrace();
		}
		
		accountJson.setResponse(response);
		jsonInString = gson.toJson(accountJson);
		return jsonInString;
	}
	
	@Transactional
	@Layout(value = "layouts/blank")
	@RequestMapping(value = "/confirmAssociatedAccountUser", method = RequestMethod.GET)
	public String confirmAssociatedAccount(String code, Model model) throws IOException {
		try {
			
			Boolean error = false;
			CodigoActivacionEntity codigo = iActivationCodeJpaRepository.findByCodigo(code);
			
			if(UValidator.isNullOrEmpty(code)){
				return "/login/login";
			}else if (UValidator.isNullOrEmpty(codigo)) {
				model.addAttribute("message", UProperties.getMessage("ERR0063", userTools.getLocale()));
				error = true;
			} else {
				UsuarioEntity usuarioEntity = codigo.getUsuario();
				if(usuarioEntity.isActivo()){
					model.addAttribute("message", UProperties.getMessage("ERR0065", userTools.getLocale()));
					error = true;
				}else{
					userTools.log("[INFO] ACTIVATING USER FROM ASSOCIATED ACCOUNT...");
					
					usuarioEntity.setActivo(true);
					entityManager.persist(usuarioEntity);
					
					codigo.setActivo(Boolean.FALSE);
					codigo.setCodigoActivado(Boolean.TRUE);
					codigo.setFechaActivacion(new Date());
					entityManager.persist(codigo);
					
					entityManager.flush();
					
					// Bitacora :: Registro de accion en bitacora
					appLog.logOperation(OperationCode.ACTIVACION_CUENTA_USUARIO_ID66, usuarioEntity);
					
					userTools.log("[INFO] USER FROM ASSOCIATED ACCOUNT ACTIVATED SUCCESSFULY");
				}
			}
			
			if(!error){
				model.addAttribute("message", UProperties.getMessage("mega.cfdi.notification.account.activated", userTools.getLocale()));
			}
			
		} catch (Exception e) {
			model.addAttribute("message", UProperties.getMessage("ERR0066", userTools.getLocale()));
		}
		
		return "/register/AccountConfirmation";
	}
}