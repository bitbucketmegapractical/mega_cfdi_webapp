package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;

public interface InvoiceService {
	
	StampResponse cfdiStamp(ApiCfdi apiCfdi, byte[] cert, byte[] privateKey, String keyPasswd);
	
	StampResponse cfdiStamp(StampProperties stampProperties);
	
}