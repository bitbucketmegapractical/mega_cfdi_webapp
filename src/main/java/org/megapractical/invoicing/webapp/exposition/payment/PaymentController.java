package org.megapractical.invoicing.webapp.exposition.payment;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.PaymentTryFileService;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.PaymentTryService;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JCustomResponse;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import lombok.val;
import lombok.var;

@Controller
@RequestMapping("/payment-tray")
@Scope("session")
public class PaymentController {

	@Autowired
	private AppLog appLog;

	@Autowired
	private UserTools userTools;

	@Autowired
	private PaymentTryService paymentTryService;

	@Autowired
	private PaymentTryFileService paymentTryFileService;

	@Autowired
	private SessionController sessionController;

	// Contribuyente
	private ContribuyenteEntity taxpayer;

	// Certificado del emisor
	private ContribuyenteCertificadoEntity certificate;

	@ModelAttribute("requiredMessage")
	public String requiredMessage() {
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}

	@ModelAttribute("processingRequest")
	public String processingRequest() {
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}

	@ModelAttribute("loadingRequest")
	public String loadingRequest() {
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}

	@ModelAttribute("emptyInformation")
	public String emptyInformation() {
		return UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
	}

	@ModelAttribute("availableStampError")
	public String availableStampError() {
		return UProperties.getMessage("ERR0163", userTools.getLocale());
	}

	@PostConstruct
	private void init() {
		this.taxpayer = paymentTryService.getCurrentTaxpayer();
		this.certificate = paymentTryService.getCertificate(this.taxpayer);
	}

	@GetMapping
	public String initPayment(Model model) {
		if (this.certificate == null) {
			return "/denied/ResourceDeniedByCertificateExpiration";
		}

		HtmlHelper.functionalitySelected("Payment", "payment-file-inbox");
		appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);

		return "/payment/PaymentMasive";
	}

	@PostMapping(params = { "paymentFileList" })
	@ResponseBody
	public String paymentFileList(HttpServletRequest request) {
		// Listado de archivos con estado generado (con paginado)
		val pageStr = UBase64.base64Decode(request.getParameter("page"));
		// Determinando la accion
		val action = UBase64.base64Decode(request.getParameter("action"));
		// Search by
		var searchName = UBase64.base64Decode(request.getParameter("searchName"));
		if (UValidator.isNullOrEmpty(searchName)) {
			searchName = "";
		}

		val cfdiFile = paymentTryFileService.paymentTry(this.taxpayer, pageStr, searchName);
		if (!"async".equalsIgnoreCase(action)) {
			sessionController.sessionUpdate();
		}
		return GsonClient.response(cfdiFile);
	}

	@PostMapping(params = { "paymentFileGeneratedList" })
	@ResponseBody
	public String paymentGeneratedFiles(HttpServletRequest request) {
		// Listado de archivos con estado generado (con paginado)
		val pageStr = UBase64.base64Decode(request.getParameter("page"));
		// Search by
		var searchName = UBase64.base64Decode(request.getParameter("searchName"));
		if (UValidator.isNullOrEmpty(searchName)) {
			searchName = "";
		}

		val cfdiFile = paymentTryFileService.loadGeneratedFiles(this.taxpayer, pageStr, searchName);
		sessionController.sessionUpdate();
		return GsonClient.response(cfdiFile);
	}

	@PostMapping(params = { "uploadFile" })
	public @ResponseBody String uploadPayment(Model model, HttpServletRequest request,
			@RequestParam(value = "fileUploaded") MultipartFile multipartFile) {
		// Enviar correo al cliente
		val sendMail = Boolean.valueOf(request.getParameter("sendMail"));
		val cfdiFile = paymentTryFileService.uploadFile(this.taxpayer, this.certificate, multipartFile, sendMail);
		sessionController.sessionUpdate();
		return GsonClient.response(cfdiFile);
	}

	@GetMapping("/file/download/{name}/{type}")
	@ResponseBody
	public ModelAndView downloadPayment(@PathVariable(value = "name") String name,
			@PathVariable(value = "type") String type, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		val fileByte = paymentTryFileService.downloadFile(taxpayer, name, type, response);
		if (fileByte != null) {
			FileCopyUtils.copy(fileByte, response.getOutputStream());
		}
		sessionController.sessionUpdate();
		return null;
	}

	@PostMapping(params = { "queuedFileRemove" })
	@ResponseBody
	public String queuedFileRemove(HttpServletRequest request) {
		JResponse response = null;
		
		val fileId = Long.valueOf(UBase64.base64Decode(request.getParameter("id")));
		val exists = paymentTryFileService.exists(fileId);
		if (exists) {
			paymentTryFileService.deleteQueuedFile(fileId);
			val toastr = Toastr.success("Eliminar archivo", "El archivo ha sido eliminado satisfactoriamente");
			val notification = JNotification.toastrNotification(toastr);
			response = JResponse.success(notification);
		} else {
			val notification = JNotification.pageMessageError("El archivo especificado no existe");
			response = JResponse.error(notification);
		}
		
		sessionController.sessionUpdate();
		val customResponse = new JCustomResponse(response);
		return GsonClient.response(customResponse);
	}

}