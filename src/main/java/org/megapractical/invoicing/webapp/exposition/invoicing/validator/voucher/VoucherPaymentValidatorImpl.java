package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MetodoPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.PaymentMethodValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.PaymentWayValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherPaymentMethodResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherPaymentWayResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.VoucherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherPaymentValidatorImpl implements VoucherPaymentValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private PaymentWayValidator paymentWayValidator;	
	
	@Autowired
	private PaymentMethodValidator paymentMethodValidator;
	
	@Override
	public VoucherPaymentWayResponse getPaymentWay(JVoucher voucher, 
												   TipoComprobanteEntity voucherType,
												   List<JError> errors) {
		FormaPagoEntity paymentWayEntity = null;
		JError error = null;
		val field = "payment-way";
		
		var paymentWay = UBase64.base64Decode(voucher.getPaymentWay());
		if (!UValidator.isNullOrEmpty(paymentWay)) {
			val paymentWayValidatorResponse = paymentWayValidator.validate(paymentWay, field);
			paymentWayEntity = paymentWayValidatorResponse.getPaymentWay();
			error = paymentWayValidatorResponse.getError();
		} else {
			if (!UValidator.isNullOrEmpty(voucherType)
					&& voucherType.getCodigo().equalsIgnoreCase(VoucherUtils.VOUCHER_TYPE_CODE_I)
					&& UValidator.isNullOrEmpty(paymentWay)) {
				error = JError.error(field, "ERR0159", userTools.getLocale());
			}
		}
		
		var paymentWayError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			paymentWayError = true;
		}
		
		return new VoucherPaymentWayResponse(paymentWayEntity, paymentWayError);
	}

	@Override
	public VoucherPaymentMethodResponse getPaymentMethod(JVoucher voucher, 
														 TipoComprobanteEntity voucherType,
														 List<JError> errors) {
		MetodoPagoEntity paymentMethodEntity = null;
		JError error = null;
		val field = "payment-method";
		
		var paymentMethod = UBase64.base64Decode(voucher.getPaymentMethod());
		if (!UValidator.isNullOrEmpty(paymentMethod)) {
			val paymentMethodValidatorResponse = paymentMethodValidator.validate(paymentMethod, field);
			paymentMethodEntity = paymentMethodValidatorResponse.getPaymentMethod();
			error = paymentMethodValidatorResponse.getError();
		} else {
			if (!UValidator.isNullOrEmpty(voucherType)
					&& voucherType.getCodigo().equalsIgnoreCase(VoucherUtils.VOUCHER_TYPE_CODE_I)
					&& UValidator.isNullOrEmpty(paymentMethod)) {
				error = JError.error(field, "ERR0160", userTools.getLocale());
			}
		}
		
		var paymentMethodError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			paymentMethodError = true;
		}
		
		return new VoucherPaymentMethodResponse(paymentMethodEntity, paymentMethodError);
	}

}