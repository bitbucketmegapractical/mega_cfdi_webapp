package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.data.repository.IPaymentWayJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.PaymentWayValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class PaymentWayValidatorImpl implements PaymentWayValidator {

	@Autowired
	private UserTools userTools;

	@Autowired
	private IPaymentWayJpaRepository iPaymentWayJpaRepository;

	@Override
	public PaymentWayValidatorResponse validate(String paymentWay) {
		var hasError = false;

		val paymentWayEntity = iPaymentWayJpaRepository.findByCodigoAndEliminadoFalse(paymentWay);
		if (paymentWayEntity == null) {
			hasError = true;
		}

		return new PaymentWayValidatorResponse(paymentWayEntity, hasError);
	}

	@Override
	public PaymentWayValidatorResponse validate(String paymentWay, String field) {
		JError error = null;

		val paymentWayEntity = getPaymentWay(paymentWay);
		if (paymentWayEntity == null) {
			val values = new String[] { "CFDI33104", "FormaPago", "c_FormaPago" };
			error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
		}

		return new PaymentWayValidatorResponse(paymentWayEntity, error);
	}

	private FormaPagoEntity getPaymentWay(String paymentWay) {
		// Validar que sea un valor de catalogo
		val paymentWayCatalog = UCatalog.getCatalog(paymentWay);
		paymentWay = paymentWayCatalog[0];
		val paymentWayValue = paymentWayCatalog[1];

		val paymentWayEntity = iPaymentWayJpaRepository.findByCodigoAndEliminadoFalse(paymentWay);
		val paymentWayEntityByValue = iPaymentWayJpaRepository.findByValorAndEliminadoFalse(paymentWayValue);
		if (paymentWayEntity == null || paymentWayEntityByValue == null) {
			return null;
		}
		return paymentWayEntity;
	}

}