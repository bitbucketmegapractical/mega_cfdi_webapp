package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.CurrencyValidatorResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.CurrencyValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.TaxValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.TaxRequest;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JRateOrFee;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JTaxes;
import org.megapractical.invoicing.webapp.json.JTaxes.JTax;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class TaxServiceImpl implements TaxService {
	
	@Autowired
	private CurrencyValidator currencyValidator;
	
	@Autowired
	private TaxValidator taxValidator;
	
	@Override
	public JTaxes validateTax(TaxRequest request) {
		val response = new JResponse();
		val errors = new ArrayList<JError>();
		
		val taxes = GsonClient.deserialize(request.getRequestData(), JTaxes.class);
		val currencyValidatorResponse = validateCurrency(request.getCurrency(), "currency", response, errors);
		if (!currencyValidatorResponse.isHasError()) {
			val currency = currencyValidatorResponse.getCurrency();
			val isTransferred = request.getSelectedTax().equalsIgnoreCase("transferred");
			val isAutomaticCalculation = !UValidator.isNullOrEmpty(request.getAction());
			val taxType = UBase64.base64Decode(taxes.getTax().getTax());
			val rateOrFee = UBase64.base64Decode(taxes.getTax().getRateOrFee());
			val amount = UBase64.base64Decode(taxes.getTax().getAmount());
			val factorType = UBase64.base64Decode(taxes.getTax().getFactorType());
			
			val base = validateBase(taxes, request.getBaseField(), currency, isTransferred, errors);
			validateTaxType(taxType, request.getTaxTypeField(), isTransferred, errors);
			validateFactorType(factorType, rateOrFee, amount, request.getFactorTypeField(), isAutomaticCalculation,
					isTransferred, errors);
			val rateOrFeeBd = validateRateOrFee(request.getRateOrFeeJson(), rateOrFee, factorType, taxType,
					request.getRateOrFeeField(), isTransferred, errors);
			val tax = validateAmount(base, rateOrFeeBd, factorType, amount, request.getAmountField(), isTransferred,
					isAutomaticCalculation, currency, errors);
			if (tax != null) {
				taxes.setTax(tax);
			}
			
			val notification = validateTax(taxes, factorType, taxType, rateOrFeeBd, isTransferred, errors);
			response.setNotification(notification);
		}
		
		if (!errors.isEmpty()) {
			response.setErrors(errors);
			response.setError(Boolean.TRUE);
		} else {
			response.setSuccess(Boolean.TRUE);
		}
		
		taxes.setResponse(response);
		return taxes;
	}
	
	private CurrencyValidatorResponse validateCurrency(String currency, String field, JResponse response, List<JError> errors) {
		val currencyValidatorResponse = currencyValidator.validate(currency, field);
		val currencyEntity = currencyValidatorResponse.getCurrency();
		val error = currencyValidatorResponse.getError();

		var hasError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			
			val notification = JNotification.modalPanelError();
			response.setNotification(notification);
			
			hasError = true;
		}
		return new CurrencyValidatorResponse(currencyEntity, hasError);
	}
	
	private BigDecimal validateBase(JTaxes taxes, 
									String field, 
									MonedaEntity currency, 
									boolean isTransferred,
									List<JError> errors) {
		val base = UBase64.base64Decode(taxes.getTax().getBase());
		val response = taxValidator.validateBase(base, field, currency, isTransferred);
		val error = response.getError();
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return response.getBase();
	}
	
	private void validateTaxType(String taxType, String field, boolean isTransferred, List<JError> errors) {
		val error = taxValidator.validateTaxType(taxType, field, isTransferred);
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
	}
	
	private void validateFactorType(String factorType, 
									String rateOrFee, 
									String amount,
									String field,
									boolean isAutomaticCalculation,
									boolean isTransferred, 
									List<JError> errors) {
		val error = taxValidator.validateFactorType(factorType, rateOrFee, amount, field, isAutomaticCalculation,
				isTransferred);
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
	}
	
	private BigDecimal validateRateOrFee(JRateOrFee rateOrFeeJson, 
								   		 String rateOrFee, 
								   		 String factorType, 
								   		 String taxType,
								   		 String field,
								   		 boolean isTransferred, 
								   		 List<JError> errors) {
		if (errors.isEmpty()) {
			val response  = taxValidator.validateRateOrFee(rateOrFeeJson, rateOrFee, factorType, taxType, field, isTransferred);
			val error = response.getError();
			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
			}
			return response.getRateOrFee();
		}
		return null;
	}
	
	private JTax validateAmount(BigDecimal base, 
								BigDecimal rateOrFee, 
								String factorType, 
								String amount,
								String field,
								boolean isTransferred, 
								boolean isAutomaticCalculation, 
								MonedaEntity currency, 
								List<JError> errors) {
		if (errors.isEmpty()) {
			val response = taxValidator.validateAmount(base, rateOrFee, factorType, amount, field, isTransferred, isAutomaticCalculation, currency);
			val error = response.getError();
			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
			}
			return response.getTax();
		}
		return null;
	}
	
	private JNotification validateTax(JTaxes taxes, String factorType, String taxType, BigDecimal rateOrFee, boolean isTransferred, List<JError> errors) {
		if (errors.isEmpty()) {
			val response = taxValidator.validateTax(taxes, factorType, taxType, rateOrFee, isTransferred);
			val error = response.getError();
			if (!UValidator.isNullOrEmpty(error)) {
				errors.add(error);
			}
			return response.getNotification();
		}
		return null;
	}
	
}