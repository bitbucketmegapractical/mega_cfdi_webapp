package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import java.math.BigDecimal;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiVoucherTaxes;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiConcept.ConceptCustomsInformation;
import org.megapractical.invoicing.api.wrapper.ApiConcept.ConceptPropertyAccount;
import org.megapractical.invoicing.webapp.json.JResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoucherConceptResponse {
	private List<ApiConcept> apiConcepts;
	private List<ConceptCustomsInformation> conceptCustomsInformations;
	private List<ConceptPropertyAccount> conceptPropertyAccounts;
	private BigDecimal voucherSubtotal;
	private ApiVoucherTaxes conceptsTax;
	private boolean discountByConcept;
	private JResponse response;
	private boolean error;
}