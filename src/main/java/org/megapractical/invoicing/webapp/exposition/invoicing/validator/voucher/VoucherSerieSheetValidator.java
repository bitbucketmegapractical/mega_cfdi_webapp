package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherSerieSheetResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;

public interface VoucherSerieSheetValidator {
	
	VoucherSerieSheetResponse getSerieSheetResponse(JVoucher voucher, ContribuyenteEntity taxpayer, List<JError> errors);
	
}