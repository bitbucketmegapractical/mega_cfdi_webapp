package org.megapractical.invoicing.webapp.exposition.retention.source;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.megapractical.invoicing.dal.bean.jpa.RetencionTipoDividendoEntity;
import org.megapractical.invoicing.dal.data.repository.IRetentionDividendTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionPaymetTypeJpaRepository;
import org.megapractical.invoicing.webapp.json.JOption;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.val;

@Component
public class RetentionDataSource {
	
	private final IRetentionPaymetTypeJpaRepository iRetentionPaymetTypeJpaRepository;
	private final IRetentionDividendTypeJpaRepository iRetentionDividendTypeJpaRepository;

	public RetentionDataSource(IRetentionPaymetTypeJpaRepository iRetentionPaymetTypeJpaRepository,
							   IRetentionDividendTypeJpaRepository iRetentionDividendTypeJpaRepository) {
		this.iRetentionPaymetTypeJpaRepository = iRetentionPaymetTypeJpaRepository;
		this.iRetentionDividendTypeJpaRepository = iRetentionDividendTypeJpaRepository;
	}
	
	@Getter
	private List<JOption> paymetTypeSource;
	@Getter
	private Map<String, String> dividendTypeSource;
	
	@PostConstruct
	public void init() {
		paymetTypeSource();
		dividendTypeSource();
	}

	public List<JOption> paymetTypeSource() {
		if (paymetTypeSource == null) {
			val source = iRetentionPaymetTypeJpaRepository.findByEliminadoFalse();
			paymetTypeSource = source.stream()
									 .map(i -> new JOption(i.getTipoPago(), i.getDescripcion(), i.getData()))
									 .collect(toList());
		}
		return paymetTypeSource;
	}

	public Map<String, String> dividendTypeSource() {
		if (dividendTypeSource == null) {
			val source = iRetentionDividendTypeJpaRepository.findByEliminadoFalse();
			dividendTypeSource = source.stream()
					.collect(toMap(RetencionTipoDividendoEntity::getCodigo, RetencionTipoDividendoEntity::getData));
		}
		return dividendTypeSource;
	}
	
}