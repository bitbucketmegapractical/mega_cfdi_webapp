package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentRelatedDocument;
import org.megapractical.invoicing.webapp.json.JResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RelatedDocumentResponse {
	private List<ComplementPaymentRelatedDocument> relatedDocuments;
	private JResponse response;
	private boolean error;
	
	public RelatedDocumentResponse(JResponse response) {
		this.response = response;
		this.error = Boolean.TRUE;
		this.relatedDocuments = new ArrayList<>();
	}
	
	public RelatedDocumentResponse(List<ComplementPaymentRelatedDocument> relatedDocuments) {
		this.relatedDocuments = relatedDocuments;
		this.error = Boolean.FALSE;
	}
	
}