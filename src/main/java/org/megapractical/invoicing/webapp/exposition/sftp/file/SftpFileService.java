package org.megapractical.invoicing.webapp.exposition.sftp.file;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.ISftpFileJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class SftpFileService {

	private final ISftpFileJpaRepository iSftpFileJpaRepository;

	public ArchivoStfpEntity findById(Long id) {
		return iSftpFileJpaRepository.findByIdAndEliminadoFalse(id)
				.orElseThrow(() -> new SftpFileNotFoundException(id));
	}

	public Page<ArchivoStfpEntity> getCfdiFiles(ContribuyenteEntity taxpayer, String name, LocalDate date,
			Pageable pageable) {
		val areas = Arrays.asList(Area.TESORERIA, Area.MERCADOS, Area.CREDITO, Area.FIDUCIARIO, Area.FACTURAS_MANUALES);
		return getFiles(taxpayer, name, date, pageable, areas);
	}

	public Page<ArchivoStfpEntity> getPayrollFiles(ContribuyenteEntity taxpayer, String name, LocalDate date,
			Pageable pageable) {
		val areas = Arrays.asList(Area.RH);
		return getFiles(taxpayer, name, date, pageable, areas);
	}

	private Page<ArchivoStfpEntity> getFiles(ContribuyenteEntity taxpayer, String name, LocalDate date,
			Pageable pageable, List<Area> areas) {
		return iSftpFileJpaRepository.findSftpFiles(taxpayer, name, date, areas, pageable);
	}

}