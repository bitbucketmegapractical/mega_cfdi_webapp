package org.megapractical.invoicing.webapp.exposition.advanced;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.CredencialEntity;
import org.megapractical.invoicing.dal.bean.jpa.CuentaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreTransferenciaEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.megapractical.invoicing.dal.data.repository.IAccountJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICredentialJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.json.JCredential;
import org.megapractical.invoicing.webapp.json.JCredential.Credential;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JStamp;
import org.megapractical.invoicing.webapp.json.JStampBuyTransfer;
import org.megapractical.invoicing.webapp.json.JStampBuyTransfer.StampTransferOptions;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
@Scope("session")
public class EmitterStampTransfer {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	StampControlService stampControlService;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Resource
	ICredentialJpaRepository iCredentialJpaRepository;
	
	@Resource
	IStampJpaRepository iStampJpaRepository;
	
	@Resource
	IAccountJpaRepository iAccountJpaRepository;
	
	@Resource
	ITaxpayerJpaRepository iTaxpayerJpaRepository;
	
	//##### Contribuyente
	private ContribuyenteEntity taxpayer;
	
	//##### Cuenta
	private List<CuentaEntity> accounts;
	
	//##### Timbre
	private TimbreEntity stamp;
	
	//##### Usuario
	private UsuarioEntity user;
	
	//##### Credencial
	private CredencialEntity credential;
	private JCredential credentialJson;
	
	//##### Mensaje campo requerido
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	//##### Mensaje procesando...
	@ModelAttribute("processingRequest")
	public String processingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	//##### Mensaje cargando...
	@ModelAttribute("loadingRequest")
	public String loadingMessage() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	//##### Mensaje formato incorrecto
	@ModelAttribute("invalidDataType")
	public String invalidDataTypeMessage() throws IOException{
		return UProperties.getMessage("ERR1003", userTools.getLocale());
	}
	
	//##### Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() throws IOException{
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	@RequestMapping(value = "/emitterStampTransfer", method = RequestMethod.GET)
	public String emitterStampTransfer(Model model) {
		try {
			
			if(userTools.isMaster()){
				populate();
				if(!UValidator.isNullOrEmpty(this.credential) && !UValidator.isNullOrEmpty(this.user) && !UValidator.isNullOrEmpty(this.taxpayer)){
					HtmlHelper.functionalitySelected("EmitterAdvancedOptions", "emitter-adv-opt-stamp-transfer");
					appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
					
					return "/advanced/EmitterStampTransfer";
				}else{
					return "/denied/ResourceDenied";
				}
			}else{
				return "/denied/ResourceDenied";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/emitterStampTransfer", method = RequestMethod.POST)
	public @ResponseBody String emitterStampTransfer(HttpServletRequest request) throws IOException {
		
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer();
		JStamp stamp = new JStamp();
			
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {			
			
			String options = "<option value=\"-\">&laquo;Seleccione&raquo;</option>";
			List<StampTransferOptions> stampTransferOptions = new ArrayList<>();
			
			if(!UValidator.isNullOrEmpty(this.credential) && !UValidator.isNullOrEmpty(this.user) && !UValidator.isNullOrEmpty(this.taxpayer)){
				// Cargando la informacion de los timbres de la cuenta RFC seleccionada
				this.stamp = iStampJpaRepository.findByContribuyente(this.taxpayer);
				stamp.setStampAvailable(UBase64.base64Encode(this.stamp.getTimbresDisponibles().toString()));
				
				// Cargando las cuentas asociadas del usuario
				this.accounts = new ArrayList<>();
				this.accounts.addAll(iAccountJpaRepository.findByContribuyenteAndEliminadoFalse(this.taxpayer));
				for (CuentaEntity item : this.accounts) {
					String option = "<option value="+UBase64.base64Encode(item.getContribuyenteAsociado().getId().toString())+">"+item.getContribuyenteAsociado().getRfc()+"</option>";
					
					if(!UValidator.isNullOrEmpty(options)){
						options += option;
					}else{
						options = option;
					}
					
					StampTransferOptions sto = new StampTransferOptions();
					sto.setId(UBase64.base64Encode(item.getId().toString()));
					sto.setValue(UBase64.base64Encode(item.getContribuyenteAsociado().getRfc()));
					stampTransferOptions.add(sto);
				}
				
				stampBuyTransfer.setRfcMatserAccount(UBase64.base64Encode(this.taxpayer.getRfc()));
				stampBuyTransfer.setOptions(UBase64.base64Encode(options));
				stampBuyTransfer.getStampTransferOptions().addAll(stampTransferOptions);
				stampBuyTransfer.setStamp(stamp);
				this.credentialJson.setStampBuyTransfer(stampBuyTransfer);
				
				response.setSuccess(Boolean.TRUE);
			}else{
				notification.setPageMessage(Boolean.TRUE);
				notification.setMessage(UProperties.getMessage("ERR1008", userTools.getLocale()));
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setError(Boolean.TRUE);
				response.setNotification(notification);
			}
			
		} catch (Exception e) {
			notification.setPageMessage(Boolean.TRUE);
			notification.setMessage(UProperties.getMessage("ERR1008", userTools.getLocale()));
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setError(Boolean.TRUE);
			response.setNotification(notification);
		}
		
		sessionController.sessionUpdate();
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		return jsonInString;
	}
	
	public void clear(){
		this.credentialJson = null;
		
		this.credential = null;
		this.user = null;
		this.taxpayer = null;
	}
	
	public void populate(){
		try {
			
			clear();
			
			this.user = userTools.getCurrentUser();
			this.taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			this.credential = iCredentialJpaRepository.findByUsuarioAndContribuyenteAndEliminadoFalse(this.user, this.taxpayer);
			
			if(!UValidator.isNullOrEmpty(this.credential)){
				Credential credential = new Credential();
				credential.setId(UBase64.base64Encode(this.credential.getId().toString()));
				credential.setLanguage(UBase64.base64Encode(this.credential.getLenguaje().getValor()));
				
				credential.setUserId(UBase64.base64Encode(this.user.getId().toString()));
				credential.setEmail(UBase64.base64Encode(this.user.getCorreoElectronico()));
				
				credential.setTaxpayerId(UBase64.base64Encode(this.taxpayer.getId().toString()));
				credential.setName(UBase64.base64Encode(this.taxpayer.getNombreRazonSocial()));
				credential.setRfc(UBase64.base64Encode(this.taxpayer.getRfc()));
				credential.setRfcActive(UBase64.base64Encode(this.taxpayer.getRfcActivo()));
				credential.setPhone(UBase64.base64Encode(this.taxpayer.getTelefono()));
				credential.setActiveProduct(UBase64.base64Encode(this.taxpayer.getProductoServicioActivo()));
				
				this.credentialJson = new JCredential();
				this.credentialJson.setCredential(credential);
			}else{
				clear();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			clear();
		}
	}
	
	@RequestMapping(value="/emitterStampTransfer", params={"loadStampTransfer"})
	public @ResponseBody String loadStampTransfer(HttpServletRequest request) throws IOException{
		
		JResponse response = new JResponse();
		
		JStamp stamp = new JStamp();
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			Long id = null;
			CuentaEntity account = null;
			String idStr = UBase64.base64Decode(request.getParameter("accountIndex"));
			
			if(!UValidator.isNullOrEmpty(idStr)){
				id = UValue.longValue(idStr);
				account = iAccountJpaRepository.findByContribuyenteAsociado_IdAndEliminadoFalse(id);
				if(!UValidator.isNullOrEmpty(account)){
					ContribuyenteEntity taxpayerAssociated = iTaxpayerJpaRepository.findById(id).orElse(null);
					if(!UValidator.isNullOrEmpty(taxpayerAssociated)){
						TimbreEntity stampEntity = iStampJpaRepository.findByContribuyente(taxpayerAssociated);
						if(!UValidator.isNullOrEmpty(stampEntity)){
							stamp.setStampAvailable(UBase64.base64Encode(stampEntity.getTimbresDisponibles().toString()));
						}else{
							stamp.setStampAvailable(UBase64.base64Encode("0"));
						}						
						stampBuyTransfer.setStamp(stamp);
						
						response.setSuccess(Boolean.TRUE);
					}else{
						error.setField("stamp-transfer-account");
						error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
						errors.add(error);
					}
				}else{
					error.setField("stamp-transfer-account");
					error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
					errors.add(error);
				}
			}else{
				error.setField("stamp-transfer-account");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			error.setField("stamp-transfer-account");
			error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
			errors.add(error);
		}
		
		if(!errors.isEmpty()){
			response.setError(Boolean.TRUE);
			response.setErrors(errors);
		}
		stampBuyTransfer.setResponse(response);
		
		sessionController.sessionUpdate();
		
		jsonInString = gson.toJson(stampBuyTransfer);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterStampTransfer", params={"stampTransfer"})
	public @ResponseBody String stampTransfer(HttpServletRequest request) throws IOException{
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		JStamp stamp = new JStamp();
		JStampBuyTransfer stampBuyTransfer = new JStampBuyTransfer();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		Gson gson = new Gson();
		String jsonInString = null;
		
		try {
			
			TimbreEntity masterStampEntity = null;
			TimbreEntity associatedStampEntity = null;
			
			String stampBuyTransferObj = request.getParameter("stampBuyTransfer");
			stampBuyTransfer = gson.fromJson(stampBuyTransferObj, JStampBuyTransfer.class);
			
			String masterRfc = UBase64.base64Decode(stampBuyTransfer.getRfcMatserAccount());
			if(UValidator.isNullOrEmpty(masterRfc)){
				error.setField("stamp-transfer-master");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}else if(!masterRfc.equalsIgnoreCase(this.taxpayer.getRfc())){
				error.setField("stamp-transfer-master");
				error.setMessage(UProperties.getMessage("ERR0001", userTools.getLocale()));
				errors.add(error);
			}
			
			Long associatedId = null;
			CuentaEntity account = null;
			ContribuyenteEntity taxpayerAssociated = null;
			
			String associatedIdStr = UBase64.base64Decode(stampBuyTransfer.getAssociatedAccountId());
			error = new JError();
			
			if(!UValidator.isNullOrEmpty(associatedIdStr)){
				associatedId = UValue.longValue(associatedIdStr);
				account = iAccountJpaRepository.findByContribuyenteAsociado_IdAndEliminadoFalse(associatedId);
				if(!UValidator.isNullOrEmpty(account)){
					taxpayerAssociated = iTaxpayerJpaRepository.findById(associatedId).orElse(null);
					if(!UValidator.isNullOrEmpty(taxpayerAssociated)){
						Boolean finded = false;
						for (CuentaEntity item : this.accounts) {
							if(item.getContribuyenteAsociado().getRfc().equalsIgnoreCase(account.getContribuyenteAsociado().getRfc())){
								finded = Boolean.TRUE;
								break;
							}
						}
						if(!finded){
							error.setField("stamp-transfer-account");
							error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
							errors.add(error);
						}
					}else{
						error.setField("stamp-transfer-account");
						error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
						errors.add(error);
					}
				}else{
					error.setField("stamp-transfer-account");
					error.setMessage(UProperties.getMessage("ERR0113", userTools.getLocale()));
					errors.add(error);
				}
			}else{
				error.setField("stamp-transfer-account");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}
			
			stamp = stampBuyTransfer.getStamp();
			
			String stampAvailableStr = UBase64.base64Decode(stamp.getStampAvailable());
			Integer stampAvailable = null;
			error = new JError();
			
			if(UValidator.isNullOrEmpty(stampAvailableStr)){
				error.setField("stamp-transfer-available");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}else{
				stampAvailable = Integer.valueOf(stampAvailableStr);
			}
			
			String transferAmountStr = UBase64.base64Decode(stampBuyTransfer.getTransferAmount());
			Integer transferAmount = null;
			error = new JError();
			
			if(UValidator.isNullOrEmpty(transferAmountStr)){
				error.setField("stamp-transfer-amount");
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errors.add(error);
			}else{
				transferAmount = Integer.valueOf(transferAmountStr);
			}
			
			Boolean transferMasterToAccount = stampBuyTransfer.getTransferMasterToAccount();
			Boolean transferAccountToMaster = stampBuyTransfer.getTransferAccountToMaster();
			
			Boolean errorMarked = false;
			
			if(errors.isEmpty()){
				if(transferMasterToAccount){
					masterStampEntity = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
					Integer masterStampAvailable = masterStampEntity.getTimbresDisponibles();
					error = new JError();
					
					if(masterStampAvailable == 0){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0115", userTools.getLocale()));
						errorMarked = true;
					}else if(!masterStampAvailable.toString().equals(stampAvailable.toString())){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0116", userTools.getLocale()));
						errorMarked = true;
					}else if(transferAmount > stampAvailable){
						error.setField("stamp-transfer-amount");
						error.setMessage(UProperties.getMessage("ERR0117", userTools.getLocale()));
						errorMarked = true;
					}else if(stampAvailable - transferAmount < 0){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0118", userTools.getLocale()));
						errorMarked = true;
					}
				}else if(transferAccountToMaster){
					associatedStampEntity = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(taxpayerAssociated);
					Integer associatedStampAvailable = associatedStampEntity.getTimbresDisponibles();
					error = new JError();
					
					if(associatedStampAvailable == 0){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0115", userTools.getLocale()));
						errorMarked = true;
					}else if(!associatedStampAvailable.toString().equals(stampAvailable.toString())){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0116", userTools.getLocale()));
						errorMarked = true;
					}else if(transferAmount > stampAvailable){
						error.setField("stamp-transfer-amount");
						error.setMessage(UProperties.getMessage("ERR0117", userTools.getLocale()));
						errorMarked = true;
					}else if(stampAvailable - transferAmount < 0){
						error.setField("stamp-transfer-available");
						error.setMessage(UProperties.getMessage("ERR0119", userTools.getLocale()));
						errorMarked = true;
					}
				}
				if(errorMarked){
					errors.add(error);
					response.setError(Boolean.TRUE);
					response.setErrors(errors);
				}else {
					String transferType = null;
					
					OperationCode operationCode = null;
					EventTypeCode eventTypeCode = null;
					
					stampAvailable = stampAvailable - transferAmount;
					
					if(transferMasterToAccount){
						masterStampEntity.setTimbresDisponibles(stampAvailable);
						if(!UValidator.isNullOrEmpty(masterStampEntity.getTimbresTransferidos())){
							masterStampEntity.setTimbresTransferidos(masterStampEntity.getTimbresTransferidos() + transferAmount);
						}else{
							masterStampEntity.setTimbresTransferidos(transferAmount);
						}
						//masterStampEntity.setTotal(masterStampEntity.getTotal() - transferAmount);
						masterStampEntity.setHabilitado(stampAvailable > 0);
						persistenceDAO.update(masterStampEntity);
						
						associatedStampEntity = iStampJpaRepository.findByContribuyente(taxpayerAssociated);
						if(!UValidator.isNullOrEmpty(associatedStampEntity)){
							Integer associatedStampAvailable = associatedStampEntity.getTimbresDisponibles() + transferAmount;
							associatedStampEntity.setTimbresDisponibles(associatedStampAvailable);
							associatedStampEntity.setTotal(associatedStampEntity.getTotal() + transferAmount );
							associatedStampEntity.setHabilitado(Boolean.TRUE);
							persistenceDAO.update(associatedStampEntity);
						}else{
							associatedStampEntity = new TimbreEntity();
							associatedStampEntity.setContribuyente(taxpayerAssociated);
							associatedStampEntity.setHabilitado(Boolean.TRUE);
							associatedStampEntity.setTimbresConsumidos(0);
							associatedStampEntity.setTimbresDisponibles(transferAmount);
							associatedStampEntity.setTimbresTransferidos(0);
							associatedStampEntity.setTotal(transferAmount);
							persistenceDAO.persist(associatedStampEntity);
						}
						transferType = UBase64.base64Encode(UProperties.getMessage("mega.cfdi.information.stamp.transfer.master.to.account", userTools.getLocale()));
						
						operationCode = OperationCode.TRANSFERENCIA_TIMBRE_ID13;
						eventTypeCode = EventTypeCode.TRANSFERENCIA_TIMBRE_ID2;
						
					}else if(transferAccountToMaster){
						associatedStampEntity.setTimbresDisponibles(stampAvailable);
						if(!UValidator.isNullOrEmpty(associatedStampEntity.getTimbresTransferidos())){
							associatedStampEntity.setTimbresTransferidos(associatedStampEntity.getTimbresTransferidos() + transferAmount);
						}else{
							associatedStampEntity.setTimbresTransferidos(transferAmount);
						}
						//associatedStampEntity.setTotal(associatedStampEntity.getTotal() - transferAmount);
						associatedStampEntity.setHabilitado(stampAvailable > 0);
						persistenceDAO.update(associatedStampEntity);
						
						masterStampEntity = iStampJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
						Integer masterStampAvailable = masterStampEntity.getTimbresDisponibles() + transferAmount;
						masterStampEntity.setTimbresDisponibles(masterStampAvailable);
						masterStampEntity.setTotal(masterStampEntity.getTotal() + transferAmount );
						masterStampEntity.setHabilitado(Boolean.TRUE);
						persistenceDAO.update(masterStampEntity);
						
						transferType = UBase64.base64Encode(UProperties.getMessage("mega.cfdi.information.stamp.transfer.account.to.master", userTools.getLocale()));
						
						operationCode = OperationCode.TRANSFERENCIA_TIMBRE_CUENTA_MASTER_ID60;
						eventTypeCode = EventTypeCode.TRANSFERENCIA_TIMBRE_CUENTA_MASTER_ID35;
					}
					
					TimbreTransferenciaEntity stampTransferEntity = new TimbreTransferenciaEntity();
					stampTransferEntity.setContribuyente(this.taxpayer);
					stampTransferEntity.setCuenta(account);
					stampTransferEntity.setCantidad(transferAmount);
					stampTransferEntity.setFecha(new Date());
					stampTransferEntity.setHora(new Date());
					stampTransferEntity.setTransferenciaCuentaMaster(transferAccountToMaster);
					persistenceDAO.persist(stampTransferEntity);

					this.stamp = iStampJpaRepository.findByContribuyente(this.taxpayer);
					
					Integer stampTransfered = !UValidator.isNullOrEmpty(this.stamp.getTimbresTransferidos()) ? this.stamp.getTimbresTransferidos() : 0; 
					Integer stampTotal = this.stamp.getTotal();
					
					stamp.setStampAvailable(UBase64.base64Encode(this.stamp.getTimbresDisponibles().toString()));
					stamp.setStampSpent(UBase64.base64Encode(this.stamp.getTimbresConsumidos().toString()));
					stamp.setEnabled(this.stamp.getHabilitado());
					stamp.setStampTransfered(UBase64.base64Encode(stampTransfered.toString()));
					stamp.setStampTotal(UBase64.base64Encode(stampTotal.toString()));
					stampBuyTransfer.setStamp(stamp);
					
					stampBuyTransfer.setTransferType(transferType);
					stampBuyTransfer.setRfcAssociatedAccount(UBase64.base64Encode(account.getContribuyenteAsociado().getRfc().toUpperCase()));
					this.credentialJson.setStampBuyTransfer(stampBuyTransfer);
					
					
					// Bitacora :: Registro de accion en bitacora
					appLog.logOperation(operationCode);
					
					// Timeline :: Registro de accion
					timelineLog.timelineLog(eventTypeCode);
					
					Toastr toastr = new Toastr();
					toastr.setTitle(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.transfer.title", userTools.getLocale()));
					toastr.setMessage(UProperties.getMessage("mega.cfdi.notification.toastr.stamp.package.transfer.message", userTools.getLocale()));
					toastr.setType(Toastr.ToastrType.SUCCESS.value());
					
					notification.setToastrNotification(Boolean.TRUE);
					notification.setToastr(toastr);
					
					response.setNotification(notification);
					response.setSuccess(Boolean.TRUE);
				}
			}else{
				errors.add(error);
				response.setError(Boolean.TRUE);
				response.setErrors(errors);
			}

		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(UProperties.getMessage("ERR0114", userTools.getLocale()));
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		this.credentialJson.setResponse(response);
		jsonInString = gson.toJson(this.credentialJson);
		
		return jsonInString;
	}
}
