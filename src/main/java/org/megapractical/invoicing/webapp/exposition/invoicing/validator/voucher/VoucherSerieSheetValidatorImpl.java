package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import javax.persistence.NonUniqueResultException;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPreferencesJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerSerieSheetJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherSerieSheetResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherSerieSheetResponse.JVoucherSerieSheetResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class VoucherSerieSheetValidatorImpl implements VoucherSerieSheetValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ITaxpayerSerieSheetJpaRepository iTaxpayerSerieSheetJpaRepository;
	
	@Autowired
	private IPreferencesJpaRepository iPreferencesJpaRepository;
	
	@Autowired
	private ICfdiJpaRepository iCfdiJpaRepository;
	
	private static final String ERR0100 = "ERR0100";
	private static final String ERR0101 = "ERR0101";
	private static final String SERIE_FIELD = "serie";
	private static final String SHEET_FIELD = "folio";
	
	@Override
	public VoucherSerieSheetResponse getSerieSheetResponse(JVoucher voucher, ContribuyenteEntity taxpayer, List<JError> errors) {
		//##### Serie y folio
		ContribuyenteSerieFolioEntity serieSheet = null;
		val preferences = iPreferencesJpaRepository.findByContribuyente(taxpayer);
		
		val allowIndicateSerieSheet = voucher.isAllowIndicateSerieSheet();
		val allowIndicateSheet = voucher.isAllowIndicateSheet();
		val serieSelectedStr = UBase64.base64Decode(voucher.getSerieSelected());
		var serie = UBase64.base64Decode(voucher.getSerie());
		var sheetStr = UBase64.base64Decode(voucher.getFolio());
		
		JVoucherSerieSheetResponse serieSheetResponse = null;
		if (allowIndicateSerieSheet) {
			serieSheetResponse = getSerieSheet(taxpayer, serie, sheetStr);
		} else if (allowIndicateSheet) {
			serieSheetResponse = getAllowIndicateSheet(preferences, taxpayer, serie, sheetStr);
		} else {
			if (!UValidator.isNullOrEmpty(serieSelectedStr)) {
				serieSheetResponse = getSerieSheetFromSerieSelected(preferences, serieSelectedStr, sheetStr);
				serieSheet = serieSheetResponse.getSerieSheet();
			} else {
				serieSheetResponse = getSerieSheet(taxpayer, serie, sheetStr);
			}
		}
		
		var serieSheetError = false;
		if (serieSheetResponse != null && serieSheetResponse.getError() != null) {
			errors.add(serieSheetResponse.getError());
			serieSheetError = true;
		}
		
		return VoucherSerieSheetResponse
				.builder()
				.serieSheet(serieSheet)
				.serie(serie)
				.sheetStr(sheetStr)
				.allowIndicateSerieSheet(allowIndicateSerieSheet)
				.allowIndicateSheet(allowIndicateSheet)
				.error(serieSheetError)
				.build();
	}
	
	private final JVoucherSerieSheetResponse getSerieSheetFromSerieSelected(PreferenciasEntity preferences,
																			String serieSelectedStr, 
																			String sheetStr) {
		JError error = null;
		val serieSelected = UValue.longValue(serieSelectedStr);
		var serieSheet = iTaxpayerSerieSheetJpaRepository.findByIdAndEliminadoFalse(serieSelected);
		if (UValidator.isNullOrEmpty(serieSheet)) {
			error = JError.error(SERIE_FIELD, ERR0100, userTools.getLocale());
		} else {
			val serie = serieSheet.getSerie(); 
			serieSheet = iTaxpayerSerieSheetJpaRepository.findByIdAndSerieIgnoreCaseAndEliminadoFalse(serieSelected, serie);
			if (UValidator.isNullOrEmpty(serieSheet)) {
				error = JError.error(SERIE_FIELD, ERR0100, userTools.getLocale());
			} else {
				val actualSheet = serieSheet.getFolioActual() + 1;
				val endSheet = serieSheet.getFolioFinal();
				val sheet = Integer.decode(sheetStr);
				if (sheet != actualSheet) {
					error = JError.error(SHEET_FIELD, ERR0101, userTools.getLocale());
				} else {							
					if (!preferences.getIncrementarFolioFinal() && sheet > endSheet) {
						error = JError.error(SHEET_FIELD, "ERR0102", userTools.getLocale());
					}
				}
			}
		}
		return new JVoucherSerieSheetResponse(serieSheet, error);
	}
	
	private final JVoucherSerieSheetResponse getSerieSheet(ContribuyenteEntity taxpayer, String serie, String sheetStr) {
		JError error = null;
		if (UValidator.isNullOrEmpty(serie) && !UValidator.isNullOrEmpty(sheetStr)) {
			error = JError.required(SERIE_FIELD, userTools.getLocale());
		} else if (!UValidator.isNullOrEmpty(serie) && UValidator.isNullOrEmpty(sheetStr)) {
			error = JError.required(SERIE_FIELD, userTools.getLocale());
		} else if (!UValidator.isNullOrEmpty(serie) && !UValidator.isNullOrEmpty(sheetStr)) {
			var existException = false;
			try {
				val cfdi = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndSerieIgnoreCaseAndFolioIgnoreCase(taxpayer, serie, sheetStr);
				if (!UValidator.isNullOrEmpty(cfdi)) {
					existException = true;
				}
			} catch (NonUniqueResultException e) {
				existException = true;
			}
			
			if (existException) {
				error = JError.error(SHEET_FIELD, "ERR0110", userTools.getLocale());
			}
		}
		return JVoucherSerieSheetResponse.builder().error(error).build();
	}
	
	private final JVoucherSerieSheetResponse getAllowIndicateSheet(PreferenciasEntity preferences, 
																   ContribuyenteEntity taxpayer, 
																   String serie, 
																   String sheetStr) {
		JError error = null;
		if (Boolean.FALSE.equals(preferences.getReutilizarFolioCancelado())) {
			error = JError.error(SHEET_FIELD, "ERR0103", userTools.getLocale());
		} else {
			var existException = false;
			try {
				val cfdi = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndSerieIgnoreCaseAndFolioIgnoreCase(taxpayer, serie, sheetStr);
				if (UValidator.isNullOrEmpty(cfdi)) {
					existException = true;
				}
			} catch (NonUniqueResultException e) {
				existException = true;
			}
			
			if (existException) {
				error = JError.error(SHEET_FIELD, "ERR0104", userTools.getLocale());
			}
		}
		return JVoucherSerieSheetResponse.builder().error(error).build();
	}

}