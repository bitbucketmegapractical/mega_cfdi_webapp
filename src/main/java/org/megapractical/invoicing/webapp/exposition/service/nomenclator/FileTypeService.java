package org.megapractical.invoicing.webapp.exposition.service.nomenclator;

import org.megapractical.invoicing.dal.bean.jpa.TipoArchivoEntity;

public interface FileTypeService {
	
	TipoArchivoEntity findByCode(String code);
	
}