package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.megapractical.invoicing.api.wrapper.ApiPaymentError;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoFicheroEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoTramaEntity;
import org.megapractical.invoicing.dal.data.repository.IPaymentDataFileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentEntryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentFieldJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.validator.EntryFileValidator;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentParserValidationServiceImpl implements PaymentParserValidationService {

	private final EntryFileValidator entryFileValidator;
	private final IPaymentDataFileJpaRepository iPaymentDataFileJpaRepository;
	private final IPaymentEntryJpaRepository iPaymentEntryJpaRepository;
	private final IPaymentFieldJpaRepository iPaymentFieldJpaRepository;
	
	public PaymentParserValidationServiceImpl(EntryFileValidator entryFileValidator,
											  IPaymentDataFileJpaRepository iPaymentDataFileJpaRepository,
											  IPaymentEntryJpaRepository iPaymentEntryJpaRepository,
											  IPaymentFieldJpaRepository iPaymentFieldJpaRepository) {
		this.entryFileValidator = entryFileValidator;
		this.iPaymentDataFileJpaRepository = iPaymentDataFileJpaRepository;
		this.iPaymentEntryJpaRepository = iPaymentEntryJpaRepository;
		this.iPaymentFieldJpaRepository = iPaymentFieldJpaRepository;
	}
	
	private static final String PAYMENT_FILE_CODE = "CPV20";
	
	private ComplementoPagoFicheroEntity paymentDataFile;
	private List<ComplementoPagoTramaEntity> paymentEntries;
	
	@PostConstruct
	private void init() {
		paymentDataFile = iPaymentDataFileJpaRepository.findByCodigoIgnoreCaseAndEliminadoFalse(PAYMENT_FILE_CODE);
		paymentEntries = iPaymentEntryJpaRepository.findByComplementoPagoFicheroAndEliminadoFalse(paymentDataFile);
	}
	
	@Override
	public ComplementoPagoFicheroEntity getPaymentDataFile() {
		return paymentDataFile;
	}

	@Override
	public Integer getLength(String[] entry) {
		if (entry != null) {
			return paymentEntries.stream()
					  		 	 .filter(i -> i.getNombre().equalsIgnoreCase(entry[0]))
					  		 	 .map(ComplementoPagoTramaEntity::getLongitud)
					  		 	 .findFirst().orElse(null);
		}
		return null;
	}

	@Override
	public boolean entryValidate(String[] entry) {
		if (entry != null) {
			return entry.length == getLength(entry);
		}
		return false;
	}

	@Override
	public boolean isEmpty(String[] entry) {
		if (entry != null && entry.length > 0) {
			val entryLength = getLength(entry) - 1;
			for (int i = 1; i <= entryLength; i ++) {
				if (!entry[i].isEmpty()) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public List<ApiPaymentError> validateField(String[] entry) {
		if (entry != null && entry.length > 0) {
			val paymentEntry = iPaymentEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(entry[0]);
			val fields = iPaymentFieldJpaRepository.findByComplementoPagoTramaAndEliminadoFalse(paymentEntry);
			if (!fields.isEmpty()) {
				val paymentErrors = new ArrayList<ApiPaymentError>();
				
				val entryLength = getLength(entry) - 1;
				for (int i = 1; i <= entryLength; i ++) {
					val paymentField = iPaymentFieldJpaRepository.findByComplementoPagoTramaAndPosicionAndEliminadoFalse(paymentEntry, i);
					val validation = entryFileValidator.validateContent(paymentEntry, paymentField, entry[i]);
					if (!validation) {
						val paymentError = ApiPaymentError.contentError(paymentField, entry, i);
						paymentErrors.add(paymentError);
					}
				}
				return paymentErrors;
			}
		}
		return Collections.emptyList();
	}

}