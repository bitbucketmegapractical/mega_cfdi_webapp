package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.smarterweb.SWRetentionStamp;
import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UZipper;
import org.megapractical.invoicing.api.validator.AppWebValidator;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.api.wrapper.ApiRetentionError;
import org.megapractical.invoicing.api.wrapper.ApiRetentionParser;
import org.megapractical.invoicing.api.wrapper.ApiRetentionStamp;
import org.megapractical.invoicing.api.wrapper.ApiRetentionStampResponse;
import org.megapractical.invoicing.api.wrapper.ApiRetentionVoucher;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionEntity;
import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.megapractical.invoicing.dal.data.repository.IFileStatusJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionFileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@EnableAsync
@Service
@Scope("session")
public class RetentionStampService {

	@Autowired
	private AppLog appLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	private TimelineLog timelineLog;
	
	@Autowired
	private RetentionParserService retentionParser;
	
	@Autowired
	private RetentionFileBuilderService retentionBuilder;
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private IPersistenceDAO persistenceDAO;
	
	@Autowired
	private StampControlService stampControl;
	
	@Autowired
	private ReportManager reportManager; 
	
	@Resource
	private IFileStatusJpaRepository iFileStatusJpaRepository;
	
	@Resource
	private IStampJpaRepository iStampJpaRepository;
	
	@Resource
	private IRetentionFileJpaRepository iRetentionFileJpaRepository;
	
	//##### Estado de archivo: En cola
	private static final String FILE_STATUS_QUEUED = "C";
	
	//##### Estado de archivo: Procesando
	private static final String FILE_STATUS_PROCESSING = "P";
	
	//##### Estado de archivo: Generado
	private static final String FILE_STATUS_GENERATED = "G";
	
	@Async
	public Future<Boolean> retentionProcessingAsync(final ApiRetentionStamp retentionStamp) {
		try {
			
			System.out.println("-----------------------------------------------------------------------------------------------------------");
			System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION STAMP SERVICE > RETENTION PROCESSING ASYNC STARTING...");
			
			//##### Registrando archivo en DB
			persistenceDAO.persist(retentionStamp.getRetentionFile());
			
			//##### Procesado archivo
			retentionProcessing(retentionStamp);									
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("-----------------------------------------------------------------------------------------------------------");
		System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION STAMP SERVICE > RETENTION PROCESSING ASYNC FINISHED");
		
		return new AsyncResult<Boolean>(Boolean.TRUE);
	}
	
	@Transactional
	private void retentionProcessing(final ApiRetentionStamp retentionStamp) {
		try {
			
			//##### Cambiando estado del archivo a Procesando
			EstadoArchivoEntity fileStatus = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_PROCESSING);
			retentionStamp.getRetentionFile().setEstadoArchivo(fileStatus);
			
			//##### Actualizando archivo
			persistenceDAO.update(retentionStamp.getRetentionFile());
			
			//##### Procesando el fichero
			ApiRetentionParser wRetentionParser = retentionParser.retentionParser(retentionStamp);
			
			//##### Listado de errores
			List<ApiRetentionError> errors = wRetentionParser.getErrors();
			if(UValidator.isNullOrEmpty(errors)) {
				errors = new ArrayList<>(); 
			}
			
			//##### Determinando la cantidad de retenciones a timbrar
			Integer retentionToStamp = wRetentionParser.getVouchers().size();
			
			//##### Obteniendo la cantidad de timbres disponibles del emisor
			Integer emitterAvailableStamp = iStampJpaRepository.findByContribuyente(wRetentionParser.getRetentionStamp().getTaxpayer()).getTimbresDisponibles();
			
			//##### Listado auxialiar de comprobantes no timbrados
			//##### En caso que el codigo de error no sea reconocido, intentar retimbrar
			List<ApiRetentionVoucher> auxiliaryVouchers = new ArrayList<>();
			
			if(!UValidator.isNullOrEmpty(retentionToStamp)) {
				if(retentionToStamp <= emitterAvailableStamp) {
					//##### Indicando que el emisor tiene suficientes timbres para timbrar todas las retenciones del archivo
					wRetentionParser.getRetentionStamp().getRetentionFile().setTimbresDisponibles(Boolean.TRUE);
					
					//##### Actualizando archivo
					persistenceDAO.update(wRetentionParser.getRetentionStamp().getRetentionFile());										
					
					//##### Generando comprobantes
					if(!UValidator.isNullOrEmpty(wRetentionParser)) {
						List<ApiRetentionVoucher> vouchers = retentionBuilder.retentionBuild(wRetentionParser);
						if(!vouchers.isEmpty()) {
							//##### Obteniendo la ruta absoluta del archivo
							String folderAbsolutePath = retentionStamp.getAbsolutePath();
							
							//##### Obteniendo la ruta en db
							String dbPath = retentionStamp.getDbPath();
							
							//##### Generando rutas
							String retentionXmlPdf = folderAbsolutePath + File.separator + "STAMP";
							File retentionXmlPdfAbsolutePath = new File(retentionXmlPdf); 
							if(!retentionXmlPdfAbsolutePath.exists()){
								retentionXmlPdfAbsolutePath.mkdirs();
							}
							
							//##### Contador de retenciones timbradas
							Integer retentionStamped = 0;
							
							//##### Información de retenciones
							String periodInitialMonth = null;
							String periodEndMonth = null;
							String periodYear = null;
							
							//##### Timbrando comprobantes
							for (ApiRetentionVoucher item : vouchers) {
								//##### Verificando si el emisor tiene timbres disponibles
								if( stampControl.stampStatus(retentionStamp.getTaxpayer())) {
									ApiRetentionStampResponse retentionStampResponse = voucherStamp(item, retentionStamp, retentionXmlPdf);
									Boolean stamped = true;
									if(!UValidator.isNullOrEmpty(retentionStampResponse.getVoucher())) {
										auxiliaryVouchers.add(retentionStampResponse.getVoucher());
										stamped = false;
									}
									
									if(!UValidator.isNullOrEmpty(retentionStampResponse.getError())){
										errors.add(retentionStampResponse.getError());
										stamped = false;
									}
									
									if(stamped){
										retentionStamped ++;
										
										if(periodInitialMonth == null) {
											periodInitialMonth = retentionStampResponse.getPeriodInitialMonth();
											periodEndMonth = retentionStampResponse.getPeriodEndMonth();
											periodYear = retentionStampResponse.getPeriodYear();
										}
									}
								} else {
									String receiverRfc = !UValidator.isNullOrEmpty(item.getApiRetention().getReceiverNatRfc()) ? item.getApiRetention().getReceiverNatRfc() : "";
									String receiverName = !UValidator.isNullOrEmpty(item.getApiRetention().getReceiverNatBusinessName()) ? item.getApiRetention().getReceiverNatBusinessName() : item.getApiRetention().getReceiverExtBusinessName();
									String receiverCurp = !UValidator.isNullOrEmpty(item.getApiRetention().getReceiverNatCurp()) ? item.getApiRetention().getReceiverNatCurp() : "";
									
									ApiRetentionError error = new ApiRetentionError();
									error.setStampError(Boolean.TRUE);
									error.setReceiverRfc(receiverRfc);
									error.setReceiverName(receiverName);
									error.setReceiverCurp(receiverCurp);
									error.setStampErrorCode("ERR0163");
									error.setStampErrorMessage(UProperties.getMessage("ERR0163", userTools.getLocale()));
									error.setDescription("Timbres insuficientes.");
									
									errors.add(error);
									break;
								}							
							}
							
							//##### Verificando si quedó alguna retención sin timbrar
							if(!auxiliaryVouchers.isEmpty()) {
								for (ApiRetentionVoucher item : auxiliaryVouchers) {
									String receiverRfc = !UValidator.isNullOrEmpty(item.getApiRetention().getReceiverNatRfc()) ? item.getApiRetention().getReceiverNatRfc() : "";
									String receiverName = !UValidator.isNullOrEmpty(item.getApiRetention().getReceiverNatBusinessName()) ? item.getApiRetention().getReceiverNatBusinessName() : item.getApiRetention().getReceiverExtBusinessName();
									String receiverCurp = !UValidator.isNullOrEmpty(item.getApiRetention().getReceiverNatCurp()) ? item.getApiRetention().getReceiverNatCurp() : "";
									
									ApiRetentionError error = new ApiRetentionError();
									error.setStampError(Boolean.TRUE);
									error.setReceiverRfc(receiverRfc);
									error.setReceiverName(receiverName);
									error.setReceiverCurp(receiverCurp);
									error.setDescription("Error de timbrado.");
									error.setStampErrorCode("ERR0037");
									error.setStampErrorMessage(UProperties.getMessage("ERR0037", userTools.getLocale()));
									
									errors.add(error);
								}
							}
							
							//##### Verificando si hay errores
							if(!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()) {
								//##### Generando ruta de almacenamiento del error
								String errorPath = folderAbsolutePath + File.separator + "ERROR";
								File folderError = new File(errorPath);
					    		if(!folderError.exists()){
					    			folderError.mkdirs();
					    		}					    							    						    						    		
								
								//##### Fichero de error a generar
								String fileErrorPath = errorPath + File.separator + retentionStamp.getRetentionFile().getNombre() + "_ERROR.txt";
								
								//##### Generando fichero de error
								ApiRetentionError.writeRetentionError(fileErrorPath, errors);
								
								//##### Actualizando la ruta de error en el archivo
								String errorDbPath = dbPath + File.separator + "ERROR" + File.separator + retentionStamp.getRetentionFile().getNombre() + "_ERROR.txt";
								wRetentionParser.getRetentionStamp().getRetentionFile().setRutaError(errorDbPath);
							}
							
							//##### Cargando las configuraciones de archivo del usuario
							ArchivoRetencionConfiguracionEntity retentionFileConfig = retentionStamp.getRetentionFileConfiguration();
							
							if(retentionStamped >= 1) {
								//##### Verificar si se genera compactado						
								if(retentionFileConfig.getCompactado()) {
									String zipDirectory = retentionStamp.getAbsolutePath() + File.separator + "STAMP";
									String zipFile = zipDirectory + ".zip";
									String zipDbPath = dbPath + File.separator + "STAMP.zip";
									
									try {
										
										System.out.println("-----------------------------------------------------------------------------------------------------------");
										System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION STAMP SERVICE > ZIPPING DIRECTORY: " + zipDirectory);
										
										UZipper.zipping(new File(zipDirectory), zipFile);								
										
										//##### Actualizando ruta de compactado
										wRetentionParser.getRetentionStamp().getRetentionFile().setRutaCompactado(zipDbPath);
										
										System.out.println("-----------------------------------------------------------------------------------------------------------");
										System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION STAMP SERVICE > ZIPPING DIRECTORY FINISHED");																				
										
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
								
								wRetentionParser.getRetentionStamp().getRetentionFile().setPeriodoMesInicial(periodInitialMonth);
								wRetentionParser.getRetentionStamp().getRetentionFile().setPeriodoMesFinal(periodEndMonth);
								wRetentionParser.getRetentionStamp().getRetentionFile().setPeriodoEjercicio(periodYear);
							}
							
							//##### Cambiando estado del archivo a Generado
							EstadoArchivoEntity fileStatusGenerated = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);
							wRetentionParser.getRetentionStamp().getRetentionFile().setEstadoArchivo(fileStatusGenerated);
							
							//##### Actualizando archivo
							persistenceDAO.update(wRetentionParser.getRetentionStamp().getRetentionFile());
							
							//##### Verificar si hay algun otro archivo en cola
							List<ArchivoRetencionEntity> retentionFileList = iRetentionFileJpaRepository.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(retentionStamp.getTaxpayer(), FILE_STATUS_QUEUED);
							Boolean finded = false;
							//##### Verificando si el emisor tiene timbres disponibles
							if(stampControl.stampStatus(retentionStamp.getTaxpayer())) {
								for (ArchivoRetencionEntity item : retentionFileList) {
									if(item.getEstadoArchivo().getCodigo().equalsIgnoreCase("C")){
										//##### Si el archivo fue puesto en cola porque se estaba procesando otro
										if(UValidator.isNullOrEmpty(item.getTimbresDisponibles())){
											item.setTimbresDisponibles(Boolean.TRUE);
										}
										
										//##### Cargando el archivo
										retentionStamp.setRetentionFile(item);
										
										//##### Ruta de las retenciones
										String retentionPath = appSettings.getPropertyValue("cfdi.retention.path");
										
										//##### Ruta db del archivo
										String folderPath = retentionStamp.getTaxpayer().getRfcActivo() + File.separator + UDate.date().replaceAll("-", "") + File.separator + item.getNombre();
										retentionStamp.setDbPath(folderPath);
										
										//##### Ruta absoluta del archivo
										folderAbsolutePath = retentionPath + File.separator + folderPath;
										retentionStamp.setAbsolutePath(folderAbsolutePath);
										
										//##### Ruta fisica del archivo
										String retentionUploadPath = retentionPath + File.separator + item.getRuta();
										retentionStamp.setUploadPath(retentionUploadPath);
										
										//##### Actualizando informacion de timbres
										retentionStamp.setStampDetail(stampControl.stampDetails(retentionStamp.getTaxpayer()));
										
										finded = true;
										break;
									}
								}
								
								//##### Procesando el archivo
								if(finded) {
									retentionProcessing(retentionStamp);
								}
							}
						} else {
							//##### Verificando si hay errores
							if(!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()){
								//##### Obteniendo la ruta absoluta del archivo
								String folderAbsolutePath = retentionStamp.getAbsolutePath();
								
								//##### Obteniendo la ruta en db
								String dbPath = retentionStamp.getDbPath();							
								
								//##### Generando ruta de almacenamiento del error
								String errorPath = folderAbsolutePath + File.separator + "ERROR";
								File folderError = new File(errorPath);
					    		if(!folderError.exists()){
					    			folderError.mkdirs();
					    		}
								
								//##### Fichero de error a generar
								String fileErrorPath = errorPath + File.separator + retentionStamp.getRetentionFile().getNombre() + "_ERROR.txt";
								
								//##### Generando fichero de error
								ApiRetentionError.writeRetentionError(fileErrorPath, errors);
								
								//##### Actualizando la ruta de error en el archivo
								String errorDbPath = dbPath + File.separator + "ERROR" + File.separator + retentionStamp.getRetentionFile().getNombre() + "_ERROR.txt";
								wRetentionParser.getRetentionStamp().getRetentionFile().setRutaError(errorDbPath);
							}
							
							//##### Cambiando estado del archivo a Cola nuevamente
							EstadoArchivoEntity fileStatusGenerated = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);
							wRetentionParser.getRetentionStamp().getRetentionFile().setEstadoArchivo(fileStatusGenerated);
							
							//##### Actualizando archivo
							persistenceDAO.update(wRetentionParser.getRetentionStamp().getRetentionFile());
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[ERROR] RETENTION STAMP SERVICE > NO VOUCHERS WERE GENERATED");
						}
					}
				} else {
					//##### Indicando que el emisor no tiene suficientes timbres para timbrar todas las retenciones del archivo
					wRetentionParser.getRetentionStamp().getRetentionFile().setTimbresDisponibles(Boolean.FALSE);
					
					//##### Cambiando estado del archivo a Cola nuevamente
					EstadoArchivoEntity fileStatusQueued = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_QUEUED);
					wRetentionParser.getRetentionStamp().getRetentionFile().setEstadoArchivo(fileStatusQueued);
					
					//##### Actualizando archivo
					persistenceDAO.update(wRetentionParser.getRetentionStamp().getRetentionFile());
					
					System.out.println("-----------------------------------------------------------------------------------------------------------");
					System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[ERROR] RETENTION STAMP SERVICE > THE EMITTER HASN'T SUFFICIENT STAMPS");
				}
			} else {
				//##### Obteniendo la ruta absoluta del archivo
				String folderAbsolutePath = retentionStamp.getAbsolutePath();
				
				//##### Obteniendo la ruta en db
				String dbPath = retentionStamp.getDbPath();
				
				//##### Verificando si hay errores
				if(!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()) {
					//##### Generando ruta de almacenamiento del error
					String errorPath = folderAbsolutePath + File.separator + "ERROR";
					File folderError = new File(errorPath);
		    		if(!folderError.exists()) {
		    			folderError.mkdirs();
		    		}
		    		
					//##### Fichero de error a generar
					String fileErrorPath = errorPath + File.separator + retentionStamp.getRetentionFile().getNombre() + "_ERROR.txt";
					
					//##### Generando fichero de error
					ApiRetentionError.writeRetentionError(fileErrorPath, errors);
					
					//##### Actualizando la ruta de error en el archivo
					String errorDbPath = dbPath + File.separator + "ERROR" + File.separator + retentionStamp.getRetentionFile().getNombre() + "_ERROR.txt";
					wRetentionParser.getRetentionStamp().getRetentionFile().setRutaError(errorDbPath);
				}
				
				//##### Cambiando estado del archivo a Generado
				EstadoArchivoEntity fileStatusGenerated = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);
				wRetentionParser.getRetentionStamp().getRetentionFile().setEstadoArchivo(fileStatusGenerated);
				
				//##### Actualizando archivo
				persistenceDAO.update(wRetentionParser.getRetentionStamp().getRetentionFile());
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
	}
	
	@Transactional
	private ApiRetentionStampResponse voucherStamp(final ApiRetentionVoucher retentionVoucher, final ApiRetentionStamp retentionStamp, final String xmlPdfFolder) {
		ApiRetentionStampResponse retentionStampResponse = new ApiRetentionStampResponse();
		try {
			
			//##### Obteniendo el comprobante
			Retenciones retention = retentionVoucher.getRetencion();
			retention.setFechaExp(UDateTime.format(LocalDateTime.now()));
			
			//##### Archivo
			ArchivoRetencionEntity retentionFile = retentionStamp.getRetentionFile();
			
			//##### Obteniendo informacion asociada a la retención
			ApiRetention apiRetention = retentionVoucher.getApiRetention();
			apiRetention.setRetentionFile(retentionFile);
			
			//##### Cargando el comprobante en las propiedades de timbrado
			StampProperties stampProperties = new StampProperties();
			stampProperties = retentionStamp.getStampProperties();
			stampProperties.setRetention(retention);
			
			String receiverRfc = !UValidator.isNullOrEmpty(apiRetention.getReceiverNatRfc()) ? apiRetention.getReceiverNatRfc() : "";
			String receiverName = !UValidator.isNullOrEmpty(apiRetention.getReceiverNatBusinessName()) ? apiRetention.getReceiverNatBusinessName() : apiRetention.getReceiverExtBusinessName();
			String receiverCurp = !UValidator.isNullOrEmpty(apiRetention.getReceiverNatCurp()) ? apiRetention.getReceiverNatCurp() : "";
			
			try {
				
				//##### Timbrando el comprobante
				StampResponse stampResponse = null;
				//stampResponse = RetentionStamp.retentionStamp(stampProperties);
				
				//##### Timbrado SW
				stampResponse = SWRetentionStamp.retentionStamp(stampProperties);
				if(stampResponse.isStamped()) {
					try {
						
						apiRetention.setStampResponse(stampResponse);
						Retenciones retStamped = stampResponse.getRetention();
						String fileName = stampResponse.getUuid();
						
						//##### XML  
						System.out.println("-----------------------------------------------------------------------------------------------------------");
						System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION STAMP SERVICE > STORING XML...");
						
                        String rutaXml = xmlPdfFolder + File.separator + fileName  + ".xml";
                        System.out.println("Escribiendo archivo en: " + rutaXml);
                        UFile.writeFile(stampResponse.getXml(), rutaXml);
                        
                        //##### PDF
                        System.out.println("-----------------------------------------------------------------------------------------------------------");
						System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION STAMP SERVICE > STORING PDF...");
						
                        String pathPdf = xmlPdfFolder + File.separator + fileName  + ".pdf";
                        reportManager.retV2GeneratePdf(apiRetention, pathPdf);
                        
                        
                        //##### Cargando las preferencias de la entidad
						/*ArchivoRetencionConfiguracionEntity retentionFileConfiguration = retentionStamp.getRetentionFileConfiguration();
						if(!UValidator.isNullOrEmpty(retentionFileConfiguration)){
							//##### Verificar si se envia correo de notificacion al trabajador
							if(retentionFileConfiguration.getEnvioXmlPdf() || retentionFileConfiguration.getEnvioXml() || retentionFileConfiguration.getEnvioPdf()){
								
							}
						}*/
						
						//##### Stamp control
						stampControl.stampControlAsync(retentionStamp.getTaxpayer(), retentionStamp.getUser(), retentionStamp.getUrlAcceso(), retentionStamp.getIp());
						
						//##### Bitacora :: Registro de accion en bitacora
						appLog.logOperation(OperationCode.GENERACION_CFDIV33_NOMV12_MASTER_ID71, retentionStamp.getUser(), retentionStamp.getUrlAcceso(), retentionStamp.getIp());
						
						//##### Timeline :: Registro de accion
						timelineLog.timelineLog(EventTypeCode.GENERACION_CFDIV33_SIN_COMPLEMENTO_MASTER_ID4, retentionStamp.getTaxpayer(), retentionStamp.getUser(), retentionStamp.getIp());
						
						retentionStampResponse.setVoucher(null);
						retentionStampResponse.setError(null);
						retentionStampResponse.setPeriodInitialMonth(String.valueOf(retStamped.getPeriodo().getMesIni()));
						retentionStampResponse.setPeriodEndMonth(String.valueOf(retStamped.getPeriodo().getMesFin()));
						retentionStampResponse.setPeriodYear(String.valueOf(retStamped.getPeriodo().getEjercicio()));
						
					} catch (Exception e) {
						ApiRetentionError error = new ApiRetentionError();
						error.setUnexpectedError(Boolean.TRUE);
						error.setReceiverRfc(receiverRfc);
						error.setReceiverName(receiverName);
						error.setReceiverCurp(receiverCurp);
						error.setDescription("Ha ocurrido un error inesperado gestionando la información del xml y/o pdf de la retención.");
						retentionStampResponse.setError(error);
					}
				} else {
					AppWebValidator validator = Validator.stampValidate(stampResponse);
					if(!UValidator.isNullOrEmpty(validator)){
						ApiRetentionError error = new ApiRetentionError();
						error.setStampError(Boolean.TRUE);
						error.setReceiverRfc(receiverRfc);
						error.setReceiverName(receiverName);
						error.setReceiverCurp(receiverCurp);
						error.setDescription("Error de validación de contenido de la retención.");
						error.setStampErrorCode(validator.getErrorCode());
						error.setStampErrorMessage(validator.getErrorMessage());
						retentionStampResponse.setError(error);
						
					} else {
						retentionStampResponse.setVoucher(retentionVoucher);
					}
				}
				
			} catch (Exception e) {
				ApiRetentionError error = new ApiRetentionError();
				error.setUnexpectedError(Boolean.TRUE);
				error.setReceiverRfc(receiverRfc);
				error.setReceiverName(receiverName);
				error.setReceiverCurp(receiverCurp);
				error.setDescription("Ha ocurrido un error inesperado timbrando la retención.");
				
				retentionStampResponse.setError(error);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			retentionStampResponse.setVoucher(retentionVoucher);
		}
		
		return retentionStampResponse;
	}
}
