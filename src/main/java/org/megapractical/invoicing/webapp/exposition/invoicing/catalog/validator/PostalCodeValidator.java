package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.PostalCodeValidatorResponse;

public interface PostalCodeValidator {
	
	PostalCodeValidatorResponse validate(String postalCode);
	
	PostalCodeValidatorResponse validate(String postalCode, String field);
	
}