package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.megapractical.invoicing.dal.data.repository.IPostalCodeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.PostalCodeValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class PostalCodeValidatorImpl implements PostalCodeValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private IPostalCodeJpaRepository iPostalCodeJpaRepository;
	
	@Override
	public PostalCodeValidatorResponse validate(String postalCode) {
		CodigoPostalEntity postalCodeEntity = null;
		var hasError = false;
		
		if (UValidator.isNullOrEmpty(postalCode)) {
			hasError = true;
		} else {
			// Validar que sea un valor de catalogo
			postalCodeEntity = iPostalCodeJpaRepository.findByCodigoAndEliminadoFalse(postalCode);
			if (postalCodeEntity == null) {
				hasError = true;
			}
		}
		
		return new PostalCodeValidatorResponse(postalCodeEntity, hasError);
	}
	
	@Override
	public PostalCodeValidatorResponse validate(String postalCode, String field) {
		CodigoPostalEntity postalCodeEntity = null;
		JError error = null;
		
		if (UValidator.isNullOrEmpty(postalCode)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			// Validar que sea un valor de catalogo
			postalCode = UCatalog.getCode(postalCode); 
			postalCodeEntity = iPostalCodeJpaRepository.findByCodigoAndEliminadoFalse(postalCode);
			if (postalCodeEntity == null) {
				val values = new String[]{"CFDI33125", "LugarExpedicion", "c_CodigoPostal"};
				val errorMessage = UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values);
				error = JError.error(field, errorMessage);
			}
		}
		
		return new PostalCodeValidatorResponse(postalCodeEntity, error);
	}

}