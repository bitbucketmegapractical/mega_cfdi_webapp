package org.megapractical.invoicing.webapp.exposition.retention.helper;

import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.webapp.exposition.retention.payload.RetentionComplementResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.springframework.stereotype.Component;

import lombok.val;

@Component
public class RetentionComplementHelper {
	
	private final RetentionComplementDivHelper retentionComplementDivHelper;
	private final RetentionComplementEnaHelper retentionComplementEnaHelper;
	private final RetentionComplementIntHelper retentionComplementIntHelper;
	private final RetentionComplementOcdHelper retentionComplementOcdHelper;
	private final RetentionComplementPexHelper retentionComplementPexHelper;
	private final RetentionComplementSfiHelper retentionComplementSfiHelper;
	
	public RetentionComplementHelper(RetentionComplementDivHelper retentionComplementDivHelper,
									 RetentionComplementEnaHelper retentionComplementEnaHelper,
									 RetentionComplementIntHelper retentionComplementIntHelper,
									 RetentionComplementOcdHelper retentionComplementOcdHelper,
									 RetentionComplementPexHelper retentionComplementPexHelper, 
									 RetentionComplementSfiHelper retentionComplementSfiHelper) {
		this.retentionComplementDivHelper = retentionComplementDivHelper;
		this.retentionComplementEnaHelper = retentionComplementEnaHelper;
		this.retentionComplementIntHelper = retentionComplementIntHelper;
		this.retentionComplementOcdHelper = retentionComplementOcdHelper;
		this.retentionComplementPexHelper = retentionComplementPexHelper;
		this.retentionComplementSfiHelper = retentionComplementSfiHelper;
	}

	public RetentionComplementResponse populateComplement(RetentionData retentionData, String retentionKey,
			final ApiRetention apiRetention, final List<JError> errors) {
		// Complemento :: Dividendos
		val retDivResponse = retentionComplementDivHelper.populate(retentionData, retentionKey, apiRetention, errors);

		// Complemento :: Enajenación de acciones
		val retEnaResponse = retentionComplementEnaHelper.populate(retentionData, retentionKey, apiRetention, errors);

		// Complemento Intereses
		val retIntResponse = retentionComplementIntHelper.populate(retentionData, retentionKey, apiRetention, errors);

		// Complemento :: Operaciones con Derivados
		val retOcdResponse = retentionComplementOcdHelper.populate(retentionData, retentionKey, apiRetention, errors);

		// Complemento :: Pagos a extranjeros
		val retPexResponse = retentionComplementPexHelper.populate(retentionData, retentionKey, apiRetention, errors);

		// Complemento :: Sector financiero
		val retSfiResponse = retentionComplementSfiHelper.populate(retentionData, retentionKey, apiRetention, errors);

		val isRetentionWithComplement = retDivResponse.isRetentionWithComplement()
				|| retEnaResponse.isRetentionWithComplement() || retIntResponse.isRetentionWithComplement()
				|| retOcdResponse.isRetentionWithComplement() || retPexResponse.isRetentionWithComplement()
				|| retSfiResponse.isRetentionWithComplement();
		
		val isSectionError = retDivResponse.isSectionError() || retEnaResponse.isSectionError()
				|| retIntResponse.isSectionError() || retOcdResponse.isSectionError() || retPexResponse.isSectionError()
				|| retSfiResponse.isSectionError();

		return RetentionComplementResponse
				.builder()
				.retentionWithComplement(isRetentionWithComplement)
				.sectionError(isSectionError)
				.build();
	}
	
}