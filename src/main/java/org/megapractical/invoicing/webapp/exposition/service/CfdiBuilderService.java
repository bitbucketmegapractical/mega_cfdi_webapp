package org.megapractical.invoicing.webapp.exposition.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiCmp;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment;
import org.megapractical.invoicing.api.wrapper.ApiPayment.Payment.RelatedDocument;
import org.megapractical.invoicing.sat.complement.foreignexchange.Divisas;
import org.megapractical.invoicing.sat.complement.payments.Pagos;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.DoctoRelacionado;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.DoctoRelacionado.ImpuestosDR;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.DoctoRelacionado.ImpuestosDR.RetencionesDR;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.DoctoRelacionado.ImpuestosDR.RetencionesDR.RetencionDR;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.DoctoRelacionado.ImpuestosDR.TrasladosDR;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.DoctoRelacionado.ImpuestosDR.TrasladosDR.TrasladoDR;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.ImpuestosP;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.ImpuestosP.RetencionesP;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.ImpuestosP.RetencionesP.RetencionP;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.ImpuestosP.TrasladosP;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Pago.ImpuestosP.TrasladosP.TrasladoP;
import org.megapractical.invoicing.sat.complement.payments.Pagos.Totales;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Addenda;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.CfdiRelacionados;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.CfdiRelacionados.CfdiRelacionado;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Complemento;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.ComplementoConcepto;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Impuestos;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Impuestos.Retenciones;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Impuestos.Retenciones.Retencion;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Impuestos.Traslados;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Impuestos.Traslados.Traslado;
import org.megapractical.invoicing.voucher.catalogs.CTipoFactor;
import org.megapractical.invoicing.webapp.constant.ForeignExchangeConstants;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.AllowStamp;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ProductTaxesResponse;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.PaymentUtils;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

@Service
@RequiredArgsConstructor
public class CfdiBuilderService {

	private final UserTools userTools;
	private final CfdiService cfdiService;
	private final VoucherBuilderService voucherBuilderService;

	public AllowStamp voucherBuild(ApiCfdi apiCfdi, boolean useDateByPostalCode) {
		try {
			val rfc = UValue.stringValueUppercase(apiCfdi.getReceiver().getRfc());
			val serie = UValue.stringValue(apiCfdi.getVoucher().getSerie());
			val folio = UValue.stringValue(apiCfdi.getVoucher().getFolio());
			val allowStamp = cfdiService.verifyCfdi(rfc, serie, folio);
			if (allowStamp.isAllowedToStamp()) {
				val voucher = new Comprobante();
				userTools.log("[INFO] CFDI BUILDER SERVICE > BUILDING VOUCHER...");
				
				voucherBuilderService.populateEmitter(apiCfdi, voucher);
				voucherBuilderService.populateReceiver(apiCfdi, voucher);
				val productTaxesResponse = populateConcepts(apiCfdi, voucher);
				setComplement(apiCfdi, voucher);
				setAddenda(apiCfdi, voucher);
				voucherBuilderService.populateVoucher(apiCfdi, voucher, useDateByPostalCode);
				setVoucherTaxes(apiCfdi, voucher, productTaxesResponse);
				setRelatedCfdis(apiCfdi, voucher);
				
				userTools.log("[INFO] CFDI BUILDER SERVICE > VOUCHER BUILDED SUCCESSFULY");
				allowStamp.setVoucher(voucher);
				return allowStamp;
			}
			return allowStamp;
		} catch (Exception e) {
			userTools.log("[ERROR] CFDI BUILDER SERVICE > ERROR BUILDING VOUCHER");
			e.printStackTrace();
		}
		return AllowStamp.notAllowed();
	}

	private ProductTaxesResponse populateConcepts(ApiCfdi apiCfdi, final Comprobante voucher) {
		val voucherTaxesTransferred = new ArrayList<Traslado>();
		val voucherTaxesWithheld = new ArrayList<Retencion>();
		val concepts = new Comprobante.Conceptos();
		for (val item : apiCfdi.getConcepts()) {
			val concepto = new Comprobante.Conceptos.Concepto();
			val impuestos = new Comprobante.Conceptos.Concepto.Impuestos();
			
			setConceptData(item, concepto);
			setConceptTransferredTaxes(apiCfdi, voucherTaxesTransferred, item, concepto, impuestos);
			setConceptWithheldTaxes(apiCfdi, voucherTaxesWithheld, item, concepto, impuestos);
			
			concepts.getConceptos().add(concepto);
		}
		voucher.setConceptos(concepts);
		
		return ProductTaxesResponse
				.builder()
				.transferreds(voucherTaxesTransferred)
				.withhelds(voucherTaxesWithheld)
				.build();
	}

	private void setConceptData(final ApiConcept item, final Comprobante.Conceptos.Concepto concepto) {
		concepto.setClaveProdServ(UValue.stringValue(item.getKeyProductServiceCode()));
		concepto.setCantidad(item.getQuantity());
		concepto.setClaveUnidad(UValue.stringValue(item.getMeasurementUnitCode()));
		concepto.setDescripcion(UValue.stringValue(UValue.singleReplace(item.getDescription())));
		concepto.setValorUnitario(item.getUnitValue());
		concepto.setImporte(item.getAmount());
		concepto.setObjetoImp(item.getObjImp());
		concepto.setNoIdentificacion(UValue.stringValue(item.getIdentificationNumber()));
		concepto.setUnidad(UValue.stringValue(item.getUnit()));
		concepto.setDescuento(UValue.getBigDecimal(item.getDiscount()));
		
		if (item.getCustomsInformation() != null) {
			concepto.getInformacionAduaneras().addAll(item.getCustomsInformation());
		}

		if (item.getPropertyAccount() != null) {
			concepto.getCuentaPredial().add(item.getPropertyAccount());
		}
		
		if (item.getParteSources() != null) {
			concepto.getPartes().addAll(item.getParteSources());
		}
		
		setComplementConcept(item, concepto);
	}
	
	private void setConceptTransferredTaxes(ApiCfdi apiCfdi,
											final List<Comprobante.Impuestos.Traslados.Traslado> voucherTaxesTransferred,
											final ApiConcept item,
											final Comprobante.Conceptos.Concepto concepto,
											final Comprobante.Conceptos.Concepto.Impuestos impuestos) {
		val transferredTaxes = new ArrayList<ApiConceptTaxes>();
		if (item.getTaxesTransferred() != null) {
			val taxesTransferreds = new Comprobante.Conceptos.Concepto.Impuestos.Traslados();
			for (val tax : item.getTaxesTransferred()) {
				val taxCatalog = UCatalog.getCatalog(tax.getTax());
				val taxCode = taxCatalog[0];
				val taxValue = taxCatalog[1];

				//  Informacion de los impuestos trasladados del concepto
				val transferred = new Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado();
				transferred.setBase(tax.getBaseBigDecimal());
				transferred.setImporte(tax.getAmountBigDecimal());
				transferred.setImpuesto(taxCode);
				transferred.setTasaOCuota(tax.getRateOrFeeBigDecimal());
				transferred.setTipoFactor(tax.getFactorType());
				taxesTransferreds.getTraslados().add(transferred);

				//  Informacion de los impuestos trasladados del comprobante
				val transferredObj = new Traslado();
				transferredObj.setBase(tax.getBaseBigDecimal());
				transferredObj.setImporte(tax.getAmountBigDecimal());
				transferredObj.setImpuesto(taxCode);
				transferredObj.setTasaOCuota(tax.getRateOrFeeBigDecimal());
				transferredObj.setTipoFactor(tax.getFactorType());

				var transferredEqual = false;
				for (int i = 0; i < voucherTaxesTransferred.size(); i++) {
					val transferredCheck = voucherTaxesTransferred.get(i);
					val taxCheck = transferredCheck.getImpuesto();
					val factorCheck = transferredCheck.getTipoFactor().value();
					val rateOrFeeCheck = UValue.bigDecimalString(transferredCheck.getTasaOCuota());

					val transferredFactor = tax.getFactor();
					val transferredRateOrFee = tax.getRateOrFee();

					if (taxCheck.equals(taxCode) && factorCheck.equals(transferredFactor)
							&& rateOrFeeCheck.equals(transferredRateOrFee)) {
						val amount = transferredCheck.getImporte().add(tax.getAmountBigDecimal());
						voucherTaxesTransferred.get(i).setImporte(amount);
						transferredEqual = true;
					}
				}
				
				if (!transferredEqual) {
					voucherTaxesTransferred.add(transferredObj);
				}

				//  Informacion de los impuestos trasladados para la impresion del PDF
				tax.setTax(taxValue);
				transferredTaxes.add(tax);

				impuestos.setTraslados(taxesTransferreds);
				concepto.setImpuestos(impuestos);
			}
			apiCfdi.getTransferred().addAll(transferredTaxes);
		}
	}

	private void setConceptWithheldTaxes(ApiCfdi apiCfdi,
										 final List<Comprobante.Impuestos.Retenciones.Retencion> voucherTaxesWithheld,
										 final ApiConcept item,
										 final Comprobante.Conceptos.Concepto concepto,
										 final Comprobante.Conceptos.Concepto.Impuestos impuestos) {
		val withheldTaxes = new ArrayList<ApiConceptTaxes>();
		if (item.getTaxesWithheld() != null) {
			val taxesWithhelds = new Comprobante.Conceptos.Concepto.Impuestos.Retenciones();
			for (val tax : item.getTaxesWithheld()) {
				val taxCatalog = UCatalog.getCatalog(tax.getTax());
				val taxCode = taxCatalog[0];
				val taxValue = taxCatalog[1];

				//  Informacion de los impuestos retenidos del concepto
				val withheld = new Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion();
				withheld.setBase(tax.getBaseBigDecimal());
				withheld.setImporte(tax.getAmountBigDecimal());
				withheld.setImpuesto(taxCode);
				withheld.setTasaOCuota(tax.getRateOrFeeBigDecimal());
				withheld.setTipoFactor(tax.getFactorType());
				taxesWithhelds.getRetencions().add(withheld);

				//  Informacion de los impuestos retenidos del comprobante
				val withheldObj = new Retencion();
				withheldObj.setImporte(tax.getAmountBigDecimal());
				withheldObj.setImpuesto(taxCode);

				var withheldEqual = false;
				for (int i = 0; i < voucherTaxesWithheld.size(); i++) {
					val withheldCheck = voucherTaxesWithheld.get(i);
					val taxCheck = withheldCheck.getImpuesto();

					if (taxCheck.equals(taxCode)) {
						val amount = withheldCheck.getImporte().add(tax.getAmountBigDecimal());
						voucherTaxesWithheld.get(i).setImporte(amount);
						withheldEqual = true;
					}
				}
				if (!withheldEqual) {
					voucherTaxesWithheld.add(withheldObj);
				}

				//  Informacion de los impuestos retenidos para la impresion del PDF
				tax.setTax(taxValue);
				withheldTaxes.add(tax);

				impuestos.setRetenciones(taxesWithhelds);
				concepto.setImpuestos(impuestos);
			}
			apiCfdi.getWithheld().addAll(withheldTaxes);
		}
	}
	
	private void setComplementConcept(final ApiConcept item, final Comprobante.Conceptos.Concepto concepto) {
		ComplementoConcepto cc = null;
		if (item.getIedu() != null) {
			cc = new ComplementoConcepto();
			cc.getAnies().add(item.getIedu());
			concepto.setComplementoConcepto(cc);
		}
	}
	
	private void setComplement(ApiCfdi apiCfdi, final Comprobante voucher) {
		if (apiCfdi.getComplement() != null) {
			val complemento = fillComplement(apiCfdi.getComplement());
			voucher.getComplementos().add(complemento);
		}
	}
	
	private Complemento fillComplement(ApiCmp apiComplement) {
		try {

			val complemento = new Complemento();
			if (PaymentUtils.PAYMENT_PACKAGE.equalsIgnoreCase(apiComplement.getPackageClass())) {
				val apiCmpPayment = apiComplement.getApiPayment();
				
				val pagos = new Pagos();
				pagos.setVersion(PaymentUtils.VERSION);
				fillPaymentTotals(pagos, apiCmpPayment);
				
				apiCmpPayment.getPayments().forEach(item -> {
					val pago = fillPayment(item);
					setPaymentTaxes(item, pago);
					
					val doctoRelacionados = fillRelatedDocuments(item);
					pago.getDoctoRelacionados().addAll(doctoRelacionados);
					pagos.getPagos().add(pago);
				});
				complemento.getAnies().add(pagos);
			} else if (ForeignExchangeConstants.PACKAGE.equalsIgnoreCase(apiComplement.getPackageClass())) {
				val apiCmpForeignExchange = apiComplement.getForeignExchange();
				
				val foreignExchange = new Divisas();
				foreignExchange.setVersion(ForeignExchangeConstants.CURRENT_VERSION);
				foreignExchange.setTipoOperacion(apiCmpForeignExchange.getOperationType());
				complemento.getAnies().add(foreignExchange);
			}

			return complemento;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void fillPaymentTotals(final Pagos pagos, final ApiPayment apiPayment) {
		val paymentTotals = apiPayment.getPaymentTotal();
		if (paymentTotals != null) {
			val totals = new Totales();
			totals.setTotalRetencionesIVA(paymentTotals.getTotalWithheldIva());
			totals.setTotalRetencionesISR(paymentTotals.getTotalWithheldIsr());
			totals.setTotalRetencionesIEPS(paymentTotals.getTotalWithheldIeps());
			totals.setTotalTrasladosBaseIVA16(paymentTotals.getTotalTransferredBaseIva16());
			totals.setTotalTrasladosImpuestoIVA16(paymentTotals.getTotalTransferredTaxIva16());
			totals.setTotalTrasladosBaseIVA8(paymentTotals.getTotalTransferredBaseIva8());
			totals.setTotalTrasladosImpuestoIVA8(paymentTotals.getTotalTransferredTaxIva8());
			totals.setTotalTrasladosBaseIVA0(paymentTotals.getTotalTransferredBaseIva0());
			totals.setTotalTrasladosImpuestoIVA0(paymentTotals.getTotalTransferredTaxIva0());
			totals.setTotalTrasladosBaseIVAExento(paymentTotals.getTotalTransferredBaseIvaExempt());
			totals.setMontoTotalPagos(paymentTotals.getTotalAmountPayments());
			pagos.setTotales(totals);
		}
	}

	private Pago fillPayment(Payment item) {
		val payment = new Pago();
		payment.setFechaPago(item.getPaymentDate());
		payment.setFormaDePagoP(item.getPaymentWay().value());
		payment.setMonto(item.getAmount());
		payment.setMonedaP(item.getCurrency());
		
		if (!UValidator.isNullOrEmpty(item.getChangeType())) {
			payment.setTipoCambioP(item.getChangeType());
		}

		if (!UValidator.isNullOrEmpty(item.getOperationNumber())) {
			payment.setNumOperacion(item.getOperationNumber());
		}

		if (!UValidator.isNullOrEmpty(item.getSourceAccountRfc())) {
			payment.setRfcEmisorCtaOrd(item.getSourceAccountRfc());
		}

		if (!UValidator.isNullOrEmpty(item.getPayerAccount())) {
			payment.setCtaOrdenante(item.getPayerAccount());
		}

		if (!UValidator.isNullOrEmpty(item.getBankName())) {
			payment.setNomBancoOrdExt(item.getBankName());
		}

		if (!UValidator.isNullOrEmpty(item.getTargetAccountRfc())) {
			payment.setRfcEmisorCtaBen(item.getTargetAccountRfc());
		}

		if (!UValidator.isNullOrEmpty(item.getReceiverAccount())) {
			payment.setCtaBeneficiario(item.getReceiverAccount());
		}

		if (!UValidator.isNullOrEmpty(item.getStringType())) {
			payment.setTipoCadPago(item.getStringTypeCode());
		}

		if (!UValidator.isNullOrEmpty(item.getOriginalString())) {
			payment.setCadPago(item.getOriginalString());
		}

		if (!UValidator.isNullOrEmpty(item.getPaymentCertificate())) {
			payment.setCertPago(item.getPaymentCertificate());
		}

		if (!UValidator.isNullOrEmpty(item.getPaymentSeal())) {
			payment.setSelloPago(item.getPaymentSeal());
		}
		
		return payment;
	}
	
	private void setPaymentTaxes(Payment item, final Pago pago) {
		val taxesTransferreds = getPaymentTransferredTaxes(item);
		val taxesWithhelds = getPaymentWithheldTaxes(item);
		
		val impuestos = new ImpuestosP();
		var hasPaymentTaxes = false;
		
		if (taxesTransferreds != null) {
			impuestos.setTrasladosP(taxesTransferreds);
			hasPaymentTaxes = true;
		}
		
		if (taxesWithhelds != null) {
			impuestos.setRetencionesP(taxesWithhelds);
			hasPaymentTaxes = true;
		}
		
		if (hasPaymentTaxes) {
			pago.setImpuestosP(impuestos);
		}
	}
	
	private TrasladosP getPaymentTransferredTaxes(final Payment item) {
		if (item.getTransferreds() != null && !item.getTransferreds().isEmpty()) {
			val transferreds = new ArrayList<TrasladoP>();
			for (val tax : item.getTransferreds()) {
				val taxCatalog = UCatalog.getCatalog(tax.getTax());
				val taxCode = taxCatalog[0];

				// Informacion de los impuestos trasladados del pago
				val transferredObj = new TrasladoP();
				transferredObj.setBaseP(tax.getBaseBigDecimal());
				transferredObj.setImporteP(tax.getAmountBigDecimal());
				transferredObj.setImpuestoP(taxCode);
				transferredObj.setTasaOCuotaP(tax.getRateOrFeeBigDecimal());
				transferredObj.setTipoFactorP(tax.getFactorType());

				var transferredEqual = false;
				for (int i = 0; i < transferreds.size(); i++) {
					val transferredCheck = transferreds.get(i);
					val taxCheck = transferredCheck.getImpuestoP();
					val factorCheck = transferredCheck.getTipoFactorP().value();
					val rateOrFeeCheck = UValue.bigDecimalString(transferredCheck.getTasaOCuotaP());

					val transferredFactor = tax.getFactor();
					val transferredRateOrFee = tax.getRateOrFee();

					if (taxCheck.equals(taxCode) && factorCheck.equals(transferredFactor)
							&& rateOrFeeCheck.equals(transferredRateOrFee)) {
						val amount = transferredCheck.getImporteP().add(tax.getAmountBigDecimal());
						transferreds.get(i).setImporteP(amount);
						transferredEqual = true;
					}
				}

				if (!transferredEqual) {
					transferreds.add(transferredObj);
				}
			}
			
			val taxesTransferreds = new TrasladosP();
			taxesTransferreds.getTrasladoP().addAll(transferreds);
			
			return taxesTransferreds;
		}
		return null;
	}
	
	private RetencionesP getPaymentWithheldTaxes(final Payment item) {
		if (item.getWithhelds() != null && !item.getWithhelds().isEmpty()) {
			val withhelds = new ArrayList<RetencionP>();
			for (val tax : item.getWithhelds()) {
				val taxCatalog = UCatalog.getCatalog(tax.getTax());
				val taxCode = taxCatalog[0];

				// Informacion de los impuestos retenidos del pago
				val withheldObj = new RetencionP();
				withheldObj.setImporteP(tax.getAmountBigDecimal());
				withheldObj.setImpuestoP(taxCode);

				var withheldEqual = false;
				for (int i = 0; i < withhelds.size(); i++) {
					val withheldCheck = withhelds.get(i);
					val taxCheck = withheldCheck.getImpuestoP();

					if (taxCheck.equals(taxCode)) {
						val amount = withheldCheck.getImporteP().add(tax.getAmountBigDecimal());
						withhelds.get(i).setImporteP(amount);
						withheldEqual = true;
					}
				}
				
				if (!withheldEqual) {
					withhelds.add(withheldObj);
				}
			}
			
			val taxesWithhelds = new RetencionesP();
			taxesWithhelds.getRetencionP().addAll(withhelds);
			return taxesWithhelds;
		}
		return null;
	}
	
	private List<DoctoRelacionado> fillRelatedDocuments(Payment item) {
		val doctoRelacionados = new ArrayList<DoctoRelacionado>();
		item.getRelatedDocuments().forEach(document -> {
			val doctoRelacionado = new DoctoRelacionado();
			doctoRelacionado.setIdDocumento(document.getDocumentId());
			doctoRelacionado.setMonedaDR(document.getCurrency());
			doctoRelacionado.setNumParcialidad(UValue.bigInteger(document.getPartiality()));
			doctoRelacionado.setImpSaldoAnt(document.getAmountPartialityBefore());
			doctoRelacionado.setImpPagado(document.getAmountPaid());
			doctoRelacionado.setImpSaldoInsoluto(document.getDifference());
			doctoRelacionado.setObjetoImpDR(document.getTaxObjectCode());
			
			if (!UValidator.isNullOrEmpty(document.getSerie())) {
				doctoRelacionado.setSerie(document.getSerie());
			}
			
			if (!UValidator.isNullOrEmpty(document.getSheet())) {
				doctoRelacionado.setFolio(document.getSheet());
			}
			
			if(!UValidator.isNullOrEmpty(document.getChangeType())){
				doctoRelacionado.setEquivalenciaDR(document.getChangeType());
			}
			
			setRelatedDocumentTaxes(document, doctoRelacionado);
			
			doctoRelacionados.add(doctoRelacionado);
		});
		return doctoRelacionados;
	}

	private void setRelatedDocumentTaxes(RelatedDocument document, final DoctoRelacionado doctoRelacionado) {
		val taxesTransferreds = getRelatedDocumentTransferredTaxes(document);
		val taxesWithhelds = getRelatedDocumentWithheldTaxes(document);
		
		val impuestos = new ImpuestosDR();
		var hasRelatedDocTaxes = false;
		
		if (taxesTransferreds != null) {
			impuestos.setTrasladosDR(taxesTransferreds);
			hasRelatedDocTaxes = true;
		}
		
		if (taxesWithhelds != null) {
			impuestos.setRetencionesDR(taxesWithhelds);
			hasRelatedDocTaxes = true;
		}
		
		if (hasRelatedDocTaxes) {
			doctoRelacionado.setImpuestosDR(impuestos);
		}
	}
	
	private TrasladosDR getRelatedDocumentTransferredTaxes(final RelatedDocument item) {
		if (item.getTransferreds() != null && !item.getTransferreds().isEmpty()) {
			val transferreds = new ArrayList<TrasladoDR>();
			for (val tax : item.getTransferreds()) {
				val taxCatalog = UCatalog.getCatalog(tax.getTax());
				val taxCode = taxCatalog[0];

				// Informacion de los impuestos trasladados del documento relacionado
				val transferredObj = new TrasladoDR();
				transferredObj.setBaseDR(tax.getBaseBigDecimal());
				transferredObj.setImporteDR(tax.getAmountBigDecimal());
				transferredObj.setImpuestoDR(taxCode);
				transferredObj.setTasaOCuotaDR(tax.getRateOrFeeBigDecimal());
				transferredObj.setTipoFactorDR(tax.getFactorType());

				var transferredEqual = false;
				for (int i = 0; i < transferreds.size(); i++) {
					val transferredCheck = transferreds.get(i);
					val taxCheck = transferredCheck.getImpuestoDR();
					val factorCheck = transferredCheck.getTipoFactorDR().value();
					val rateOrFeeCheck = UValue.bigDecimalString(transferredCheck.getTasaOCuotaDR());

					val transferredFactor = tax.getFactor();
					val transferredRateOrFee = tax.getRateOrFee();

					if (taxCheck.equals(taxCode) && factorCheck.equals(transferredFactor)
							&& rateOrFeeCheck.equals(transferredRateOrFee)) {
						val amount = transferredCheck.getImporteDR().add(tax.getAmountBigDecimal());
						transferreds.get(i).setImporteDR(amount);
						transferredEqual = true;
					}
				}

				if (!transferredEqual) {
					transferreds.add(transferredObj);
				}
			}
			
			val taxesTransferreds = new TrasladosDR();
			taxesTransferreds.getTrasladoDR().addAll(transferreds);
			
			return taxesTransferreds;
		}
		return null;
	}
	
	private RetencionesDR getRelatedDocumentWithheldTaxes(final RelatedDocument item) {
		if (item.getWithhelds() != null && !item.getWithhelds().isEmpty()) {
			val withhelds = new ArrayList<RetencionDR>();
			for (val tax : item.getWithhelds()) {
				val taxCatalog = UCatalog.getCatalog(tax.getTax());
				val taxCode = taxCatalog[0];

				// Informacion de los impuestos retenidos del documento relacionado
				val withheldObj = new RetencionDR();
				withheldObj.setImporteDR(tax.getAmountBigDecimal());
				withheldObj.setImpuestoDR(taxCode);
				withheldObj.setImpuestoDR(taxCode);
				withheldObj.setTasaOCuotaDR(tax.getRateOrFeeBigDecimal());
				withheldObj.setTipoFactorDR(tax.getFactorType());

				var withheldEqual = false;
				for (int i = 0; i < withhelds.size(); i++) {
					val withheldCheck = withhelds.get(i);
					val taxCheck = withheldCheck.getImpuestoDR();

					if (taxCheck.equals(taxCode)) {
						val amount = withheldCheck.getImporteDR().add(tax.getAmountBigDecimal());
						withhelds.get(i).setImporteDR(amount);
						withheldEqual = true;
					}
				}
				
				if (!withheldEqual) {
					withhelds.add(withheldObj);
				}
			}
			
			val taxesWithhelds = new RetencionesDR();
			taxesWithhelds.getRetencionDR().addAll(withhelds);
			
			return taxesWithhelds;
		}
		return null;
	}
	
	private void setAddenda(ApiCfdi apiCfdi, final Comprobante voucher) {
		if (!UValidator.isNullOrEmpty(apiCfdi.getVoucher().getAddenda())) {
			val addenda = new Addenda();
			addenda.getAnies().add(apiCfdi.getVoucher().getAddenda());
			voucher.setAddenda(addenda);
		}
	}
	
	private void setVoucherTaxes(ApiCfdi apiCfdi, 
								 final Comprobante voucher,
								 final ProductTaxesResponse productTaxesResponse) {
		val voucherTaxes = new Impuestos();
		val taxes = apiCfdi.getVoucher().getTaxes();
		if (taxes != null) {
			if (taxes.getTotalTaxTransferred() != null) {
				voucherTaxes.setTotalImpuestosTrasladados(taxes.getTotalTaxTransferred());
			}
			
			if (taxes.getTotalTaxWithheld() != null) {
				voucherTaxes.setTotalImpuestosRetenidos(taxes.getTotalTaxWithheld());
			}
		}
		
		if (productTaxesResponse.getTransferreds() != null && !productTaxesResponse.getTransferreds().isEmpty()) {
			val totalExemptTaxes = productTaxesResponse.getTransferreds().stream()
																		 .filter(tax -> tax.getTipoFactor().equals(CTipoFactor.EXENTO))
																		 .count();
			List<Traslado> taxTransferredList;
			if (totalExemptTaxes == productTaxesResponse.getTransferreds().size()) {
				taxTransferredList = productTaxesResponse.getTransferreds().stream()
						.filter(tax -> tax.getTasaOCuota() == null && tax.getImporte() == null)
						.collect(Collectors
								.groupingBy(tax -> tax.getImpuesto() + tax.getTipoFactor(),
										Collectors.reducing(new Traslado(BigDecimal.ZERO), Traslado::excemptReduce)))
						.values().stream().collect(Collectors.toList());
			} else {
				taxTransferredList = productTaxesResponse.getTransferreds().stream()
						.filter(tax -> tax.getTasaOCuota() != null)
						.collect(Collectors
								.groupingBy(tax -> tax.getImpuesto() + tax.getTipoFactor() + tax.getTasaOCuota(),
										Collectors.reducing(new Traslado(BigDecimal.ZERO, "", null, BigDecimal.ZERO,
												BigDecimal.ZERO), Traslado::reduce)))
						.values().stream().collect(Collectors.toList());
			}
			
			val taxTransferreds = new Traslados();
			if (taxTransferredList != null && !taxTransferredList.isEmpty()) {
				taxTransferreds.getTraslados().addAll(taxTransferredList);
				voucherTaxes.setTraslados(taxTransferreds);
			}
			voucher.setImpuestos(voucherTaxes);
		}
		
		if (productTaxesResponse.getWithhelds() != null && !productTaxesResponse.getWithhelds().isEmpty()) {
			val withhelds = productTaxesResponse.getWithhelds().stream()
					.collect(Collectors.groupingBy(Retencion::getImpuesto,
							Collectors.reducing(new Retencion("", BigDecimal.ZERO), Retencion::reduce)))
					.values().stream().collect(Collectors.toList());
			
			val taxWithhelds = new Retenciones();
			taxWithhelds.getRetencions().addAll(withhelds);
			voucherTaxes.setRetenciones(taxWithhelds);
			voucher.setImpuestos(voucherTaxes);
		}
	}
	
	private void setRelatedCfdis(ApiCfdi apiCfdi, final Comprobante voucher) {
		if (apiCfdi.getVoucher().getRelatedCfdi() != null) {
			val uuids = new ArrayList<CfdiRelacionado>();
			for (val item : apiCfdi.getVoucher().getRelatedCfdi().getUuids()) {
				val relatedCfdi = new CfdiRelacionado();
				relatedCfdi.setUUID(item.getUuid());
				uuids.add(relatedCfdi);
			}
			
			val relatedCfdi = new CfdiRelacionados();
			relatedCfdi.setTipoRelacion(apiCfdi.getVoucher().getRelatedCfdi().getRelationshipTypeCode());
			relatedCfdi.getCfdiRelacionados().addAll(uuids);
			voucher.setCfdiRelacionados(relatedCfdi);
		}
	}

}