package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UCalculation;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.CurrencyValidator;
import org.megapractical.invoicing.webapp.json.JCalculation;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.wrapper.WAutomaticCalculations;
import org.megapractical.invoicing.webapp.wrapper.WConcept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class InvoiceAutomaticCalculationServiceImpl implements InvoiceAutomaticCalculationService {

	@Autowired
	private CurrencyValidator currencyValidator;
	
	@Override
	public JCalculation automaticCalculations(String currency, 
											  List<WConcept> wConcepts,
											  WAutomaticCalculations wAutomaticCalculations) {
		JVoucher voucher = null;
		val currencyValidatorResponse = currencyValidator.validate(currency, null);
		if (!currencyValidatorResponse.isHasError()) {
			val currencyEntity = currencyValidatorResponse.getCurrency();
			//##### Actualizando calculos automaticos
			String valueNull = null;
			
			var voucherSubtotal = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
			var voucherDiscount = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
			var voucherTaxTransferred = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
			var voucherTaxWithheld = UValue.bigDecimal(valueNull, currencyEntity.getDecimales());
			
			for (val item : wConcepts) {
				// Subtotal
				voucherSubtotal = voucherSubtotal.add(UValue.bigDecimalStrict(item.getAmount(), currencyEntity.getDecimales()));
				
				// Descuento
				if (!UValidator.isNullOrEmpty(item.getDiscount())) {
					voucherDiscount = voucherDiscount.add(UValue.bigDecimalStrict(item.getDiscount(), currencyEntity.getDecimales()));
				}
				
				// Total de impuestos trasladados
				for (val transferred : item.getTaxesTransferred()) {
					voucherTaxTransferred = voucherTaxTransferred.add(transferred.getAmount());
				}
				
				// Total de impuestos retenidos
				for (val withheld : item.getTaxesWithheld()) {
					voucherTaxWithheld = voucherTaxWithheld.add(withheld.getAmount());
				}
			}
			val voucherTotal = UCalculation.total(voucherSubtotal, voucherDiscount, voucherTaxTransferred, voucherTaxWithheld, currencyEntity.getDecimales());
			
			wAutomaticCalculations.setVoucherSubtotal(voucherSubtotal);
			wAutomaticCalculations.setVoucherDiscount(voucherDiscount);
			wAutomaticCalculations.setVoucherTaxTransferred(voucherTaxTransferred);
			wAutomaticCalculations.setVoucherTaxWithheld(voucherTaxWithheld);
			wAutomaticCalculations.setVoucherTotal(voucherTotal);
			
			voucher = JVoucher
						.builder()
						.subTotal(UBase64.base64Encode(UValue.bigDecimalString(voucherSubtotal)))
						.discount(UBase64.base64Encode(UValue.bigDecimalString(voucherDiscount)))
						.totalTaxesTransferred(UBase64.base64Encode(UValue.bigDecimalString(voucherTaxTransferred)))
						.totalTaxesWithheld(UBase64.base64Encode(UValue.bigDecimalString(voucherTaxWithheld)))
						.total(UBase64.base64Encode(UValue.bigDecimalString(voucherTotal)))
						.build();
		} else {
			voucher = new JVoucher();
		}
		return new JCalculation(voucher);
	}

}