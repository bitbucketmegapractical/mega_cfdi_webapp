package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.common.ApiConsolidate;
import org.megapractical.invoicing.api.smarterweb.SWCfdiStamp;
import org.megapractical.invoicing.api.util.ApiFileErrorWritter;
import org.megapractical.invoicing.api.util.UConsolidate;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UZipper;
import org.megapractical.invoicing.api.validator.AppWebValidator;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.api.wrapper.ApiPayrollError;
import org.megapractical.invoicing.api.wrapper.ApiPayrollParser;
import org.megapractical.invoicing.api.wrapper.ApiPayrollStamp;
import org.megapractical.invoicing.api.wrapper.ApiPayrollStampResponse;
import org.megapractical.invoicing.api.wrapper.ApiPayrollVoucher;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEstadisticaEntity;
import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.data.repository.IFileStatusJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollFileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollFileStatisticsJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStampJpaRepository;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WCfdiNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.val;
import lombok.var;

@EnableAsync
@Service
@Scope("session")
public class PayrollStampService {

	@Autowired
	AppLog appLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	PayrollParserService payrollParser;
	
	@Autowired
	PayrollEntryBuilderService payrollBuilder;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	DataStoreService dataStore;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	StampControlService stampControl;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	ReportManager reportManager;
	
	@Autowired
	PayrollService payrollService;
	
	@Resource
	IFileStatusJpaRepository iFileStatusJpaRepository;
	
	@Resource
	IPayrollFileJpaRepository iPayrollFileJpaRepository;
	
	@Resource
	IPayrollFileStatisticsJpaRepository iPayrollFileStatisticsJpaRepository;
	
	@Resource
	IStampJpaRepository iStampJpaRepository;
	
	//##### Estado de archivo: En cola
	private static final String FILE_STATUS_QUEUED = "C";
	
	//##### Estado de archivo: Procesando
	private static final String FILE_STATUS_PROCESSING = "P";
	
	//##### Estado de archivo: Generado
	private static final String FILE_STATUS_GENERATED = "G";
	
	@Async
	public Future<Boolean> payrollProcessingAsync(ApiPayrollStamp payrollStamp, Boolean canUploadPayroll) {
		try {
			
			System.out.println("-----------------------------------------------------------------------------------------------------------");
			System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] PAYROLL STAMP SERVICE > PAYROLL PROCESSING ASYNC STARTING...");
			
			//##### Registrando archivo en DB
			persistenceDAO.persist(payrollStamp.getPayrollFile());
			
			//##### Procesado archivo
			payrollPrcessing(payrollStamp, canUploadPayroll);									
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("-----------------------------------------------------------------------------------------------------------");
		System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] PAYROLL STAMP SERVICE > PAYROLL PROCESSING ASYNC FINISHED");
		
		return new AsyncResult<Boolean>(Boolean.TRUE);
	}
	
	@Transactional
	private void payrollPrcessing(ApiPayrollStamp payrollStamp, Boolean canUploadPayroll) {
		try {
			
			//##### Cambiando estado del archivo a Procesando
			EstadoArchivoEntity fileStatus = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_PROCESSING);
			payrollStamp.getPayrollFile().setEstadoArchivo(fileStatus);
			
			//##### Actualizando archivo
			persistenceDAO.update(payrollStamp.getPayrollFile());
			
			//##### Procesando el fichero
			ApiPayrollParser wPayrollParser = payrollParser.payrollParser(payrollStamp);
			
			//##### Listado de errores
			List<ApiPayrollError> errors = wPayrollParser.getErrors();
			if (UValidator.isNullOrEmpty(errors)) {
				errors = new ArrayList<>(); 
			}
			
			//##### Determinando la cantidad de nominas a timbrar
			Integer payrollToStamp = wPayrollParser.getStatistics().getSingleEntrie();
			
			//##### Obteniendo la cantidad de timbres disponibles del emisor
			Integer emitterAvailableStamp = iStampJpaRepository.findByContribuyente(wPayrollParser.getPayrollStamp().getTaxpayer()).getTimbresDisponibles();//wPayrollParser.getPayrollStamp().getStampDetail().getTimbresDisponibles();
			
			//##### Listado auxiliar de comprobantes no timbrados
			//##### En caso que el codigo de error no sea reconocido, intentar retimbrar
			List<ApiPayrollVoucher> auxiliaryVouchers = new ArrayList<>();
			
			if (!UValidator.isNullOrEmpty(payrollToStamp)) {
				if (payrollToStamp <= emitterAvailableStamp || canUploadPayroll) {
					//##### Indicando que el emisor tiene suficientes timbres para timbrar todas las nominas del archivo
					wPayrollParser.getPayrollStamp().getPayrollFile().setTimbresDisponibles(Boolean.TRUE);
					
					//##### Actualizando archivo
					persistenceDAO.update(wPayrollParser.getPayrollStamp().getPayrollFile());
					
					//##### Generando comprobantes
					if (!UValidator.isNullOrEmpty(wPayrollParser)) {
						List<ApiPayrollVoucher> vouchers = new ArrayList<>();
						vouchers.addAll(payrollBuilder.payrollBuild(wPayrollParser));
						
						//##### Estadisticas generadas del archivo
						ArchivoNominaEstadisticaEntity payrollFileStatistics = managePayrollFileStatistics(wPayrollParser);
						
						//##### Cargando las estadisticas del archivo
						payrollStamp.setPayrollFileStatistics(payrollFileStatistics);
						
						if (!vouchers.isEmpty()) {
							// Obteniendo el nombre del archivo
							val payrollFileName = payrollStamp.getPayrollFile().getNombre();
							
							//##### Obteniendo la ruta absoluta del archivo
							String folderAbsolutePath = payrollStamp.getAbsolutePath();
							
							//##### Obteniendo la ruta en db
							String dbPath = payrollStamp.getDbPath();
							
							//##### Generando rutas segun la configuracion del usuario
							String xmlFolder = null;
							String xmlDb = null;
							
							String pdfFolder = null;
							String pdfDb = null;
							
							if (payrollStamp.getPayrollFileConfiguration().getDirectorioXmlPdfSeparado()) {
								xmlFolder = folderAbsolutePath + File.separator + payrollFileName + File.separator + "XML";
								xmlDb = dbPath + File.separator + payrollFileName + File.separator + "XML";
								
								File xmlAbsolutePath = new File(xmlFolder); 
								if (!xmlAbsolutePath.exists()) {
									xmlAbsolutePath.mkdirs();
								}
								
								pdfFolder = folderAbsolutePath + File.separator + payrollFileName + File.separator + "PDF";
								pdfDb = dbPath + File.separator + payrollFileName + File.separator + "PDF";
								
								File pdfAbsolutePath = new File(pdfFolder); 
								if (!pdfAbsolutePath.exists()) {
									pdfAbsolutePath.mkdirs();
								}
							} else {
								String payrollXmlPdf = folderAbsolutePath + File.separator + payrollFileName;
								xmlDb = dbPath + File.separator + payrollFileName;
								pdfDb = dbPath + File.separator + payrollFileName;
								
								File payrollXmlPdfAbsolutePath = new File(payrollXmlPdf); 
								if (!payrollXmlPdfAbsolutePath.exists()) {
									payrollXmlPdfAbsolutePath.mkdirs();
								}
								
								xmlFolder = payrollXmlPdf;
								pdfFolder = payrollXmlPdf;
								 
							}
							
							//##### Contador de nominas timbradas
							Integer payrollStamped = 0;
																					
							
							//##### Timbrando comprobantes
							for (ApiPayrollVoucher item : vouchers) {
								//##### Verificando si el emisor tiene timbres disponibles
								if ( stampControl.stampStatus(payrollStamp.getTaxpayer()) || canUploadPayroll ) {
									ApiPayrollStampResponse payrollStampResponse = voucherStamp(item, payrollStamp, xmlFolder, pdfFolder, xmlDb, pdfDb);
									Boolean stamped = true;
									if (!UValidator.isNullOrEmpty(payrollStampResponse.getVoucher())) {
										auxiliaryVouchers.add(payrollStampResponse.getVoucher());
										stamped = false;
									}
									
									if (!UValidator.isNullOrEmpty(payrollStampResponse.getError())) {
										errors.add(payrollStampResponse.getError());
										stamped = false;
									}
									
									payrollStamp.setPayrollFileStatistics(payrollStampResponse.getStatistics());
									
									if (stamped) {
										payrollStamped ++;
									}
								} else {
									ApiPayrollError error = new ApiPayrollError();
									error.setStampError(Boolean.TRUE);
									error.setReceiverRfc(item.getCfdi().getReceiver().getRfc());
									error.setReceiverName(item.getCfdi().getReceiver().getNombreRazonSocial());
									error.setReceiverCurp(item.getCfdi().getReceiver().getCurp());
									error.setStampErrorCode("ERR0163");
									error.setStampErrorMessage(UProperties.getMessage("ERR0163", userTools.getLocale()));
									error.setDescription("Timbres insuficientes.");
									
									errors.add(error);
									
									break;
								}							
							}
							
							//##### Retimbrando comprobantes con errores de timbrado no identificados
							//##### Hacer maximo 3 intentos de retimbrado
							/*Integer attempt = 0;
							while (attempt < 3 && !auxiliaryVouchers.isEmpty()) {
								Boolean error = false;
								Boolean stampAvailableError = false;
								
								List<ApiPayrollVoucher> temporalList = new ArrayList<>();							
								for (ApiPayrollVoucher item : auxiliaryVouchers) {
									//##### Verificando si el emisor tiene timbres disponibles
									if (stampControl.stampStatus(payrollStamp.getTaxpayer())) {
										ApiPayrollStampResponse payrollStampResponse = voucherAttemptStamp(item, payrollStamp, xmlFolder, pdfFolder, xmlDb, pdfDb);
										Boolean stamped = true;
										if (!UValidator.isNullOrEmpty(payrollStampResponse.getVoucher())) {
											temporalList.add(payrollStampResponse.getVoucher());
											stamped = false;
										}
										
										if (!UValidator.isNullOrEmpty(payrollStampResponse.getError())) {
											errors.add(payrollStampResponse.getError());
											stamped = false;
										}
										
										payrollStamp.setPayrollFileStatistics(payrollStampResponse.getStatistics());
										
										if (stamped) {
											payrollStamped ++;
										}
									} else {
										ApiPayrollError wPayrollError = new ApiPayrollError();
										wPayrollError.setStampError(Boolean.TRUE);
										wPayrollError.setReceiverRfc(item.getCfdi().getReceiver().getRfc());
										wPayrollError.setReceiverName(item.getCfdi().getReceiver().getNombreRazonSocial());
										wPayrollError.setReceiverCurp(item.getCfdi().getReceiver().getCurp());
										wPayrollError.setStampErrorCode("ERR0163");
										wPayrollError.setStampErrorMessage(UProperties.getMessage("ERR0163", userTools.getLocale()));
										wPayrollError.setDescription("Timbres insuficientes.");
										
										errors.add(wPayrollError);
										stampAvailableError = true;
										break;
									}
								}
								
								if (stampAvailableError) {
									break;
								}
								
								if (error) {
									auxiliaryVouchers = new ArrayList<>();
									auxiliaryVouchers.addAll(temporalList);
									attempt ++;
								} else {
									break;
								}
							}*/
							
							//##### Verificando si quedo alguna nomina sin timbrar
							if (!auxiliaryVouchers.isEmpty()) {
								for (ApiPayrollVoucher item : auxiliaryVouchers) {
									ApiCfdi cfdi = item.getCfdi();
									
									ApiPayrollError error = new ApiPayrollError();
									error.setStampError(Boolean.TRUE);
									error.setReceiverRfc(cfdi.getReceiver().getRfc());
									error.setReceiverName(cfdi.getReceiver().getNombreRazonSocial());
									error.setReceiverCurp(cfdi.getReceiver().getCurp());
									error.setDescription("Error de timbrado.");
									error.setStampErrorCode("ERR0037");
									error.setStampErrorMessage(UProperties.getMessage("ERR0037", userTools.getLocale()));
									
									errors.add(error);
									
									//##### Actualizando estadisticas del archivo
									Integer payrollStampedError = UValidator.isNullOrEmpty(payrollStamp.getPayrollFileStatistics().getNominasError()) ? 0 : payrollStamp.getPayrollFileStatistics().getNominasError();
									payrollStampedError ++;
									payrollStamp.getPayrollFileStatistics().setNominasError(payrollStampedError);
									persistenceDAO.update(payrollStamp.getPayrollFileStatistics());
								}
							}
							
							//##### Verificando si hay errores
							if (!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()) {
								//##### Generando ruta de almacenamiento del error
								String errorPath = folderAbsolutePath + File.separator + "ERROR";
								File folderError = new File(errorPath);
					    		if (!folderError.exists()) {
					    			folderError.mkdirs();
					    		}					    							    						    						    		
								
								//##### Fichero de error a generar
								String fileErrorPath = errorPath + File.separator + payrollFileName + "_ERROR.txt";
								
								//##### Generando fichero de error
								ApiFileErrorWritter.writeError(fileErrorPath, errors);
								
								//##### Actualizando la ruta de error en el archivo
								String errorDbPath = dbPath + File.separator + "ERROR" + File.separator + payrollStamp.getPayrollFile().getNombre() + "_ERROR.txt";
								wPayrollParser.getPayrollStamp().getPayrollFile().setRutaError(errorDbPath);
							}
							
							//##### Cargando las configuraciones de archivo del usuario
							ArchivoNominaConfiguracionEntity payrollFileConfig = payrollStamp.getPayrollFileConfiguration();
							
							if (payrollStamped >= 1) {
								//##### Verificar si se genera compactado						
								if (payrollFileConfig.getCompactado()) {
									String zipDirectory = payrollStamp.getAbsolutePath() + File.separator + payrollFileName;
									String zipFile = zipDirectory + ".zip";
									String zipDbPath = dbPath + File.separator + payrollFileName + ".zip";
									
									try {
										
										System.out.println("-----------------------------------------------------------------------------------------------------------");
										System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] PAYROLL STAMP SERVICE > ZIPPING DIRECTORY: " + zipDirectory);
										
										UZipper.zipping(new File(zipDirectory), zipFile);								
										
										//##### Actualizando ruta de compactado
										wPayrollParser.getPayrollStamp().getPayrollFile().setRutaCompactado(zipDbPath);
										
										System.out.println("-----------------------------------------------------------------------------------------------------------");
										System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] PAYROLL STAMP SERVICE > ZIPPING DIRECTORY FINISHED");																				
										
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
								
								/*
								//Incrementamos el numero de archivos subidos
								ContribuyenteEntity contribuyente = wPayrollParser.getPayrollStamp().getTaxpayer();
								int archivosSubidos = contribuyente.getNominasSubidas();																
								archivosSubidos++;								
								contribuyente.setNominasSubidas(archivosSubidos);
								persistenceDAO.update(contribuyente);
								*/
								
								//##### Verificar si se genera consolidado
								ApiConsolidate consolidate = new ApiConsolidate();
								consolidate.setAbsolutePath(folderAbsolutePath);
								consolidate.setSourcePath(pdfFolder);
								consolidate.setConsolidateDbPath(dbPath);
								consolidate.setFileName(payrollStamp.getPayrollFile().getNombre());
								
								ApiConsolidate consolidateResult = null;
								if (payrollFileConfig.getConsolidado1x()) {
									consolidateResult = UConsolidate.consolidateSingle(consolidate);
								}else if (payrollFileConfig.getConsolidado2x()) {
									consolidateResult = UConsolidate.consolidateDual(consolidate);
								}
								if (!UValidator.isNullOrEmpty(consolidateResult)) {
									wPayrollParser.getPayrollStamp().getPayrollFile().setRutaConsolidado(consolidateResult.getConsolidateDbPath());
								}
							}
							
							//##### Cambiando estado del archivo a Generado
							EstadoArchivoEntity fileStatusGenerated = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);
							wPayrollParser.getPayrollStamp().getPayrollFile().setEstadoArchivo(fileStatusGenerated);
							
							//##### Actualizando archivo
							persistenceDAO.update(wPayrollParser.getPayrollStamp().getPayrollFile());
							
							//##### Actualizando estadisticas del archivo
							payrollStamp.getPayrollFileStatistics().setFechaFinTimbrado(new Date());
							payrollStamp.getPayrollFileStatistics().setHoraFinTimbrado(new Date());
							persistenceDAO.update(payrollStamp.getPayrollFileStatistics());														
							
							//##### Verificar si hay algun otro archivo en cola
							List<ArchivoNominaEntity> payrollFileList = iPayrollFileJpaRepository.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdAsc(payrollStamp.getTaxpayer(), FILE_STATUS_QUEUED);
							Boolean finded = false;
							//##### Verificando si el emisor tiene timbres disponibles
							if (stampControl.stampStatus(payrollStamp.getTaxpayer()) || canUploadPayroll) {
								for (ArchivoNominaEntity item : payrollFileList) {
									if (item.getEstadoArchivo().getCodigo().equalsIgnoreCase("C")) {
										//##### Si el archivo fue puesto en cola porque se estaba procesando otro
										if (UValidator.isNullOrEmpty(item.getTimbresDisponibles())) {
											item.setTimbresDisponibles(Boolean.TRUE);
										}
										
										//##### Verificando si el archivo no fue procesado por disponibilidad de timbres
										/*if (!item.getTimbresDisponibles()) {
											item.setTimbresDisponibles(Boolean.TRUE);
										}*/
										
										//##### Cargando el archivo
										payrollStamp.setPayrollFile(item);
										
										//##### Ruta de las nominas
										String payrollPath = appSettings.getPropertyValue("cfdi.payroll.path");
										
										//##### Ruta db del archivo
										String folderPath = payrollStamp.getTaxpayer().getRfcActivo() + File.separator + UDate.date().replaceAll("-", "") + File.separator + item.getNombre();
										payrollStamp.setDbPath(folderPath);
										
										//##### Ruta absoluta del archivo
										folderAbsolutePath = payrollPath + File.separator + folderPath;
										payrollStamp.setAbsolutePath(folderAbsolutePath);
										
										//##### Ruta fisica del archivo
										String payrollUploadPath = payrollPath + File.separator + item.getRuta();
										payrollStamp.setUploadPath(payrollUploadPath);
										
										//##### Actualizando informacion de timbres
										payrollStamp.setStampDetail(stampControl.stampDetails(payrollStamp.getTaxpayer()));
										
										finded = true;
										break;
									}
								}
								
								//##### Procesando el archivo
								if (finded) {
									payrollPrcessing(payrollStamp,canUploadPayroll);
								}
							}
						} else {
							//##### Verificando si hay errores
							if (!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()) {
								//##### Obteniendo la ruta absoluta del archivo
								String folderAbsolutePath = payrollStamp.getAbsolutePath();
								
								//##### Obteniendo la ruta en db
								String dbPath = payrollStamp.getDbPath();							
								
								//##### Generando ruta de almacenamiento del error
								String errorPath = folderAbsolutePath + File.separator + "ERROR";
								File folderError = new File(errorPath);
					    		if (!folderError.exists()) {
					    			folderError.mkdirs();
					    		}
								
								//##### Fichero de error a generar
								String fileErrorPath = errorPath + File.separator + payrollStamp.getPayrollFile().getNombre() + "_ERROR.txt";
								
								//##### Generando fichero de error
								ApiFileErrorWritter.writeError(fileErrorPath, errors);
								
								//##### Actualizando la ruta de error en el archivo
								String errorDbPath = dbPath + File.separator + "ERROR" + File.separator + payrollStamp.getPayrollFile().getNombre() + "_ERROR.txt";
								wPayrollParser.getPayrollStamp().getPayrollFile().setRutaError(errorDbPath);
							}
							
							//##### Cambiando estado del archivo a Cola nuevamente
							EstadoArchivoEntity fileStatusGenerated = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);
							wPayrollParser.getPayrollStamp().getPayrollFile().setEstadoArchivo(fileStatusGenerated);
							
							//##### Actualizando archivo
							persistenceDAO.update(wPayrollParser.getPayrollStamp().getPayrollFile());
							
							//##### Estadisticas generadas del archivo
							managePayrollFileStatistics(wPayrollParser);
							
							System.out.println("-----------------------------------------------------------------------------------------------------------");
							System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[ERROR] PAYROLL STAMP SERVICE > NO VOUCHERS WERE GENERATED");
						}
					}
				} else {
					//##### Indicando que el emisor no tiene suficientes timbres para timbrar todas las nominas del archivo
					wPayrollParser.getPayrollStamp().getPayrollFile().setTimbresDisponibles(Boolean.FALSE);
					
					//##### Cambiando estado del archivo a Cola nuevamente
					EstadoArchivoEntity fileStatusQueued = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_QUEUED);
					wPayrollParser.getPayrollStamp().getPayrollFile().setEstadoArchivo(fileStatusQueued);
					
					//##### Actualizando archivo
					persistenceDAO.update(wPayrollParser.getPayrollStamp().getPayrollFile());
					
					//##### Estadisticas generadas del archivo
					managePayrollFileStatistics(wPayrollParser);
					
					System.out.println("-----------------------------------------------------------------------------------------------------------");
					System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[ERROR] PAYROLL STAMP SERVICE > THE EMITTER HASN'T SUFFICIENT STAMPS");
				}
			} else {
				//##### Obteniendo la ruta absoluta del archivo
				String folderAbsolutePath = payrollStamp.getAbsolutePath();
				
				//##### Obteniendo la ruta en db
				String dbPath = payrollStamp.getDbPath();
				
				//##### Verificando si hay errores
				if (!UValidator.isNullOrEmpty(errors) && !errors.isEmpty()) {
					//##### Generando ruta de almacenamiento del error
					String errorPath = folderAbsolutePath + File.separator + "ERROR";
					File folderError = new File(errorPath);
		    		if (!folderError.exists()) {
		    			folderError.mkdirs();
		    		}
					
		    		
					//##### Fichero de error a generar
					String fileErrorPath = errorPath + File.separator + payrollStamp.getPayrollFile().getNombre() + "_ERROR.txt";
					
					//##### Generando fichero de error
					ApiFileErrorWritter.writeError(fileErrorPath, errors);
					
					//##### Actualizando la ruta de error en el archivo
					String errorDbPath = dbPath + File.separator + "ERROR" + File.separator + payrollStamp.getPayrollFile().getNombre() + "_ERROR.txt";
					wPayrollParser.getPayrollStamp().getPayrollFile().setRutaError(errorDbPath);
				}
				
				//##### Cambiando estado del archivo a Generado
				EstadoArchivoEntity fileStatusGenerated = iFileStatusJpaRepository.findByCodigoAndEliminadoFalse(FILE_STATUS_GENERATED);
				wPayrollParser.getPayrollStamp().getPayrollFile().setEstadoArchivo(fileStatusGenerated);
				
				//##### Actualizando archivo
				persistenceDAO.update(wPayrollParser.getPayrollStamp().getPayrollFile());
				
				//##### Actualizando estadisticas del archivo
				if (!UValidator.isNullOrEmpty(payrollStamp.getPayrollFileStatistics())) {
					payrollStamp.getPayrollFileStatistics().setFechaFinTimbrado(new Date());
					payrollStamp.getPayrollFileStatistics().setHoraFinTimbrado(new Date());
					persistenceDAO.update(payrollStamp.getPayrollFileStatistics());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
	}
	
	@Transactional
	private ApiPayrollStampResponse voucherStamp(ApiPayrollVoucher payrollVoucher, ApiPayrollStamp payrollStamp, String xmlFolder, String pdfFolder, String xmlDb, String pdfDb) {
		ApiPayrollStampResponse payrollStampResponse = new ApiPayrollStampResponse();
		try {
			
			//##### Obteniendo el comprobante
			Comprobante voucher = payrollVoucher.getVoucher();
			var voucherDate = UDateTime.format(LocalDateTime.now().minusHours(1));
			if (!appSettings.isDev()) {
				voucherDate = UDate.getDateByZipCode(payrollVoucher.getCfdi().getPostalCode().getCodigo());
			}
			
			//##### Actualizando la fecha de emision del comprobante
			//##### para que no exista mucha diferencia con la fehca de timbrado
			voucher.setFecha(voucherDate);
			
			//##### Obteniendo la informacion asociada al comprobante
			ApiCfdi cfdi = payrollVoucher.getCfdi();
			cfdi.getVoucher().setDateTimeExpedition(voucherDate);
			
			//##### Obteniendo informacion asociada a la nomina
			ApiPayroll payroll = payrollVoucher.getPayroll();
			payroll.setApiCfdi(cfdi);
			payroll.setPayrollFile(payrollStamp.getPayrollFile());
			
			//##### Cargando el comprobante en las propiedades de timbrado
			val stampProperties = payrollStamp.getStampProperties();
			stampProperties.setVoucher(voucher);
			
			//##### Obteniendo las estadisticas del archivo
			ArchivoNominaEstadisticaEntity statistics = payrollStamp.getPayrollFileStatistics();
			
			try {
				//##### Timbrando el comprobante
				
				val stampResponse = SWCfdiStamp.cfdiStamp(stampProperties);
				
				
				
				if (stampResponse.isStamped()) {
					try {
						
						//##### Cfdi stamp response
						cfdi.setStampResponse(stampResponse);													
						
						//##### Cfid voucher information
						cfdi.getVoucher().setTaxSheet(cfdi.getStampResponse().getUuid());
						cfdi.getVoucher().setCertificateEmitterNumber(cfdi.getStampResponse().getCertificateEmitterNumber());
						cfdi.getVoucher().setCertificateSatNumber(cfdi.getStampResponse().getCertificateSatNumber());
						cfdi.getVoucher().setEmitterSeal(cfdi.getStampResponse().getEmitterSeal());
						cfdi.getVoucher().setSatSeal(cfdi.getStampResponse().getSatSeal());
						cfdi.getVoucher().setDateTimeStamp(cfdi.getStampResponse().getDateStamp());
						cfdi.getVoucher().setOriginalString(cfdi.getStampResponse().getOriginalString());
						
						//##### Employee number
						String employeeNumber = payroll.getPayrollReceiver().getEmployeeNumber();
						
						//##### XML/PDF name
						String fileName = cfdi.getStampResponse().getUuid();
						
						//##### Xml physical storage
						String xmlByEmployeePath = xmlFolder + File.separator + employeeNumber;
						File fileFolder = new File(xmlByEmployeePath); 
						if (!fileFolder.exists()) {
							fileFolder.mkdirs();
						}
						xmlByEmployeePath = xmlByEmployeePath + File.separator + fileName + ".xml";
						
						//##### Xml database path storage
						String xmlDbPath = xmlDb + File.separator + employeeNumber + File.separator + fileName + ".xml";
						payroll.setXmlPath(xmlDbPath);
						
						//##### PDF physical storage
						String pdfByEmployeePath = pdfFolder + File.separator + employeeNumber;
						fileFolder = new File(pdfByEmployeePath); 
						if (!fileFolder.exists()) {
							fileFolder.mkdirs();
						}
						pdfByEmployeePath = pdfByEmployeePath + File.separator + fileName + ".pdf";
						
						//##### Pdf database path storage
						String pdfDbPath = pdfDb + File.separator + employeeNumber + File.separator + fileName + ".pdf";
						payroll.setPdfPath(pdfDbPath);
						
						//##### QR
						cfdi.setQr(cfdi.getStampResponse().getQrCode());
						
						//##### Storing payroll
						NominaEntity payrollEntity = payrollService.save(payroll); 
						if (!UValidator.isNullOrEmpty(payrollEntity)) {
							try {
								
								System.out.println("-----------------------------------------------------------------------------------------------------------");
								System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] PAYROLL STAMP SERVICE > STORING XML...");
								
								//##### Storing XML
								UFile.writeFile(cfdi.getStampResponse().getXml(), xmlByEmployeePath);
								
								//##### Generating and storing PDF
								Boolean pdfGenerated = false;
								pdfGenerated = reportManager.generatePDFPayroll(payrollVoucher, pdfByEmployeePath);
								if (pdfGenerated) {
									System.out.println("-----------------------------------------------------------------------------------------------------------");
									System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] STORING PDF...");
								}
								
								//##### Cargando las preferencias de la entidad
								ArchivoNominaConfiguracionEntity payrollFileConfiguration = payrollStamp.getPayrollFileConfiguration();
								if (!UValidator.isNullOrEmpty(payrollFileConfiguration)) {
									//##### Verificar si se envia correo de notificacion al trabajador
									if (payrollFileConfiguration.getEnvioXmlPdf() || payrollFileConfiguration.getEnvioXml() || payrollFileConfiguration.getEnvioPdf()) {
										WCfdiNotification notification = new WCfdiNotification();
										notification.setReceiverEmail(cfdi.getReceiver().getCorreoElectronico());
										notification.setReceiverName(cfdi.getReceiver().getNombreRazonSocial());
										notification.setReceiverRfc(cfdi.getReceiver().getRfc());
										notification.setEmitterName(cfdi.getEmitter().getNombreRazonSocial());
										notification.setEmitterRfc(cfdi.getEmitter().getRfc());
										notification.setXmlPath(xmlByEmployeePath);
										notification.setPdfPath(pdfByEmployeePath);
										notification.setUuid(fileName);
										notification.setPayroll(payrollEntity);
										
										PreferenciasEntity preferences = new PreferenciasEntity();
										preferences.setReceptorXmlPdf(payrollFileConfiguration.getEnvioXmlPdf());
										preferences.setReceptorXml(payrollFileConfiguration.getEnvioXml());
										preferences.setReceptorPdf(payrollFileConfiguration.getEnvioPdf());
										notification.setPreferences(preferences);
										
										mailService.sendPayroll(notification);
									}
								}
								
								//##### Actualizando estadisticas del archivo
								Integer payrollStamped = UValidator.isNullOrEmpty(statistics.getNominasTimbradas()) ? 0 : statistics.getNominasTimbradas();
								payrollStamped ++;
								statistics.setNominasTimbradas(payrollStamped);
								persistenceDAO.update(statistics);
								
								//##### Stamp control
								stampControl.stampControlAsync(payrollStamp.getTaxpayer(), payrollStamp.getUser(), payrollStamp.getUrlAcceso(), payrollStamp.getIp());
								
								//##### Bitacora :: Registro de accion en bitacora
								appLog.logOperation(OperationCode.GENERACION_CFDIV33_NOMV12_MASTER_ID71, payrollStamp.getUser(), payrollStamp.getUrlAcceso(), payrollStamp.getIp());
								
								//##### Timeline :: Registro de accion
								timelineLog.timelineLog(EventTypeCode.GENERACION_CFDIV33_SIN_COMPLEMENTO_MASTER_ID4, payrollStamp.getTaxpayer(), payrollStamp.getUser(), payrollStamp.getIp());
								
								payrollStampResponse.setVoucher(null);
								payrollStampResponse.setError(null);
								
							} catch (Exception e) {
								
								ApiPayrollError error = new ApiPayrollError();
								error.setUnexpectedError(Boolean.TRUE);
								error.setReceiverRfc(cfdi.getReceiver().getRfc());
								error.setReceiverName(cfdi.getReceiver().getNombreRazonSocial());
								error.setReceiverCurp(cfdi.getReceiver().getCurp());
								error.setDescription("Ha ocurrido un error inesperado guardando el xml y/o pdf de la nómina.");
								
								payrollStampResponse.setError(error);
								
							}
						} else {
							ApiPayrollError error = new ApiPayrollError();
							error.setUnexpectedError(Boolean.TRUE);
							error.setReceiverRfc(cfdi.getReceiver().getRfc());
							error.setReceiverName(cfdi.getReceiver().getNombreRazonSocial());
							error.setReceiverCurp(cfdi.getReceiver().getCurp());
							error.setDescription("Ha ocurrido un error inesperado registrando la información de la nómina.");
							
							payrollStampResponse.setError(error);
						}
						payrollStampResponse.setStatistics(statistics);
						
					} catch (Exception e) {
						
						ApiPayrollError error = new ApiPayrollError();
						error.setUnexpectedError(Boolean.TRUE);
						error.setReceiverRfc(cfdi.getReceiver().getRfc());
						error.setReceiverName(cfdi.getReceiver().getNombreRazonSocial());
						error.setReceiverCurp(cfdi.getReceiver().getCurp());
						error.setDescription("Ha ocurrido un error inesperado gestionando la información del xml y/o pdf de la nómina.");
						
						payrollStampResponse.setError(error);
						
					}
				} else {
					AppWebValidator validator = Validator.stampValidate(stampResponse);
					if (!UValidator.isNullOrEmpty(validator)) {
						ApiPayrollError error = new ApiPayrollError();
						error.setStampError(Boolean.TRUE);
						error.setReceiverRfc(cfdi.getReceiver().getRfc());
						error.setReceiverName(cfdi.getReceiver().getNombreRazonSocial());
						error.setReceiverCurp(cfdi.getReceiver().getCurp());
						error.setDescription("Error de validación de contenido de la nómina.");
						error.setStampErrorCode(validator.getErrorCode());
						error.setStampErrorMessage(validator.getErrorMessage());
						
						payrollStampResponse.setError(error);
						
						//##### Actualizando estadisticas del archivo
						Integer payrollStampedError = UValidator.isNullOrEmpty(statistics.getNominasError()) ? 0 : statistics.getNominasError();
						payrollStampedError ++;
						statistics.setNominasError(payrollStampedError);
						persistenceDAO.update(statistics);
						
						payrollStampResponse.setStatistics(statistics);
						
					} else {
						payrollStampResponse.setStatistics(statistics);
						payrollStampResponse.setVoucher(payrollVoucher);
					}
				}
			} catch (Exception e) {
				
				ApiPayrollError error = new ApiPayrollError();
				error.setUnexpectedError(Boolean.TRUE);
				error.setReceiverRfc(cfdi.getReceiver().getRfc());
				error.setReceiverName(cfdi.getReceiver().getNombreRazonSocial());
				error.setReceiverCurp(cfdi.getReceiver().getCurp());
				error.setDescription("Ha ocurrido un error inesperado timbrando la nómina.");
				
				payrollStampResponse.setError(error);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			payrollStampResponse.setVoucher(payrollVoucher);
		}
		
		return payrollStampResponse;
	}
	
	/*private ApiPayrollStampResponse voucherAttemptStamp(ApiPayrollVoucher payrollVoucher, ApiPayrollStamp payrollStamp, String xmlFolder, String pdfFolder, String xmlDb, String pdfDb) {
		
		System.out.println("-----------------------------------------------------------------------------------------------------------");
		System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] PAYROLL STAMP SERVICE > VOUCHER ATTEMPT STAMP STARTING...");
		
		ApiPayrollStampResponse payrollStampResponse = null; 
				
		try {
			
			payrollStampResponse = voucherStamp(payrollVoucher, payrollStamp, xmlFolder, pdfFolder, xmlDb, pdfDb); 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return payrollStampResponse;
	}*/
	
	public ArchivoNominaEstadisticaEntity managePayrollFileStatistics(ApiPayrollParser wPayrollParser) {
		try {
			
			ArchivoNominaEstadisticaEntity payrollFileStatistics = null;
			
			//##### Verificando si el archivo tiene estadisticas generadas
			payrollFileStatistics = iPayrollFileStatisticsJpaRepository.findByArchivoNomina(wPayrollParser.getPayrollStamp().getPayrollFile());
			if (UValidator.isNullOrEmpty(payrollFileStatistics)) {
				//##### Registrando estadisticas del archivo
				payrollFileStatistics = wPayrollParser.getStatistics().getPayrollFileStatistics();
				payrollFileStatistics.setArchivoNomina(wPayrollParser.getPayrollStamp().getPayrollFile());
				payrollFileStatistics.setFechaInicioTimbrado(new Date());
				payrollFileStatistics.setHoraInicioTimbrado(new Date());
				payrollFileStatistics = (ArchivoNominaEstadisticaEntity) persistenceDAO.persist(payrollFileStatistics);
			} else {
				//##### Actualizando estadisticas del archivo
				ArchivoNominaEstadisticaEntity statistics = wPayrollParser.getStatistics().getPayrollFileStatistics();
				payrollFileStatistics.setNominas(statistics.getNominas());
				payrollFileStatistics.setNominasTimbradas(statistics.getNominasTimbradas());
				payrollFileStatistics.setNominasError(statistics.getNominasError());
				payrollFileStatistics.setFechaInicioAnalisis(statistics.getFechaInicioAnalisis());
				payrollFileStatistics.setHoraInicioAnalisis(statistics.getHoraInicioAnalisis());
				payrollFileStatistics.setFechaFinAnalisis(statistics.getFechaFinAnalisis());
				payrollFileStatistics.setHoraFinAnalisis(statistics.getHoraFinAnalisis());
				payrollFileStatistics.setFechaInicioGeneracionComprobante(statistics.getFechaInicioGeneracionComprobante());
				payrollFileStatistics.setHoraInicioGeneracionComprobante(statistics.getHoraInicioGeneracionComprobante());
				payrollFileStatistics.setFechaFinGeneracionComprobante(statistics.getFechaFinGeneracionComprobante());
				payrollFileStatistics.setHoraFinGeneracionComprobante(statistics.getHoraFinGeneracionComprobante());
				payrollFileStatistics.setFechaInicioTimbrado(new Date());
				payrollFileStatistics.setHoraInicioTimbrado(new Date());
				payrollFileStatistics = (ArchivoNominaEstadisticaEntity) persistenceDAO.update(payrollFileStatistics);
			}
			return payrollFileStatistics;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}