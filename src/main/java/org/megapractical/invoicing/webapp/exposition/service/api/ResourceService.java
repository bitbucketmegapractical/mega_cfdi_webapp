package org.megapractical.invoicing.webapp.exposition.service.api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.ResourceData;
import org.megapractical.invoicing.webapp.exposition.service.api.payload.UploadResponse;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class ResourceService {
	
	private final AppSettings appSettings;
	private final ApiTokenService apiTokenService;
	private final RestTemplate restTemplate;
	
	private String resourceServiceUrl;
	
	@PostConstruct
	public void init() {
		this.resourceServiceUrl = appSettings.getPropertyValue("mega.cfdi.resource.service.url");
	}
	
	public UploadResponse uploadFile(String emitterRfc, String filePath) throws IOException {
		try {
			// Read the contents of the file into a ByteArrayResource
	        val path = Paths.get(filePath);
	        val resource = new ByteArrayResource(Files.readAllBytes(path));

	        // Create the headers for the request
	        HttpHeaders headers = populateHeaders(MediaType.MULTIPART_FORM_DATA);

	        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
	        body.add("file", resource);

	        // Create the HTTP entity with the request body and headers
	        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);

	        // Make the request to the API
	        val baseUrl = resourceUploadUrl(emitterRfc);
	        val response = restTemplate.postForEntity(baseUrl, entity, UploadResponse.class, emitterRfc);
	        UPrint.logWithLine(UProperties.env(), "[INFO] RESOURCE SERVICE UPLOAD FILE > RESPONSE " + response.getBody());
	        
	        // Return the response body
	        return response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
	
	public List<UploadResponse> uploadFiles(List<ResourceData> resourceDataSource) throws IOException {
		val uploadResponseList = new ArrayList<UploadResponse>();
		for (val resourceData : resourceDataSource) {
			try {
				// Create the headers for the request
				HttpHeaders headers = populateHeaders(MediaType.MULTIPART_FORM_DATA);
				
				// Create the body of the request
		        MultiValueMap<String, Object> body = populateMultipleBody(resourceData);
				
				// Create the HTTP entity with the request body and headers
				HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);
				
				// Make the request to the API using ParameterizedTypeReference
				val emitterRfc = getEmitterRfc(resourceData);
				val baseUrl = resourceUploadMultipleUrl(emitterRfc);
				val response = restTemplate.exchange(baseUrl, HttpMethod.POST, entity, new ParameterizedTypeReference<List<UploadResponse>>(){}, emitterRfc);
				UPrint.logWithLine(UProperties.env(), "[INFO] RESOURCE SERVICE UPLOAD FILES > RESPONSE " + response.getBody());
				
				// Return the list of UploadResponse objects
				response.getBody().stream().filter(Objects::nonNull).map(uploadResponseList::add).collect(Collectors.toList());
			} catch (RestClientException e) {
				e.printStackTrace();
			}
		}
		return uploadResponseList;
    }
	
	private HttpHeaders populateHeaders(MediaType mediaType) {
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);
        headers.setBearerAuth(apiTokenService.getToken());
        return headers;
	}
	
	private MultiValueMap<String, Object> populateMultipleBody(ResourceData resourceData) {
		try {
			val xmlPath = Paths.get(resourceData.getXmlPath());
	        val xmlResource = new ByteArrayResource(Files.readAllBytes(xmlPath));
			
	        val pdfPath = Paths.get(resourceData.getPdfPath());
	        val pdfResource = new ByteArrayResource(Files.readAllBytes(pdfPath));
	        
	        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
	        body.add("files", xmlResource);
	        body.add("files", pdfResource);
	        
	        return body;
		} catch (IOException e) {
			return new LinkedMultiValueMap<>();
		}
	}
	
	private String resourceUploadUrl(String emitterRfc) {
		val url = String.format("%s/upload/%s", resourceServiceUrl, emitterRfc);
		UPrint.logWithLine(UProperties.env(), "[INFO] RESOURCE SERVICE UPLOAD > REQUEST FOR " + url);
		return url; 
	}
	
	private String resourceUploadMultipleUrl(String emitterRfc) {
		val url = String.format("%s/upload/multiple/%s", resourceServiceUrl, emitterRfc);
		UPrint.logWithLine(UProperties.env(), "[INFO] RESOURCE SERVICE UPLOAD > REQUEST FOR " + url);
		return url;
	}
	
	@SuppressWarnings("unused")
	private String resourceDownloadUrl(String emitterRfc, String uuid, CfdiType cfdiType) {
		val url = String.format("%s/download/%s/%s/%s", resourceServiceUrl, emitterRfc, uuid, cfdiType);
		UPrint.logWithLine(UProperties.env(), "[INFO] RESOURCE SERVICE DOWNLOAD > REQUEST FOR " + url);
		return url;
	}
	
	private String getEmitterRfc(ResourceData resourceData) {
		if (resourceData.getResourceCfdiData() != null) {
			return resourceData.getResourceCfdiData().getApiCfdi().getEmitter().getRfc();
		} else if (resourceData.getResourcePayrollData() != null) {
			return resourceData.getResourcePayrollData().getApiPayroll().getApiCfdi().getEmitter().getRfc();
		}
		return null;
	}
	
	public enum CfdiType {
		XML, PDF
	}
	
}