package org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload;

import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaxValidatorResponse {
	private JNotification notification;
	private JError error;
}