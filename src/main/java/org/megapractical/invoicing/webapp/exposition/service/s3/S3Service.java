package org.megapractical.invoicing.webapp.exposition.service.s3;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exception.S3UploadFileException;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.Delete;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectsRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ObjectIdentifier;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;
import software.amazon.awssdk.services.s3.model.S3Exception;
import software.amazon.awssdk.services.s3.model.S3Object;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@Slf4j(topic = "S3Service")
@RequiredArgsConstructor
public class S3Service {

	private final AppSettings appSettings;
	private final S3Client s3Client;

	private String bucketName;
	
	@PostConstruct
	public void init() {
		this.bucketName = appSettings.getPropertyValue("aws.s3.bucket.name");
	}

	public List<String> listObjects() {
		try {
			val listObjectsRequest = ListObjectsV2Request.builder().bucket(bucketName).build();
			val listObjectsResponse = s3Client.listObjectsV2(listObjectsRequest);
			return listObjectsResponse.contents().stream().map(S3Object::key).collect(Collectors.toList());
		} catch (S3Exception e) {
			log.error("listObjectsV2 error: {}", e.getMessage());
		}
		return Collections.emptyList();
	}

	public PutObjectResponse uploadFile(String key, File file) {
		try {
	    	val putObjectRequest = PutObjectRequest.builder().bucket(bucketName).key(key).build();
	        return s3Client.putObject(putObjectRequest, RequestBody.fromFile(file));
	    } catch (S3Exception e) {
	    	log.error("uploadFile error: {}", e.getMessage());
	    	throw new S3UploadFileException(e.getMessage());
	    }
	}
	
	public PutObjectResponse uploadFile(String key, InputStream inputStream, long contentLength) {
		val putObjectRequest = PutObjectRequest.builder().bucket(bucketName).key(key).build();
		try {
			return s3Client.putObject(putObjectRequest, RequestBody.fromInputStream(inputStream, contentLength));
		} catch (S3Exception e) {
			log.error("uploadFile error: {}", e.getMessage());
			throw new S3UploadFileException(e.getMessage());
		}
	}
	
	public byte[] getObject(String key) {
		log.info("getObject key: {}", key);
		try {
			val getObjectRequest = GetObjectRequest.builder().bucket(bucketName).key(key).build();
			val getObjectResponse = s3Client.getObjectAsBytes(getObjectRequest);
			return getObjectResponse.asByteArray();
		} catch (S3Exception e) {
			log.error("getObject error: {}", e.getMessage());
		}
		return new byte[0];
	}

	public void deleteObject(String key) {
		try {
			val deleteObjectRequest = DeleteObjectRequest.builder().bucket(bucketName).key(key).build();
			s3Client.deleteObject(deleteObjectRequest);
		} catch (S3Exception e) {
			log.error("deleteObject error: {}", e.getMessage());
		}
	}

	public void deleteObjects(List<String> keys) {
		try {
			val objectIdentifiers = keys.stream().map(key -> ObjectIdentifier.builder().key(key).build()).collect(Collectors.toList());
			val deleteObjectsRequest = DeleteObjectsRequest
										.builder()
										.bucket(bucketName)
										.delete(Delete.builder().objects(objectIdentifiers).build())
										.build();
			s3Client.deleteObjects(deleteObjectsRequest);
		} catch (S3Exception e) {
			log.error("deleteObjects error: {}", e.getMessage());
		}
	}

	public void ensureFolderExists(String folderName) {
		try {
			val listObjectsRequest = ListObjectsV2Request.builder().bucket(bucketName).prefix(folderName + "/").build();
		    val listObjectsResponse = s3Client.listObjectsV2(listObjectsRequest);
		    if (listObjectsResponse.hasContents()) {
		        return; // Folder already exists in the bucket
		    }
		    
			val putObjectRequest = PutObjectRequest.builder().bucket(bucketName).key(folderName + "/").build();
		    s3Client.putObject(putObjectRequest, RequestBody.empty());
		} catch (S3Exception e) {
			log.error("ensureFolderExists error: {}", e.getMessage());
		}
	}
	
	public void deleteFolder(String folderName) {
		try {
			val listObjectsRequest = ListObjectsV2Request.builder().bucket(bucketName).prefix(folderName).build();
			val listObjectsResponse = s3Client.listObjectsV2(listObjectsRequest);
			val objectIdentifiers = listObjectsResponse.contents()
													   .stream()
													   .map(S3Object::key)
													   .map(key -> ObjectIdentifier.builder().key(key).build())
													   .collect(Collectors.toList());
			val deleteObjectsRequest = DeleteObjectsRequest
										.builder()
										.bucket(bucketName)
										.delete(Delete.builder().objects(objectIdentifiers).build())
										.build();
			s3Client.deleteObjects(deleteObjectsRequest);
		} catch (S3Exception e) {
			log.error("deleteFolder error: {}", e.getMessage());
		}
	}

}