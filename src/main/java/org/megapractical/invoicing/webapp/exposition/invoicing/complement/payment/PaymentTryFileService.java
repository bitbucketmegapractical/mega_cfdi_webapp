package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.json.JCfdiFile;
import org.springframework.web.multipart.MultipartFile;

public interface PaymentTryFileService {

	JCfdiFile paymentTry(ContribuyenteEntity taxpayer, String pageStr, String searchBy);
	
	JCfdiFile loadGeneratedFiles(ContribuyenteEntity taxpayer, String pageStr, String searchBy);

	JCfdiFile uploadFile(ContribuyenteEntity taxpayer, ContribuyenteCertificadoEntity certificate,
			MultipartFile multipartUploadedFile, boolean sendMail);

	byte[] downloadFile(ContribuyenteEntity taxpayer, String fileName, String fieType, HttpServletResponse response);
	
	boolean exists(Long id);
	
	void deleteQueuedFile(Long id);

}