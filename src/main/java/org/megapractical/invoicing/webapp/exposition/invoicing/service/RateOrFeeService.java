package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.webapp.json.JRateOrFee;

public interface RateOrFeeService {
	
	JRateOrFee load();
	
}