package org.megapractical.invoicing.webapp.exposition.invoicing.validator.cfdiuse;

import java.util.List;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.dal.bean.jpa.UsoCfdiEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiUseJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverCfdiUseResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class CfdiUseValidatorImpl implements CfdiUseValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ICfdiUseJpaRepository iCfdiUseJpaRepository;
	
	@Override
	public ReceiverCfdiUseResponse validate(String cfdiUse, String rfc) {
		UsoCfdiEntity cfdiUseValidate = null;
		var cfdiUseError = false;
		
		if (UValidator.isNullOrEmpty(cfdiUse)) {
			cfdiUseError = true;
		} else {
			val cfdiUseEntityByCode = findByCode(cfdiUse);
			if (cfdiUseEntityByCode == null) {
				cfdiUseError = true;
			} else {
				if (!UTools.isMoralPerson(rfc)) {
					cfdiUseValidate = findByPerson(cfdiUse);
				} else if (UTools.isMoralPerson(rfc)) {
					cfdiUseValidate = findByMoral(cfdiUse);
				}
				
				if (cfdiUseValidate == null) {
					cfdiUseError = true;
				}
			}
		}
		return new ReceiverCfdiUseResponse(cfdiUseValidate, cfdiUseError);
	}

	@Override
	public ReceiverCfdiUseResponse validate(String cfdiUse, String field, String rfc, List<JError> errors) {
		JError error = null;
		UsoCfdiEntity cfdiUseValidate = null;
		
		if (UValidator.isNullOrEmpty(cfdiUse)) {
			error = JError.required(field, userTools.getLocale());
		} else {
			// Validar que sea un valor de catalogo
			val cfdiUseCatalog = UCatalog.getCatalog(cfdiUse);
			cfdiUse = cfdiUseCatalog[0];
			val cfdiUseValue = cfdiUseCatalog[1];
			
			val cfdiUseEntityByCode = findByCode(cfdiUse);
			val cfdiUseEntityByValue = findByValue(cfdiUseValue);
			if (cfdiUseEntityByCode == null || cfdiUseEntityByValue == null) {
				val values = new String[]{"CFDI33140", "UsoCFDI", "c_UsoCFDI"};
				val errorMessage = UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(), values);
				error = JError.error(field, errorMessage);
			} else {
				if (!UTools.isMoralPerson(rfc)) {
					cfdiUseValidate = findByPerson(cfdiUse);
				} else if (UTools.isMoralPerson(rfc)) {
					cfdiUseValidate = findByMoral(cfdiUse);
				}
				
				if (cfdiUseValidate == null) {
					error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33141").value(), userTools.getLocale());
				}
			}
		}
		
		var cfdiUseError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			cfdiUseError = true;
		}
		
		return new ReceiverCfdiUseResponse(cfdiUseValidate, cfdiUseError);
	}

	private UsoCfdiEntity findByCode(String cfdiUse) {
		return iCfdiUseJpaRepository.findByCodigoAndEliminadoFalse(cfdiUse);
	}

	private UsoCfdiEntity findByValue(final java.lang.String cfdiUseValue) {
		return iCfdiUseJpaRepository.findByValorAndEliminadoFalse(cfdiUseValue);
	}

	private UsoCfdiEntity findByPerson(String cfdiUse) {
		return iCfdiUseJpaRepository.findByCodigoAndAplicaPersonaFisicaTrueAndEliminadoFalse(cfdiUse);
	}
	
	private UsoCfdiEntity findByMoral(String cfdiUse) {
		return iCfdiUseJpaRepository.findByCodigoAndAplicaPersonaMoralTrueAndEliminadoFalse(cfdiUse);
	}

}