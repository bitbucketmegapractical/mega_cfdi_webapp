package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.StringTypeValidatorResponse;

public interface StringTypeValidator {
	
	StringTypeValidatorResponse validate(String stringType);
	
	StringTypeValidatorResponse validate(String stringType, String field);
	
}