package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoCancelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CancellationNotification {
	private ContribuyenteEntity taxpayer;
	private ArchivoCancelacionEntity cancellationFile;
	private String fileName;
	private List<String> recipients;
	private String subject;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class CancellationNotificationData {
		private String emitterRfc;
		private String emitterName;
		private String fileName;
		private String recipient;
		private String subject;
	}
}