package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.dal.bean.jpa.TipoCadenaPagoEntity;
import org.megapractical.invoicing.dal.data.repository.IStringTypeJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.StringTypeValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class StringTypeValidatorImpl implements StringTypeValidator {

	@Autowired
	private UserTools userTools;

	@Autowired
	private IStringTypeJpaRepository iStringTypeJpaRepository;

	@Override
	public StringTypeValidatorResponse validate(String stringType) {
		String stringTypeCode = null;
		var hasError = false;
		
		val paymentStringType = iStringTypeJpaRepository.findByCodigoAndEliminadoFalse(stringType);
		if (paymentStringType != null) {
			stringTypeCode = paymentStringType.getCodigo();
		} else {
			hasError = true;
		}
		return new StringTypeValidatorResponse(stringType, stringTypeCode, hasError);
	}
	
	@Override
	public StringTypeValidatorResponse validate(String stringType, String field) {
		JError error = null;
		String stringTypeCode = null;
		
		val paymentStringType = getStringType(stringType);
		if (paymentStringType == null) {
			val values = new String[] { "Tipo cadena pago", "c_TipoCadena" };
			error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
		} else {
			stringTypeCode = paymentStringType.getCodigo();
		}

		return new StringTypeValidatorResponse(stringType, stringTypeCode, error);
	}
	
	private TipoCadenaPagoEntity getStringType(String stringType) {
		// Validar que sea un valor de catalogo
		val stringTypeCatalog = UCatalog.getCatalog(stringType);
		val stringTypeCode = stringTypeCatalog[0];
		val stringTypeValue = stringTypeCatalog[1];

		val paymentStringTypeByCode = iStringTypeJpaRepository.findByCodigoAndEliminadoFalse(stringTypeCode);
		val paymentStringTypeByValue = iStringTypeJpaRepository.findByValorAndEliminadoFalse(stringTypeValue);
		if (paymentStringTypeByCode == null || paymentStringTypeByValue == null) {
			return null;
		}
		return paymentStringTypeByCode;
	}

}