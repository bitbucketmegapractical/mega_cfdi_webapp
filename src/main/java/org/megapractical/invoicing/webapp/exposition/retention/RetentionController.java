package org.megapractical.invoicing.webapp.exposition.retention;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.smarterweb.SWRetentionStamp;
import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.api.wrapper.ApiRetention.ApiRetImpRetenido;
import org.megapractical.invoicing.dal.bean.jpa.CfdiTipoRelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionPaisEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionTipoContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoImpuestoEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiRelationshipTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionComplementJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionCountryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionDividendTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionPaymetTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionTaxpayerJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionYearRangeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerTaxRegimeJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.retention.helper.RetentionComplementHelper;
import org.megapractical.invoicing.webapp.exposition.retention.helper.RetentionHelper;
import org.megapractical.invoicing.webapp.exposition.retention.source.RetentionDataSource;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.exposition.service.RetentionBuilderService;
import org.megapractical.invoicing.webapp.exposition.service.StampControlService;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JOption;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JRetention;
import org.megapractical.invoicing.webapp.json.JRetention.RetTotal.RetTotalWithheldTax;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WRetention;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.annotation.SessionScope;

import com.google.gson.Gson;

import lombok.val;
import lombok.var;

@Controller
@SessionScope
@RequestMapping("/retention")
public class RetentionController {

	@Autowired
	private AppLog appLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private PropertiesTools propertiesTools; 
	
	@Autowired
	private SessionController sessionController;
	
	@Autowired
	private StampControlService stampControl;
	
	@Autowired
	private DataStoreService dataStoreService;
	
	@Autowired
	private RetentionBuilderService retentionBuilderService;
	
	@Autowired
	private ReportManager reportManager;
	
	@Autowired
	private RetentionHelper retentionHelper;
	
	@Autowired
	private RetentionComplementHelper retentionComplementHelper;
	
	@Autowired
	private RetentionDataSource retentionDataSource;
	
	@Resource
	private ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	
	@Resource
	private IRetentionDividendTypeJpaRepository iRetentionDividendTypeJpaRepository;
	
	@Resource
	private IRetentionCountryJpaRepository iRetentionCountryJpaRepository;
	
	@Resource
	private ITaxTypeJpaRepository iTaxTypeJpaRepository;
	
	@Resource
	private IRetentionTaxpayerJpaRepository iRetentionTaxpayerJpaRepository;
	
	@Resource
	private IRetentionComplementJpaRepository iRetentionComplementJpaRepository;
	
	@Resource
	private IRetentionYearRangeJpaRepository iRetentionYearRangeJpaRepository;
	
	@Resource
	private ITaxpayerTaxRegimeJpaRepository iTaxpayerTaxRegimeJpaRepository;
	
	@Resource
	private IRetentionPaymetTypeJpaRepository iRetentionPaymetTypeJpaRepository;
	
	@Resource
	private ICfdiRelationshipTypeJpaRepository iCfdiRelationshipType;
	
	//##### Error inesperado
	private String unexpectedError;
	
	//##### Contribuyente
	private ContribuyenteEntity taxpayer;
		
	//##### Certificado del emisor
	private ContribuyenteCertificadoEntity certificate;
	private Date certificateExpired;
	private String certificateNumber;
	
	private List<ApiRetImpRetenido> withheldTaxs;
	private List<RetTotalWithheldTax> totalWithheldTaxs;
	private Map<String, String> relationshipMap = new HashMap<>();
	
	//##### Mensaje campo requerido
	@ModelAttribute("requiredMessage")
	public String requiredMessage() {
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	//##### Mensaje procesando...
	@ModelAttribute("processingRequest")
	public String processingMessage() {
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	//##### Mensaje cargando...
	@ModelAttribute("loadingRequest")
	public String loadingMessage() {
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	//##### Mensaje valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() {
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	//##### Mensaje formato incorrecto
	@ModelAttribute("invalidDataType")
	public String invalidDataTypeMessage() {
		return UProperties.getMessage("ERR1003", userTools.getLocale());
	}
	
	private List<RetencionPaisEntity> countrySource() {
		return iRetentionCountryJpaRepository.findByEliminadoFalse();
	}
	
	private List<TipoImpuestoEntity> taxTypeSource() {
		return iTaxTypeJpaRepository.findByEliminadoFalse();
	}
	
	private List<RetencionTipoContribuyenteEntity> retentionTaxpayerSource() {
		return iRetentionTaxpayerJpaRepository.findByEliminadoFalse();
	}
	
	public Map<String, String> cfdiRelationshipTypeSource() {
		val source = iCfdiRelationshipType.findByEliminadoFalse();
		relationshipMap = source.stream()
				.collect(Collectors.toMap(CfdiTipoRelacionEntity::getCodigo, CfdiTipoRelacionEntity::getData));
		return relationshipMap;
	}
	
	//##### Resetear impuestos
	private void taxReset() {
		this.withheldTaxs = new ArrayList<>();
		this.totalWithheldTaxs = new ArrayList<>();
	}
	
	@GetMapping(params = {"stamp"})
	public String init(Model model) {
		try {
			
			//##### Cargando el contribuyente por el rfc activo
			this.taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
			if (stampControl.stampStatus(this.taxpayer) || userTools.haveDaysLeft()) {
				initial(model);
				
				HtmlHelper.functionalitySelected("Retention", "retention-stamp");
				appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			} else {
				return "/home/Dashboard";
			}
			return "/retention/RetentionStamp";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	private void initial(Model model) {
		try {
			
			this.unexpectedError = UProperties.genericUnexpectedError(userTools.getLocale());
			taxReset();
			initialContent();
			
			//##### Cargando valores en el model
			model.addAttribute("emitterData", this.certificate != null);
			if (this.certificate != null) {
				model.addAttribute("residenteSource", Arrays.asList("Nacional", "Extranjero"));
				model.addAttribute("taxTypeSource", taxTypeSource());
				model.addAttribute("taxPaymentTypeSource", retentionDataSource.paymetTypeSource());
				model.addAttribute("dividendTypeSource", retentionDataSource.dividendTypeSource());
				model.addAttribute("dividendSocietySource", Arrays.asList("Sociedad Nacional", "Sociedad Extranjera"));
				model.addAttribute("countrySource", countrySource());
				model.addAttribute("retentionTaxpayerSource", retentionTaxpayerSource());
				model.addAttribute("cfdiRelationshipTypeSource", cfdiRelationshipTypeSource());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initialContent() {
		//##### Cargando certificado del contribuyente
		this.certificate = new ContribuyenteCertificadoEntity();
		this.certificate = iTaxpayerCertificateJpaRepository.findByContribuyenteAndHabilitadoTrue(this.taxpayer);
		if (!UValidator.isNullOrEmpty(this.certificate)) {
			this.certificateNumber =  this.certificate.getNumero();
		}
	}
	
	@PostMapping(params={"initretention"})
	@ResponseBody
	public String initretention(final HttpServletRequest request) {
		sessionController.sessionUpdate();
		taxReset();
		
		val retentionJson = new JRetention(JResponse.success());
		return GsonClient.response(retentionJson);
	}
	
	@PostMapping(params={"addtax"})
	@ResponseBody
	public String addTax(final HttpServletRequest request) {
		try {
			
			JRetention retentionJson = new JRetention();
			
			JError error = new JError();
			List<JError> errors = new ArrayList<>();
			Boolean errorMarked = false;
			
			JResponse response = new JResponse();
			
			Gson gson = new Gson();
			
			ApiRetImpRetenido apiTax = new ApiRetImpRetenido();
			RetTotalWithheldTax withheldTax = new RetTotalWithheldTax();
			Integer index = this.withheldTaxs.size() + 1;
			
			String requestTax = request.getParameter("objTax");
			withheldTax = gson.fromJson(requestTax, RetTotalWithheldTax.class);
			withheldTax.setIndex(String.valueOf(index));
			apiTax.setIndex(String.valueOf(index));
			
			//##### Base
			String base = UBase64.base64Decode(withheldTax.getBase());
			if (!UValidator.isNullOrEmpty(base)) {
				BigDecimal baseBigDecimal = UValue.bigDecimalStrict(base);
				if (!UValidator.isNullOrEmpty(baseBigDecimal) && UValidator.isDouble(baseBigDecimal)) {
					apiTax.setBase(UValue.bigDecimalString(UValue.bigDecimalString(base)));
					withheldTax.setBase(base);
				} else {
					error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
					error.setField("tax-base");
					errors.add(error);
				}				
			}
			
			//##### Impuesto
			error = new JError();
			errorMarked = false;
			
			String taxType = UBase64.base64Decode(withheldTax.getTax());
			if (!UValidator.isNullOrEmpty(taxType)) {
				val taxTypeEntity = iTaxTypeJpaRepository.findByCodigoAndEliminadoFalse(taxType);
				if (!UValidator.isNullOrEmpty(taxTypeEntity)) {
					apiTax.setTax(taxType);
					apiTax.setTaxValue(taxTypeEntity.getCodigo() + " " + taxTypeEntity.getValor());
					withheldTax.setTax(taxTypeEntity.getCodigo() + " " + taxTypeEntity.getValor());
				} else {
					error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
					error.setField("tax-type");
					errors.add(error);
				}
			}
			
			//##### Monto
			error = new JError();
			errorMarked = false;
			
			String amount = UBase64.base64Decode(withheldTax.getAmount());
			if (!UValidator.isNullOrEmpty(amount)) {
				BigDecimal baseBigDecimal = UValue.bigDecimalStrict(amount);
				if (!UValidator.isNullOrEmpty(baseBigDecimal) && UValidator.isDouble(baseBigDecimal)) {
					apiTax.setAmount(UValue.bigDecimalString(UValue.bigDecimalString(amount)));
					withheldTax.setAmount(amount);
				} else {
					error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
					errorMarked = true;				
				}
			} else {
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errorMarked = true;
			}
			if (errorMarked) {
				error.setField("tax-amount");
				errors.add(error);
			}
			
			//##### Tipo pago
			error = new JError();
			errorMarked = false;
			
			val paymentType = UBase64.base64Decode(withheldTax.getPaymentType());
			if (!UValidator.isNullOrEmpty(paymentType)) {
				val isValid = retentionDataSource.getPaymetTypeSource().stream()
																	   .map(JOption::getCode)
																	   .anyMatch(paymentType::equalsIgnoreCase);
				if (isValid) {
					apiTax.setPaymentType(paymentType);
					withheldTax.setPaymentType(paymentType);
				} else {
					error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
					errorMarked = true;
				}
			} else {
				error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
				errorMarked = true;
			}
			if (errorMarked) {
				error.setField("tax-payment-type");
				errors.add(error);
			}
			
			if (errors.isEmpty()) {
				this.withheldTaxs.add(apiTax);
				this.totalWithheldTaxs.add(withheldTax);

				retentionJson.setWithheldTaxs(totalWithheldTaxs);
				response.setSuccess(Boolean.TRUE);
			} else {
				response.setErrors(errors);
				response.setError(Boolean.TRUE);
			}			
			
			sessionController.sessionUpdate();
			
			retentionJson.setResponse(response);
			String jsonInString = gson.toJson(retentionJson);
			return jsonInString;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		return null;
	}
	
	@PostMapping(params={"removetax"})
	@ResponseBody
	public String removeTax(final HttpServletRequest request) {
		sessionController.sessionUpdate();
		try {
			val index = request.getParameter("index");
			
			this.withheldTaxs = this.withheldTaxs
									.stream()
							 		.filter(item -> !item.getIndex().equals(index))
							 		.collect(Collectors.toList());
			
			this.totalWithheldTaxs = this.totalWithheldTaxs
										 .stream()
					 					 .filter(item -> !item.getIndex().equals(index))
					 					 .collect(Collectors.toList());
			
			val retentionJson = new JRetention(JResponse.success());
			retentionJson.setWithheldTaxs(totalWithheldTaxs);
			return GsonClient.response(retentionJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional
	@PostMapping(params = {"stamp"})
	@ResponseBody
	public String retentionStamp(final HttpServletRequest request) {
		var retentionJson = new JRetention();
		var response = new JResponse();
		String errorMsg = null;
		val errors = new ArrayList<JError>();
		var sectionError = false;
		
		try {
			if (stampControl.stampStatus(this.taxpayer) || userTools.haveDaysLeft()) {
				val objRetentionData = request.getParameter("objRetentionData");
				val retentionData = GsonClient.deserialize(objRetentionData, RetentionData.class);
				if (!UValidator.isNullOrEmpty(retentionData)) {
					//##### Información del certificado del emisor de la retención
					val certificateByte = this.certificate.getCertificado();
					val privateKeyByte = this.certificate.getLlavePrivada();
					val keyPasswd = UBase64.base64Decode(this.certificate.getClavePrivada());
					this.certificateExpired = this.certificate.getFechaExpiracion();
					if (!UValidator.isNullOrEmpty(this.certificateExpired)) {
						if (!UDateTime.isBeforeLocalDate(this.certificateExpired, new Date())) {
							val apiRetention = new ApiRetention();
							
							// Información del emisor de la retención
							retentionHelper.populateEmitterData(this.taxpayer, this.certificateNumber, apiRetention);
							
							// Información de la retención
							val retData = retentionHelper.populateRetention(retentionData, apiRetention, errors);
							val retentionKey = retData.getRetentionKey();
							if (retData.isSectionError()) {
								sectionError = true;
							}
							
							// Información del receptor
							val receiverSectionError = retentionHelper.populateReceiverData(retentionData, apiRetention, errors);
							if (receiverSectionError) {
								sectionError = true;
							}
							
							// Información de Totales
							val totalsSectionError = retentionHelper.populateRetentionTotals(retentionData, withheldTaxs, apiRetention, errors);
							if (totalsSectionError) {
								sectionError = true;
							}
							
							// Documentos relacionados
							retentionHelper.populateRelatedCfdi(retentionData, apiRetention, relationshipMap);
							
							// Complementos
							val retCompResponse = retentionComplementHelper.populateComplement(retentionData,
									retentionKey, apiRetention, errors);
							if (retCompResponse.isSectionError()) {
								sectionError = true;
							}
							
							if (!retCompResponse.isRetentionWithComplement()) {
								val validateRetentionKey = iRetentionComplementJpaRepository.findByCodigoAndEliminadoFalse(retentionKey);
								if (UValidator.isNullOrEmpty(validateRetentionKey)) {
									val error = JError.error("retention-key", "La clave de retención especificada no es válida");
									errors.add(error);
								}
							}
							
							if (errors.isEmpty() && !sectionError) {
								val apiRetentionVoucher = retentionBuilderService.retentionBuild(apiRetention);
								if (!UValidator.isNullOrEmpty(apiRetentionVoucher)) {
									// Informacion para el timbrado
									val stampProperties = new StampProperties();										
									stampProperties.setRetention(apiRetentionVoucher.getRetencion());
									stampProperties.setCertificate(certificateByte);
									stampProperties.setPrivateKey(privateKeyByte);
									stampProperties.setPasswd(keyPasswd);										
									stampProperties.setEnvironment(propertiesTools.getEnvironment());
									
									// Timbrando la retención
									val stampResponse = SWRetentionStamp.retentionStamp(stampProperties);
									if (stampResponse.isStamped()) {
										apiRetention.setStampResponse(stampResponse);
										// Repository path
										val repositoryPath = appSettings.getPropertyValue("cfdi.retention.path");
										// Folder type
										val target = "emitted";
										// XML/PDF name
										val fileName = stampResponse.getUuid();
										val rfcByEmitter = apiRetention.getEmitterRfc();
										
										// Database path storage
										String receiverPath = null;
										if (apiRetention.getReceiverNationality().equalsIgnoreCase("nacional")) {
											receiverPath = apiRetention.getReceiverNatRfc();
										} else {
											if (!UValidator.isNullOrEmpty(apiRetention.getReceiverExtNumber())) {
												receiverPath = apiRetention.getReceiverExtNumber().toUpperCase();
											} else {
												receiverPath = apiRetention.getReceiverExtBusinessName().toUpperCase();
											}
										}
										val dbPath = rfcByEmitter + File.separator + target + File.separator + receiverPath;
										
										// Xml database path storage
										val xmlDbPath = dbPath + File.separator + fileName + ".xml";
										apiRetention.setXmlPath(xmlDbPath);
										
										// Pdf database path storage
										val pdfDbPath = dbPath + File.separator + fileName + ".pdf";
										apiRetention.setPdfPath(pdfDbPath);
										
										val wRetention = new WRetention();
										wRetention.setTaxpayer(this.taxpayer);
										wRetention.setApiRetention(apiRetention);
										val retentionEntity = dataStoreService.retentionStore(wRetention);
										if (!UValidator.isNullOrEmpty(retentionEntity)) {
											val filePath = repositoryPath + File.separator + dbPath;
											val fileFolder = new File(filePath); 
											if (!fileFolder.exists()){
												fileFolder.mkdirs();
											}
											
											// Storing XML
											val xmlPath = filePath + File.separator + fileName + ".xml";
											UFile.writeFile(stampResponse.getXml(), xmlPath);
											
											// Generating and storing PDF
											val pdfPath = filePath + File.separator + fileName + ".pdf";
											reportManager.retV2GeneratePdf(apiRetention, pdfPath);
											
											// Stamp control
											stampControl.stampControl(this.taxpayer);
											
											// Bitacora :: Registro de accion en bitacora
											OperationCode operationCode = retentionLog(apiRetention.getRetentionKey());
											appLog.logOperation(operationCode);
											
											// Reset
											taxReset();
											
											// Toastr Notification
											response = getSuccessMessage();
										} else {
											errorMsg = "Ha ocurrido un error inesperado registrando la información de la retención";
										}
									} else {
										errorMsg = stampResponse.getErrorMessage();
									}
								} else {						
									errorMsg = "Ha ocurrido un error inesperado generando la retención.";
								}
							} else {
								response = JResponse.markedError(errors);
								if (sectionError) {
									errorMsg = "Debe completar correctamente la información de todas las secciones de la retención";
								}
							}
						} else {
							errorMsg = UProperties.getMessage("ERR0004", userTools.getLocale());
						}
					} else {
						errorMsg = UProperties.getMessage("ERR0007", userTools.getLocale());
					}
				} else {
					errorMsg = this.unexpectedError;
				}
			} else {
				errorMsg = UProperties.getMessage("ERR0163", userTools.getLocale());
			}
		} catch (Exception e) {
			e.printStackTrace();
			errorMsg = UProperties.getMessage("ERR0164", userTools.getLocale());
		}
		
		if (errorMsg != null) {
			response = JResponse.error(JNotification.pageMessageError(errorMsg));
		}
		
		sessionController.sessionUpdate();
		
		retentionJson.setResponse(response);
		return GsonClient.response(retentionJson);
	}

	private JResponse getSuccessMessage() {
		val toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.generate.retention.title", userTools.getLocale());
		val toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.generate.retention.message", userTools.getLocale());
		val toastr = Toastr.success(toastrTitle, toastrMessage);
		return JResponse.success(JNotification.toastrNotification(toastr));
	}
	
	private OperationCode retentionLog(final String retentionCode) {
		switch (retentionCode) {
		case "01":
			return OperationCode.GENERACION_RETENCION_CLAVE_01_ID88;
		case "02":
			return OperationCode.GENERACION_RETENCION_CLAVE_02_ID89;
		case "03":
			return OperationCode.GENERACION_RETENCION_CLAVE_03_ID90;
		case "04":
			return OperationCode.GENERACION_RETENCION_CLAVE_04_ID91;
		case "05":
			return OperationCode.GENERACION_RETENCION_CLAVE_05_ID92;
		case "06":
			return OperationCode.GENERACION_RETENCION_CLAVE_06_ID93;
		case "07":
			return OperationCode.GENERACION_RETENCION_CLAVE_07_ID94;
		case "08":
			return OperationCode.GENERACION_RETENCION_CLAVE_08_ID95;
		case "09":
			return OperationCode.GENERACION_RETENCION_CLAVE_09_ID96;
		case "10":
			return OperationCode.GENERACION_RETENCION_CLAVE_10_ID97;
		case "11":
			return OperationCode.GENERACION_RETENCION_CLAVE_11_ID98;
		case "12":
			return OperationCode.GENERACION_RETENCION_CLAVE_12_ID99;
		case "13":
			return OperationCode.GENERACION_RETENCION_CLAVE_13_ID100;
		case "14":
			return OperationCode.GENERACION_RETENCION_CLAVE_14_ID101;
		case "15":
			return OperationCode.GENERACION_RETENCION_CLAVE_15_ID102;
		case "16":
			return OperationCode.GENERACION_RETENCION_CLAVE_16_ID103;
		case "17":
			return OperationCode.GENERACION_RETENCION_CLAVE_17_ID104;
		case "18":
			return OperationCode.GENERACION_RETENCION_CLAVE_18_ID105;
		case "19":
			return OperationCode.GENERACION_RETENCION_CLAVE_19_ID106;
		case "20":
			return OperationCode.GENERACION_RETENCION_CLAVE_20_ID107;
		case "21":
			return OperationCode.GENERACION_RETENCION_CLAVE_21_ID108;
		case "22":
			return OperationCode.GENERACION_RETENCION_CLAVE_22_ID109;
		case "23":
			return OperationCode.GENERACION_RETENCION_CLAVE_23_ID110;
		case "24":
			return OperationCode.GENERACION_RETENCION_CLAVE_24_ID111;
		case "25":
			return OperationCode.GENERACION_RETENCION_CLAVE_25_ID112;
		default:
			break;
		}
		return null;
	}
	
}