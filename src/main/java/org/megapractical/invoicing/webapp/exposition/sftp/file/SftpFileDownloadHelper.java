package org.megapractical.invoicing.webapp.exposition.sftp.file;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.webapp.exposition.cfdi.payload.FileDownload;
import org.springframework.util.FileCopyUtils;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Slf4j(topic = "FileDownloadService")
public final class SftpFileDownloadHelper {
	
	private SftpFileDownloadHelper() {		
	}
	
	public static void download(FileDownload fileDownload, Long id, HttpServletResponse response) {
		try {
			if (fileDownload != null) {
				log.info("Downloading file with id {}", id);
				val header = fileDownload.getHeader();
				
				response.setContentType(fileDownload.getContentType());
				response.setContentLength(fileDownload.getFileByte().length);
				response.setHeader("Content-Disposition", header);
				
				FileCopyUtils.copy(fileDownload.getFileByte(), response.getOutputStream());
			} else {
				throw new SftpFileDownloadException(String.format("File download content is null for file with id %s", id));
			}
		} catch (IOException e) {
			log.error("Error downloading file with id {}", id);
			throw new SftpFileDownloadException(String.format("Error downloading file with id %s", id));
		}
	}
	
}