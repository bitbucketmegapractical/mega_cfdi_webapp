package org.megapractical.invoicing.webapp.exposition.cfdi;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiSolicitudCancelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.megapractical.invoicing.dal.data.repository.ICfdiCancelationRequestJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPersonTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPreInvoiceJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxpayerCertificateJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IVoucherTypeJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.CancelationService;
import org.megapractical.invoicing.webapp.exposition.service.CfdiDownloadService;
import org.megapractical.invoicing.webapp.exposition.service.CfdiNotificationService;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JCfdi;
import org.megapractical.invoicing.webapp.json.JCfdi.Cfdi;
import org.megapractical.invoicing.webapp.json.JCfdiNotification;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.mail.MailService;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import lombok.val;

@Controller
@Scope("session")
public class CfdiController {
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	PropertiesTools propertiesTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	CancelationService cancelationService;
	
	@Autowired
	CfdiNotificationService cfdiNotificationService;
	
	@Autowired
	CfdiDownloadService cfdiDownloadService;
	
	@Resource
	ICfdiJpaRepository iCfdiJpaRepository;
	
	@Resource
	IPersonTypeJpaRepository iPersonTypeJpaRepository;
	
	@Resource
	ITaxpayerCertificateJpaRepository iTaxpayerCertificateJpaRepository;
	
	@Resource
	IPreInvoiceJpaRepository iPreInvoiceJpaRepository;
	
	@Resource
	ICfdiCancelationRequestJpaRepository iCfdiCancelationRequestJpaRepository;
	
	@Resource
	IVoucherTypeJpaRepository iVoucherTypeJpaRepository;
	
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	private static final String SORT_PROPERTY = "id";
	private static final Integer INITIAL_PAGE = 0;
	private static final String CFDI_PATH = "cfdi.repository";
	
	//##### Contribuyente
	private ContribuyenteEntity taxpayer;
	
	
	//##### Mensaje procesando...
	@ModelAttribute("processingRequest")
	public String processingMessage() {
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	//##### Mensaje cargando...
	@ModelAttribute("loadingRequest")
	public String loadingMessage() {
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	//##### Css class danger
	@ModelAttribute("cssAlertDanger")
	public String cssAlertDanger() {
		return CssStyleClass.ERROR.value();
	}
	
	@ModelAttribute("invalidCfdi")
	public String invalidCfdi() {
		return UProperties.getMessage("ERR0057", userTools.getLocale());
	}
	
	//##### Cargando Tipo contribuyente (Tipo persona)
	public List<String> personTypeSource() {
		List<String> personTypeSource = new ArrayList<>();
		try {
			
			Iterable<TipoPersonaEntity> personTypeEntitySource = iPersonTypeJpaRepository.findByEliminadoFalse();
			personTypeEntitySource.forEach(item -> personTypeSource.add(item.getValor()));	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return personTypeSource;
	}
	
	@GetMapping("/HelpDesk")
	public String helpDesk(Model model) {
		return "/invoicing/HelpDesk";
	}
	
	@GetMapping("/CfdiList")
	public String cfdiList(Model model) {
		try {
			model.addAttribute("page", INITIAL_PAGE);
			model.addAttribute("personTypeSource", personTypeSource());
			
			HtmlHelper.functionalitySelected("MyCFDIs", "invoicing-inbox");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			//##### Cargando el contribuyente por el rfc activo
			this.taxpayer = userTools.getContribuyenteActive(userTools.getContribuyenteActive().getRfcActivo());
			
			return "/invoicing/CfdiList";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@PostMapping("/CfdiList")
	public @ResponseBody String cfdiList(HttpServletRequest request) {
		
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JCfdi cfdi = new JCfdi();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String determineQuery = null;
			
			Integer paginateSize = appSettings.getPaginationSize();
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			Boolean cfdiValid = false;
			Boolean cfdiCanceled = false;
			Boolean cfdiCancelationRequest = false;
			String cfdiType = UBase64.base64Decode(request.getParameter("cfdiType"));
			if (!UValidator.isNullOrEmpty(cfdiType)) {
				if (cfdiType.equals("cfdiValid")) {
					cfdiType = "cfdiValid";
					cfdiValid = true;
				}else if (cfdiType.equals("cfdiCanceled")) {
					cfdiType = "cfdiCanceled";
					cfdiCanceled = true;
				}else if (cfdiType.equals("cfdiCancelationRequest")) {
					cfdiType = "cfdiCancelationRequest";
					cfdiCancelationRequest = true;
				}
			}else{
				cfdiType = "cfdiValid";
				cfdiValid = true;
			}
			cfdi.setCfdiType(UBase64.base64Encode(cfdiType));
			
			String searchUuid = UBase64.base64Decode(request.getParameter("searchUuid"));
			if (UValidator.isNullOrEmpty(searchUuid)) {
				searchUuid = "";
			}
			
			String searchRfc = UBase64.base64Decode(request.getParameter("searchRfc"));
			if (UValidator.isNullOrEmpty(searchRfc)) {
				searchRfc = "";
			}
			
			String searchClave = UBase64.base64Decode(request.getParameter("searchClave"));
			if (!UValidator.isNullOrEmpty(searchClave)) {
				determineQuery = "clave";
			}
			
			String searchDateStart = UBase64.base64Decode(request.getParameter("searchDateStart"));
			Date startDate = null;
			
			String searchDateEnd = UBase64.base64Decode(request.getParameter("searchDateEnd"));
			Date endDate = null;
			
			Boolean error = false;
			if (!UValidator.isNullOrEmpty(searchDateStart) && !UValidator.isNullOrEmpty(searchDateEnd)) {
				startDate = searchDateStart != null ? UDate.formattedDate(searchDateStart, DATE_FORMAT) : null;
				endDate = searchDateEnd != null ? UDate.formattedDate(searchDateEnd, DATE_FORMAT) : null;
				if (UValidator.isNullOrEmpty(startDate) || UValidator.isNullOrEmpty(endDate)) {
					error = true;
				}else{
					if (UDateTime.isAfterLocalDate(startDate, endDate)) {
						error = true;
					}else{
						determineQuery = determineQuery != null ?  determineQuery.concat("_between") : "between";
					}
				}
			}else if (!UValidator.isNullOrEmpty(searchDateStart)) {
				startDate = searchDateStart != null ? UDate.formattedDate(searchDateStart, DATE_FORMAT) : null;
				if (UValidator.isNullOrEmpty(startDate)) {
					error = true;
				}else{
					determineQuery = determineQuery != null ?  determineQuery.concat("_after") : "after";
				}
			}else if (!UValidator.isNullOrEmpty(searchDateEnd)) {
				endDate = searchDateEnd != null ? UDate.formattedDate(searchDateEnd, DATE_FORMAT) : null;
				if (UValidator.isNullOrEmpty(endDate)) {
					error = true;
				}else{
					determineQuery = determineQuery != null ?  determineQuery.concat("_before") : "before";
				}
			}
			determineQuery = determineQuery == null ? "default" : determineQuery;
			
			String paginate = UBase64.base64Decode(request.getParameter("paginateSize"));
			if (!UValidator.isNullOrEmpty(paginate)) {
				paginateSize = UValue.integerValue(paginate);
			}
			
			if (error) {
				notification.setMessage(UProperties.getMessage("ERR0056", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
				cfdi.setResponse(response);
			}else{
				Sort sort = Sort.by(Sort.Order.desc(SORT_PROPERTY));
				//PageRequest pageRequest = PageRequest.of(page, paginateSize, sort);
				
				if (cfdiCanceled) {
					sort = Sort.by(Sort.Order.desc("fechaCancelado"), Sort.Order.desc("horaCancelado"));							
				}
				PageRequest pageRequest = PageRequest.of(page, paginateSize, sort);
				Page<CfdiEntity> cfdiEntityPage = null;
				
				switch (determineQuery) {
					case "clave":
						if (cfdiValid) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(this.taxpayer, searchUuid, searchRfc, searchClave, pageRequest);
						}else if (cfdiCanceled) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(this.taxpayer, searchUuid, searchRfc, searchClave, pageRequest);
						}else if (cfdiCancelationRequest) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(this.taxpayer, searchUuid, searchRfc, searchClave, pageRequest);
						}
						break;
					case "clave_between":
						if (cfdiValid) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(this.taxpayer, searchUuid, searchRfc, searchClave, startDate, endDate, pageRequest);
						}else if (cfdiCanceled) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(this.taxpayer, searchUuid, searchRfc, searchClave, startDate, endDate, pageRequest);
						}else if (cfdiCancelationRequest) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(this.taxpayer, searchUuid, searchRfc, searchClave, startDate, endDate, pageRequest);
						}
						break;
					case "clave_after":
						if (cfdiValid) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(this.taxpayer, searchUuid, searchRfc, searchClave, startDate, pageRequest);
						}else if (cfdiCanceled) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(this.taxpayer, searchUuid, searchRfc, searchClave, startDate, pageRequest);
						}else if (cfdiCancelationRequest) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(this.taxpayer, searchUuid, searchRfc, searchClave, startDate, pageRequest);
						}
						break;
					case "clave_before":
						if (cfdiValid) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(this.taxpayer, searchUuid, searchRfc, searchClave, endDate, pageRequest);
						}else if (cfdiCanceled) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(this.taxpayer, searchUuid, searchRfc, searchClave, endDate, pageRequest);
						}else if (cfdiCancelationRequest) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(this.taxpayer, searchUuid, searchRfc, searchClave, endDate, pageRequest);
						}
						break;
					case "between":
						if (cfdiValid) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(this.taxpayer, searchUuid, searchRfc, startDate, endDate, pageRequest);
						}else if (cfdiCanceled) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(this.taxpayer, searchUuid, searchRfc, startDate, endDate, pageRequest);
						}else if (cfdiCancelationRequest) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(this.taxpayer, searchUuid, searchRfc, startDate, endDate, pageRequest);
						}
						break;
					case "after":
						if (cfdiValid) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(this.taxpayer, searchUuid, searchRfc, startDate, pageRequest);
						}else if (cfdiCanceled) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(this.taxpayer, searchUuid, searchRfc, startDate, pageRequest);
						}else if (cfdiCancelationRequest) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(this.taxpayer, searchUuid, searchRfc, startDate, pageRequest);
						}
						break;
					case "before":
						if (cfdiValid) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(this.taxpayer, searchUuid, searchRfc, endDate, pageRequest);
						}else if (cfdiCanceled) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(this.taxpayer, searchUuid, searchRfc, endDate, pageRequest);
						}else if (cfdiCancelationRequest) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(this.taxpayer, searchUuid, searchRfc, endDate, pageRequest);
						}
						break;
					default:
						if (cfdiValid) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(this.taxpayer, searchUuid, searchRfc, pageRequest);
						}else if (cfdiCanceled) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(this.taxpayer, searchUuid, searchRfc, pageRequest);
						}else if (cfdiCancelationRequest) {
							cfdiEntityPage = iCfdiJpaRepository.findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(this.taxpayer, searchUuid, searchRfc, pageRequest);
							//cfdiEntityPage = iCfdiJpaRepository.findByFechaSolicitud(this.taxpayer.getId(),searchUuid,searchRfc,pageRequest);
						}
						break;
				}
				
				val pagination = JPagination.buildPaginaton(cfdiEntityPage);
				cfdi.setPagination(pagination);
				
				for (CfdiEntity item : cfdiEntityPage) {
					Cfdi cfdiObj = new Cfdi();
					cfdiObj.setUuid(UBase64.base64Encode(item.getUuid()));
					cfdiObj.setExpeditionDate(UBase64.base64Encode(UDate.formattedDate(item.getFechaExpedicion(), DATE_FORMAT)));
					cfdiObj.setReceiverRfc(UBase64.base64Encode(item.getReceptorRfc()));
					if ( item.getSerie() != null ) {
						if ( item.getFolio() != null ) {
							cfdiObj.setSerieFolio( UBase64.base64Encode(item.getSerie() + "-" + item.getFolio()) );
						}
						else {
							cfdiObj.setSerieFolio( UBase64.base64Encode(item.getSerie() + "-"));
						}
					}else if ( item.getFolio() != null) {
						cfdiObj.setSerieFolio( UBase64.base64Encode( "-" + item.getFolio() ) );
					}else {
						cfdiObj.setSerieFolio( UBase64.base64Encode( "-") );
					}
					cfdiObj.setConfirmationCode(UBase64.base64Encode(""));
					
					cfdiObj.setCancelated(item.isCancelado());
					if (item.isCancelado()) {
						if (!UValidator.isNullOrEmpty(item.getFechaCancelado()) && !UValidator.isNullOrEmpty(item.getHoraCancelado())) {
							String datetime = UBase64.base64Encode(UDateTime.format(item.getFechaCancelado()) + " " + UDateTime.formatShort(item.getHoraCancelado()));
							cfdiObj.setCancelatedDateTime(datetime);
						}						
						cfdiObj.setCancelatedAcuseXml(UBase64.base64Encode(item.getAcuseCancelacion()));
					}
					
					cfdiObj.setCancelationRequest(item.isSolicitudCancelacion());
					if (item.isSolicitudCancelacion()) {
						CfdiSolicitudCancelacionEntity cancelRequest = iCfdiCancelationRequestJpaRepository.findByUuidIgnoreCaseAndSolicitudActivaTrue(item.getUuid());
						if (!UValidator.isNullOrEmpty(cancelRequest)) {
							String datetime = UBase64.base64Encode(UDate.formattedDate(cancelRequest.getFechaSolicitud(), DATE_FORMAT) + " " + UDate.formattedShortTime(cancelRequest.getHoraSolicitud()));
							cfdiObj.setCancelationRequestDateTime(datetime);
							if (!UValidator.isNullOrEmpty(cancelRequest.getFechaUltimaActualizacion()) && !UValidator.isNullOrEmpty(cancelRequest.getHoraUltimaActualizacion())) {
								datetime = UBase64.base64Encode(UDate.formattedDate(cancelRequest.getFechaUltimaActualizacion(), DATE_FORMAT) + " " + UDate.formattedShortTime(cancelRequest.getHoraUltimaActualizacion()));
								cfdiObj.setCancelationRequestLastStatusVerified(datetime);
							}
						}
					}
					val voucherType = iVoucherTypeJpaRepository.findByCodigoAndEliminadoFalse(item.getTipoComprobante());
					cfdiObj.setTipoComprobante(UBase64.base64Encode((voucherType.getValor())));
					cfdi.getCfdis().add(cfdiObj);
				}
				
				String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
				if (!UValidator.isNullOrEmpty(searchUuid) || !UValidator.isNullOrEmpty(searchRfc) || !UValidator.isNullOrEmpty(searchClave) || !UValidator.isNullOrEmpty(startDate) || !UValidator.isNullOrEmpty(endDate)) {
					notification.setSearch(Boolean.TRUE);
					searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
				}
				
				if (cfdi.getCfdis().isEmpty()) {
					notification.setSearchResultMessage(searchResultEmpty);
					notification.setPanelMessage(Boolean.TRUE);
					notification.setDismiss(Boolean.FALSE);
					notification.setCssStyleClass(CssStyleClass.INFO.value());
				}
				
				response.setSuccess(Boolean.TRUE);
				response.setNotification(notification);
				cfdi.setResponse(response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sessionController.sessionUpdate();
		
		jsonInString = gson.toJson(cfdi);
		return jsonInString;
	}
	
	@ResponseBody
	@GetMapping("/cfdi/download/{uuid}/{fileType}")
	public ModelAndView cfdiDownload(@PathVariable(value = "uuid") String uuid,
									 @PathVariable(value = "fileType") String fileType, 
									 HttpServletResponse response) {
		try {
			val fileDownload = cfdiDownloadService.download(uuid, fileType);
			if (fileDownload != null) {
				val header = fileDownload.getHeader();
				
				response.setContentType(fileDownload.getContentType());
				response.setContentLength(fileDownload.getFileByte().length);
				response.setHeader("Content-Disposition", header);
				
				FileCopyUtils.copy(fileDownload.getFileByte(), response.getOutputStream());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		sessionController.sessionUpdate();
		return null;
    }
	
	@ResponseBody
	@GetMapping("/cfdis/{file_name}")
	public FileSystemResource getFile(@PathVariable("file_name") String fileName)  {

		String[] fileNameSplitted = fileName.split(Pattern.quote("."));
		String uuid = fileNameSplitted[0];
		String ext = fileNameSplitted[1];
		
		String repositoryPath = appSettings.getPropertyValue(CFDI_PATH);
		CfdiEntity cfdiEntity = iCfdiJpaRepository.findByUuidIgnoreCaseAndCanceladoFalse(uuid);
		
		if (!UValidator.isNullOrEmpty(cfdiEntity)) {
			String path = repositoryPath + File.separator + (ext.equalsIgnoreCase("xml") ? cfdiEntity.getRutaXml() : cfdiEntity.getRutaPdf());
			return new FileSystemResource(new File(path));
		}
		
		return null;
	}
	
	@PostMapping("/cfdiMailSend")
	public @ResponseBody String cfdiMailSend(HttpServletRequest request) {
		val cfdiNotificationRequest = request.getParameter("cfdiNotificationRequest");
		val cfdiNotificationJson = GsonClient.deserialize(cfdiNotificationRequest, JCfdiNotification.class);
		cfdiNotificationService.sendNotification(cfdiNotificationJson);
		
		sessionController.sessionUpdate();
		return GsonClient.response(cfdiNotificationJson);
	}
	
}