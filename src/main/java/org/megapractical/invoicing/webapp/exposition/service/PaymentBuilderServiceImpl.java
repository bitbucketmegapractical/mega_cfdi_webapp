package org.megapractical.invoicing.webapp.exposition.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.megapractical.invoicing.api.common.ApiRelatedCfdi;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiCmp;
import org.megapractical.invoicing.api.wrapper.ApiEmitter;
import org.megapractical.invoicing.api.wrapper.ApiPaymentError;
import org.megapractical.invoicing.api.wrapper.ApiPaymentParser;
import org.megapractical.invoicing.api.wrapper.ApiReceiver;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.PaymentConceptService;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.PaymentService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.PaymentEntryService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.PaymentFileService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.VoucherPaymentFileService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.validator.ReceiverFileValidator;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentBaseTax;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentRelatedDocument;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentWithheld;
import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPaymentTotal;
import org.megapractical.invoicing.webapp.json.JReceiver;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.json.JVoucher.JVoucherResponse;
import org.megapractical.invoicing.webapp.util.PaymentUtils;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment.WRelatedDocument;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPaymentTotal;
import org.megapractical.invoicing.webapp.wrapper.WTaxes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class PaymentBuilderServiceImpl implements PaymentBuilderService {
	
	@Autowired
	private VoucherPaymentFileService voucherPaymentFileService;
	
	@Autowired
	private ReceiverFileValidator receiverFileValidator;
	
	@Autowired
	private PaymentFileService paymentFileService;
	
	@Autowired
	private PaymentEntryService paymentEntryService;
	
	@Autowired
	private PaymentConceptService paymentConceptService;
	
	@Autowired
	private PaymentService paymentService;
	
	private static final String RECEIVER_CFDI_USE = "CP01";
	
	private JVoucherResponse voucherResponse;
	private ApiReceiver apiReceiver;
	private ApiRelatedCfdi apiRelatedCfdi;
	private Map<String, WPayment> payments;
	private Map<String, List<WTaxes>> paymentWithhelds;
	private Map<String, List<WTaxes>> paymentTransferreds;
	private Map<String, List<WRelatedDocument>> relatedDocuments;
	private Map<String, List<WTaxes>> relDocWithhelds;
	private Map<String, List<WTaxes>> relDocTransferreds;
	private WPaymentTotal paymentTotals;
	
	@Override
	public List<ApiCfdi> build(ApiPaymentParser paymentParser) {
		paymentParser.getStatistics().getInvoiceFileStatistics().setFechaInicioGeneracionComprobante(new Date());
		paymentParser.getStatistics().getInvoiceFileStatistics().setHoraInicioGeneracionComprobante(new Date());

		val apiEmitter = paymentParser.getPaymentStamp().getApiEmitter();
		val preferences = paymentParser.getPaymentStamp().getPreferences();
		val certificate = paymentParser.getPaymentStamp().getCertificate();
		val vouchers = new ArrayList<ApiCfdi>();

		// List of entries to generate vouchers
		val entries = new ArrayList<String[]>();
		if (paymentParser.getStatistics().getEntries() != null
				&& !paymentParser.getStatistics().getEntries().isEmpty()) {
			entries.addAll(paymentParser.getStatistics().getEntries());
		}

		var counter = 0;
		while (counter < entries.size()) {
			try {
				val entry = entries.get(counter);
				buildPayment(entry);

				// Verify whether the next entry exists
				val nextEntry = getNextEntry(entries, counter);
				var isNextEntryPrincipal = isNextEntryPrincipal(nextEntry);

				if (isNullOrEmptyEntry(nextEntry) || isNextEntryPrincipal) {
					val voucher = populateData(apiEmitter, apiReceiver, preferences, certificate, voucherResponse, paymentTotals);
					if (voucher != null) {
						vouchers.add(voucher);
					} else {
						populatePaymentError(paymentParser, entry, null);
					}
				}
				counter++;
			} catch (Exception e) {
				populatePaymentError(paymentParser, entries.get(counter), Arrays.toString(e.getStackTrace()));
				counter = incrementToNextPrincipalEntryLine(entries, counter);
			}
		}

		paymentParser.getStatistics().getInvoiceFileStatistics().setFechaFinGeneracionComprobante(new Date());
		paymentParser.getStatistics().getInvoiceFileStatistics().setHoraFinGeneracionComprobante(new Date());

		return vouchers;
	}
	
	private void init() {
		voucherResponse = null;
		apiReceiver = null;
		apiRelatedCfdi = new ApiRelatedCfdi();
		payments = new HashMap<>();
		paymentTransferreds = new HashMap<>();
		paymentWithhelds = new HashMap<>();
		relatedDocuments = new HashMap<>();
		relDocWithhelds = new HashMap<>();
		relDocTransferreds = new HashMap<>();
		paymentTotals = null;
	}
	
	private void buildPayment(String[] entry) {
		if (entry[0].equalsIgnoreCase("CP")) {
			init();
		}
		
		if (entry[0].equalsIgnoreCase("COMP")) {
			voucherResponse = populateVoucher(entry);
		}
		
		if (entry[0].trim().equalsIgnoreCase("REC")) {
			apiReceiver = populateReceiver(entry);
		}
		
		populateRelatedCfdis(entry, apiRelatedCfdi);
		populatePayment(entry, payments);
		populatePaymentWithhelds(entry, paymentWithhelds);
		populatePaymentTransferreds(entry, paymentTransferreds);
		populateRelDoc(entry, relatedDocuments);
		populateRelDocWithhelds(entry, relDocWithhelds);
		populateRelDocTransferreds(entry, relDocTransferreds);
		
		if (entry[0].equalsIgnoreCase("TOT")) {
			paymentTotals = populatePaymentTotal(entry);
		}
	}
	
	private JVoucherResponse populateVoucher(String[] entry) {
		val voucher = JVoucher
						.builder()
						.serie(UValue.stringValue(entry[1]))
						.folio(UValue.stringValue(entry[2]))
						.postalCode(UValue.stringValue(entry[3]))
						.exportation(UValue.stringValue(entry[4]))
						.build();
		return voucherPaymentFileService.populateVoucher(voucher);
	}
	
	private ApiReceiver populateReceiver(String[] entry) {
		val receiverData = JReceiver
							.builder()
							.rfc(UValue.stringValue(entry[1]))
							.nameOrBusinessName(UValue.stringValue(entry[2]))
							.resident(UValue.stringValue(entry[3]))
							.residency(UValue.stringValue(entry[4]))
							.identityRegistrationNumber(UValue.stringValue(entry[5]))
							.postalCode(UValue.stringValue(entry[6]))
							.taxRegime(UValue.stringValue(entry[7]))
							.cfdiUse(RECEIVER_CFDI_USE)
							.build();
		return receiverFileValidator.getReceiver(receiverData);
	}
	
	private void populateRelatedCfdis(String[] entry, final ApiRelatedCfdi apiRelatedCfdi) {
		if (entry[0].equalsIgnoreCase("UUID")) {
			val uuid = UValue.stringValue(entry[1]);
			val relatedPaymentUuid = new RelatedCfdiUuid(uuid);
			apiRelatedCfdi.getUuids().add(relatedPaymentUuid);
		}
	}
	
	private void populatePayment(String[] entry, final Map<String, WPayment> payments) {
		if (entry[0].trim().equalsIgnoreCase("PAG")) {
			val paymentNumber = UValue.stringValue(entry[1]);
			val paymentData = ComplementPayment
								.builder()
								.paymentDate(UValue.stringValue(entry[2]))
								.paymentWay(UValue.stringValue(entry[3]))
								.amount(UValue.stringValue(entry[4]))
								.currency(UValue.stringValue(entry[5]))
								.changeType(UValue.stringValue(entry[6]))
								.operationNumber(UValue.stringValue(entry[7]))
								.sourceAccountRfc(UValue.stringValue(entry[8]))
								.payerAccount(UValue.stringValue(entry[9]))
								.bank(UValue.stringValue(entry[10]))
								.targetAccountRfc(UValue.stringValue(entry[11]))
								.receiverAccount(UValue.stringValue(entry[12]))
								.stringType(UValue.stringValue(entry[13]))
								.originalString(UValue.stringValue(entry[14]))
								.certificate(UValue.stringValue(entry[15]))
								.seal(UValue.stringValue(entry[16]))
								.build();
			paymentFileService.populatePayment(paymentNumber, paymentData, payments);
		}
	}
	
	private void populatePaymentWithhelds(String[] entry, final Map<String, List<WTaxes>> paymentWithhelds) {
		if (entry[0].trim().equalsIgnoreCase("PAGIR")) {
			val paymentNumber = UValue.stringValue(entry[1]);
			val taxData = ComplementPaymentWithheld
							.builder()
							.tax(UValue.stringValue(entry[2]))
							.amount(UValue.stringValue(entry[3]))
							.build();
			paymentFileService.populatePaymentWithhelds(paymentNumber, taxData, paymentWithhelds);
		}
	}
	
	private void populatePaymentTransferreds(String[] entry, final Map<String, List<WTaxes>> paymentTransferreds) {
		if (entry[0].trim().equalsIgnoreCase("PAGIT")) {
			val paymentNumber = UValue.stringValue(entry[1]);
			val taxData = populateTax(entry);
			paymentFileService.populatePaymentTransferreds(paymentNumber, taxData, paymentTransferreds);
		}
	}
	
	private void populateRelDoc(String[] entry, Map<String, List<WRelatedDocument>> relatedDocuments) {
		if (entry[0].trim().equalsIgnoreCase("PAGDR")) {
			val paymentNumber = UValue.stringValue(entry[1]);
			val relDocData = ComplementPaymentRelatedDocument
								.builder()
								.documentId(UValue.stringValue(entry[2]))
								.serie(UValue.stringValue(entry[3]))
								.sheet(UValue.stringValue(entry[4]))
								.currency(UValue.stringValue(entry[5]))
								.changeType(UValue.stringValue(entry[6]))
								.partiality(UValue.stringValue(entry[7]))
								.amountPartialityBefore(UValue.stringValue(entry[8]))
								.amountPaid(UValue.stringValue(entry[9]))
								.difference(UValue.stringValue(entry[10]))
								.taxObject(UValue.stringValue(entry[11]))
								.build();
			paymentFileService.populateRelatedDocument(paymentNumber, relDocData, relatedDocuments);
		}
	}
	
	private void populateRelDocWithhelds(String[] entry, final Map<String, List<WTaxes>> relDocWithhelds) {
		if (entry[0].trim().equalsIgnoreCase("PAGDRIR")) {
			val docId = UValue.stringValue(entry[1]);
			val taxData = populateTax(entry);
			paymentFileService.populateRelDocWithhelds(docId, taxData, relDocWithhelds);
		}
	}
	
	private void populateRelDocTransferreds(String[] entry, final Map<String, List<WTaxes>> relDocTransferreds) {
		if (entry[0].trim().equalsIgnoreCase("PAGDRIT")) {
			val docId = UValue.stringValue(entry[1]);
			val taxData = populateTax(entry);
			paymentFileService.populateRelDocTransferreds(docId, taxData, relDocTransferreds);
		}
	}
	
	private WPaymentTotal populatePaymentTotal(String[] entry) {
		val complementPaymentTotal = ComplementPaymentTotal
										.builder()
										.totalWithheldIva(UValue.stringValue(entry[1]))
										.totalWithheldIsr(UValue.stringValue(entry[2]))
										.totalWithheldIeps(UValue.stringValue(entry[3]))
										.totalTransferredBaseIva16(UValue.stringValue(entry[4]))
										.totalTransferredTaxIva16(UValue.stringValue(entry[5]))
										.totalTransferredBaseIva8(UValue.stringValue(entry[6]))
										.totalTransferredTaxIva8(UValue.stringValue(entry[7]))
										.totalTransferredBaseIva0(UValue.stringValue(entry[8]))
										.totalTransferredTaxIva0(UValue.stringValue(entry[9]))
										.totalTransferredBaseIvaExempt(UValue.stringValue(entry[10]))
										.totalAmountPayments(UValue.stringValue(entry[11]))
										.build();
		return paymentFileService.paymentTotal(complementPaymentTotal);
	}
	
	private ComplementPaymentBaseTax populateTax(String[] entry) {
		return ComplementPaymentBaseTax
				.builder()
				.base(UValue.stringValue(entry[2]))
				.tax(UValue.stringValue(entry[3]))
				.factorType(UValue.stringValue(entry[4]))
				.rateOrFee(UValue.stringValue(entry[5]))
				.amount(UValue.stringValue(entry[6]))
				.build();
	}
	
	private ApiCfdi populateData(ApiEmitter emitter, 
								 ApiReceiver receiver,
								 PreferenciasEntity preferences,
								 ContribuyenteCertificadoEntity certificate,
								 JVoucherResponse voucherResponse, 
								 WPaymentTotal paymentTotals) {
		if (voucherResponse != null && receiver != null && preferences != null && paymentTotals != null) {
			val complement = fillComplement(paymentTotals);
			if (complement != null) {
				val concept = paymentConceptService.paymentConcept();
				return ApiCfdi
						.builder()
						.voucher(voucherResponse.getApiVoucher())
						.voucherType(voucherResponse.getVoucherType())
						.currency(voucherResponse.getCurrency())
						.postalCode(voucherResponse.getExpeditionPlace())
						.receiver(receiver)
						.emitter(emitter)
						.preferences(preferences)
						.certificate(certificate)
						.concepts(Arrays.asList(concept))
						.complement(complement)
						.build();
			}
		}
		return null;
	}
	
	private ApiCmp fillComplement(WPaymentTotal paymentTotals) {
		if (payments != null) {
			val paymentList = payments.entrySet()
									  .stream()
									  .filter(i -> i.getValue() != null)
									  .map(fillPayent())
									  .map(Entry::getValue)
									  .collect(Collectors.toList());
			
			if (paymentTotals != null && !paymentList.isEmpty()) {
				val apiCmpPayment = paymentService.getPayment(paymentTotals, paymentList);
				return new ApiCmp(PaymentUtils.PAYMENT_PACKAGE, apiCmpPayment);
			}
		}
		return null;
	}

	private Function<? super Entry<String, WPayment>, ? extends Entry<String, WPayment>> fillPayent() {
		return entry -> {
			val paymentId = entry.getKey();
			val payment = entry.getValue();
			payment.getWithhelds().addAll(fillPaymentWithhelds(paymentId));
			payment.getTransferreds().addAll(fillPaymentTransferreds(paymentId));
			payment.getRelatedDocuments().addAll(fillRelatedDocument(paymentId));
			return entry;
		};
	}
	
	private List<WTaxes> fillPaymentWithhelds(String paymentId) {
		if (!paymentWithhelds.isEmpty()) {
			return paymentWithhelds.entrySet()
								   .stream()
								   .filter(i -> i.getKey().equals(paymentId))
								   .map(Map.Entry::getValue)
								   .flatMap(Collection::stream)
								   .collect(Collectors.toList());
		}
		return Collections.emptyList();
	}
	
	private List<WTaxes> fillPaymentTransferreds(String paymentId) {
		if (!paymentTransferreds.isEmpty()) {
			return paymentTransferreds.entrySet()
									  .stream()
									  .filter(i -> i.getKey().equals(paymentId))
									  .map(Map.Entry::getValue)
									  .flatMap(Collection::stream)
									  .collect(Collectors.toList());
		}
		return Collections.emptyList();
	}
	
	private List<WRelatedDocument> fillRelatedDocument(String paymentId) {
		if (!relatedDocuments.isEmpty()) {
			return relatedDocuments.entrySet()
							.stream()
							.filter(i -> i.getKey().equals(paymentId))
							.map(Map.Entry::getValue)
							.flatMap(Collection::stream)
							.collect(Collectors.toList())
							.stream()
							.map(i -> {
								i.getWithhelds().addAll(fillRelDocWithhelds(i.getDocumentId()));
								i.getTransferreds().addAll(fillRelDocTransferreds(i.getDocumentId()));
								return i;
							})
							.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}
	
	private List<WTaxes> fillRelDocWithhelds(String documentId) {
		if (!relDocWithhelds.isEmpty()) {
			return relDocWithhelds.entrySet()
								  .stream()
								  .filter(i -> i.getKey().equals(documentId))
								  .map(Map.Entry::getValue)
								  .flatMap(Collection::stream)
								  .collect(Collectors.toList());
		}
		return Collections.emptyList();
	}
	
	private List<WTaxes> fillRelDocTransferreds(String documentId) {
		if (!relDocTransferreds.isEmpty()) {
			return relDocTransferreds.entrySet()
					.stream()
					.filter(i -> i.getKey().equals(documentId))
					.map(Map.Entry::getValue)
					.flatMap(Collection::stream)
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}
	
	private String[] getNextEntry(List<String[]> entries, int currentPosition) {
		try {
			return entries.get(currentPosition + 1);
		} catch (IndexOutOfBoundsException e) {
			return new String[] {};
		}
	}
	
	private boolean isNextEntryPrincipal(String[] nextEntry) {
		if (!isNullOrEmptyEntry(nextEntry)) {
			return paymentEntryService.isPrincipal(nextEntry[0]);
		}
		return false;
	}
	
	private boolean isNullOrEmptyEntry(String[] entry) {
		return entry == null || entry.length == 0;
	}
	
	private void populatePaymentError(final ApiPaymentParser paymentParser, String[] entry, String stackTrace) {
		val error = new ApiPaymentError();
		error.setDescription("Error al intentar generar el comprobante del complemento de pago.");
		
		if (apiReceiver != null) {
			error.setReceiverRfc(apiReceiver.getRfc());
			error.setReceiverCurp(apiReceiver.getCurp());
			error.setReceiverName(apiReceiver.getNombreRazonSocial());
		}
		
		error.setEntry(paymentEntryService.findByName(entry[0]));
		error.setVoucherGenerateError(Boolean.TRUE);
		error.setVoucherGenerateErrorException(stackTrace);

		paymentParser.getErrors().add(error);
	}
	
	private Integer incrementToNextPrincipalEntryLine(List<String[]> entries, int currentPosition) {
		var counter = currentPosition + 1;
		while (counter < entries.size()) {
			val entry = entries.get(counter);
			if (entry[0].equalsIgnoreCase("CP")) {
				break;
			} else {
				counter++;
			}
		}
		return counter;
	}
}