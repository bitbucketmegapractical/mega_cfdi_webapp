package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface PdfFromXmlUpdaterService {
	
	void updateData(ApiCfdi cfdi);
	
	void updateData(ApiPayroll payroll);
	
}