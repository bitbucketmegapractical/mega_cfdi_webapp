package org.megapractical.invoicing.webapp.exposition.cancellation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CancellationNotificationResponse {
	private boolean sent;
	private String reason;
	
	public static CancellationNotificationResponse sent() {
		return CancellationNotificationResponse.builder().sent(true).build();
	}
	
	public static CancellationNotificationResponse error(String error) {
		return CancellationNotificationResponse.builder().sent(false).reason(error).build();
	}
}