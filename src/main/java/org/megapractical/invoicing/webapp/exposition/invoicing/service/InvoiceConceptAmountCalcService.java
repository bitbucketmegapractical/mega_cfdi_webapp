package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.json.JConcept;

public interface InvoiceConceptAmountCalcService {
	
	JConcept conceptAmountCalc(String objConcept, MonedaEntity currency);
	
}