package org.megapractical.invoicing.webapp.exposition.service.api.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UploadResponse {
	private String fileName;
	private String uploadPath;
	private boolean uploaded;
	private String error;
	
	public static UploadResponse error(String fileName, String error) {
		return UploadResponse
				.builder()
				.fileName(fileName)
				.uploaded(false)
				.error(error)
				.build();
	}
}