package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherChangeTypeResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;

public interface VoucherChangeTypeValidator {
	
	VoucherChangeTypeResponse getChangeType(JVoucher voucher, MonedaEntity currency, List<JError> errors);
	
}