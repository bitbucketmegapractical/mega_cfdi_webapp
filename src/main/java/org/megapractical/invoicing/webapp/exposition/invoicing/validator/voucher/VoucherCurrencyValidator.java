package org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.VoucherCurrencyResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JVoucher;

public interface VoucherCurrencyValidator {
	
	VoucherCurrencyResponse getCurrency(JVoucher voucher, List<JError> errors);
	
}