package org.megapractical.invoicing.webapp.exposition.cfdi.payload;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResourceData {
	private ResourceCfdiData resourceCfdiData;
	private ResourcePayrollData resourcePayrollData;
	private String xmlFileName;
	private String xmlPath;
	private String pdfFileName;
	private String pdfPath;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ResourceCfdiData {
		private CfdiEntity cfdi;
		private ApiCfdi apiCfdi;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ResourcePayrollData {
		private NominaEntity payroll;
		private ApiPayroll apiPayroll;
	}
	
}