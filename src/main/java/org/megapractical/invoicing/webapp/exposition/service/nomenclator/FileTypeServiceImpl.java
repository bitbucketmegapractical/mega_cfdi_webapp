package org.megapractical.invoicing.webapp.exposition.service.nomenclator;

import org.megapractical.invoicing.dal.bean.jpa.TipoArchivoEntity;
import org.megapractical.invoicing.dal.data.repository.IFileTypeJpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class FileTypeServiceImpl implements FileTypeService {
	
	private final IFileTypeJpaRepository iFileTypeJpaRepository;

	public FileTypeServiceImpl(IFileTypeJpaRepository iFileTypeJpaRepository) {
		this.iFileTypeJpaRepository = iFileTypeJpaRepository;
	}

	@Override
	public TipoArchivoEntity findByCode(String code) {
		return iFileTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
	}

}