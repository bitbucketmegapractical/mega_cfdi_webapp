package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CatalogValidatorResponse {
	private JError error;
	private boolean hasError;
	
	public CatalogValidatorResponse(JError error) {
		this.error = error;
		this.hasError = error != null;
	}
	
}