package org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment;

import org.megapractical.invoicing.webapp.json.JComplementPayment.ComplementPayment.ComplementPaymentRelatedDocument;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment.WRelatedDocument;

public interface RelatedDocumentFileService {
	
	WRelatedDocument populateRelatedDocument(ComplementPaymentRelatedDocument data);
	
}