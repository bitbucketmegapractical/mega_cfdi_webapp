package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.api.smarterweb.SWCfdiStamp;
import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.webapp.exposition.service.CfdiBuilderService;
import org.megapractical.invoicing.webapp.support.PropertiesTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
@Scope("session")
public class InvoiceServiceImpl implements InvoiceService {
	
	@Autowired
	private PropertiesTools propertiesTools;
	
	@Autowired
	private CfdiBuilderService cfdiBuilderService;
	
	@Override
	public StampResponse cfdiStamp(ApiCfdi apiCfdi, byte[] cert, byte[] privateKey, String keyPasswd) {
		val stampProperties = getStampProperties(apiCfdi, cert, privateKey, keyPasswd);
		return cfdiStamp(stampProperties);
	}
	
	@Override
	public StampResponse cfdiStamp(final StampProperties stampProperties) {
		return stamp(stampProperties);
	}

	private StampResponse stamp(final StampProperties stampProperties) {
		return SWCfdiStamp.cfdiStamp(stampProperties);
	}
	
	private StampProperties getStampProperties(ApiCfdi apiCfdi, byte[] cert, byte[] privateKey, String keyPasswd) {
		val allowStamp = cfdiBuilderService.voucherBuild(apiCfdi, true);
		val stampProperties = new StampProperties();
		stampProperties.setEmitterId(propertiesTools.getEmitterId());
		stampProperties.setSessionId(propertiesTools.getSessionId());
		stampProperties.setSourceSystem(propertiesTools.getSourceSystem());
		stampProperties.setCorrelationId(propertiesTools.getCorrelationId());
		stampProperties.setVoucher(allowStamp.getVoucher());
		stampProperties.setCertificate(cert);
		stampProperties.setPrivateKey(privateKey);
		stampProperties.setPasswd(keyPasswd);
		stampProperties.setEnvironment(propertiesTools.getEnvironment());
		stampProperties.setServices(propertiesTools.getServices());
		return stampProperties;
	}
	
}