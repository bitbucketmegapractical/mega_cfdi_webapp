package org.megapractical.invoicing.webapp.exposition.cancellation;

import static org.megapractical.invoicing.api.util.UValue.stringSafeValue;
import static org.megapractical.invoicing.api.util.UValue.stringValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.wrapper.ApiCancellationRequest;
import org.megapractical.invoicing.dal.bean.datatype.CancellationType;
import org.megapractical.invoicing.webapp.filereader.FileReader;
import org.megapractical.invoicing.webapp.util.UProperties;

import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
final class CancellationFileParserService {
	
	private CancellationFileParserService() {		
	}
	
	public static CancellationFileParser parseFile(String cancellationFilePath) {
		// File content
		val entries = FileReader.readFile(cancellationFilePath);
		// Parsing content
		return parse(entries);
	}
	
	private static CancellationFileParser parse(List<String[]> entries) {
		val cancellationRequests = new ArrayList<ApiCancellationRequest>();
		var recipients = new ArrayList<String>();
		val parserErrors = new ArrayList<String>();
		int line = 1;
		
		try {
			for (int i = 0; i < entries.size(); i++) {
				val entry = entries.get(i);
				
				if ("CAN".equals(stringValue(entry[0]))) {
					val source = stringValue(entry[2]).split(";");
					recipients.addAll(Arrays.asList(source));
				}
				
				if (CancellationType.CFDI.name().equals(stringValue(entry[0]))
						|| CancellationType.RET.name().equals(stringValue(entry[0]))) {
					val cancellationRequest = populateCancellationRequest(entry);
					if (AssertUtils.nonNull(cancellationRequest)) {
						cancellationRequests.add(cancellationRequest);
					}
				}
				
				line ++;
			}
		} catch (Exception e) {
			UPrint.logWithLine(UProperties.env(), "[ERROR] CANCELLATION FILE PARSER > AN ERROR OCCURRED WHILE PARSING THE FILE");
			parserErrors.add(determineErrorCause(line, e.getMessage()));
			e.printStackTrace();
		}
		
		return CancellationFileParser
				.builder()
				.cancellationRequests(cancellationRequests)
				.recipients(recipients)
				.parserErrors(parserErrors)
				.build();
	}
	
	private static ApiCancellationRequest populateCancellationRequest(String[] entry) {
		if (AssertUtils.nonNull(entry)) {
			return ApiCancellationRequest
					.builder()
					.type(CancellationType.valueOf(entry[0]))
					.uuid(stringValue(entry[1]))
					.receiverRfc(stringValue(entry[2]))
					.voucherTotal(stringValue(entry[3]))
					.reason(stringValue(entry[4]))
					.substitutionUuid(stringSafeValue(entry[5]))
					.build();
		}
		return null;
	}
	
	private static String determineErrorCause(int line, String errorCause) {
		val entry = "Linea " + line;
		try {
			val errorInt = Integer.valueOf(errorCause);
			return entry + " -> java.lang.ArrayIndexOutOfBoundsException: " + errorInt;
		} catch (NumberFormatException e) {
			return entry + " -> " + errorCause;
		}
	}
	
}