package org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment;

import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiPayment;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.PaymentTotalsResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.complement.payment.payload.WPaymentRequest;
import org.megapractical.invoicing.webapp.json.JComplementPayment;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPayment;
import org.megapractical.invoicing.webapp.wrapper.WComplementPayment.WPaymentTotal;

public interface PaymentService {
	
	boolean isPayment(String packageClass);
	
	ApiPayment getPayment(WPaymentTotal wPaymentTotal, List<WPayment> payments);
	
	JComplementPayment loadPaymentData(List<WPayment> payments, Integer index);
	
	JComplementPayment addOrEditPayment(String packageClass,
										String paymentData, 
										boolean isEdit, 
										Integer index,
										List<WPayment> payments);
	
	PaymentTotalsResponse populatePaymentTotals(String data, List<JError> errors);
	
	WPayment populatePayment(WPaymentRequest request);
	
}