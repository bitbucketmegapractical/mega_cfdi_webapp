package org.megapractical.invoicing.webapp.exposition.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.regeneratepdf.PdfType;
import org.megapractical.invoicing.webapp.report.ReportManager;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * Use this service class only in development environment
 * 
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class PdfBulkServiceImpl implements PdfBulkService {

	private final AppSettings appSettings;
	private final PdfFromXmlBuilderService pdfFromXmlBuilderService;
	private final PdfFromXmlUpdaterService pdfFromXmlUpdaterService;
	private final ReportManager reportManager;
	
	private int totalPdfs = 0;
	private int totalGeneratedPdfs = 0;
	private int totalError = 0;
	
	@Override
	//@Scheduled(fixedRate = 1, timeUnit = TimeUnit.DAYS)
	public void pdfFromXmlBulkGeneration() {
		if (appSettings.isDev()) {
			val basePath = "PATH/TO/FOLDER";
			UPrint.logWithLine(UProperties.env(), "[INFO] PDF BULK SERVICE > STARING BULK GENERATION FROM: " + basePath);
			
			val pdfType = PdfType.CFDI;
			val folders = Arrays.asList("<FOLDER_NAME>");
			folders.stream().forEach(folder -> processXmlFiles(basePath, folder, pdfType)); 
			
			UPrint.logWithLine(UProperties.env(), "[INFO] PDF BULK SERVICE > PROCESS COMPLETED");
			UPrint.logWithLine(UProperties.env(), "[INFO] PDF BULK SERVICE > TOTAL PDF EVALUATED: " + totalPdfs);
			UPrint.logWithLine(UProperties.env(), "[INFO] PDF BULK SERVICE > TOTAL PDF GENERATED: " + totalGeneratedPdfs);
			UPrint.logWithLine(UProperties.env(), "[INFO] PDF BULK SERVICE > TOTAL ERRORS: " + totalError);
		}
	}
	
	private void processXmlFiles(String rootDirectory, String folder, PdfType pdfType) {
		try {
			val rootPath = Paths.get(rootDirectory);
	        Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
	            @Override
	            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
	                if (dir.endsWith(folder)) {
	                    processStampDirectory(dir, pdfType);
	                }
	                return FileVisitResult.CONTINUE;
	            }
	        });
		} catch (IOException e) {
			Logger.getLogger(PdfBulkServiceImpl.class.getName()).log(Level.SEVERE, (String) null, e);
		}
    }

    private void processStampDirectory(Path stampDir, PdfType pdfType) {
    	try (val paths = Files.walk(stampDir)) {
	        paths.filter(Files::isRegularFile)
	             .filter(path -> path.toString().toLowerCase().endsWith(".xml"))
	             .forEach(path -> {
	            	 if (PdfType.CFDI.equals(pdfType)) {
	            		 generateCfdiPdf(path);
	            	 } else if (PdfType.PAYROLL.equals(pdfType)) {
	            		 generatePayrollPdf(path);
	            	 }
	             });
	        
	        if (shouldCompressStampDirectory(stampDir)) {
	            compressStampDirectory(stampDir);
	        }
	    } catch (IOException e) {
	    	UPrint.logWithLine(UProperties.env(), "[ERROR] PDF BULK SERVICE > ERROR PROCESSING THE STAMP DIRECTORY: " + e.getMessage());
	    }
    }

    private void generateCfdiPdf(Path xmlPath) {
    	val xmlAbsolutePath = xmlPath.toAbsolutePath().toString();
        UPrint.logWithLine(UProperties.env(), "[INFO] CFDI PDF BULK SERVICE > PROCESSING DIRECTORY: " + xmlAbsolutePath);
        val cfdiVoucher = pdfFromXmlBuilderService.buildCfdi(xmlAbsolutePath);
        if (cfdiVoucher != null) {
        	val cfdi = cfdiVoucher.getCfdi();
        	// Update data
        	pdfFromXmlUpdaterService.updateData(cfdi);
        	
        	val pdfPath = xmlAbsolutePath.replace(".xml", ".pdf"); 
        	val pdfGenerated = reportManager.cfdiV4GeneratePdf(cfdi, pdfPath);
        	if (pdfGenerated) {
        		UPrint.logWithLine(UProperties.env(), "[INFO] CFDI PDF BULK SERVICE > PDF SUCCESSFULLY GENERATING IN THE PATH: " + pdfPath);
        		totalGeneratedPdfs ++;
        	} else {
        		UPrint.logWithLine(UProperties.env(), "[ERROR] CFDI PDF BULK SERVICE > PDF COULD NOT BE GENERATED: " + pdfPath);
        		totalError ++;
        	}
        	totalPdfs ++;
        }
    }
    
    private void generatePayrollPdf(Path xmlPath) {
    	val xmlAbsolutePath = xmlPath.toAbsolutePath().toString();
        UPrint.logWithLine(UProperties.env(), "[INFO] PAYROLL PDF BULK SERVICE > PROCESSING DIRECTORY: " + xmlAbsolutePath);
        val payrollVoucher = pdfFromXmlBuilderService.buildPayroll(xmlAbsolutePath);
        if (payrollVoucher != null) {
        	// Update data
        	pdfFromXmlUpdaterService.updateData(payrollVoucher.getPayroll());
        	payrollVoucher.setCfdi(payrollVoucher.getPayroll().getApiCfdi());
        	
        	val pdfPath = xmlAbsolutePath.replace(".xml", ".pdf"); 
        	val pdfGenerated = reportManager.generatePDFPayroll(payrollVoucher, pdfPath);
        	if (pdfGenerated) {
        		UPrint.logWithLine(UProperties.env(), "[INFO] PAYROLL PDF BULK SERVICE > PDF SUCCESSFULLY GENERATING IN THE PATH: " + pdfPath);
        		totalGeneratedPdfs ++;
        	} else {
        		UPrint.logWithLine(UProperties.env(), "[ERROR] PAYROLL PDF BULK SERVICE > PDF COULD NOT BE GENERATED: " + pdfPath);
        		totalError ++;
        	}
        	totalPdfs ++;
        }
    }
    
    private static boolean shouldCompressStampDirectory(Path stampDir) throws IOException {
        try (val subdirs = Files.list(stampDir).filter(Files::isDirectory)) {
            return subdirs.anyMatch(subdir -> {
                try (val files = Files.list(subdir)) {
                    return files.findAny().isPresent();
                } catch (IOException e) {
                    UPrint.logWithLine(UProperties.env(), "[ERROR] PDF BULK SERVICE > ERROR VERIFYING SUBDIRECTORY CONTENT: " + e.getMessage());
                    return false;
                }
            });
        }
    }
    
    private static void compressStampDirectory(Path stampDir) throws IOException {
        val zipFilePath = stampDir.resolveSibling("STAMP.zip");
        try (val fos = new FileOutputStream(zipFilePath.toFile());
             val zos = new ZipOutputStream(fos)) {
            Files.walkFileTree(stampDir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (!Files.isDirectory(file)) {
                        // Añadir el archivo al archivo ZIP
                        try (val fis = new FileInputStream(file.toFile())) {
                            val zipEntry = new ZipEntry(stampDir.relativize(file).toString());
                            zos.putNextEntry(zipEntry);
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            while ((bytesRead = fis.read(buffer)) != -1) {
                                zos.write(buffer, 0, bytesRead);
                            }
                            zos.closeEntry();
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }

}