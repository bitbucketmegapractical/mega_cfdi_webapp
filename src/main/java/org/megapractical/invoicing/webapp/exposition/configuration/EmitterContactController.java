package org.megapractical.invoicing.webapp.exposition.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.ContactoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.IContactJpaRepository;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.service.DataStoreService;
import org.megapractical.invoicing.webapp.json.JContact;
import org.megapractical.invoicing.webapp.json.JContact.Contact;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.log.AppLog;
import org.megapractical.invoicing.webapp.log.EventTypeCode;
import org.megapractical.invoicing.webapp.log.OperationCode;
import org.megapractical.invoicing.webapp.log.TimelineLog;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.notification.Toastr.ToastrType;
import org.megapractical.invoicing.webapp.persistence.interfaces.IPersistenceDAO;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.ui.HtmlHelper;
import org.megapractical.invoicing.webapp.ui.Paginator;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.megapractical.invoicing.webapp.wrapper.WContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import lombok.val;

@Controller
@Scope("session")
public class EmitterContactController {

	@Autowired
	AppLog appLog;
	
	@Autowired
	TimelineLog timelineLog;
	
	@Autowired
	AppSettings appSettings;
	
	@Autowired
	UserTools userTools;
	
	@Autowired
	IPersistenceDAO persistenceDAO;
	
	@Autowired
	SessionController sessionController;
	
	@Autowired
	DataStoreService dataStore;
	
	@Resource
	IContactJpaRepository iContactJpaRepository;
	
	private static final String SORT_PROPERTY = "nombre";
	private static final Integer INITIAL_PAGE = 0;
	
	private String configAction;
	private String unexpectedError;
	
	@ModelAttribute("requiredMessage")
	public String requiredMessage() throws IOException{
		return UProperties.getMessage("ERR1001", userTools.getLocale());
	}
	
	@ModelAttribute("processingRequest")
	public String processingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.processing", userTools.getLocale());
	}
	
	@ModelAttribute("loadingRequest")
	public String loadingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.loading", userTools.getLocale());
	}
	
	@ModelAttribute("updatingRequest")
	public String updatingRequest() throws IOException{
		return UProperties.getMessage("mega.cfdi.updating", userTools.getLocale());
	}
	
	// Valores no válidos
	@ModelAttribute("invalidValue")
	public String invalidValue() throws IOException{
		return UProperties.getMessage("ERR1004", userTools.getLocale());
	}
	
	@RequestMapping(value = "/emitterContact", method = RequestMethod.GET)
	public String emitterContact(Model model, Integer page, String searchString) {
		try {
			
			this.unexpectedError = UProperties.genericUnexpectedError(userTools.getLocale());
			
			model.addAttribute("page", INITIAL_PAGE);
			
			HtmlHelper.functionalitySelected("EmitterConfig", "emitter-config-contact");
			appLog.logOperation(OperationCode.FUNCIONALIDAD_ACCEDIDA_ID42);
			
			return "/config/EmitterContact";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/denied/ResourceDenied";
	}
	
	@RequestMapping(value = "/emitterContact", method = RequestMethod.POST)
	public @ResponseBody String emitterContact(HttpServletRequest request) {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JContact contact = new JContact();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String pageStr = UBase64.base64Decode(request.getParameter("page"));
			Integer page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
			page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
			
			String searchName = UBase64.base64Decode(request.getParameter("searchName"));
			if(UValidator.isNullOrEmpty(searchName)){
				searchName = "";
			}
			
			Sort sort = Sort.by(Sort.Order.asc(SORT_PROPERTY));
			PageRequest pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Page<ContactoEntity> contactPage = null;
			Paginator<ContactoEntity> paginator = null;
			
			contactPage = iContactJpaRepository.findByContribuyenteAndNombreContainingIgnoreCaseAndEliminadoFalse(taxpayer, searchName, pageRequest);
			val pagination = JPagination.buildPaginaton(contactPage);
			contact.setPagination(pagination);
			
			for (ContactoEntity item : contactPage) {
				Contact contactObj = new Contact(); 
				contactObj.setId(UBase64.base64Encode(item.getId().toString()));
				contactObj.setName(UBase64.base64Encode(item.getNombre()));
				contactObj.setEmail(UBase64.base64Encode(item.getCorreoElectronico()));
				contact.getContacts().add(contactObj);
			}
			
			String searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
			if(!UValidator.isNullOrEmpty(searchName)){
				notification.setSearch(Boolean.TRUE);
				searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
			}
			
			if(contact.getContacts().isEmpty()){
				notification.setSearchResultMessage(searchResultEmpty);
				notification.setPanelMessage(Boolean.TRUE);
				notification.setDismiss(Boolean.FALSE);
				notification.setCssStyleClass(CssStyleClass.INFO.value());
			}
			
			response.setSuccess(Boolean.TRUE);
			response.setNotification(notification);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		sessionController.sessionUpdate();
		
		contact.setResponse(response);
		jsonInString = gson.toJson(contact);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterContact", params={"contactDataLoad"})
	public @ResponseBody String emitterThirdPartiesDataLoad(HttpServletRequest request) throws IOException{
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JContact contact = new JContact();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
			
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			ContactoEntity contactEntity = iContactJpaRepository.findByIdAndEliminadoFalse(id);
			if(!UValidator.isNullOrEmpty(contactEntity)){
				Contact contactObj = new Contact();
				contactObj.setName(UBase64.base64Encode(contactEntity.getNombre()));
				contactObj.setEmail(UBase64.base64Encode(contactEntity.getCorreoElectronico()));

				contact.setContact(contactObj);
				
				response.setSuccess(Boolean.TRUE);				
			}else{
				notification.setMessage(UProperties.getMessage("ERR0105", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0105", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}
		
		sessionController.sessionUpdate();
		
		contact.setResponse(response);
		jsonInString = gson.toJson(contact);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterContact", params={"contactRemove"})
    public @ResponseBody String emitterThirdPartiesRemove(HttpServletRequest request) throws IOException {
		JNotification notification = new JNotification();
		JResponse response = new JResponse();
		JContact contact = new JContact();
		
		String jsonInString = null;
		Gson gson = new Gson();
		
		try {
        	
			String idString = UBase64.base64Decode(request.getParameter("id"));
			Long id = UValue.longValue(idString);
			
			ContactoEntity contactEntity = iContactJpaRepository.findByIdAndEliminadoFalse(id);
			if(!UValidator.isNullOrEmpty(contactEntity)){
				contactEntity.setEliminado(Boolean.TRUE);
				persistenceDAO.update(contactEntity);
				
				// Bitacora :: Registro de accion en bitacora
				appLog.logOperation(OperationCode.ELIMINAR_CONTACTO_ID58);
				
				// Timeline :: Registro de accion
				timelineLog.timelineLog(EventTypeCode.ELIMINAR_CONTACTO_ID33);
				
				response.setSuccess(Boolean.TRUE);
			}else{
				notification.setMessage(UProperties.getMessage("ERR0108", userTools.getLocale()));
				notification.setPageMessage(Boolean.TRUE);
				notification.setCssStyleClass(CssStyleClass.ERROR.value());
				
				response.setNotification(notification);
				response.setError(Boolean.TRUE);
			}			
        	
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setMessage(UProperties.getMessage("ERR0108", userTools.getLocale()));
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			
			response.setNotification(notification);
			response.setError(Boolean.TRUE);
		}		
		
		sessionController.sessionUpdate();
		
		contact.setResponse(response);
		jsonInString = gson.toJson(contact);
		return jsonInString;
	}
	
	@RequestMapping(value="/emitterContact", params={"contactAddEdit"})
    public @ResponseBody String emitterThirdPartiesAddEdit(HttpServletRequest request) throws IOException {
		JContact contactJson = new JContact();
		Contact contact = new Contact(); 
		
		JResponse response = new JResponse();
		JNotification notification = new JNotification();
		
		List<JError> errors = new ArrayList<>();
		JError error = new JError();
		
		Gson gson = new Gson();
		String jsonInString = null;

		try {
			
			this.configAction = UBase64.base64Decode(request.getParameter("operation"));
			this.unexpectedError = this.configAction.equals("config") ? UProperties.getMessage("ERR0106", userTools.getLocale()) : UProperties.getMessage("ERR0107", userTools.getLocale());
			
			String rfcActive = userTools.getContribuyenteActive().getRfcActivo();
			ContribuyenteEntity taxpayer = userTools.getContribuyenteActive(rfcActive);
			
			Boolean contactDeletedFinded = false;
			Boolean errorMarked = false;
			
			ContactoEntity contactEntity = new ContactoEntity();
			
			if(request.getParameter("objContact") != null){
				String objThirdParties = request.getParameter("objContact");
				contact = gson.fromJson(objThirdParties, Contact.class);
				
				//##### Id
				Long contactId = null;
				String index = UBase64.base64Decode(contact.getId());
				if(!UValidator.isNullOrEmpty(index)){
					contactId = UValue.longValue(index);
				}
				
				//##### Nombre
				String name = UBase64.base64Decode(contact.getName());
				error = new JError();
				
				if(UValidator.isNullOrEmpty(name)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}
				if(errorMarked){
					error.setField("name");
					errors.add(error);
				}else{
					name = name.toUpperCase();
				}
				
				//##### Correo electronico
				String email = UBase64.base64Decode(contact.getEmail());
				error = new JError();
				errorMarked = false;
				
				if(UValidator.isNullOrEmpty(email)){
					error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
					errorMarked = true;
				}else{
					if(this.configAction.equals("config")){
						ContactoEntity contactObj = iContactJpaRepository.findByContribuyenteAndCorreoElectronicoIgnoreCaseAndEliminadoFalse(taxpayer, email);
						if(!UValidator.isNullOrEmpty(contactObj)){
							error.setMessage(UProperties.getMessage("ERR0109", userTools.getLocale()));
							errorMarked = true;
						}else{
							contactObj = iContactJpaRepository.findByContribuyenteAndCorreoElectronicoIgnoreCaseAndEliminadoTrue(taxpayer, email);
							if(!UValidator.isNullOrEmpty(contactObj)){
								contactEntity = contactObj;
								contactDeletedFinded = true;
							}else{
								contactEntity = new ContactoEntity();
							}							
						}
					}else if(this.configAction.equals("edit")){
						contactEntity = iContactJpaRepository.findByIdAndEliminadoFalse(contactId);
						String contactEmail = contactEntity.getCorreoElectronico();
						if(!email.equalsIgnoreCase(contactEmail)){
							ContactoEntity contactObj = iContactJpaRepository.findByContribuyenteAndCorreoElectronicoIgnoreCaseAndEliminadoFalse(taxpayer, email);
							if(!UValidator.isNullOrEmpty(contactObj)){
								error.setMessage(UProperties.getMessage("ERR0109", userTools.getLocale()));
								errorMarked = true;
							}
						}
					}
				}
				if(errorMarked){
					error.setField("email");
					errors.add(error);
				}
				
				if(!errors.isEmpty()){
					response.setError(Boolean.TRUE);
					response.setErrors(errors);
				}else{
					contactEntity.setContribuyente(taxpayer);
					contactEntity.setNombre(name);
					contactEntity.setCorreoElectronico(email);
					contactEntity.setEliminado(Boolean.FALSE);
					
					WContact wContact = new WContact();
					wContact.setContact(contactEntity);
					wContact.setAction(this.configAction);
					wContact.setContactDeletedFinded(contactDeletedFinded);
					
					contactEntity = null;
					contactEntity = dataStore.contactStore(wContact);
					
					if(!UValidator.isNullOrEmpty(contactEntity)){
						OperationCode operationCode = null;
						EventTypeCode eventTypeCode = null;
						String toastrTitle = null;
						String toastrMessage = null;
						
						if(this.configAction.equals("edit")){
							operationCode = OperationCode.ACTUALIZACION_CONTACTO_ID57;
							eventTypeCode = EventTypeCode.ACTUALIZACION_CONTACTO_ID32;
							toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.contact.edit.title", userTools.getLocale());
							toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.contact.edit.message", userTools.getLocale());
						}else{						
							operationCode = OperationCode.REGISTRO_CONTACTO_ID56;
							eventTypeCode = EventTypeCode.REGISTRO_CONTACTO_ID31;
							toastrTitle = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.contact.config.title", userTools.getLocale());
							toastrMessage = UProperties.getMessage("mega.cfdi.notification.toastr.taxpayer.contact.config.message", userTools.getLocale());
						}
						contact.setId(UBase64.base64Encode(contactEntity.getId().toString()));
						contactJson.setContact(contact);
						
						// Bitacora :: Registro de accion en bitacora
						appLog.logOperation(operationCode);
						
						// Timeline :: Registro de accion
						timelineLog.timelineLog(eventTypeCode);
						
						// Notificacion Toastr
						Toastr toastr = new Toastr();
						toastr.setTitle(toastrTitle);
						toastr.setMessage(toastrMessage);
						toastr.setType(ToastrType.SUCCESS.value());
						
						notification.setToastrNotification(Boolean.TRUE);
						notification.setToastr(toastr);
						
						response.setSuccess(Boolean.TRUE);
						response.setNotification(notification);
					}else{
						notification.setPageMessage(Boolean.TRUE);
						notification.setCssStyleClass(CssStyleClass.ERROR.value());
						notification.setMessage(this.unexpectedError);
						response.setNotification(notification);
						response.setError(Boolean.TRUE);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			notification.setPageMessage(Boolean.TRUE);
			notification.setCssStyleClass(CssStyleClass.ERROR.value());
			notification.setMessage(this.unexpectedError);
			response.setNotification(notification);
			response.setUnexpectedError(Boolean.TRUE);
		}
		
		contactJson.setResponse(response);
		jsonInString = gson.toJson(contactJson);
		
		return jsonInString;
	}
}
