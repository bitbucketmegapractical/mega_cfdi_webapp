package org.megapractical.invoicing.webapp.exposition.scheduled;

import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.webapp.exposition.service.api.ApiTokenService;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class ApiTokenSchedule {
	
	private final ApiTokenService apiTokenService;
	
	//@Scheduled(fixedRate = 1800000) // 30m => 30 * 60 * 1000 
	public void loadFiles() {
		UPrint.logWithLine(UProperties.env(), "[INFO] API TOKEN SCHEDULE > UPDATING TOKEN");
		apiTokenService.updateToken();
		UPrint.logWithLine(UProperties.env(), "[INFO] API TOKEN SCHEDULE > UPDATE TOKEN COMPLETE");
	}
	
}