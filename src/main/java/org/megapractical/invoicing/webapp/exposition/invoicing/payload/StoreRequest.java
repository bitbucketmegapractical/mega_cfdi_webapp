package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class StoreRequest {
	private ContribuyenteEntity taxpayer;
	private String emitterRfc;
	private String receiverRfc;
	private String receiverName;
	private String receiverEmail;
	private ApiCfdi apiCfdi;
	private String complementCode;
	private PreferenciasEntity preferences;
}