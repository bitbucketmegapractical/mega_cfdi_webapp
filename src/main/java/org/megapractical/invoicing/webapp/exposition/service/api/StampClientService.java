package org.megapractical.invoicing.webapp.exposition.service.api;

import javax.annotation.PostConstruct;

import org.megapractical.invoicing.api.service.CfdiXmlService;
import org.megapractical.invoicing.api.smarterweb.SWStampClient;
import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.xml.XmlHelper;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.val;

@Service
@RequiredArgsConstructor
public class StampClientService {
	
	private final AppSettings appSettings;
	private final ApiTokenService apiTokenService;
	private final RestTemplate restTemplate;
	
	private String stampServiceUrl;
	
	@PostConstruct
	public void init() {
		this.stampServiceUrl = appSettings.getPropertyValue("mega.cfdi.stamp.service.url");
	}
	
	public StampResponse stamp(StampProperties stampProperties) {
		val xml = CfdiXmlService.generateXml(stampProperties.getVoucher());
		UPrint.logWithLine(UProperties.env(), "[INFO] STAMP CLIENT SERVICE > XML TO STAMP");
		UPrint.log(UProperties.env(), UValue.byteToString(xml));
		
		val voucherRequest = SWStampClient.voucherRequest(stampProperties, xml);
		val sealedXml = XmlHelper.sealedXml(voucherRequest);
		
		return stampRequest(sealedXml);
	}
	
	private StampResponse stampRequest(String sealedXml) {
		try {
	        // Create the headers for the request
	        val headers = populateHeaders();
	        // Create the HTTP entity with the request body and headers
	        val entity = new HttpEntity<String>(sealedXml, headers);
	        // Make the request to the API
	        val response = restTemplate.postForEntity(stampServiceUrl, entity, StampResponse.class);
	        UPrint.logWithLine(UProperties.env(), "[INFO] STAMP CLIENT SERVICE > RESPONSE " + response.getBody());
	        // Return the response body
	        return response.getBody();
		} catch (RestClientException e) {
			UPrint.logWithLine(UProperties.env(), "[ERROR] STAMP CLIENT SERVICE > " + e.getMessage());
			e.printStackTrace();
			return StampResponse.error(e.getMessage(), e.getMessage(), sealedXml);
		}
    }
	
	private HttpHeaders populateHeaders() {
		val headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        apiTokenService.updateToken();
        headers.setBearerAuth(apiTokenService.getToken());
        return headers;
	}
	
}