package org.megapractical.invoicing.webapp.exposition.invoicing.validator.residency;

import java.util.List;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.dal.bean.jpa.PaisEntity;
import org.megapractical.invoicing.dal.data.repository.ICountryJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverResidencyResponse;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.ReceiverResidencyResponse.ResidencyResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class ResidencyValidatorImpl implements ResidencyValidator {

	@Autowired
	private UserTools userTools;
	
	@Autowired
	private ICountryJpaRepository iCountryJpaRepository;

	@Override
	public ReceiverResidencyResponse validate(String resident, String residency) {
		PaisEntity residencyEntity = null;
		var residencyError = false;

		if (UValidator.isNullOrEmpty(resident)) {
			residencyError = true;
		} else if (resident.equalsIgnoreCase("extranjero")) {
			residencyEntity = getForeCountry(residency);
			if (residencyEntity == null) {
				residencyError = true;
			}
		}

		return new ReceiverResidencyResponse(resident, residencyEntity, residencyError);
	}

	@Override
	public ReceiverResidencyResponse validate(String resident, String residency, String field, List<JError> errors) {
		PaisEntity residencyEntity = null;
		JError error = null;

		if (UValidator.isNullOrEmpty(resident)) {
			error = JError.required("resident", userTools.getLocale());
		} else if (resident.equalsIgnoreCase("extranjero")) {
			val residencyResponse = getForeignerCountry(residency);
			residencyEntity = residencyResponse.getResidency();
			error = residencyResponse.getError();
		}

		var residencyError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			residencyError = true;
		}

		return new ReceiverResidencyResponse(resident, residencyEntity, residencyError);
	}

	private final PaisEntity getForeCountry(String residency) {
		if (residency.equals("MEX (México)")) {
			return null;
		} else {
			if (UValidator.isNullOrEmpty(residency)) {
				return null;
			} else {
				return getCountry(residency);
			}
		}
	}
	
	private final ResidencyResponse getForeignerCountry(String residency) {
		PaisEntity residencyEntity = null;
		JError error = null;
		val field = "residency";

		if (residency.equals("MEX (México)")) {
			error = JError.error(field, Validator.ErrorSource.fromValue("CFDI33135").value(), userTools.getLocale());
		} else {
			if (UValidator.isNullOrEmpty(residency)) {
				error = JError.required(field, userTools.getLocale());
			} else {
				residencyEntity = getCountry(residency);
				if (residencyEntity == null) {
					val values = new String[] { "CFDI33133", "ResidenciaFiscal", "c_Pais" };
					val errorMessage = UInvalidMessage.satInvalidCatalogMessage("ERR1002", userTools.getLocale(),
							values);
					error = JError.error(field, errorMessage);
				}
			}
		}

		return new ResidencyResponse(residencyEntity, error);
	}
	
	private PaisEntity getCountry(String residency) {
		// Validar que sea un valor de catalogo
		val residencyCatalog = UCatalog.getCatalog(residency);
		val residencyCode = residencyCatalog[0];
		val residencyValue = residencyCatalog[1];

		val residencyEntity = iCountryJpaRepository.findByCodigoAndEliminadoFalse(residencyCode);
		val residencyEntityByValue = iCountryJpaRepository.findByValorAndEliminadoFalse(residencyValue);
		if (residencyEntity == null || residencyEntityByValue == null) {
			return null;
		}
		return residencyEntity;
	}

}