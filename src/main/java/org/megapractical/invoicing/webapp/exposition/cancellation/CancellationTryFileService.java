package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UDateTime;
import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiFileDetail;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoCancelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.webapp.config.AppSettings;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.payload.UploadFilePath;
import org.megapractical.invoicing.webapp.exposition.service.nomenclator.FileStatusService;
import org.megapractical.invoicing.webapp.exposition.service.nomenclator.FileTypeService;
import org.megapractical.invoicing.webapp.json.JCfdiFile;
import org.megapractical.invoicing.webapp.json.JCfdiFile.CfdiFile;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JNotification;
import org.megapractical.invoicing.webapp.json.JNotification.CssStyleClass;
import org.megapractical.invoicing.webapp.json.JPagination;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.FilePathUtils;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@Scope("session")
@RequiredArgsConstructor
class CancellationTryFileService {
	
	private final AppSettings appSettings;
	private final UserTools userTools;
	private final CancellationFileService cancellationFileService;
	private final FileTypeService fileTypeService;
	private final FileStatusService fileStatusService;
	private final CancellationFileAsyncService cancellationFileAsyncService;
	
	private static final Integer INITIAL_PAGE = 0;
	private static final String SORT_PROPERTY = "id";
	
	public JCfdiFile cancellationTry(ContribuyenteEntity taxpayer, String pageStr, String searchBy) {
		val cfdiFile = loadGeneratedFiles(taxpayer, pageStr, searchBy);
		cfdiFile.setProcessingFiles(getProcessingFiles(taxpayer));
		cfdiFile.setQueuedFiles(getQueuedFiles(taxpayer));
		return cfdiFile;
	}
	
	public JCfdiFile loadGeneratedFiles(ContribuyenteEntity taxpayer, String pageStr, String searchBy) {
		var page = !UValidator.isNullOrEmpty(pageStr) ? Integer.valueOf(pageStr) : null;
		page = (page != null && page != 0) ? page - 1 : INITIAL_PAGE;
		
		val sort = Sort.by(Sort.Order.desc(SORT_PROPERTY));
		val pageRequest = PageRequest.of(page, appSettings.getPaginationSize(), sort);

		val cancellationFilePage = cancellationFileService.getFiles(taxpayer, searchBy, FileStatus.GENERATED,
				CancellationConstants.FILE_TXT_CODE, pageRequest);
		val pagination = JPagination.buildPaginaton(cancellationFilePage);
		val generatedFiles = getGeneratedFiles(cancellationFilePage);
		
		val notification = new JNotification();
		var searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty", userTools.getLocale());
		if (!UValidator.isNullOrEmpty(searchBy)) {
			notification.setSearch(Boolean.TRUE);
			searchResultEmpty = UProperties.getMessage("mega.cfdi.information.empty.by.search", userTools.getLocale());
		}

		if (generatedFiles.isEmpty()) {
			notification.setSearchResultMessage(searchResultEmpty);
			notification.setPanelMessage(Boolean.TRUE);
			notification.setDismiss(Boolean.FALSE);
			notification.setCssStyleClass(CssStyleClass.INFO.value());
		}
		
		val response = new JResponse();
		response.setSuccess(Boolean.TRUE);
		response.setNotification(notification);
		
		return JCfdiFile
				.builder()
				.pagination(pagination)
				.generatedFiles(generatedFiles)
				.response(response)
				.build();
	}
	
	public JCfdiFile uploadFile(ContribuyenteEntity taxpayer, ContribuyenteCertificadoEntity certificate, MultipartFile multipartFile, boolean sendMail) {
		val response = new JResponse();
		val errors = new ArrayList<JError>();
		
		val fileDetails = loadFileDetails(taxpayer, multipartFile, errors);
		if (!fileDetails.isHasError()) {
			val cancellationRepository = appSettings.getPropertyValue(CancellationConstants.CANCELLATION_PATH);
			val uploadFilePath = FilePathUtils.uploadPath(taxpayer.getRfcActivo(), fileDetails, cancellationRepository);
			try {
				UFile.writeFile(UFile.fileToByte(fileDetails.getFile()), uploadFilePath.getUploadPath());
				val cancellationFile = populateCancellationFile(taxpayer, fileDetails, uploadFilePath.getDbPath(), sendMail);
				if (!isAnotherFileProcessing(taxpayer)) {
					processFile(taxpayer, certificate, cancellationFile, uploadFilePath, fileDetails);
				} else {
					cancellationFileService.save(cancellationFile);
				}
				successResponse(response);
			} catch (IOException e) {
				errorResponse(response, errors);
			}
		} else {
			errorResponse(response, errors);
		}
		return JCfdiFile.builder().response(response).build();
	}

	public byte[] downloadFile(ContribuyenteEntity taxpayer, 
							   String fileName, 
							   String fieType,
							   HttpServletResponse response) {
		val cancellationPath = appSettings.getPropertyValue(CancellationConstants.CANCELLATION_PATH);
		val cancellationFile = cancellationFileService.findByTaxpayerAndName(taxpayer, fileName);
		if (AssertUtils.nonNull(cancellationFile)) {
			var absolutePath = cancellationPath + File.separator;
			if (fieType.equalsIgnoreCase("acuses")) {
				absolutePath = absolutePath + cancellationFile.getRutaAcuses();
			} else if (fieType.equalsIgnoreCase("error")) {
				absolutePath = absolutePath + cancellationFile.getRutaError();
			}

			val file = new File(absolutePath);
			byte[] fileByte;
			try {
				fileByte = UFile.fileToByte(file);
				
				response.setContentType("application/xml");
				response.setContentLength(fileByte.length);
				response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

				return fileByte;
			} catch (IOException e) {
				return new byte[0];
			}
		}
		return new byte[0];
	}
	
	public boolean exists(Long id) {
		return cancellationFileService.findOne(id, FileStatus.QUEUED) != null;
	}
	
	public void deleteQueuedFile(Long id) {
		cancellationFileService.delete(id);
	}
	
	private List<CfdiFile> getGeneratedFiles(Page<ArchivoCancelacionEntity> cancellationFilePage) {
		return cancellationFilePage.stream()
							  .map(CancellationFileConverter::convert)
							  .filter(Objects::nonNull)
							  .collect(Collectors.toList());
	}
	
	private List<CfdiFile> getProcessingFiles(ContribuyenteEntity taxpayer) {
		val processingFiles = new ArrayList<CfdiFile>();
		
		val files = cancellationFileService.getFiles(taxpayer, FileStatus.PROCESSING, CancellationConstants.FILE_TXT_CODE);
		files.forEach(item -> {
			val cfdiFile = new CfdiFile();
			cfdiFile.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
			cfdiFile.setName(UBase64.base64Encode(item.getNombre()));
			processingFiles.add(cfdiFile);
		});
		
		return processingFiles;
	}
	
	private List<CfdiFile> getQueuedFiles(ContribuyenteEntity taxpayer) {
		val queuedFiles = new ArrayList<CfdiFile>();

		val files = cancellationFileService.getFiles(taxpayer, FileStatus.QUEUED, CancellationConstants.FILE_TXT_CODE);
		files.forEach(item -> {
			val cfdiFile = new CfdiFile();
			cfdiFile.setId(UBase64.base64Encode(UValue.longString(item.getId()).toString()));
			cfdiFile.setCfdiType(UBase64.base64Encode("Cancelaciones"));
			cfdiFile.setName(UBase64.base64Encode(item.getNombre()));
			cfdiFile.setUploadDate(UBase64.base64Encode(UDateTime.format(item.getFechaCarga())));
			queuedFiles.add(cfdiFile);
		});

		return queuedFiles;
	}
	
	private ApiFileDetail loadFileDetails(ContribuyenteEntity taxpayer, 
										  MultipartFile multipartFile,
										  final List<JError> errors) {
		ApiFileDetail fileDetails = null;
		val error = new JError();
		var errorMarked = false;

		if (multipartFile.isEmpty()) {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		} else {
			fileDetails = UFile.fileDetails(multipartFile);
			
			val fileName = fileDetails.getFileName();
			if (AssertUtils.hasValue(fileName)) {
				val cancellationFile = cancellationFileService.findByTaxpayerAndName(taxpayer, fileName);
				if (AssertUtils.nonNull(cancellationFile)) {
					error.setMessage(UProperties.getMessage("ERR0189", userTools.getLocale()));
					errorMarked = true;
				}
			} else {
				error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
				errorMarked = true;
			}
			
			val fileExt = fileDetails.getFileExt();
			if (fileExt == null || !fileExt.equalsIgnoreCase("txt")) {
				error.setMessage(UProperties.getMessage("ERR1005", userTools.getLocale()));
				errorMarked = true;
			}
		}

		if (errorMarked) {
			error.setField("cancellation-file");
			errors.add(error);
			return ApiFileDetail.withError();
		}
		return fileDetails;
	}
	
	private ArchivoCancelacionEntity populateCancellationFile(ContribuyenteEntity taxpayer, ApiFileDetail fileDetails, String path, boolean sendMail) {
		val fileName = fileDetails.getFileName();
		val fileExt = fileDetails.getFileExt();
		val dbPath = path + File.separator + fileName + "." + fileExt;
		val fileTypeCode = fileExt.equalsIgnoreCase("txt") ? CancellationConstants.FILE_TXT_CODE : null;
		val fileType = fileTypeService.findByCode(fileTypeCode);
		val fileStatus = fileStatusService.findByCode(FileStatus.QUEUED);
		
		val cancellationFile = new ArchivoCancelacionEntity();
		cancellationFile.setContribuyente(taxpayer); 
		cancellationFile.setTipoArchivo(fileType);
		cancellationFile.setEstadoArchivo(fileStatus);
		cancellationFile.setNombre(fileName);
		cancellationFile.setRuta(dbPath);
		cancellationFile.setFechaCarga(LocalDateTime.now());
		cancellationFile.setEliminado(Boolean.FALSE);
		
		return cancellationFile;
	}
	
	private boolean isAnotherFileProcessing(ContribuyenteEntity taxpayer) {
		val processingFiles = cancellationFileService.findByTaxpayerAndStatus(taxpayer, FileStatus.PROCESSING);
		return !processingFiles.isEmpty();
	}
	
	public void processFile(ContribuyenteEntity taxpayer, 
							ContribuyenteCertificadoEntity certificate,
							ArchivoCancelacionEntity cancellationFile, 
							UploadFilePath uploadFilePath,
							ApiFileDetail fileDetails) {
		val acusesPath = CancellationHelper.createAcusesDirectory(cancellationFile, uploadFilePath);
		val cancellationFileData = CancellationFileData
									.builder()
									.taxpayer(taxpayer)
									.certificate(certificate)
									.cancellationFile(cancellationFile)
									.uploadFilePath(uploadFilePath)
									.fileDetail(fileDetails)
									.acusesPath(acusesPath)
									.build();
		// Async processing
		cancellationFileAsyncService.processFileAsync(cancellationFileData);
	}
	
	private void errorResponse(final JResponse response, final List<JError> errors) {
		val errorMessage = UProperties.getMessage("ERR0162", userTools.getLocale());
		val notification = JNotification.modalPanelMessageError(errorMessage);
		
		response.setNotification(notification);
		response.setError(Boolean.TRUE);
		response.setErrors(errors);
	}
	
	private void successResponse(final JResponse response) {
		// Notificacion Toastr
		val title = UProperties.getMessage("mega.cfdi.notification.toastr.cancellationComplement.file.upload.title", userTools.getLocale());
		val message = UProperties.getMessage("mega.cfdi.notification.toastr.file.uploaded.message", userTools.getLocale());
		val toastr = Toastr.success(title, message);
		val notification = JNotification.toastrNotification(toastr);
		
		response.setNotification(notification);
		response.setSuccess(Boolean.TRUE);
	}
	
}