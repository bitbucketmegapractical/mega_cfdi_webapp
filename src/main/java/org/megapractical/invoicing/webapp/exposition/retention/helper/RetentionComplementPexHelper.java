package org.megapractical.invoicing.webapp.exposition.retention.helper;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.dal.bean.jpa.RetencionPaisEntity;
import org.megapractical.invoicing.dal.data.repository.IRetentionCountryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionTaxpayerJpaRepository;
import org.megapractical.invoicing.webapp.exposition.retention.payload.RetentionComplementResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRetention.RetPex;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.val;
import lombok.var;

@Component
public class RetentionComplementPexHelper {

	private final UserTools userTools;
	private final IRetentionCountryJpaRepository iRetentionCountryJpaRepository;
	private final IRetentionTaxpayerJpaRepository iRetentionTaxpayerJpaRepository;

	public RetentionComplementPexHelper(UserTools userTools,
										IRetentionCountryJpaRepository iRetentionCountryJpaRepository,
										IRetentionTaxpayerJpaRepository iRetentionTaxpayerJpaRepository) {
		this.userTools = userTools;
		this.iRetentionCountryJpaRepository = iRetentionCountryJpaRepository;
		this.iRetentionTaxpayerJpaRepository = iRetentionTaxpayerJpaRepository;
	}

	public RetentionComplementResponse populate(RetentionData retentionData, String retentionKey,
			final ApiRetention apiRetention, final List<JError> errors) {
		RetPex retPex = retentionData.getRetPex();
		if (!UValidator.isNullOrEmpty(retPex)) {
			// Es beneficiario
			val beneficiaryResponse = populatePexBeneficiary(retPex, apiRetention, errors);
			val isBeneficiary = beneficiaryResponse.isBeneficiary();
			val errorMarked = beneficiaryResponse.isErrorMarked();

			if (!errorMarked) {
				if (isBeneficiary) {
					// Beneficiario :: RFC
					populatePexBeneficiaryRfc(retPex, apiRetention, errors);
					// Beneficiario :: Razón social
					populatePexBeneficiaryBusinessName(retPex, apiRetention, errors);
					// Beneficiario :: CURP
					populatePexBeneficiaryCurp(retPex, apiRetention, errors);
				} else {
					// No Beneficiario :: País residencia
					populatePexNoBeneficiaryCountry(retPex, apiRetention, errors);
				}

				// Concepto pago
				populatePexPaymentConcept(retPex, apiRetention, errors);
				// Descripción concepto
				populatePexConceptDescription(retPex, apiRetention, errors);

				return RetentionComplementResponse.ok();
			}
		} else if (retentionKey.equalsIgnoreCase("18")) {
			return RetentionComplementResponse.error();
		}
		return RetentionComplementResponse.empty();
	}
	
	private BeneficiaryResponse populatePexBeneficiary(RetPex retPex, final ApiRetention apiRetention,
			final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;

		boolean isBeneficiary = false;
		val pexBeneficiary = UBase64.base64Decode(retPex.getBeneficiary());
		if (!UValidator.isNullOrEmpty(pexBeneficiary)) {
			if (pexBeneficiary.equals("SI") || pexBeneficiary.equals("NO")) {
				isBeneficiary = pexBeneficiary.equalsIgnoreCase("si");
				apiRetention.setBeneficiary(isBeneficiary);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}

		if (errorMarked) {
			error.setField("pex-beneficiary");
			errors.add(error);
		}

		return new BeneficiaryResponse(isBeneficiary, errorMarked);
	}

	private void populatePexBeneficiaryRfc(RetPex retPex, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;

		val pexBeneficiaryRfc = UBase64.base64Decode(retPex.getBeneficiaryRfc());
		if (!UValidator.isNullOrEmpty(pexBeneficiaryRfc)) {
			if (pexBeneficiaryRfc.length() < RetentionHelper.RFC_MIN_LENGTH
					|| pexBeneficiaryRfc.length() > RetentionHelper.RFC_MAX_LENGTH
					|| !pexBeneficiaryRfc.matches(RetentionHelper.RFC_EXPRESSION)) {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			} else {
				apiRetention.setBeneficiaryRfc(pexBeneficiaryRfc);
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}

		if (errorMarked) {
			error.setField("pex-beneficiary-rfc");
			errors.add(error);
		}
	}

	private void populatePexBeneficiaryBusinessName(RetPex retPex, final ApiRetention apiRetention,
			final List<JError> errors) {
		val pexBeneficiaryBusinessName = UBase64.base64Decode(retPex.getBeneficiaryBusinessName());
		if (!UValidator.isNullOrEmpty(pexBeneficiaryBusinessName)) {
			apiRetention.setBeneficiaryBusinessName(pexBeneficiaryBusinessName);
		} else {
			val error = JError.required("pex-beneficiary-business-name", userTools.getLocale());
			errors.add(error);
		}
	}

	private void populatePexBeneficiaryCurp(RetPex retPex, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;

		val pexBeneficiaryCurp = UBase64.base64Decode(retPex.getBeneficiaryCurp());
		if (!UValidator.isNullOrEmpty(pexBeneficiaryCurp)) {
			if (pexBeneficiaryCurp.length() == 18 && pexBeneficiaryCurp.matches(RetentionHelper.CURP_EXPRESSION)) {
				apiRetention.setBeneficiaryCurp(pexBeneficiaryCurp);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}

		if (errorMarked) {
			error.setField("pex-beneficiary-curp");
			errors.add(error);
		}
	}

	private void populatePexNoBeneficiaryCountry(RetPex retPex, final ApiRetention apiRetention,
			final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;

		String pexNoBeneficiaryCountry = UBase64.base64Decode(retPex.getNoBeneficiaryCountry());
		if (!UValidator.isNullOrEmpty(pexNoBeneficiaryCountry)) {
			RetencionPaisEntity country = iRetentionCountryJpaRepository
					.findByCodigoAndEliminadoFalse(pexNoBeneficiaryCountry);
			if (!UValidator.isNullOrEmpty(country)) {
				apiRetention.setNoBeneficiaryCountry(pexNoBeneficiaryCountry);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}
		if (errorMarked) {
			error.setField("pex-no-beneficiary-country");
			errors.add(error);
		}
	}

	private void populatePexPaymentConcept(RetPex retPex, final ApiRetention apiRetention, final List<JError> errors) {
		val error = new JError();
		var errorMarked = false;

		val pexPaymentConcept = UBase64.base64Decode(retPex.getPaymentConcept());
		if (!UValidator.isNullOrEmpty(pexPaymentConcept)) {
			val retentionTaxpayer = iRetentionTaxpayerJpaRepository.findByCodigoAndEliminadoFalse(pexPaymentConcept);
			if (!UValidator.isNullOrEmpty(retentionTaxpayer)) {
				apiRetention.setPaymentConcept(pexPaymentConcept);
			} else {
				error.setMessage(UProperties.getMessage("ERR1004", userTools.getLocale()));
				errorMarked = true;
			}
		} else {
			error.setMessage(UProperties.getMessage("ERR1001", userTools.getLocale()));
			errorMarked = true;
		}

		if (errorMarked) {
			error.setField("pex-payment-concept");
			errors.add(error);
		}
	}

	private void populatePexConceptDescription(RetPex retPex, final ApiRetention apiRetention,
			final List<JError> errors) {
		val pexConceptDescription = UBase64.base64Decode(retPex.getConceptDescription());
		if (!UValidator.isNullOrEmpty(pexConceptDescription)) {
			apiRetention.setConceptDescription(pexConceptDescription);
		} else {
			val error = JError.required("pex-concept-description", userTools.getLocale());
			errors.add(error);
		}
	}
	
	@Data
	@AllArgsConstructor
	static class BeneficiaryResponse {
		private boolean beneficiary;
		private boolean errorMarked;
	}
}