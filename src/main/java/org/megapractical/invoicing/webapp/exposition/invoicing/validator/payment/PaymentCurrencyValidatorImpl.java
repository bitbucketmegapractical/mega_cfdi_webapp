package org.megapractical.invoicing.webapp.exposition.invoicing.validator.payment;

import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.CurrencyValidator;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentCurrencyValidatorImpl implements PaymentCurrencyValidator {

	@Autowired
	private UserTools userTools;
	
	@Autowired
	private CurrencyValidator currencyValidator;
	
	@Override
	public MonedaEntity getCurrency(String currency) {
		val currencyValidatorResponse = currencyValidator.validate(currency);
		val cmpPaymentCurrencyEntity = currencyValidatorResponse.getCurrency();
		val hasError = currencyValidatorResponse.isHasError();
		
		if (cmpPaymentCurrencyEntity != null && !hasError
				&& cmpPaymentCurrencyEntity.getCodigo().equalsIgnoreCase("XXX")) {
			return null;
		}
		
		return cmpPaymentCurrencyEntity;
	}

	@Override
	public MonedaEntity getCurrency(String currency, String field, List<JError> errors) {
		JError error = null;
		
		val currencyValidatorResponse = currencyValidator.validate(currency, field);
		val cmpPaymentCurrencyEntity = currencyValidatorResponse.getCurrency();
		error = currencyValidatorResponse.getError();
		
		if (cmpPaymentCurrencyEntity != null && error == null
				&& cmpPaymentCurrencyEntity.getCodigo().equalsIgnoreCase("XXX")) {
			error = JError.error(field, "ERR0156", userTools.getLocale());
		}
		
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
		}
		return cmpPaymentCurrencyEntity;
	}

}
