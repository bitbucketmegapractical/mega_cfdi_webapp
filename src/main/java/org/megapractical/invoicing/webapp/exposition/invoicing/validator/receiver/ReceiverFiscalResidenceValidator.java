package org.megapractical.invoicing.webapp.exposition.invoicing.validator.receiver;

import java.util.List;

import org.megapractical.invoicing.webapp.exposition.invoicing.payload.PostalCodeResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JReceiver;

public interface ReceiverFiscalResidenceValidator {
	
	PostalCodeResponse getFiscalResidence(JReceiver receiver, List<JError> errors);
	
}