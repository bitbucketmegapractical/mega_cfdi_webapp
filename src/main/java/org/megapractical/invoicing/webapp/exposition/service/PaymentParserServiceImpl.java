package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiPaymentError;
import org.megapractical.invoicing.api.wrapper.ApiPaymentFileStatistics;
import org.megapractical.invoicing.api.wrapper.ApiPaymentParser;
import org.megapractical.invoicing.api.wrapper.ApiPaymentStamp;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.PaymentEntryService;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.complement.payment.validator.PaymentParserValidationService;
import org.megapractical.invoicing.webapp.filereader.FileReader;
import org.megapractical.invoicing.webapp.util.PaymentUtils;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
@Scope("session")
public class PaymentParserServiceImpl implements PaymentParserService {
	
	private final PaymentEntryService paymentEntryService;
	private final PaymentParserValidationService parserValidationService;

	public PaymentParserServiceImpl(PaymentEntryService paymentEntryService,
			PaymentParserValidationService parserValidationService) {
		this.paymentEntryService = paymentEntryService;
		this.parserValidationService = parserValidationService;
	}

	private ApiPaymentFileStatistics statistics;
	private List<ApiPaymentError> paymentErrors;
	
	// Processing file
	public ApiPaymentParser paymentParser(ApiPaymentStamp complementStamp) {
		try {
			// Current environment
			val env = UProperties.env();

			// File path
			val paymentFilePath = complementStamp.getUploadPath();

			// Getting the file encoding
			val encoding = UFile.fileEncoding(new File(paymentFilePath));
			if (encoding != null && !encoding.equalsIgnoreCase("utf-8") && !encoding.equalsIgnoreCase("utf8")) {
				UPrint.logWithLine(env, "[INFO] PAYMENT PARSER > PROCESS ENCODING UTF-8 FILE STARTING...");
				if (Boolean.FALSE.equals(UFile.utf8Encoding(new File(paymentFilePath)))) {
					return null;
				}
				UPrint.logWithLine(env, "[INFO] PAYMENT PARSER > PROCESS ENCODING ENCODING UTF-8 FILE FINISHED...");
			}

			UPrint.logWithLine(env, "[INFO] PAYMENT PARSER > PARSER FILE STARTING...");

			// File content
			val entries = FileReader.readFile(paymentFilePath);
			
			val paymentParser = new ApiPaymentParser();
			paymentParser.setPaymentStamp(complementStamp);

			statistics = new ApiPaymentFileStatistics();
			statistics.setInvoiceFileStatistics(new ArchivoFacturaEstadisticaEntity());
			statistics.getInvoiceFileStatistics().setFechaInicioAnalisis(new Date());
			statistics.getInvoiceFileStatistics().setHoraInicioAnalisis(new Date());
			
			val validEntries = new ArrayList<String[]>();
			paymentErrors = new ArrayList<>();
			
			// Determinando tipo de archivo a parsear
			val fileType = complementStamp.getInvoiceFile().getTipoArchivo();
			if (fileType.getCodigo().equalsIgnoreCase(PaymentUtils.PAYMENT_FILE_TXT_CODE)) {
				parse(entries, validEntries);
			}

			paymentParser.setEntries(validEntries);
			paymentParser.setPaymentDataFile(parserValidationService.getPaymentDataFile());
			paymentParser.setErrors(paymentErrors);
			paymentParser.setStatistics(statistics);

			UPrint.logWithLine(env, "[INFO] PAYMENT PARSER > PARSER FILE FINISHED...");
			return paymentParser;
		} catch (Exception e) {
			Logger.getLogger(PaymentParserService.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}
	
	private void parse(List<String[]> entries, final List<String[]> validEntries) {
		// Final content after parser process
		var entriesAux = new ArrayList<String[]>();
	
		// Current line file
		var line = 0;
	
		// Total paymets in the file
		var singleEntriesTotal = 0;
	
		boolean errorSet = false;
		
		// Current environment
		val env = UProperties.env();
	
		for (int i = 0; i < entries.size(); i++) {
			// Informacion de la línea actual
			val item = entries.get(i);
	
			// Incrementenado línea
			line++;
	
			errorSet = false;
	
			// Determinando tipo de archivo a parsear
			UPrint.logWithLine(env, "[INFO] PAYMENT PARSER > TXT FILE > READING FILE LINE > " + line);
			
			// Getting entry
			val paymentEntry = paymentEntryService.findByName(item[0]);
			if (!UValidator.isNullOrEmpty(paymentEntry)) {
				if (paymentEntry.isPrincipal()) {
					// Aumentando el valor de las tramas principales
					// para contabilizar el total de comprobantes del fichero
					singleEntriesTotal++;
				}
				
				if (!paymentEntry.isPrincipal() && !paymentEntry.getNombre().equalsIgnoreCase("CEND")
						&& !parserValidationService.entryValidate(item)) {
					// Indicando que ha ocurrido un error
					errorSet = true;
					
					// Agregando el error a la lista de errores
					val paymentError = ApiPaymentError.lenghtError(paymentEntry, item, line);
					paymentErrors.add(paymentError);
					
					// Decrementando el total de pagos parseados
					singleEntriesTotal--;
				} else {
					if (paymentEntry.isPrincipal()) {
						entriesAux.add(item);
					} else {
						val errors = parserValidationService.validateField(item);
						if (!errors.isEmpty()) {
							// Indicando que ha ocurrido un error
							errorSet = true;
							// Agregando el error a la lista de errores
							errors.stream().map(paymentErrors::add).collect(Collectors.toList());
							// Decrementando el total de pagos parseados
							singleEntriesTotal--;
						}
					}
				}
				
				if (!errorSet) {
					if (i >= 1 && !paymentEntry.isPrincipal() && !parserValidationService.isEmpty(item)) {
						entriesAux.add(item);
					}
					
					var isBreak = false;
					
					String[] nextEntry = null;
					try {
						nextEntry = entries.get(i + 1);
						val nextPaymentEntry = paymentEntryService.findByName(nextEntry[0]);
						if (i > 1 && !UValidator.isNullOrEmpty(nextPaymentEntry) && (nextPaymentEntry.isPrincipal()
								|| nextPaymentEntry.getNombre().equalsIgnoreCase("CEND"))) {
							validEntries.addAll(entriesAux);
							entriesAux = new ArrayList<>();
							
							if (nextPaymentEntry.getNombre().equalsIgnoreCase("CEND")) {
								statistics.setEntries(validEntries);
								statistics.setSingleEntrie(singleEntriesTotal);
								isBreak = true;
							}
						}
					} catch (IndexOutOfBoundsException e) {
						if (item[0].equals("CEND") && singleEntriesTotal >= 1) {
							validEntries.addAll(entriesAux);
							statistics.setEntries(validEntries);
						} else {
							statistics.setEntries(new ArrayList<>());
						}
						statistics.setSingleEntrie(singleEntriesTotal);
						isBreak = true;
					}
					
					if (isBreak) {
						break;
					}
				} else {
					// Elimino las tramas cargadas en entriesAux
					entriesAux = new ArrayList<>();
					
					// Aumento la posicion de i e incremento la línea del fichero mientras la
					// trama siguiente no sea CP o CEND
					i++;
					line++;
					
					var itemErr = entries.get(i);
					while (!itemErr[0].equalsIgnoreCase("CP") && !itemErr[0].equalsIgnoreCase("CEND")
							&& line < entries.size()) {
						i++;
						line++;

						try {
							itemErr = entries.get(i + 1);
						} catch (Exception e) {
							line++;
						}
					}
				}
			}
		}
		
		statistics.getInvoiceFileStatistics().setFacturas(singleEntriesTotal);
		statistics.getInvoiceFileStatistics().setFechaFinAnalisis(new Date());
		statistics.getInvoiceFileStatistics().setHoraFinAnalisis(new Date());
	}
	
}