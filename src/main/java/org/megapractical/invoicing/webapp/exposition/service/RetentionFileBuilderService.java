package org.megapractical.invoicing.webapp.exposition.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.api.wrapper.ApiRetention.ApiRetImpRetenido;
import org.megapractical.invoicing.api.wrapper.ApiRetentionParser;
import org.megapractical.invoicing.api.wrapper.ApiRetentionVoucher;
import org.megapractical.invoicing.dal.bean.jpa.RetencionTipoImpuestoEntity;
import org.megapractical.invoicing.dal.data.repository.IRetentionTaxTypeJpaRepository;
import org.megapractical.invoicing.sat.complement.dividendos.Dividendos;
import org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenaciondeAcciones;
import org.megapractical.invoicing.sat.complement.intereses.Intereses;
import org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.CPais;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros;
import org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("session")
public class RetentionFileBuilderService {

	//##### Nueva retencion
    private Retenciones retencion;
    private Retenciones.Periodo periodo;
	private Retenciones.Emisor emisor;
	private Retenciones.Receptor receptor; 
	private Retenciones.Receptor.Nacional receptorNacional;
	private Retenciones.Receptor.Extranjero receptorExtranjero;
	private Retenciones.Totales totales;
	private Retenciones.Totales.ImpRetenidos impuestosRetenidos;
	private Retenciones.Complemento complemento;			
	
	//##### Complemento Dividendos
	private Dividendos dividendo;
	private Dividendos.DividOUtil dividendoUtilidad;
	private Dividendos.Remanente dividendoRemanente;
	
	//##### Complemento Enajenacion de acciones
	private EnajenaciondeAcciones enajenacion;
	
	//##### Complemento Intereses
	private Intereses intereses;
	
	//##### Complemento Operaciones con Derivados
	private OperacionesconDerivados operacionesDerivados;
	
	//##### Complemento Pagos a Extranjeros
	private PagosaExtranjeros pagosExtranjeros;
	private PagosaExtranjeros.Beneficiario beneficiario;
	private PagosaExtranjeros.NoBeneficiario noBeneficiario;
	
	//##### Complemento Sector Financiero
	private SectorFinanciero sectorFinanciero;
	
	private List<ApiRetImpRetenido> apiRetImpRetenidos;
	
	@Resource
	private IRetentionTaxTypeJpaRepository iRetentionTaxTypeJpaRepository;
	
	public void retencionInstance() {
    	try {
    		
    		//##### Nueva retencion
			retencion = new Retenciones();
			periodo = new Retenciones.Periodo();
			emisor = new Retenciones.Emisor();
			receptor = new Retenciones.Receptor(); 
			receptorNacional = new Retenciones.Receptor.Nacional();
			receptorExtranjero = new Retenciones.Receptor.Extranjero();
			totales = new Retenciones.Totales();
			impuestosRetenidos = new Retenciones.Totales.ImpRetenidos();
			apiRetImpRetenidos = new ArrayList<>();
			complemento = new Retenciones.Complemento();			
			
			//##### Complemento Dividendos
			dividendo = new Dividendos();
			dividendoUtilidad = new Dividendos.DividOUtil();
			dividendoRemanente = new Dividendos.Remanente();
			
			//##### Complemento Enajenacion de acciones
			enajenacion = new EnajenaciondeAcciones();
			
			//##### Complemento Intereses
			intereses = new Intereses();
			
			//##### Complemento Operaciones con Derivados
			operacionesDerivados = new OperacionesconDerivados();
			
			//##### Complemento Pagos a Extranjeros
			pagosExtranjeros = new PagosaExtranjeros();
			beneficiario = new PagosaExtranjeros.Beneficiario();
			noBeneficiario = new PagosaExtranjeros.NoBeneficiario();
			
			//##### Complemento Sector Financiero
			sectorFinanciero = new SectorFinanciero();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	public List<ApiRetentionVoucher> retentionBuild(final ApiRetentionParser wRetentionParser) {
    	try {
    		
    		//##### Listado de tramas
    		List<String[]> entries = new ArrayList<>();
    		
    		//##### Listado de retenciones a devolver
    		List<ApiRetentionVoucher> retencionesList = new ArrayList<>();
    		
    		retencionInstance();
    		ApiRetention apiRetention = null;
			
			String mesInicial = "0";
			String mesFinal = "0";
			String ejercicio = "0";
			String folioInt = null;
			
			Integer line = 0;
			
			entries.addAll(wRetentionParser.getEntries());
			for (int i = 0; i < entries.size(); i++) {
				String[] entry = entries.get(i);
				
				if(i == 0) {
					mesInicial = entry[1];
					mesFinal = entry[2];
					ejercicio = entry[3];
					folioInt = !entry[1].trim().equals("") ? entry[1] : null;
				}
				
				//##### Periodo
				if(entry[0].equalsIgnoreCase("H0")) {
					//##### Informacion general
					retencion.setVersion("1.0");
					retencion.setFolioInt(folioInt);
					
					periodo.setMesIni(mesInicial);
					periodo.setMesFin(mesFinal);
					periodo.setEjercicio(ejercicio);
					
					retencion.setPeriodo(periodo);
				}
				
				//##### Emisor
				if(entry[0].equalsIgnoreCase("t1")) {
					apiRetention = new ApiRetention();
					apiRetention.setCertificateEmitterNumber(wRetentionParser.getRetentionStamp().getStampProperties().getCertificateNumber());
					
					String emitterRfc = !entry[1].trim().equals("") ? entry[1] : null;
					emisor.setRfcE(emitterRfc);
					apiRetention.setEmitterRfc(emitterRfc);
					
					String emitterBusinessName = !entry[2].trim().equals("") ? entry[2] : null; 
					emisor.setNomDenRazSocE(emitterBusinessName);
					apiRetention.setEmitterBusinessName(emitterBusinessName);
					
					String emitterTaxRegime = !entry[3].trim().equals("") ? entry[3] : null; 
					emisor.setRegimenFiscalE(emitterTaxRegime);
					apiRetention.setEmitterTaxRegime(emitterTaxRegime);
					
					retencion.setEmisor(emisor);
				}
				
				//##### Receptor
				if(entry[0].equalsIgnoreCase("t2")) {
					String receiverNationality = !entry[1].trim().equals("") ? entry[1] : null;
					if(!UValidator.isNullOrEmpty(receiverNationality)) {
						receptor.setNacionalidadR(receiverNationality);
						apiRetention.setReceiverNationality(receiverNationality);
						
						if(receiverNationality.trim().equalsIgnoreCase("nacional")) {// Receptor Nacional
							String receiverNatRfc = !entry[2].trim().equals("") ? entry[2] : null;
							receptorNacional.setRfcR(receiverNatRfc);
							apiRetention.setReceiverNatRfc(receiverNatRfc);
							
							String receiverNatBusinessName = !entry[3].trim().equals("") ? entry[3] : null;
							receptorNacional.setNomDenRazSocR(receiverNatBusinessName);
							apiRetention.setReceiverNatBusinessName(receiverNatBusinessName);
							
							String receiverNatCurp = !entry[4].trim().equals("") ? entry[4] : null;
							receptorNacional.setCurpR(receiverNatCurp);
							apiRetention.setReceiverNatCurp(receiverNatCurp);
							
							receptor.setNacional(receptorNacional);
							retencion.setReceptor(receptor);
						} else if(receiverNationality.equalsIgnoreCase("extranjero")) {// Receptor Extranjero
							String receiverExtBusinessName = !entry[5].trim().equals("") ? entry[2] : null;
							receptorExtranjero.setNomDenRazSocR(receiverExtBusinessName);
							apiRetention.setReceiverExtBusinessName(receiverExtBusinessName);
							
							String receiverExtNumber = !entry[6].trim().equals("") ? entry[3] : null;
							receptorExtranjero.setNumRegIdTribR(receiverExtNumber);
							apiRetention.setReceiverExtNumber(receiverExtNumber);
							
							receptor.setExtranjero(receptorExtranjero);
							retencion.setReceptor(receptor);
						}
					}					
				}
				
				//##### Impuestos
				if(entry[0].equalsIgnoreCase("t3")) {
					impuestosRetenidos = new Retenciones.Totales.ImpRetenidos();
					ApiRetImpRetenido withheld = new ApiRetImpRetenido();
					
					String base= !entry[1].equals("") ? entry[1] : null;
					BigDecimal baseBig = !UValidator.isNullOrEmpty(base) ? new BigDecimal(base.replaceAll(",", "")) : BigDecimal.ZERO;
					impuestosRetenidos.setBaseRet(baseBig);
					withheld.setBase(UValue.bigDecimalString(UValue.bigDecimalString(base)));
					
					String tax = !entry[2].trim().equals("") ? entry[2] : null;
					impuestosRetenidos.setImpuestoRet(tax);
					RetencionTipoImpuestoEntity taxTypeEntity = iRetentionTaxTypeJpaRepository.findByCodigoAndEliminadoFalse(tax);
					if(taxTypeEntity != null) {
						withheld.setTax(tax);
						withheld.setTaxValue(taxTypeEntity.getCodigo() + " " + taxTypeEntity.getValor());
					}
					
					String amount = !entry[3].equals("") ? entry[3] : null;
					BigDecimal amountBig = !UValidator.isNullOrEmpty(amount) ? new BigDecimal(amount.replaceAll(",", "")) : BigDecimal.ZERO;
					impuestosRetenidos.setMontoRet(amountBig);
					withheld.setAmount(UValue.bigDecimalString(UValue.bigDecimalString(amountBig)));
					
					String paymentType = !entry[4].trim().equals("") ? entry[4] : null;
					impuestosRetenidos.setTipoPagoRet(paymentType);
					withheld.setPaymentType(paymentType);
					
					totales.getImpRetenidos().add(impuestosRetenidos);
					apiRetImpRetenidos.add(withheld);
				}
				
				//##### Totales
				if(entry[0].equalsIgnoreCase("t4")) {
					String totalOperationAmount = !entry[1].trim().equals("") ? entry[1] : null;
					BigDecimal totalOperationAmountBig = !UValidator.isNullOrEmpty(totalOperationAmount) ? new BigDecimal(totalOperationAmount.replaceAll(",", "")) : BigDecimal.ZERO;
					totales.setMontoTotOperacion(totalOperationAmountBig);
					apiRetention.setTotalOperationAmount(totalOperationAmount);
					
					String totalAmountTaxed = !entry[2].trim().equals("") ? entry[2] : null;
					BigDecimal totalAmountTaxedBig = !UValidator.isNullOrEmpty(totalAmountTaxed) ? new BigDecimal(totalAmountTaxed.replaceAll(",", "")) : BigDecimal.ZERO;
					totales.setMontoTotGrav(totalAmountTaxedBig);
					apiRetention.setTotalAmountTaxed(totalAmountTaxed);
					
					String totalAmountExempt = !entry[3].trim().equals("") ? entry[3] : null;
					BigDecimal totalAmountExemptBig = !UValidator.isNullOrEmpty(totalAmountExempt) ? new BigDecimal(totalAmountExempt.replaceAll(",", "")) : BigDecimal.ZERO;
					totales.setMontoTotExent(totalAmountExemptBig);
					apiRetention.setTotalAmountExempt(totalAmountExempt);
					
					String totalAmountWithheld = !entry[4].trim().equals("") ? entry[4] : null;
					BigDecimal totalAmountWithheldBig = !UValidator.isNullOrEmpty(totalAmountWithheld) ? new BigDecimal(totalAmountWithheld.replaceAll(",", "")) : BigDecimal.ZERO;
					totales.setMontoTotRet(totalAmountWithheldBig);
					apiRetention.setTotalAmountWithheld(totalAmountWithheld);
					
					retencion.setTotales(totales);
					apiRetention.setApiRetImpRetenidos(apiRetImpRetenidos);
				}
				
				//##### Complemento Dividendos
				if(entry[0].equalsIgnoreCase("t5")) {
					String dividendType = !entry[1].trim().equals("") ? entry[1] : null;
					dividendoUtilidad.setCveTipDivOUtil(dividendType);
					apiRetention.setCveTipDivOUtil(dividendType);
					
					String dividendAmountNat = !entry[2].trim().equals("") ? entry[2] : null;
					BigDecimal dividendAmountNatBig = !UValidator.isNullOrEmpty(dividendAmountNat) ? new BigDecimal(dividendAmountNat.replaceAll(",", "")) : BigDecimal.ZERO;
					dividendoUtilidad.setMontISRAcredRetMexico(dividendAmountNatBig);
					apiRetention.setMontISRAcredRetMexico(dividendAmountNat);
					
					String dividendAmountExt = !entry[3].trim().equals("") ? entry[3] : null;
					BigDecimal dividendAmountExtBig = !UValidator.isNullOrEmpty(dividendAmountExt) ? new BigDecimal(dividendAmountExt.replaceAll(",", "")) : BigDecimal.ZERO;
					dividendoUtilidad.setMontISRAcredRetExtranjero(dividendAmountExtBig);
					apiRetention.setMontISRAcredRetExtranjero(dividendAmountExt);
					
					String dividendAmountWithheldExt = !entry[4].trim().equals("") ? entry[4] : null;
					BigDecimal dividendAmountWithheldExtBig = !UValidator.isNullOrEmpty(dividendAmountWithheldExt) ? new BigDecimal(dividendAmountWithheldExt.replaceAll(",", "")) : BigDecimal.ZERO;
					dividendoUtilidad.setMontRetExtDivExt(dividendAmountWithheldExtBig);
					apiRetention.setMontRetExtDivExt(dividendAmountWithheldExt);
					
					String dividendSocieties = !entry[5].trim().equals("") ? entry[5] : null;
					dividendoUtilidad.setTipoSocDistrDiv(dividendSocieties);
					apiRetention.setTipoSocDistrDiv(dividendSocieties);
					
					String dividendIsr = !entry[6].trim().equals("") ? entry[6] : null;
					BigDecimal dividendIsrBig = !UValidator.isNullOrEmpty(dividendIsr) ? new BigDecimal(dividendIsr.replaceAll(",", "")) : BigDecimal.ZERO;
					dividendoUtilidad.setMontISRAcredNal(dividendIsrBig);
					apiRetention.setMontISRAcredNal(dividendIsr);
					
					String dividendCumulativeNat = !entry[7].trim().equals("") ? entry[7] : null;
					BigDecimal dividendCumulativeNatBig = !UValidator.isNullOrEmpty(dividendCumulativeNat) ? new BigDecimal(dividendCumulativeNat.replaceAll(",", "")) : BigDecimal.ZERO;
					dividendoUtilidad.setMontDivAcumNal(dividendCumulativeNatBig);
					apiRetention.setMontDivAcumNal(dividendCumulativeNat);
					
					String dividendCumulativeExt = !entry[8].trim().equals("") ? entry[8] : null;
					BigDecimal dividendCumulativeExtBig = !UValidator.isNullOrEmpty(dividendCumulativeExt) ? new BigDecimal(dividendCumulativeExt.replaceAll(",", "")) : BigDecimal.ZERO;
					dividendoUtilidad.setMontDivAcumExt(dividendCumulativeExtBig);
					apiRetention.setMontDivAcumExt(dividendCumulativeExt);
					
					String dividendRemaining = !entry[9].trim().equals("") ? entry[9] : null;
					BigDecimal dividendRemainingBig = !UValidator.isNullOrEmpty(dividendRemaining) ? new BigDecimal(dividendRemaining.replaceAll(",", "")) : BigDecimal.ZERO;
					dividendoRemanente.setProporcionRem(dividendRemainingBig);
					apiRetention.setProporcionRem(dividendRemaining);
					
					dividendo.setVersion("1.0");
					dividendo.setDividOUtil(dividendoUtilidad);
					dividendo.setRemanente(dividendoRemanente);
					
					complemento.getAny().add(dividendo);
					retencion.setCveRetenc("14");
				}
				
				//##### Complemento Enajenacion de Acciones
				if(entry[0].equalsIgnoreCase("t6")) {
					enajenacion.setVersion("1.0");
					
					String contract = !entry[1].trim().equals("") ? entry[1] : null;
					enajenacion.setContratoIntermediacion(contract);
					apiRetention.setContract(contract);
					
					String profit = !entry[2].trim().equals("") ? entry[2] : null;
					BigDecimal profitBig = !UValidator.isNullOrEmpty(profit) ? new BigDecimal(profit.replaceAll(",", "")) : BigDecimal.ZERO;
					enajenacion.setGanancia(profitBig);
					apiRetention.setProfit(profit);
					
					String waste = !entry[3].trim().equals("") ? entry[3] : null;
					BigDecimal wasteBig = !UValidator.isNullOrEmpty(waste) ? new BigDecimal(waste.replaceAll(",", "")) : BigDecimal.ZERO;
					enajenacion.setPerdida(wasteBig);
					apiRetention.setWaste(waste);
					
					complemento.getAny().add(enajenacion);
					retencion.setCveRetenc("06");
				}
				
				//##### Complemento Intereses
				if(entry[0].equalsIgnoreCase("t7")) {
					intereses.setVersion("1.0");
					
					String financeSystem = !entry[1].trim().equals("") ? entry[1] : null;
					intereses.setSistFinanciero(financeSystem);
					apiRetention.setFinanceSystem(financeSystem);
					
					String retirement = !entry[2].trim().equals("") ? entry[2] : null;
					intereses.setRetiroAORESRetInt(retirement);
					apiRetention.setRetirement(retirement);
					
					String financialOperations = !entry[3].trim().equals("") ? entry[3] : null;
					intereses.setOperFinancDerivad(financialOperations);
					apiRetention.setFinancialOperations(financialOperations);
					
					String interestAmount = !entry[4].trim().equals("") ? entry[4] : null;
					BigDecimal interestAmountBig = !UValidator.isNullOrEmpty(interestAmount) ? new BigDecimal(interestAmount.replaceAll(",", "")) : BigDecimal.ZERO;
					intereses.setMontIntNominal(interestAmountBig);
					apiRetention.setInterestAmount(interestAmount);
					
					String realInterest = !entry[5].trim().equals("") ? entry[5] : null;
					BigDecimal realInterestBig = !UValidator.isNullOrEmpty(realInterest) ? new BigDecimal(realInterest.replaceAll(",", "")) : BigDecimal.ZERO;
					intereses.setMontIntReal(realInterestBig);
					apiRetention.setRealInterest(realInterest);
					
					String interestWaste = !entry[6].trim().equals("") ? entry[6] : null;
					BigDecimal interestWasteBig = !UValidator.isNullOrEmpty(interestWaste) ? new BigDecimal(interestWaste.replaceAll(",", "")) : BigDecimal.ZERO;
					intereses.setPerdida(interestWasteBig);
					apiRetention.setInterestWaste(interestWaste);
					
					complemento.getAny().add(intereses);
					retencion.setCveRetenc("16");
				}
				
				//##### Complemento Operaciones con Derivados
				if(entry[0].equalsIgnoreCase("t8")) {
					operacionesDerivados.setVersion("1.0");
					
					String profitAmount = !entry[1].trim().equals("") ? entry[1] : null;
					BigDecimal profitAmountBig = !UValidator.isNullOrEmpty(profitAmount) ? new BigDecimal(profitAmount.replaceAll(",", "")) : BigDecimal.ZERO;
					operacionesDerivados.setMontGanAcum(profitAmountBig);
					apiRetention.setProfitAmount(profitAmount);
					
					String deductibleWaste = !entry[2].trim().equals("") ? entry[2] : null;
					BigDecimal deductibleWasteBig = !UValidator.isNullOrEmpty(deductibleWaste) ? new BigDecimal(deductibleWaste.replaceAll(",", "")) : BigDecimal.ZERO;
					operacionesDerivados.setMontPerdDed(deductibleWasteBig);
					apiRetention.setDeductibleWaste(deductibleWaste);
					
					complemento.getAny().add(operacionesDerivados);
					retencion.setCveRetenc("24");
				}
				
				//##### Complemento Pagos a Extranjeros
				if(entry[0].equalsIgnoreCase("t9")) {
					pagosExtranjeros.setVersion("1.0");
					
					String isb = !entry[1].trim().equals("") ? entry[1] : null;
					if(isb != null) {
						String paymentConcept = !entry[6].trim().equals("") ? entry[6] : null;
						apiRetention.setPaymentConcept(paymentConcept);
						
						String conceptDescription = !entry[7].trim().equals("") ? entry[7] : null;
						apiRetention.setConceptDescription(conceptDescription);
						
						pagosExtranjeros.setEsBenefEfectDelCobro(isb);
						if(isb.equalsIgnoreCase("si")) {
							apiRetention.setBeneficiary(true);
							
							String beneficiaryRfc = !entry[3].trim().equals("") ? entry[3] : null; 
							beneficiario.setRFC(beneficiaryRfc);
							apiRetention.setBeneficiaryRfc(beneficiaryRfc);
							
							String beneficiaryCurp = !entry[4].trim().equals("") ? entry[4] : null; 
							beneficiario.setCURP(beneficiaryCurp);
							apiRetention.setBeneficiaryCurp(beneficiaryCurp);
							
							String beneficiaryBusinessName = !entry[5].trim().equals("") ? entry[5] : null; 
							beneficiario.setNomDenRazSocB(beneficiaryBusinessName);
							apiRetention.setBeneficiaryBusinessName(beneficiaryBusinessName);
							
							beneficiario.setConceptoPago(paymentConcept);
							beneficiario.setDescripcionConcepto(conceptDescription);
							pagosExtranjeros.setBeneficiario(beneficiario);
						} else if(isb.equalsIgnoreCase("no")) {
							apiRetention.setBeneficiary(false);
							
							String noBeneficiaryCountry = !entry[2].trim().equals("") ? entry[2] : null;
							CPais countryCatalogue = !UValidator.isNullOrEmpty(noBeneficiaryCountry) ? CPais.fromValue(noBeneficiaryCountry) : null;
							noBeneficiario.setPaisDeResidParaEfecFisc(countryCatalogue);
							apiRetention.setNoBeneficiaryCountry(noBeneficiaryCountry);
							
							noBeneficiario.setConceptoPago(paymentConcept);
							noBeneficiario.setDescripcionConcepto(conceptDescription);
							pagosExtranjeros.setNoBeneficiario(noBeneficiario);
						}
						
						complemento.getAny().add(pagosExtranjeros);
						retencion.setCveRetenc("18");
					}
				}
				
				//##### Complemento Sector Financiero
				if(entry[0].equalsIgnoreCase("t10")) {
					sectorFinanciero.setVersion("1.0");
					
					String fideicomId = !entry[1].trim().equals("") ? entry[1] : null;
					sectorFinanciero.setIdFideicom(fideicomId);
					apiRetention.setFideicomId(fideicomId);
					
					String fideicomName = !entry[2].trim().equals("") ? entry[2] : null;
					sectorFinanciero.setNomFideicom(fideicomName);
					apiRetention.setFideicomName(fideicomName);
					
					String fideicomDescription = !entry[3].trim().equals("") ? entry[3] : null;
					sectorFinanciero.setDescripFideicom(fideicomDescription);
					apiRetention.setFideicomDescription(fideicomDescription);
					
					complemento.getAny().add(sectorFinanciero);
					retencion.setCveRetenc("25");
				}
				
				line ++;
				
				boolean nextEntrieExist = false;
				try {
					
					String[] nextEntrie = entries.get(i + 1);										
					nextEntrieExist = nextEntrie[0].equalsIgnoreCase("t1") ? true : false;
					
				} catch (Exception e) {
					nextEntrieExist = false;
				}
				
				if(i > 0 && (line == entries.size() || nextEntrieExist)) {
					if(complemento.getAny().size() > 0){
						retencion.setComplemento(complemento);
					}
					
					ApiRetentionVoucher arv = new ApiRetentionVoucher();
					arv.setRetencion(retencion);
					arv.setApiRetention(apiRetention);
					retencionesList.add(arv);
					
					retencionInstance();
				}
			}						
			
			return retencionesList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
}
