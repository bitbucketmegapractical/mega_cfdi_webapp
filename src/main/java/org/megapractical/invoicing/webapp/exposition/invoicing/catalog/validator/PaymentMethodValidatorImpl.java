package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.api.util.UCatalog;
import org.megapractical.invoicing.dal.data.repository.IPaymentMethodJpaRepository;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.PaymentMethodValidatorResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class PaymentMethodValidatorImpl implements PaymentMethodValidator {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private IPaymentMethodJpaRepository iPaymentMethodJpaRepository;
	
	@Override
	public PaymentMethodValidatorResponse validate(String paymentMethod, String field) {
		JError error = null;
		
		// Validar que sea un valor de catalogo
		val paymentMethodCatalog = UCatalog.getCatalog(paymentMethod);
		paymentMethod = paymentMethodCatalog[0];
		val paymentMethodValue = paymentMethodCatalog[1];
		
		val paymentMethodEntity = iPaymentMethodJpaRepository.findByCodigoAndEliminadoFalse(paymentMethod);
		val paymentMethodEntityByValue = iPaymentMethodJpaRepository.findByValorAndEliminadoFalse(paymentMethodValue);
		if (paymentMethodEntity == null || paymentMethodEntityByValue == null) {
			val values = new String[]{"CFDI33121", "MetodoPago", "c_MetodoPago"};
			error = JError.invalidCatalog(field, "ERR1002", values, userTools.getLocale());
		}
		
		return new PaymentMethodValidatorResponse(paymentMethodEntity, error);
	}

}