package org.megapractical.invoicing.webapp.exposition.retention.helper;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiRetention;
import org.megapractical.invoicing.webapp.exposition.retention.payload.RetentionComplementResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JRetention.RetentionData;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.stereotype.Component;

import lombok.val;

@Component
public class RetentionComplementSfiHelper {
	
	private final UserTools userTools;

	public RetentionComplementSfiHelper(UserTools userTools) {
		this.userTools = userTools;
	}
	
	public RetentionComplementResponse populate(RetentionData retentionData, String retentionKey,
			final ApiRetention apiRetention, final List<JError> errors) {
		val retSfi = retentionData.getRetSfi();
		if (!UValidator.isNullOrEmpty(retSfi)) {
			// Número de fideicomiso
			val sfiFideicomId = UBase64.base64Decode(retSfi.getFideicomId());
			if (!UValidator.isNullOrEmpty(sfiFideicomId)) {
				apiRetention.setFideicomId(sfiFideicomId);
			} else {
				val error = JError.required("sfi-fideicom-id", userTools.getLocale());
				errors.add(error);
			}
			
			// Nombre de fideicomiso
			val sfiFideicomName = UBase64.base64Decode(retSfi.getFideicomName());
			if (!UValidator.isNullOrEmpty(sfiFideicomName)) {
				apiRetention.setFideicomName(sfiFideicomName);
			}
			
			// Descricpción de fideicomiso
			val sfiFideicomDescription = UBase64.base64Decode(retSfi.getFideicomDescription());
			if (!UValidator.isNullOrEmpty(sfiFideicomDescription)) {
				apiRetention.setFideicomDescription(sfiFideicomDescription);
			} else {
				val error = JError.required("sfi-fideicom-description", userTools.getLocale());
				errors.add(error);
			}
			
			return RetentionComplementResponse.ok();
		} else if (retentionKey.equalsIgnoreCase("25")) {
			return RetentionComplementResponse.error();
		}
		return RetentionComplementResponse.empty();
	}
	
}