package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator;

import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload.VoucherTypeValidatorResponse;

public interface VoucherTypeCatalogValidator {
	
	VoucherTypeValidatorResponse validate(String voucherType);
	
	VoucherTypeValidatorResponse validate(String voucherType, String field);
	
}