package org.megapractical.invoicing.webapp.exposition.service;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiRetentionError;
import org.megapractical.invoicing.api.wrapper.ApiRetentionParser;
import org.megapractical.invoicing.api.wrapper.ApiRetentionStamp;
import org.megapractical.invoicing.dal.bean.jpa.RetencionCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionFicheroEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionTramaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoArchivoEntity;
import org.megapractical.invoicing.dal.data.repository.IRetentionDataFileJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionEntryJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRetentionFieldJpaRepository;
import org.megapractical.invoicing.webapp.util.UProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

@Service
@Scope("session")
public class RetentionParserService {
	
	@Resource
	private IRetentionDataFileJpaRepository iRetentionDataFileJpaRepository;
	
	@Resource
	private IRetentionEntryJpaRepository iRetentionEntryJpaRepository;
	
	@Resource
	private IRetentionFieldJpaRepository iRetentionFieldJpaRepository;
	
	private static final String RETENTION_FILE_CODE = "RETV10";
	private static final String RETENTION_FILE_TXT_CODE = "R10TXT";
	
	private RetencionFicheroEntity retentionDataFile;
	private List<RetencionTramaEntity> retentionEntriesList = new ArrayList<>();

	//##### Inicializando contenido
	private void init() {
		try {
			
			retentionDataFile = iRetentionDataFileJpaRepository.findByCodigoIgnoreCaseAndEliminadoFalse(RETENTION_FILE_CODE);
			retentionEntriesList = iRetentionEntryJpaRepository.findByRetencionFicheroAndEliminadoFalse(retentionDataFile);
			
		} catch (Exception e) {
			e.printStackTrace();
			
			retentionDataFile = null;
			retentionEntriesList = new ArrayList<>();
		}	
	}
		
	//##### Longitud de la trama
	public Integer entriesLength(String[] entry) {
		Integer length = null;
		try {
			
			for (RetencionTramaEntity item : retentionEntriesList) {
				if(item.getNombre().equalsIgnoreCase(entry[0])){
					return item.getLongitud();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return length;
	}
	
	//##### Validacion de trama
	public boolean entriesValidate(String[] entry) {
		try {
			
			return entry.length == entriesLength(entry) ? true : false;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	//##### Validacion de campo
	public List<ApiRetentionError> entryValidate(final String[] entry, final String condition) {
		List<ApiRetentionError> retentionErrors = new ArrayList<ApiRetentionError>();
		try {
			
			Integer entryLength = entriesLength(entry) - 1;
			RetencionTramaEntity retentionEntry = iRetentionEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(entry[0]);
			
			ApiRetentionError retentionError = null; 
			Boolean validation = true;
			
			List<RetencionCampoEntity> campoSource = iRetentionFieldJpaRepository.findByRetencionTramaAndEliminadoFalse(retentionEntry);
			if(!campoSource.isEmpty()) {
				for (int i = 1; i <= entryLength; i ++) {
					RetencionCampoEntity retentionField = iRetentionFieldJpaRepository.findByRetencionTramaAndPosicionAndEliminadoFalse(retentionEntry, i);
					validation = entryValidateContent(retentionEntry, retentionField, entry[i], condition);
					if(!validation){
						retentionError = new ApiRetentionError();
						retentionError.setField(retentionField);
						retentionError.setFieldPosition(i);
						retentionError.setFieldValue(entry[i]);
						retentionError.setFieldLength(retentionField.getLongitud());
						retentionError.setFieldMinLength(retentionField.getLongitudMinima());
						retentionError.setFieldMaxLength(retentionField.getLongitudMaxima());
						retentionError.setFieldLengthOnFile(entry[i].length());
						retentionError.setFieldRegularExpression(retentionField.getValidacion());
						retentionError.setDescription("Validación de contenido incorrecta.");
						retentionErrors.add(retentionError);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retentionErrors;
	}
	
	//##### Validacion de contenido del campo
	public boolean entryValidateContent(final RetencionTramaEntity retentionEntry, final RetencionCampoEntity retentionField, String value, final String condition) {
		try {
			
			value = value.trim();
			boolean evaluateFild = true;
			
			//##### Valida si la trama y el campo son requeridos
			//##### El campo debe traer valor
			if(retentionEntry.getRequerido() && retentionField.getRequerido() && value.isEmpty()) {
				if(!UValidator.isNullOrEmpty(retentionField.getCondicion()) && retentionField.getCondicion().equalsIgnoreCase(condition)) {
					return false;
				} else {
					evaluateFild = false;
				}
			}
			
			if(evaluateFild) {
				//##### Valida que la longitud sea igual a la longitud del campo
				if(!UValidator.isNullOrEmpty(retentionField.getLongitud())) {
					if(retentionEntry.getRequerido()) {
						if(retentionField.getRequerido()) {
							return value.length() == retentionField.getLongitud() ? true : false;
						} else {
							if(!UValidator.isNullOrEmpty(value)) {
								return value.length() == retentionField.getLongitud() ? true : false;
							}
						}					
					} else {
						if(!UValidator.isNullOrEmpty(value)) {
							return value.length() == retentionField.getLongitud() ? true : false;
						}
					}
				}
				
				//##### Valida que la longitud sea mayor o igual a la longitud minima del campo
				//##### No se tiene en cuenta la longitud maxima
				if(!UValidator.isNullOrEmpty(retentionField.getLongitudMinima()) && UValidator.isNullOrEmpty(retentionField.getLongitudMaxima())) {
					if(retentionEntry.getRequerido()) {
						if(retentionField.getRequerido()) {
							return value.length() >= retentionField.getLongitudMinima() ? true : false;
						} else {
							if(!UValidator.isNullOrEmpty(value)) {
								return value.length() >= retentionField.getLongitudMinima() ? true : false;
							}
						}
					} else {
						if(!UValidator.isNullOrEmpty(value)) {
							return value.length() >= retentionField.getLongitudMinima() ? true : false;
						}
					}
				}
				
				//##### Valida que la longitud sea mayor o igual a la longitud minima
				//##### y menor o igual a la longitd maxima del campo
				if(!UValidator.isNullOrEmpty(retentionField.getLongitudMinima()) && !UValidator.isNullOrEmpty(retentionField.getLongitudMaxima())) {
					if(retentionEntry.getRequerido()) {
						if(retentionField.getRequerido()) {
							return value.length() >= retentionField.getLongitudMinima() && value.length() <= retentionField.getLongitudMaxima() ? true : false;
						} else {
							if(!UValidator.isNullOrEmpty(value)) {
								return value.length() >= retentionField.getLongitudMinima() && value.length() <= retentionField.getLongitudMaxima() ? true : false;
							}
						}
					} else {
						if(!UValidator.isNullOrEmpty(value)) {
							return value.length() >= retentionField.getLongitudMinima() && value.length() <= retentionField.getLongitudMaxima() ? true : false;
						}
					}
				}
				
				//##### Valida que la longitud sea menor o igual a la longitud maxima del campo
				//##### No se tiene en cuenta la longitud minima
				if(!UValidator.isNullOrEmpty(retentionField.getLongitudMaxima()) && UValidator.isNullOrEmpty(retentionField.getLongitudMinima())) {
					if(retentionEntry.getRequerido()) {
						if(retentionField.getRequerido()) {
							return value.length() <= retentionField.getLongitudMaxima() ? true : false;
						} else {
							if(!UValidator.isNullOrEmpty(value)) {
								return value.length() <= retentionField.getLongitudMaxima() ? true : false;
							}
						}
					} else {
						if(!UValidator.isNullOrEmpty(value)) {
							return value.length() <= retentionField.getLongitudMaxima() ? true : false;
						}
					}
				}
				
				//##### Valida por expresion regular
				if(!UValidator.isNullOrEmpty(retentionField.getValidacion())) {
					if(retentionEntry.getRequerido()) {
						if(retentionField.getRequerido()) {
							return value.toString().matches(retentionField.getValidacion());
						} else {
							if(!UValidator.isNullOrEmpty(value)) {
								return value.toString().matches(retentionField.getValidacion());
							}
						}
					} else {
						if(!UValidator.isNullOrEmpty(value)) {
							return value.toString().matches(retentionField.getValidacion());
						}
					}
				}
			}
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}		
	}
	
	//##### Valida si una trama esta vacia
	//##### NOTA: Los valores que indican si la trama trae 
	//##### contenido comienzan en la posicion 1 de la misma
	public boolean isEntryEmpty(String[] entry) {
		boolean isEmpty = false;
		try {
			
			Integer entryLength = entriesLength(entry) - 1;
			for (int i = 1; i <= entryLength; i ++) {
				isEmpty = entry[i].isEmpty() ? true : false;
				if(!isEmpty) break;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isEmpty;
	}
	
	public ApiRetentionParser retentionParser(ApiRetentionStamp retentionStamp) {
		try {
			
			//##### Ruta del fichero
			String retentionFilePath = retentionStamp.getUploadPath();
			
			//##### Determinando el encoding
			String encoding = UFile.fileEncoding(new File(retentionFilePath));
			if(encoding != null && !encoding.equalsIgnoreCase("utf-8") && !encoding.equalsIgnoreCase("utf8")){
				
				System.out.println("-----------------------------------------------------------------------------------------------------------");
				System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION PARSER > PROCESS ENCODING UTF-8 FILE STARTING...");
				
				if(!UFile.fileUtf8(new File(retentionFilePath))){
					return null;
				}
				
				System.out.println("-----------------------------------------------------------------------------------------------------------");
				System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION PARSER > PROCESS ENCODING ENCODING UTF-8 FILE FINISHED...");
			}
			
			System.out.println("-----------------------------------------------------------------------------------------------------------");
			System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION PARSER > PARSER FILE STARTING...");
			
			init();
			
			//##### Cargando el fichero
    		File file = new File(retentionFilePath);
    		
    		//##### Leyendo fichero
			//CSVReader reader = new CSVReader(new FileReader(file), '|');
    		CSVParser parser = new CSVParserBuilder()
    								.withSeparator('|')
    								.withIgnoreQuotations(true)
    							    .build();
    		CSVReader reader = new CSVReaderBuilder(new FileReader(file))
									.withCSVParser(parser)
									.build();
        	
        	//##### Contenido del fichero
        	List<String[]> entries = reader.readAll();
        	
        	//##### Cerrando el fichero
        	reader.close();
        	
        	//##### Contenido final despues de parsear
        	List<String[]> entriesFinally = new ArrayList<>();
        	
        	//##### Indica la línea del fichero
        	Integer line = 0;
        	
        	//##### Indica la cantidad de nominas del fichero
        	Integer singleEntriesTotal = 0;
        	
        	//##### Indica la cantidad de complementos en la retencion
        	Integer complement = 0;
        	
        	//##### Condicional de campos requeridos en algunas tramas 
        	String condition = null;
        	
        	boolean errorSet = false;
        	
        	List<String[]> entriesAux = new ArrayList<>();
        	
        	ApiRetentionParser retentionParser = new ApiRetentionParser();
        	retentionParser.setRetentionStamp(retentionStamp);
        	
        	ApiRetentionError retentionError = new ApiRetentionError();
        	List<ApiRetentionError> retentionErrors = new ArrayList<>();
        	
        	for (int i = 0; i < entries.size(); i ++) {
        		
        		//##### Informacion de la línea actual
        		String[] item = entries.get(i);
        		
        		//##### Incrementenado línea
				line ++;
				
				condition = null;
				errorSet = false;
				retentionError = new ApiRetentionError();
				
				//##### Determinando tipo de archivo a parsear
				TipoArchivoEntity fileType = retentionStamp.getRetentionFile().getTipoArchivo();
				if(fileType.getCodigo().equalsIgnoreCase(RETENTION_FILE_TXT_CODE)){
					System.out.println("-----------------------------------------------------------------------------------------------------------");
					System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION PARSER > TXT FILE > READING FILE LINE > " + line);
					
					//##### Obteniendo trama
					RetencionTramaEntity retentionEntry = new RetencionTramaEntity();
					retentionEntry = iRetentionEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(item[0]);	
					
					if(!UValidator.isNullOrEmpty(retentionEntry)) {
						boolean isPrincipal = retentionEntry.getPrincipal(); 
						if(isPrincipal) {
							//##### Aumentando el valor de las tramas principales
							//##### para contabilizar el total de comprobantes del fichero
		        			singleEntriesTotal ++;
		        			
		        			//##### Reseteo el valor de complemento
	            			complement = 0;
						}
						
						if(!entriesValidate(item)) {
							//##### Indicando que ha ocurrido un error
							errorSet = true;
							
							//##### Agregando el error a la lista de errores
							retentionError.setLine(line);
							retentionError.setEntry(retentionEntry);
							retentionError.setEntryLength(retentionEntry.getLongitud());
							retentionError.setEntryLengthOnFile(item.length);
							retentionError.setDescription("Longitud de trama no válida.");
							retentionErrors.add(retentionError);
							
							//##### Decrementando el total de nominas parseadas
							singleEntriesTotal --;
						} else {
							if(isPrincipal) {
								entriesAux.add(item);
							} else {
								//##### Definiendo los condicionales de tramas
								if(item[0].equalsIgnoreCase("t2") || item[0].equalsIgnoreCase("t9")) {
									condition = item[1].trim();
								}
								
								List<ApiRetentionError> errorWrapperListAux = new ArrayList<>();
								errorWrapperListAux.addAll(entryValidate(item, condition));
								
								if(!errorWrapperListAux.isEmpty()) {
									//##### Indicando que ha ocurrido un error
									errorSet = true;
									
									//##### Agregando el error a la lista de errores
									for (ApiRetentionError error : errorWrapperListAux) {
										retentionErrors.add(error);
									}
									
									//##### Decrementando el total de nominas parseadas
									singleEntriesTotal --;
								} else {
									//##### Validar que solo haya un complemento ===================================================================================================================
									if(item[0].equalsIgnoreCase("t5") || item[0].equalsIgnoreCase("t6") || item[0].equalsIgnoreCase("t7") || item[0].equalsIgnoreCase("t8") || 
											item[0].equalsIgnoreCase("t9") || item[0].equalsIgnoreCase("t10")) {
										// ==========================================================================================================================================================
										// ===== Aumentando el valor de complemento =================================================================================================================
										// ===== NOTA: Solo se puede tener un solo complemento por retencion ========================================================================================
										// ==========================================================================================================================================================
										Boolean isT5 = item[0].equalsIgnoreCase("t5") ? true : false;
										Boolean isT6 = item[0].equalsIgnoreCase("t6") ? true : false;
										Boolean isT7 = item[0].equalsIgnoreCase("t7") ? true : false;
										Boolean isT8 = item[0].equalsIgnoreCase("t8") ? true : false;
										Boolean isT9 = item[0].equalsIgnoreCase("t9") ? true : false;
										Boolean isT10 = item[0].equalsIgnoreCase("t10") ? true : false;
										
										if(isT5) {
											complement += (!item[1].isEmpty() || !item[2].isEmpty() || !item[3].isEmpty() || !item[4].isEmpty() || !item[5].isEmpty() || !item[6].isEmpty() 
												|| !item[7].isEmpty() || !item[8].isEmpty() || !item[9].isEmpty()) ? 1 : 0; 
										} else if(isT6) {
											complement += (!item[1].isEmpty() || !item[2].isEmpty() || !item[3].isEmpty()) ? 1 : 0; 
										} else if(isT7) {
											complement += (!item[1].isEmpty() || !item[2].isEmpty() || !item[3].isEmpty() || !item[4].isEmpty() || !item[5].isEmpty() || !item[6].isEmpty()) ? 1 : 0;
										} else if(isT8) {
											complement += (!item[1].isEmpty() || !item[2].isEmpty()) ? 1 : 0;
										} else if(isT9) {
											complement += (!item[1].isEmpty() || !item[2].isEmpty() || !item[3].isEmpty() || !item[4].isEmpty() || !item[5].isEmpty() || !item[6].isEmpty() 
													|| !item[7].isEmpty()) ? 1 : 0;
										} else if(isT10) {
											complement += (!item[1].isEmpty() || !item[2].isEmpty() || !item[3].isEmpty()) ? 1 : 0;
										}
										errorSet = complement > 1 ? true : false;
				            			// ==========================================================================================================================================================
									}
								}
							}
						}
						
						if(!errorSet) {
							if(i >= 1 && !isPrincipal) {
								if(!isEntryEmpty(item)) {
									entriesAux.add(item);
								}
							}
							
							String[] nextEntry = null;
							RetencionTramaEntity nextRetentionEntry = new RetencionTramaEntity();
							
							try {
								
								nextEntry = entries.get(i + 1);
								
							} catch (Exception e) {
								if(nextEntry == null && singleEntriesTotal >= 1) {
									entriesFinally.addAll(entriesAux);
								}								
								break;
							}
							
							nextRetentionEntry = iRetentionEntryJpaRepository.findByNombreIgnoreCaseAndEliminadoFalse(nextEntry[0]);
							if(i > 1 && !UValidator.isNullOrEmpty(nextRetentionEntry) && nextRetentionEntry.getPrincipal()) {
								entriesFinally.addAll(entriesAux);
								entriesAux = new ArrayList<>();
							}
						} else {
		            		//##### Elimino las tramas cargadas en entriesAux
		            		entriesAux = new ArrayList<>();
		            		
		            		//##### Aumento la posicion de i e incremento la línea del fichero mientras la trama siguiente no sea H0
		            		i ++;
		        			line ++;
		        			
		        			String[] itemErr = entries.get(i);
		            		
		        			while(!itemErr[0].equalsIgnoreCase("H0") && line < entries.size()) {
		            			i ++;
		            			line ++;
		            			
		            			try {
		            				
		            				itemErr = entries.get(i + 1);
		            				
								} catch (Exception e) {
									line ++;
								}
		            		}
						}
					}
				}
        	}
        	
        	retentionParser.setEntries(entriesFinally);
        	retentionParser.setRetentionDataFile(this.retentionDataFile);
        	retentionParser.setErrors(retentionErrors);
        	
        	System.out.println("-----------------------------------------------------------------------------------------------------------");
			System.out.println(UPrint.consoleDatetime() + UProperties.env() + " " + "[INFO] RETENTION PARSER > PARSER FILE FINISHED...");
        	
        	return retentionParser;
        	
		} catch (Exception e) {
			Logger.getLogger(RetentionParserService.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}
}