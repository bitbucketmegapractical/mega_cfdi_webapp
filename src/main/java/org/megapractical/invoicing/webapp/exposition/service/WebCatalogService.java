package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentMethodJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentWayJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IStringTypeJpaRepository;
import org.megapractical.invoicing.voucher.catalogs.CFormaPago;
import org.megapractical.invoicing.voucher.catalogs.CMetodoPago;
import org.megapractical.invoicing.voucher.catalogs.CMoneda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class WebCatalogService {

	@Autowired
	private IPaymentWayJpaRepository iPaymentWayJpaRepository;

	@Autowired
	private ICurrencyJpaRepository iCurrencyJpaRepository;

	@Autowired
	private IPaymentMethodJpaRepository iPaymentMethodJpaRepository;

	@Autowired
	private IStringTypeJpaRepository iStringTypeJpaRepository;

	private static final String STRING_FORMAT = "%s (%s)";
	
	public String paymentWay(CFormaPago paymentWay) {
		val entity = iPaymentWayJpaRepository.findByCodigoAndEliminadoFalse(paymentWay.value());
		if (!UValidator.isNullOrEmpty(entity)) {
			return String.format(STRING_FORMAT, entity.getCodigo(), entity.getValor());
		}
		return null;
	}

	public String currency(CMoneda currency) {
		val entity = iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(currency.value());
		if (!UValidator.isNullOrEmpty(entity)) {
			return String.format(STRING_FORMAT, entity.getCodigo(), entity.getValor());
		}
		return null;
	}

	public String paymentMethod(CMetodoPago paymentMethod) {
		val entity = iPaymentMethodJpaRepository.findByCodigoAndEliminadoFalse(paymentMethod.value());
		if (!UValidator.isNullOrEmpty(entity)) {
			return String.format(STRING_FORMAT, entity.getCodigo(), entity.getValor());
		}
		return null;
	}

	public String stringType(String code) {
		val entity = iStringTypeJpaRepository.findByCodigoAndEliminadoFalse(code);
		if (!UValidator.isNullOrEmpty(entity)) {
			return String.format(STRING_FORMAT, entity.getCodigo(), entity.getValor());
		}
		return null;
	}
	
}