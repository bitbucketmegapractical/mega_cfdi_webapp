package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.payload;

import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.megapractical.invoicing.webapp.json.JError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoucherTypeValidatorResponse {
	private TipoComprobanteEntity voucherType;
	private JError error;
	private boolean hasError;
	
	public VoucherTypeValidatorResponse(TipoComprobanteEntity voucherType) {
		this.voucherType = voucherType;
		this.hasError = Boolean.FALSE;
	}
	
	public VoucherTypeValidatorResponse(TipoComprobanteEntity voucherType, JError error) {
		this.voucherType = voucherType;
		this.error = error;
		this.hasError = error != null;
	}
	
	public VoucherTypeValidatorResponse(TipoComprobanteEntity voucherType, boolean hasError) {
		this.voucherType = voucherType;
		this.hasError = hasError;
	}
	
}