package org.megapractical.invoicing.webapp.exposition.service;

import static java.util.stream.Collectors.toList;

import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction.PayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception;
import org.megapractical.invoicing.dal.data.repository.IBankJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICfdiUseJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IContractJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ICurrencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IDayTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IDeductionTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IFederalEntityJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IJobRiskJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IOtherPaymentTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentFrequencyJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPaymentMethodJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPayrollTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPerceptionTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IPostalCodeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IRegimeTypeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IResourceOriginJpaRepository;
import org.megapractical.invoicing.dal.data.repository.ITaxRegimeJpaRepository;
import org.megapractical.invoicing.dal.data.repository.IVoucherTypeJpaRepository;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@RequiredArgsConstructor
public class PdfFromXmlUpdaterServiceImpl implements PdfFromXmlUpdaterService {

	private final ITaxRegimeJpaRepository iTaxRegimeJpaRepository;
	private final IResourceOriginJpaRepository iResourceOriginJpaRepository;
	private final IDayTypeJpaRepository iDayTypeJpaRepository;
	private final IRegimeTypeJpaRepository iRegimeTypeJpaRepository;
	private final IContractJpaRepository iContractJpaRepository;
	private final IBankJpaRepository iBankJpaRepository;
	private final IJobRiskJpaRepository iJobRiskJpaRepository;
	private final IFederalEntityJpaRepository iFederalEntityJpaRepository;
	private final IPaymentFrequencyJpaRepository iPaymentFrequencyJpaRepository;
	private final ICfdiUseJpaRepository iCfdiUseJpaRepository;
	private final IPayrollTypeJpaRepository iPayrollTypeJpaRepository;
	private final IVoucherTypeJpaRepository iVoucherTypeJpaRepository;
	private final ICurrencyJpaRepository iCurrencyJpaRepository;
	private final IPaymentMethodJpaRepository iPaymentMethodJpaRepositoy;
	private final IPostalCodeJpaRepository iPostalCodeJpaRepository;
	private final IPerceptionTypeJpaRepository iPerceptionTypeJpaRepository;
	private final IDeductionTypeJpaRepository iDeductionTypeJpaRepository;
	private final IOtherPaymentTypeJpaRepository iOtherPaymentTypeJpaRepository;

	@Override
	public void updateData(ApiCfdi cfdi) {
		updateTaxRegime(cfdi);
		updateReceiverTaxRegime(cfdi);
		updateUsoCfdi(cfdi);
		updateVoucherType(cfdi);
		updateCurrency(cfdi);
		updatePaymentMethod(cfdi);
		updatePostalCode(cfdi);
	}
	
	@Override
	public void updateData(final ApiPayroll payroll) {
		updateTaxRegime(payroll.getApiCfdi());
		updateResourceOrigin(payroll);
		updateDayType(payroll);
		updateRegimeType(payroll);
		updateContractType(payroll);
		updateBank(payroll);
		updateJobRisk(payroll);
		updateFederalEntity(payroll);
		updatePaymentFrequency(payroll);
		updateAge(payroll);
		updateUsoCfdi(payroll.getApiCfdi());
		updatePayrollType(payroll);
		updateVoucherType(payroll.getApiCfdi());
		updateCurrency(payroll.getApiCfdi());
		updatePaymentMethod(payroll.getApiCfdi());
		updatePostalCode(payroll.getApiCfdi());
		updatePayrollPerceptions(payroll);
		updatePayrollDeductions(payroll);
		updateOtherPayments(payroll);
	}

	private void updateTaxRegime(final ApiCfdi cfdi) {
		val taxRegime = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalse(cfdi.getEmitter().getTaxRegimeCode());
		cfdi.getEmitter().setRegimenFiscal(taxRegime);
	}
	
	private void updateReceiverTaxRegime(final ApiCfdi cfdi) {
		val taxRegime = iTaxRegimeJpaRepository.findByCodigoAndEliminadoFalse(cfdi.getReceiver().getTaxRegime());
		cfdi.getReceiver().setTaxRegimeEntity(taxRegime);
	}

	private void updateResourceOrigin(final ApiPayroll payroll) {
		val resourceOriginCode = payroll.getPayrollEmitter().getResourceOriginCatalog().value();
		if (resourceOriginCode != null) {
			val resourceOrigin = iResourceOriginJpaRepository.findByCodigoAndEliminadoFalse(resourceOriginCode);
			payroll.getPayrollEmitter().setResourceOrigin(resourceOrigin);
			payroll.getPayrollEmitter().setResourceOriginCode(resourceOrigin.getCodigo());
			payroll.getPayrollEmitter().setResourceOriginValue(resourceOrigin.getValor());
		}
	}

	private void updateDayType(final ApiPayroll payroll) {
		val dayTypeCode = payroll.getPayrollReceiver().getDayTypeCode();
		if (dayTypeCode != null) {
			val dayType = iDayTypeJpaRepository.findByCodigoAndEliminadoFalse(dayTypeCode);
			payroll.getPayrollReceiver().setDayType(dayType);
			payroll.getPayrollReceiver().setDayTypeCode(dayType.getCodigo());
			payroll.getPayrollReceiver().setDayTypeValue(dayType.getValor());
		}
	}

	private void updateRegimeType(final ApiPayroll payroll) {
		val regimeTypeCode = payroll.getPayrollReceiver().getRegimeTypeCode();
		if (regimeTypeCode != null) {
			val regimeType = iRegimeTypeJpaRepository.findByCodigoAndEliminadoFalse(regimeTypeCode);
			payroll.getPayrollReceiver().setRegimeType(regimeType);
			payroll.getPayrollReceiver().setRegimeTypeCode(regimeType.getCodigo());
			payroll.getPayrollReceiver().setRegimeTypeValue(regimeType.getValor());
		}
	}

	private void updateContractType(final ApiPayroll payroll) {
		val contractTypeCode = payroll.getPayrollReceiver().getContractTypeCode();
		if (contractTypeCode != null) {
			val contractType = iContractJpaRepository.findByCodigoAndEliminadoFalse(contractTypeCode);
			payroll.getPayrollReceiver().setContractType(contractType);
			payroll.getPayrollReceiver().setContractTypeCode(contractType.getCodigo());
			payroll.getPayrollReceiver().setContractTypeValue(contractType.getValor());
		}
	}

	private void updateBank(final ApiPayroll payroll) {
		val bankCode = payroll.getPayrollReceiver().getBankCode();
		if (bankCode != null) {
			val bank = iBankJpaRepository.findByCodigoAndEliminadoFalse(bankCode);
			payroll.getPayrollReceiver().setBank(bank);
			payroll.getPayrollReceiver().setBankCode(bank.getCodigo());
			payroll.getPayrollReceiver().setBankValue(bank.getValor());
		}
	}

	private void updateJobRisk(ApiPayroll payroll) {
		val jobRiskCode = payroll.getPayrollReceiver().getJobRiskCode();
		if (jobRiskCode != null) {
			val jobRisk = iJobRiskJpaRepository.findByCodigoAndEliminadoFalse(jobRiskCode);
			payroll.getPayrollReceiver().setJobRisk(jobRisk);
			payroll.getPayrollReceiver().setJobRiskCode(jobRisk.getCodigo());
			payroll.getPayrollReceiver().setJobRiskValue(jobRisk.getValor());
		}
	}

	private void updateFederalEntity(ApiPayroll payroll) {
		val federalEntityCode = payroll.getPayrollReceiver().getFederalEntityCatalog().value();
		val federalEntity = iFederalEntityJpaRepository.findByCodigoAndEliminadoFalse(federalEntityCode);
		payroll.getPayrollReceiver().setFederalEntityCode(federalEntity.getCodigo());
		payroll.getPayrollReceiver().setFederalEntityValue(federalEntity.getValor());
	}

	private void updatePaymentFrequency(ApiPayroll payroll) {
		val paymentFrequencyCode = payroll.getPayrollReceiver().getPaymentFrequencyCode();
		val paymentFrequency = iPaymentFrequencyJpaRepository.findByCodigoAndEliminadoFalse(paymentFrequencyCode);
		payroll.getPayrollReceiver().setPaymentFrequencyValue(paymentFrequency.getValor());
	}

	private void updateAge(ApiPayroll payroll) {
		payroll.getPayrollReceiver().setAge(payroll.getPayrollReceiver().getAgeDescription());
	}

	private void updateUsoCfdi(ApiCfdi cfdi) {
		val cfdiUseCode = cfdi.getReceiver().getUsoCfdiCatalog().value();
		val cfdiUse = iCfdiUseJpaRepository.findByCodigoAndEliminadoFalse(cfdiUseCode);
		cfdi.getReceiver().setUsoCFDI(cfdiUse);
	}

	private void updatePayrollType(ApiPayroll payroll) {
		val payrollTypeCode = payroll.getPayrollTypeCatalog().value();
		val payrollType = iPayrollTypeJpaRepository.findByCodigoAndEliminadoFalse(payrollTypeCode);
		payroll.setPayrollType(payrollType);
		payroll.setPayrollTypeCode(payrollType.getCodigo());
		payroll.setPayrollTypeValue(payrollType.getValor());
	}

	private void updateVoucherType(ApiCfdi cfdi) {
		val voucherTypeCode = cfdi.getVoucherTypeCatalog().value();
		val voucherType = iVoucherTypeJpaRepository.findByCodigoAndEliminadoFalse(voucherTypeCode);
		cfdi.setVoucherType(voucherType);
	}

	private void updateCurrency(ApiCfdi cfdi) {
		val currencyCode = cfdi.getCurrencyCatalog().value();
		val currency = iCurrencyJpaRepository.findByCodigoAndEliminadoFalse(currencyCode);
		cfdi.setCurrency(currency);
	}

	private void updatePaymentMethod(ApiCfdi cfdi) {
		val paymentMethodCode = cfdi.getPaymentMethodCatalog().value();
		val paymentMethod = iPaymentMethodJpaRepositoy.findByCodigoAndEliminadoFalse(paymentMethodCode);
		cfdi.setPaymentMethod(paymentMethod);
	}

	private void updatePostalCode(ApiCfdi cfdi) {
		val expeditionPlace = cfdi.getExpeditionPlace();
		val postalCode = iPostalCodeJpaRepository.findByCodigoAndEliminadoFalse(expeditionPlace);
		cfdi.setPostalCode(postalCode);
	}

	private void updatePayrollPerceptions(ApiPayroll payroll) {
		payroll.getPayrollPerception().getPayrollPerceptions().stream().map(this::updatePerception).collect(toList());
	}

	private PayrollPerception updatePerception(PayrollPerception perception) {
		val perceptionType = iPerceptionTypeJpaRepository
				.findByCodigoAndEliminadoFalse(perception.getPerceptionTypeCode());
		perception.setPerceptionTypeValue(perceptionType.getValor());
		return perception;
	}

	private void updatePayrollDeductions(ApiPayroll payroll) {
		payroll.getPayrollDeduction().getPayrollDeductions().stream().map(this::updateDeduction).collect(toList());
	}

	private PayrollDeduction updateDeduction(PayrollDeduction deduction) {
		val deductionType = iDeductionTypeJpaRepository.findByCodigoAndEliminadoFalse(deduction.getDeductionTypeCode());
		deduction.setDeductionTypeValue(deductionType.getValor());
		return deduction;
	}

	private void updateOtherPayments(ApiPayroll payroll) {
		payroll.getPayrollOtherPayments().stream().map(this::updateOtherPayment).collect(toList());
	}

	private ApiPayrollOtherPayment updateOtherPayment(ApiPayrollOtherPayment otherPayment) {
		val otherPaymentType = iOtherPaymentTypeJpaRepository
				.findByCodigoAndEliminadoFalse(otherPayment.getOtherPaymentTypeCode());
		otherPayment.setOtherPaymentTypeValue(otherPaymentType.getValor());
		return otherPayment;
	}

}