package org.megapractical.invoicing.webapp.exposition.common;

import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.megapractical.invoicing.webapp.exposition.invoicing.validator.voucher.VoucherCurrencyVoucherTypeValidator;
import org.megapractical.invoicing.webapp.exposition.service.StampPackageService;
import org.megapractical.invoicing.webapp.gson.GsonClient;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JMap;
import org.megapractical.invoicing.webapp.json.JResponse;
import org.megapractical.invoicing.webapp.json.JVoucher;
import org.megapractical.invoicing.webapp.security.SessionController;
import org.megapractical.invoicing.webapp.support.UserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.val;

@Controller
@Scope("session")
public class CommonController {
	
	@Autowired
	private UserTools userTools;
	
	@Autowired
	private SessionController sessionController;
	
	@Autowired
	private VoucherCurrencyVoucherTypeValidator voucherCurrencyVoucherTypeValidator;
	
	@Autowired
	private StampPackageService stampPackageService;
	
	private String locale;
	
	@PostConstruct
	public void init() {
		locale = userTools.getLocale();
	}
	
	@PostMapping(value="/validateCurrencyVoucherType")
	public @ResponseBody String validateCurrencyVoucherType(HttpServletRequest request) {
		val currency = request.getParameter("currency");
		val voucherType = request.getParameter("voucherType");
		val response = voucherCurrencyVoucherTypeValidator.validateCurrencyVoucherType(currency, voucherType);
		
		val voucher = new JVoucher(response);
		return GsonClient.response(voucher);
	}
	
	@PostMapping("/loadStampPackageInfo")
	public @ResponseBody String loadStampPackageInfo(HttpServletRequest request) {
		val stampPackageJson = stampPackageService.stampPackageInfo();
		sessionController.sessionUpdate();
		return GsonClient.response(stampPackageJson);
	}

	@PostMapping("/modalConfirmationRequest")
	public @ResponseBody String modalConfirmation() {
		val maps = Arrays.asList(
			JMap.encoded("related.cfdi.uuid", "mega.cfdi.confirmation.voucher.related.cfdi.uuid.delete", locale),
			JMap.encoded("concept", "mega.cfdi.confirmation.concept.delete", locale),
			JMap.encoded("customs.information.number", "mega.cfdi.confirmation.concept.customs.information.number.delete", locale),
			JMap.encoded("tax.transferred", "mega.cfdi.confirmation.concept.tax.transferred.delete", locale),
			JMap.encoded("tax.withheld", "mega.cfdi.confirmation.concept.tax.withheld.delete", locale),
			JMap.encoded("account", "mega.cfdi.confirmation.account.delete", locale),
			JMap.encoded("customer", "mega.cfdi.confirmation.customer.delete", locale),
			JMap.encoded("contact", "mega.cfdi.confirmation.contact.delete", locale),
			JMap.encoded("serie.sheet", "mega.cfdi.confirmation.serie.sheet.delete", locale),
			JMap.encoded("cfdi", "mega.cfdi.confirmation.cfdi.cancel", locale),
			JMap.encoded("complement.payment", "mega.cfdi.confirmation.complement.payment.delete", locale),
			JMap.encoded("emitter.logo", "mega.cfdi.confirmation.emiter.logo.delete", locale)
		);
		
		val response = JResponse.maps(maps);
		return GsonClient.response(response);
	}
	
	@PostMapping("/fieldErrorRequest")
	public @ResponseBody String fieldError() {
		val taxTransWithFactorTypeField = "tax-transferred-withheld-factor-type";
		val taxTransWithRateFeeField = "tax-transferred-withheld-rate-fee";
		val paymentWayField = "payment-way";
		val lengthErrorCode = "ERR1016";
		val catalogErrorCode = "ERR1002";
		val taxValues = new String[]{"CFDI33155", "Impuesto", "c_Impuesto"};
		val taxWithheldValues = new String[]{"CFDI33164", "Impuesto", "c_Impuesto"};
		val factorTypeValues = new String[]{"CFDI33156", "TipoFactor", "c_TipoFactor"};
		val factorTypeWithheldValues = new String[]{"CFDI33165", "TipoFactor", "c_TipoFactor"};
		
		val errors = Arrays.asList(
			JError.errorEncoded("ERR1006", "ERR1006", locale),
			JError.errorSourceEncoded("confirmation-code", "CFDI33119", "CFDI33119", locale),
			JError.errorEncoded("confirmation-code", "ERR0044", "ERR0044", locale),
			JError.errorSourceEncoded("base", "CFDI33154", "CFDI33154", locale),
			JError.invalidCatalogEncoded("tax", catalogErrorCode, "CFDI33155", taxValues, locale),
			JError.invalidCatalogEncoded("factorType", catalogErrorCode, "CFDI33156", factorTypeValues, locale),
			JError.errorSourceEncoded(taxTransWithFactorTypeField, "CFDI33157", "CFDI33157", locale),
			JError.errorSourceEncoded(taxTransWithFactorTypeField, "CFDI33158", "CFDI33158", locale),
			JError.errorSourceEncoded(taxTransWithRateFeeField, "CFDI33159", "CFDI33159", locale),
			JError.errorSourceEncoded("amount", "CFDI33161", "CFDI33161", locale),
			JError.errorSourceEncoded("base", "CFDI33163", "CFDI33163", locale),
			JError.invalidCatalogEncoded("tax", catalogErrorCode, "CFDI33164", taxWithheldValues, locale),
			JError.invalidCatalogEncoded(taxTransWithFactorTypeField, catalogErrorCode, "CFDI33165", factorTypeWithheldValues, locale),
			JError.errorSourceEncoded(taxTransWithRateFeeField, "CFDI33166", "CFDI33166", locale),
			JError.errorSourceEncoded(taxTransWithRateFeeField, "CFDI33167", "CFDI33167", locale),
			JError.errorSourceEncoded("amount", "CFDI33169", "CFDI33169", locale),
			JError.errorEncoded("customs-information-number", "ERR0043", "ERR0043", locale),
			JError.errorSourceEncoded("customs-information-number", "CFDI33170", "CFDI33170", locale),
			JError.errorEncoded("cfdi-related-uuid", "ERR0053", "ERR0053", locale),
			JError.errorEncoded("relationship-type", "ERR0112", "ERR0112", locale),
			JError.errorEncoded("ERR1003", "ERR1003", locale),
			JError.errorEncoded("cmp-payment-related-document-id", "ERR0139", "ERR0139", locale),
			JError.errorEncoded("cmp-payment-related-document-amount-paid", "ERR0141", "ERR0141", locale),
			JError.errorEncoded("ERR0142", "ERR0142", locale),
			JError.errorEncoded("ERR0144", "ERR0144", locale),
			JError.errorEncoded("ERR0145", "ERR0145", locale),
			JError.errorEncoded("ERR0147", "ERR0147", locale),
			JError.errorEncoded("cfdi-use", "ERR0150", "ERR0150", locale),
			JError.errorEncoded("voucher-type", "ERR0151", "ERR0151", locale),
			JError.errorEncoded("voucher-type", "ERR0158", "ERR0158", locale),
			JError.errorEncoded("currency", "ERR0152", "ERR0152", locale),
			JError.errorEncoded("relationship-type", "ERR0153", "ERR0153", locale),
			JError.errorEncoded(paymentWayField, "ERR0159", "ERR0159", locale),
			JError.errorEncoded(paymentWayField, "ERR0186", "ERR0186", locale),
			JError.errorEncoded(paymentWayField, "ERR0187", "ERR0187", locale),
			JError.errorEncoded("payment-method", "ERR0160", "ERR0160", locale),
			JError.invalidLengthEncoded("unit", "ERR1014", "ERR1014", "Unidad", 20, locale),
			JError.invalidLengthEncoded("name-business-name", lengthErrorCode, lengthErrorCode, "Nombre o razón social", 1, 254, locale),
			JError.invalidLengthEncoded("rfc", lengthErrorCode, "ERR1017", "Rfc", 12, 13, locale),
			JError.errorEncoded("email", "ERR1018", "ERR1018", locale),
			JError.invalidLengthEncoded("identity-registration-number", lengthErrorCode, lengthErrorCode, "Número registro identidad fiscal", 1, 40, locale)
		);
		
		val response = JResponse.error(errors);
		return GsonClient.response(response);
	}
	
}