package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.io.File;

import org.megapractical.invoicing.api.util.UFile;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoCancelacionEntity;
import org.megapractical.invoicing.webapp.exposition.invoicing.file.payload.UploadFilePath;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
final class CancellationHelper {
	
	private CancellationHelper() {		
	}
	
	public static String createAcusesDirectory(ArchivoCancelacionEntity cancellationFile, UploadFilePath uploadFilePath) {
		val folderAbsolutePath = uploadFilePath.getAbsolutePath(); 
		val cancellationFileName = cancellationFile.getNombre();
		val acusesPath = folderAbsolutePath + File.separator + cancellationFileName;
		UFile.createDirectory(acusesPath);
		return acusesPath;
	}
	
}