package org.megapractical.invoicing.webapp.exposition.invoicing.catalog.service;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;

public interface CurrencyService {
	
	MonedaEntity findByCode(String code);
	
	MonedaEntity findByValue(String value);
	
}