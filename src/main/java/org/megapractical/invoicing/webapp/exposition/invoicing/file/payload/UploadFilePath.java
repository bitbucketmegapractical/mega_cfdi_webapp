package org.megapractical.invoicing.webapp.exposition.invoicing.file.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UploadFilePath {
	// Ruta absoluta del archivo
	private String absolutePath;
	// Ruta fisica del archivo
	private String uploadPath;
	// Ruta para db
	private String dbPath;
}