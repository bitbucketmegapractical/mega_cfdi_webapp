package org.megapractical.invoicing.webapp.exposition.invoicing.service;

import org.megapractical.invoicing.webapp.exposition.invoicing.validator.tax.payload.TaxRequest;
import org.megapractical.invoicing.webapp.json.JTaxes;

public interface TaxService {
	
	JTaxes validateTax(TaxRequest request);
	
}