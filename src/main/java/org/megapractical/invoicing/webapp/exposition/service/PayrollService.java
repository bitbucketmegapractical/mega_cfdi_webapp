package org.megapractical.invoicing.webapp.exposition.service;

import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.dal.bean.jpa.NominaEntity;
import org.megapractical.invoicing.webapp.exposition.cfdi.payload.AllowStamp;
import org.megapractical.invoicing.webapp.json.JPayroll;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface PayrollService {
	
	boolean isAllowedToStamp(String curp, String serie, String folio);
	
	AllowStamp verifyPayroll(String curp, String serie, String folio);
	
	NominaEntity save(ApiPayroll apiPayroll);
	
	NominaEntity save(NominaEntity payroll);
	
	public JPayroll payrollList(String emitterRfc, String pageStr, int paginateSize, String searchBy, String date);
	
	NominaEntity findByUuid(String uuid);
	
	NominaEntity findForCancellation(String emitterRfc, String uuid);
	
}