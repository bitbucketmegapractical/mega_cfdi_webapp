package org.megapractical.invoicing.webapp.exposition.invoicing.validator.receiver;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.catalog.validator.PostalCodeValidator;
import org.megapractical.invoicing.webapp.exposition.invoicing.payload.PostalCodeResponse;
import org.megapractical.invoicing.webapp.json.JError;
import org.megapractical.invoicing.webapp.json.JReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.var;

@Service
public class ReceiverFiscalResidenceValidatorImpl implements ReceiverFiscalResidenceValidator {
	
	@Autowired
	private PostalCodeValidator postalCodeValidator;
	
	@Override
	public PostalCodeResponse getFiscalResidence(JReceiver receiver, List<JError> errors) {
		var postalCode = UBase64.base64Decode(receiver.getPostalCode());
		val validatorResponse = postalCodeValidator.validate(postalCode, "postal-code");
		val postalCodeEntity = validatorResponse.getPostalCode();
		val error = validatorResponse.getError();
		
		var expeditionPlaceError = false;
		if (!UValidator.isNullOrEmpty(error)) {
			errors.add(error);
			expeditionPlaceError = true;
		}
		
		return new PostalCodeResponse(postalCodeEntity, expeditionPlaceError);
	}

}