package org.megapractical.invoicing.webapp.exposition.invoicing.payload;

import java.math.BigDecimal;

import org.megapractical.invoicing.api.common.ApiVoucherTaxes;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoucherTotalsRequest {
	private BigDecimal voucherSubtotal;
	private BigDecimal voucherTotalsSubtotal;
	private BigDecimal voucherTotalsDiscount;
	private BigDecimal voucherTotalsTotal;
	private BigDecimal discount;
	private ApiVoucherTaxes conceptsTax;
	private MonedaEntity currency;
	private String voucherType;
	private boolean discountByConcept;
}