package org.megapractical.invoicing.webapp.exposition.cancellation;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoCancelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.data.repository.ICancellationFileJpaRepository;
import org.megapractical.invoicing.webapp.util.FileStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class CancellationFileService {

	private final ICancellationFileJpaRepository cancellationFileJpaRepository;

	public ArchivoCancelacionEntity findOne(Long id, FileStatus status) {
		return cancellationFileJpaRepository
				.findByIdAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalse(id, status.value()).orElse(null);
	}

	public List<ArchivoCancelacionEntity> findQueuedFiles(ContribuyenteEntity taxpayer) {
		return cancellationFileJpaRepository
				.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdAsc(taxpayer,
						FileStatus.QUEUED.value());
	}

	public List<ArchivoCancelacionEntity> findByTaxpayerAndStatus(ContribuyenteEntity taxpayer, FileStatus status) {
		return cancellationFileJpaRepository
				.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(taxpayer,
						status.value());
	}

	public Page<ArchivoCancelacionEntity> getFiles(ContribuyenteEntity taxpayer, String name, FileStatus status,
			String fileType, Pageable pageable) {
		return cancellationFileJpaRepository
				.findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivo_CodigoAndTipoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(
						taxpayer, name, status.value(), fileType, pageable);
	}

	public List<ArchivoCancelacionEntity> getFiles(ContribuyenteEntity taxpayer, FileStatus status, String fileType) {
		return cancellationFileJpaRepository
				.findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndTipoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(
						taxpayer, status.value(), fileType);
	}

	public ArchivoCancelacionEntity findByTaxpayerAndName(ContribuyenteEntity taxpayer, String fileName) {
		return cancellationFileJpaRepository.findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(taxpayer,
				fileName);
	}

	@Transactional
	public ArchivoCancelacionEntity save(ArchivoCancelacionEntity entity) {
		if (entity != null) {
			return cancellationFileJpaRepository.save(entity);
		}
		return null;
	}

	@Transactional
	public void delete(Long id) {
		if (id != null) {
			cancellationFileJpaRepository.deleteById(id);
		}
	}

}