package org.megapractical.invoicing.webapp.payload;

import org.megapractical.invoicing.api.wrapper.ApiPayrollParser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PayrollParserResponse {
	private ApiPayrollParser payrollParser;
}