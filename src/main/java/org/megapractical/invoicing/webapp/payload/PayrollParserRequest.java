package org.megapractical.invoicing.webapp.payload;

import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiPayrollFileStatistics;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PayrollParserRequest extends PayrollParserResponse {
	private ApiPayrollFileStatistics statistics;
	private List<String[]> entries;
}