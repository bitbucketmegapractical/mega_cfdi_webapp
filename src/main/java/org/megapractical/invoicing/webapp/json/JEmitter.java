package org.megapractical.invoicing.webapp.json;

import java.io.File;
import java.util.Date;
import java.util.Optional;

import org.megapractical.invoicing.api.wrapper.ApiEmitter;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JEmitter {
	private String index;
	private String rfc;
	private String nameOrBusinessName;
	private String email;
	private String personType;
	private String taxRegime;
	private String passwd;
	private String certificateNumber;
	private String certifaicateCreationDate;
	private String certificateValidDate;
	private String phone;
	private JResponse response;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor	
	public static class JEmitterResponse {
		private ApiEmitter apiEmitter;
		private boolean error;
		
		public JEmitterResponse(ApiEmitter apiEmitter) {
			this.apiEmitter = apiEmitter;
			this.error = Boolean.FALSE;
		}
		
		public static final JEmitterResponse empty() {
			return JEmitterResponse.builder().error(Boolean.TRUE).build();
		}
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class JEmitterCertificateRequest {
		private ContribuyenteCertificadoEntity emitterCertificate;
		private Optional<MultipartFile> certificateMultipart;
		private Optional<MultipartFile> privateKeyMultipart;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor	
	public static class JEmitterCertificateValidationResponse {
		private JEmitterCertificateDataResponse emitterCertificate;
		private JResponse response;
		private boolean error;
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor	
		public static class JEmitterCertificateFileResponse {
			private File file;
			private boolean error;
		}
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor	
		public static class JEmitterCertificateDatesResponse {
			private Date createdAt;
			private Date expiredOn;
			private JError error;
			private JResponse response;
			
			public static final JEmitterCertificateDatesResponse empty() {
				return JEmitterCertificateDatesResponse.builder().build();
			}
		}
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor	
		public static class JEmitterCertificateDataResponse {
			private File certificate;
			private byte[] certificateByte;
			private File privateKey;
			private byte[] privateKeyByte;
			private String keyPasswd;
			private Date createdAt;
			private Date expiredOn;
			private boolean error;
		}
	}
	
}