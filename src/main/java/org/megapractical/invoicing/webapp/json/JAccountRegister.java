package org.megapractical.invoicing.webapp.json;

public class JAccountRegister {

	private String nameOrBusinessName;
	private String rfc;
	private String email;
	private String passwd;
	private String passwdRepeat;
	private String phone;
	private JResponse response;
	
	public JAccountRegister(){
		super();
	}

	/*Getters and Setters*/
	public String getNameOrBusinessName() {
		return nameOrBusinessName;
	}

	public void setNameOrBusinessName(String nameOrBusinessName) {
		this.nameOrBusinessName = nameOrBusinessName;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public String getPasswdRepeat() {
		return passwdRepeat;
	}

	public void setPasswdRepeat(String passwdRepeat) {
		this.passwdRepeat = passwdRepeat;
	}	
}