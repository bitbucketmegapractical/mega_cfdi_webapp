package org.megapractical.invoicing.webapp.json;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.webapp.util.UInvalidMessage;
import org.megapractical.invoicing.webapp.util.UProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JError {
	private String field;
	private String message;
	private String code;
	
	public static final String REQUIRED_FIELD = "ERR1001";
	public static final String INVALID_VALUE = "ERR1004";
	
	public static JError required(String field, String locale) {
		val requiredMessage = UProperties.getMessage(REQUIRED_FIELD, locale);
		return JError.builder().field(field).message(requiredMessage).build();
	}
	
	public static JError invalidValue(String field, String locale) {
		val invalidValueMessage = UProperties.getMessage(INVALID_VALUE, locale);
		return JError.builder().field(field).message(invalidValueMessage).build();
	}
	
	public static JError error(String field, String message) {
		return JError.builder().field(field).message(message).build();
	}
	
	public static JError error(String field, String messageCode, String locale) {
		val message = UProperties.getMessage(messageCode, locale);
		return JError.builder().field(field).message(message).build();
	}
	
	public static JError errorSource(String field, String messageCode, String locale) {
		val errorSource = Validator.ErrorSource.fromValue(messageCode).value();
		val message = UProperties.getMessage(errorSource, locale);
		return JError.builder().field(field).message(message).build();
	}
	
	public static JError message(String messageCode, String locale) {
		val message = UProperties.getMessage(messageCode, locale);
		return JError.builder().message(message).build();
	}
	
	public static JError invalidCatalog(String field, String messageCode, String[] values, String locale) {
		val message = UInvalidMessage.satInvalidCatalogMessage(messageCode, locale, values);
		return JError.builder().field(field).message(message).build();
	}
	
	public static JError errorEncoded(String messageCode, String code, String locale) {
		val encodedMessage = UBase64.base64Encode(UProperties.getMessage(messageCode, locale));
		val encodedCode = UBase64.base64Encode(code);
		return JError.builder().message(encodedMessage).code(encodedCode).build();
	}

	public static JError errorEncoded(String field, String messageCode, String code, String locale) {
		val encodedField = UBase64.base64Encode(field);
		val encodedMessage = UBase64.base64Encode(UProperties.getMessage(messageCode, locale));
		val encodedCode = UBase64.base64Encode(code);
		return JError.builder().field(encodedField).message(encodedMessage).code(encodedCode).build();
	}
	
	public static JError errorSourceEncoded(String field, String messageCode, String code, String locale) {
		val encodedField = UBase64.base64Encode(field);
		val errorSource = Validator.ErrorSource.fromValue(messageCode).value();
		val encodedMessage = UBase64.base64Encode(UProperties.getMessage(errorSource, locale));
		val encodedCode = UBase64.base64Encode(code);
		return JError.builder().field(encodedField).message(encodedMessage).code(encodedCode).build();
	}
	
	public static JError invalidCatalogEncoded(String field, String messageCode, String code, String[] values, String locale) {
		val encodedField = UBase64.base64Encode(field);
		val encodedMessage = UBase64.base64Encode(UInvalidMessage.satInvalidCatalogMessage(messageCode, locale, values));
		val encodedCode = UBase64.base64Encode(code);
		return JError.builder().field(encodedField).message(encodedMessage).code(encodedCode).build();
	}
	
	public static JError invalidLengthEncoded(String field, String messageCode, String code, String name, Integer length, String locale) {
		val encodedField = UBase64.base64Encode(field);
		val encodedMessage = UBase64.base64Encode(UInvalidMessage.systemInvalidLength(UProperties.getMessage(messageCode, locale), name, length));
		val encodedCode = UBase64.base64Encode(code);
		return JError.builder().field(encodedField).message(encodedMessage).code(encodedCode).build();
	}
	
	public static JError invalidLengthEncoded(String field, String messageCode, String code, String name, Integer minLength, Integer maxLength, String locale) {
		val encodedField = UBase64.base64Encode(field);
		val encodedMessage = UBase64.base64Encode(UInvalidMessage.systemInvalidLength(UProperties.getMessage(messageCode, locale), name, minLength, maxLength));
		val encodedCode = UBase64.base64Encode(code);
		return JError.builder().field(encodedField).message(encodedMessage).code(encodedCode).build();
	}
	
}