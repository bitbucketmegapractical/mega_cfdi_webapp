package org.megapractical.invoicing.webapp.json;

import java.util.List;

public class JCfdiNotification {

	private CfdiNotification cfdiNotification;
	private JResponse response;
	
	public JCfdiNotification() {
		super();
	}
	
	public static class CfdiNotification{
		private String uuid;
		private String recipientTo;
		private List<String> recipients;
		
		/*Getters and Setters*/
		public String getUuid() {
			return uuid;
		}

		public void setUuid(String uuid) {
			this.uuid = uuid;
		}

		public List<String> getRecipients() {
			return recipients;
		}

		public void setRecipients(List<String> recipients) {
			this.recipients = recipients;
		}

		public String getRecipientTo() {
			return recipientTo;
		}

		public void setRecipientTo(String recipientTo) {
			this.recipientTo = recipientTo;
		}		
	}
	
	/*Getters and Setters*/
	public CfdiNotification getCfdiNotification() {
		return cfdiNotification;
	}

	public void setCfdiNotification(CfdiNotification cfdiNotification) {
		this.cfdiNotification = cfdiNotification;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}	
}