package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JConceptParser {

	private String totalConceptInFile;
	private String totalConceptRegistered;
	private String errorParserMessage;
	
	private List<JConcept> concepts;
	private List<ConceptParserError> conceptParserErrors;
	
	private JResponse response;
	
	public static class ConceptParserError {
		private String conceptDescription;
		private String errorMessage;

		public String getConceptDescription() {
			return conceptDescription;
		}

		public void setConceptDescription(String conceptDescription) {
			this.conceptDescription = conceptDescription;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}		
	}

	public String getTotalConceptInFile() {
		return totalConceptInFile;
	}

	public void setTotalConceptInFile(String totalConceptInFile) {
		this.totalConceptInFile = totalConceptInFile;
	}

	public String getTotalConceptRegistered() {
		return totalConceptRegistered;
	}

	public void setTotalConceptRegistered(String totalConceptRegistered) {
		this.totalConceptRegistered = totalConceptRegistered;
	}

	public List<JConcept> getConcepts() {
		if(concepts == null){
			concepts = new ArrayList<>();
		}
		return concepts;
	}

	public void setConcepts(List<JConcept> concepts) {
		this.concepts = concepts;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public String getErrorParserMessage() {
		return errorParserMessage;
	}

	public void setErrorParserMessage(String errorParserMessage) {
		this.errorParserMessage = errorParserMessage;
	}

	public List<ConceptParserError> getConceptParserErrors() {
		if(conceptParserErrors == null){
			conceptParserErrors = new ArrayList<>();
		}
		return conceptParserErrors;
	}

	public void setConceptParserErrors(List<ConceptParserError> conceptParserErrors) {
		this.conceptParserErrors = conceptParserErrors;
	}	
}