package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JRetentionFile {

	private RetentionFile retentionFile;
	private List<RetentionFile> queuedRetentionFiles;
	private List<RetentionFile> processingRetentionFiles;
	private List<RetentionFile> generatedRetentionFiles;
	private JResponse response;
	private JPagination pagination;
	
	public JRetentionFile(){
		super();
	}
	
	public static class RetentionFile {
		private String id;
		private String fileType;
		private String fileStatus;
		private String name;
		private String path;
		private String uploadDate;		
		private String period;
		private String year;
		private Boolean stampAvailable;
		private String compactedPath;
		private String errorPath;

		/*Getters and Setters*/
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getFileType() {
			return fileType;
		}

		public void setFileType(String fileType) {
			this.fileType = fileType;
		}

		public String getFileStatus() {
			return fileStatus;
		}

		public void setFileStatus(String fileStatus) {
			this.fileStatus = fileStatus;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public String getUploadDate() {
			return uploadDate;
		}

		public void setUploadDate(String uploadDate) {
			this.uploadDate = uploadDate;
		}

		public String getPeriod() {
			return period;
		}

		public void setPeriod(String period) {
			this.period = period;
		}

		public String getYear() {
			return year;
		}

		public void setYear(String year) {
			this.year = year;
		}

		public Boolean getStampAvailable() {
			return stampAvailable;
		}

		public void setStampAvailable(Boolean stampAvailable) {
			this.stampAvailable = stampAvailable;
		}

		public String getCompactedPath() {
			return compactedPath;
		}

		public void setCompactedPath(String compactedPath) {
			this.compactedPath = compactedPath;
		}

		public String getErrorPath() {
			return errorPath;
		}

		public void setErrorPath(String errorPath) {
			this.errorPath = errorPath;
		}
	}

	/*Getters and Setters*/
	public List<RetentionFile> getQueuedRetentionFiles() {
		if(queuedRetentionFiles == null)
			queuedRetentionFiles = new ArrayList<>();
		return queuedRetentionFiles;
	}

	public void setQueuedRetentionFiles(List<RetentionFile> queuedRetentionFiles) {
		this.queuedRetentionFiles = queuedRetentionFiles;
	}

	public List<RetentionFile> getProcessingRetentionFiles() {
		if(processingRetentionFiles == null)
			processingRetentionFiles = new ArrayList<>();
		return processingRetentionFiles;
	}

	public void setProcessingRetentionFiles(List<RetentionFile> processingRetentionFiles) {
		this.processingRetentionFiles = processingRetentionFiles;
	}

	public List<RetentionFile> getGeneratedRetentionFiles() {
		if(generatedRetentionFiles == null)
			generatedRetentionFiles = new ArrayList<>();		
		return generatedRetentionFiles;
	}

	public void setGeneratedRetentionFiles(List<RetentionFile> generatedRetentionFiles) {
		this.generatedRetentionFiles = generatedRetentionFiles;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}

	public RetentionFile getRetentionFile() {
		return retentionFile;
	}

	public void setRetentionFile(RetentionFile retentionFile) {
		this.retentionFile = retentionFile;
	}	
}