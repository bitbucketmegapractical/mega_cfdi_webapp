package org.megapractical.invoicing.webapp.json;

import java.util.List;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.NominaEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JPayroll {
	private List<Payroll> payrolls;
	private JResponse response;
	private JPagination<?> pagination;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Payroll {
		private String id;
		private String curp;
		private String uuid;
		private String serieFolio;
		private String stampDate;
		
		public static Payroll convert(NominaEntity entity) {
			if (entity == null) {
				return null;
			}
			
			return Payroll
					.builder()
					.id(UBase64.base64Encode(UValue.longString(entity.getId())))
					.curp(UBase64.base64Encode(entity.getCurp()))
					.uuid(UBase64.base64Encode(entity.getUuid()))
					.serieFolio(UBase64.base64Encode(entity.getSerie() + "-" + entity.getFolio()))
					.stampDate(UBase64.base64Encode(entity.getFechaHoraTimbrado()))
					.build();
		}
	}
	
}