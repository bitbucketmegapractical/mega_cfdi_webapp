package org.megapractical.invoicing.webapp.json;

public class JPreferences {

	private String currency;
	private Boolean sendToEmitterXmlAndPdf;
	private Boolean sendToEmitterOnlyXml;
	private Boolean sendToEmitterOnlyPdf;
	private Boolean sendToReceiverXmlAndPdf;
	private Boolean sendToReceiverOnlyXml;
	private Boolean sendToReceiverOnlyPdf;
	private Boolean sendToThirdPartiesXmlAndPdf;
	private Boolean sendToThirdPartiesOnlyXml;
	private Boolean sendToThirdPartiesOnlyPdf;
	private Boolean registerCustomer;
	private Boolean updateCustomer;
	private Boolean registerConcept;
	private Boolean updateConcept;
	private Boolean registerTaxRegime;
	private Boolean updateTaxRegime;
	private Boolean reuseSheetCanceled;
	private Boolean increaseEndSheet;
	private Boolean registerUpdatePostalCode;
	private Boolean preInvoicePdf;
	
	JResponse response;
	
	public JPreferences(){
		super();
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Boolean getSendToEmitterXmlAndPdf() {
		return sendToEmitterXmlAndPdf;
	}

	public void setSendToEmitterXmlAndPdf(Boolean sendToEmitterXmlAndPdf) {
		this.sendToEmitterXmlAndPdf = sendToEmitterXmlAndPdf;
	}

	public Boolean getSendToEmitterOnlyXml() {
		return sendToEmitterOnlyXml;
	}

	public void setSendToEmitterOnlyXml(Boolean sendToEmitterOnlyXml) {
		this.sendToEmitterOnlyXml = sendToEmitterOnlyXml;
	}

	public Boolean getSendToEmitterOnlyPdf() {
		return sendToEmitterOnlyPdf;
	}

	public void setSendToEmitterOnlyPdf(Boolean sendToEmitterOnlyPdf) {
		this.sendToEmitterOnlyPdf = sendToEmitterOnlyPdf;
	}

	public Boolean getSendToReceiverXmlAndPdf() {
		return sendToReceiverXmlAndPdf;
	}

	public void setSendToReceiverXmlAndPdf(Boolean sendToReceiverXmlAndPdf) {
		this.sendToReceiverXmlAndPdf = sendToReceiverXmlAndPdf;
	}

	public Boolean getSendToReceiverOnlyXml() {
		return sendToReceiverOnlyXml;
	}

	public void setSendToReceiverOnlyXml(Boolean sendToReceiverOnlyXml) {
		this.sendToReceiverOnlyXml = sendToReceiverOnlyXml;
	}

	public Boolean getSendToReceiverOnlyPdf() {
		return sendToReceiverOnlyPdf;
	}

	public void setSendToReceiverOnlyPdf(Boolean sendToReceiverOnlyPdf) {
		this.sendToReceiverOnlyPdf = sendToReceiverOnlyPdf;
	}

	public Boolean getSendToThirdPartiesXmlAndPdf() {
		return sendToThirdPartiesXmlAndPdf;
	}

	public void setSendToThirdPartiesXmlAndPdf(Boolean sendToThirdPartiesXmlAndPdf) {
		this.sendToThirdPartiesXmlAndPdf = sendToThirdPartiesXmlAndPdf;
	}

	public Boolean getSendToThirdPartiesOnlyXml() {
		return sendToThirdPartiesOnlyXml;
	}

	public void setSendToThirdPartiesOnlyXml(Boolean sendToThirdPartiesOnlyXml) {
		this.sendToThirdPartiesOnlyXml = sendToThirdPartiesOnlyXml;
	}

	public Boolean getSendToThirdPartiesOnlyPdf() {
		return sendToThirdPartiesOnlyPdf;
	}

	public void setSendToThirdPartiesOnlyPdf(Boolean sendToThirdPartiesOnlyPdf) {
		this.sendToThirdPartiesOnlyPdf = sendToThirdPartiesOnlyPdf;
	}

	public Boolean getRegisterCustomer() {
		return registerCustomer;
	}

	public void setRegisterCustomer(Boolean registerCustomer) {
		this.registerCustomer = registerCustomer;
	}

	public Boolean getUpdateCustomer() {
		return updateCustomer;
	}

	public void setUpdateCustomer(Boolean updateCustomer) {
		this.updateCustomer = updateCustomer;
	}

	public Boolean getRegisterConcept() {
		return registerConcept;
	}

	public void setRegisterConcept(Boolean registerConcept) {
		this.registerConcept = registerConcept;
	}

	public Boolean getUpdateConcept() {
		return updateConcept;
	}

	public void setUpdateConcept(Boolean updateConcept) {
		this.updateConcept = updateConcept;
	}

	public Boolean getRegisterTaxRegime() {
		return registerTaxRegime;
	}

	public void setRegisterTaxRegime(Boolean registerTaxRegime) {
		this.registerTaxRegime = registerTaxRegime;
	}

	public Boolean getUpdateTaxRegime() {
		return updateTaxRegime;
	}

	public void setUpdateTaxRegime(Boolean updateTaxRegime) {
		this.updateTaxRegime = updateTaxRegime;
	}

	public Boolean getReuseSheetCanceled() {
		return reuseSheetCanceled;
	}

	public void setReuseSheetCanceled(Boolean reuseSheetCanceled) {
		this.reuseSheetCanceled = reuseSheetCanceled;
	}

	public Boolean getIncreaseEndSheet() {
		return increaseEndSheet;
	}

	public void setIncreaseEndSheet(Boolean increaseEndSheet) {
		this.increaseEndSheet = increaseEndSheet;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public Boolean getRegisterUpdatePostalCode() {
		return registerUpdatePostalCode;
	}

	public void setRegisterUpdatePostalCode(Boolean registerUpdatePostalCode) {
		this.registerUpdatePostalCode = registerUpdatePostalCode;
	}

	public Boolean getPreInvoicePdf() {
		return preInvoicePdf;
	}

	public void setPreInvoicePdf(Boolean preInvoicePdf) {
		this.preInvoicePdf = preInvoicePdf;
	}	
}