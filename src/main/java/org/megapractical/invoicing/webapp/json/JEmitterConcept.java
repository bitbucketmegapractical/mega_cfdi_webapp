package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JEmitterConcept {

	private List<Concept> concepts;
	private JResponse response;
	private JPagination pagination;
	
	public JEmitterConcept(){
		super();
	}
	
	public static class Concept{
		private String id;
		private String description;
		private String measurementUnit;
		private String keyProductService;
		private String identificationNumber;
		private String unit;
		private String quantity;
		private String unitValue;
		private String discount;
		private String amount;
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getDescription() {
			return description;
		}
		
		public void setDescription(String description) {
			this.description = description;
		}

		public String getMeasurementUnit() {
			return measurementUnit;
		}

		public void setMeasurementUnit(String measurementUnit) {
			this.measurementUnit = measurementUnit;
		}

		public String getKeyProductService() {
			return keyProductService;
		}

		public void setKeyProductService(String keyProductService) {
			this.keyProductService = keyProductService;
		}

		public String getIdentificationNumber() {
			return identificationNumber;
		}

		public void setIdentificationNumber(String identificationNumber) {
			this.identificationNumber = identificationNumber;
		}

		public String getUnit() {
			return unit;
		}

		public void setUnit(String unit) {
			this.unit = unit;
		}

		public String getUnitValue() {
			return unitValue;
		}

		public void setUnitValue(String unitValue) {
			this.unitValue = unitValue;
		}

		public String getDiscount() {
			return discount;
		}

		public void setDiscount(String discount) {
			this.discount = discount;
		}

		public String getQuantity() {
			return quantity;
		}

		public void setQuantity(String quantity) {
			this.quantity = quantity;
		}

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}		
	}

	public List<Concept> getConcepts() {
		if(concepts == null)
			concepts = new ArrayList<>();
		return concepts;
	}

	public void setConcepts(List<Concept> concepts) {
		this.concepts = concepts;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}	
}