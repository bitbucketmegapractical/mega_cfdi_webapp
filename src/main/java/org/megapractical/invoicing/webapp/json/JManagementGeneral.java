package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JManagementGeneral {

	private String lastUpdate;
	private List<StatisticsMonth> statisticsMonths;
	private GeneralInfo generalInfo;
	private LineData lineData;
	private LineData lineData1;
	private JBitacora log;
	private JResponse response;
	
	public JManagementGeneral(){
		super();
	}
	
	public static class StatisticsMonth{
		private String monthName;
		private String monthNameAbbreviation;		
		private Integer month;

		public String getMonthName() {
			return monthName;
		}

		public void setMonthName(String monthName) {
			this.monthName = monthName;
		}

		public String getMonthNameAbbreviation() {
			return monthNameAbbreviation;
		}

		public void setMonthNameAbbreviation(String monthNameAbbreviation) {
			this.monthNameAbbreviation = monthNameAbbreviation;
		}

		public Integer getMonth() {
			return month;
		}

		public void setMonth(Integer month) {
			this.month = month;
		}		
	}
	
	public static class GeneralInfo{
		private String megaCfdiFreeRegisteredUsers;
		private String megaCfdiPlusRegisteredUsers;
		
		private String cfdiFreeVisitsToday;
		private String cfdiPlusVisitsToday;
		
		private String cfdiFreeRegisteredUsersToday;
		private String cfdiPlusRegisteredUsersToday;
		
		private String cfdiFreeGeneratedInvoicesToday;
		private String cfdiPlusGeneratedInvoicesToday;
		
		private String cfdiFreeCanceledInvoicesToday;
		private String cfdiPlusCanceledInvoicesToday;
		
		public String getMegaCfdiFreeRegisteredUsers() {
			return megaCfdiFreeRegisteredUsers;
		}

		public void setMegaCfdiFreeRegisteredUsers(String megaCfdiFreeRegisteredUsers) {
			this.megaCfdiFreeRegisteredUsers = megaCfdiFreeRegisteredUsers;
		}

		public String getMegaCfdiPlusRegisteredUsers() {
			return megaCfdiPlusRegisteredUsers;
		}

		public void setMegaCfdiPlusRegisteredUsers(String megaCfdiPlusRegisteredUsers) {
			this.megaCfdiPlusRegisteredUsers = megaCfdiPlusRegisteredUsers;
		}

		public String getCfdiFreeVisitsToday() {
			return cfdiFreeVisitsToday;
		}

		public void setCfdiFreeVisitsToday(String cfdiFreeVisitsToday) {
			this.cfdiFreeVisitsToday = cfdiFreeVisitsToday;
		}

		public String getCfdiPlusVisitsToday() {
			return cfdiPlusVisitsToday;
		}

		public void setCfdiPlusVisitsToday(String cfdiPlusVisitsToday) {
			this.cfdiPlusVisitsToday = cfdiPlusVisitsToday;
		}

		public String getCfdiFreeRegisteredUsersToday() {
			return cfdiFreeRegisteredUsersToday;
		}

		public void setCfdiFreeRegisteredUsersToday(String cfdiFreeRegisteredUsersToday) {
			this.cfdiFreeRegisteredUsersToday = cfdiFreeRegisteredUsersToday;
		}

		public String getCfdiPlusRegisteredUsersToday() {
			return cfdiPlusRegisteredUsersToday;
		}

		public void setCfdiPlusRegisteredUsersToday(String cfdiPlusRegisteredUsersToday) {
			this.cfdiPlusRegisteredUsersToday = cfdiPlusRegisteredUsersToday;
		}

		public String getCfdiFreeGeneratedInvoicesToday() {
			return cfdiFreeGeneratedInvoicesToday;
		}

		public void setCfdiFreeGeneratedInvoicesToday(String cfdiFreeGeneratedInvoicesToday) {
			this.cfdiFreeGeneratedInvoicesToday = cfdiFreeGeneratedInvoicesToday;
		}

		public String getCfdiPlusGeneratedInvoicesToday() {
			return cfdiPlusGeneratedInvoicesToday;
		}

		public void setCfdiPlusGeneratedInvoicesToday(String cfdiPlusGeneratedInvoicesToday) {
			this.cfdiPlusGeneratedInvoicesToday = cfdiPlusGeneratedInvoicesToday;
		}

		public String getCfdiFreeCanceledInvoicesToday() {
			return cfdiFreeCanceledInvoicesToday;
		}

		public void setCfdiFreeCanceledInvoicesToday(String cfdiFreeCanceledInvoicesToday) {
			this.cfdiFreeCanceledInvoicesToday = cfdiFreeCanceledInvoicesToday;
		}

		public String getCfdiPlusCanceledInvoicesToday() {
			return cfdiPlusCanceledInvoicesToday;
		}

		public void setCfdiPlusCanceledInvoicesToday(String cfdiPlusCanceledInvoicesToday) {
			this.cfdiPlusCanceledInvoicesToday = cfdiPlusCanceledInvoicesToday;
		}
	}
	
	public static class LineData{
		private List<String> months;
		private List<DataSet> dataSets;
		
		public static class DataSet{
			private String label;
			private String backgroundColor;
			private String borderColor;
			private String pointBackgroundColor;
			private String pointBorderColor;
			private List<String> data;

			public String getLabel() {
				return label;
			}

			public void setLabel(String label) {
				this.label = label;
			}

			public String getBackgroundColor() {
				return backgroundColor;
			}

			public void setBackgroundColor(String backgroundColor) {
				this.backgroundColor = backgroundColor;
			}

			public String getBorderColor() {
				return borderColor;
			}

			public void setBorderColor(String borderColor) {
				this.borderColor = borderColor;
			}

			public String getPointBackgroundColor() {
				return pointBackgroundColor;
			}

			public void setPointBackgroundColor(String pointBackgroundColor) {
				this.pointBackgroundColor = pointBackgroundColor;
			}

			public String getPointBorderColor() {
				return pointBorderColor;
			}

			public void setPointBorderColor(String pointBorderColor) {
				this.pointBorderColor = pointBorderColor;
			}

			public List<String> getData() {
				if(data == null)
					data = new ArrayList<>();
				return data;
			}

			public void setData(List<String> data) {
				this.data = data;
			}			
		}

		public List<String> getMonths() {
			if(months == null)
				months = new ArrayList<>();
			return months;
		}

		public void setMonths(List<String> months) {
			this.months = months;
		}

		public List<DataSet> getDataSets() {
			if(dataSets == null)
				dataSets = new ArrayList<>();
			return dataSets;
		}

		public void setDataSets(List<DataSet> dataSets) {
			this.dataSets = dataSets;
		}
	}
	
	public GeneralInfo getGeneralInfo() {
		return generalInfo;
	}

	public void setGeneralInfo(GeneralInfo generalInfo) {
		this.generalInfo = generalInfo;
	}

	public LineData getLineData() {
		return lineData;
	}

	public void setLineData(LineData lineData) {
		this.lineData = lineData;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JBitacora getLog() {
		return log;
	}

	public void setLog(JBitacora log) {
		this.log = log;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public LineData getLineData1() {
		return lineData1;
	}

	public void setLineData1(LineData lineData1) {
		this.lineData1 = lineData1;
	}

	public List<StatisticsMonth> getStatisticsMonths() {
		if(statisticsMonths == null)
			statisticsMonths = new ArrayList<>();
		return statisticsMonths;
	}

	public void setStatisticsMonths(List<StatisticsMonth> statisticsMonths) {
		this.statisticsMonths = statisticsMonths;
	}	
}