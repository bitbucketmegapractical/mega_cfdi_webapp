package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JSerieSheet {

	private SerieSheet serieSheet;
	private List<SerieSheet> serieSheets;
	private JResponse response;
	private JPagination pagination;
	
	public JSerieSheet(){
		super();
	}
	
	public static class SerieSheetOptions{
		private String id;
		private String value;
		private String actualSheet;
		
		public String getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}
		
		public String getValue() {
			return value;
		}
		
		public void setValue(String value) {
			this.value = value;
		}

		public String getActualSheet() {
			return actualSheet;
		}

		public void setActualSheet(String actualSheet) {
			this.actualSheet = actualSheet;
		}		
	}
	
	public static class SerieSheet{
		private String id;
		private String serie;
		private String initialSheet;
		private String endSheet;
		private String actualSheet;
		private String creationDate;
		private String lastUpdatedDate;
		private Boolean enableEdit;
		private String options;
		private List<SerieSheetOptions> serieSheetOptions;

		public String getSerie() {
			return serie;
		}

		public void setSerie(String serie) {
			this.serie = serie;
		}

		public String getInitialSheet() {
			return initialSheet;
		}

		public void setInitialSheet(String initialSheet) {
			this.initialSheet = initialSheet;
		}

		public String getEndSheet() {
			return endSheet;
		}

		public void setEndSheet(String endSheet) {
			this.endSheet = endSheet;
		}

		public String getActualSheet() {
			return actualSheet;
		}

		public void setActualSheet(String actualSheet) {
			this.actualSheet = actualSheet;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getLastUpdatedDate() {
			return lastUpdatedDate;
		}

		public void setLastUpdatedDate(String lastUpdatedDate) {
			this.lastUpdatedDate = lastUpdatedDate;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Boolean getEnableEdit() {
			return enableEdit;
		}

		public void setEnableEdit(Boolean enableEdit) {
			this.enableEdit = enableEdit;
		}

		public String getOptions() {
			return options;
		}

		public void setOptions(String options) {
			this.options = options;
		}

		public List<SerieSheetOptions> getSerieSheetOptions() {
			if(serieSheetOptions == null)
				serieSheetOptions = new ArrayList<>();
			return serieSheetOptions;
		}

		public void setSerieSheetOptions(List<SerieSheetOptions> serieSheetOptions) {
			this.serieSheetOptions = serieSheetOptions;
		}		
	}

	public SerieSheet getSerieSheet() {
		return serieSheet;
	}

	public void setSerieSheet(SerieSheet serieSheet) {
		this.serieSheet = serieSheet;
	}

	public List<SerieSheet> getSerieSheets() {
		if(serieSheets == null)
			serieSheets = new ArrayList<>();
		return serieSheets;
	}

	public void setSerieSheets(List<SerieSheet> serieSheets) {
		this.serieSheets = serieSheets;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}	
}