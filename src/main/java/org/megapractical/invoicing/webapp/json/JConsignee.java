package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JConsignee {

	private Consignee consignee;
	private List<Consignee> consignees;
	private JResponse response;
	private JPagination pagination;
	
	public JConsignee(){
		super();
	}
	
	public static class Consignee{
		private String id;
		private String idTaxpayer;
		private String contractStartDate;
		private String contractRenewalDate;
		private String contractEndDate;
		private String contractName;
		private boolean contractPeriodEnded;
		private boolean active;
		private List<ConsigneeContract> consigneeContracts;
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getIdTaxpayer() {
			return idTaxpayer;
		}

		public void setIdTaxpayer(String idTaxpayer) {
			this.idTaxpayer = idTaxpayer;
		}

		public String getContractStartDate() {
			return contractStartDate;
		}

		public void setContractStartDate(String contractStartDate) {
			this.contractStartDate = contractStartDate;
		}

		public String getContractRenewalDate() {
			return contractRenewalDate;
		}

		public void setContractRenewalDate(String contractRenewalDate) {
			this.contractRenewalDate = contractRenewalDate;
		}

		public String getContractEndDate() {
			return contractEndDate;
		}

		public void setContractEndDate(String contractEndDate) {
			this.contractEndDate = contractEndDate;
		}

		public boolean isActive() {
			return active;
		}

		public void setActive(boolean active) {
			this.active = active;
		}

		public List<ConsigneeContract> getConsigneeContracts() {
			if(consigneeContracts == null){
				consigneeContracts = new ArrayList<>();
			}
			return consigneeContracts;
		}

		public void setConsigneeContracts(List<ConsigneeContract> consigneeContracts) {
			this.consigneeContracts = consigneeContracts;
		}

		public boolean isContractPeriodEnded() {
			return contractPeriodEnded;
		}

		public void setContractPeriodEnded(boolean contractPeriodEnded) {
			this.contractPeriodEnded = contractPeriodEnded;
		}

		public String getContractName() {
			return contractName;
		}

		public void setContractName(String contractName) {
			this.contractName = contractName;
		}		
	}
	
	public static class ConsigneeContract{
		private String id;
		private String idConsignee;
		private String contractStartDate;
		private String contractEndDate;
		private String contractPath;
		private String document;
		private String documentExtension;
		private boolean active;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getIdConsignee() {
			return idConsignee;
		}

		public void setIdConsignee(String idConsignee) {
			this.idConsignee = idConsignee;
		}

		public boolean isActive() {
			return active;
		}

		public void setActive(boolean active) {
			this.active = active;
		}

		public String getContractPath() {
			return contractPath;
		}

		public void setContractPath(String contractPath) {
			this.contractPath = contractPath;
		}

		public String getDocument() {
			return document;
		}

		public void setDocument(String document) {
			this.document = document;
		}

		public String getDocumentExtension() {
			return documentExtension;
		}

		public void setDocumentExtension(String documentExtension) {
			this.documentExtension = documentExtension;
		}

		public String getContractStartDate() {
			return contractStartDate;
		}

		public void setContractStartDate(String contractStartDate) {
			this.contractStartDate = contractStartDate;
		}

		public String getContractEndDate() {
			return contractEndDate;
		}

		public void setContractEndDate(String contractEndDate) {
			this.contractEndDate = contractEndDate;
		}		
	}

	public Consignee getConsignee() {
		return consignee;
	}

	public void setConsignee(Consignee consignee) {
		this.consignee = consignee;
	}

	public List<Consignee> getConsignees() {
		if(consignees == null){
			consignees = new ArrayList<>();
		}
		return consignees;
	}

	public void setConsignees(List<Consignee> consignees) {
		this.consignees = consignees;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}	
}