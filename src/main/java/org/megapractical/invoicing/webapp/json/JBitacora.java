package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JBitacora {

	private String logDate;
	private List<Bitacora> logs;
	private JResponse response;
	
	public JBitacora(){
		super();
	}
	
	public static class Bitacora{
	
		private String username;
		private String operationDescription;
		private String operationImg;
		private String product;
		private String date;
		private String time;
		private String ip;
		private String url;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getOperationDescription() {
			return operationDescription;
		}

		public void setOperationDescription(String operationDescription) {
			this.operationDescription = operationDescription;
		}

		public String getOperationImg() {
			return operationImg;
		}

		public void setOperationImg(String operationImg) {
			this.operationImg = operationImg;
		}

		public String getProduct() {
			return product;
		}

		public void setProduct(String product) {
			this.product = product;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getTime() {
			return time;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}		
	}

	public String getLogDate() {
		return logDate;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}

	public List<Bitacora> getLogs() {
		if(logs == null)
			logs = new ArrayList<>();
		return logs;
	}

	public void setLogs(List<Bitacora> logs) {
		this.logs = logs;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}	
}