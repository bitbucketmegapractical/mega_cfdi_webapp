package org.megapractical.invoicing.webapp.json;

public class JEmitterLogo {

	private boolean isLogoDefined;
	private String logoUrl;
	private JResponse response;
	
	public JEmitterLogo(){
		super();
	}

	public boolean isLogoDefined() {
		return isLogoDefined;
	}

	public void setLogoDefined(boolean isLogoDefined) {
		this.isLogoDefined = isLogoDefined;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}	
}