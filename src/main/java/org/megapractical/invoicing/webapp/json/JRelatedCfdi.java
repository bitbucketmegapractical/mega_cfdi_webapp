package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiRelatedCfdi;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JRelatedCfdi {
	private String relationshipType;
	@Getter(AccessLevel.NONE)
	private List<String> uuids;

	public List<String> getUuids() {
		if (uuids == null) {
			uuids = new ArrayList<>();
		}
		return uuids;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class JRelatedCfdiRequest {
		private String relatedCfdiCheck;
		private JRelatedCfdi relatedCfdi;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class JRelatedCfdiResponse {
		private ApiRelatedCfdi apiRelatedCfdi;
		private boolean error;
	}
	
}