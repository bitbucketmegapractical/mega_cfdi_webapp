package org.megapractical.invoicing.webapp.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JStamp {
	private String stampAvailable;
	private String stampSpent;
	private String stampTotal;
	private String stampDiscounted;
	private String percentage;
	private boolean enabled;
	private String stampTransfered;
	private JResponse response;
}