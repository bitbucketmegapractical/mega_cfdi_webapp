package org.megapractical.invoicing.webapp.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JCfdiStampError {
	private String code;
	private String field;
	private String message;
	private String tab;
}