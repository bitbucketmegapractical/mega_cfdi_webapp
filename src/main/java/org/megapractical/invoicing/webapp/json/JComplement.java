package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JComplement {

	private List<Complement> complements;
	private List<String> educationLevelSource;
	private JResponse response;
	
	public JComplement(){
		super();
	}
	
	public static class Complement {
		private String code;
		private String name;
		private String version;
		private String lastActualization;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getLastActualization() {
			return lastActualization;
		}

		public void setLastActualization(String lastActualization) {
			this.lastActualization = lastActualization;
		}
	}

	/*Getters and Setters*/
	public List<Complement> getComplements() {
		if(complements == null)
			complements = new ArrayList<>();
		return complements;
	}

	public void setComplements(List<Complement> complements) {
		this.complements = complements;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public List<String> getEducationLevelSource() {
		if(educationLevelSource == null) {
			educationLevelSource = new ArrayList<>();
		}
		return educationLevelSource;
	}

	public void setEducationLevelSource(List<String> educationLevelSource) {
		this.educationLevelSource = educationLevelSource;
	}	
}