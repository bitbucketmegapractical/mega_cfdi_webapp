package org.megapractical.invoicing.webapp.json;

import org.megapractical.invoicing.api.wrapper.ApiReceiver;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JReceiver {
	private String rfc;
	private String nameOrBusinessName;
	private String cfdiUse;
	private String resident;
	private String residency;
	private String identityRegistrationNumber;
	private String customerSelected;
	private String email;
	private String curp;
	private String postalCode;
	private String taxRegime;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor	
	public static class JReceiverResponse {
		private ApiReceiver apiReceiver;
		private boolean error;
		
		public JReceiverResponse(ApiReceiver apiReceiver) {
			this.apiReceiver = apiReceiver;
			this.error = Boolean.FALSE;
		}
		
		public static final JReceiverResponse empty() {
			return JReceiverResponse.builder().error(Boolean.TRUE).build();
		}
	}
	
}