package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JStampBuyTransfer {

	private String rfcBuy;
	private String nameBuy;
	private String quantity;
	private String unitPrice;
	private String tax;
	private String salePrice;
	private String date;
	private String time;
	private String transactionId;
	private String reference;
	private String description;
	private String document;
	private String documentExtension;
	private String documentPath;
	
	private String rfcMatserAccount;
	private String rfcAssociatedAccount;
	private String associatedAccountId;
	private String transferAmount;
	private Boolean transferMasterToAccount;
	private Boolean transferAccountToMaster;
	private Boolean stampIndicate;
	private Boolean stampIndicateChangeSystem;
	private String transferType;
	private String options;
	private List<StampTransferOptions> stampTransferOptions;
	
	private JStamp stamp;
	private JStampPackage stampPackage;
	private JResponse response;

	public static class StampTransferOptions{
		private String id;
		private String value;
		
		public String getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}
		
		public String getValue() {
			return value;
		}
		
		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public JStampBuyTransfer(){
		super();
	}
	
	/* Getters and Setters*/
	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getRfcBuy() {
		return rfcBuy;
	}

	public void setRfcBuy(String rfcBuy) {
		this.rfcBuy = rfcBuy;
	}

	public String getNameBuy() {
		return nameBuy;
	}

	public void setNameBuy(String nameBuy) {
		this.nameBuy = nameBuy;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public JStamp getStamp() {
		return stamp;
	}

	public void setStamp(JStamp stamp) {
		this.stamp = stamp;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public JStampPackage getStampPackage() {
		return stampPackage;
	}

	public void setStampPackage(JStampPackage stampPackage) {
		this.stampPackage = stampPackage;
	}

	public String getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(String transferAmount) {
		this.transferAmount = transferAmount;
	}

	public Boolean getTransferMasterToAccount() {
		return transferMasterToAccount;
	}

	public void setTransferMasterToAccount(Boolean transferMasterToAccount) {
		this.transferMasterToAccount = transferMasterToAccount;
	}

	public Boolean getTransferAccountToMaster() {
		return transferAccountToMaster;
	}

	public void setTransferAccountToMaster(Boolean transferAccountToMaster) {
		this.transferAccountToMaster = transferAccountToMaster;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public List<StampTransferOptions> getStampTransferOptions() {
		if(stampTransferOptions == null)
			stampTransferOptions = new ArrayList<>();
		return stampTransferOptions;
	}

	public void setStampTransferOptions(List<StampTransferOptions> stampTransferOptions) {
		this.stampTransferOptions = stampTransferOptions;
	}

	public String getRfcMatserAccount() {
		return rfcMatserAccount;
	}

	public void setRfcMatserAccount(String rfcMatserAccount) {
		this.rfcMatserAccount = rfcMatserAccount;
	}

	public String getAssociatedAccountId() {
		return associatedAccountId;
	}

	public void setAssociatedAccountId(String associatedAccountId) {
		this.associatedAccountId = associatedAccountId;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getRfcAssociatedAccount() {
		return rfcAssociatedAccount;
	}

	public void setRfcAssociatedAccount(String rfcAssociatedAccount) {
		this.rfcAssociatedAccount = rfcAssociatedAccount;
	}

	public String getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}

	public Boolean getStampIndicate() {
		return stampIndicate;
	}

	public void setStampIndicate(Boolean stampIndicate) {
		this.stampIndicate = stampIndicate;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getDocumentExtension() {
		return documentExtension;
	}

	public void setDocumentExtension(String documentExtension) {
		this.documentExtension = documentExtension;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public Boolean getStampIndicateChangeSystem() {
		return stampIndicateChangeSystem;
	}

	public void setStampIndicateChangeSystem(Boolean stampIndicateChangeSystem) {
		this.stampIndicateChangeSystem = stampIndicateChangeSystem;
	}	
}