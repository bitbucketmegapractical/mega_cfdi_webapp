package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JAccount {

	private boolean isConsignee;
	private List<Account> accounts;
	private JResponse response;
	private JPagination pagination;
	
	public JAccount(){
		super();
	}
	
	public static class Account{
		private String id;
		private String rfc;
		private String nameOrBussinesName;
		private String taxpayerType;
		private String taxRegime;
		private boolean active;
		
		private boolean accountWithUser;
		private boolean userBySystemCreated;
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getRfc() {
			return rfc;
		}
		
		public void setRfc(String rfc) {
			this.rfc = rfc;
		}
		
		public String getNameOrBussinesName() {
			return nameOrBussinesName;
		}
		
		public void setNameOrBussinesName(String nameOrBussinesName) {
			this.nameOrBussinesName = nameOrBussinesName;
		}
		
		public String getTaxpayerType() {
			return taxpayerType;		
		}
		
		public void setTaxpayerType(String taxpayerType) {
			this.taxpayerType = taxpayerType;
		}
		
		public String getTaxRegime() {
			return taxRegime;
		}
		
		public void setTaxRegime(String taxRegime) {
			this.taxRegime = taxRegime;
		}
		
		public boolean isActive() {
			return active;
		}
		
		public void setActive(boolean active) {
			this.active = active;
		}

		public boolean isAccountWithUser() {
			return accountWithUser;
		}

		public void setAccountWithUser(boolean accountWithUser) {
			this.accountWithUser = accountWithUser;
		}

		public boolean isUserBySystemCreated() {
			return userBySystemCreated;
		}

		public void setUserBySystemCreated(boolean userBySystemCreated) {
			this.userBySystemCreated = userBySystemCreated;
		}		
	}

	public List<Account> getAccounts() {
		if(accounts == null)
			accounts = new ArrayList<>();
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}

	public boolean isConsignee() {
		return isConsignee;
	}

	public void setConsignee(boolean isConsignee) {
		this.isConsignee = isConsignee;
	}	
}