package org.megapractical.invoicing.webapp.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JTaxes {
	private JTax tax;
	private List<JTax> taxes;
	private JResponse response;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class JTax {
		private String base;
		private String tax;
		private String factorType;
		private String rateOrFee;
		private String amount;
		
		public JTax(String amount) {
			this.amount = amount;
		}
	}
	
}