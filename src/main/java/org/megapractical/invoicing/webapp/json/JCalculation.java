package org.megapractical.invoicing.webapp.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JCalculation {
	private JVoucher voucher;
	private JConcept concept;
	
	public JCalculation(JVoucher voucher) {
		this.voucher = voucher;
	}
	
	public JCalculation(JConcept concept) {
		this.concept = concept;
	}
	
}