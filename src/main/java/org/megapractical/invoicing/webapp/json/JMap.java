package org.megapractical.invoicing.webapp.json;

import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.webapp.util.UProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JMap {
	private String key;
	private String value;
	
	public static JMap encoded(String key, String value, String locale) {
		val encodedKey = UBase64.base64Encode(key);
		val encodedValue = UBase64.base64Encode(UProperties.getMessage(value, locale));
		return JMap.builder().key(encodedKey).value(encodedValue).build();
	}
	
}