package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JPreInvoice {

	private List<PreInvoice> preInvoices;
	private JResponse response;
	private JPagination pagination;
	
	public JPreInvoice(){
		super();
	}
	
	public static class PreInvoice{
		private String preInvoiceName;
		private String expeditionDate;
		private String receiverRfc;		

		public String getExpeditionDate() {
			return expeditionDate;
		}

		public void setExpeditionDate(String expeditionDate) {
			this.expeditionDate = expeditionDate;
		}

		public String getReceiverRfc() {
			return receiverRfc;
		}

		public void setReceiverRfc(String receiverRfc) {
			this.receiverRfc = receiverRfc;
		}

		public String getPreInvoiceName() {
			return preInvoiceName;
		}

		public void setPreInvoiceName(String preInvoiceName) {
			this.preInvoiceName = preInvoiceName;
		}		
	}

	public List<PreInvoice> getPreInvoices() {
		if(preInvoices == null)
			preInvoices = new ArrayList<>();
		return preInvoices;
	}

	public void setPreInvoices(List<PreInvoice> preInvoices) {
		this.preInvoices = preInvoices;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}	
}