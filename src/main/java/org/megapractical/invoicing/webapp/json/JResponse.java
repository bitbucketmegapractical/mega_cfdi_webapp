package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JResponse {
	private boolean success;
	private boolean error;
	private boolean unexpectedError;
	private String tabActivate;
	private String tabError;
	private JNotification notification;
	@Getter(AccessLevel.NONE)
	private List<JError> errors;
	@Getter(AccessLevel.NONE)
	private List<JMap> map;
	private boolean uploadLimit;

	public static final JResponse success() {
		return JResponse.builder().success(Boolean.TRUE).build();
	}
	
	public static final JResponse success(JNotification notification) {
		return JResponse.builder().notification(notification).success(Boolean.TRUE).build();
	}

	public static final JResponse maps(List<JMap> maps) {
		return JResponse.builder().map(maps).build();
	}
	
	public static final JResponse error(JNotification notification) {
		return JResponse.builder().notification(notification).error(Boolean.TRUE).build();
	}
	
	public static final JResponse error(JNotification notification, List<JError> errors) {
		return JResponse.builder().notification(notification).errors(errors).error(Boolean.TRUE).build();
	}
	
	public static final JResponse error(List<JError> errors) {
		return JResponse.builder().errors(errors).build();
	}
	
	public static final JResponse markedError(List<JError> errors) {
		return JResponse.builder().errors(errors).error(Boolean.TRUE).build();
	}
	
	public static final JResponse tabError(String tab, JNotification notification) {
		return tabError(tab, notification, new ArrayList<>());
	}
	
	public static final JResponse tabError(String tab, JNotification notification, List<JError> errors) {
		return JResponse.builder().tabError(tab).notification(notification).errors(errors).error(Boolean.TRUE).build();
	}

	public List<JError> getErrors() {
		if (errors == null) {
			errors = new ArrayList<>();
		}
		return errors;
	}

	public List<JMap> getMap() {
		if (map == null) {
			map = new ArrayList<>();
		}
		return map;
	}
}