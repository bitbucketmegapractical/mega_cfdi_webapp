package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JCustomer {
	private Customer customer;
	@Getter(AccessLevel.NONE)
	private List<Customer> customers;
	private JResponse response;
	private JPagination pagination;

	public JCustomer(List<Customer> customers) {
		this.customers = customers;
	}
	
	public List<Customer> getCustomers() {
		if (customers == null) {
			customers = new ArrayList<>();
		}
		return customers;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Customer {
		private String id;
		private String idEmitter;
		private String rfc;
		private String nameOrBusinessName;
		private String curp;
		private String email;
		private String resident;
		private String residency;
		private String identityRegistrationNumber;
		private String postalCode;
		private String taxRegime;
	}

}