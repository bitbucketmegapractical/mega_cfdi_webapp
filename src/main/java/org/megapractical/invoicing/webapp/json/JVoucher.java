package org.megapractical.invoicing.webapp.json;

import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MetodoPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JVoucher {
	private String voucherType;
	private String postalCode;
	private String paymentWay;
	private String paymentMethod;
	private String currency;
	private String changeType;
	private String confirmationCode;
	private String paymentConditions;
	private String exportation;
	private String subTotal;
	private String discount;
	private String totalTaxesTransferred;
	private String totalTaxesWithheld;
	private String total;
	private String legend;
	private String serie;
	private String folio;
	private boolean allowIndicateSerieSheet;
	private boolean allowIndicateSheet;
	private String serieSelected;
	private JRelatedCfdi relatedCFDI;
	private JResponse response;
	
	public JVoucher(JResponse response) {
		this.response = response;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor	
	public static class JVoucherResponse {
		private ApiVoucher apiVoucher;
		private ContribuyenteSerieFolioEntity serieSheet;
		private TipoComprobanteEntity voucherType;
		private CodigoPostalEntity expeditionPlace;
		private FormaPagoEntity paymentWay;
		private MetodoPagoEntity paymentMethod;
		private MonedaEntity currency;
		private String legend;
		private boolean error;
		
		public static final JVoucherResponse empty() {
			return JVoucherResponse.builder().error(Boolean.TRUE).build();
		}
	}
	
}