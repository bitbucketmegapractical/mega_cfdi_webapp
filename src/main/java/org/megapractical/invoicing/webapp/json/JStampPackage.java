package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.dal.bean.jpa.TimbrePaqueteEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JStampPackage {
	private StampPackage stampPackage;
	@Getter(AccessLevel.NONE)
	private List<StampPackage> stampPackages;
	@Getter(AccessLevel.NONE)
	private List<StampPackageOptions> stampPackageOptions;
	private JResponse response;
	
	public JStampPackage(List<StampPackage> stampPackages, JResponse response) {
		this.stampPackages = stampPackages;
		this.response = response;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class StampPackageOptions {
		private String id;
		private String value;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class StampPackage {
		private String rfcBuy;
		private String nameBuy;
		private String description;
		private Integer quantity;
		private String unitPrice;
		private String taxPercent;
		private String subtotal;
		private String taxImport;
		private String total;
		private String options;
		
		public StampPackage(TimbrePaqueteEntity item) {
			this.quantity = item.getCantidad();
			this.unitPrice = UValue.doubleString(item.getPrecioUnitario());
			this.taxPercent = UValue.doubleString(item.getIvaPorciento());
			this.taxImport = UValue.doubleString(item.getImpuesto());
			this.subtotal = UValue.doubleString(item.getPrecioUnitario());
			this.total = UValue.doubleString(item.getPrecioVenta());
			this.description = item.getDescripcion();
		}
	}

	public List<StampPackageOptions> getStampPackageOptions() {
		if (stampPackageOptions == null) {
			stampPackageOptions = new ArrayList<>();
		}
		return stampPackageOptions;
	}

	public List<StampPackage> getStampPackages() {
		if (stampPackages == null) {
			stampPackages = new ArrayList<>();
		}
		return stampPackages;
	}

}