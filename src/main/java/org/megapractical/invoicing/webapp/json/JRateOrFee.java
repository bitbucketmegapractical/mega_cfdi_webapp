package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JRateOrFee {
	private List<RateOrFee> rateOrFees;
	private JResponse response;

	public List<RateOrFee> getRateOrFees() {
		if (rateOrFees == null) {
			rateOrFees = new ArrayList<>();
		}
		return rateOrFees;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RateOrFee {
		private String rangeType;
		private String taxType;
		private String factorType;
		private String minValue;
		private String maxValue;
		private boolean transferred;
		private boolean withheld;
		private List<String> expectedValues;

		public List<String> getExpectedValues() {
			if (expectedValues == null) {
				expectedValues = new ArrayList<>();
			}
			return expectedValues;
		}
	}

}