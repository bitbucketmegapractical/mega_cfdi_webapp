package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.webapp.json.JRetention.RetTotal.RetTotalWithheldTax;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JRetention {
	@Getter(AccessLevel.NONE)
	private List<Retention> retentions;
	private RetentionData retentionData;
	@Getter(AccessLevel.NONE)
	private List<RetTotalWithheldTax> withheldTaxs;
	private RetentionNotification retentionNotification;
	private JResponse response;
	private JPagination pagination;
	
	public JRetention(JResponse response) {
		this.response = response;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Retention {
		private String uuid;
		private String receiverRfc;
		private String receiverCurp;
		private String receiverBusinessName;
		private String receiverRegNumber;
		private String folio;
		private String retetionKey;
		private String periodInitialMonth;
		private String periodEndMonth;
		private String periodYear;
		private String expeditionDate;
		private String expeditionPlace;
		private String description;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetentionNotification {
		private String uuid;
		private String recipientTo;
		@Getter(AccessLevel.NONE)
		private List<String> recipients;
		
		public List<String> getRecipients() {
			if (recipients == null) {
				recipients = new ArrayList<>();
			}
			return recipients;
		}
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetentionData {
		private RetInfo retInfo;
		private RetReceiver retReceiver;
		private RetTotal retTotal;
		private RetRelatedCfdi retRelatedCfdi;
		private RetDiv retDiv;
		private RetEna retEna;
		private RetInt retInt;
		private RetOcd retOcd;
		private RetPex retPex;
		private RetSfi retSfi;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetInfo {
		private String initialMonth;
		private String endMonth;
		private String year;
		private String folio;
		private String retentionKey;
		private String expeditionPlace;
		private String description;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetReceiver {
		private String receiverNationality;
		private String receiverNatRfc;
		private String receiverNatBusinessName;
		private String receiverNatCurp;
		private String receiverNatPostalCode;
		private String receiverExtBusinessName;
		private String receiverExtNumber;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetTotal {
		private String totalOperationAmount;
		private String totalAmountTaxed;
		private String totalAmountExempt;
		private String totalAmountWithheld;
		private String quarterlyProfit;
		private String correspondingISR;

		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class RetTotalWithheldTax {
			private String index;
			private String base;
			private String tax;
			private String amount;
			private String paymentType;
		}
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetRelatedCfdi {
		private String type;
		private String uuid;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetDiv {
		private String cveTipDivOUtil;
		private String montISRAcredRetMexico;
		private String montISRAcredRetExtranjero;
		private String montRetExtDivExt;
		private String tipoSocDistrDiv;
		private String montISRAcredNal;
		private String montDivAcumNal;
		private String montDivAcumExt;
		private String proporcionRem;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetEna {
		private String contract;
		private String profit;
		private String waste;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetInt {
		private String financeSystem;
		private String retirement;
		private String financialOperations;
		private String interestAmount;
		private String realInterest;
		private String interestWaste;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetOcd {
		private String profitAmount;
		private String deductibleWaste;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetPex {
		private String beneficiary;
		private String beneficiaryRfc;
		private String beneficiaryCurp;
		private String beneficiaryBusinessName;
		private String paymentConcept;
		private String conceptDescription;
		private String noBeneficiaryCountry;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RetSfi {
		private String fideicomId;
		private String fideicomName;
		private String fideicomDescription;
	}

	public List<RetTotalWithheldTax> getWithheldTaxs() {
		if(withheldTaxs == null) {
			withheldTaxs = new ArrayList<>();
		}
		return withheldTaxs;
	}

	public List<Retention> getRetentions() {
		if(retentions == null) {
			retentions = new ArrayList<>();
		}
		return retentions;
	}

}