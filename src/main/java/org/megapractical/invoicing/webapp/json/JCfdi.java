package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JCfdi {
	
	private List<Cfdi> cfdis;
	private CfdiCancel cfdiCancel;
	
	private boolean cancelated;
	private boolean cancelationInProgress;
	
	private String cancelationCode;
	private String cancelationMessage;
	private String cfdiType;
	private JResponse response;
	private JPagination pagination;
	
	public JCfdi(){
		super();
	}
	
	public static class Cfdi{
		private String uuid;
		private String expeditionDate;
		private String stampedDate;
		private String receiverRfc;
		private String serieFolio;
		private String confirmationCode;
		
		private boolean cancelated;
		private boolean cancelationInProgress;
		private boolean cancelationRequest;
		
		private String cancelatedDateTime;
		private String cancelationRequestDateTime;
		private String cancelationRequestLastStatusVerified;
		private String cancelatedAcuseXml;
		
		private String tipoComprobante;
		
		public String getTipoComprobante() {
			return tipoComprobante;
		}

		public void setTipoComprobante(String tipoComprobante) {
			this.tipoComprobante = tipoComprobante;
		}

		public String getUuid() {
			return uuid;
		}

		public void setUuid(String uuid) {
			this.uuid = uuid;
		}

		public String getExpeditionDate() {
			return expeditionDate;
		}

		public void setExpeditionDate(String expeditionDate) {
			this.expeditionDate = expeditionDate;
		}

		public String getReceiverRfc() {
			return receiverRfc;
		}

		public void setReceiverRfc(String receiverRfc) {
			this.receiverRfc = receiverRfc;
		}
		
		public String getSerieFolio() {
			return serieFolio;
		}

		public void setSerieFolio(String serieFolio) {
			this.serieFolio = serieFolio;
		}

		public String getConfirmationCode() {
			return confirmationCode;
		}

		public void setConfirmationCode(String confirmationCode) {
			this.confirmationCode = confirmationCode;
		}

		public boolean isCancelated() {
			return cancelated;
		}

		public void setCancelated(boolean cancelated) {
			this.cancelated = cancelated;
		}

		public boolean isCancelationInProgress() {
			return cancelationInProgress;
		}

		public void setCancelationInProgress(boolean cancelationInProgress) {
			this.cancelationInProgress = cancelationInProgress;
		}

		public boolean isCancelationRequest() {
			return cancelationRequest;
		}

		public void setCancelationRequest(boolean cancelationRequest) {
			this.cancelationRequest = cancelationRequest;
		}

		public String getCancelatedDateTime() {
			return cancelatedDateTime;
		}

		public void setCancelatedDateTime(String cancelatedDateTime) {
			this.cancelatedDateTime = cancelatedDateTime;
		}

		public String getCancelationRequestDateTime() {
			return cancelationRequestDateTime;
		}

		public void setCancelationRequestDateTime(String cancelationRequestDateTime) {
			this.cancelationRequestDateTime = cancelationRequestDateTime;
		}

		public String getCancelatedAcuseXml() {
			return cancelatedAcuseXml;
		}

		public void setCancelatedAcuseXml(String cancelatedAcuseXml) {
			this.cancelatedAcuseXml = cancelatedAcuseXml;
		}

		public String getCancelationRequestLastStatusVerified() {
			return cancelationRequestLastStatusVerified;
		}

		public void setCancelationRequestLastStatusVerified(String cancelationRequestLastStatusVerified) {
			this.cancelationRequestLastStatusVerified = cancelationRequestLastStatusVerified;
		}

		public String getStampedDate() {
			return stampedDate;
		}

		public void setStampedDate(String stampedDate) {
			this.stampedDate = stampedDate;
		}
	}
	
	public static class CfdiCancel{
		private String uuid;
		private String rfc;
		private String emitterType;
		private String passwd;
		
		public String getUuid() {
			return uuid;
		}
		
		public void setUuid(String uuid) {
			this.uuid = uuid;
		}
		
		public String getEmitterType() {
			return emitterType;
		}
		
		public void setEmitterType(String emitterType) {
			this.emitterType = emitterType;
		}
		
		public String getPasswd() {
			return passwd;
		}
		
		public void setPasswd(String passwd) {
			this.passwd = passwd;
		}

		public String getRfc() {
			return rfc;
		}

		public void setRfc(String rfc) {
			this.rfc = rfc;
		}		
	}
	
	public List<Cfdi> getCfdis() {
		if(cfdis == null)
			cfdis = new ArrayList<>();
		return cfdis;
	}

	public void setCfdis(List<Cfdi> cfdis) {
		this.cfdis = cfdis;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public boolean isCancelated() {
		return cancelated;
	}

	public void setCancelated(boolean cancelated) {
		this.cancelated = cancelated;
	}

	public String getCancelationCode() {
		return cancelationCode;
	}

	public void setCancelationCode(String cancelationCode) {
		this.cancelationCode = cancelationCode;
	}

	public String getCancelationMessage() {
		return cancelationMessage;
	}

	public void setCancelationMessage(String cancelationMessage) {
		this.cancelationMessage = cancelationMessage;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}

	public CfdiCancel getCfdiCancel() {
		return cfdiCancel;
	}

	public void setCfdiCancel(CfdiCancel cfdiCancel) {
		this.cfdiCancel = cfdiCancel;
	}	
	
	public String getCfdiType() {
		return cfdiType;
	}

	public void setCfdiType(String cfdiType) {
		this.cfdiType = cfdiType;
	}

	public boolean isCancelationInProgress() {
		return cancelationInProgress;
	}

	public void setCancelationInProgress(boolean cancelationInProgress) {
		this.cancelationInProgress = cancelationInProgress;
	}
}