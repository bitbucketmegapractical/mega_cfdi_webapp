package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JCfdiFile {
	private CfdiFile cfdiFile;
	private List<CfdiFile> queuedFiles;
	private List<CfdiFile> processingFiles;
	private List<CfdiFile> generatedFiles;
	@Getter(AccessLevel.NONE)
	private List<String> uuids;
	private JResponse response;
	private JPagination<?> pagination;

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class CfdiFile {
		private String id;
		private String cfdiType;
		private String payrollType;
		private String fileType;
		private String fileStatus;
		private String name;
		private String path;
		private String uploadDate;		
		private String period;
		private String year;
		private Boolean stampAvailable;
		private String compactedPath;
		private String consolidatedPath;
		private String acusesPath;
		private String errorPath;
		private CfdiFileStatistics cfdiFileStatistics;
		private String stimatedTime;
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class CfdiFileStatistics{
			private String invoiceFileId;
			private String totalInvoice;
			private String totalInvoiceStamped;
			private String totalInvoiceError;
			private String parserStartDate;
			private String parserStartTime;
			private String parserEndDate;
			private String parserEndTime;
			private String voucherGeneratingStartDate;
			private String voucherGeneratingStartTime;
			private String voucherGeneratingEndDate;
			private String voucherGeneratingEndTime;
			private String voucherStampingStartDate;
			private String voucherStampingStartTime;
			private String voucherStampingEndDate;
			private String voucherStampingEndTime;
		}
	}

	/* Getters and Setters */
	public List<String> getUuids() {
		if (uuids == null) {
			uuids = new ArrayList<>();
		}
		return uuids;
	}

}