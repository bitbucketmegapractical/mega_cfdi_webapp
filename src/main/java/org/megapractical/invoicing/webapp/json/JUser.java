package org.megapractical.invoicing.webapp.json;

public class JUser {
	
	private String nameOrBusinessName;
	private String rfc;
	private String rfcActive;
	private String phone;
	private String productServiceActive;
	private String username;	
	private String passwd;
	private boolean enabled;
	private String userId;
	private String email;
	private String hash;
	private String passwdRepeat;
	private JResponse response;
	
	public JUser(){
		super();
	}

	public String getNameOrBusinessName() {
		return nameOrBusinessName;
	}

	public void setNameOrBusinessName(String nameOrBusinessName) {
		this.nameOrBusinessName = nameOrBusinessName;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getRfcActive() {
		return rfcActive;
	}

	public void setRfcActive(String rfcActive) {
		this.rfcActive = rfcActive;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProductServiceActive() {
		return productServiceActive;
	}

	public void setProductServiceActive(String productServiceActive) {
		this.productServiceActive = productServiceActive;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getPasswdRepeat() {
		return passwdRepeat;
	}

	public void setPasswdRepeat(String passwdRepeat) {
		this.passwdRepeat = passwdRepeat;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}	
}