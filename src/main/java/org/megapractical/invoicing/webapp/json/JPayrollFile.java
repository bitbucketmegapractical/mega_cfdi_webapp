package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JPayrollFile {

	private PayrollFile payrollFile;
	private List<PayrollFile> queuedPayrollFiles;
	private List<PayrollFile> processingPayrollFiles;
	private List<PayrollFile> generatedPayrollFiles;
	private JResponse response;
	private JPagination pagination;
	
	public JPayrollFile(){
		super();
	}
	
	public static class PayrollFile{
		private String id;
		private String payrollType;
		private String fileType;
		private String fileStatus;
		private String name;
		private String path;
		private String uploadDate;		
		private String period;
		private String year;
		private Boolean stampAvailable;
		private String compactedPath;
		private String consolidatedPath;
		private String errorPath;
		private PayrollFileStatistics payrollFileStatistics;
		private String stimatedTime;

		public static class PayrollFileStatistics{
			private String payrollFileId;
			private String totalPayroll;
			private String totalPayrollStamped;
			private String totalPayrollError;			
			private String parserStartDate;
			private String parserStartTime;
			private String parserEndDate;
			private String parserEndTime;
			private String voucherGeneratingStartDate;
			private String voucherGeneratingStartTime;
			private String voucherGeneratingEndDate;
			private String voucherGeneratingEndTime;
			private String voucherStampingStartDate;
			private String voucherStampingStartTime;
			private String voucherStampingEndDate;
			private String voucherStampingEndTime;

			/*Getters and Setters*/
			public String getTotalPayroll() {
				return totalPayroll;
			}

			public void setTotalPayroll(String totalPayroll) {
				this.totalPayroll = totalPayroll;
			}

			public String getTotalPayrollStamped() {
				return totalPayrollStamped;
			}

			public void setTotalPayrollStamped(String totalPayrollStamped) {
				this.totalPayrollStamped = totalPayrollStamped;
			}

			public String getTotalPayrollError() {
				return totalPayrollError;
			}

			public void setTotalPayrollError(String totalPayrollError) {
				this.totalPayrollError = totalPayrollError;
			}

			public String getParserStartDate() {
				return parserStartDate;
			}

			public void setParserStartDate(String parserStartDate) {
				this.parserStartDate = parserStartDate;
			}

			public String getParserStartTime() {
				return parserStartTime;
			}

			public void setParserStartTime(String parserStartTime) {
				this.parserStartTime = parserStartTime;
			}

			public String getParserEndDate() {
				return parserEndDate;
			}

			public void setParserEndDate(String parserEndDate) {
				this.parserEndDate = parserEndDate;
			}

			public String getParserEndTime() {
				return parserEndTime;
			}

			public void setParserEndTime(String parserEndTime) {
				this.parserEndTime = parserEndTime;
			}

			public String getVoucherGeneratingStartDate() {
				return voucherGeneratingStartDate;
			}

			public void setVoucherGeneratingStartDate(String voucherGeneratingStartDate) {
				this.voucherGeneratingStartDate = voucherGeneratingStartDate;
			}

			public String getVoucherGeneratingStartTime() {
				return voucherGeneratingStartTime;
			}

			public void setVoucherGeneratingStartTime(String voucherGeneratingStartTime) {
				this.voucherGeneratingStartTime = voucherGeneratingStartTime;
			}

			public String getVoucherGeneratingEndDate() {
				return voucherGeneratingEndDate;
			}

			public void setVoucherGeneratingEndDate(String voucherGeneratingEndDate) {
				this.voucherGeneratingEndDate = voucherGeneratingEndDate;
			}

			public String getVoucherGeneratingEndTime() {
				return voucherGeneratingEndTime;
			}

			public void setVoucherGeneratingEndTime(String voucherGeneratingEndTime) {
				this.voucherGeneratingEndTime = voucherGeneratingEndTime;
			}

			public String getVoucherStampingStartDate() {
				return voucherStampingStartDate;
			}

			public void setVoucherStampingStartDate(String voucherStampingStartDate) {
				this.voucherStampingStartDate = voucherStampingStartDate;
			}

			public String getVoucherStampingStartTime() {
				return voucherStampingStartTime;
			}

			public void setVoucherStampingStartTime(String voucherStampingStartTime) {
				this.voucherStampingStartTime = voucherStampingStartTime;
			}

			public String getVoucherStampingEndDate() {
				return voucherStampingEndDate;
			}

			public void setVoucherStampingEndDate(String voucherStampingEndDate) {
				this.voucherStampingEndDate = voucherStampingEndDate;
			}

			public String getVoucherStampingEndTime() {
				return voucherStampingEndTime;
			}

			public void setVoucherStampingEndTime(String voucherStampingEndTime) {
				this.voucherStampingEndTime = voucherStampingEndTime;
			}

			public String getPayrollFileId() {
				return payrollFileId;
			}

			public void setPayrollFileId(String payrollFileId) {
				this.payrollFileId = payrollFileId;
			}
		}
		
		/*Getters and Setters*/
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getPayrollType() {
			return payrollType;
		}

		public void setPayrollType(String payrollType) {
			this.payrollType = payrollType;
		}

		public String getFileType() {
			return fileType;
		}

		public void setFileType(String fileType) {
			this.fileType = fileType;
		}

		public String getFileStatus() {
			return fileStatus;
		}

		public void setFileStatus(String fileStatus) {
			this.fileStatus = fileStatus;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public String getUploadDate() {
			return uploadDate;
		}

		public void setUploadDate(String uploadDate) {
			this.uploadDate = uploadDate;
		}

		public String getPeriod() {
			return period;
		}

		public void setPeriod(String period) {
			this.period = period;
		}

		public String getYear() {
			return year;
		}

		public void setYear(String year) {
			this.year = year;
		}

		public Boolean getStampAvailable() {
			return stampAvailable;
		}

		public void setStampAvailable(Boolean stampAvailable) {
			this.stampAvailable = stampAvailable;
		}

		public String getCompactedPath() {
			return compactedPath;
		}

		public void setCompactedPath(String compactedPath) {
			this.compactedPath = compactedPath;
		}

		public String getConsolidatedPath() {
			return consolidatedPath;
		}

		public void setConsolidatedPath(String consolidatedPath) {
			this.consolidatedPath = consolidatedPath;
		}

		public String getErrorPath() {
			return errorPath;
		}

		public void setErrorPath(String errorPath) {
			this.errorPath = errorPath;
		}

		public PayrollFileStatistics getPayrollFileStatistics() {
			return payrollFileStatistics;
		}

		public void setPayrollFileStatistics(PayrollFileStatistics payrollFileStatistics) {
			this.payrollFileStatistics = payrollFileStatistics;
		}

		public String getStimatedTime() {
			return stimatedTime;
		}

		public void setStimatedTime(String stimatedTime) {
			this.stimatedTime = stimatedTime;
		}		
	}

	/*Getters and Setters*/
	public List<PayrollFile> getQueuedPayrollFiles() {
		if(queuedPayrollFiles == null)
			queuedPayrollFiles = new ArrayList<>();
		return queuedPayrollFiles;
	}

	public void setQueuedPayrollFiles(List<PayrollFile> queuedPayrollFiles) {
		this.queuedPayrollFiles = queuedPayrollFiles;
	}

	public List<PayrollFile> getProcessingPayrollFiles() {
		if(processingPayrollFiles == null)
			processingPayrollFiles = new ArrayList<>();
		return processingPayrollFiles;
	}

	public void setProcessingPayrollFiles(List<PayrollFile> processingPayrollFiles) {
		this.processingPayrollFiles = processingPayrollFiles;
	}

	public List<PayrollFile> getGeneratedPayrollFiles() {
		if(generatedPayrollFiles == null)
			generatedPayrollFiles = new ArrayList<>();		
		return generatedPayrollFiles;
	}

	public void setGeneratedPayrollFiles(List<PayrollFile> generatedPayrollFiles) {
		this.generatedPayrollFiles = generatedPayrollFiles;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}

	public PayrollFile getPayrollFile() {
		return payrollFile;
	}

	public void setPayrollFile(PayrollFile payrollFile) {
		this.payrollFile = payrollFile;
	}	
}