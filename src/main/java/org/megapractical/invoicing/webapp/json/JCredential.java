package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JCredential {

	private Credential credential;
	private List<Credential> credentials;
	
	private JStamp stamp;
	private JStampPackage stampPackage;
	private JStampBuyTransfer stampBuyTransfer;
	
	private JConsignee consignee;
	
	private JResponse response;
	private JPagination pagination;
	
	public JCredential(){
		super();
	}
	
	public static class Credential{
		private String id;
		private String taxpayerId;
		private String userId;
		private String language;
		private String name;
		private String rfc;
		private String rfcActive;
		private String phone;
		private String activeProduct;
		private String email;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getTaxpayerId() {
			return taxpayerId;
		}

		public void setTaxpayerId(String taxpayerId) {
			this.taxpayerId = taxpayerId;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getLanguage() {
			return language;
		}

		public void setLanguage(String language) {
			this.language = language;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getRfc() {
			return rfc;
		}

		public void setRfc(String rfc) {
			this.rfc = rfc;
		}

		public String getRfcActive() {
			return rfcActive;
		}

		public void setRfcActive(String rfcActive) {
			this.rfcActive = rfcActive;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getActiveProduct() {
			return activeProduct;
		}

		public void setActiveProduct(String activeProduct) {
			this.activeProduct = activeProduct;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}		
	}

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	public List<Credential> getCredentials() {
		if(credentials == null)
			credentials = new ArrayList<>();
		return credentials;
	}

	public void setCredentials(List<Credential> credentials) {
		this.credentials = credentials;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}

	public JStampPackage getStampPackage() {
		return stampPackage;
	}

	public void setStampPackage(JStampPackage stampPackage) {
		this.stampPackage = stampPackage;
	}

	public JStampBuyTransfer getStampBuyTransfer() {
		return stampBuyTransfer;
	}

	public void setStampBuyTransfer(JStampBuyTransfer stampBuyTransfer) {
		this.stampBuyTransfer = stampBuyTransfer;
	}

	public JStamp getStamp() {
		return stamp;
	}

	public void setStamp(JStamp stamp) {
		this.stamp = stamp;
	}

	public JConsignee getConsignee() {
		return consignee;
	}

	public void setConsignee(JConsignee consignee) {
		this.consignee = consignee;
	}	
}