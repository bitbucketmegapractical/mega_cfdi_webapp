package org.megapractical.invoicing.webapp.json;

import org.megapractical.invoicing.webapp.ui.Paginator;
import org.springframework.data.domain.Page;

import lombok.Data;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 * @param <T> Object type of pagination
 */
@Data
public class JPagination<T> {
	private Paginator<T> paginator;
	private JPage page;
	
	public static <T> JPagination<T> buildPaginaton(Page<T> page) {
		if (page == null) {
			page = Page.empty();
		}
		
		val pagination = new JPagination<T>();
		pagination.setPaginator(new Paginator<>(page));
		pagination.setPage(createPagination(page));
		
		return pagination;
	}

	private static <T> org.megapractical.invoicing.webapp.json.JPage createPagination(Page<T> page) {
		val paginationPage = new JPage();
		paginationPage.setFirst(page.isFirst());
		paginationPage.setHasContent(page.hasContent());
		paginationPage.setHasNext(page.hasNext());
		paginationPage.setHasPrevious(page.hasPrevious());
		paginationPage.setLast(page.isLast());
		paginationPage.setNumber(page.getNumber());
		paginationPage.setNumberOfElements(page.getNumberOfElements());
		paginationPage.setSize(page.getSize());
		paginationPage.setTotalPages(page.getTotalPages());
		paginationPage.setTotalElements(Integer.valueOf(String.valueOf(page.getTotalElements())));
		return paginationPage;
	}
	
}