package org.megapractical.invoicing.webapp.json;

import java.util.List;

import org.megapractical.invoicing.webapp.json.JTaxes.JTax;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JConcept {
	private String conceptSelected;
	private String description;
	private String measurementUnit;
	private String keyProductService;
	private String identificationNumber;
	private String unit;
	private String quantity;
	private String unitValue;
	private String discount;
	private String amount;
	private String objImp;
	private String propertyAccountNumber;
	private List<String> customsInformationNumbers;
	private List<JTax> taxesTransferred;
	private List<JTax> taxesWithheld;
	private Iedu iedu;
	private String index;
	private boolean isEdit;
	private JResponse response;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Iedu {
		private String studentName;
		private String studentCurp;
		private String educationLevel;
		private String schoolKey;
		private String studentRfc;
	}
	
}