package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JComplementPayment {
	@Getter(AccessLevel.NONE)
	private List<ComplementPayment> complementPayments;
	private ComplementPayment complementPayment;
	private ComplementPaymentTotal complementPaymentTotal;
	private JResponse response;
	
	public JComplementPayment(JResponse response) {
		this.response = response;
	}
	
	public List<ComplementPayment> getComplementPayments() {
		if (complementPayments == null) {
			complementPayments = new ArrayList<>();
		}
		return complementPayments;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ComplementPaymentData {
		private ComplementPayment complementPayment;
		private ComplementPaymentTotal complementPaymentTotal;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ComplementPaymentTotal {
		private String totalWithheldIva;
		private String totalWithheldIsr;
		private String totalWithheldIeps;
		private String totalTransferredBaseIva16;
		private String totalTransferredTaxIva16;
		private String totalTransferredBaseIva8;
		private String totalTransferredTaxIva8;
		private String totalTransferredBaseIva0;
		private String totalTransferredTaxIva0;
		private String totalTransferredBaseIvaExempt;
		private String totalAmountPayments;
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ComplementPayment {
		private String paymentDate;
		private String paymentWay;
		private String currency;
		private String changeType;
		private String amount;
		private String operationNumber;
		private String sourceAccountRfc;
		private String bank;
		private String payerAccount;
		private String targetAccountRfc;
		private String receiverAccount;
		private String stringType;
		private String certificate;
		private String originalString;
		private String seal;
		@Getter(AccessLevel.NONE)
		private List<ComplementPaymentRelatedDocument> relatedDocuments;
		@Getter(AccessLevel.NONE)
		private List<ComplementPaymentWithheld> withhelds;
		@Getter(AccessLevel.NONE)
		private List<ComplementPaymentBaseTax> transferreds;

		public List<ComplementPaymentRelatedDocument> getRelatedDocuments() {
			if (relatedDocuments == null) {
				relatedDocuments = new ArrayList<>();
			}
			return relatedDocuments;
		}
		
		public List<ComplementPaymentWithheld> getWithhelds() {
			if (withhelds == null) {
				withhelds = new ArrayList<>();
			}
			return withhelds;
		}
		
		public List<ComplementPaymentBaseTax> getTransferreds() {
			if (transferreds == null) {
				transferreds = new ArrayList<>();
			}
			return transferreds;
		}
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class ComplementPaymentRelatedDocument {
			private String documentId;
			private String serie;
			private String sheet;
			private String currency;
			private String changeType;
			private String partiality;
			private String amountPartialityBefore;
			private String amountPaid;
			private String difference;
			private String taxObject;
			@Getter(AccessLevel.NONE)
			private List<ComplementPaymentBaseTax> withhelds;
			@Getter(AccessLevel.NONE)
			private List<ComplementPaymentBaseTax> transferreds;
			
			public List<ComplementPaymentBaseTax> getWithhelds() {
				if (withhelds == null) {
					withhelds = new ArrayList<>();
				}
				return withhelds;
			}
			
			public List<ComplementPaymentBaseTax> getTransferreds() {
				if (transferreds == null) {
					transferreds = new ArrayList<>();
				}
				return transferreds;
			}
		}
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class ComplementPaymentBaseTax {
			private String base;
			private String tax;
			private String factorType;
			private String rateOrFee;
			private String amount;
		}
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class ComplementPaymentWithheld {
			private String tax;
			private String amount;
		}
	}
}