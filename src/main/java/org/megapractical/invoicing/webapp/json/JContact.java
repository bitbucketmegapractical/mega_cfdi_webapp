package org.megapractical.invoicing.webapp.json;

import java.util.ArrayList;
import java.util.List;

public class JContact {

	private Contact contact;
	private List<Contact> contacts;
	private JResponse response;
	private JPagination pagination;
	
	public JContact(){
		super();
	}
	
	public static class Contact{
		private String id;
		private String idEmitter;
		private String name;
		private String email;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getIdEmitter() {
			return idEmitter;
		}

		public void setIdEmitter(String idEmitter) {
			this.idEmitter = idEmitter;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public List<Contact> getContacts() {
		if(contacts == null)
			contacts = new ArrayList<>();
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public JResponse getResponse() {
		return response;
	}

	public void setResponse(JResponse response) {
		this.response = response;
	}

	public JPagination getPagination() {
		return pagination;
	}

	public void setPagination(JPagination pagination) {
		this.pagination = pagination;
	}	
}
