package org.megapractical.invoicing.webapp.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JOption {
	private String code;
	private String value;
	private String customValue;
}