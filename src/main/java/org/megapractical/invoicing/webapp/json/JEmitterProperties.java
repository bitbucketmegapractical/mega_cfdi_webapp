package org.megapractical.invoicing.webapp.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JEmitterProperties {
	private String personType;
	private String taxRegime;
	private boolean displayCustomerCatalog;
	private boolean displayConceptCatalog;
	private boolean isCertificateConfigured;
	private String certificateCreationDate;
	private String certificateValidDate;
	private String certificateNumber;
	private String currency;
	private String postalCode;
	private JStamp stamp;
}