package org.megapractical.invoicing.webapp.json;

import org.megapractical.invoicing.webapp.notification.Toastr;
import org.megapractical.invoicing.webapp.notification.W2Tag;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JNotification {
	private boolean pageMessage;
	private boolean modalPanelMessage;
	private boolean panelMessage;
	private boolean search;
	private String cssStyleClass;
	private String message;
	private String searchResultMessage;
	@Default
	private boolean dismiss = Boolean.TRUE;
	private Integer fadeOut;
	private boolean toastrNotification;
	private Toastr toastr;
	private boolean w2tagNotification;	
	private W2Tag w2Tag;
	
	public static final JNotification toastrNotification(Toastr toastr) {
		return JNotification.builder().toastr(toastr).toastrNotification(Boolean.TRUE).build();
	}
	
	public static final JNotification pageMessageError(String message) {
		return JNotification
				.builder()
				.pageMessage(Boolean.TRUE)
				.message(message)
				.cssStyleClass(CssStyleClass.ERROR.value())
				.build();
	}
	
	public static final JNotification modalPanelError() {
		return JNotification
				.builder()
				.modalPanelMessage(Boolean.TRUE)
				.cssStyleClass(CssStyleClass.ERROR.value())
				.build();
	}
	
	public static final JNotification modalPanelMessageError(String message) {
		return JNotification
				.builder()
				.modalPanelMessage(Boolean.TRUE)
				.message(message)
				.cssStyleClass(CssStyleClass.ERROR.value())
				.build();
	}
	
	public enum CssStyleClass{
		SUCCESS("alert alert-success alert-dismissable"),
		ERROR("alert alert-danger alert-dismissable"),
		INFO("alert alert-info alert-dismissable"),
		WARNING("alert alert-warning alert-dismissable");
		
		private final String value;

		CssStyleClass(String v) {
	        value = v;
	    }

	    public String value() {
	        return value;
	    }

	    public CssStyleClass fromValue(String v) {
	        return valueOf(v);
	    }
	}
	
}