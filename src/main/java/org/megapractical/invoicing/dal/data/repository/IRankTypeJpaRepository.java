package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoRangoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRankTypeJpaRepository extends PagingAndSortingRepository<TipoRangoEntity, Long> {

	public List<TipoRangoEntity> findByEliminadoFalse();
	public TipoRangoEntity findByCodigoAndEliminadoFalse(String codigo);
	
}
