package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoFactorEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IFactorTypeJpaRepository extends PagingAndSortingRepository<TipoFactorEntity, Long> {

	public List<TipoFactorEntity> findByEliminadoFalse();
	public TipoFactorEntity findByValorAndEliminadoFalse(String valor);
	public TipoFactorEntity findByCodigoAndEliminadoFalse(String codigo);
}
