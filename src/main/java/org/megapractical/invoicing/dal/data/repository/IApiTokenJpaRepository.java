package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ApiToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IApiTokenJpaRepository extends JpaRepository<ApiToken, Long>{

	public ApiToken findFirstByOrderByIdDesc();
	
}