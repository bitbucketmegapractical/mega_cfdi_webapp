package org.megapractical.invoicing.dal.data.repository;

import java.util.List;
import java.util.Optional;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoCancelacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface ICancellationFileJpaRepository extends JpaRepository<ArchivoCancelacionEntity, Long> {

	public Optional<ArchivoCancelacionEntity> findByIdAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalse(Long id, String code);
	
	public Page<ArchivoCancelacionEntity> findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivo_CodigoAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String name, String fileStatus, Pageable pageable);
	public Page<ArchivoCancelacionEntity> findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivo_CodigoAndTipoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String name, String fileStatus, String fileType, Pageable pageable);
	
	public List<ArchivoCancelacionEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	
	public List<ArchivoCancelacionEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdAsc(ContribuyenteEntity taxpayer, String code);
	public List<ArchivoCancelacionEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String code);
	public List<ArchivoCancelacionEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndTipoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String fileStatus, String fileType);
	
	public ArchivoCancelacionEntity findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String name);
	
}