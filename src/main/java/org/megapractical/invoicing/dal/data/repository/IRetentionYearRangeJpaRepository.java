package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.RetencionEjercicioRangoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository : RetencionEjercicioRango.
 */
public interface IRetentionYearRangeJpaRepository extends PagingAndSortingRepository<RetencionEjercicioRangoEntity, Long> {

	RetencionEjercicioRangoEntity findByEliminadoFalse();
	
}