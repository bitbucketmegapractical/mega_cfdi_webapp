package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPostalCodeJpaRepository extends PagingAndSortingRepository<CodigoPostalEntity, Long> {

	public List<CodigoPostalEntity> findAll();
	public List<CodigoPostalEntity> findByEliminadoFalse();
	public CodigoPostalEntity findByCodigoAndEliminadoFalse(String codigo);
	public CodigoPostalEntity findByValorAndEliminadoFalse(String valor);
}
