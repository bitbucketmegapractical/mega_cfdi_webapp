package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoContratoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IContractJpaRepository extends PagingAndSortingRepository<TipoContratoEntity, Long> {

	public List<TipoContratoEntity> findByEliminadoFalse();
	public TipoContratoEntity findByCodigoAndEliminadoFalse(String code);
	public TipoContratoEntity findByValorAndEliminadoFalse(String valor);
}
