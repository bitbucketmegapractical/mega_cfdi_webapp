package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.NominaFicheroEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPayrollDataFileJpaRepository extends PagingAndSortingRepository<NominaFicheroEntity, Long> {

	public NominaFicheroEntity findByCodigoIgnoreCaseAndEliminadoFalse(String code);
	
}
