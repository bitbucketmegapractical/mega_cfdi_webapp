package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.PaisEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ICountryJpaRepository extends PagingAndSortingRepository<PaisEntity, Long> {

	public List<PaisEntity> findAll();
	public List<PaisEntity> findByEliminadoFalse();
	public PaisEntity findByCodigoAndEliminadoFalse(String codigo);
	public PaisEntity findByValorAndEliminadoFalse(String valor);
	
}
