package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ConceptoTipoImpuestoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IConceptTaxTypeJpaRepository extends PagingAndSortingRepository<ConceptoTipoImpuestoEntity, Long> {

	public ConceptoTipoImpuestoEntity findByCodigoAndEliminadoFalse(String codigo);
	
}
