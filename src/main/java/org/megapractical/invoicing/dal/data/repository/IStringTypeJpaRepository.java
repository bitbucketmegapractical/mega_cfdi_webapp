package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoCadenaPagoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IStringTypeJpaRepository extends PagingAndSortingRepository<TipoCadenaPagoEntity, Long> {

	public List<TipoCadenaPagoEntity> findAll();
	public List<TipoCadenaPagoEntity> findByEliminadoFalse();
	public TipoCadenaPagoEntity findByCodigoAndEliminadoFalse(String codigo);
	public TipoCadenaPagoEntity findByValorAndEliminadoFalse(String valor);
	
}
