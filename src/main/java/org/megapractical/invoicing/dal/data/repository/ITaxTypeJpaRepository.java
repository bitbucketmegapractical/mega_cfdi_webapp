package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoImpuestoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxTypeJpaRepository extends PagingAndSortingRepository<TipoImpuestoEntity, Long> {

	public List<TipoImpuestoEntity> findByEliminadoFalse();
	
	public TipoImpuestoEntity findByCodigo(String codigo);
	
	public TipoImpuestoEntity findByCodigoAndEliminadoFalse(String codigo);
	
	public TipoImpuestoEntity findByValor(String valor);
	
	public TipoImpuestoEntity findByValorAndEliminadoFalse(String valor);
	
}