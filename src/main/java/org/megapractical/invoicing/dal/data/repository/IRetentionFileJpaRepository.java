package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionFileJpaRepository extends PagingAndSortingRepository<ArchivoRetencionEntity, Long> {

	public Page<ArchivoRetencionEntity> findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivoAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String name, EstadoArchivoEntity statusFile, Pageable pageable);
	public Page<ArchivoRetencionEntity> findByContribuyenteAndNombreContainingIgnoreCaseAndPeriodoEjercicioAndEstadoArchivoAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String name, String year, EstadoArchivoEntity statusFile, Pageable pageable);
	
	public List<ArchivoRetencionEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	public List<ArchivoRetencionEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String code);
	public ArchivoRetencionEntity findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String name);
	
}
