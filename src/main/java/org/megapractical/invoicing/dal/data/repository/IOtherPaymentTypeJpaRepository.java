package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoOtroPagoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IOtherPaymentTypeJpaRepository extends PagingAndSortingRepository<TipoOtroPagoEntity, Long> {

	public List<TipoOtroPagoEntity> findByEliminadoFalse();
	public TipoOtroPagoEntity findByCodigoAndEliminadoFalse(String code);
	public TipoOtroPagoEntity findByValorAndEliminadoFalse(String valor);
	
}
