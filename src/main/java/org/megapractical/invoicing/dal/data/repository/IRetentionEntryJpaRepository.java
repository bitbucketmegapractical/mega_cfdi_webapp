package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionFicheroEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionTramaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionEntryJpaRepository extends PagingAndSortingRepository<RetencionTramaEntity, Long> {

	public List<RetencionTramaEntity> findByRetencionFicheroAndEliminadoFalse(RetencionFicheroEntity retentionDataFile);
	public RetencionTramaEntity findByNombreIgnoreCaseAndEliminadoFalse(String name);
	
}
