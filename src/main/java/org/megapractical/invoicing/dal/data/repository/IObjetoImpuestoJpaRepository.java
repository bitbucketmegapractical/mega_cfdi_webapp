package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ObjetoImpuestoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository : ObjetoImpuesto.
 */
public interface IObjetoImpuestoJpaRepository extends PagingAndSortingRepository<ObjetoImpuestoEntity, Long> {
	
	List<ObjetoImpuestoEntity> findByEliminadoFalse();
	
	ObjetoImpuestoEntity findByCodigoAndEliminadoFalse(String codigo);
	
	ObjetoImpuestoEntity findByValorAndEliminadoFalse(String valor);
	
}