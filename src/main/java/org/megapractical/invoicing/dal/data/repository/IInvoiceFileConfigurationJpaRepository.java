package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository : ArchivoNominaConfiguracion.
 */
public interface IInvoiceFileConfigurationJpaRepository extends PagingAndSortingRepository<ArchivoFacturaConfiguracionEntity, Long> {
	
	ArchivoFacturaConfiguracionEntity findByContribuyente(ContribuyenteEntity taxpayer);
	
}