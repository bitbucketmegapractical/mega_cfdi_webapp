package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoDeduccionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IDeductionTypeJpaRepository extends PagingAndSortingRepository<TipoDeduccionEntity, Long> {

	public List<TipoDeduccionEntity> findByEliminadoFalse();
	public TipoDeduccionEntity findByCodigoAndEliminadoFalse(String code);
	public TipoDeduccionEntity findByValorAndEliminadoFalse(String valor);
	
}
