package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ClaveProductoServicioEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IKeyProductServiceJpaRepository extends PagingAndSortingRepository<ClaveProductoServicioEntity, Long> {
	
	public List<ClaveProductoServicioEntity> findAll();
	public List<ClaveProductoServicioEntity> findByEliminadoFalse();
	public ClaveProductoServicioEntity findByCodigoAndEliminadoFalse(String codigo);
	public ClaveProductoServicioEntity findByValorAndEliminadoFalse(String valor);
	public ClaveProductoServicioEntity findByCodigoAndIncluirIvaAndEliminadoFalse(String codigo, String ivaInclude);
	public ClaveProductoServicioEntity findByCodigoAndIncluirIepsTrasladadoAndEliminadoFalse(String codigo, String ivaIeps);
	public ClaveProductoServicioEntity findByCodigoAndComplementoDebeIncluirAndEliminadoFalse(String codigo, String complemento);
}
