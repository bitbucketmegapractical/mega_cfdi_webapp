package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoJornadaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IDayTypeJpaRepository extends PagingAndSortingRepository<TipoJornadaEntity, Long> {

	public List<TipoJornadaEntity> findByEliminadoFalse();
	public TipoJornadaEntity findByCodigoAndEliminadoFalse(String code);
	public TipoJornadaEntity findByValorAndEliminadoFalse(String valor);
	
}
