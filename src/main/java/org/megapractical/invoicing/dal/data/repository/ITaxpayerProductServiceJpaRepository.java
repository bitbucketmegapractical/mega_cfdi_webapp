package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteProductoServicioEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerProductServiceJpaRepository extends PagingAndSortingRepository<ContribuyenteProductoServicioEntity, Long> {

	ContribuyenteProductoServicioEntity findByContribuyente(ContribuyenteEntity taxpayer);
	ContribuyenteProductoServicioEntity findByContribuyenteAndHabilitadoTrue(ContribuyenteEntity taxpayer);
	
}
