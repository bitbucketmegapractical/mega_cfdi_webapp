package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerJpaRepository extends PagingAndSortingRepository<ContribuyenteEntity, Long> {

	public ContribuyenteEntity findByRfc(String rfc);
	public ContribuyenteEntity findByNombreRazonSocial(String name);
	public ContribuyenteEntity findByCodigoSeguridad(String securityCode);
	
}