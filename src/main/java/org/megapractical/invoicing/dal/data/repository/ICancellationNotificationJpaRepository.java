package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.NotificacionCancelacionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface ICancellationNotificationJpaRepository extends JpaRepository<NotificacionCancelacionEntity, Long> {
	
}