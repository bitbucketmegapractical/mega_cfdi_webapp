package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoPersonaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPersonTypeJpaRepository extends PagingAndSortingRepository<TipoPersonaEntity, Long> {

	public List<TipoPersonaEntity> findAll();
	public List<TipoPersonaEntity> findByEliminadoFalse();
	public TipoPersonaEntity findByCodigoAndEliminadoFalse(String codigo);
	public TipoPersonaEntity findByValorAndEliminadoFalse(String valor);
	
}
