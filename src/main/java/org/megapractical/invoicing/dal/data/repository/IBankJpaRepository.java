package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.BancoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IBankJpaRepository extends PagingAndSortingRepository<BancoEntity, Long> {

	public List<BancoEntity> findByEliminadoFalse();
	public BancoEntity findByCodigoAndEliminadoFalse(String code);
	public BancoEntity findByValorAndEliminadoFalse(String valor);
	
}
