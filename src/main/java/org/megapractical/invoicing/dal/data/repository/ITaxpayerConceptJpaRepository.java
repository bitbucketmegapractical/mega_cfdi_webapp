package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteConceptoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerConceptJpaRepository extends PagingAndSortingRepository<ContribuyenteConceptoEntity, Long> {

	List<ContribuyenteConceptoEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	Page<ContribuyenteConceptoEntity> findByContribuyenteAndDescripcionContainingIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String description, Pageable pageable);
	
	ContribuyenteConceptoEntity findByContribuyenteAndDescripcionIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String description);
	ContribuyenteConceptoEntity findByContribuyenteAndDescripcionIgnoreCaseAndEliminadoTrue(ContribuyenteEntity taxpayer, String description);
	ContribuyenteConceptoEntity findByIdAndEliminadoFalse(Long id);
	
}