package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.EntidadFederativaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IFederalEntityJpaRepository extends PagingAndSortingRepository<EntidadFederativaEntity, Long> {

	public List<EntidadFederativaEntity> findByEliminadoFalse();
	public EntidadFederativaEntity findByCodigoAndEliminadoFalse(String code);
	public EntidadFederativaEntity findByValorAndEliminadoFalse(String valor);
	
}
