package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionTramaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionFieldJpaRepository extends PagingAndSortingRepository<RetencionCampoEntity, Long> {

	public List<RetencionCampoEntity> findByRetencionTramaAndEliminadoFalse(RetencionTramaEntity retentionEntry);
	public RetencionCampoEntity findByRetencionTramaAndPosicionAndEliminadoFalse(RetencionTramaEntity retentionEntry, Integer position);
	public RetencionCampoEntity findByNombreAndEliminadoFalse(String name);
	
}
