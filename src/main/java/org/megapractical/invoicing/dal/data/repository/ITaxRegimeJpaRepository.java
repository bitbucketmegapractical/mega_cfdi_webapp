package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxRegimeJpaRepository extends PagingAndSortingRepository<RegimenFiscalEntity, Long> {

	public List<RegimenFiscalEntity> findAll();
	public List<RegimenFiscalEntity> findByEliminadoFalse();
	public RegimenFiscalEntity findByCodigoAndEliminadoFalse(String codigo);
	public RegimenFiscalEntity findByValorAndEliminadoFalse(String valor);
	public RegimenFiscalEntity findByCodigoAndEliminadoFalseAndPersonaFisicaTrue(String codigo);
	public RegimenFiscalEntity findByCodigoAndEliminadoFalseAndPersonaMoralTrue(String codigo);
	public Page<RegimenFiscalEntity> findAll(Pageable pageable);
	
}
