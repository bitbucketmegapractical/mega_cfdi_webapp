package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteLugarExpedicionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerPostalCode extends PagingAndSortingRepository<ContribuyenteLugarExpedicionEntity, Long> {
	
	public ContribuyenteLugarExpedicionEntity findByIdAndEliminadoFalse(Long id);
	public ContribuyenteLugarExpedicionEntity findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	public ContribuyenteLugarExpedicionEntity findByContribuyenteAndCodigoPostalAndEliminadoFalse(ContribuyenteEntity taxpayer, CodigoPostalEntity postalCode);
	public ContribuyenteLugarExpedicionEntity findByContribuyenteAndCodigoPostalAndEliminadoTrue(ContribuyenteEntity taxpayer, CodigoPostalEntity postalCode);
	
}