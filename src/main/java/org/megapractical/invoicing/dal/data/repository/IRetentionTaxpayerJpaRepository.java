package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionTipoContribuyenteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionTaxpayerJpaRepository extends PagingAndSortingRepository<RetencionTipoContribuyenteEntity, Long> {

	public List<RetencionTipoContribuyenteEntity> findByEliminadoFalse();
	public RetencionTipoContribuyenteEntity findByCodigoAndEliminadoFalse(String code);
	public RetencionTipoContribuyenteEntity findByValorAndEliminadoFalse(String value);
	
}