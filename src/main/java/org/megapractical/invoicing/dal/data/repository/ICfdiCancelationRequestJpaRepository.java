package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.CfdiSolicitudCancelacionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ICfdiCancelationRequestJpaRepository extends PagingAndSortingRepository<CfdiSolicitudCancelacionEntity, Long> {

	public CfdiSolicitudCancelacionEntity findByUuidIgnoreCaseAndSolicitudActivaTrue(String uuid);
	public List<CfdiSolicitudCancelacionEntity> findBySolicitudActivaTrue();
	
}