package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioContratoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IConsigneeContractJpaRepository extends PagingAndSortingRepository<ConsignatarioContratoEntity, Long> {

	public Page<ConsignatarioContratoEntity> findByConsignatario(ConsignatarioEntity consignee, Pageable pageable);
	public List<ConsignatarioContratoEntity> findByConsignatario(ConsignatarioEntity consignee);
	
	public ConsignatarioContratoEntity findByConsignatarioAndActivoTrue(ConsignatarioEntity consignee);
	public ConsignatarioContratoEntity findByRutaContrato(String contractPath);
	
}
