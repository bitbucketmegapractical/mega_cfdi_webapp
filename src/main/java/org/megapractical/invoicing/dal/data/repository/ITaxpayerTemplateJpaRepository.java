package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyentePlantillaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerTemplateJpaRepository extends PagingAndSortingRepository<ContribuyentePlantillaEntity, Long> {
	
	public ContribuyentePlantillaEntity findByContribuyenteAndCodigoTipoComprobanteAndHabilitadoTrue(ContribuyenteEntity contribuyente, String code);
	
	public ContribuyentePlantillaEntity findByContribuyenteAndHabilitadoTrue(ContribuyenteEntity contribuyente);
	
}
