package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ProductoServicioEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IProductServiceJpaRepository extends PagingAndSortingRepository<ProductoServicioEntity, Long> {

	public ProductoServicioEntity findByCodigoAndHabilitadoTrue(String codigo);
	
}
