package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.TareaProgramadaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IScheduledTaskJpaRepository extends PagingAndSortingRepository<TareaProgramadaEntity, Long> {

	public TareaProgramadaEntity findByCodigoAndHabilitadoTrue(String code);
}
