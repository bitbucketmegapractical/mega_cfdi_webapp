package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPaymentWayJpaRepository extends PagingAndSortingRepository<FormaPagoEntity, Long> {

	public List<FormaPagoEntity> findAll();
	public List<FormaPagoEntity> findByEliminadoFalse();
	public FormaPagoEntity findByCodigoAndEliminadoFalse(String codigo);
	public FormaPagoEntity findByValorAndEliminadoFalse(String valor);
	
}
