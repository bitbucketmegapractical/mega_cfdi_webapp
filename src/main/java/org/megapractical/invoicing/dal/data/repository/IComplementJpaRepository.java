package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ComplementoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IComplementJpaRepository extends PagingAndSortingRepository<ComplementoEntity, Long> {

	List<ComplementoEntity> findByHabilitadoTrue();
	
	ComplementoEntity findByCodigoAndHabilitadoTrue(String code);
}
