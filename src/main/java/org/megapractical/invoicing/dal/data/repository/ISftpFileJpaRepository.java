package org.megapractical.invoicing.dal.data.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.megapractical.invoicing.dal.bean.datatype.Area;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoStfpEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface ISftpFileJpaRepository extends JpaRepository<ArchivoStfpEntity, Long> {
	
	Optional<ArchivoStfpEntity> findByIdAndEliminadoFalse(Long id);
	
	Optional<ArchivoStfpEntity> findByNombreIgnoreCaseAndEliminadoFalse(String name);
	
	@Query("FROM ArchivoStfpEntity a " + 
		   "WHERE a.contribuyente = :taxpayer " +
		   "AND lower(a.nombre) like lower(concat('%', :name, '%')) " + 
		   "AND (cast(:date as date) IS NULL OR a.fechaCarga = :date) " + 
		   "AND a.origen = 'SFTP' " +
		   "AND a.area IS NOT NULL " +
		   "AND a.area IN (:areas)" +
		   "AND a.eliminado = false")
	Page<ArchivoStfpEntity> findSftpFiles(@Param("taxpayer") ContribuyenteEntity taxpayer, @Param("name") String name,
			@Param("date") LocalDate date, @Param("areas") List<Area> areas, Pageable pageable);
	
}