package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ConsignatarioEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IConsigneeJpaRepository extends PagingAndSortingRepository<ConsignatarioEntity, Long> {

	public Page<ConsignatarioEntity> findAll(Pageable pageable);
	
	public ConsignatarioEntity findByContribuyente(ContribuyenteEntity taxpayer);
	public ConsignatarioEntity findByContribuyenteAndActivoFalse(ContribuyenteEntity taxpayer);	
	public ConsignatarioEntity findByContribuyenteAndActivoTrue(ContribuyenteEntity taxpayer);
}
