package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoFicheroEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPaymentDataFileJpaRepository extends PagingAndSortingRepository<ComplementoPagoFicheroEntity, Long> {

	public ComplementoPagoFicheroEntity findByCodigoIgnoreCaseAndEliminadoFalse(String code);
	
}
