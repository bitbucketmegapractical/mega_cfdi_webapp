package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.OrigenRecursoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IResourceOriginJpaRepository extends PagingAndSortingRepository<OrigenRecursoEntity, Long> {

	public List<OrigenRecursoEntity> findByEliminadoFalse();
	public OrigenRecursoEntity findByCodigoAndEliminadoFalse(String code);
	public OrigenRecursoEntity findByValorAndEliminadoFalse(String valor);
	
}
