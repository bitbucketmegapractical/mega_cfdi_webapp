package org.megapractical.invoicing.dal.data.repository;

import java.util.Optional;

import org.megapractical.invoicing.dal.bean.jpa.CancelacionCodigoRespuestaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface ICancellationResponseCodeJpaRepository extends JpaRepository<CancelacionCodigoRespuestaEntity, Long> {
	
	Optional<CancelacionCodigoRespuestaEntity> findByCodigoAndEliminadoFalse(String code);
	
}