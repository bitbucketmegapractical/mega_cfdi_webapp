package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IStampJpaRepository extends PagingAndSortingRepository<TimbreEntity, Long> {

	public TimbreEntity findByContribuyente(ContribuyenteEntity contribuyenteEntity);
	public TimbreEntity findByContribuyenteAndHabilitadoTrue(ContribuyenteEntity contribuyenteEntity);
	
}
