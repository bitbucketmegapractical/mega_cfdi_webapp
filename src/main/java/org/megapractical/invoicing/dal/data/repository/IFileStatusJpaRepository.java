package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IFileStatusJpaRepository extends PagingAndSortingRepository<EstadoArchivoEntity, Long> {

	public EstadoArchivoEntity findByCodigoAndEliminadoFalse(String code);
	
}
