package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.LenguajeEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ILanguageJpaRepository extends PagingAndSortingRepository<LenguajeEntity, Long> {

	public LenguajeEntity findByCodigoAndEliminadoFalse(String code);
	
}