package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.BitacoraCancelacionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface ICancellationLogJpaRepository extends JpaRepository<BitacoraCancelacionEntity, Long> {
	
}