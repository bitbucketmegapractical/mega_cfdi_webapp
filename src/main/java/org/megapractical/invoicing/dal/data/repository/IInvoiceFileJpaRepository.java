package org.megapractical.invoicing.dal.data.repository;

import java.util.List;
import java.util.Optional;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IInvoiceFileJpaRepository extends PagingAndSortingRepository<ArchivoFacturaEntity, Long> {

	public Optional<ArchivoFacturaEntity> findByIdAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalse(Long id, String code);
	
	public Page<ArchivoFacturaEntity> findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivo_CodigoAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String name, String fileStatus, Pageable pageable);
	public Page<ArchivoFacturaEntity> findByContribuyenteAndNombreContainingIgnoreCaseAndEstadoArchivo_CodigoAndTipoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String name, String fileStatus, String fileType, Pageable pageable);
	
	public List<ArchivoFacturaEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	
	public List<ArchivoFacturaEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdAsc(ContribuyenteEntity taxpayer, String code);
	public List<ArchivoFacturaEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String code);
	public List<ArchivoFacturaEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndTipoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String fileStatus, String fileType);
	
	public ArchivoFacturaEntity findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String name);
	
	@Query("Select distinct(archivoFactura.ejercicio) from ArchivoFacturaEntity archivoFactura where archivoFactura.eliminado = false")
	public List<String> findByDistinctYear();
}
