package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.NominaCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaTramaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPayrollFieldJpaRepository extends PagingAndSortingRepository<NominaCampoEntity, Long> {

	public List<NominaCampoEntity> findByNominaTramaAndEliminadoFalse(NominaTramaEntity payrollEntry);
	public NominaCampoEntity findByNominaTramaAndPosicionAndEliminadoFalse(NominaTramaEntity payrollEntry, Integer position);
	public NominaCampoEntity findByNombreAndEliminadoFalse(String name);
	
}
