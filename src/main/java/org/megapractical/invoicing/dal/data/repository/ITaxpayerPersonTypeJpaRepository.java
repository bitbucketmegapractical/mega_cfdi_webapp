package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteTipoPersonaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerPersonTypeJpaRepository extends PagingAndSortingRepository<ContribuyenteTipoPersonaEntity, Long> {
	
	public ContribuyenteTipoPersonaEntity findByContribuyente(ContribuyenteEntity contribuyente);
	
}
