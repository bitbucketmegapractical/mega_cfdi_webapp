package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ClienteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ICustomerJpaRepository extends PagingAndSortingRepository<ClienteEntity, Long> {

	public List<ClienteEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	public Page<ClienteEntity> findByContribuyenteAndRfcContainingIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String rfc, Pageable pageable);
	
	public ClienteEntity findByIdAndEliminadoFalse(Long id);
	public ClienteEntity findByContribuyenteAndRfcIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String rfc);	
	public ClienteEntity findByContribuyenteAndRfcIgnoreCaseAndEliminadoTrue(ContribuyenteEntity taxpayer, String rfc);
	
}
