package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ICurrencyJpaRepository extends PagingAndSortingRepository<MonedaEntity, Long> {

	public List<MonedaEntity> findAll();
	public List<MonedaEntity> findByEliminadoFalse();
	public MonedaEntity findByCodigoAndEliminadoFalse(String codigo);
	public MonedaEntity findByValorAndEliminadoFalse(String valor);
	
}
