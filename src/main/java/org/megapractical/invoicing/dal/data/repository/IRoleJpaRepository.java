package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.RolEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRoleJpaRepository extends PagingAndSortingRepository<RolEntity, Long> {

	public RolEntity findByCodigo(String codigo);
	
}
