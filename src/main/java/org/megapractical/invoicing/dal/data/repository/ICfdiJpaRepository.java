package org.megapractical.invoicing.dal.data.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.CfdiEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ICfdiJpaRepository extends JpaRepository<CfdiEntity, Long> {
	
	public CfdiEntity findByUuidIgnoreCaseAndCanceladoFalse(String uuid);
	public CfdiEntity findByUuidIgnoreCaseAndCanceladoTrue(String uuid);
	
	public CfdiEntity findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull(String uuid);
	public CfdiEntity findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoTrueAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull(String uuid);
	public CfdiEntity findByUuidIgnoreCaseAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndRutaPdfNotNull(String uuid);
	
	public List<CfdiEntity> findByCanceladoFalseAndRutaXmlNotNullAndFechaExpedicionBetween(Date startDate, Date endDate);
	public List<CfdiEntity> findByCanceladoFalseAndCancelacionEnProcesoTrueAndSolicitudCancelacionFalseAndRutaXmlNotNullAndRutaPdfNotNull();
	
	//##### CFDI VIGENTES [MEGA-CFDI PLUS]
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(ContribuyenteEntity emitter, String uuid, String rfc, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date startDate, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date startDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(ContribuyenteEntity emitter, String uuid, String rfc, Date startDate, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, Date startDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, Date endDate, Pageable pageable);
	
	//##### CFDI CANCELADOS [MEGA-CFDI PLUS]
	public Page<CfdiEntity> findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(ContribuyenteEntity emitter, String uuid, String rfc, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date startDate, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date startDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(ContribuyenteEntity emitter, String uuid, String rfc, Date startDate, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, Date startDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, Date endDate, Pageable pageable);
	
	//##### CFDI CON SOLICITUD DE CANCELACION [MEGA-CFDI FREE / MEGA-CFDI PLUS]
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(ContribuyenteEntity emitter, String uuid, String rfc, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCase(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date startDate, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date startDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, String clave, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetween(ContribuyenteEntity emitter, String uuid, String rfc, Date startDate, Date endDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, Date startDate, Pageable pageable);
	public Page<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionTrueAndRutaXmlNotNullAndUuidContainingIgnoreCaseAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqual(ContribuyenteEntity emitter, String uuid, String rfc, Date endDate, Pageable pageable);
	
	public CfdiEntity findByContribuyenteAndSerieIgnoreCaseAndFolioIgnoreCase(ContribuyenteEntity emitter, String sheet, String folio);
	public CfdiEntity findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndSerieIgnoreCaseAndFolioIgnoreCase(ContribuyenteEntity emitter, String serie, String sheet);
	public CfdiEntity findByContribuyenteAndCanceladoTrueAndSerieIgnoreCaseAndFolioIgnoreCase(ContribuyenteEntity emitter, String serie, String sheet);
	public List<CfdiEntity> findByContribuyenteAndCanceladoFalseAndCancelacionEnProcesoFalseAndSolicitudCancelacionFalseAndSerieIgnoreCase(ContribuyenteEntity emitter, String serie);
	
	/**
	 * @param emitter
	 * @param uuid
	 * @param receptorRfc
	 * @param pageable
	 * @return
	 */
	@Query(value = "select a.* FROM facturacion.cfdi as a JOIN facturacion.cfdi_solicitud_cancelacion as b ON a.uuid = b.uuid " 
			+ "WHERE a.id_emisor = :emitter "
			+ "AND a.importado = false "
			+ "AND a.cancelado = false "
			+ "AND a.cancelacion_en_proceso = false "
			+ "AND a.solicitud_cancelacion = true "
			+ "AND a.ruta_xml is not null "
			+ "AND a.ruta_pdf is not null "
			+ "AND a.uuid like lower(concat('%', :uuid, '%')) "
			+ "AND a.receptor_rfc like lower(concat('%', :receptorRfc, '%')) "
			+ "ORDER BY ?#{#pageable} ",countQuery = "SELECT count(a.*) FROM facturacion.cfdi as a JOIN facturacion.cfdi_solicitud_cancelacion as b ON a.uuid = b.uuid "
					+ "WHERE a.id_emisor = :emitter "
					+ "AND a.importado = false "
					+ "AND a.cancelado = false "
					+ "AND a.cancelacion_en_proceso = false "
					+ "AND a.solicitud_cancelacion = true "
					+ "AND a.ruta_xml is not null "
					+ "AND a.ruta_pdf is not null "
					+ "AND a.uuid like lower(concat('%', :uuid, '%')) "
					+ "AND a.receptor_rfc like lower(concat('%', :receptorRfc, '%')) ORDER BY ?#{#pageable}", nativeQuery=true)
	public Page<CfdiEntity> findByFechaSolicitud(@Param("emitter") Long emitter,@Param("uuid") String uuid,@Param("receptorRfc") String receptorRfc, Pageable pageable);
	
	public CfdiEntity findFirstByArchivoFacturaAndCanceladoFalseOrderByIdDesc(ArchivoFacturaEntity invoiceFile);
	
	public Optional<CfdiEntity> findByReceptorRfcAndSerieAndFolio(String rfc, String serie, String folio);
	
	public Optional<CfdiEntity> findByEmisorRfcAndReceptorRfcAndUuid(String emitterRfc, String receiverRfc, String uuid);
}
