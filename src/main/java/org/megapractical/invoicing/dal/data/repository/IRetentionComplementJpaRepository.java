package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionComplementoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionComplementJpaRepository extends PagingAndSortingRepository<RetencionComplementoEntity, Long> {

	public List<RetencionComplementoEntity> findByEliminadoFalse();
	public RetencionComplementoEntity findByCodigoAndEliminadoFalse(String code);
	public RetencionComplementoEntity findByValorAndEliminadoFalse(String valor);
	
}