package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoHoraEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IHourTypeJpaRepository extends PagingAndSortingRepository<TipoHoraEntity, Long> {

	public List<TipoHoraEntity> findByEliminadoFalse();
	public TipoHoraEntity findByCodigoAndEliminadoFalse(String code);
	public TipoHoraEntity findByValorAndEliminadoFalse(String valor);
	
}
