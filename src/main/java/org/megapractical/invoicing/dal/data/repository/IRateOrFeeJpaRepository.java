package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TasaCuotaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoFactorEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoImpuestoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRateOrFeeJpaRepository extends PagingAndSortingRepository<TasaCuotaEntity, Long> {

	public List<TasaCuotaEntity> findByTipoImpuestoAndTipoFactorAndTrasladoTrueAndEliminadoFalse(TipoImpuestoEntity impuesto, TipoFactorEntity factor);
	public List<TasaCuotaEntity> findByTipoImpuestoAndTipoFactorAndRetencionTrueAndEliminadoFalse(TipoImpuestoEntity impuesto, TipoFactorEntity factor);
	public List<TasaCuotaEntity> findByTipoImpuestoAndTipoFactorAndEliminadoFalseOrderByValorMaximo(TipoImpuestoEntity impuesto, TipoFactorEntity factor);
	public List<TasaCuotaEntity> findByEliminadoFalse();
	
}
