package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.MetodoPagoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPaymentMethodJpaRepository extends PagingAndSortingRepository<MetodoPagoEntity, Long> {
	
	public List<MetodoPagoEntity> findAll();
	public List<MetodoPagoEntity> findByEliminadoFalse();
	public MetodoPagoEntity findByCodigoAndEliminadoFalse(String codigo);
	public MetodoPagoEntity findByValorAndEliminadoFalse(String valor);

}
