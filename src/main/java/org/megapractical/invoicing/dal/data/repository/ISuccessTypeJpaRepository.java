package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.TipoSucesoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ISuccessTypeJpaRepository extends PagingAndSortingRepository<TipoSucesoEntity, Long> {

	public TipoSucesoEntity findByCodigo(String codigo); 
}
