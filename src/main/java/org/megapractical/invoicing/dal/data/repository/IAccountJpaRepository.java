package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.CuentaEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IAccountJpaRepository extends PagingAndSortingRepository<CuentaEntity, Long>{

	public List<CuentaEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity contribuyenteEntity);
	public Page<CuentaEntity> findByContribuyenteAndContribuyenteAsociado_RfcContainingIgnoreCaseAndEliminadoFalse(ContribuyenteEntity contribuyenteEntity, String rfc, Pageable pageable);
	
	public CuentaEntity findByIdAndEliminadoFalse(Long id);
	
	public CuentaEntity findByCorreoElectronico(String email);
	public CuentaEntity findByCorreoElectronicoAndEliminadoFalse(String email);
	public CuentaEntity findByCorreoElectronicoAndEliminadoTrue(String email);
	
	public CuentaEntity findByContribuyenteAndContribuyenteAsociado_RfcContainingIgnoreCaseAndEliminadoFalse(ContribuyenteEntity contribuyenteEntity, String rfc);
	public CuentaEntity findByContribuyenteAndContribuyenteAsociado_RfcContainingIgnoreCaseAndEliminadoTrue(ContribuyenteEntity contribuyenteEntity, String rfc);
	
	public CuentaEntity findByContribuyenteAsociadoAndEliminadoFalse(ContribuyenteEntity taxpayer);
	public CuentaEntity findByContribuyenteAndContribuyenteAsociadoAndEliminadoTrue(ContribuyenteEntity taxpayer, ContribuyenteEntity taxpayerAssociated);
	public CuentaEntity findByContribuyenteAsociado_IdAndEliminadoFalse(Long id);
	public CuentaEntity findByContribuyenteAsociado_IdAndEliminadoTrue(Long id);
	public CuentaEntity findByContribuyenteAsociado_RfcIgnoreCaseAndEliminadoFalse(String rfc);
	public CuentaEntity findByContribuyenteAsociado_RfcIgnoreCaseAndEliminadoTrue(String rfc);
}
