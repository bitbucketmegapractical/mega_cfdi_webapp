package org.megapractical.invoicing.dal.data.repository;

import java.util.Date;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PrefacturaEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPreInvoiceJpaRepository extends PagingAndSortingRepository<PrefacturaEntity, Long> {

	public Page<PrefacturaEntity> findByContribuyenteAndRutaPdfNotNullAndReceptorRfcContainingIgnoreCaseAndEliminadoFalse(ContribuyenteEntity emitter, String rfc, Pageable pageable);
	public Page<PrefacturaEntity> findByContribuyenteAndRutaPdfNotNullAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionBetweenAndEliminadoFalse(ContribuyenteEntity emitter, String rfc, Date startDate, Date endDate, Pageable pageable);
	public Page<PrefacturaEntity> findByContribuyenteAndRutaPdfNotNullAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionGreaterThanEqualAndEliminadoFalse(ContribuyenteEntity emitter, String rfc, Date startDate, Pageable pageable);
	public Page<PrefacturaEntity> findByContribuyenteAndRutaPdfNotNullAndReceptorRfcContainingIgnoreCaseAndFechaExpedicionLessThanEqualAndEliminadoFalse(ContribuyenteEntity emitter, String rfc, Date endDate, Pageable pageable);
}
