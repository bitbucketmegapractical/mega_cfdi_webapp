package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoPercepcionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPerceptionTypeJpaRepository extends PagingAndSortingRepository<TipoPercepcionEntity, Long> {

	public List<TipoPercepcionEntity> findByEliminadoFalse();
	public TipoPercepcionEntity findByCodigoAndEliminadoFalse(String code);
	public TipoPercepcionEntity findByValorAndEliminadoFalse(String valor);
	
}
