package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.TimbreCompraEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IStampBuyJpaRepository extends PagingAndSortingRepository<TimbreCompraEntity, Long> {

	public TimbreCompraEntity findByIdTransaccion(String transactionId);
	public TimbreCompraEntity findByReferencia(String reference);
	public TimbreCompraEntity findByDocumento(String document);
	
}
