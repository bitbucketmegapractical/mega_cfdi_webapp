package org.megapractical.invoicing.dal.data.repository;

import java.util.List;
import java.util.Optional;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.EstadoArchivoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPayrollFileJpaRepository extends PagingAndSortingRepository<ArchivoNominaEntity, Long> {

	public Optional<ArchivoNominaEntity> findByIdAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalse(Long id, String code);
	
	public Page<ArchivoNominaEntity> findByContribuyenteAndNombreContainingIgnoreCaseAndPeriodoContainingIgnoreCaseAndEjercicioContainingIgnoreCaseAndEstadoArchivoAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String name, String period, String year, EstadoArchivoEntity statusFile, Pageable pageable);
	public List<ArchivoNominaEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	public List<ArchivoNominaEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdAsc(ContribuyenteEntity taxpayer, String code);
	public List<ArchivoNominaEntity> findByContribuyenteAndEstadoArchivo_CodigoIgnoreCaseAndEliminadoFalseOrderByIdDesc(ContribuyenteEntity taxpayer, String code);
	public ArchivoNominaEntity findByContribuyenteAndNombreIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String name);
	
}
