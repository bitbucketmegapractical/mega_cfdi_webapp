package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ExportacionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository : Exportacion.
 */
public interface IExportationJpaRepository extends PagingAndSortingRepository<ExportacionEntity, Long> {
	
	List<ExportacionEntity> findByEliminadoFalse();
	
	ExportacionEntity findByCodigoAndEliminadoFalse(String codigo);
	
	ExportacionEntity findByValorAndEliminadoFalse(String valor);
	
}