package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoTramaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPaymentFieldJpaRepository extends PagingAndSortingRepository<ComplementoPagoCampoEntity, Long> {
	
	public List<ComplementoPagoCampoEntity> findByComplementoPagoTramaAndEliminadoFalse(ComplementoPagoTramaEntity invoiceEntry);
	public ComplementoPagoCampoEntity findByComplementoPagoTramaAndPosicionAndEliminadoFalse(ComplementoPagoTramaEntity invoiceEntry, Integer position);

}