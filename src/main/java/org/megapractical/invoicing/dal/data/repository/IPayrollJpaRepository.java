package org.megapractical.invoicing.dal.data.repository;

import java.util.Optional;

import org.megapractical.invoicing.dal.bean.jpa.NominaEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public interface IPayrollJpaRepository extends JpaRepository<NominaEntity, Long> {
	
	Optional<NominaEntity> findByCurpAndSerieAndFolio(String curp, String serie, String folio);
	
	@Query("FROM NominaEntity n " +
	       "WHERE n.emisorRfc = :emitterRfc " +
	       "AND (lower(n.curp) like lower(concat('%', :searchBy, '%')) " +
	       "OR lower(n.serie) like lower(concat('%', :searchBy, '%')) " +
	       "OR lower(n.folio) like lower(concat('%', :searchBy, '%')) " +
	       "OR lower(n.uuid) like lower(concat('%', :searchBy, '%'))) " +
	       "AND (:date = '' OR n.fechaHoraTimbrado LIKE concat(:date, '%')) " +
	       "AND n.cancelado = false")
	Page<NominaEntity> findAll(@Param("emitterRfc") String emitterRfc, @Param("searchBy") String searchBy,
			@Param("date") String date, Pageable pageable);
	
	public NominaEntity findByUuidIgnoreCaseAndCanceladoFalse(String uuid);
	
	public Optional<NominaEntity> findByEmisorRfcAndUuid(String emitterRfc, String uuid);
	
}