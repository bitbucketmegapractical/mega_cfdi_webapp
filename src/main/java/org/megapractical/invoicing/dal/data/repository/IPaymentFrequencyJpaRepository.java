package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.PeriodicidadPagoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPaymentFrequencyJpaRepository extends PagingAndSortingRepository<PeriodicidadPagoEntity, Long> {

	public List<PeriodicidadPagoEntity> findByEliminadoFalse();
	public PeriodicidadPagoEntity findByCodigoAndEliminadoFalse(String code);
	public PeriodicidadPagoEntity findByValorAndEliminadoFalse(String valor);
	
}
