package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ContactoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IContactJpaRepository extends PagingAndSortingRepository<ContactoEntity, Long> {

	public List<ContactoEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	public Page<ContactoEntity> findByContribuyenteAndNombreContainingIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String name, Pageable pageable);
	
	public ContactoEntity findByIdAndEliminadoFalse(Long id);
	public ContactoEntity findByContribuyenteAndCorreoElectronicoIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String email);
	public ContactoEntity findByContribuyenteAndCorreoElectronicoIgnoreCaseAndEliminadoTrue(ContribuyenteEntity taxpayer, String email);
}
