package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.TipoArchivoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IFileTypeJpaRepository extends PagingAndSortingRepository<TipoArchivoEntity, Long> {

	public TipoArchivoEntity findByCodigoAndEliminadoFalse(String code);
	
}
