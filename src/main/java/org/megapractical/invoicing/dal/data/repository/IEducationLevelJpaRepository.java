package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.NivelEducativoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository : NivelEducativo.
 */
public interface IEducationLevelJpaRepository extends PagingAndSortingRepository<NivelEducativoEntity, Long> {

	public List<NivelEducativoEntity> findByEliminadoFalse();
	public NivelEducativoEntity findByCodigoAndEliminadoFalse(String code);
	public NivelEducativoEntity findByValorAndEliminadoFalse(String valor);
	
}