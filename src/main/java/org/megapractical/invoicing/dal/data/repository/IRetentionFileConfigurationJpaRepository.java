package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionFileConfigurationJpaRepository extends PagingAndSortingRepository<ArchivoRetencionConfiguracionEntity, Long> {

	ArchivoRetencionConfiguracionEntity findByContribuyente(ContribuyenteEntity taxpayer);
	
}