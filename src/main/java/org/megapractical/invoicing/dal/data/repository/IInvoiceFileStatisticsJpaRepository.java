package org.megapractical.invoicing.dal.data.repository;

import java.util.Date;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IInvoiceFileStatisticsJpaRepository extends PagingAndSortingRepository<ArchivoFacturaEstadisticaEntity, Long> {

	public ArchivoFacturaEstadisticaEntity findByArchivoFactura(ArchivoFacturaEntity payrollFile);
	
	public Page<ArchivoFacturaEstadisticaEntity> findByArchivoFactura_EjercicioContainingIgnoreCaseAndArchivoFactura_EliminadoFalse(String year, Pageable pageable);
	public Page<ArchivoFacturaEstadisticaEntity> findByArchivoFactura_EjercicioContainingIgnoreCaseAndArchivoFactura_FechaCargaBetweenAndArchivoFactura_EliminadoFalse(String year, Date startDate, Date endDate, Pageable pageable);
	
	public List<ArchivoFacturaEstadisticaEntity> findByArchivoFactura_EjercicioContainingIgnoreCaseAndArchivoFactura_EliminadoFalse(String year);
	public List<ArchivoFacturaEstadisticaEntity> findByArchivoFactura_EjercicioContainingIgnoreCaseAndArchivoFactura_FechaCargaBetweenAndArchivoFactura_EliminadoFalse(String year, Date startDate, Date endDate);
}
