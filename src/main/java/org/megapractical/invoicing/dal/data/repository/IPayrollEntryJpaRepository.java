package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.NominaFicheroEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaTramaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPayrollEntryJpaRepository extends PagingAndSortingRepository<NominaTramaEntity, Long> {

	public List<NominaTramaEntity> findByNominaFicheroAndEliminadoFalse(NominaFicheroEntity payrollDataFile);
	public NominaTramaEntity findByNombreIgnoreCaseAndEliminadoFalse(String name);
	
}
