package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerCertificateJpaRepository extends PagingAndSortingRepository<ContribuyenteCertificadoEntity, Long> {
	
	public ContribuyenteCertificadoEntity findByContribuyenteAndHabilitadoTrue(ContribuyenteEntity contribuyente);
	
}
