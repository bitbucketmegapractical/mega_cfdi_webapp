package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoNominaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPayrollTypeJpaRepository extends PagingAndSortingRepository<TipoNominaEntity, Long> {

	public List<TipoNominaEntity> findByEliminadoFalse();
	public TipoNominaEntity findByCodigoAndEliminadoFalse(String code);
	public TipoNominaEntity findByValorAndEliminadoFalse(String valor);
	
}