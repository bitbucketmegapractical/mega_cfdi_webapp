package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.UsoCfdiEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ICfdiUseJpaRepository extends PagingAndSortingRepository<UsoCfdiEntity, Long> {
	
	public List<UsoCfdiEntity> findAll();
	public List<UsoCfdiEntity> findByEliminadoFalse();
	public UsoCfdiEntity findByCodigoAndEliminadoFalse(String codigo);
	public UsoCfdiEntity findByValorAndEliminadoFalse(String valor);
	public UsoCfdiEntity findByCodigoAndAplicaPersonaFisicaTrueAndEliminadoFalse(String codigo);
	public UsoCfdiEntity findByCodigoAndAplicaPersonaMoralTrueAndEliminadoFalse(String codigo);

}
