package org.megapractical.invoicing.dal.data.repository;

import java.util.Date;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.BitacoraEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ILogJpaRepository extends PagingAndSortingRepository<BitacoraEntity, Long>{

	List<BitacoraEntity> findByFechaEquals(Date date);
	List<BitacoraEntity> findByFechaEqualsAndTipoOperacion_Codigo(Date date, String code);
	List<BitacoraEntity> findByEjercicio(Integer ejercicio);
	List<BitacoraEntity> findByEjercicioAndTipoOperacion_Codigo(Integer ejercicio, String code);
	List<BitacoraEntity> findByTipoOperacion_Codigo(String code);
	List<BitacoraEntity> findByFechaEqualsOrderByFechaDescHoraDesc(Date date);
	
	@Query("SELECT DISTINCT log.mes FROM BitacoraEntity log WHERE log.ejercicio = ?1 ORDER BY log.mes")
	List<Integer> findDistinctMonthsInLog(Integer year);
	
	Long countByTipoOperacion_CodigoAndEjercicioAndMes(String code, Integer year, Integer month);
	
	List<BitacoraEntity> findByMesIsNullOrDiaIsNull();
}
