package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionTipoImpuestoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionTaxTypeJpaRepository extends PagingAndSortingRepository<RetencionTipoImpuestoEntity, Long> {

	public List<RetencionTipoImpuestoEntity> findByEliminadoFalse();
	public RetencionTipoImpuestoEntity findByCodigoAndEliminadoFalse(String code);
	public RetencionTipoImpuestoEntity findByValorAndEliminadoFalse(String value);
	
}