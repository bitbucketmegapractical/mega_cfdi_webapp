package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.TipoContribuyenteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerTypeJpaRepository extends PagingAndSortingRepository<TipoContribuyenteEntity, Long> {

	public TipoContribuyenteEntity findByCodigo(String codigo);
	
}
