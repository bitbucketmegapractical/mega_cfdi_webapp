package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoIncapacidadEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IInabilityTypeJpaRepository extends PagingAndSortingRepository<TipoIncapacidadEntity, Long> {

	public List<TipoIncapacidadEntity> findByEliminadoFalse();
	public TipoIncapacidadEntity findByCodigoAndEliminadoFalse(String code);
	public TipoIncapacidadEntity findByValorAndEliminadoFalse(String valor);
	
}
