package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.CfdiTipoRelacionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ICfdiRelationshipTypeJpaRepository extends PagingAndSortingRepository<CfdiTipoRelacionEntity, Long> {

	List<CfdiTipoRelacionEntity> findByEliminadoFalse();
	CfdiTipoRelacionEntity findByCodigoAndEliminadoFalse(String codigo);
	CfdiTipoRelacionEntity findByValorAndEliminadoFalse(String valor);
}
