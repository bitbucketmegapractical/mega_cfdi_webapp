package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionTipoDividendoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionDividendTypeJpaRepository extends PagingAndSortingRepository<RetencionTipoDividendoEntity, Long> {

	public List<RetencionTipoDividendoEntity> findByEliminadoFalse();
	public RetencionTipoDividendoEntity findByCodigoAndEliminadoFalse(String code);
	public RetencionTipoDividendoEntity findByValorAndEliminadoFalse(String valor);
	
}