package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoFicheroEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoTramaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPaymentEntryJpaRepository extends PagingAndSortingRepository<ComplementoPagoTramaEntity, Long> {
	
	public List<ComplementoPagoTramaEntity> findByComplementoPagoFicheroAndEliminadoFalse(ComplementoPagoFicheroEntity invoiceDataFile);
	
	public ComplementoPagoTramaEntity findByNombreIgnoreCaseAndEliminadoFalse(String name);

}