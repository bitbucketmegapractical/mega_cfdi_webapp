package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerSerieSheetJpaRepository extends PagingAndSortingRepository<ContribuyenteSerieFolioEntity, Long> {
	
	public List<ContribuyenteSerieFolioEntity> findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	public Page<ContribuyenteSerieFolioEntity> findByContribuyenteAndSerieContainingIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String serie, Pageable pageable);
	
	public ContribuyenteSerieFolioEntity findByIdAndEliminadoFalse(Long id);
	public ContribuyenteSerieFolioEntity findByIdAndSerieIgnoreCaseAndEliminadoFalse(Long id, String serie);
	public ContribuyenteSerieFolioEntity findByContribuyenteAndSerieIgnoreCaseAndEliminadoFalse(ContribuyenteEntity taxpayer, String serie);
	public ContribuyenteSerieFolioEntity findByContribuyenteAndSerieIgnoreCaseAndEliminadoTrue(ContribuyenteEntity taxpayer, String serie);
	
}