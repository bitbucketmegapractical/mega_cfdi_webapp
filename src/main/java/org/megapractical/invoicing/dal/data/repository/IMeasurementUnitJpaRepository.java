package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.UnidadMedidaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IMeasurementUnitJpaRepository extends PagingAndSortingRepository<UnidadMedidaEntity, Long> {
	
	public List<UnidadMedidaEntity> findAll();
	public List<UnidadMedidaEntity> findByEliminadoFalse();
	public UnidadMedidaEntity findByCodigoAndEliminadoFalse(String codigo);
	public UnidadMedidaEntity findByValorAndEliminadoFalse(String valor);

}
