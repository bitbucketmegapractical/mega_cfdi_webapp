package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteRegimenFiscalEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ITaxpayerTaxRegimeJpaRepository extends PagingAndSortingRepository<ContribuyenteRegimenFiscalEntity, Long> {
	
	public ContribuyenteRegimenFiscalEntity findByContribuyente(ContribuyenteEntity contribuyente);
	
}
