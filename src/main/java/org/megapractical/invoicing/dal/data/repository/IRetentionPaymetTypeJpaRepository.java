package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionTipoPagoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionPaymetTypeJpaRepository extends PagingAndSortingRepository<RetencionTipoPagoEntity, Long> {

	public List<RetencionTipoPagoEntity> findByEliminadoFalse();
	
	public RetencionTipoPagoEntity findByTipoPagoAndEliminadoFalse(String paymentType);
	
}