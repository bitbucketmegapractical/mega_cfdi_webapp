package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.TipoOperacionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface IOperationTypeJpaRepository extends PagingAndSortingRepository<TipoOperacionEntity, Long> {
	
	public TipoOperacionEntity findByCodigo(String codigo);
	
}