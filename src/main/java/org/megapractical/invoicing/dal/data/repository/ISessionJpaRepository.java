package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.SesionEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ISessionJpaRepository extends PagingAndSortingRepository<SesionEntity, Long> {

	public SesionEntity findByUsuario(UsuarioEntity user);
	
}
