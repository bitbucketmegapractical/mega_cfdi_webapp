package org.megapractical.invoicing.dal.data.repository;

import java.util.Optional;

import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserJpaRepository extends JpaRepository<UsuarioEntity, Long> {

	public UsuarioEntity findByCorreoElectronico(String correoElectronico);
	
	public Optional<UsuarioEntity> findByCorreoElectronicoAndActivoTrue(String correoElectronico);
	
}