package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.CredencialEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ICredentialJpaRepository extends PagingAndSortingRepository<CredencialEntity, Long> {
	
	public CredencialEntity findByUsuarioAndEliminadoFalse(UsuarioEntity user);
	public CredencialEntity findByContribuyenteAndEliminadoFalse(ContribuyenteEntity taxpayer);
	public CredencialEntity findByUsuarioAndContribuyenteAndEliminadoFalse(UsuarioEntity user, ContribuyenteEntity taxpayer);
	
	public Page<CredencialEntity> findByUsuario(UsuarioEntity user, Pageable pageable);
	public Page<CredencialEntity> findByUsuario_Id(Long id, Pageable pageable);
	
	public Page<CredencialEntity> findByEliminadoFalseAndUsuario_CorreoElectronicoContainingIgnoreCaseAndContribuyente_RfcContainingIgnoreCaseAndContribuyente_NombreRazonSocialContainingIgnoreCase(String email, String rfc, String name, Pageable pageable);
	public Page<CredencialEntity> findByUsuarioAndContribuyenteAndEliminadoFalseAndUsuario_CorreoElectronicoContainingIgnoreCaseAndContribuyente_RfcContainingIgnoreCaseAndContribuyente_NombreRazonSocialContainingIgnoreCase(ContribuyenteEntity taxpayer, UsuarioEntity user, String email, String rfc, String name, Pageable pageable);
	
}
