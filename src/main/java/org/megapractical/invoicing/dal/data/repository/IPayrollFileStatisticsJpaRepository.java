package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEstadisticaEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPayrollFileStatisticsJpaRepository extends PagingAndSortingRepository<ArchivoNominaEstadisticaEntity, Long> {

	public ArchivoNominaEstadisticaEntity findByArchivoNomina(ArchivoNominaEntity payrollFile);
	
}
