package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.RetencionFicheroEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionDataFileJpaRepository extends PagingAndSortingRepository<RetencionFicheroEntity, Long> {

	public RetencionFicheroEntity findByCodigoIgnoreCaseAndEliminadoFalse(String code);
	
}
