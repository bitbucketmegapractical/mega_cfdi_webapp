package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TimbrePaqueteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IStampPackageJpaRepository extends PagingAndSortingRepository<TimbrePaqueteEntity, Long> {

	public List<TimbrePaqueteEntity> findAll();
	public List<TimbrePaqueteEntity> findByEliminadoFalse();
	public List<TimbrePaqueteEntity> findByEliminadoFalseOrderByCantidad();
	public TimbrePaqueteEntity findByCantidad(Integer cantidad);
}
