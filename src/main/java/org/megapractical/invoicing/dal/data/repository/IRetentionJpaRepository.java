package org.megapractical.invoicing.dal.data.repository;

import java.util.Date;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface IRetentionJpaRepository extends PagingAndSortingRepository<RetencionEntity, Long> {
	
	public RetencionEntity findByUuidAndCanceladoFalse(String uuid); 
	
	static final String sql = "FROM RetencionEntity r " +
							  "WHERE r.contribuyente = :taxpayer " + 
							  "AND r.cancelado = false " +
							  "AND (lower(r.uuid) like lower(concat('%', :val, '%')) " +
							  "OR lower(r.receptorRazonSocial) like lower(concat('%', :val, '%')) " +
							  "OR lower(r.receptorRfc) like lower(concat('%', :val, '%')) " +
							  "OR lower(r.receptorCurp) like lower(concat('%', :val, '%')) " +
							  "OR lower(r.receptorNumeroIdentificacion) like lower(concat('%', :val, '%')) " +
							  "OR lower(r.receptorCorreoElectronico) like lower(concat('%', :val, '%')) " +
							  "OR lower(r.claveRetencion) like lower(concat('%', :val, '%')) " +
							  "OR lower(r.folio) like lower(concat('%', :val, '%'))) ";
	@Query(sql)
	public Page<RetencionEntity> findAll(@Param("taxpayer") ContribuyenteEntity taxpayer, @Param("val") String val, Pageable pageable);
	
	static final String sql_range = sql + "AND r.fechaExpedicion between :sdate and :edate ";
	@Query(sql_range)
	public Page<RetencionEntity> findBetween(@Param("taxpayer") ContribuyenteEntity taxpayer, @Param("val") String val, @Param("sdate") Date sdate, @Param("edate") Date edate, Pageable pageable);
	
	static final String sql_before = sql + "AND r.fechaExpedicion <= :sdate ";
	@Query(sql_before)
	public Page<RetencionEntity> findBefore(@Param("taxpayer") ContribuyenteEntity taxpayer, @Param("val") String val, @Param("sdate") Date sdate, Pageable pageable);
	
	static final String sql_after = sql + "AND r.fechaExpedicion >= :edate ";
	@Query(sql_after)
	public Page<RetencionEntity> findAfter(@Param("taxpayer") ContribuyenteEntity taxpayer, @Param("val") String val, @Param("edate") Date edate, Pageable pageable);
	
}