package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IVoucherTypeJpaRepository extends PagingAndSortingRepository<TipoComprobanteEntity, Long> {

	public List<TipoComprobanteEntity> findAll();
	public List<TipoComprobanteEntity> findByEliminadoFalse();
	public TipoComprobanteEntity findByCodigoAndEliminadoFalse(String codigo);
	public TipoComprobanteEntity findByValorAndEliminadoFalse(String valor);
	
}
