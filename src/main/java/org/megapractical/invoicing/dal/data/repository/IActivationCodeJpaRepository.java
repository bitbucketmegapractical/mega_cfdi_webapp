package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.CodigoActivacionEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IActivationCodeJpaRepository extends PagingAndSortingRepository<CodigoActivacionEntity, Long> {

	public CodigoActivacionEntity findByCodigo(String codigo);
	public CodigoActivacionEntity findByUsuario(UsuarioEntity user);
}
