package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionPaisEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRetentionCountryJpaRepository extends PagingAndSortingRepository<RetencionPaisEntity, Long> {

	public List<RetencionPaisEntity> findByEliminadoFalse();
	public RetencionPaisEntity findByCodigoAndEliminadoFalse(String code);
	public RetencionPaisEntity findByValorAndEliminadoFalse(String valor);
	
}