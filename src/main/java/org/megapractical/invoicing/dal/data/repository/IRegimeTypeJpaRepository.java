package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoRegimenEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IRegimeTypeJpaRepository extends PagingAndSortingRepository<TipoRegimenEntity, Long> {

	public List<TipoRegimenEntity> findByEliminadoFalse();
	public TipoRegimenEntity findByCodigoAndEliminadoFalse(String code);
	public TipoRegimenEntity findByValorAndEliminadoFalse(String valor);
	
}
