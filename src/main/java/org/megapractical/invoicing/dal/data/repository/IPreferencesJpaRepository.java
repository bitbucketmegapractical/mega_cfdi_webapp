package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPreferencesJpaRepository extends PagingAndSortingRepository<PreferenciasEntity, Long> {

	public PreferenciasEntity findByContribuyente(ContribuyenteEntity taxpayer);
	
}
