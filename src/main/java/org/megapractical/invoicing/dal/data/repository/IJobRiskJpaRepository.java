package org.megapractical.invoicing.dal.data.repository;

import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RiesgoPuestoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IJobRiskJpaRepository extends PagingAndSortingRepository<RiesgoPuestoEntity, Long> {

	public List<RiesgoPuestoEntity> findByEliminadoFalse();
	public RiesgoPuestoEntity findByCodigoAndEliminadoFalse(String code);
	public RiesgoPuestoEntity findByValorAndEliminadoFalse(String valor);
	
}