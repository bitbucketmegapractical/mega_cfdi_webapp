package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPayrollFileConfigurationJpaRepository extends PagingAndSortingRepository<ArchivoNominaConfiguracionEntity, Long> {

	ArchivoNominaConfiguracionEntity findByContribuyente(ContribuyenteEntity taxpayer);
}
