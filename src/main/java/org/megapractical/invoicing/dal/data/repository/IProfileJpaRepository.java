package org.megapractical.invoicing.dal.data.repository;

import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PerfilEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IProfileJpaRepository extends PagingAndSortingRepository<PerfilEntity, Long> {

	public PerfilEntity findByContribuyente(ContribuyenteEntity contribuyenteEntity);
	public Long countByRol_CodigoEquals(String code);
	
}
